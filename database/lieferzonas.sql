-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 17, 2016 at 07:05 PM
-- Server version: 5.6.30-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lieferzonas`
--

-- --------------------------------------------------------

--
-- Table structure for table `lie_access_lists`
--

CREATE TABLE IF NOT EXISTS `lie_access_lists` (
  `id` int(11) NOT NULL,
  `access_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_access_lists`
--

INSERT INTO `lie_access_lists` (`id`, `access_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Add', 'Add', 0, 0, '0000-00-00 00:00:00', '2016-05-19 23:17:49', '1'),
(2, 'Edit', 'Edit', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(3, 'View', 'View', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(4, 'Delete', 'View', 0, 0, '0000-00-00 00:00:00', '2016-05-19 23:18:12', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_access_maps`
--

CREATE TABLE IF NOT EXISTS `lie_access_maps` (
  `id` int(11) NOT NULL,
  `access_list_id` int(11) NOT NULL,
  `page_list_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_access_maps`
--

INSERT INTO `lie_access_maps` (`id`, `access_list_id`, `page_list_id`, `user_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 2, 1, 1, '2016-05-16 05:17:38', '2016-05-16 05:17:38', '2'),
(2, 2, 1, 2, 1, 1, '2016-05-16 05:17:38', '2016-05-16 05:17:38', '2'),
(3, 1, 1, 2, 1, 1, '2016-05-16 05:17:46', '2016-05-16 05:17:46', '2'),
(4, 2, 1, 2, 1, 1, '2016-05-16 05:17:46', '2016-05-16 05:17:46', '2'),
(5, 3, 1, 2, 1, 1, '2016-05-16 05:17:46', '2016-05-16 05:17:46', '2'),
(6, 2, 1, 2, 1, 1, '2016-05-16 05:26:48', '2016-05-16 05:26:48', '2'),
(7, 3, 1, 2, 1, 1, '2016-05-16 05:26:48', '2016-05-16 05:26:48', '2'),
(8, 1, 2, 2, 1, 1, '2016-05-16 05:26:48', '2016-05-16 05:26:48', '2'),
(9, 2, 2, 2, 1, 1, '2016-05-16 05:26:48', '2016-05-16 05:26:48', '2'),
(10, 1, 1, 1, 1, 1, '2016-05-16 06:43:11', '2016-05-16 06:43:11', '2'),
(11, 1, 5, 1, 2, 2, '2016-05-20 07:33:04', '2016-05-20 07:33:04', '2'),
(12, 2, 5, 1, 2, 2, '2016-05-20 07:33:04', '2016-05-20 07:33:04', '2'),
(13, 1, 6, 1, 2, 2, '2016-05-20 07:33:04', '2016-05-20 07:33:04', '2'),
(14, 2, 6, 1, 2, 2, '2016-05-20 07:33:04', '2016-05-20 07:33:04', '2'),
(15, 1, 1, 1, 2, 2, '2016-05-20 07:33:04', '2016-05-20 07:33:04', '2'),
(16, 3, 1, 1, 2, 2, '2016-05-20 07:33:04', '2016-05-20 07:33:04', '2'),
(17, 1, 5, 1, 2, 2, '2016-05-20 07:33:24', '2016-05-20 07:33:24', '2'),
(18, 2, 5, 1, 2, 2, '2016-05-20 07:33:24', '2016-05-20 07:33:24', '2'),
(19, 1, 6, 1, 2, 2, '2016-05-20 07:33:24', '2016-05-20 07:33:24', '2'),
(20, 2, 6, 1, 2, 2, '2016-05-20 07:33:24', '2016-05-20 07:33:24', '2'),
(21, 1, 5, 1, 2, 2, '2016-05-20 07:33:33', '2016-05-20 07:33:33', '2'),
(22, 2, 5, 1, 2, 2, '2016-05-20 07:33:33', '2016-05-20 07:33:33', '2'),
(23, 3, 5, 1, 2, 2, '2016-05-20 07:33:33', '2016-05-20 07:33:33', '2'),
(24, 1, 6, 1, 2, 2, '2016-05-20 07:33:33', '2016-05-20 07:33:33', '2'),
(25, 2, 6, 1, 2, 2, '2016-05-20 07:33:33', '2016-05-20 07:33:33', '2'),
(26, 1, 5, 1, 2, 2, '2016-05-20 07:33:47', '2016-05-20 07:33:47', '2'),
(27, 2, 5, 1, 2, 2, '2016-05-20 07:33:47', '2016-05-20 07:33:47', '2'),
(28, 3, 5, 1, 2, 2, '2016-05-20 07:33:47', '2016-05-20 07:33:47', '2'),
(29, 2, 6, 1, 2, 2, '2016-05-25 07:14:55', '2016-05-25 07:14:55', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_alergic_contents`
--

CREATE TABLE IF NOT EXISTS `lie_alergic_contents` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_alergic_contents`
--

INSERT INTO `lie_alergic_contents` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(5, 'test', 'rootrootrootrootrootrootrootrootroot', 0, 0, '2016-05-04 04:07:32', '2016-05-04 10:01:05', '1'),
(7, 'chilli', 'dfsdfsd', 0, 3, '2016-05-14 11:52:13', '2016-05-16 06:07:20', '1'),
(8, 'dhaniya', 'testtesttesttest', 3, 3, '2016-05-16 06:06:26', '2016-05-16 06:06:38', '1'),
(9, 'testtest', 'testtest', 3, 3, '2016-05-16 06:06:43', '2016-05-16 06:06:57', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_cities`
--

CREATE TABLE IF NOT EXISTS `lie_cities` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_cities`
--

INSERT INTO `lie_cities` (`id`, `country_id`, `state_id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 'Noida', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(2, 1, 2, 'Chennai', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(3, 1, 3, 'Mumbai', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(4, 1, 5, 'Kolkata', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(5, 1, 6, 'Jaipur', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(6, 1, 7, 'Chandigarh', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(7, 1, 8, 'Lucknow', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(8, 1, 9, 'Ahmedabad', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(9, 1, 10, 'Pune', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(10, 1, 11, 'Kochi', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_color_settings`
--

CREATE TABLE IF NOT EXISTS `lie_color_settings` (
  `id` int(11) NOT NULL,
  `color` varchar(50) NOT NULL,
  `color_code` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_color_settings`
--

INSERT INTO `lie_color_settings` (`id`, `color`, `color_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'yellows', '#000000', 0, 3, '2016-05-04 04:39:24', '2016-05-16 06:10:13', '1'),
(2, 'red', '#ca467b', 0, 0, '2016-05-04 10:15:39', '2016-05-04 10:16:27', '1'),
(3, 'ssss', '#744848', 0, 0, '2016-05-04 10:15:50', '2016-05-04 10:16:03', '2'),
(4, 'yellows', '#a74343', 0, 0, '2016-05-14 11:53:05', '2016-05-14 11:53:31', '2'),
(5, 'ssss', '#851a1a', 3, 3, '2016-05-16 06:09:49', '2016-05-16 06:10:27', '2');

-- --------------------------------------------------------

--
-- Table structure for table `lie_countries`
--

CREATE TABLE IF NOT EXISTS `lie_countries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_countries`
--

INSERT INTO `lie_countries` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'India', 'India', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(2, 'France', 'France', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(3, 'America', 'America', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(5, 'Bhutan', 'Bhutan', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(6, 'Sri Lanka', 'Sri Lanka', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(7, 'UK', 'UK', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(8, 'Belgium', 'belgium', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(9, 'Brazil', 'Brazil', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(10, 'Argentina', 'Argentina', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(11, 'Russia', 'Russia', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_currencies`
--

CREATE TABLE IF NOT EXISTS `lie_currencies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `currency_code` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_currencies`
--

INSERT INTO `lie_currencies` (`id`, `name`, `currency_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4, 'Indian', 'INR', 0, 0, '2016-05-03 12:31:51', '2016-05-04 11:02:22', '1'),
(6, 'dubai', 'euro', 0, 0, '2016-05-04 10:59:23', '2016-05-04 11:27:58', '1'),
(7, 'aaa', 'aaaaaaaaaaa', 0, 0, '2016-05-04 10:59:47', '2016-05-04 10:59:53', '2'),
(8, 'usa', 'pound', 0, 3, '2016-05-04 11:02:34', '2016-05-16 06:37:23', '0'),
(10, 'aaa', 'aaaaa', 3, 3, '2016-05-16 06:37:37', '2016-05-16 06:37:37', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_email_templates`
--

CREATE TABLE IF NOT EXISTS `lie_email_templates` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT '1 for superadmin,2 for admin,3 for users',
  `text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lie_email_types`
--

CREATE TABLE IF NOT EXISTS `lie_email_types` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_email_types`
--

INSERT INTO `lie_email_types` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'corporate', 'This is a corporate email', 0, 0, 0, 0, '1'),
(2, 'personal', 'This is a personal email', 0, 0, 0, 2016, '2'),
(4, 'personal', 'addadsdasda', 0, 3, 2016, 2016, '1'),
(6, 'Personalss', 'Personal', 3, 3, 2016, 2016, '2'),
(7, 'Vertrieb', 'Mario Bauer', 2, 2, 2016, 2016, '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_faq_categories`
--

CREATE TABLE IF NOT EXISTS `lie_faq_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_faq_categories`
--

INSERT INTO `lie_faq_categories` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, '0', '0', 0, 0, '2016-05-16 07:15:17', '2016-05-16 07:50:09', '2'),
(2, 'Test Cat', 'test cat test cat test cat test cat', 0, 0, '2016-05-16 07:44:32', '2016-05-16 07:44:32', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_faq_questions`
--

CREATE TABLE IF NOT EXISTS `lie_faq_questions` (
  `id` int(11) NOT NULL,
  `faq_category_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_faq_questions`
--

INSERT INTO `lie_faq_questions` (`id`, `faq_category_id`, `question`, `answer`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 2, 'Test FAQ', 'test faq answer test faq answer', 0, 0, '2016-05-16 07:45:00', '2016-05-16 07:45:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_city_maps`
--

CREATE TABLE IF NOT EXISTS `lie_front_city_maps` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2','') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_city_maps`
--

INSERT INTO `lie_front_city_maps` (`id`, `city_id`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 0, 0, 0, 0, '1'),
(2, 2, 2, 0, 0, 0, 0, '1'),
(3, 3, 3, 0, 0, 0, 0, '1'),
(4, 4, 4, 0, 0, 0, 0, '1'),
(5, 5, 5, 0, 0, 0, 0, '1'),
(6, 6, 6, 0, 0, 0, 0, '1'),
(7, 7, 7, 0, 0, 0, 0, '1'),
(8, 8, 8, 0, 0, 0, 0, '1'),
(9, 9, 9, 0, 0, 0, 0, '1'),
(10, 10, 10, 0, 0, 0, 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_color_maps`
--

CREATE TABLE IF NOT EXISTS `lie_front_color_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `header_color` int(11) NOT NULL,
  `background_color` int(11) NOT NULL,
  `expire_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_country_maps`
--

CREATE TABLE IF NOT EXISTS `lie_front_country_maps` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_country_maps`
--

INSERT INTO `lie_front_country_maps` (`id`, `country_id`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(2, 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(3, 3, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(4, 5, 4, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(5, 6, 5, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(6, 7, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(7, 8, 7, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(8, 9, 8, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(9, 10, 9, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(10, 11, 10, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_kitchen_maps`
--

CREATE TABLE IF NOT EXISTS `lie_front_kitchen_maps` (
  `id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_kitchen_maps`
--

INSERT INTO `lie_front_kitchen_maps` (`id`, `kitchen_id`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 0, 1, 0, 0, '0000-00-00 00:00:00', '2016-05-17 23:03:29', '1'),
(2, 0, 2, 0, 0, '0000-00-00 00:00:00', '2016-05-17 23:03:29', '1'),
(3, 0, 3, 0, 0, '0000-00-00 00:00:00', '2016-05-17 23:03:29', '1'),
(4, 0, 4, 0, 0, '0000-00-00 00:00:00', '2016-05-17 23:03:29', '1'),
(5, 0, 5, 0, 0, '0000-00-00 00:00:00', '2016-05-17 23:03:29', '1'),
(6, 6, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(7, 0, 7, 0, 0, '0000-00-00 00:00:00', '2016-05-17 23:03:29', '1'),
(8, 0, 8, 0, 0, '0000-00-00 00:00:00', '2016-05-17 23:03:29', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_pages`
--

CREATE TABLE IF NOT EXISTS `lie_front_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `type` enum('s','d') NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_pages`
--

INSERT INTO `lie_front_pages` (`id`, `title`, `slug`, `priority`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'test page', 'test-page-slug', 0, 's', 0, 0, '2016-05-16 07:46:10', '2016-05-16 07:46:15', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_page_maps`
--

CREATE TABLE IF NOT EXISTS `lie_front_page_maps` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_page_maps`
--

INSERT INTO `lie_front_page_maps` (`id`, `page_id`, `content`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'dastgsdgsdghsdvhsdhsfgvrhtjvjfdastgsdgsdghsdvhsdhsfgvrhtjvjfdastgsdgsdghsdvhsdhsfgvrhtjvjfdastgsdgsdghsdvhsdhsfgvrhtjvjfdastgsdgsdghsdvhsdhsfgvrhtjvjf', 0, 0, '2016-05-16 07:46:10', '2016-05-16 07:46:10', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_services`
--

CREATE TABLE IF NOT EXISTS `lie_front_services` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_services`
--

INSERT INTO `lie_front_services` (`id`, `name`, `description`, `valid_from`, `valid_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'test service', 'test service', '2016-05-23', '2016-05-31', 9, 1, '2016-04-01 00:00:00', '2016-05-16 07:12:19', '1'),
(2, 'test service1', 'test service1', '2016-05-24', '2016-05-27', 9, 9, '2016-05-12 07:54:52', '2016-05-12 11:41:58', '1'),
(3, 'test service2', 'test service2w', '0000-00-00', '0000-00-00', 9, 9, '2016-05-12 08:14:03', '2016-05-12 08:14:13', '1'),
(4, '', '', '0000-00-00', '0000-00-00', 9, 0, '2016-05-12 08:19:12', '2016-05-12 08:20:19', '2'),
(5, 'test service3', 'test service3', '0000-00-00', '0000-00-00', 9, 9, '2016-05-12 08:27:07', '2016-05-12 08:27:28', '1'),
(6, 'test service4', 'test service4', '2016-05-13', '2016-05-25', 9, 2, '2016-05-12 11:47:51', '2016-05-25 07:28:05', '2');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_state_maps`
--

CREATE TABLE IF NOT EXISTS `lie_front_state_maps` (
  `id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_state_maps`
--

INSERT INTO `lie_front_state_maps` (`id`, `state_id`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(2, 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(3, 3, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(4, 5, 4, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(5, 6, 5, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(6, 7, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(7, 8, 7, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(8, 9, 8, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(9, 10, 9, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(10, 11, 10, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_users`
--

CREATE TABLE IF NOT EXISTS `lie_front_users` (
  `id` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(50) NOT NULL DEFAULT '0',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_front_users`
--

INSERT INTO `lie_front_users` (`id`, `email`, `password`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'nikhil@gmail.com', '$2y$10$NGgP.6Q7M89btqz.khoVz.cdT2tDqUudv0DwWg81JrNysI.44aVay', 'meFnf8xw2w6SY5ucEjaXbc9pZCxn8pAx3Mu5ygGoiXmDAy0WXY', 0, 0, '0000-00-00 00:00:00', '2016-06-17 12:14:21', '0'),
(5, 'office@lieferzonas.at', '$2y$10$j9QeQmxm/0d0ei9A85mebu0QdKwIf9fr0nWDSjddsPoLwQLACL1ga', '0', 0, 0, '2016-06-01 00:11:03', '2016-06-01 00:11:03', '0'),
(27, 'rohan@gmail.com', '$2y$10$Iw73bVRE13t6RQflhaI86O7c2YKEFNMlZBriTIkZMk8jjIfrAgMQS', 'q7OcwAMevavgDcjUl5jWBbbb57UB2kYhM1npif5RPkoPJdY6Jl', 0, 0, '2016-06-10 11:28:23', '2016-06-10 11:29:08', '0'),
(28, 'kishan@gmail.com', '$2y$10$SUF3SGskoI23UKy/h2PkaucwJ4MociIFK1Y/wQaFP1OROMrbvKEUO', 'OULSwFM8ofA6aUx6iL900ep0Z2kNiovPEn1wo8kA5afIMNp2C3', 0, 0, '2016-06-17 06:11:18', '2016-06-17 06:32:39', '0'),
(29, 'sadasd@fsdf.com', '$2y$10$rB6Uo3ZXUvWwmQXLIjgGe.gd/lWAMX2s2GxZRnEDVq8pxzI404cRC', '0', 0, 0, '2016-06-17 06:33:24', '2016-06-17 06:33:24', '0'),
(30, 'rootert@wqew.com', '$2y$10$juYgq7ztJvQeX2269RJ4tetXWuAbgwn7wBj1Yh7ygMsUXbptpvkZa', '0', 0, 0, '2016-06-17 06:34:48', '2016-06-17 06:34:48', '0'),
(31, 'sujal@gmail.com', '$2y$10$cn4n0dHPg.TTfSpZ7v9rrez6O9RS6u4KaD0E4unObEksjCjgAea12', 'qM9t3YNX1OfSTmT85yX2WKMhg6GwXqw9hXIrAu75qerOEOw4V7', 0, 0, '2016-06-17 07:10:21', '2016-06-17 07:10:21', '0'),
(32, 'dfgdfg@sdf.com', '$2y$10$3P4cfX5QnTCGYeXWbax75e7aOXsP1o6mDte28Kzrp0/ygAoSvW5uG', 'VSqoDZ0705THPlZd2LiSI2WpL1onqFHDYywecbaxLa8RHJBmQK', 0, 0, '2016-06-17 07:14:02', '2016-06-17 07:14:25', '0'),
(33, 'pawan@gmail.com', '$2y$10$/28C9wSGXg6BVyzovbcDpuVDVvofcPl0x3vGJCrGJv2/Km4jHul1u', 'fNLFd0k48EDsYMpzC2AIuVFAQd5COxbmhFsdVZ0z1iZhVVdYI0', 0, 0, '2016-06-17 12:21:20', '2016-06-17 12:57:05', '0'),
(34, 'dfgfg@erew.com', '$2y$10$Ga2PSSmH/fxsLGROrdEE1OdTziZLH0oxZtmH4HTexVWuKLWX.niVG', 'lhkuTY0yHlRu2Xc1CHOoD1NS4Feq1Ty1UP3Bv3ehl7Vo3McOEI', 0, 0, '2016-06-17 13:01:37', '2016-06-17 13:01:37', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_user_addresses`
--

CREATE TABLE IF NOT EXISTS `lie_front_user_addresses` (
  `id` int(11) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `city` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `is_type` enum('p','a') NOT NULL COMMENT 'p->primary, s->secondary',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_user_addresses`
--

INSERT INTO `lie_front_user_addresses` (`id`, `front_user_id`, `mobile`, `address1`, `address2`, `city`, `state`, `country`, `zipcode`, `latitude`, `longitude`, `is_type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 28, 'kishan@gmail.com', 'mohan estate', 'new delhi', 0, 0, 0, '110044', '', '', 'p', 0, 0, '2016-06-17 06:11:18', '2016-06-17 06:11:18', '0'),
(2, 29, 'sadasd@fsdf.com', 'sfsdfsdf', 'sdfsdf', 0, 0, 0, 'sdfsdf', '', '', 'p', 0, 0, '2016-06-17 06:33:24', '2016-06-17 06:33:24', '0'),
(3, 31, 'sujal@gmail.com', 'mohan estate', 'new delhi', 0, 0, 0, '110044', '', '', 'p', 0, 0, '2016-06-17 07:10:21', '2016-06-17 07:10:21', '0'),
(4, 32, 'dfgdfg@sdf.com', '435435', '435435', 435, 0, 0, '435', '', '', 'p', 0, 0, '2016-06-17 07:14:02', '2016-06-17 07:14:02', '0'),
(5, 33, 'pawan@gmail.com', 'hgjhghjg', 'hgfhhf', 0, 0, 0, '45454', '', '', 'p', 0, 0, '2016-06-17 12:21:20', '2016-06-17 12:21:20', '0'),
(6, 34, 'dfgfg@erew.com', 'sdfsd', 'fsdfsdf', 0, 0, 0, 'fsdfsdf', '', '', 'p', 0, 0, '2016-06-17 13:01:37', '2016-06-17 13:01:37', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_user_details`
--

CREATE TABLE IF NOT EXISTS `lie_front_user_details` (
  `id` int(11) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` enum('m','f','o') NOT NULL COMMENT 'm->male, f->female, o->other',
  `dob` date NOT NULL,
  `registered_via` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_user_details`
--

INSERT INTO `lie_front_user_details` (`id`, `front_user_id`, `nickname`, `fname`, `lname`, `mobile`, `email`, `gender`, `dob`, `registered_via`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'nikhil', 'nikhil', 'malik', '123456789', 'nikhil@gmail.com', 'm', '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(2, 28, '', 'kishan', 'kumar', '9654789654', 'kishan@gmail.com', 'm', '0000-00-00', 1, 0, 0, '2016-06-17 06:11:18', '2016-06-17 06:11:18', '1'),
(3, 29, '', 'adasd', 'sadas', '3243242342', 'sadasd@fsdf.com', 'm', '0000-00-00', 1, 0, 0, '2016-06-17 06:33:24', '2016-06-17 06:33:24', '1'),
(4, 30, '', 'tre', 'treter', '', 'rootert@wqew.com', 'm', '0000-00-00', 1, 0, 0, '2016-06-17 06:34:48', '2016-06-17 06:34:48', '1'),
(5, 31, '', 'sujal', 'kumar', '9654123654', 'sujal@gmail.com', 'm', '0000-00-00', 1, 0, 0, '2016-06-17 07:10:21', '2016-06-17 07:10:21', '1'),
(6, 32, '', 'gfdgfd', 'gfdgdg', '345435435', 'dfgdfg@sdf.com', 'm', '0000-00-00', 1, 0, 0, '2016-06-17 07:14:02', '2016-06-17 07:14:02', '1'),
(7, 33, '', 'pawan', 'kumar', '9654875456', 'pawan@gmail.com', 'm', '0000-00-00', 1, 0, 0, '2016-06-17 12:21:20', '2016-06-17 12:21:20', '1'),
(8, 34, '', 'gfdg', 'fdgfdg', 'sfsdf', 'dfgfg@erew.com', 'm', '0000-00-00', 1, 0, 0, '2016-06-17 13:01:37', '2016-06-17 13:01:37', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_user_email_verifications`
--

CREATE TABLE IF NOT EXISTS `lie_front_user_email_verifications` (
  `id` int(11) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `is_verified` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->no, 1->yes',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_front_user_email_verifications`
--

INSERT INTO `lie_front_user_email_verifications` (`id`, `front_user_id`, `verification_code`, `email_id`, `is_verified`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 28, 'a2lzaGFuQGdtYWlsLmNvbQ1466143878', 'kishan@gmail.com', '0', 0, 0, '2016-06-17 06:11:18', '2016-06-17 06:11:18', '0'),
(2, 29, 'c2FkYXNkQGZzZGYuY29t1466145204', 'sadasd@fsdf.com', '0', 0, 0, '2016-06-17 06:33:24', '2016-06-17 06:33:24', '0'),
(3, 30, 'cm9vdGVydEB3cWV3LmNvbQ1466145288', 'rootert@wqew.com', '0', 0, 0, '2016-06-17 06:34:48', '2016-06-17 06:34:48', '0'),
(4, 31, 'c3VqYWxAZ21haWwuY29t1466147421', 'sujal@gmail.com', '0', 0, 0, '2016-06-17 07:10:21', '2016-06-17 07:10:21', '0'),
(5, 32, 'ZGZnZGZnQHNkZi5jb201466147642', 'dfgdfg@sdf.com', '0', 0, 0, '2016-06-17 07:14:02', '2016-06-17 07:14:02', '0'),
(6, 33, 'cGF3YW5AZ21haWwuY29t1466166080', 'pawan@gmail.com', '0', 0, 0, '2016-06-17 12:21:20', '2016-06-17 12:21:20', '0'),
(7, 34, 'ZGZnZmdAZXJldy5jb201466168497', 'dfgfg@erew.com', '0', 0, 0, '2016-06-17 13:01:37', '2016-06-17 13:01:37', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_user_login_logs`
--

CREATE TABLE IF NOT EXISTS `lie_front_user_login_logs` (
  `id` int(11) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_front_user_login_logs`
--

INSERT INTO `lie_front_user_login_logs` (`id`, `front_user_id`, `type`, `ip_address`, `time`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'O', '127.0.0.1', '2016-06-11 13:00:08', 1, 1, '2016-06-11 13:00:08', '2016-06-11 13:00:08', '1'),
(2, 1, 'O', '127.0.0.1', '2016-06-11 13:28:43', 1, 1, '2016-06-11 13:28:43', '2016-06-11 13:28:43', '1'),
(3, 1, 'O', '127.0.0.1', '2016-06-17 06:01:20', 1, 1, '2016-06-17 06:01:20', '2016-06-17 06:01:20', '1'),
(4, 28, 'O', '127.0.0.1', '2016-06-17 06:32:39', 28, 28, '2016-06-17 06:32:39', '2016-06-17 06:32:39', '1'),
(5, 32, 'O', '127.0.0.1', '2016-06-17 07:14:25', 32, 32, '2016-06-17 07:14:25', '2016-06-17 07:14:25', '1'),
(6, 1, 'O', '127.0.0.1', '2016-06-17 12:14:21', 1, 1, '2016-06-17 12:14:21', '2016-06-17 12:14:21', '1'),
(7, 33, 'O', '127.0.0.1', '2016-06-17 12:57:05', 33, 33, '2016-06-17 12:57:05', '2016-06-17 12:57:05', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_user_mobile_verifications`
--

CREATE TABLE IF NOT EXISTS `lie_front_user_mobile_verifications` (
  `id` int(11) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `is_verified` enum('0','1') NOT NULL COMMENT '0->no, 1->yes',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_front_user_social_maps`
--

CREATE TABLE IF NOT EXISTS `lie_front_user_social_maps` (
  `id` int(11) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `social_type_id` int(11) NOT NULL,
  `social_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_kitchens`
--

CREATE TABLE IF NOT EXISTS `lie_kitchens` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_kitchens`
--

INSERT INTO `lie_kitchens` (`id`, `name`, `description`, `valid_from`, `valid_to`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(6, 'root', 'rootrootroot', '2016-05-06 00:00:00', '2016-05-12 00:00:00', 2, 0, 3, '2016-05-16 05:55:09', '2016-05-16 05:55:09', '1'),
(7, 'test', 'adasdasd', '2016-05-05 00:00:00', '2016-05-12 00:00:00', 2, 0, 0, '2016-05-04 09:48:03', '2016-05-04 09:48:03', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_live_feeds`
--

CREATE TABLE IF NOT EXISTS `lie_live_feeds` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_live_feeds`
--

INSERT INTO `lie_live_feeds` (`id`, `subject`, `topic`, `description`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'New Order', 'Nikhil places new order with Bercos', 'WOW Amazing. Nikhil got 10% discount, Place now and enjoy', 'https://www.google.co.in', 1, 1, '2016-05-04 00:00:00', '2016-05-04 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_marketing_tips`
--

CREATE TABLE IF NOT EXISTS `lie_marketing_tips` (
  `id` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_marketing_tips`
--

INSERT INTO `lie_marketing_tips` (`id`, `topic`, `description`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Test Tip', 'Test tip test tip test tip', 0, 0, 0, '2016-05-16 07:13:05', '2016-05-16 07:13:05', '1'),
(2, 'fdcs', 'gfdcgtfrdeshnbgvfcdxsynhbgfvcdxsayhbgvfcd s', 0, 0, 0, '2016-05-23 23:37:33', '2016-05-23 23:37:33', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_menu_categories`
--

CREATE TABLE IF NOT EXISTS `lie_menu_categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_menu_categories`
--

INSERT INTO `lie_menu_categories` (`id`, `cat_name`, `description`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(7, 'vvvv', 'vvvvvv', 4, 0, 3, '2016-05-14 12:37:38', '2016-05-16 06:45:39', '1'),
(10, 'aaa', 'sdsdsdsds', 5, 3, 3, '2016-05-16 06:45:08', '2016-05-16 06:45:45', '2'),
(11, 'aaa', 'sdfsdfsfdf', 4, 3, 3, '2016-05-16 06:45:55', '2016-05-16 06:45:55', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_menu_extras`
--

CREATE TABLE IF NOT EXISTS `lie_menu_extras` (
  `id` int(11) NOT NULL,
  `extra_type_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` varchar(6) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_menu_extras`
--

INSERT INTO `lie_menu_extras` (`id`, `extra_type_id`, `name`, `description`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 2, 'menu1', 'menu1', '1000', 9, 1, '2016-05-10 00:00:00', '2016-05-16 07:22:19', '1'),
(2, 2, 'menu4', 'menu4', '10000', 9, 9, '2016-05-13 10:08:39', '2016-05-13 10:40:17', '2'),
(3, 2, 'menu2', 'menu2', '1000', 9, 0, '2016-05-13 10:10:16', '2016-05-13 10:40:07', '2'),
(4, 2, 'menu3', 'menu3', '1000', 9, 9, '2016-05-13 10:10:23', '2016-05-13 10:40:17', '2'),
(5, 1, 'menu5', 'menu5', '1000', 9, 0, '2016-05-13 10:20:57', '2016-05-25 07:26:05', '2');

-- --------------------------------------------------------

--
-- Table structure for table `lie_menu_extra_types`
--

CREATE TABLE IF NOT EXISTS `lie_menu_extra_types` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_menu_extra_types`
--

INSERT INTO `lie_menu_extra_types` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'test', 'test', 9, 1, '2016-05-12 00:00:00', '2016-05-16 07:21:09', '1'),
(2, 'test1', 'test1', 9, 9, '2016-05-13 06:28:52', '2016-05-25 07:25:34', '2'),
(3, 'test2', 'test2', 9, 9, '2016-05-13 06:52:00', '2016-05-13 10:40:35', '2'),
(4, 'vvv', 'vvvvvvvvv', 11, 0, '2016-05-14 11:57:32', '2016-05-14 11:57:32', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_menu_price_maps`
--

CREATE TABLE IF NOT EXISTS `lie_menu_price_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `price` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_menu_price_maps`
--

INSERT INTO `lie_menu_price_maps` (`id`, `rest_detail_id`, `menu_id`, `price_type_id`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(3, 1, 2, 1, '222', 2, 2, '2016-05-31 06:41:16', '2016-05-31 06:41:16', '0'),
(4, 1, 2, 2, '555', 2, 2, '2016-05-31 06:41:17', '2016-05-31 06:41:17', '0'),
(5, 5, 5, 1, '65', 2, 2, '2016-05-31 06:44:21', '2016-05-31 06:44:21', '0'),
(6, 5, 5, 2, '95', 2, 2, '2016-05-31 06:44:21', '2016-05-31 06:44:21', '0'),
(7, 2, 6, 2, '900', 2, 2, '2016-05-31 06:45:56', '2016-05-31 06:45:56', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_metrics`
--

CREATE TABLE IF NOT EXISTS `lie_metrics` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_metrics`
--

INSERT INTO `lie_metrics` (`id`, `name`, `code`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(2, 'MIliGram', 'mg', 'mili gram gram gram gram', 0, 3, '0000-00-00 00:00:00', '2016-05-16 06:55:59', '1'),
(4, 'grams', 'grams', 'abcdabcd', 0, 3, '2016-05-14 11:46:27', '2016-05-16 07:14:50', '1'),
(6, 'abcd', 'abcdabcd', 'abcdabcd', 3, 3, '2016-05-16 06:56:14', '2016-05-16 06:56:58', '2');

-- --------------------------------------------------------

--
-- Table structure for table `lie_page_lists`
--

CREATE TABLE IF NOT EXISTS `lie_page_lists` (
  `id` int(11) NOT NULL,
  `page_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_page_lists`
--

INSERT INTO `lie_page_lists` (`id`, `page_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'pagesss', ' page 1 page 1page 1page 1page 1page 1page 1page 1page 1 page 1 page 1page 1 page 1 page 1page 1 page 1page 1page 1page 1vpage 1page 1', 0, 3, '2016-05-03 12:13:25', '2016-05-16 06:31:15', '1'),
(4, 'test', 'testtesttest', 0, 0, '2016-05-04 10:45:38', '2016-05-04 10:45:51', '2'),
(5, 'aaaaa', 'aaaaaaaaaaa', 3, 3, '2016-05-14 11:54:26', '2016-05-16 06:22:05', '1'),
(6, 'hello', 'sdfsfdfsdsdf', 3, 3, '2016-05-16 06:30:56', '2016-05-16 06:30:56', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_price_types`
--

CREATE TABLE IF NOT EXISTS `lie_price_types` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_price_types`
--

INSERT INTO `lie_price_types` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'delivery', 'This is a home delivery price', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(2, 'takeaway', 'This is a takeaway price', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_promocodes`
--

CREATE TABLE IF NOT EXISTS `lie_promocodes` (
  `id` int(11) NOT NULL,
  `coupon_code` varchar(50) NOT NULL,
  `is_rest_specific` int(11) NOT NULL,
  `is_user_specific` int(11) NOT NULL,
  `validity_type` enum('T','C') NOT NULL COMMENT 'T for table,C for calendar',
  `validity_table` int(11) NOT NULL DEFAULT '0',
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `payment_mode` enum('P','C') NOT NULL COMMENT 'P for percentage and C for cash',
  `payment_amount` varchar(10) NOT NULL,
  `min_amount` varchar(10) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_promocodes`
--

INSERT INTO `lie_promocodes` (`id`, `coupon_code`, `is_rest_specific`, `is_user_specific`, `validity_type`, `validity_table`, `valid_from`, `valid_to`, `payment_mode`, `payment_amount`, `min_amount`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'b6OTFhPe', 1, 1, 'T', 6, '2016-05-14 12:40:27', '2017-07-19 00:00:00', 'P', '10', '100', 9, 9, '2016-05-09 08:20:26', '2016-05-14 12:40:27', '0'),
(2, 'e8igML3X', 1, 1, 'T', 6, '2016-05-09 11:08:47', '2017-07-13 00:00:00', 'P', '10', '127', 9, 9, '2016-05-09 08:21:14', '2016-05-09 11:08:47', '0'),
(3, 'OXyBzSmV', 1, 1, 'C', 0, '2016-05-30 00:00:00', '2016-05-31 00:00:00', 'C', '100', '1000', 9, 2, '2016-05-09 08:22:42', '2016-05-25 07:33:42', '2'),
(4, '5fdIWgrc', 1, 1, 'C', 0, '2016-05-17 00:00:00', '2016-05-23 00:00:00', 'P', '10', '100', 9, 2, '2016-05-09 08:24:00', '2016-05-25 07:33:32', '1'),
(5, '6RQA2grJ', 1, 1, 'C', 0, '2016-05-18 00:00:00', '2016-05-31 00:00:00', 'P', '10', '100', 9, 9, '2016-05-09 08:25:42', '2016-05-10 12:47:43', '1'),
(6, '0KGLR5ui', 0, 0, 'T', 6, '2016-05-10 12:47:51', '2017-07-14 00:00:00', 'P', '10', '100', 9, 9, '2016-05-09 08:28:48', '2016-05-10 12:47:51', '1'),
(7, 'LpLGCQiP', 1, 1, 'T', 6, '2016-05-11 05:10:17', '2017-07-15 00:00:00', 'P', '10', '100', 9, 9, '2016-05-09 08:29:26', '2016-05-11 05:10:17', '1'),
(8, 'QSr8tTxA', 0, 0, 'T', 6, '2016-05-09 08:30:14', '2016-05-09 08:30:14', 'P', '10', '100', 9, 0, '2016-05-09 08:30:14', '2016-05-09 08:30:14', '1'),
(9, '6mSHMfJU', 0, 0, 'T', 6, '2016-05-09 09:40:23', '2017-10-30 00:00:00', 'P', '10', '100', 9, 0, '2016-05-09 09:40:23', '2016-05-09 09:40:23', '1'),
(10, 'i98uIKEq', 1, 1, 'C', 0, '2016-05-09 00:00:00', '2017-07-10 00:00:00', 'P', '15', '10000', 9, 9, '2016-05-09 10:38:23', '2016-05-11 05:09:52', '1'),
(11, 'p[p', 1, 0, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'C', '', 'pp[', 9, 0, '2016-05-09 11:44:20', '2016-05-09 11:44:20', '1'),
(12, 'pWEV6kvA', 1, 0, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'C', '', 'oio', 9, 9, '2016-05-09 11:56:40', '2016-05-10 10:50:10', '1'),
(13, 'TGLxtWSG', 0, 0, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'P', '100', '67', 9, 0, '2016-05-10 10:49:52', '2016-05-10 10:49:52', '1'),
(14, 'iv9tuisT', 1, 1, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'P', '', '70', 9, 0, '2016-05-10 10:54:41', '2016-05-10 10:54:41', '1'),
(15, 'KTQ4m4qc', 0, 0, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'P', '100', '1000', 9, 0, '2016-05-10 11:22:49', '2016-05-10 11:22:49', '1'),
(16, '8xG2RVWL', 0, 0, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'P', '100', '100', 9, 0, '2016-05-10 11:28:35', '2016-05-10 11:28:35', '1'),
(17, 'cpTWR3aI', 0, 0, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'P', '100', '200', 9, 0, '2016-05-10 11:29:22', '2016-05-10 11:29:22', '1'),
(18, 'ggg', 0, 0, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'P', 'fff', 'hhh', 9, 0, '2016-05-10 11:52:45', '2016-05-10 11:52:45', '1'),
(19, 'ytuy6u', 0, 0, 'C', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'C', 'oiuo', 'poip', 9, 0, '2016-05-10 12:01:07', '2016-05-10 12:01:07', '1'),
(20, '8ZRJ4zMI', 0, 0, 'C', 0, '2016-05-10 00:00:00', '2016-05-23 00:00:00', 'P', '10', '100', 9, 0, '2016-05-10 12:26:08', '2016-05-10 12:26:08', '1'),
(21, 'sbV7U7CP', 1, 1, 'T', 7, '2016-05-16 06:54:24', '2017-05-30 00:00:00', 'P', '10', '1000', 1, 1, '2016-05-16 06:53:51', '2016-05-16 06:54:24', '1'),
(22, 'SAwbC9Do', 0, 0, 'C', 0, '2016-05-01 00:00:00', '2016-05-27 00:00:00', 'C', '2', '5', 2, 0, '2016-05-17 23:35:00', '2016-05-17 23:35:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_promo_rest_maps`
--

CREATE TABLE IF NOT EXISTS `lie_promo_rest_maps` (
  `id` int(11) NOT NULL,
  `promo_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_promo_rest_maps`
--

INSERT INTO `lie_promo_rest_maps` (`id`, `promo_id`, `rest_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 9, 0, '2016-05-09 08:20:26', '2016-05-09 08:20:26', '2'),
(2, 2, 2, 9, 0, '2016-05-09 08:21:14', '2016-05-09 08:21:14', '2'),
(3, 4, 1, 9, 0, '2016-05-09 08:24:00', '2016-05-09 08:24:00', '2'),
(4, 5, 1, 9, 0, '2016-05-09 08:25:42', '2016-05-09 08:25:42', '2'),
(5, 10, 2, 9, 0, '2016-05-09 09:43:45', '2016-05-09 09:43:45', '2'),
(6, 10, 1, 9, 0, '2016-05-09 10:07:34', '2016-05-09 10:07:34', '2'),
(7, 10, 2, 9, 0, '2016-05-09 10:07:34', '2016-05-09 10:07:34', '2'),
(8, 10, 1, 9, 0, '2016-05-09 10:07:42', '2016-05-09 10:07:42', '2'),
(9, 10, 2, 9, 0, '2016-05-09 10:07:42', '2016-05-09 10:07:42', '2'),
(10, 10, 1, 9, 0, '2016-05-09 10:07:55', '2016-05-09 10:07:55', '2'),
(11, 10, 2, 9, 0, '2016-05-09 10:07:55', '2016-05-09 10:07:55', '2'),
(12, 10, 1, 9, 0, '2016-05-09 10:08:13', '2016-05-09 10:08:13', '2'),
(13, 10, 2, 9, 0, '2016-05-09 10:08:13', '2016-05-09 10:08:13', '2'),
(14, 10, 1, 9, 0, '2016-05-09 10:09:06', '2016-05-09 10:09:06', '2'),
(15, 10, 2, 9, 0, '2016-05-09 10:09:06', '2016-05-09 10:09:06', '2'),
(16, 10, 1, 9, 0, '2016-05-09 10:11:14', '2016-05-09 10:11:14', '2'),
(17, 10, 2, 9, 0, '2016-05-09 10:11:14', '2016-05-09 10:11:14', '2'),
(18, 10, 1, 9, 0, '2016-05-09 10:12:12', '2016-05-09 10:12:12', '2'),
(19, 10, 2, 9, 0, '2016-05-09 10:12:12', '2016-05-09 10:12:12', '2'),
(20, 10, 1, 9, 0, '2016-05-09 10:12:42', '2016-05-09 10:12:42', '2'),
(21, 10, 2, 9, 0, '2016-05-09 10:12:42', '2016-05-09 10:12:42', '2'),
(22, 10, 1, 9, 0, '2016-05-09 10:12:53', '2016-05-09 10:12:53', '2'),
(23, 10, 2, 9, 0, '2016-05-09 10:12:53', '2016-05-09 10:12:53', '2'),
(24, 10, 1, 9, 0, '2016-05-09 10:13:39', '2016-05-09 10:13:39', '2'),
(25, 10, 2, 9, 0, '2016-05-09 10:13:39', '2016-05-09 10:13:39', '2'),
(26, 10, 1, 9, 0, '2016-05-09 10:16:35', '2016-05-09 10:16:35', '2'),
(27, 10, 2, 9, 0, '2016-05-09 10:16:35', '2016-05-09 10:16:35', '2'),
(28, 10, 1, 9, 0, '2016-05-09 10:17:56', '2016-05-09 10:17:56', '2'),
(29, 10, 2, 9, 0, '2016-05-09 10:17:56', '2016-05-09 10:17:56', '2'),
(30, 10, 1, 9, 0, '2016-05-09 10:19:44', '2016-05-09 10:19:44', '2'),
(31, 10, 2, 9, 0, '2016-05-09 10:19:44', '2016-05-09 10:19:44', '2'),
(32, 10, 1, 9, 0, '2016-05-09 10:20:00', '2016-05-09 10:20:00', '2'),
(33, 10, 2, 9, 0, '2016-05-09 10:20:00', '2016-05-09 10:20:00', '2'),
(34, 10, 1, 9, 0, '2016-05-09 10:38:12', '2016-05-09 10:38:12', '2'),
(35, 10, 2, 9, 0, '2016-05-09 10:38:12', '2016-05-09 10:38:12', '2'),
(36, 10, 1, 9, 0, '2016-05-09 10:38:23', '2016-05-09 10:38:23', '2'),
(37, 10, 2, 9, 0, '2016-05-09 10:38:23', '2016-05-09 10:38:23', '2'),
(38, 10, 1, 9, 0, '2016-05-09 10:42:03', '2016-05-09 10:42:03', '2'),
(39, 10, 2, 9, 0, '2016-05-09 10:42:03', '2016-05-09 10:42:03', '2'),
(40, 5, 1, 9, 0, '2016-05-09 10:45:00', '2016-05-09 10:45:00', '2'),
(41, 4, 1, 9, 0, '2016-05-09 10:45:29', '2016-05-09 10:45:29', '2'),
(42, 3, 1, 9, 0, '2016-05-09 10:53:37', '2016-05-09 10:53:37', '2'),
(43, 3, 2, 9, 0, '2016-05-09 10:53:37', '2016-05-09 10:53:37', '2'),
(44, 3, 1, 9, 0, '2016-05-09 10:57:32', '2016-05-09 10:57:32', '2'),
(45, 3, 2, 9, 0, '2016-05-09 10:57:32', '2016-05-09 10:57:32', '2'),
(46, 3, 1, 9, 0, '2016-05-09 10:57:54', '2016-05-09 10:57:54', '2'),
(47, 3, 2, 9, 0, '2016-05-09 10:57:54', '2016-05-09 10:57:54', '2'),
(48, 2, 1, 9, 0, '2016-05-09 11:05:04', '2016-05-09 11:05:04', '2'),
(49, 2, 2, 9, 0, '2016-05-09 11:05:04', '2016-05-09 11:05:04', '2'),
(50, 1, 1, 9, 0, '2016-05-09 11:08:19', '2016-05-09 11:08:19', '0'),
(51, 3, 1, 9, 0, '2016-05-09 11:08:32', '2016-05-09 11:08:32', '2'),
(52, 3, 2, 9, 0, '2016-05-09 11:08:32', '2016-05-09 11:08:32', '2'),
(53, 2, 1, 9, 0, '2016-05-09 11:08:47', '2016-05-09 11:08:47', '0'),
(54, 2, 2, 9, 0, '2016-05-09 11:08:47', '2016-05-09 11:08:47', '0'),
(55, 3, 1, 9, 0, '2016-05-09 11:22:54', '2016-05-09 11:22:54', '2'),
(56, 3, 2, 9, 0, '2016-05-09 11:22:54', '2016-05-09 11:22:54', '2'),
(57, 3, 1, 9, 0, '2016-05-09 13:09:04', '2016-05-09 13:09:04', '2'),
(58, 3, 2, 9, 0, '2016-05-09 13:09:04', '2016-05-09 13:09:04', '2'),
(59, 14, 1, 9, 0, '2016-05-10 10:54:41', '2016-05-10 10:54:41', '1'),
(60, 10, 1, 9, 0, '2016-05-11 05:09:52', '2016-05-11 05:09:52', '1'),
(61, 7, 1, 9, 0, '2016-05-11 05:10:10', '2016-05-11 05:10:10', '2'),
(62, 7, 1, 9, 0, '2016-05-11 05:10:17', '2016-05-11 05:10:17', '1'),
(63, 7, 2, 9, 0, '2016-05-11 05:10:18', '2016-05-11 05:10:18', '1'),
(64, 3, 2, 9, 0, '2016-05-12 10:47:40', '2016-05-12 10:47:40', '2'),
(65, 3, 2, 9, 0, '2016-05-14 12:08:54', '2016-05-14 12:08:54', '2'),
(66, 3, 2, 9, 0, '2016-05-14 12:09:14', '2016-05-14 12:09:14', '2'),
(67, 3, 3, 9, 0, '2016-05-14 12:09:14', '2016-05-14 12:09:14', '2'),
(68, 3, 2, 9, 0, '2016-05-14 12:09:26', '2016-05-14 12:09:26', '0'),
(69, 3, 3, 9, 0, '2016-05-14 12:09:26', '2016-05-14 12:09:26', '0'),
(70, 21, 1, 1, 0, '2016-05-16 06:53:51', '2016-05-16 06:53:51', '2'),
(71, 21, 2, 1, 0, '2016-05-16 06:53:51', '2016-05-16 06:53:51', '2'),
(72, 21, 1, 1, 0, '2016-05-16 06:54:24', '2016-05-16 06:54:24', '1'),
(73, 21, 2, 1, 0, '2016-05-16 06:54:24', '2016-05-16 06:54:24', '1'),
(74, 3, 1, 2, 0, '2016-05-25 07:29:17', '2016-05-25 07:29:17', '2'),
(75, 3, 2, 2, 0, '2016-05-25 07:29:17', '2016-05-25 07:29:17', '2'),
(76, 3, 1, 2, 0, '2016-05-25 07:29:25', '2016-05-25 07:29:25', '2'),
(77, 3, 2, 2, 0, '2016-05-25 07:29:25', '2016-05-25 07:29:25', '2'),
(78, 3, 1, 2, 0, '2016-05-25 07:29:33', '2016-05-25 07:29:33', '2'),
(79, 3, 2, 2, 0, '2016-05-25 07:29:34', '2016-05-25 07:29:34', '2'),
(80, 3, 1, 2, 0, '2016-05-25 07:29:44', '2016-05-25 07:29:44', '2'),
(81, 3, 2, 2, 0, '2016-05-25 07:29:44', '2016-05-25 07:29:44', '2'),
(82, 3, 3, 2, 0, '2016-05-25 07:29:44', '2016-05-25 07:29:44', '2'),
(83, 3, 1, 2, 0, '2016-05-25 07:29:52', '2016-05-25 07:29:52', '2'),
(84, 3, 2, 2, 0, '2016-05-25 07:29:52', '2016-05-25 07:29:52', '2'),
(85, 3, 3, 2, 0, '2016-05-25 07:30:02', '2016-05-25 07:30:02', '2'),
(86, 3, 2, 2, 0, '2016-05-25 07:30:13', '2016-05-25 07:30:13', '2'),
(87, 3, 1, 2, 0, '2016-05-25 07:30:41', '2016-05-25 07:30:41', '2'),
(88, 3, 4, 2, 0, '2016-05-25 07:30:41', '2016-05-25 07:30:41', '2'),
(89, 3, 1, 2, 0, '2016-05-25 07:30:56', '2016-05-25 07:30:56', '2'),
(90, 3, 2, 2, 0, '2016-05-25 07:30:56', '2016-05-25 07:30:56', '2'),
(91, 3, 3, 2, 0, '2016-05-25 07:30:56', '2016-05-25 07:30:56', '2'),
(92, 3, 4, 2, 0, '2016-05-25 07:30:56', '2016-05-25 07:30:56', '2'),
(93, 3, 2, 2, 0, '2016-05-25 07:31:24', '2016-05-25 07:31:24', '1'),
(94, 3, 3, 2, 0, '2016-05-25 07:31:24', '2016-05-25 07:31:24', '1'),
(95, 4, 1, 2, 0, '2016-05-25 07:32:58', '2016-05-25 07:32:58', '2'),
(96, 4, 1, 2, 0, '2016-05-25 07:33:05', '2016-05-25 07:33:05', '2'),
(97, 4, 2, 2, 0, '2016-05-25 07:33:05', '2016-05-25 07:33:05', '2'),
(98, 4, 1, 2, 0, '2016-05-25 07:33:17', '2016-05-25 07:33:17', '2'),
(99, 4, 1, 2, 0, '2016-05-25 07:33:24', '2016-05-25 07:33:24', '2'),
(100, 4, 4, 2, 0, '2016-05-25 07:33:24', '2016-05-25 07:33:24', '2'),
(101, 4, 1, 2, 0, '2016-05-25 07:33:32', '2016-05-25 07:33:32', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_promo_user_maps`
--

CREATE TABLE IF NOT EXISTS `lie_promo_user_maps` (
  `id` int(11) NOT NULL,
  `promo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_promo_user_maps`
--

INSERT INTO `lie_promo_user_maps` (`id`, `promo_id`, `user_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 2, 9, 0, '2016-05-09 08:20:26', '2016-05-09 08:20:26', '2'),
(2, 1, 4, 9, 0, '2016-05-09 08:20:26', '2016-05-09 08:20:26', '2'),
(3, 2, 4, 9, 0, '2016-05-09 08:21:14', '2016-05-09 08:21:14', '2'),
(4, 2, 10, 9, 0, '2016-05-09 08:21:14', '2016-05-09 08:21:14', '2'),
(5, 4, 1, 9, 0, '2016-05-09 08:24:01', '2016-05-09 08:24:01', '2'),
(6, 5, 1, 9, 0, '2016-05-09 08:25:42', '2016-05-09 08:25:42', '2'),
(7, 10, 2, 9, 0, '2016-05-09 09:43:45', '2016-05-09 09:43:45', '2'),
(8, 10, 2, 9, 0, '2016-05-09 10:07:34', '2016-05-09 10:07:34', '2'),
(9, 10, 2, 9, 0, '2016-05-09 10:07:42', '2016-05-09 10:07:42', '2'),
(10, 10, 10, 9, 0, '2016-05-09 10:07:42', '2016-05-09 10:07:42', '2'),
(11, 10, 2, 9, 0, '2016-05-09 10:07:55', '2016-05-09 10:07:55', '2'),
(12, 10, 10, 9, 0, '2016-05-09 10:07:55', '2016-05-09 10:07:55', '2'),
(13, 10, 2, 9, 0, '2016-05-09 10:08:13', '2016-05-09 10:08:13', '2'),
(14, 10, 10, 9, 0, '2016-05-09 10:08:13', '2016-05-09 10:08:13', '2'),
(15, 10, 2, 9, 0, '2016-05-09 10:09:06', '2016-05-09 10:09:06', '2'),
(16, 10, 10, 9, 0, '2016-05-09 10:09:06', '2016-05-09 10:09:06', '2'),
(17, 10, 2, 9, 0, '2016-05-09 10:11:14', '2016-05-09 10:11:14', '2'),
(18, 10, 10, 9, 0, '2016-05-09 10:11:14', '2016-05-09 10:11:14', '2'),
(19, 10, 2, 9, 0, '2016-05-09 10:12:12', '2016-05-09 10:12:12', '2'),
(20, 10, 10, 9, 0, '2016-05-09 10:12:12', '2016-05-09 10:12:12', '2'),
(21, 10, 2, 9, 0, '2016-05-09 10:12:42', '2016-05-09 10:12:42', '2'),
(22, 10, 10, 9, 0, '2016-05-09 10:12:42', '2016-05-09 10:12:42', '2'),
(23, 10, 2, 9, 0, '2016-05-09 10:12:53', '2016-05-09 10:12:53', '2'),
(24, 10, 10, 9, 0, '2016-05-09 10:12:53', '2016-05-09 10:12:53', '2'),
(25, 10, 2, 9, 0, '2016-05-09 10:13:40', '2016-05-09 10:13:40', '2'),
(26, 10, 10, 9, 0, '2016-05-09 10:13:40', '2016-05-09 10:13:40', '2'),
(27, 10, 2, 9, 0, '2016-05-09 10:16:35', '2016-05-09 10:16:35', '2'),
(28, 10, 10, 9, 0, '2016-05-09 10:16:35', '2016-05-09 10:16:35', '2'),
(29, 10, 2, 9, 0, '2016-05-09 10:17:56', '2016-05-09 10:17:56', '2'),
(30, 10, 10, 9, 0, '2016-05-09 10:17:56', '2016-05-09 10:17:56', '2'),
(31, 10, 2, 9, 0, '2016-05-09 10:19:44', '2016-05-09 10:19:44', '2'),
(32, 10, 10, 9, 0, '2016-05-09 10:19:44', '2016-05-09 10:19:44', '2'),
(33, 10, 2, 9, 0, '2016-05-09 10:20:00', '2016-05-09 10:20:00', '2'),
(34, 10, 10, 9, 0, '2016-05-09 10:20:00', '2016-05-09 10:20:00', '2'),
(35, 10, 2, 9, 0, '2016-05-09 10:38:12', '2016-05-09 10:38:12', '2'),
(36, 10, 10, 9, 0, '2016-05-09 10:38:12', '2016-05-09 10:38:12', '2'),
(37, 10, 2, 9, 0, '2016-05-09 10:38:23', '2016-05-09 10:38:23', '2'),
(38, 10, 10, 9, 0, '2016-05-09 10:38:23', '2016-05-09 10:38:23', '2'),
(39, 10, 2, 9, 0, '2016-05-09 10:42:03', '2016-05-09 10:42:03', '2'),
(40, 10, 10, 9, 0, '2016-05-09 10:42:03', '2016-05-09 10:42:03', '2'),
(41, 5, 1, 9, 0, '2016-05-09 10:45:00', '2016-05-09 10:45:00', '2'),
(42, 4, 1, 9, 0, '2016-05-09 10:45:29', '2016-05-09 10:45:29', '2'),
(43, 3, 1, 9, 0, '2016-05-09 10:53:37', '2016-05-09 10:53:37', '2'),
(44, 3, 4, 9, 0, '2016-05-09 10:53:38', '2016-05-09 10:53:38', '2'),
(45, 3, 1, 9, 0, '2016-05-09 10:57:32', '2016-05-09 10:57:32', '2'),
(46, 3, 4, 9, 0, '2016-05-09 10:57:32', '2016-05-09 10:57:32', '2'),
(47, 3, 1, 9, 0, '2016-05-09 10:57:54', '2016-05-09 10:57:54', '2'),
(48, 3, 4, 9, 0, '2016-05-09 10:57:54', '2016-05-09 10:57:54', '2'),
(49, 2, 4, 9, 0, '2016-05-09 11:05:04', '2016-05-09 11:05:04', '2'),
(50, 2, 10, 9, 0, '2016-05-09 11:05:04', '2016-05-09 11:05:04', '2'),
(51, 1, 2, 9, 0, '2016-05-09 11:08:19', '2016-05-09 11:08:19', '0'),
(52, 1, 4, 9, 0, '2016-05-09 11:08:19', '2016-05-09 11:08:19', '0'),
(53, 3, 1, 9, 0, '2016-05-09 11:08:32', '2016-05-09 11:08:32', '2'),
(54, 3, 4, 9, 0, '2016-05-09 11:08:32', '2016-05-09 11:08:32', '2'),
(55, 2, 4, 9, 0, '2016-05-09 11:08:47', '2016-05-09 11:08:47', '0'),
(56, 2, 10, 9, 0, '2016-05-09 11:08:47', '2016-05-09 11:08:47', '0'),
(57, 3, 1, 9, 0, '2016-05-09 11:22:54', '2016-05-09 11:22:54', '2'),
(58, 3, 4, 9, 0, '2016-05-09 11:22:54', '2016-05-09 11:22:54', '2'),
(59, 3, 1, 9, 0, '2016-05-09 13:09:04', '2016-05-09 13:09:04', '2'),
(60, 3, 4, 9, 0, '2016-05-09 13:09:04', '2016-05-09 13:09:04', '2'),
(61, 4, 1, 9, 0, '2016-05-09 13:09:20', '2016-05-09 13:09:20', '2'),
(62, 3, 1, 9, 0, '2016-05-09 13:09:34', '2016-05-09 13:09:34', '2'),
(63, 3, 4, 9, 0, '2016-05-09 13:09:34', '2016-05-09 13:09:34', '2'),
(64, 5, 1, 9, 0, '2016-05-09 13:09:49', '2016-05-09 13:09:49', '2'),
(65, 3, 1, 9, 0, '2016-05-10 10:49:58', '2016-05-10 10:49:58', '2'),
(66, 3, 4, 9, 0, '2016-05-10 10:49:58', '2016-05-10 10:49:58', '2'),
(67, 3, 1, 9, 0, '2016-05-10 10:51:57', '2016-05-10 10:51:57', '2'),
(68, 3, 4, 9, 0, '2016-05-10 10:51:57', '2016-05-10 10:51:57', '2'),
(69, 3, 1, 9, 0, '2016-05-10 10:52:06', '2016-05-10 10:52:06', '2'),
(70, 3, 4, 9, 0, '2016-05-10 10:52:06', '2016-05-10 10:52:06', '2'),
(71, 3, 1, 9, 0, '2016-05-10 10:54:49', '2016-05-10 10:54:49', '2'),
(72, 3, 4, 9, 0, '2016-05-10 10:54:49', '2016-05-10 10:54:49', '2'),
(73, 4, 1, 9, 0, '2016-05-10 10:54:54', '2016-05-10 10:54:54', '2'),
(74, 3, 1, 9, 0, '2016-05-10 12:06:12', '2016-05-10 12:06:12', '2'),
(75, 3, 4, 9, 0, '2016-05-10 12:06:12', '2016-05-10 12:06:12', '2'),
(76, 3, 1, 9, 0, '2016-05-10 12:09:55', '2016-05-10 12:09:55', '2'),
(77, 3, 4, 9, 0, '2016-05-10 12:09:55', '2016-05-10 12:09:55', '2'),
(78, 5, 1, 9, 0, '2016-05-10 12:47:43', '2016-05-10 12:47:43', '1'),
(79, 10, 2, 9, 0, '2016-05-11 05:09:52', '2016-05-11 05:09:52', '1'),
(80, 10, 4, 9, 0, '2016-05-11 05:09:52', '2016-05-11 05:09:52', '1'),
(81, 10, 10, 9, 0, '2016-05-11 05:09:52', '2016-05-11 05:09:52', '1'),
(82, 7, 1, 9, 0, '2016-05-11 05:10:10', '2016-05-11 05:10:10', '2'),
(83, 7, 1, 9, 0, '2016-05-11 05:10:18', '2016-05-11 05:10:18', '1'),
(84, 3, 1, 9, 0, '2016-05-12 10:47:40', '2016-05-12 10:47:40', '2'),
(85, 3, 4, 9, 0, '2016-05-12 10:47:40', '2016-05-12 10:47:40', '2'),
(86, 21, 1, 1, 0, '2016-05-16 06:53:51', '2016-05-16 06:53:51', '2'),
(87, 21, 2, 1, 0, '2016-05-16 06:53:51', '2016-05-16 06:53:51', '2'),
(88, 21, 1, 1, 0, '2016-05-16 06:54:24', '2016-05-16 06:54:24', '1'),
(89, 21, 2, 1, 0, '2016-05-16 06:54:24', '2016-05-16 06:54:24', '1'),
(90, 3, 1, 2, 0, '2016-05-25 07:29:17', '2016-05-25 07:29:17', '2'),
(91, 3, 2, 2, 0, '2016-05-25 07:29:17', '2016-05-25 07:29:17', '2'),
(92, 3, 1, 2, 0, '2016-05-25 07:29:25', '2016-05-25 07:29:25', '2'),
(93, 3, 2, 2, 0, '2016-05-25 07:29:25', '2016-05-25 07:29:25', '2'),
(94, 3, 1, 2, 0, '2016-05-25 07:29:34', '2016-05-25 07:29:34', '2'),
(95, 3, 2, 2, 0, '2016-05-25 07:29:34', '2016-05-25 07:29:34', '2'),
(96, 3, 1, 2, 0, '2016-05-25 07:29:44', '2016-05-25 07:29:44', '2'),
(97, 3, 1, 2, 0, '2016-05-25 07:29:52', '2016-05-25 07:29:52', '2'),
(98, 3, 1, 2, 0, '2016-05-25 07:30:03', '2016-05-25 07:30:03', '2'),
(99, 3, 1, 2, 0, '2016-05-25 07:30:13', '2016-05-25 07:30:13', '2'),
(100, 3, 1, 2, 0, '2016-05-25 07:30:27', '2016-05-25 07:30:27', '2'),
(101, 3, 1, 2, 0, '2016-05-25 07:30:41', '2016-05-25 07:30:41', '2'),
(102, 3, 1, 2, 0, '2016-05-25 07:31:16', '2016-05-25 07:31:16', '2'),
(103, 3, 1, 2, 0, '2016-05-25 07:31:24', '2016-05-25 07:31:24', '1'),
(104, 4, 1, 2, 0, '2016-05-25 07:32:58', '2016-05-25 07:32:58', '2'),
(105, 4, 1, 2, 0, '2016-05-25 07:33:06', '2016-05-25 07:33:06', '2'),
(106, 4, 1, 2, 0, '2016-05-25 07:33:17', '2016-05-25 07:33:17', '2'),
(107, 4, 1, 2, 0, '2016-05-25 07:33:24', '2016-05-25 07:33:24', '2'),
(108, 4, 1, 2, 0, '2016-05-25 07:33:32', '2016-05-25 07:33:32', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_admin_contact_us`
--

CREATE TABLE IF NOT EXISTS `lie_rest_admin_contact_us` (
  `id` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_categories`
--

CREATE TABLE IF NOT EXISTS `lie_rest_categories` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `root_cat_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_categories`
--

INSERT INTO `lie_rest_categories` (`id`, `rest_detail_id`, `root_cat_id`, `name`, `description`, `image`, `valid_from`, `valid_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 3, 'first category', 'This is first category', '1464676327597.jpg', '2016-05-27 00:00:00', '2016-06-09 00:00:00', 2, 2, '2016-05-31 06:32:07', '2016-05-31 06:32:07', '1'),
(2, 2, 3, 'second category', 'This is a second category', '1464676388676.jpg', '2016-05-27 00:00:00', '2016-06-11 00:00:00', 2, 2, '2016-05-31 06:33:08', '2016-05-31 06:33:08', '1'),
(3, 5, 9, 'jayeka category', 'This is jayeka first category', '1464676432535.jpg', '2016-05-20 00:00:00', '2016-06-06 00:00:00', 2, 2, '2016-05-31 06:33:52', '2016-05-31 06:33:52', '1'),
(4, 5, 12, 'jayeka second category', 'This is a jayeka second category', '1464676490845.jpg', '2016-05-25 00:00:00', '2016-06-13 00:00:00', 2, 2, '2016-05-31 06:34:50', '2016-05-31 06:34:50', '1'),
(5, 1, 3, 'uytuytu', 'ytuytu', '', '2016-06-22 00:00:00', '2016-06-30 00:00:00', 2, 2, '2016-06-14 05:02:09', '2016-06-14 05:02:09', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_cust_bonus`
--

CREATE TABLE IF NOT EXISTS `lie_rest_cust_bonus` (
  `id` int(11) NOT NULL,
  `value` float(16,2) NOT NULL,
  `type` enum('p','c') NOT NULL DEFAULT 'p' COMMENT 'p for percent,c for cash',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_rest_cust_bonus`
--

INSERT INTO `lie_rest_cust_bonus` (`id`, `value`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 50.00, 'p', 9, 9, '2016-05-17 06:39:56', '2016-05-17 06:39:56', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_cust_bonus_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_cust_bonus_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `bonus_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_days`
--

CREATE TABLE IF NOT EXISTS `lie_rest_days` (
  `id` int(11) NOT NULL,
  `day` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_rest_days`
--

INSERT INTO `lie_rest_days` (`id`, `day`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Monday', 0, 0, '2016-04-28 10:59:37', '2016-04-28 10:59:37', '0'),
(2, 'Tuesday', 0, 0, '2016-04-28 10:59:37', '2016-04-28 10:59:37', '0'),
(3, 'Wednesday', 0, 0, '2016-04-28 10:59:37', '2016-04-28 10:59:37', '0'),
(4, 'Thursday', 0, 0, '2016-04-28 10:59:37', '2016-04-28 10:59:37', '0'),
(5, 'Friday', 0, 0, '2016-04-28 10:59:37', '2016-04-28 10:59:37', '0'),
(6, 'Saturday', 0, 0, '2016-04-28 10:59:37', '2016-04-28 10:59:37', '0'),
(7, 'Sunday', 0, 0, '2016-04-28 10:59:37', '2016-04-28 10:59:37', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_day_time_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_day_time_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `open_time` varchar(50) NOT NULL,
  `close_time` varchar(50) NOT NULL,
  `is_open` int(11) NOT NULL,
  `is_24` int(11) NOT NULL COMMENT '1 for open, 0 for close',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_day_time_maps`
--

INSERT INTO `lie_rest_day_time_maps` (`id`, `rest_detail_id`, `day_id`, `open_time`, `close_time`, `is_open`, `is_24`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(3, 1, 1, '00:00 AM', '11:59 PM', 1, 1, 3, 3, '2016-05-04 04:40:32', '2016-05-16 06:17:25', '1'),
(4, 1, 4, '12:00 PM', '04:30 PM', 1, 0, 3, 3, '2016-05-04 04:40:32', '2016-05-16 06:17:25', '1'),
(5, 2, 1, '01:30 AM', '06:00 PM', 1, 0, 0, 0, '2016-05-04 10:17:26', '2016-05-14 12:58:40', '1'),
(6, 2, 2, '00:00 AM', '11:59 PM', 1, 0, 0, 0, '2016-05-04 10:22:20', '2016-05-14 12:58:40', '1'),
(7, 2, 3, '00:00 AM', '11:59 PM', 1, 1, 0, 0, '2016-05-04 10:28:06', '2016-05-14 12:58:40', '1'),
(8, 2, 4, '00:00 AM', '11:59 PM', 1, 1, 0, 0, '2016-05-04 10:32:31', '2016-05-14 12:58:40', '1'),
(9, 1, 5, '08:30 AM', '10:00 AM', 1, 0, 3, 3, '2016-05-14 11:53:58', '2016-05-16 06:17:25', '1'),
(10, 3, 1, '00:00 AM', '00:30 AM', 1, 0, 3, 3, '2016-05-16 06:17:11', '2016-05-16 06:17:11', '1'),
(11, 3, 6, '01:00 AM', '03:30 AM', 1, 0, 3, 3, '2016-05-16 06:17:11', '2016-05-16 06:17:11', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_details`
--

CREATE TABLE IF NOT EXISTS `lie_rest_details` (
  `id` int(11) NOT NULL,
  `f_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `add1` varchar(100) NOT NULL,
  `add2` varchar(100) NOT NULL,
  `city` varchar(40) NOT NULL,
  `state` varchar(40) NOT NULL,
  `country` varchar(40) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `tin_number` varchar(50) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `total_email` int(11) NOT NULL,
  `total_mobile` int(11) NOT NULL,
  `total_landline` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_details`
--

INSERT INTO `lie_rest_details` (`id`, `f_name`, `l_name`, `add1`, `add2`, `city`, `state`, `country`, `pincode`, `tin_number`, `latitude`, `longitude`, `fax`, `total_email`, `total_mobile`, `total_landline`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'food', 'plaza', 'gandhi chowk', 'ewrwerewr', '1', '1', '', '845401', '3343434', '', '', '5845621', 0, 0, 0, 0, 1, '2016-04-26 07:52:03', '2016-05-26 05:02:58', '1'),
(2, 'Annapurna', '', 'motihari', 'tyuytu', 'motihari', 'bihar', '', '', '', '', '', '125478', 0, 0, 0, 0, 0, '2016-04-26 07:54:40', '2016-05-26 05:03:18', '1'),
(3, 'Suniti', 'Sweets', 'gandhi chowk', '', 'motihari', 'bihar', 'FRA', '', '', '', '', '345345', 0, 0, 0, 0, 0, '2016-04-26 07:57:07', '2016-04-26 07:57:07', '1'),
(4, 'jayeka', '', 'meena bazar', '', 'motihari', 'bihar', 'FRA', '', '', '', '', '125412', 0, 0, 0, 0, 0, '2016-04-27 05:05:54', '2016-04-27 05:05:54', '1'),
(5, 'jayeka', 'hotel', 'main road', 'motihari', 'motihari', 'bihar', '', '', '', '', '', '25415784', 2, 2, 1, 2, 2, '2016-05-25 08:10:29', '2016-05-25 10:39:13', '1'),
(6, 'wqewqe', 'wqewqasdfg', 'wqe', 'wqe', 'wqe', 'wqe', '', '', '', '', '', '', 2, 1, 1, 2, 2, '2016-05-30 11:42:13', '2016-06-16 06:54:59', '1'),
(7, 'retert', 'ertet', 'rtre', 'tertreterter', 'tert', 'retret', '-', '', '', '', '', '', 1, 1, 1, 2, 2, '2016-06-16 07:00:10', '2016-06-16 07:00:10', '1'),
(8, 'zxcvbnm', 'qwerty', 'sfsdf', 'sdfsdfds', 'fdsfdsf', 'dsfdsfdsf', '-', '', '', '', '', 'sdfsdf', 1, 1, 1, 2, 2, '2016-06-16 07:00:53', '2016-06-16 07:00:53', '1'),
(9, 'shankar', 'sweets', 'dasdsad', 'sadasdasd', 'asdsad', 'asdsad', '-', '', '', '', '', '3123', 1, 1, 1, 2, 2, '2016-06-16 07:01:50', '2016-06-16 07:01:50', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_email_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_email_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `email_type` varchar(30) NOT NULL,
  `is_primary` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_email_maps`
--

INSERT INTO `lie_rest_email_maps` (`id`, `rest_detail_id`, `email`, `email_type`, `is_primary`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4, 3, 'suniti@gmail.com', '1', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(5, 4, 'jayeka@gmail.com', '1', '0', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(6, 4, 'roshan@gmail.com', '2', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(33, 5, 'jayeka@gmail.com', '1', '0', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(34, 5, 'jk@gmail.com', '4', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(39, 6, 'erew@erew.com', '', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(40, 6, '', '', '0', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(41, 7, '', '', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(42, 8, 'erew@erew.com', '1', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(43, 9, 'gokul@gmail.com', '', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_extra_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_extra_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_extra_type_id` int(11) NOT NULL,
  `menu_extra_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_extra_type_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_extra_type_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_extra_type_id` int(11) NOT NULL,
  `price` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_extra_type_maps`
--

INSERT INTO `lie_rest_extra_type_maps` (`id`, `rest_detail_id`, `menu_extra_type_id`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(2, 1, 2, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(5, 1, 1, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(7, 3, 1, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(8, 3, 2, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(9, 1, 4, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(10, 2, 2, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_kitchen_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_kitchen_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_kitchen_maps`
--

INSERT INTO `lie_rest_kitchen_maps` (`id`, `rest_detail_id`, `kitchen_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(12, 3, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(47, 1111, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(48, 1111, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(52, 1, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(53, 2, 6, 2, 2, '2016-05-26 05:05:04', '2016-05-26 05:05:04', '1'),
(54, 5, 6, 2, 2, '2016-05-27 05:05:31', '2016-05-27 05:05:31', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_landline_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_landline_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `landline` varchar(15) NOT NULL,
  `extension` varchar(6) NOT NULL,
  `name` varchar(40) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_landline_maps`
--

INSERT INTO `lie_rest_landline_maps` (`id`, `rest_detail_id`, `landline`, `extension`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 3, '30181', '152', 'Counter', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(2, 3, '30181', '155', 'Acoount', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(3, 4, '21542', '211', 'test', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(4, 4, '21542', '144', '251', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(24, 5, '21542', '144', 'gfdgdfg', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(27, 6, 'wqe', 'wqeqwe', 'wqe', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(28, 7, '06252', '', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(29, 8, '06252', '144', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(30, 9, '06252', '144', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_menus`
--

CREATE TABLE IF NOT EXISTS `lie_rest_menus` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `rest_category_id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(150) NOT NULL,
  `code` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `info` varchar(255) NOT NULL,
  `priority` varchar(20) NOT NULL,
  `have_submenu` enum('0','1') NOT NULL COMMENT '''0=>no submenu,1=>having submenu',
  `is_alergic` enum('0','1') NOT NULL COMMENT '0=>no alergic,1=>its alergic',
  `is_spicy` enum('0','1') NOT NULL COMMENT '0=>no spicy,1=>its spicy',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_menus`
--

INSERT INTO `lie_rest_menus` (`id`, `rest_detail_id`, `rest_category_id`, `kitchen_id`, `name`, `short_name`, `code`, `description`, `image`, `info`, `priority`, `have_submenu`, `is_alergic`, `is_spicy`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 0, 1, 0, 'food first menu', 'qwerty', 'ffm251', 'This is food first menu', '1464676796119.jpg', 'This is food first menu', '2', '0', '0', '0', 2, 2, '2016-05-31 06:39:56', '2016-06-14 09:58:41', '1'),
(2, 1, 1, 6, 'food second menu', 'fsm', 'fsm253', 'This is a food second menu', '1464676876719.jpg', 'This is a food second menu', '3', '0', '0', '0', 2, 2, '2016-05-31 06:41:16', '2016-05-31 06:41:16', '1'),
(3, 5, 3, 6, 'jayeka first menu', 'jfm', 'jfm878', 'This is jayeka first menu', '1464676932875.jpg', 'This is jayeka first menu', '3', '1', '0', '0', 2, 2, '2016-05-31 06:42:12', '2016-05-31 06:42:12', '1'),
(4, 5, 4, 6, 'jayeka second menu', 'jsm', 'jsm676', 'This is a jayeka second menu', '1464676989533.jpg', 'This is a jayeka second menu', '5', '1', '0', '0', 2, 2, '2016-05-31 06:43:09', '2016-05-31 06:43:09', '1'),
(5, 5, 3, 6, 'jayeka third menu', 'jtc', 'jtc256', 'This is a jayeka third menu', '1464677061862.jpg', 'This is a jayeka third menu', '6', '0', '0', '0', 2, 2, '2016-05-31 06:44:21', '2016-05-31 06:44:21', '1'),
(6, 2, 2, 6, 'anapurna first category', 'afc', 'afc543', 'This is a annapurna first category', '1464677156198.jpg', 'This is a annapurna first category', '5', '0', '0', '0', 2, 2, '2016-05-31 06:45:56', '2016-05-31 06:45:56', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_menu_allergic_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_menu_allergic_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `allergic_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_menu_allergic_maps`
--

INSERT INTO `lie_rest_menu_allergic_maps` (`id`, `rest_detail_id`, `menu_id`, `allergic_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 5, 7, 2, 2, '2016-05-25 10:36:45', '2016-05-25 10:36:45', '1'),
(10, 1, 8, 7, 2, 2, '2016-05-26 13:30:17', '2016-05-26 13:30:17', '1'),
(11, 2, 10, 7, 2, 2, '2016-05-27 05:03:41', '2016-05-27 05:03:41', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_menu_spice_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_menu_spice_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `spice_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_menu_spice_maps`
--

INSERT INTO `lie_rest_menu_spice_maps` (`id`, `rest_detail_id`, `menu_id`, `spice_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 5, 1, 2, 2, '2016-05-25 10:36:46', '2016-05-25 10:36:46', '1'),
(35, 1, 8, 1, 2, 2, '2016-05-26 13:30:17', '2016-05-26 13:30:17', '1'),
(36, 1, 8, 2, 2, 2, '2016-05-26 13:30:17', '2016-05-26 13:30:17', '1'),
(37, 1, 8, 3, 2, 2, '2016-05-26 13:30:17', '2016-05-26 13:30:17', '1'),
(38, 1, 8, 5, 2, 2, '2016-05-26 13:30:17', '2016-05-26 13:30:17', '1'),
(39, 1, 8, 6, 2, 2, '2016-05-26 13:30:17', '2016-05-26 13:30:17', '1'),
(40, 2, 10, 1, 2, 2, '2016-05-27 05:03:41', '2016-05-27 05:03:41', '1'),
(41, 2, 10, 2, 2, 2, '2016-05-27 05:03:41', '2016-05-27 05:03:41', '1'),
(42, 2, 10, 3, 2, 2, '2016-05-27 05:03:41', '2016-05-27 05:03:41', '1'),
(43, 2, 10, 6, 2, 2, '2016-05-27 05:03:41', '2016-05-27 05:03:41', '1'),
(44, 2, 10, 7, 2, 2, '2016-05-27 05:03:41', '2016-05-27 05:03:41', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_mobile_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_mobile_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_primary` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_mobile_maps`
--

INSERT INTO `lie_rest_mobile_maps` (`id`, `rest_detail_id`, `mobile`, `name`, `is_primary`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4, 3, '8547821546', 'Owner', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(5, 4, '9587451254', 'Owner', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(6, 4, '9654875412', 'Account', '0', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(36, 5, '9874562154', 'anil', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(37, 5, '8945871254', 'amit', '0', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(40, 6, '432423', 'erwere', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(41, 7, '', '', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(42, 8, '9874562154', 'anil', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(43, 9, '9874562154', 'anil', '1', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_new_cust_bonus_logs`
--

CREATE TABLE IF NOT EXISTS `lie_rest_new_cust_bonus_logs` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `is_new_cust_bonus` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `type` enum('S','A') NOT NULL DEFAULT 'S' COMMENT 'S for superadmin, A for admin',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_notifications`
--

CREATE TABLE IF NOT EXISTS `lie_rest_notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `send_type` enum('1','2') NOT NULL COMMENT '1 for all,2 for custom resturant',
  `is_type` enum('1','2') NOT NULL COMMENT '1 for notification,2 for sms',
  `is_read` enum('0','1') NOT NULL COMMENT '0 for unread,1 for read',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_notifications`
--

INSERT INTO `lie_rest_notifications` (`id`, `title`, `comment`, `send_type`, `is_type`, `is_read`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(0, 'dasd', 'sadsad', '2', '1', '0', 2, 2, '2016-05-25 07:48:55', '2016-05-25 07:49:12', '2');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_notification_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_notification_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_notification_maps`
--

INSERT INTO `lie_rest_notification_maps` (`id`, `rest_detail_id`, `notification_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(0, 1, 0, 2, 0, '2016-05-25 07:48:55', '2016-05-25 07:48:55', '2'),
(0, 1, 0, 2, 0, '2016-05-25 07:49:03', '2016-05-25 07:49:03', '1'),
(0, 2, 0, 2, 0, '2016-05-25 07:49:03', '2016-05-25 07:49:03', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_owners`
--

CREATE TABLE IF NOT EXISTS `lie_rest_owners` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `contact_no` varchar(12) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(50) NOT NULL DEFAULT '0',
  `validity_type` enum('T','C') NOT NULL COMMENT 'T for table,C for calendar',
  `validity_table` int(11) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_owners`
--

INSERT INTO `lie_rest_owners` (`id`, `rest_detail_id`, `firstname`, `lastname`, `contact_no`, `email`, `password`, `remember_token`, `validity_type`, `validity_table`, `valid_from`, `valid_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'vikas', 'batra', '1234567890', 'batra@gmail.com', '$2y$10$NGgP.6Q7M89btqz.khoVz.cdT2tDqUudv0DwWg81JrNysI.44aVay', 'UXv32LyxwGj6DO3Qn84Abavuxjnhk5mwvqtyVYSxOEpdCO1EBh', 'T', 6, '2016-05-14 11:48:21', '2017-07-18 00:00:00', 0, 9, '0000-00-00 00:00:00', '2016-06-11 13:01:57', '0'),
(2, 3, 'kishan', 'kumar', '1234567890', 'kishankr11@gmail.com', '$2y$10$NGgP.6Q7M89btqz.khoVz.cdT2tDqUudv0DwWg81JrNysI.44aVay', 'DtoaQPjlHI0SyEZY8iFhdODwk0LQbHVfpGpjcxRQR9EmFSPxV8', 'T', 6, '2016-05-25 07:26:47', '2017-07-30 00:00:00', 0, 2, '0000-00-00 00:00:00', '2016-05-25 07:26:47', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_owner_login_logs`
--

CREATE TABLE IF NOT EXISTS `lie_rest_owner_login_logs` (
  `id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `rest_owner_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_rest_owner_login_logs`
--

INSERT INTO `lie_rest_owner_login_logs` (`id`, `rest_id`, `rest_owner_id`, `type`, `ip_address`, `time`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 'L', '127.0.0.1', '2016-05-25 07:06:53', 1, 1, '2016-05-25 07:06:53', '2016-05-25 07:06:53', '1'),
(2, 1, 1, 'O', '127.0.0.1', '2016-05-25 07:42:56', 1, 1, '2016-05-25 07:42:56', '2016-05-25 07:42:56', '1'),
(3, 1, 1, 'L', '127.0.0.1', '2016-05-25 07:43:01', 1, 1, '2016-05-25 07:43:01', '2016-05-25 07:43:01', '1'),
(4, 1, 1, 'L', '127.0.0.1', '2016-05-25 07:44:04', 1, 1, '2016-05-25 07:44:04', '2016-05-25 07:44:04', '1'),
(5, 1, 1, 'L', '127.0.0.1', '2016-05-25 07:49:42', 1, 1, '2016-05-25 07:49:42', '2016-05-25 07:49:42', '1'),
(6, 1, 1, 'O', '127.0.0.1', '2016-05-25 08:01:42', 1, 1, '2016-05-25 08:01:42', '2016-05-25 08:01:42', '1'),
(7, 1, 1, 'L', '127.0.0.1', '2016-05-25 08:01:46', 1, 1, '2016-05-25 08:01:46', '2016-05-25 08:01:46', '1'),
(8, 1, 1, 'L', '127.0.0.1', '2016-06-02 07:23:26', 1, 1, '2016-06-02 07:23:26', '2016-06-02 07:23:26', '1'),
(9, 1, 1, 'L', '127.0.0.1', '2016-06-11 13:01:52', 1, 1, '2016-06-11 13:01:52', '2016-06-11 13:01:52', '1'),
(10, 1, 1, 'O', '127.0.0.1', '2016-06-11 13:01:57', 1, 1, '2016-06-11 13:01:57', '2016-06-11 13:01:57', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_price_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_price_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_price_maps`
--

INSERT INTO `lie_rest_price_maps` (`id`, `rest_detail_id`, `price_type_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(30, 5, 1, 2, 2, '2016-05-31 06:22:25', '2016-05-31 06:22:25', '0'),
(31, 5, 2, 2, 2, '2016-05-31 06:22:25', '2016-05-31 06:22:25', '0'),
(32, 1, 1, 2, 2, '2016-05-31 06:26:25', '2016-05-31 06:26:25', '0'),
(33, 1, 2, 2, 2, '2016-05-31 06:26:25', '2016-05-31 06:26:25', '0'),
(34, 2, 2, 2, 2, '2016-05-31 06:26:38', '2016-05-31 06:26:38', '0'),
(39, 6, 1, 2, 2, '2016-06-16 06:55:05', '2016-06-16 06:55:05', '0'),
(40, 6, 2, 2, 2, '2016-06-16 06:55:05', '2016-06-16 06:55:05', '0'),
(41, 7, 1, 2, 2, '2016-06-16 07:00:10', '2016-06-16 07:00:10', '0'),
(42, 7, 2, 2, 2, '2016-06-16 07:00:10', '2016-06-16 07:00:10', '0'),
(43, 8, 2, 2, 2, '2016-06-16 07:00:53', '2016-06-16 07:00:53', '0'),
(44, 9, 1, 2, 2, '2016-06-16 07:01:50', '2016-06-16 07:01:50', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_privilege`
--

CREATE TABLE IF NOT EXISTS `lie_rest_privilege` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactiive,1=>active,2=>deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_privileges`
--

CREATE TABLE IF NOT EXISTS `lie_rest_privileges` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactiive,1=>active,2=>deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_privileges`
--

INSERT INTO `lie_rest_privileges` (`id`, `name`, `image`, `description`, `valid_from`, `valid_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'normal', '', 'sfdfsdfsdf', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(2, 'featured', '', 'dfgfdgdfg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_privilege_logs`
--

CREATE TABLE IF NOT EXISTS `lie_rest_privilege_logs` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL COMMENT '1=>normal,2=>featured',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_privilege_logs`
--

INSERT INTO `lie_rest_privilege_logs` (`id`, `rest_detail_id`, `privilege_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 5, 0, 2, 2, '2016-05-25 08:10:29', '2016-05-25 08:10:29', '1'),
(2, 5, 1, 2, 2, '2016-05-25 08:18:40', '2016-05-25 08:18:40', '1'),
(3, 6, 0, 2, 2, '2016-05-30 11:42:14', '2016-05-30 11:42:14', '1'),
(4, 7, 0, 2, 2, '2016-06-16 07:00:10', '2016-06-16 07:00:10', '1'),
(5, 8, 0, 2, 2, '2016-06-16 07:00:53', '2016-06-16 07:00:53', '1'),
(6, 9, 0, 2, 2, '2016-06-16 07:01:50', '2016-06-16 07:01:50', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_root_cat_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_root_cat_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `root_cat_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_root_cat_maps`
--

INSERT INTO `lie_rest_root_cat_maps` (`id`, `rest_detail_id`, `root_cat_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(2, 2, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(10, 1, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(11, 3, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(12, 3, 4, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(13, 5, 9, 2, 2, '2016-05-27 05:04:53', '2016-05-27 05:04:53', '1'),
(14, 5, 11, 2, 2, '2016-05-27 05:04:53', '2016-05-27 05:04:53', '1'),
(15, 5, 12, 2, 2, '2016-05-27 05:04:53', '2016-05-27 05:04:53', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_search_filters`
--

CREATE TABLE IF NOT EXISTS `lie_rest_search_filters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `low_value` int(11) NOT NULL,
  `high_value` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_search_filters`
--

INSERT INTO `lie_rest_search_filters` (`id`, `name`, `low_value`, `high_value`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Price', 1, 100, 1, 1, '2016-05-04 00:00:00', '2016-05-04 14:15:08', '1'),
(2, 'Distance', 1, 10, 1, 1, '2016-05-04 00:00:00', '2016-05-04 14:16:03', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_service_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_service_maps` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_service_maps`
--

INSERT INTO `lie_rest_service_maps` (`id`, `service_id`, `rest_detail_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(10, 1, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(11, 2, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(12, 3, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(19, 2, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(20, 3, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(21, 6, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(26, 1, 4, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(27, 2, 4, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(28, 1, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(29, 5, 1, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_settings`
--

CREATE TABLE IF NOT EXISTS `lie_rest_settings` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `is_open` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>closed,1=>open',
  `is_new` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>not new,1=>new',
  `header_color` varchar(100) NOT NULL,
  `body_color` varchar(100) NOT NULL,
  `privilege_id` int(11) NOT NULL COMMENT '1=>normal,2=>featured',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_settings`
--

INSERT INTO `lie_rest_settings` (`id`, `rest_detail_id`, `is_open`, `is_new`, `header_color`, `body_color`, `privilege_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 5, '1', '0', '#ca467b', '#ca467b', 1, 2, 2, '2016-05-25 08:10:29', '2016-05-25 08:18:40', '1'),
(2, 6, '1', '0', '#ffffff', '#ffffff', 0, 2, 2, '2016-05-30 11:42:13', '2016-05-30 11:42:13', '1'),
(3, 7, '1', '0', '#ffffff', '#ffffff', 0, 2, 2, '2016-06-16 07:00:10', '2016-06-16 07:00:10', '1'),
(4, 8, '1', '0', '#ffffff', '#ffffff', 0, 2, 2, '2016-06-16 07:00:53', '2016-06-16 07:00:53', '1'),
(5, 9, '1', '0', '#ffffff', '#ffffff', 0, 2, 2, '2016-06-16 07:01:50', '2016-06-16 07:01:50', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_stamps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_stamps` (
  `id` int(11) NOT NULL,
  `stamp_count` int(11) NOT NULL DEFAULT '0',
  `value` float(16,2) NOT NULL,
  `type` enum('p','c') NOT NULL DEFAULT 'p' COMMENT 'p for percent,c for cash',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_rest_stamps`
--

INSERT INTO `lie_rest_stamps` (`id`, `stamp_count`, `value`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 22, 0.20, 'p', 9, 9, '2016-05-17 06:32:15', '2016-05-17 06:32:15', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_stamp_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_stamp_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `stamp_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_submenu_allergic_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_submenu_allergic_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `allergic_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_submenu_allergic_maps`
--

INSERT INTO `lie_rest_submenu_allergic_maps` (`id`, `rest_detail_id`, `menu_id`, `sub_menu_id`, `allergic_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 5, 6, 0, 7, 2, 2, '2016-05-27 08:00:19', '2016-05-27 08:00:19', '1'),
(2, 2, 0, 7, 7, 2, 2, '2016-05-27 08:20:48', '2016-05-27 08:20:48', '1'),
(4, 5, 11, 9, 7, 2, 2, '2016-05-27 10:03:38', '2016-05-27 10:03:38', '1'),
(6, 1, 5, 8, 7, 2, 2, '2016-05-27 10:58:57', '2016-05-27 10:58:57', '1'),
(7, 1, 1, 1, 7, 2, 2, '2016-05-31 07:04:25', '2016-05-31 07:04:25', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_submenu_spice_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_submenu_spice_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `spice_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_submenu_spice_maps`
--

INSERT INTO `lie_rest_submenu_spice_maps` (`id`, `rest_detail_id`, `menu_id`, `sub_menu_id`, `spice_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 5, 6, 0, 1, 2, 2, '2016-05-27 08:00:19', '2016-05-27 08:00:19', '1'),
(2, 5, 6, 0, 2, 2, 2, '2016-05-27 08:00:19', '2016-05-27 08:00:19', '1'),
(3, 5, 6, 0, 3, 2, 2, '2016-05-27 08:00:19', '2016-05-27 08:00:19', '1'),
(4, 5, 6, 0, 4, 2, 2, '2016-05-27 08:00:19', '2016-05-27 08:00:19', '1'),
(5, 2, 0, 7, 2, 2, 2, '2016-05-27 08:20:49', '2016-05-27 08:20:49', '1'),
(6, 2, 0, 7, 3, 2, 2, '2016-05-27 08:20:49', '2016-05-27 08:20:49', '1'),
(7, 2, 0, 7, 6, 2, 2, '2016-05-27 08:20:49', '2016-05-27 08:20:49', '1'),
(8, 2, 0, 7, 7, 2, 2, '2016-05-27 08:20:49', '2016-05-27 08:20:49', '1'),
(13, 5, 11, 9, 2, 2, 2, '2016-05-27 10:03:38', '2016-05-27 10:03:38', '1'),
(14, 5, 11, 9, 3, 2, 2, '2016-05-27 10:03:38', '2016-05-27 10:03:38', '1'),
(15, 5, 11, 9, 6, 2, 2, '2016-05-27 10:03:38', '2016-05-27 10:03:38', '1'),
(16, 5, 11, 9, 7, 2, 2, '2016-05-27 10:03:39', '2016-05-27 10:03:39', '1'),
(24, 1, 5, 8, 2, 2, 2, '2016-05-27 10:58:57', '2016-05-27 10:58:57', '1'),
(25, 1, 5, 8, 3, 2, 2, '2016-05-27 10:58:57', '2016-05-27 10:58:57', '1'),
(26, 1, 5, 8, 4, 2, 2, '2016-05-27 10:58:57', '2016-05-27 10:58:57', '1'),
(27, 1, 5, 8, 5, 2, 2, '2016-05-27 10:58:57', '2016-05-27 10:58:57', '1'),
(28, 1, 5, 8, 6, 2, 2, '2016-05-27 10:58:57', '2016-05-27 10:58:57', '1'),
(29, 1, 5, 8, 7, 2, 2, '2016-05-27 10:58:57', '2016-05-27 10:58:57', '1'),
(30, 1, 1, 1, 2, 2, 2, '2016-05-31 07:04:26', '2016-05-31 07:04:26', '1'),
(31, 1, 1, 1, 3, 2, 2, '2016-05-31 07:04:26', '2016-05-31 07:04:26', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_sub_menus`
--

CREATE TABLE IF NOT EXISTS `lie_rest_sub_menus` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `short_name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `info` varchar(255) NOT NULL,
  `code` varchar(150) NOT NULL,
  `priority` varchar(150) NOT NULL,
  `image` text NOT NULL,
  `is_alergic` enum('0','1') NOT NULL COMMENT '0=>no alergic,1=>its alergic',
  `is_spicy` enum('0','1') NOT NULL COMMENT '0=>no spicy,1=>its spicy',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_rest_sub_menus`
--

INSERT INTO `lie_rest_sub_menus` (`id`, `rest_detail_id`, `menu_id`, `name`, `short_name`, `description`, `info`, `code`, `priority`, `image`, `is_alergic`, `is_spicy`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 'food first sub menu', 'ffsm', 'This is a food first sub menu', 'This is a food first sub menu', 'ffsm444', '6', '1464678265840.png', '1', '1', 2, 2, '2016-05-31 07:04:25', '2016-05-31 07:04:25', '1'),
(2, 1, 1, 'food second submenu', 'fsm', 'This is a food second submenu', 'This is a food second submenu', 'fsm874', '7', '1464678353915.jpg', '0', '0', 2, 2, '2016-05-31 07:05:53', '2016-05-31 07:05:53', '1'),
(3, 2, 6, 'annapurna first submenu', 'afs', 'This is a annapurna first submenu', 'This is a annapurna first submenu', 'afs256', '9', '1464678438500.jpg', '0', '0', 2, 2, '2016-05-31 07:07:18', '2016-05-31 07:07:18', '1'),
(4, 5, 4, 'jayeka first submenu', 'jfs', 'This is  A jayeka first submenu', 'This is  A jayeka first submenu', 'jfs699', '6', '1464678517993.jpg', '0', '0', 2, 2, '2016-05-31 07:08:37', '2016-05-31 07:08:37', '1'),
(5, 5, 4, 'jayeka second submenu', 'jss', 'This is a jayeka second submenu', 'This is a jayeka second submenu', 'jss211', '5', '1464678573898.jpg', '0', '0', 2, 2, '2016-05-31 07:09:33', '2016-05-31 07:09:33', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_rest_tax_maps`
--

CREATE TABLE IF NOT EXISTS `lie_rest_tax_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `tax_setting_id` int(11) NOT NULL,
  `tax_type` varchar(20) NOT NULL,
  `tax_amount` varchar(15) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_root_categories`
--

CREATE TABLE IF NOT EXISTS `lie_root_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_root_categories`
--

INSERT INTO `lie_root_categories` (`id`, `name`, `description`, `valid_from`, `valid_to`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'vvv', 'vvvvvvvvv', '2016-04-10 00:00:00', '2016-04-15 00:00:00', 2, 0, 0, '0000-00-00 00:00:00', '2016-05-03 11:47:07', '2'),
(3, 'vikas', 'vikas12345', '2016-04-02 00:00:00', '2016-04-06 00:00:00', 3, 0, 0, '2016-04-25 05:14:01', '2016-05-04 09:24:17', '1'),
(5, 'helo', 'dsfsdfsdfsdfd', '2016-04-08 00:00:00', '2016-04-13 00:00:00', 1, 0, 0, '2016-04-27 08:23:21', '2016-05-14 11:51:37', '2'),
(8, 'vikass', 'vikas12345', '2016-04-02 00:00:00', '2016-04-06 00:00:00', 3, 0, 0, '2016-05-10 23:24:17', '2016-05-16 04:53:52', '2'),
(9, 'Recai', 'tgrfds', '2016-05-01 00:00:00', '2016-05-07 00:00:00', 4, 0, 3, '2016-05-10 23:24:48', '2016-05-16 05:49:51', '1'),
(11, 'vvv', 'vvvvvv', '2016-05-05 00:00:00', '2016-05-12 00:00:00', 4, 0, 0, '2016-05-16 04:51:10', '2016-05-16 04:51:10', '1'),
(12, 'vvvv', 'vvvvvv', '2016-05-18 00:00:00', '2016-05-19 00:00:00', 5, 3, 3, '2016-05-16 05:50:21', '2016-05-16 05:50:21', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_service_rest_maps`
--

CREATE TABLE IF NOT EXISTS `lie_service_rest_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `service_setting_id` int(11) NOT NULL,
  `expire_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_service_settings`
--

CREATE TABLE IF NOT EXISTS `lie_service_settings` (
  `id` int(11) NOT NULL,
  `service_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_service_settings`
--

INSERT INTO `lie_service_settings` (`id`, `service_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'vvvvvv', 'vvvvvvv', 0, 3, 2016, 2016, '1'),
(3, 'aaa', 'VvvvvvVvvvvv', 3, 3, 2016, 2016, '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_set_bonus_points`
--

CREATE TABLE IF NOT EXISTS `lie_set_bonus_points` (
  `id` int(11) NOT NULL,
  `activity_title` varchar(50) NOT NULL,
  `points` varchar(5) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_set_bonus_points`
--

INSERT INTO `lie_set_bonus_points` (`id`, `activity_title`, `points`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'test', '60', 'test', 9, 2, '2016-05-10 00:00:00', '2016-05-25 07:32:13', '1'),
(2, 'test1', '90', 'test', 9, 2, '2016-05-10 00:00:00', '2016-05-25 07:32:03', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_set_cashback_points`
--

CREATE TABLE IF NOT EXISTS `lie_set_cashback_points` (
  `id` int(11) NOT NULL,
  `cashback_title` varchar(50) NOT NULL,
  `points` varchar(5) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_set_cashback_points`
--

INSERT INTO `lie_set_cashback_points` (`id`, `cashback_title`, `points`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'test', '55', 'test', 9, 2, '2016-05-09 00:00:00', '2016-05-25 07:31:44', '1'),
(2, 'test1', '70', 'test', 9, 1, '2016-05-09 00:00:00', '2016-05-16 07:09:14', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_shop_categories`
--

CREATE TABLE IF NOT EXISTS `lie_shop_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_shop_categories`
--

INSERT INTO `lie_shop_categories` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'abcd', 'abcdabcd', 3, 3, '2016-05-14 11:47:59', '2016-05-16 07:08:28', '1'),
(3, 'abcds', 'abcdabcd', 3, 3, '2016-05-16 07:08:38', '2016-05-16 07:09:17', '2'),
(4, 'abcds', 'sdsdsd', 3, 3, '2016-05-16 07:09:24', '2016-05-16 07:09:31', '2');

-- --------------------------------------------------------

--
-- Table structure for table `lie_shop_inventories`
--

CREATE TABLE IF NOT EXISTS `lie_shop_inventories` (
  `id` int(11) NOT NULL,
  `shop_category_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `quantity` float(16,2) NOT NULL DEFAULT '0.00',
  `price` float(16,2) NOT NULL DEFAULT '0.00',
  `action` enum('P','M') NOT NULL DEFAULT 'P' COMMENT 'P for Plus & M for Minus',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_shop_inventories`
--

INSERT INTO `lie_shop_inventories` (`id`, `shop_category_id`, `shop_item_id`, `quantity`, `price`, `action`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(5, 1, 1, 20.00, 440.00, 'P', 11, 11, '2016-05-14 11:50:13', '2016-05-14 11:50:13', '1'),
(6, 1, 2, 50.00, 2500.00, 'P', 3, 3, '2016-05-16 07:29:25', '2016-05-16 07:29:25', '1'),
(7, 1, 1, 40.00, 880.00, 'P', 3, 3, '2016-05-16 07:29:25', '2016-05-16 07:29:25', '1'),
(8, 1, 1, 1.00, 333.00, 'M', 1, 1, '2016-05-25 07:11:42', '2016-05-25 07:11:42', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_shop_items`
--

CREATE TABLE IF NOT EXISTS `lie_shop_items` (
  `id` int(11) NOT NULL,
  `shop_category_id` int(11) NOT NULL,
  `metric_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `item_number` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float(16,2) NOT NULL,
  `front_price` float(16,2) NOT NULL,
  `quantity` float(16,2) NOT NULL DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_shop_items`
--

INSERT INTO `lie_shop_items` (`id`, `shop_category_id`, `metric_id`, `name`, `item_number`, `description`, `price`, `front_price`, `quantity`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 4, 'daal', '', 'sdsdsdsd vvvvv', 22.00, 333.00, 0.00, 3, 3, '2016-05-16 07:17:56', '2016-05-25 07:11:42', '1'),
(2, 1, 2, 'cheeni', '', 'vvvvvvvvv', 50.00, 60.00, 3.00, 3, 2, '2016-05-18 06:47:52', '2016-05-18 06:47:52', '1'),
(3, 1, 2, 'dddd', '', 'sdasdas', 22.00, 33.00, 30.00, 3, 3, '2016-05-16 07:26:15', '2016-05-16 07:26:24', '2');

-- --------------------------------------------------------

--
-- Table structure for table `lie_shop_metric_categories`
--

CREATE TABLE IF NOT EXISTS `lie_shop_metric_categories` (
  `id` int(11) NOT NULL,
  `shop_category_id` int(11) NOT NULL,
  `metric_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_shop_metric_categories`
--

INSERT INTO `lie_shop_metric_categories` (`id`, `shop_category_id`, `metric_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(5, 1, 2, 3, 3, '2016-05-16 07:13:40', '2016-05-16 07:13:40', '1'),
(6, 1, 4, 3, 3, '2016-05-16 07:13:40', '2016-05-16 07:13:40', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_shop_orders`
--

CREATE TABLE IF NOT EXISTS `lie_shop_orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `total_price` float(16,2) NOT NULL DEFAULT '0.00',
  `discount` float(16,2) NOT NULL DEFAULT '0.00',
  `grand_total` float(16,2) NOT NULL DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_shop_orders`
--

INSERT INTO `lie_shop_orders` (`id`, `order_id`, `rest_detail_id`, `total_price`, `discount`, `grand_total`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Order-1', 1, 333.00, 0.00, 333.00, 1, 1, '2016-05-25 07:11:42', '2016-05-25 07:11:42', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_shop_rest_cart_maps`
--

CREATE TABLE IF NOT EXISTS `lie_shop_rest_cart_maps` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `quantity` float(16,2) NOT NULL DEFAULT '0.00',
  `price` float(16,2) NOT NULL DEFAULT '0.00',
  `discount` float(16,2) NOT NULL DEFAULT '0.00',
  `grand_total` float(16,2) NOT NULL DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_shop_rest_cart_maps`
--

INSERT INTO `lie_shop_rest_cart_maps` (`id`, `order_id`, `rest_detail_id`, `shop_item_id`, `quantity`, `price`, `discount`, `grand_total`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Order-1', 1, 1, 1.00, 333.00, 0.00, 333.00, 1, 1, '2016-05-25 07:11:42', '2016-05-25 07:11:42', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_shop_rest_cart_temps`
--

CREATE TABLE IF NOT EXISTS `lie_shop_rest_cart_temps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `quantity` float(16,2) NOT NULL DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_shop_rest_cart_temps`
--

INSERT INTO `lie_shop_rest_cart_temps` (`id`, `rest_detail_id`, `shop_item_id`, `quantity`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(3, 1, 2, 1.00, 1, 1, '2016-06-02 08:13:21', '2016-06-02 08:21:48', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_social_networks`
--

CREATE TABLE IF NOT EXISTS `lie_social_networks` (
  `id` int(11) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `google` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `vk` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `pinterest` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_social_networks`
--

INSERT INTO `lie_social_networks` (`id`, `facebook`, `twitter`, `youtube`, `google`, `linkedin`, `vk`, `instagram`, `pinterest`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'facebook', 'twitter', 'youtube', 'google', 'linkedin', 'vk', 'instagram', 'pinterest', 1, 1, '2016-05-03 00:00:00', '2016-05-16 07:14:06', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_social_types`
--

CREATE TABLE IF NOT EXISTS `lie_social_types` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_spices`
--

CREATE TABLE IF NOT EXISTS `lie_spices` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_spices`
--

INSERT INTO `lie_spices` (`id`, `name`, `image`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'testspice', '1463723703900.jpg', 'testspice', 9, 9, '2016-05-03 00:00:00', '2016-05-20 06:18:50', '1'),
(2, 'test2', '1463721999235.jpg', 'test2', 9, 9, '2016-05-20 05:12:46', '2016-05-20 05:26:39', '1'),
(3, 'test3', '1463722013342.jpg', 'test3', 9, 9, '2016-05-20 05:14:55', '2016-05-20 05:26:53', '1'),
(4, 'test4', '1463722020750.jpg', 'test4', 9, 9, '2016-05-20 05:16:06', '2016-05-20 05:27:00', '1'),
(5, 'test5', '1463722027257.jpg', 'test5', 9, 9, '2016-05-20 05:19:23', '2016-05-20 05:27:07', '1'),
(6, 'testspice1', '1463723796330.jpg', 'testspice1', 9, 0, '2016-05-20 05:56:36', '2016-05-20 05:56:36', '1'),
(7, 'testspice3', '1463724444357.jpg', 'testspice3', 9, 9, '2016-05-20 06:07:01', '2016-05-20 06:09:41', '1'),
(8, 'oioi', '1463724600155.jpg', 'oio', 9, 9, '2016-05-20 06:09:51', '2016-05-20 06:10:13', '2');

-- --------------------------------------------------------

--
-- Table structure for table `lie_states`
--

CREATE TABLE IF NOT EXISTS `lie_states` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_states`
--

INSERT INTO `lie_states` (`id`, `country_id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'U.P.', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(2, 2, 'Punjab', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(3, 4, 'Delhi', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(5, 5, 'Kerala\r\n', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(6, 6, 'Maharashtra\r\n', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(7, 7, 'Mizoram\r\n', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(8, 8, 'Haryana\r\n', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(9, 9, 'Jammu and Kashmir\r\n', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(10, 10, 'Rajasthan\r\n', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(11, 11, 'Goa\r\n', '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_sub_menu_price_maps`
--

CREATE TABLE IF NOT EXISTS `lie_sub_menu_price_maps` (
  `id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `price` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_sub_menu_price_maps`
--

INSERT INTO `lie_sub_menu_price_maps` (`id`, `rest_detail_id`, `menu_id`, `sub_menu_id`, `price_type_id`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 1, 1, '522', 2, 2, '2016-05-31 07:04:25', '2016-05-31 07:04:25', '0'),
(2, 1, 1, 1, 2, '655', 2, 2, '2016-05-31 07:04:25', '2016-05-31 07:04:25', '0'),
(3, 1, 1, 2, 1, '900', 2, 2, '2016-05-31 07:05:53', '2016-05-31 07:05:53', '0'),
(4, 1, 1, 2, 2, '850', 2, 2, '2016-05-31 07:05:53', '2016-05-31 07:05:53', '0'),
(5, 2, 6, 3, 2, '622', 2, 2, '2016-05-31 07:07:18', '2016-05-31 07:07:18', '0'),
(6, 5, 4, 4, 1, '955', 2, 2, '2016-05-31 07:08:38', '2016-05-31 07:08:38', '0'),
(7, 5, 4, 4, 2, '366', 2, 2, '2016-05-31 07:08:38', '2016-05-31 07:08:38', '0'),
(8, 5, 4, 5, 1, '122', 2, 2, '2016-05-31 07:09:33', '2016-05-31 07:09:33', '0'),
(9, 5, 4, 5, 2, '200', 2, 2, '2016-05-31 07:09:33', '2016-05-31 07:09:33', '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_superadmin_login_logs`
--

CREATE TABLE IF NOT EXISTS `lie_superadmin_login_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lie_superadmin_login_logs`
--

INSERT INTO `lie_superadmin_login_logs` (`id`, `user_id`, `type`, `ip_address`, `time`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 2, 'L', '127.0.0.1', '2016-05-25 06:43:09', 2, 2, '2016-05-25 06:43:09', '2016-05-25 06:43:09', '1'),
(2, 2, 'L', '127.0.0.1', '2016-05-25 07:03:10', 2, 2, '2016-05-25 07:03:10', '2016-05-25 07:03:10', '1'),
(3, 2, 'L', '127.0.0.1', '2016-05-25 12:23:29', 2, 2, '2016-05-25 12:23:29', '2016-05-25 12:23:29', '1'),
(4, 2, 'O', '127.0.0.1', '2016-05-25 12:28:33', 2, 2, '2016-05-25 12:28:33', '2016-05-25 12:28:33', '1'),
(5, 2, 'L', '127.0.0.1', '2016-05-25 12:28:38', 2, 2, '2016-05-25 12:28:38', '2016-05-25 12:28:38', '1'),
(6, 2, 'O', '127.0.0.1', '2016-05-25 12:29:41', 2, 2, '2016-05-25 12:29:41', '2016-05-25 12:29:41', '1'),
(7, 2, 'L', '127.0.0.1', '2016-05-25 12:29:55', 2, 2, '2016-05-25 12:29:55', '2016-05-25 12:29:55', '1'),
(8, 2, 'L', '127.0.0.1', '2016-05-26 04:55:24', 2, 2, '2016-05-26 04:55:24', '2016-05-26 04:55:24', '1'),
(9, 2, 'L', '127.0.0.1', '2016-05-26 05:02:05', 2, 2, '2016-05-26 05:02:05', '2016-05-26 05:02:05', '1'),
(10, 2, 'L', '127.0.0.1', '2016-05-27 04:47:55', 2, 2, '2016-05-27 04:47:55', '2016-05-27 04:47:55', '1'),
(11, 2, 'L', '127.0.0.1', '2016-05-27 04:54:36', 2, 2, '2016-05-27 04:54:36', '2016-05-27 04:54:36', '1'),
(12, 2, 'L', '127.0.0.1', '2016-05-28 05:17:13', 2, 2, '2016-05-28 05:17:13', '2016-05-28 05:17:13', '1'),
(13, 2, 'L', '127.0.0.1', '2016-05-28 05:36:06', 2, 2, '2016-05-28 05:36:06', '2016-05-28 05:36:06', '1'),
(14, 2, 'O', '127.0.0.1', '2016-05-28 05:38:54', 2, 2, '2016-05-28 05:38:54', '2016-05-28 05:38:54', '1'),
(15, 1, 'L', '127.0.0.1', '2016-05-28 05:39:51', 1, 1, '2016-05-28 05:39:51', '2016-05-28 05:39:51', '1'),
(16, 1, 'O', '127.0.0.1', '2016-05-28 06:25:39', 1, 1, '2016-05-28 06:25:39', '2016-05-28 06:25:39', '1'),
(17, 2, 'O', '127.0.0.1', '2016-05-28 06:27:37', 2, 2, '2016-05-28 06:27:37', '2016-05-28 06:27:37', '1'),
(18, 1, 'O', '127.0.0.1', '2016-05-28 06:37:54', 1, 1, '2016-05-28 06:37:54', '2016-05-28 06:37:54', '1'),
(19, 1, 'L', '127.0.0.1', '2016-05-28 07:58:21', 1, 1, '2016-05-28 07:58:21', '2016-05-28 07:58:21', '1'),
(20, 1, 'L', '127.0.0.1', '2016-05-28 12:11:35', 1, 1, '2016-05-28 12:11:35', '2016-05-28 12:11:35', '1'),
(21, 1, 'O', '127.0.0.1', '2016-05-28 12:26:40', 1, 1, '2016-05-28 12:26:40', '2016-05-28 12:26:40', '1'),
(22, 1, 'L', '127.0.0.1', '2016-05-28 12:26:48', 1, 1, '2016-05-28 12:26:48', '2016-05-28 12:26:48', '1'),
(23, 1, 'O', '127.0.0.1', '2016-05-28 12:27:14', 1, 1, '2016-05-28 12:27:14', '2016-05-28 12:27:14', '1'),
(24, 2, 'L', '127.0.0.1', '2016-05-30 07:09:46', 2, 2, '2016-05-30 07:09:46', '2016-05-30 07:09:46', '1'),
(25, 1, 'L', '127.0.0.1', '2016-05-30 07:41:22', 1, 1, '2016-05-30 07:41:22', '2016-05-30 07:41:22', '1'),
(26, 2, 'L', '127.0.0.1', '2016-05-31 06:21:34', 2, 2, '2016-05-31 06:21:34', '2016-05-31 06:21:34', '1'),
(27, 2, 'L', '127.0.0.1', '2016-06-03 07:59:22', 2, 2, '2016-06-03 07:59:22', '2016-06-03 07:59:22', '1'),
(28, 2, 'L', '127.0.0.1', '2016-06-11 13:01:00', 2, 2, '2016-06-11 13:01:00', '2016-06-11 13:01:00', '1'),
(29, 2, 'O', '127.0.0.1', '2016-06-11 13:01:06', 2, 2, '2016-06-11 13:01:06', '2016-06-11 13:01:06', '1'),
(30, 2, 'L', '127.0.0.1', '2016-06-11 13:06:57', 2, 2, '2016-06-11 13:06:57', '2016-06-11 13:06:57', '1'),
(31, 2, 'L', '127.0.0.1', '2016-06-13 06:40:50', 2, 2, '2016-06-13 06:40:50', '2016-06-13 06:40:50', '1'),
(32, 2, 'L', '127.0.0.1', '2016-06-14 04:37:47', 2, 2, '2016-06-14 04:37:47', '2016-06-14 04:37:47', '1'),
(33, 2, 'L', '127.0.0.1', '2016-06-14 04:47:42', 2, 2, '2016-06-14 04:47:42', '2016-06-14 04:47:42', '1'),
(34, 2, 'O', '127.0.0.1', '2016-06-14 05:25:17', 2, 2, '2016-06-14 05:25:17', '2016-06-14 05:25:17', '1'),
(35, 2, 'O', '127.0.0.1', '2016-06-14 05:25:38', 2, 2, '2016-06-14 05:25:38', '2016-06-14 05:25:38', '1'),
(36, 2, 'L', '127.0.0.1', '2016-06-14 09:40:18', 2, 2, '2016-06-14 09:40:18', '2016-06-14 09:40:18', '1'),
(37, 2, 'L', '127.0.0.1', '2016-06-16 06:45:44', 2, 2, '2016-06-16 06:45:44', '2016-06-16 06:45:44', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_tax_settings`
--

CREATE TABLE IF NOT EXISTS `lie_tax_settings` (
  `id` int(11) NOT NULL,
  `tax_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_tax_settings`
--

INSERT INTO `lie_tax_settings` (`id`, `tax_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'vvvvv', 'vvvvvvv', 0, 0, '2016-05-14 11:56:57', '2016-05-14 11:57:17', '2'),
(2, 'vvvvvvv', 'vvvvvvvvv', 0, 0, '2016-05-14 11:57:11', '2016-05-14 11:57:11', '1'),
(3, 'aaa', 'aaaaaaa', 3, 3, '2016-05-16 06:49:21', '2016-05-16 06:49:21', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_time_validations`
--

CREATE TABLE IF NOT EXISTS `lie_time_validations` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `year` tinyint(11) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `week` tinyint(4) NOT NULL,
  `day` tinyint(4) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_time_validations`
--

INSERT INTO `lie_time_validations` (`id`, `name`, `year`, `month`, `week`, `day`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'dfdfd', 1, 0, 1, 0, 'ouio', '2', 9, 9, '2016-05-06 05:38:16', '2016-05-06 09:55:52'),
(2, 'fgjgj', 1, 0, 0, 0, 'fdgfg', '2', 9, 9, '2016-05-06 05:38:41', '2016-05-06 10:04:24'),
(3, 'hgfhg', 0, 1, 0, 0, 'jhjh', '2', 9, 0, '2016-05-06 06:05:49', '2016-05-09 06:58:42'),
(4, 'gjhhjh', 1, 0, 0, 0, 'jgjh', '2', 9, 0, '2016-05-06 06:06:04', '2016-05-09 06:58:42'),
(5, 'gjhhjhkl', 0, 0, 0, 0, 'jgjh', '2', 9, 0, '2016-05-06 06:06:15', '2016-05-09 06:58:42'),
(6, '1 year 2 month 6 days validity', 1, 2, 0, 6, '1 year 2 month 6 days validity', '1', 9, 2, '2016-05-09 07:06:01', '2016-05-25 07:32:33'),
(7, '1 year 2 weeks', 1, 0, 2, 0, '1 year 2 weeks', '1', 1, 0, '2016-05-16 06:53:07', '2016-05-16 06:53:07');

-- --------------------------------------------------------

--
-- Table structure for table `lie_users`
--

CREATE TABLE IF NOT EXISTS `lie_users` (
  `id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_users`
--

INSERT INTO `lie_users` (`id`, `user_role_id`, `email`, `password`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 2, 'nikhil', '$2y$10$NGgP.6Q7M89btqz.khoVz.cdT2tDqUudv0DwWg81JrNysI.44aVay', 0, 0, 1, '0000-00-00 00:00:00', '2016-05-28 12:27:14', '1'),
(2, 1, 'vikas@gmail.com', '$2y$10$NGgP.6Q7M89btqz.khoVz.cdT2tDqUudv0DwWg81JrNysI.44aVay', 0, 1, 2, '2016-05-16 05:16:11', '2016-06-14 05:25:38', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_user_contact_maps`
--

CREATE TABLE IF NOT EXISTS `lie_user_contact_maps` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_user_contact_maps`
--

INSERT INTO `lie_user_contact_maps` (`id`, `user_id`, `contact`, `created_by`, `updated_by`, `created_at`, `updated_at`, `flag`, `status`) VALUES
(1, 2, '9876345212', 1, 0, '2016-05-16 05:16:11', '2016-05-16 05:16:11', 1, '2'),
(2, 2, '9121211111', 1, 0, '2016-05-16 05:16:11', '2016-05-16 05:16:11', 0, '2'),
(4, 1, '9876543211', 0, 1, '2016-05-16 06:25:55', '2016-05-16 06:25:55', 1, '0'),
(5, 2, '7876222011', 0, 2, '2016-05-16 09:13:44', '2016-05-16 09:13:44', 1, '0'),
(6, 2, '', 0, 2, '2016-05-16 09:13:44', '2016-05-16 09:13:44', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_user_email_maps`
--

CREATE TABLE IF NOT EXISTS `lie_user_email_maps` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_user_email_maps`
--

INSERT INTO `lie_user_email_maps` (`id`, `user_id`, `email`, `created_by`, `updated_by`, `created_at`, `updated_at`, `flag`, `status`) VALUES
(1, 2, 'rekha@gmail.com', 1, 0, '2016-05-16 05:16:11', '2016-05-16 05:16:11', 1, '2'),
(2, 2, 'rekha@gmail.com', 1, 0, '2016-05-16 05:16:11', '2016-05-16 05:16:11', 0, '2'),
(4, 1, 'nikhil.alivenet@gmail.com', 0, 1, '2016-05-16 06:25:55', '2016-05-16 06:25:55', 1, '0'),
(5, 2, 'vikas@gmail.com', 0, 2, '2016-05-16 09:13:44', '2016-05-16 09:13:44', 1, '0'),
(6, 2, '', 0, 2, '2016-05-16 09:13:44', '2016-05-16 09:13:44', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `lie_user_masters`
--

CREATE TABLE IF NOT EXISTS `lie_user_masters` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `doj` date NOT NULL,
  `profile_pic` varchar(250) NOT NULL,
  `add1` varchar(100) NOT NULL,
  `add2` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `zipcode` varchar(6) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_user_masters`
--

INSERT INTO `lie_user_masters` (`id`, `user_id`, `first_name`, `last_name`, `email`, `contact`, `dob`, `doj`, `profile_pic`, `add1`, `add2`, `city`, `state`, `country`, `zipcode`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'Nikhil', 'Nikhil', 'nikhil.alivenet@gmail.com', '9876543211', '1989-03-30', '2016-05-15', '', 'A-512,Gali no 10', 'South Ganesh Nagar', '1', '1', '1', '201301', 0, 1, '0000-00-00 00:00:00', '2016-05-16 06:25:55', '1'),
(2, 2, 'vikas', 'batra', 'vikas@gmail.com', '7876222011', '1989-03-30', '2016-05-16', '1463390024742.jpg', 'A-512,Gali no 10', 'South Ganesh Nagar', '1', '1', '1', '123456', 1, 2, '2016-05-16 05:16:11', '2016-05-16 09:48:14', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_user_roles`
--

CREATE TABLE IF NOT EXISTS `lie_user_roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_user_roles`
--

INSERT INTO `lie_user_roles` (`id`, `role_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', 'Super Admin', 1, 1, '0000-00-00 00:00:00', '2016-05-14 11:47:29', '1'),
(2, 'Sales', 'sales', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(3, 'admin', 'admin', 1, 1, '2016-04-19 12:38:04', '2016-04-19 12:38:04', '1'),
(4, 'Vertrieb Manager', 'Vertrieb CEO', 0, 0, '2016-05-18 22:23:44', '2016-05-18 22:23:44', '1'),
(5, 'Vertrieb', 'Vertrieb Agent', 0, 0, '2016-05-18 22:24:37', '2016-05-18 22:24:37', '1'),
(6, 'CEO', 'Recai Firat', 0, 0, '2016-05-18 22:36:34', '2016-05-18 22:36:34', '1');

-- --------------------------------------------------------

--
-- Table structure for table `lie_user_role_maps`
--

CREATE TABLE IF NOT EXISTS `lie_user_role_maps` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lie_zipcodes`
--

CREATE TABLE IF NOT EXISTS `lie_zipcodes` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lie_zipcodes`
--

INSERT INTO `lie_zipcodes` (`id`, `city_id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, '110059', 'New Delhi.', 1, 0, '2016-04-29 12:46:13', '2016-04-29 13:22:35', '1'),
(2, 1, '0', 'fgnffnfghnfgngf', 0, 0, '2016-04-29 12:53:24', '2016-04-29 12:56:46', '2'),
(3, 11, '1010', 'Innere Stadt', 0, 0, '2016-05-18 22:34:20', '2016-05-18 22:34:20', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lie_access_lists`
--
ALTER TABLE `lie_access_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_access_maps`
--
ALTER TABLE `lie_access_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_access_list_id` (`access_list_id`),
  ADD KEY `lie_page_list_id` (`page_list_id`),
  ADD KEY `lie_user_id` (`user_id`);

--
-- Indexes for table `lie_alergic_contents`
--
ALTER TABLE `lie_alergic_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_cities`
--
ALTER TABLE `lie_cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_color_settings`
--
ALTER TABLE `lie_color_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_countries`
--
ALTER TABLE `lie_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_currencies`
--
ALTER TABLE `lie_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_email_templates`
--
ALTER TABLE `lie_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_email_types`
--
ALTER TABLE `lie_email_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_faq_categories`
--
ALTER TABLE `lie_faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_faq_questions`
--
ALTER TABLE `lie_faq_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_city_maps`
--
ALTER TABLE `lie_front_city_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_color_maps`
--
ALTER TABLE `lie_front_color_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_rest_detail_id` (`rest_detail_id`);

--
-- Indexes for table `lie_front_country_maps`
--
ALTER TABLE `lie_front_country_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_kitchen_maps`
--
ALTER TABLE `lie_front_kitchen_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_pages`
--
ALTER TABLE `lie_front_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_page_maps`
--
ALTER TABLE `lie_front_page_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_services`
--
ALTER TABLE `lie_front_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_state_maps`
--
ALTER TABLE `lie_front_state_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_users`
--
ALTER TABLE `lie_front_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_user_addresses`
--
ALTER TABLE `lie_front_user_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_user_details`
--
ALTER TABLE `lie_front_user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_user_email_verifications`
--
ALTER TABLE `lie_front_user_email_verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_user_login_logs`
--
ALTER TABLE `lie_front_user_login_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_user_mobile_verifications`
--
ALTER TABLE `lie_front_user_mobile_verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_front_user_social_maps`
--
ALTER TABLE `lie_front_user_social_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_kitchens`
--
ALTER TABLE `lie_kitchens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_marketing_tips`
--
ALTER TABLE `lie_marketing_tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_menu_categories`
--
ALTER TABLE `lie_menu_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_menu_extras`
--
ALTER TABLE `lie_menu_extras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_menu_extra_types`
--
ALTER TABLE `lie_menu_extra_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_menu_price_maps`
--
ALTER TABLE `lie_menu_price_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_metrics`
--
ALTER TABLE `lie_metrics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_page_lists`
--
ALTER TABLE `lie_page_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_price_types`
--
ALTER TABLE `lie_price_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_promocodes`
--
ALTER TABLE `lie_promocodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_promo_rest_maps`
--
ALTER TABLE `lie_promo_rest_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_promo_user_maps`
--
ALTER TABLE `lie_promo_user_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_admin_contact_us`
--
ALTER TABLE `lie_rest_admin_contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_categories`
--
ALTER TABLE `lie_rest_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_cust_bonus`
--
ALTER TABLE `lie_rest_cust_bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_cust_bonus_maps`
--
ALTER TABLE `lie_rest_cust_bonus_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_days`
--
ALTER TABLE `lie_rest_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_day_time_maps`
--
ALTER TABLE `lie_rest_day_time_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_rest_detail_id` (`rest_detail_id`);

--
-- Indexes for table `lie_rest_details`
--
ALTER TABLE `lie_rest_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_email_maps`
--
ALTER TABLE `lie_rest_email_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_rest_detail_id` (`rest_detail_id`);

--
-- Indexes for table `lie_rest_extra_maps`
--
ALTER TABLE `lie_rest_extra_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_extra_type_maps`
--
ALTER TABLE `lie_rest_extra_type_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_kitchen_maps`
--
ALTER TABLE `lie_rest_kitchen_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_landline_maps`
--
ALTER TABLE `lie_rest_landline_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_rest_detail_id` (`rest_detail_id`);

--
-- Indexes for table `lie_rest_menus`
--
ALTER TABLE `lie_rest_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_menu_allergic_maps`
--
ALTER TABLE `lie_rest_menu_allergic_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_menu_spice_maps`
--
ALTER TABLE `lie_rest_menu_spice_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_mobile_maps`
--
ALTER TABLE `lie_rest_mobile_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_rest_detail_id` (`rest_detail_id`);

--
-- Indexes for table `lie_rest_new_cust_bonus_logs`
--
ALTER TABLE `lie_rest_new_cust_bonus_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_owners`
--
ALTER TABLE `lie_rest_owners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_owner_login_logs`
--
ALTER TABLE `lie_rest_owner_login_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_price_maps`
--
ALTER TABLE `lie_rest_price_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_privilege`
--
ALTER TABLE `lie_rest_privilege`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_privilege_logs`
--
ALTER TABLE `lie_rest_privilege_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_root_cat_maps`
--
ALTER TABLE `lie_rest_root_cat_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_search_filters`
--
ALTER TABLE `lie_rest_search_filters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_service_maps`
--
ALTER TABLE `lie_rest_service_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_settings`
--
ALTER TABLE `lie_rest_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_stamps`
--
ALTER TABLE `lie_rest_stamps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_stamp_maps`
--
ALTER TABLE `lie_rest_stamp_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_submenu_allergic_maps`
--
ALTER TABLE `lie_rest_submenu_allergic_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_submenu_spice_maps`
--
ALTER TABLE `lie_rest_submenu_spice_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_sub_menus`
--
ALTER TABLE `lie_rest_sub_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_rest_tax_maps`
--
ALTER TABLE `lie_rest_tax_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_rest_detail_id` (`rest_detail_id`),
  ADD KEY `lie_tax_setting_id` (`tax_setting_id`);

--
-- Indexes for table `lie_root_categories`
--
ALTER TABLE `lie_root_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_service_rest_maps`
--
ALTER TABLE `lie_service_rest_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_rest_detail_id` (`rest_detail_id`),
  ADD KEY `lie_service_setting_id` (`service_setting_id`);

--
-- Indexes for table `lie_service_settings`
--
ALTER TABLE `lie_service_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_set_bonus_points`
--
ALTER TABLE `lie_set_bonus_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_shop_categories`
--
ALTER TABLE `lie_shop_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_shop_inventories`
--
ALTER TABLE `lie_shop_inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_shop_items`
--
ALTER TABLE `lie_shop_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_shop_metric_categories`
--
ALTER TABLE `lie_shop_metric_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_shop_orders`
--
ALTER TABLE `lie_shop_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_shop_rest_cart_maps`
--
ALTER TABLE `lie_shop_rest_cart_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_shop_rest_cart_temps`
--
ALTER TABLE `lie_shop_rest_cart_temps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_social_networks`
--
ALTER TABLE `lie_social_networks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_social_types`
--
ALTER TABLE `lie_social_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_spices`
--
ALTER TABLE `lie_spices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_states`
--
ALTER TABLE `lie_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_sub_menu_price_maps`
--
ALTER TABLE `lie_sub_menu_price_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_superadmin_login_logs`
--
ALTER TABLE `lie_superadmin_login_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_tax_settings`
--
ALTER TABLE `lie_tax_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_time_validations`
--
ALTER TABLE `lie_time_validations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_users`
--
ALTER TABLE `lie_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_user_role_id` (`user_role_id`) USING BTREE;

--
-- Indexes for table `lie_user_contact_maps`
--
ALTER TABLE `lie_user_contact_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_user_id` (`user_id`);

--
-- Indexes for table `lie_user_email_maps`
--
ALTER TABLE `lie_user_email_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_user_id` (`user_id`);

--
-- Indexes for table `lie_user_masters`
--
ALTER TABLE `lie_user_masters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_user_id` (`user_id`);

--
-- Indexes for table `lie_user_roles`
--
ALTER TABLE `lie_user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lie_user_role_maps`
--
ALTER TABLE `lie_user_role_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lie_user_id` (`user_id`),
  ADD KEY `lie_user_role_id` (`user_role_id`);

--
-- Indexes for table `lie_zipcodes`
--
ALTER TABLE `lie_zipcodes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lie_access_lists`
--
ALTER TABLE `lie_access_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `lie_access_maps`
--
ALTER TABLE `lie_access_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `lie_alergic_contents`
--
ALTER TABLE `lie_alergic_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `lie_cities`
--
ALTER TABLE `lie_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `lie_color_settings`
--
ALTER TABLE `lie_color_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lie_currencies`
--
ALTER TABLE `lie_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `lie_email_templates`
--
ALTER TABLE `lie_email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_email_types`
--
ALTER TABLE `lie_email_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lie_faq_categories`
--
ALTER TABLE `lie_faq_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lie_faq_questions`
--
ALTER TABLE `lie_faq_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lie_front_city_maps`
--
ALTER TABLE `lie_front_city_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `lie_front_color_maps`
--
ALTER TABLE `lie_front_color_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_front_country_maps`
--
ALTER TABLE `lie_front_country_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `lie_front_kitchen_maps`
--
ALTER TABLE `lie_front_kitchen_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `lie_front_pages`
--
ALTER TABLE `lie_front_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lie_front_page_maps`
--
ALTER TABLE `lie_front_page_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lie_front_services`
--
ALTER TABLE `lie_front_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_front_state_maps`
--
ALTER TABLE `lie_front_state_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `lie_front_users`
--
ALTER TABLE `lie_front_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `lie_front_user_addresses`
--
ALTER TABLE `lie_front_user_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_front_user_details`
--
ALTER TABLE `lie_front_user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `lie_front_user_email_verifications`
--
ALTER TABLE `lie_front_user_email_verifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lie_front_user_login_logs`
--
ALTER TABLE `lie_front_user_login_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lie_front_user_mobile_verifications`
--
ALTER TABLE `lie_front_user_mobile_verifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_front_user_social_maps`
--
ALTER TABLE `lie_front_user_social_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_kitchens`
--
ALTER TABLE `lie_kitchens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lie_marketing_tips`
--
ALTER TABLE `lie_marketing_tips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lie_menu_categories`
--
ALTER TABLE `lie_menu_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `lie_menu_extras`
--
ALTER TABLE `lie_menu_extras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lie_menu_extra_types`
--
ALTER TABLE `lie_menu_extra_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `lie_menu_price_maps`
--
ALTER TABLE `lie_menu_price_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lie_metrics`
--
ALTER TABLE `lie_metrics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_page_lists`
--
ALTER TABLE `lie_page_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_price_types`
--
ALTER TABLE `lie_price_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lie_promocodes`
--
ALTER TABLE `lie_promocodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `lie_promo_rest_maps`
--
ALTER TABLE `lie_promo_rest_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `lie_promo_user_maps`
--
ALTER TABLE `lie_promo_user_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `lie_rest_admin_contact_us`
--
ALTER TABLE `lie_rest_admin_contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_rest_categories`
--
ALTER TABLE `lie_rest_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lie_rest_cust_bonus`
--
ALTER TABLE `lie_rest_cust_bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lie_rest_cust_bonus_maps`
--
ALTER TABLE `lie_rest_cust_bonus_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_rest_days`
--
ALTER TABLE `lie_rest_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lie_rest_day_time_maps`
--
ALTER TABLE `lie_rest_day_time_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `lie_rest_details`
--
ALTER TABLE `lie_rest_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `lie_rest_email_maps`
--
ALTER TABLE `lie_rest_email_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `lie_rest_extra_maps`
--
ALTER TABLE `lie_rest_extra_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_rest_extra_type_maps`
--
ALTER TABLE `lie_rest_extra_type_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `lie_rest_kitchen_maps`
--
ALTER TABLE `lie_rest_kitchen_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `lie_rest_landline_maps`
--
ALTER TABLE `lie_rest_landline_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `lie_rest_menus`
--
ALTER TABLE `lie_rest_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_rest_menu_allergic_maps`
--
ALTER TABLE `lie_rest_menu_allergic_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `lie_rest_menu_spice_maps`
--
ALTER TABLE `lie_rest_menu_spice_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `lie_rest_mobile_maps`
--
ALTER TABLE `lie_rest_mobile_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `lie_rest_new_cust_bonus_logs`
--
ALTER TABLE `lie_rest_new_cust_bonus_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_rest_owners`
--
ALTER TABLE `lie_rest_owners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lie_rest_owner_login_logs`
--
ALTER TABLE `lie_rest_owner_login_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `lie_rest_price_maps`
--
ALTER TABLE `lie_rest_price_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `lie_rest_privilege`
--
ALTER TABLE `lie_rest_privilege`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_rest_privilege_logs`
--
ALTER TABLE `lie_rest_privilege_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_rest_root_cat_maps`
--
ALTER TABLE `lie_rest_root_cat_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `lie_rest_search_filters`
--
ALTER TABLE `lie_rest_search_filters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lie_rest_service_maps`
--
ALTER TABLE `lie_rest_service_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `lie_rest_settings`
--
ALTER TABLE `lie_rest_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lie_rest_stamps`
--
ALTER TABLE `lie_rest_stamps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lie_rest_stamp_maps`
--
ALTER TABLE `lie_rest_stamp_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_rest_submenu_allergic_maps`
--
ALTER TABLE `lie_rest_submenu_allergic_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lie_rest_submenu_spice_maps`
--
ALTER TABLE `lie_rest_submenu_spice_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `lie_rest_sub_menus`
--
ALTER TABLE `lie_rest_sub_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lie_rest_tax_maps`
--
ALTER TABLE `lie_rest_tax_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_root_categories`
--
ALTER TABLE `lie_root_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `lie_service_rest_maps`
--
ALTER TABLE `lie_service_rest_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_service_settings`
--
ALTER TABLE `lie_service_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lie_set_bonus_points`
--
ALTER TABLE `lie_set_bonus_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lie_shop_categories`
--
ALTER TABLE `lie_shop_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `lie_shop_inventories`
--
ALTER TABLE `lie_shop_inventories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `lie_shop_items`
--
ALTER TABLE `lie_shop_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lie_shop_metric_categories`
--
ALTER TABLE `lie_shop_metric_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_shop_orders`
--
ALTER TABLE `lie_shop_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lie_shop_rest_cart_maps`
--
ALTER TABLE `lie_shop_rest_cart_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lie_shop_rest_cart_temps`
--
ALTER TABLE `lie_shop_rest_cart_temps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lie_social_networks`
--
ALTER TABLE `lie_social_networks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lie_social_types`
--
ALTER TABLE `lie_social_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_spices`
--
ALTER TABLE `lie_spices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `lie_states`
--
ALTER TABLE `lie_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `lie_sub_menu_price_maps`
--
ALTER TABLE `lie_sub_menu_price_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `lie_superadmin_login_logs`
--
ALTER TABLE `lie_superadmin_login_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `lie_tax_settings`
--
ALTER TABLE `lie_tax_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lie_time_validations`
--
ALTER TABLE `lie_time_validations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lie_users`
--
ALTER TABLE `lie_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lie_user_contact_maps`
--
ALTER TABLE `lie_user_contact_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_user_email_maps`
--
ALTER TABLE `lie_user_email_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_user_masters`
--
ALTER TABLE `lie_user_masters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lie_user_roles`
--
ALTER TABLE `lie_user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `lie_user_role_maps`
--
ALTER TABLE `lie_user_role_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lie_zipcodes`
--
ALTER TABLE `lie_zipcodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
