<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    
    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],
    
    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],
    
    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    //Socialite
    'facebook' => [
        'client_id'     => '142207662866810',
        'client_secret' => '8e0d173a3bffcfb91262b7151dbe7b7c',
        'redirect'      => 'http://localhost:8000/auth/facebook/callback',
    ],
    'twitter' => [
        'client_id' =>  'HecKFqJfHWBwCeLx31PXCkh5X',
        'client_secret' => 'kEGKyYOWCfMlEFq9GnOEkvcQi87Lte8gtR8gq37SRksLBzBNhZ',
        'redirect' => 'http://localhost:8000/auth/twitter/callback',
    ],
    'google' => [
        'client_id' =>  '550509315643-b7u8aqta0prprnljc3avinlaim4hrult.apps.googleusercontent.com',
        'client_secret' => 'M8J7Air6llw3QmPT5D82rjlP',
        'redirect' => 'http://localhost:8000/auth/google/callback',
    ]
    
];
