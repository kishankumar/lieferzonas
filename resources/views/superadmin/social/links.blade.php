@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Social Networking
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.sociallinks.index')}}"><i class="fa fa-dashboard"></i> Social Networking Management</a></li>
            <li class="active">Social Network Links</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Social Networking</h3>
                    </div>
                    <!-- /.box-header -->

                    {{--<div class="col-sm-12 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <a class="btn btn-default" data-toggle="modal" data-target="#add-form"
                               data-original-title><span>Add</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable"><span>Download</span></a>
                            <a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2"
                               tabindex="0" aria-controls="simpledatatable" onclick="delete_country()"><span>Delete</span></a></div>
                    </div>--}}

                    <div class="box-body">
                    <div class="row">
						
                    <div class="col-md-6">


                            {!! Form::open(['route'=>'root.sociallinks.store','method'=>'post']) !!}

                                    <div class="form-group">
                                        <label class="control-label">Facebook</label><span class="red">*</span>
                                        {!! Form::text('facebook',$social->facebook,['class'=>'form-control','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Twitter</label><span class="red">*</span>
                                        {!! Form::text('twitter',$social->twitter,['class'=>'form-control','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Youtube</label><span class="red">*</span>
                                        {!! Form::text('youtube',$social->youtube,['class'=>'form-control','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Google +</label><span class="red">*</span>
                                        {!! Form::text('google',$social->google,['class'=>'form-control','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Linkedin</label><span class="red">*</span>
                                        {!! Form::text('linkedin',$social->linkedin,['class'=>'form-control','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">VK</label><span class="red">*</span>
                                        {!! Form::text('vk',$social->vk,['class'=>'form-control','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Instagram</label><span class="red">*</span>
                                        {!! Form::text('instagram',$social->instagram,['class'=>'form-control','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Pinterest</label><span class="red">*</span>
                                        {!! Form::text('pinterest',$social->pinterest,['class'=>'form-control','required']) !!}
                                    </div>

                            {!! Form::submit('Save Changes',['id'=>'submit','class'=>'btn  btn-primary']) !!}
							<hr>
							<p><span class="red">*</span> - Required Fields.</p>
                            {!! Form::close() !!}
                    </div>
						
                    </div>
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@endsection

@section('script')

    {{--    {!! Html::script('resources/assets/js/jquery.validate.min.js') !!}
        {!! Html::script('resources/assets/js/additional-methods.js') !!}

        <script type="text/javascript">
        </script> --}}
@endsection