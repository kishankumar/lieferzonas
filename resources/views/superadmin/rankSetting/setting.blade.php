@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Search Rank Setting
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.rank.setting.create')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
        <li class="active">Rank Setting</li>
      </ol>
    </section>

        <!-- Main content -->
        <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
            <div class="row">
        <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
        
                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Search Rank Setting With Full Features</h3>
                    </div>
                <!-- /.box-header -->
                    
                    {{--    Error Display--}}
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul class="list-unstyled">
                                @foreach($errors->all() as $error)
                                    <li> {{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{--    Error Display ends--}}
                    
                    <div class="panel panel-primary theme-border">
                        <div class="panel-heading theme-bg">
                            <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Rank Setting</h4>
                        </div>
                        
                        <div class="modal-body" style="padding: 5px;">
                            <div class="panel-body">
                                
                                @if(Session::has('message'))
                                    <div class="alert alert-success" style="padding: 7px 15px;">
                                        {{ Session::get('message') }}
                                        {{ Session::put('message','') }}
                                    </div>
                                @endif
                                
                                {!! Form::open(array('route'=>'root.rank.setting.store','id'=>'settingForm','class'=>'form-horizontal')) !!}
                                
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <tr>
                                            <td>Rank</td>
                                            <td>1</td>
                                            <td>2</td>
                                            <td>3</td>
                                            <td>4</td>
                                            <td>5</td>
                                            <td>6</td>
                                            <td>7</td>
                                            <td>8</td>
                                        </tr>
                                        <tr>
                                            <td>Price</td>
                                            <?php
                                            for($i=1; $i<=8; $i++){
                                                $priceVal='';
                                                if(count($sel_prices)){
                                                    foreach($sel_prices as $priceInfo){
                                                        if($priceInfo->rank==$i){
                                                            $priceVal = $priceInfo->price;
                                                        }
                                                    }
                                                }
                                                ?>
                                                <td>
                                                    <label>
                                                    <!--{!! Form::text('title', $priceVal ? $priceVal :'',['class'=>'form-control']) !!}-->
                                                    <input type="text" class="rankPrice form-control" value="{{ $priceVal or ''}}" name="rank[<?=$i;?>]">
                                                    <span></span>
                                                    </label>
                                                </td>
                                        <?php } ?>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-md-12 form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" type="submit" onclick="return frmvalidation()">Update</button>
                                        <a class="btn btn-primary" href="{{url('root/rank/setting/create')}}">Cancel</a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>  
                </div>
                <!-- /.box-body -->
            </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
<script>
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    $.validator.addMethod("check_value", function(value, element) {
        return this.optional(element) || /^[0-9]+([.][0-9]{1,2})?$/.test(value);
    }, "Price not valid.");
    $.validator.addMethod('greater_than_zero', function(value, element, param) {
        return isNaN(value) || this.optional(element) || value > 0;
    }, 'Price must be greater than 0');
    
    function frmvalidation(){
        var count=0;
        $(".rankPrice").each(function() {
            var quanV=/^[0-9]+([.][0-9]{1,2})?$/;
            var validQuan=quanV.test($(this).val());
            if($(this).val()==''){
                $(this).next().html("Please enter price").css('color','red');
                count=1;
            }
            else if($(this).val()< 0 || validQuan==false)
            {
                $(this).next().html("Price not valid").css('color','red');
                count=1;
            }
            else
            {
                $(this).next().html("").css('color','');
            }
        });
        if(count==1){
            return false;
        }else{
            return true;
        }
        ('#settingForm').submit();
    }

</script>
@endsection
