@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Search Rank Setting
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.rank.setting.create')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
        <li class="active">Rank Setting</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
        <!-- Custom tabs (Charts with tabs)-->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Search Rank Setting With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-12 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                </div>
            </div>
            
            <div class="box-body">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    {{ Session::put('message','') }}
                @endif
                
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Rank Assign to</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> 1 </td>
                            <td>{{ 'Zip Codes' }} </td>
                            <td>
                                <i class="fa fa-check text-success fa-2xkc text-success"></i>
                            </td>
                            <td>
                                <a class="btn btn-success" href="{{url('root/rank/setting/1/edit')}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                            
                    </tbody>
                </table>
            </div>
        </div>
        <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
    
@endsection

@section('script')
<script>

</script>

@endsection


