@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
       <section class="content-header">
        <h1>
            Restaurant Complaint
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
			 <li><a href="{{route('root.rest.complaint.index')}}"><i class="fa fa-dashboard"></i>Restauranr Complaints</a></li>
            <li class="active">Complaint List</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">Restaurant Complaint With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/rest/complaint/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/rest/complaint/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;"> 
                <div class="btn-group">
<!--                    <a class="btn btn-default" data-toggle="modal" href="{{ url('root/home/pages/create') }}" >
                        <span>Add</span>
                    </a>-->
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <!--<a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="download_pages()"><span>Download</span></a>-->
                     
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_users()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
            </div>
            
             <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            <div class="clearfix" style=""></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                            <div class="alert alert-success" style="padding: 7px 15px;">
                                {{ Session::get('message') }}
                                {{ Session::put('message','') }}
                            </div>
                        @endif
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>
                            <a href="{{url('root/rest/complaint/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='topic' ? 'topic-desc' :'topic';?>">
                            Topic
                            <?php if($type=='topic'){ echo $class; } elseif($type=='topic-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/rest/complaint/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                            Description
                            <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/rest/complaint/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Name
                            <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/rest/complaint/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='email' ? 'email-desc' :'email';?>">
                            Email Id
                            <?php if($type=='email'){ echo $class; } elseif($type=='email-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/rest/complaint/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='phone' ? 'phone-desc' :'phone';?>">
                            Phone
                            <?php if($type=='phone'){ echo $class; } elseif($type=='phone-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/rest/complaint/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='created' ? 'created-desc' :'created';?>">
                            Created At
                            <?php if($type=='created'){ echo $class; } elseif($type=='created-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Status</th>
                    </tr>
                </thead>
                
                <tbody>
                @if(count($complaintInfo))
                    @foreach($complaintInfo as $info)
                        <tr>
                            <td>
                                <div class="checkbox icheck">
                                    <label>
                                        {!! Form::checkbox('pages', $info->id, null, ['class' => 'pages checkBoxClass']) !!}
                                        <span class="button-checkbox"></span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <a href="{{url('root/rest/complaint/'.$info->id)}}">
                                    <?php
                                    if($info->check_status=='U') { ?>
                                        <strong>{{ ucfirst($info->topic)  }}</strong>
                                    <?php }else{ ?>
                                        {{ ucfirst($info->topic)  }}
                                    <?php } ?>
                                </a>
                            </td>
                            <td>{{ (strlen($info->description) > 25) ? strip_tags(substr($info->description,0,25)).'...' : strip_tags($info->description)  }}</td>
                            <td>{{ ucfirst($info->name)  }}</td>
                            <td>{{ ($info->email)  }}</td>
                            <td>{{ ($info->phone)  }}</td>
                            <td>{{date('d M Y',strtotime($info->created_at))}}</td>
                            <td>
                                @if($info->status == 1) 
                                    <i class="fa fa-check text-success fa-2x text-success"></i>
                                @else
                                    <i class="fa fa-ban text-danger fa-2x text-danger" ></i>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $complaintInfo->appends(Request::except('page'))->render() !!}
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')
    <script>
        function delete_users(){
            var len = $(".pages:checked").length;
            if(len==0){
                alert('Please select atleast one complaint');
                return false;
            }
            var base_host = window.location.origin;  //give http://localhost
            var r = confirm("Are you sure to delete complaints?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="pages"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: '{{url('root/rest/complaint/delete')}}',
                    type: "post",
                    dataType:"json",
                    data: {'selectedPages':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                        if(res.success==1)
                        {
                            location.reload();
                        }
                        else
                        {
                            alert('complaint not deleted');
                        }
                    }
                });
            } else {
                return false;
            }
        }
    </script>
    
    {!! Html::script('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}
    {!! Html::script('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') !!}
    <script>
        $('#contents').ckeditor();
    </script>
@endsection


