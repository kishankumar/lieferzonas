@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <section class="content-header">
        <h1>
            Restaurant Complaint
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
			 <li><a href="{{route('root.rest.complaint.index')}}"><i class="fa fa-dashboard"></i>Restauranr Complaints</a></li>
            <li class="active">Complaint Detail</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-tags"></i> Complaint Detail</h3>
                    <span class="pull-right">
                        <a class="btn btn-primary" href="{{url('root/rest/complaint/')}}">Back</a>
                    </span>
                </div>
                <!-- /.box-header -->
                
        @if(count($viewDetails))
            @foreach($viewDetails as $info)
                    
                <div class="box-body">
                    <div class="row form-horizontal">
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Restaurant :</label></div>
                            <div class="col-md-8">{{ ucfirst($info->rest_name->f_name).' '.ucfirst($info->rest_name->l_name)  }}</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Order Id :</label></div>
                            <div class="col-md-8">{{$info->order_id}}</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Topic :</label></div>
                            <div class="col-md-8">{{ ucfirst($info->topic)  }}</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Name :</label></div>
                            <div class="col-md-8">{{$info->name}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Email Id  :</label></div>
                            <div class="col-md-8">{{$info->email}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Phone  :</label></div>
                            <div class="col-md-8">{{$info->phone}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Created  :</label></div>
                            <div class="col-md-8">{{date('d M Y',strtotime($info->created_at))}} </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Status  :</label></div>
                            <div class="col-md-8">
                                <?php
                                    if($info->check_status=='U') { ?>
                                        <strong>{{ 'Unread'  }}</strong>
                                <?php }else{ ?>
                                        {{ 'Read'  }}
                                <?php } ?>
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Description  :</label></div>
                            <div class="col-md-8">{{$info->description}}	</div>
                        </div>
                        
                    </div>
                </div>
            @endforeach
        @endif
            </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
    </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

	
	<!--added by Rajlakshmi(02-06-16)-->
	<div id="download" style="display:none;" >
	</div>
   <!--added by Rajlakshmi(02-06-16)-->


    @endsection

@section('script')

@endsection





