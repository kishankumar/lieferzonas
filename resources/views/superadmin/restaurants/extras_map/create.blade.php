@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Extras
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.extramap.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Add Extras</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->

            @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif

            
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add New Extras </h4>
                    </div>
                    
                     <div class="modal-body" style="padding: 5px;">
                     <div class="panel-body">
						 {!! Form::open(array('route'=>'root.extramap.create','id'=>'searchingForm','class'=>'form-horizontal','method'=>'GET')) !!}
						 {{--<form class="form-horizontal">--}}
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label">Restaurant Name<span class="red">*</span></label>
								 <div class="col-sm-4">
                 @if($restro_detail_id)
									 <select class="form-control" disabled name="restaurant">
                   @else
                   <select class="form-control js-example-basic-multiple" name="restaurant" id="restaurant" onChange="hideErrorMsg('restauranterr')">
                   @endif
                   <option value="">Select</option>

                   @foreach($restro_names as $restro_name)

                   @if($restro_name->id==$restro_detail_id)
                   <option value="{{$restro_name->id}}" selected >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @else
                   <option value="{{$restro_name->id}}" >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @endif
                   @endforeach
                   </select>
                   <span id="restauranterr" class="text-danger"></span>

								 </div>
                 @if(!$restro_detail_id)
								 <div class="col-sm-4">
									 <button class="btn btn-primary" type="button" onclick="validate_rest()">Get Extra Types</button>
								 </div>
                 @endif
							 </div>
               <div class="clearfix"></div>
               {!! Form::close() !!}
							 
               @if($restro_detail_id)
               <div>
                {!! Form::open(array('route'=>'root.extramap.store','class'=>'form-horizontal', 'id'=> 'serviceform')) !!}
							 <hr>
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label">&nbsp;</label>
								 <div class="col-sm-9">

                 <?php $i=1; ?>
                 @if(count($extraTypeMaps)>0)
                   @foreach($extraTypeMaps as $extraTypeMap)

                   <li>{{$extraTypeMap->name}}
                   <input type="hidden" name="extra_type<?php echo $i; ?>" value="{{$extraTypeMap->id}}">

                   <ul>
                  
                   @foreach(App\superadmin\MenuExtra::extraType($extraTypeMap->id) as $data)
                     
                     @if(in_array($data['id'],$extrasMaps))
                     <input type="checkbox" class="extraTypes" name="extras<?php echo $i; ?>[]" value="{{$data['id']}}" checked>{{$data['name']}}
                     @else
                    <input type="checkbox" class="extraTypes" name="extras<?php echo $i; ?>[]" value="{{$data['id']}}" >{{$data['name']}}
                     @endif
                     
                      
                      
                      @endforeach
                   </ul>
                   </li>

									<?php $i++ ?>
                   @endforeach
                   @else
                         <span>No Record Found</span>
                     @endif

                   {!! Form :: hidden('restaurant',$restro_detail_id,['id'=>'restro-detail-id'])  !!}

                   {!! Form :: hidden('total_value',$i,['id'=>''])  !!}

								 </div>
							 </div>
                                                         <span id="serviceerr" class="text-danger" ></span>
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label"></label>
								 <div class="col-sm-4">
								 @if(count($extraTypeMaps)>0)
                                    <button class="btn btn-primary" type="button"  onclick="validate_service()">Submit</button>
                                    @endif
                                     <a class="btn btn-primary" href="{{url('root/extramap/create/')}}">Cancel</a>
								 </div>
							 </div>
						  {!! Form::close() !!}
              </div>
              @endif
							<!-- </form> -->
						</div>
                     
                     </div>  
                    </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
<script>
    function hideErrorMsg(id){
        $('#'+id).attr('style', 'display:none');
    }
    function validate_rest()
    {
        $restaurant = $('#restaurant').val();
        if($restaurant=='')
        {
            $("#restaurant").focus();
            $("#restauranterr").html('Please select a restaurant');
            $("#restauranterr").show();
            return false;
        }
        document.forms["searchingForm"].submit();
    }
    function validate_service()
    {
        var len = $(".extraTypes:checked").length;
        if(len==0){
            $("#serviceerr").html('Please check atleast one Extra');
            $("#serviceerr").show();
            return false;
        }
        document.forms["serviceform"].submit();
    }
</script>
@endsection

