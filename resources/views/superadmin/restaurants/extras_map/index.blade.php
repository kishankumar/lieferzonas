@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Extras
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.extramap.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Extras</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            

            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/extramap/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/extramap/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
             <div class="col-sm-4 text-right" style="margin-bottom:10px;"> <div class="btn-group"><a class="btn btn-default" href="{{route('root.extramap.create')}}"><span>Assign Extras</span></a>


             <!-- <a class="btn btn-default DTTT_button_csv"  data-toggle="modal" data-target="#edit-form" data-original-title><span>Edit</span></a><a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Print</span></a><a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a><a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable"><span>Delete</span></a> --></div></div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            
            <div class="clearfix" style="margin: 20px 0;"></div>
            
            <div class="box-body">
			@if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
					  {{ Session::get('message') }}
					  {{ Session::put('message','') }}
                    </div>
                @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>S No.</th>
                <th>
                    <a href="{{url('root/extramap/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                    Restaurant
                    <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                    </a>
                </th>
                  <th>Views</th>
                </tr>
                </thead>
                <tbody>
               
              
                <?php $i=$pages['from']; ?>
                  @foreach($restro_names as $restro_name)
                        <tr>
                            <td><?php echo $i; ?></td>
                            
                            <td>{{$restro_name->f_name}} {{$restro_name->l_name}}</td>

                         	<?php $restroName=$restro_name['f_name'].' '.$restro_name['l_name'] ?>

                           <td>{!! Html::link('#','View Assigned Extras',array('id' => 'restro-kitchen','onclick'=>'show_modal("extra-type-view",'.$restro_name->id.',"extra-type-list","restro-name","'.$restroName.'")'), true) !!}

                           		</td>

                           {!! Form :: hidden('restaurant',$restroName ,['id'=>'restro-names'])  !!}
                          

                        </tr>
                        <?php $i++; ?>
                 @endforeach
              
               
              
                </tbody> 
                
              </table>
            {!! $restro_names->appends(Request::except('page'))->render() !!}
               		
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->



<div class="modal fade" id="extra-type-view" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                   
                    <span id="restro-name">hello</span>
                   
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
                   				<div class="panel-body">
                   				<div class="row PopupPublished" id="extra-type-list">
                   					

               					</div>
                   				</div>

                    </div>
                </div>
            </div>



<script>
function show_modal(formId,restro_id,appendId,restroNameId,restroName) {

	$.ajax({

		url: '{{ url('ajax/getExtraType') }}',
        type: "POST",
        data: {'restro_detail_id':restro_id, '_token': '{{ csrf_token() }}'},
        
	    success: function (data) {
		    //alert(data);
		    if(data=='' || data==null){
		    	data='No extra type assigned';
		    }
		    else{
		    	data=data;
		    }
			document.getElementById(appendId).innerHTML = data;
      document.getElementById(restroNameId).innerHTML = restroName;
			
		},
	    error: function (jqXHR, textStatus, errorThrown) {
	      
	    }
	});
	
	$("#"+formId).modal('show');

}


</script>

@endsection


