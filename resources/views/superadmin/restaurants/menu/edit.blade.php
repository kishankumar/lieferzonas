@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Menu
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.menu.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Edit Menu</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            @if(Session::has('message'))
				<div style="color:red;">
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
				</div>
             @endif
            {{--    Error Display--}}
            @if($errors->any())
                <ul class="alert">
                    @foreach($errors->all() as $error)
                        <li style="color:red;"> <b>{{ $error }}</b></li>
                    @endforeach
                </ul>
                @endif
                {{--    Error Display ends--}}

                 @if(Session::has('menu_error'))
                   <li style="color:red;"> {{ Session::get('menu_error') }}</li>
                    
                    {{ Session::put('menu_error','') }}
                @endif
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 id="contactLabel" class="panel-title"><span class="glyphicon glyphicon-plus"></span>Edit Menu.</h4>
                    </div>
                    
                     <div style="padding: 5px;" class="modal-body">
                     <div class="panel-body">

                     {!! Form::model($menus,['route'=>['root.menu.update',$menus->id],'method'=>'patch','class' => 'form','novalidate' => 'novalidate', 'files' => true,'id'=>'menu-form']) !!}
                     
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Restaurant Name', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      <select class="form-control js-example-basic-multiple" id="restro-id" name="restro_name" onchange="getCatKitchenPriceTypeByRestaurant(this.value,'category','kitchen','price-type')">

                      <option value="">Select</option>
                      @foreach($restro_names as $restro_name)

                      @if($restro_name->id==$menus->rest_detail_id)
                      <option value="{{$restro_name->id}}" selected>{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                      @else
                      <option value="{{$restro_name->id}}">{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                      @endif
                      @endforeach

                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Category Name', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      <select class="form-control" name="cat_name" id="category">

                       @foreach($categories as $category)

                      @if($category->id==$menus->rest_category_id)
                      <option value="{{$category->id}}" selected>{{$category->name}}</option>
                      @else
                      <option value="{{$category->id}}">{{$category->name}}</option>
                      @endif
                      @endforeach

                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Kitchen Name', array('class' => '"control-label')) !!}
                    
                      <select class="form-control" name="kitchen_name" id="kitchen">

                      @foreach($kitchens as $kitchen)

                      @if($kitchen->id==$menus->kitchen_id)
                      <option value="{{$kitchen->getKitchen->id}}" selected>{{$kitchen->getKitchen->name}}</option>
                      @else
                      <option value="{{$kitchen->getKitchen->id}}">{{$kitchen->getKitchen->name}}</option>
                      @endif
                      @endforeach
                      
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Name', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      {!! Form :: text('menu_name',$menus->name,['placeholder'=>'Enter Category Name','class'=>'form-control','id'=>'menu-name'])  !!}

                      <span id="cat-exist-error" style="color:red;"></span>
                    </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Short Name', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('short_menu_name',$menus->short_name,['placeholder'=>'Enter Short Name','class'=>'form-control','id'=>'short-name'])  !!}

                    </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Menu Code', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      {!! Form :: text('menu_code',$menus->code,['placeholder'=>'Enter Code','class'=>'form-control','id'=>'code'])  !!}

                    </div>

                  </div>
                  <div class="col-md-6">  
                    <div class="form-group">
                    {!! Form::label('', 'Image', array('class' => '"control-label')) !!}
                        <input class="form-control" type="file" name="file" style="border: none; font-size: 12px; padding: 0px;"></input>
                    </div>
                    {!! Html::image('public/uploads/superadmin/menu/'.$menus->image, '', array('class' => 'thumb','style'=>'max-width: 50%')) !!}
                </div>
                  
                  
                  <div class="col-md-6">
                    <div class="form-group">
                    <label class="&quot;control-label" for="">Description</label>

                    {!! Form :: textarea('description',$menus->description,['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    <label class="&quot;control-label" for="">Info</label><span class="red">*</span>

                    {!! Form :: text('info',$menus->info,['placeholder'=>'Enter Info','class'=>'form-control','id'=>'info'])  !!}
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    <label class="&quot;control-label" for="">Priority</label><span class="red">*</span>

                    {!! Form :: text('priority',$menus->priority,['placeholder'=>'Enter Priority','class'=>'form-control','id'=>'priority'])  !!}
                    </div>
                  </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Status') !!}
                            {!! Form::select('status',
                            array(
                            '1'=>'Active',
                            '0'=>'Deactive',
                            ), $menus->status, ['id' => 'status','class' => 'form-control']) !!}
                            <span id="statusErr" style="color:red"></span>
                        </div>

                    </div>

                    <div class="col-md-12">
                        <div class="form-group">

                            <?php
                            if($menus->have_submenu==1){
                                $check='checked';
                                $displayAttr='none';
                            }
                            else{
                                $check='';
                                $displayAttr='block';
                            }
                            ?>

                            <label class="checkbox">{!! Form :: checkbox('have_submenu','1',$check,['class'=>'submenu-exist','id'=>'submenu-exist','onclick'=>'showValueOnCheckbox("alergic-check","alergic-contents")'])  !!}
                                <span><strong>Have Submenu</strong></span>
                            </label>

                        </div>
                    </div>

                    <div id="have-submenu" style="display:<?php echo $displayAttr ?>;">
                        <div class="col-md-12">
                            <div class="form-group" id="price-type">

                                <span><strong>Price Type</strong></span>
                                <?php if(count($priceTypesWithValue)>0) { ?>
                                @foreach($priceTypesWithValue as $priceTypeWithValue)
                                <input type="text" name="price_types[]" placeholder="<?php echo $priceTypeWithValue->name ?> price" value="<?php echo $priceTypeWithValue->price ?>">
                                    <input type="hidden" name="price_types_id[]" value="<?php echo $priceTypeWithValue->id ?>">
                                @endforeach
                                <?php } else { ?>

                                @foreach($priceTypes as $priceType)
                                    <input type="text" name="price_types[]" placeholder="<?php echo $priceType->name ?> price" >
                                    <input type="hidden" name="price_types_id[]" value="<?php echo $priceType->id ?>">
                                @endforeach

                                <?php } ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                if($menus->is_alergic==1){
                                    $check='checked';
                                    $displayAttr='block';
                                }
                                else{
                                    $check='';
                                    $displayAttr='none';
                                }
                                ?>

                                <label class="checkbox">{!! Form :: checkbox('alergic','1',$check,['class'=>'alergic-check','id'=>'alergic-check','onclick'=>'showValueOnCheckbox("alergic-check","alergic-contents")'])  !!}
                                    <span><strong>Is Alergic</strong></span>
                                </label>

                                <div class="row" id="alergic-contents" style="display:<?php echo $displayAttr ?>;">
                                    @foreach($alergic_contents as $alergic_content)
                                        <div class="col-md-3">
                                            @if(in_array($alergic_content->id,$allergicMaps))
                                            <label class="checkbox">{!! Form :: checkbox('alergic_contents[]',$alergic_content->id,'checked') !!}<span>{{$alergic_content->name}}</span></label>
                                                @else
                                                <label class="checkbox">{!! Form :: checkbox('alergic_contents[]',$alergic_content->id) !!}<span>{{$alergic_content->name}}</span></label>
                                                @endif
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                if($menus->is_spicy==1){
                                    $check='checked';
                                    $displayAttr='block';
                                }
                                else{
                                    $check='';
                                    $displayAttr='none';
                                }
                                ?>

                                <label class="checkbox">{!! Form :: checkbox('spicy','1',$check,['class'=>'spicy-check','id'=>'spicy-check','onclick'=>'showValueOnCheckbox("spicy-check","spicy-contents")'])  !!}
                                    <span><strong>Is Spicy</strong></span>
                                </label>

                                <div class="row" id="spicy-contents" style="display:<?php echo $displayAttr ?>;">
                                    @foreach($spices as $spice)
                                        <div class="col-md-3">
                                            @if(in_array($spice->id,$spiceMaps))
                                            <label class="checkbox">{!! Form :: checkbox('spicy_content[]',$spice->id,'checked') !!}<span>{{$spice->name}}</span></label>
                                                @else
                                                <label class="checkbox">{!! Form :: checkbox('spicy_content[]',$spice->id) !!}<span>{{$spice->name}}</span></label>
                                                @endif
                                        </div>
                                    @endforeach


                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group margin-top">
              <hr>
              <input type="submit" value="Update" name="submit" class="btn btn-primary pull-right">
                
                  <!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->
                  
                  <p><span class="red">*</span> - Required Fields.</p>
                </div>
                {!! Form :: close() !!}
                
              <!-- </form> -->
            </div>
                     
                     </div>  
                      
                    </div>
            
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
     $("#menu-form").validate({
      rules: {


        restro_name: {
            required: true
               },
         menu_name: {
            required: true
               },
         cat_name: {
            required: true
               },
          info: {
              required: true
          },
          'price_types[]': {
              required: true,
              number: true
          },
         menu_code: {
            required: true
               },
         priority: {
            required: true,
             number: true

               }
         
         
        },
       messages: {
                    restro_name: "Please select restaurant",
                    menu_name: "Please enter menu name",
                    cat_name: "Please enter category name",
                    menu_code: "Please enter menu code",
                    info: "Please enter some information",
                    priority: "Please enter priority"
                    
                },
         
     });
});
    </script>

    <script type="text/javascript">

        function getCatKitchenPriceTypeByRestaurant(restro_id,firstAppendId,secondAppendId,thirdAppendId){

            var token=$("#token-value").val();

            $.ajax({

                url: base_url+'/ajax/getCatKitchenPriceType',
                type: "POST",
                dataType:"json",
                data: {'restroId':restro_id,'_token':token},

                success: function (data) {

                    var catgory=data.option+data.categoryItem;
                    var kitchen=data.option+data.kitchenItem;
                    if(data.priceTypesItem=='' || data.priceTypesItem==null){
                        var priceType='';

                    }
                    else{
                        var priceType=data.priceTypesSpan+data.priceTypesItem;

                    }


                    $('#'+firstAppendId).html(catgory);
                    $('#'+secondAppendId).html(kitchen);
                    $('#'+thirdAppendId).html(priceType);

                },
                error: function (jqXHR, textStatus, errorThrown) {

                }

            });

        }



    $("#submenu-exist").click(function(){

        var checked=$(".submenu-exist").is(':checked');

        if(checked==true){
            $("#have-submenu").attr("style","display:none;");
        }
        else{
            $("#have-submenu").attr("style","display:block;");
        }
    });


    </script>

    
    @endsection



