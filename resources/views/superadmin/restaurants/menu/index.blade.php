@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Menu
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.menu.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Menu List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Table With Full Features</h3>
            </div>
			
            <div class="col-md-8">
                {!! Form::open(array('route'=>'root.menu.index','class'=>'form-horizontal','method'=>'GET')) !!}
            
               
                 <label class="col-sm-2 control-label">Restaurant Name</label>
                 <div class="col-sm-4">
               
                   <select class="form-control js-example-basic-multiple" name="restaurant">
                   <option value="">Select</option>

                   @foreach($restro_names as $restro_name)

                   @if($restro_name->id==$restro_detail_id)
                   <option value="{{$restro_name->id}}" selected >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @else
                   <option value="{{$restro_name->id}}" >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @endif
                   @endforeach
                   </select>

                 </div>
               
                 <div class="col-sm-4">
                   <button class="btn btn-primary" type="submit">Get Details</button>
                   <a class="btn btn-primary" href="{{url('root/menu/')}}">Reset Search</a>
                 </div>
                
               
               {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-4 text-right" style="margin-bottom:10px;"> 
                <div class="btn-group">
			 <a class="btn btn-default" href="{{route('root.menu.create')}}"><span>Add</span></a>
			 <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
			 <a class="btn btn-default DTTT_button_print" onclick="download_menu()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>

                        <a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable" onclick="delete_items('menu_class','menus','rest_menus')"><span>Delete</span></a>
                </div>
            </div>
            <div class="clearfix"></div>

            @if(Session::has('message'))
                <div class="alert alert-success" style="padding: 7px 15px;">
                    {{ Session::get('message') }}
                    {{ Session::put('message','') }}
                </div>
            @endif
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th>
                    <div class="checkbox icheck">
                        <label>
                        {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                        <span class=""></span>
                        </label>
                    </div>
                </th>
                 <th>Menu Name</th>
                  <th>Restaurant</th>
                  <th>Category</th>
                  <th>Kitchen</th>
                  <th>Menu Code</th>
                    <th>Status</th>
                   <th>Action</th>
                  
                  
                </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
              @foreach($getRestroMenus as $getRestroMenu)
              <tr>
                <td><div class="checkbox icheck">
                  <label>
                   <!--  <input type="checkbox"> <span> </span> -->
                    {!! Form::checkbox('menus', $getRestroMenu->id, null, ['class' => 'menu_class checkBoxClass']) !!}<span></span>
                  </label>
                </div></td>
                <td>
                    <a href="{{url('root/menu/'.$getRestroMenu->id)}}">
                        {{$getRestroMenu->name}}
                    </a>
                </td>
                <td>{{$getRestroMenu->getResturant->f_name}} {{$getRestroMenu->getResturant->l_name}}</td>
                <td>@if($getRestroMenu->getCategory){{$getRestroMenu->getCategory->name}}@else{{''}}@endif</td>
                <td>@if($getRestroMenu->getKitchen){{$getRestroMenu->getKitchen->name}}@else{{''}} @endif</td>
                <td>{{$getRestroMenu->code}}</td>
                  <td>@if($getRestroMenu->status=='1')
                          Active
                      @elseif($getRestroMenu->status=='0')
                          Inactive
                      @endif
                  </td>
                <td>
                <a href="{{URL :: asset('root/menu/'.$getRestroMenu->id)}}/edit" class="btn btn-success"><i class="fa fa-edit"></i></a>
                </td>
              </tr>
              <?php $i++; ?>
              @endforeach
              
                </tbody>
                
              </table>
              {!! $getRestroMenus->appends(['restaurant'=>$restro_detail_id])->render() !!}
               		
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->

@endsection
@section('script')
<script>
/*added by Rajlakshmi(31-05-16)*/
		function download_menu(){
            var len = $(".menu_class:checked").length;
            if(len==0){
                alert('Please select atleast one Menu');
                return false;
            }

            var r = confirm("Are you sure to download the selected Menu.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="menus"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: '{{url('root/menu/download')}}',
                    type: "post",
                    dataType:"json",
                    data: {'selectedMenus':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("menu_list.pdf");
				}
            });
        } else {
            return false;
        }
	   }
	 
/*added by Rajlakshmi(31-05-16)*/
</script>
@endsection

