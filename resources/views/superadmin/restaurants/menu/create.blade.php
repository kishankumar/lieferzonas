@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Menu
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('root.menu.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a>
                </li>
                <li class="active">Add Menu</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">


                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="box">
                        <div class="box-header">
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if(Session::has('message'))
                                <div style="color:red;">
                                    {{ Session::get('message') }}

                                    {{ Session::put('message','') }}
                                </div>
                            @endif
                            {{--    Error Display--}}
                            @if($errors->any())
                                <ul class="alert">
                                    @foreach($errors->all() as $error)
                                        <li style="color:red;"><b>{{ $error }}</b></li>
                                    @endforeach
                                </ul>
                            @endif
                            {{--    Error Display ends--}}

                            @if(Session::has('menu_error'))
                                <li style="color:red;"> {{ Session::get('menu_error') }}</li>

                                {{ Session::put('menu_error','') }}
                            @endif

                            <div class="panel panel-primary theme-border">
                                <div class="panel-heading theme-bg">

                                    <h4 id="contactLabel" class="panel-title"><span
                                                class="glyphicon glyphicon-plus"></span> Add New Menu.</h4>
                                </div>

                                <div style="padding: 5px;" class="modal-body">
                                    <div class="panel-body">
                                        {!! Form::open(array('route'=>'root.menu.store','id'=>'menu-form','files'=>true)) !!}

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    {!! Form::label('', 'Restaurant Name', array('class' => '"control-label')) !!}
                                                    <span class="red">*</span>

                                                    <select class="form-control js-example-basic-multiple"
                                                            id="restro-id" name="restro_name"
                                                            onchange="getCatKitchenPriceTypeByRestaurant(this.value,'category','kitchen','price-type')">

                                                        <option value="">Select</option>
                                                        @foreach($restro_names as $restro_name)
                                                            <option value="{{$restro_name->id}}">{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('', 'Category Name', array('class' => '"control-label')) !!}
                                                    <span class="red">*</span>

                                                    <select class="form-control" name="cat_name" id="category">


                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('', 'Kitchen Name', array('class' => '"control-label')) !!}

                                                    <select class="form-control" name="kitchen_name" id="kitchen">


                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('', 'Name', array('class' => '"control-label')) !!}
                                                    <span class="red">*</span>

                                                    {!! Form :: text('menu_name','',['placeholder'=>'Enter Menu Name','class'=>'form-control','id'=>'cat-name'])  !!}

                                                    <span id="cat-exist-error" style="color:red;"></span>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('', 'Short Name', array('class' => '"control-label')) !!}

                                                    {!! Form :: text('short_menu_name','',['placeholder'=>'Enter Short Name','class'=>'form-control','id'=>'short-name'])  !!}

                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('', 'Menu Code', array('class' => '"control-label')) !!}
                                                    <span class="red">*</span>

                                                    {!! Form :: text('menu_code','',['placeholder'=>'Enter Code','class'=>'form-control','id'=>'code'])  !!}

                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('', 'Image', array('class' => '"control-label')) !!}

                                                    <input class="form-control" type="file" name="file"
                                                           style="border: none; font-size: 12px; padding: 0px;"></input>
                                                </div>

                                                <div class="form-group">
                                                    <label class="&quot;control-label" for="">Priority</label><span
                                                            class="red">*</span>

                                                    {!! Form :: text('priority','',['placeholder'=>'Enter Priority','class'=>'form-control','id'=>'priority'])  !!}
                                                </div>

                                                <div class="form-group">
                                                    <label class="&quot;control-label" for="">Info</label><span
                                                            class="red">*</span>

                                                    {!! Form :: text('info','',['placeholder'=>'Enter Info','class'=>'form-control','id'=>'info'])  !!}
                                                </div>

                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="&quot;control-label" for="">Description</label>

                                                    {!! Form :: textarea('description','',['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
                                                </div>
                                            </div>

                                            <div class="col-md-6">

                                            </div>

                                            <div class="col-md-6">

                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">


                                                    <label class="checkbox">{!! Form :: checkbox('have_submenu','1','',['class'=>'submenu-exist','id'=>'submenu-exist','onclick'=>'showValueOnCheckbox("alergic-check","alergic-contents")'])  !!}
                                                        <span><strong>Have Submenu</strong></span>
                                                    </label>

                                                </div>
                                            </div>

                                            <div id="have-submenu">
                                                <div class="col-md-12">
                                                    <div class="form-group" id="price-type">


                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">


                                                        <label class="checkbox">{!! Form :: checkbox('alergic',1,'',['class'=>'alergic-check','id'=>'alergic-check','onclick'=>'showValueOnCheckbox("alergic-check","alergic-contents")'])  !!}
                                                            <span><strong>Is Alergic</strong></span>
                                                        </label>

                                                        <div class="row" id="alergic-contents" style="display:none;">
                                                            @foreach($alergic_contents as $alergic_content)
                                                                <div class="col-md-3">
                                                                    <label class="checkbox">{!! Form :: checkbox('alergic_contents[]',$alergic_content->id) !!}
                                                                        <span>{{$alergic_content->name}}</span></label>
                                                                </div>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">


                                                        <label class="checkbox">{!! Form :: checkbox('spicy',1,'',['class'=>'spicy-check','id'=>'spicy-check','onclick'=>'showValueOnCheckbox("spicy-check","spicy-contents")'])  !!}
                                                            <span><strong>Is Spicy</strong></span>
                                                        </label>

                                                        <div class="row" id="spicy-contents" style="display:none;">
                                                            @foreach($spices as $spice)
                                                                <div class="col-md-3">
                                                                    <label class="checkbox">{!! Form :: checkbox('spicy_content[]',$spice->id) !!}
                                                                        <span>{{$spice->name}}</span></label>
                                                                </div>
                                                            @endforeach


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin-top">
                                            <hr>
                                            <input type="submit" value="Add" name="submit"
                                                   class="btn btn-primary pull-right">

                                            <!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->

                                            <p><span class="red">*</span> - Required Fields.</p>
                                        </div>
                                    {!! Form :: close() !!}

                                    <!-- </form> -->
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <!-- quick email widget -->


                </section>
                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#menu-form").validate({
                rules: {


                    restro_name: {
                        required: true
                    },
                    menu_name: {
                        required: true
                    },
                    cat_name: {
                        required: true
                    },
                    info: {
                        required: true
                    },
                    'price_types[]': {
                        required: true,
                        number: true
                    },
                    menu_code: {
                        required: true
                    },
                    priority: {
                        required: true,
                        number: true

                    }


                },
                messages: {
                    restro_name: "Please select restaurant",
                    menu_name: "Please enter menu name",
                    cat_name: "Please enter category name",
                    menu_code: "Please enter menu code",
                    info: "Please enter some information",
                    priority: "Please enter priority"

                },

            });
        });
    </script>

    <script type="text/javascript">

        function getCatKitchenPriceTypeByRestaurant(restro_id, firstAppendId, secondAppendId, thirdAppendId) {

            var token = $("#token-value").val();

            $.ajax({

                url: base_url + '/ajax/getCatKitchenPriceType',
                type: "POST",
                dataType: "json",
                data: {'restroId': restro_id, '_token': token},

                success: function (data) {

                    var catgory = data.option + data.categoryItem;
                    var kitchen = data.option + data.kitchenItem;
                    if (data.priceTypesItem == '' || data.priceTypesItem == null) {
                        var priceType = '';

                    }
                    else {
                        var priceType = data.priceTypesSpan + data.priceTypesItem;

                    }


                    $('#' + firstAppendId).html(catgory);
                    $('#' + secondAppendId).html(kitchen);
                    $('#' + thirdAppendId).html(priceType);

                },
                error: function (jqXHR, textStatus, errorThrown) {

                }

            });

        }

        /*function showValueOnCheckbox(classname,displayId) {

         var checked=$('.'+classname).is(':checked');

         if(checked==true){
         $("#"+displayId).attr("style","display:block;");
         }
         else{
         $("#"+displayId).attr("style","display:none;");
         }

         }*/

        $("#submenu-exist").click(function () {

            var checked = $(".submenu-exist").is(':checked');

            if (checked == true) {
                $("#have-submenu").attr("style", "display:none;");
            }
            else {
                $("#have-submenu").attr("style", "display:block;");
            }
        });


    </script>


@endsection



