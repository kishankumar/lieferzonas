@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

    <script type="text/template" id="extra-choice-form-additional-element-template">

        <div class="row form-group margin-top" id="extra-choice-form-additional-element-<%- index %>">
            <div class="col-md-5">
                {!! Form::label('', 'Element-Name', array('class' => 'control-label')) !!}
                <span class="red">*</span>
                <input type="text" name="element-name-<%- index %>" class="form-control"/>
            </div>
            <div class="col-md-5">
                {!! Form::label('', 'Element-Preis', array('class' => 'control-label')) !!}
                <span class="red">*</span>
                <input type="text" name="element-price-<%- index %>" class="form-control" value="0.00"/>
            </div>
            <div class="col-md-2">
                {!! Form::label('', 'entfernen', array('class' => 'control-label')) !!}
                <button type="button" class="btn btn-block btn-danger"
                        id="extra-choice-form-element-remove-<%- index %>"><span
                            class="glyphicon glyphicon-remove"></span></button>
            </div>
        </div>

    </script>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurant Men&uuml;
                <small>Bearbeiten</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Edit Menu</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <h1>{{ $rest_detail->f_name }}</h1>

            @if(Session::has('message'))
                <div style="color:red;">
                    {{ Session::get('message') }}

                    {{ Session::put('message','') }}
                </div>
            @endif
            {{--    Error Display--}}
            @if($errors->any())
                <ul class="alert">
                    @foreach($errors->all() as $error)
                        <li style="color:red;"><b>{{ $error }}</b></li>
                    @endforeach
                </ul>
            @endif
            {{--    Error Display ends--}}

            @if(Session::has('menu_error'))
                <li style="color:red;"> {{ Session::get('menu_error') }}</li>

                {{ Session::put('menu_error','') }}
            @endif

            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal"
                    data-target="#lie-add-category-modal">
                Kategorie hinzuf&uuml;gen
            </button>

            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#lie-add-menu-modal">
                Men&uuml; hinzuf&uuml;gen
            </button>

            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#lie-add-meal-modal">
                Gericht hinzuf&uuml;gen
            </button>

            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#lie-add-extra-modal">
                Extra-Auswahl hinzuf&uuml;gen
            </button>

            @foreach ($menus as $menu)
                <?php
                $menu_category = null;
                $menu_kitchen = null;
                foreach ($categories as $category) {
                    if ($menu->rest_category_id == $category->id) {
                        $menu_category = $category;
                        break;
                    }
                }
                foreach ($kitchens as $kitchen) {
                    if ($menu->kitchen_id == $kitchen->id) {
                        $menu_kitchen = $kitchen;
                        break;
                    }
                }
                ?>
                <h2>{{$menu->name}} ({{$menu_kitchen != null ? $menu_kitchen->name : "Keine Küche"}}
                    , {{$menu_category != null ? $menu_category->name : "Keine Kategorie"}})
                    <button type="button" class="btn btn-sm btn-danger" onclick="removeMenu('{{$menu->id}}')"><span class="glyphicon glyphicon-remove"></span></button>
                </h2>

                <ul>
                    @foreach ($meals as $meal)
                        @if ($meal->menu_id == $menu->id)
                            <li><strong>{{$meal->name}}</strong>
                                <?php
                                $delivery_price = App\superadmin\RestSubMenu::getSubMenuDeliveryPrice($meal->id);
                                $pickup_price = App\superadmin\RestSubMenu::getSubMenuPickupPrice($meal->id);
                                if ($delivery_price != null) {
                                    $delivery_price = number_format($delivery_price->price, 2);
                                } else {
                                    $delivery_price = " - ";
                                }
                                if ($pickup_price != null) {
                                    $pickup_price = number_format($pickup_price->price, 2);
                                } else {
                                    $pickup_price = " - ";
                                }
                                ?>
                                [
                                {{$delivery_price}} /
                                {{$pickup_price}}
                                ]
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-default dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">Extra-Auswahl
                                        hinzuf&uuml;gen<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        @foreach (\App\superadmin\ExtraChoice::getByRestaurant($rest_detail->id) as $extra_choice)
                                            <li>
                                                <a href="javascript: void(0); event.stopPropagation()">
                                                    <label>
                                                        <input type="checkbox"
                                                               @if (\App\superadmin\SubMenuExtraMap::hasSubMenuExtra($meal->id,$extra_choice->id))
                                                               checked
                                                               @endif
                                                               class="sub-menu-extra-choice-{{$meal->id}}"
                                                               data-meal="{{$meal->id}}"
                                                               data-extra="{{$extra_choice->id}}"/>
                                                        {{$extra_choice->name}}
                                                    </label>
                                                </a>
                                            </li>
                                        @endforeach
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <a href="javascript: saveExtraChoices({{$meal->id}})"><strong>Speichern</strong></a>
                                        </li>
                                    </ul>
                                </div>
                                <ul>
                                    <li>{{$meal->info}}</li>
                                </ul>
                            </li>
                        @endif
                    @endforeach
                </ul>
        @endforeach

        <!-- Kategorie Modal -->
            <div class="modal fade" tabindex="-1" role="dialog" id="lie-add-category-modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Kategorie hinzuf&uuml;gen</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('route'=>'root.category.store','id'=>'category-form','files'=>true)) !!}
                            <input type="hidden" name="restro_name" value="{{$rest_detail->id}}"/>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Überkategorie', array('class' => 'control-label')) !!}
                                        <span class="red">*</span>
                                        <select class="form-control" name="root_cat_name" id="root-category">
                                            @foreach (\App\superadmin\Root_categorie::all() as $root_category)
                                                <option value="{{$root_category->id}}">{{$root_category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Name', array('class' => 'control-label')) !!}
                                        <span class="red">*</span>
                                        {!! Form :: text('cat_name','',['placeholder'=>'Enter Category Name','class'=>'form-control','id'=>'cat-name','onchange'=>'checkExistingValue("cat-name","restro-id","rest_categories","name","rest_detail_id","cat-exist-error","This category is already exist for this restaurant","")'])  !!}
                                        <span id="cat-exist-error" style="color:red;"></span>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Bild', array('class' => 'control-label')) !!}<span
                                                class="red">*</span>

                                        <input class="form-control" type="file" name="file"
                                               style="border: none; font-size: 12px; padding: 0px;"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('valid_from', 'Gültig von', array('class' => 'control-label')) !!}
                                        <span class="red">*</span>
                                        {!! Form::date('valid_from', '2000-01-01', ['placeholder'=>'Enter from date','class'=>'form-control','id'=>'valid-from']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('valid_to', 'Gültig bis', array('class' => 'control-label')) !!}
                                        <span class="red">*</span>
                                        {!! Form::date('valid_to', '2099-01-01', ['placeholder'=>'Enter from date','class'=>'form-control','id'=>'valid-from']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="&quot;control-label" for="">Description</label>

                                    {!! Form :: textarea('description','',['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
                                    <!-- <textarea class="form-control"> </textarea> -->
                                    </div>
                                </div>

                            </div>

                            <div class="form-group margin-top">
                                <hr>
                                <input type="submit" value="Add" name="submit" class="btn btn-primary pull-right"
                                       onclick='checkExistingValue("cat-name","restro-id","rest_categories","name","rest_detail_id","cat-exist-error","This category is already exist for this restaurant","")'>

                                <!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->

                                <p><span class="red">*</span> - Required Fields.</p>
                            </div>
                            {!! Form :: close() !!}
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Menü Modal -->
            <div class="modal fade" tabindex="-1" role="dialog" id="lie-add-menu-modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Men&uuml; hinzuf&uuml;gen</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('route'=>'root.menu.store','id'=>'menu-form','files'=>true)) !!}

                            <input type="hidden" name="restro_name" value="{{$rest_detail->id}}"/>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Kategorie', array('class' => '"control-label')) !!}
                                        <span class="red">*</span>

                                        <select class="form-control" name="cat_name" id="category">
                                            @foreach ($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Küche', array('class' => '"control-label')) !!}

                                        <select class="form-control" name="kitchen_name" id="kitchen">
                                            @foreach ($kitchens as $kitchen)
                                                <option value="{{$kitchen->id}}">{{$kitchen->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Name', array('class' => '"control-label')) !!}<span
                                                class="red">*</span>

                                        {!! Form :: text('menu_name','',['placeholder'=>'Enter Menu Name','class'=>'form-control','id'=>'cat-name'])  !!}

                                        <span id="cat-exist-error" style="color:red;"></span>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Kurzname', array('class' => '"control-label')) !!}

                                        {!! Form :: text('short_menu_name','',['placeholder'=>'Enter Short Name','class'=>'form-control','id'=>'short-name'])  !!}

                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Menu Code', array('class' => '"control-label')) !!}
                                        <span
                                                class="red">*</span>

                                        {!! Form :: text('menu_code','',['placeholder'=>'Enter Code','class'=>'form-control','id'=>'code'])  !!}

                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Image', array('class' => '"control-label')) !!}

                                        <input class="form-control" type="file" name="file"
                                               style="border: none; font-size: 12px; padding: 0px;"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="priority">Priority</label>
                                        <span class="red">*</span>

                                        {!! Form :: text('priority','',['placeholder'=>'Enter Priority','class'=>'form-control','id'=>'priority'])  !!}
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="info">Info</label>
                                        <span class="red">*</span>

                                        {!! Form :: text('info','',['placeholder'=>'Enter Info','class'=>'form-control','id'=>'info'])  !!}
                                    </div>

                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="description">Description</label>

                                        {!! Form :: textarea('description','',['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
                                    </div>
                                </div>

                                <div class="col-md-6">

                                </div>

                                <div class="col-md-6">

                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="checkbox">{!! Form :: checkbox('have_submenu','1','',['class'=>'submenu-exist','id'=>'submenu-exist','onclick'=>'showValueOnCheckbox("alergic-check","alergic-contents")'])  !!}
                                            <span><strong>Have Submenu</strong></span>
                                        </label>
                                    </div>
                                </div>

                                <div id="have-submenu">
                                    <div class="col-md-12">
                                        <div class="form-group price-type">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">


                                            <label class="checkbox">{!! Form :: checkbox('alergic',1,'',['class'=>'alergic-check','id'=>'alergic-check','onclick'=>'showValueOnCheckbox("alergic-check","alergic-contents")'])  !!}
                                                <span><strong>Hat Allergene</strong></span>
                                            </label>

                                            <div class="row" id="alergic-contents" style="display:none;">
                                                @foreach(\App\superadmin\Alergic_content::where('status',1)->select('id','name')->get() as $alergic_content)
                                                    <div class="col-md-3">
                                                        <label class="checkbox">{!! Form :: checkbox('alergic_contents[]',$alergic_content->id) !!}
                                                            <span>{{$alergic_content->name}}</span></label>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">


                                            <label class="checkbox">{!! Form :: checkbox('spicy',1,'',['class'=>'spicy-check','id'=>'spicy-check','onclick'=>'showValueOnCheckbox("spicy-check","spicy-contents")'])  !!}
                                                <span><strong>Extra Infos</strong></span>
                                            </label>

                                            <div class="row" id="spicy-contents" style="display:none;">
                                                @foreach(\App\superadmin\Spice::where('status',1)->select('id','name')->get() as $spice)
                                                    <div class="col-md-3">
                                                        <label class="checkbox">{!! Form :: checkbox('spicy_content[]',$spice->id) !!}
                                                            <span>{{$spice->name}}</span></label>
                                                    </div>
                                                @endforeach


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-top">
                                <hr>
                                <input type="submit" value="Add" name="submit" class="btn btn-primary pull-right">

                                <!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->

                                <p><span class="red">*</span> - Required Fields.</p>
                            </div>
                            {!! Form :: close() !!}
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Gericht Modal -->
            <div class="modal fade" tabindex="-1" role="dialog" id="lie-add-meal-modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Gericht hinzuf&uuml;gen</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('route'=>'root.submenu.store','id'=>'submenu-form','files'=>true)) !!}
                            <input type="hidden" name="restro_name" value="{{$rest_detail->id}}"/>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Menü', array('class' => 'control-label')) !!}<span
                                                class="red">*</span>

                                        <select class="form-control" name="menu_name" id="menu">
                                            @foreach ($menus as $menu)
                                                <option value="{{$menu->id}}">{{$menu->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Name', array('class' => 'control-label')) !!}<span
                                                class="red">*</span>
                                        {!! Form :: text('submenu_name','',['placeholder'=>'Enter Sub Menu Name','class'=>'form-control','id'=>'sub-menu'])  !!}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Kurzname', array('class' => 'control-label')) !!}
                                        {!! Form :: text('shortname','',['placeholder'=>'Enter Short Name','class'=>'form-control','id'=>'sub-menu'])  !!}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Menü Code', array('class' => 'control-label')) !!}
                                        {!! Form :: text('code','',['placeholder'=>'Enter Code','class'=>'form-control','id'=>'sub-menu'])  !!}
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Image', array('class' => 'control-label')) !!}
                                        <input class="form-control" type="file" name="file"
                                               style="border: none; font-size: 12px; padding: 0px;">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="&quot;control-label" for="">Beschreibung</label>
                                    {!! Form :: textarea('description','',['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
                                    <!-- <textarea class="form-control"> </textarea> -->
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Info', array('class' => 'control-label')) !!}
                                        {!! Form :: text('info','',['placeholder'=>'Enter Information','class'=>'form-control','id'=>'sub-menu'])  !!}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Priority', array('class' => 'control-label')) !!}<span
                                                class="red">*</span>
                                        {!! Form :: text('priority','',['placeholder'=>'Enter Priority','class'=>'form-control','id'=>'priority'])  !!}
                                    </div>

                                </div>

                                <div id="have-submenu">
                                    <div class="col-md-12">
                                        <div class="form-group price-type">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="checkbox">{!! Form :: checkbox('alergic',1,'',['class'=>'menu-alergic-check','id'=>'menu-alergic-check','onclick'=>'showValueOnCheckbox("menu-alergic-check","menu-alergic-contents")'])  !!}
                                                <span><strong>Hat Allergene</strong></span>
                                            </label>
                                            <div class="row" id="menu-alergic-contents" style="display:none;">
                                                @foreach(\App\superadmin\Alergic_content::where('status',1)->select('id','name')->get() as $alergic_content)
                                                    <div class="col-md-3">
                                                        <label class="checkbox">{!! Form :: checkbox('alergic_contents[]',$alergic_content->id) !!}
                                                            <span>{{$alergic_content->name}}</span></label>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="checkbox">{!! Form :: checkbox('spicy',1,'',['class'=>'menu-spicy-check','id'=>'menu-spicy-check','onclick'=>'showValueOnCheckbox("menu-spicy-check","menu-spicy-contents")'])  !!}
                                                <span><strong>Extra Infos</strong></span>
                                            </label>
                                            <div class="row" id="menu-spicy-contents" style="display:none;">
                                                @foreach(\App\superadmin\Spice::where('status',1)->select('id','name')->get() as $spice)
                                                    <div class="col-md-3">
                                                        <label class="checkbox">{!! Form :: checkbox('spicy_content[]',$spice->id) !!}
                                                            <span>{{$spice->name}}</span></label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group margin-top">
                                <hr>
                                <input type="submit" value="Add" name="submit" class="btn btn-primary pull-right"
                                       onclick='checkExistingValue("cat-name","restro-id","rest_categories","name","rest_detail_id","cat-exist-error","This category is already exist for this restaurant","")'>

                                <!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->

                                <p><span class="red">*</span> - Required Fields.</p>
                            </div>
                            {!! Form :: close() !!}
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Extra Auswahl Modal -->
            <div class="modal fade" tabindex="-1" role="dialog" id="lie-add-extra-modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Gericht hinzuf&uuml;gen</h4>
                        </div>
                        <div class="modal-body">

                            {!! Form::open(array('route'=>'root.extra_choice.store','id'=>'extra-choice-form')) !!}
                            <input type="hidden" name="restaurant_id" value="{{$rest_detail->id}}"/>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Name', array('class' => 'control-label')) !!}
                                        <span class="red">*</span>
                                        <input type="text" name="name" class="form-control"/>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Typ', array('class' => 'control-label')) !!}
                                        <span class="red">*</span>
                                        <select class="form-control" name="type">
                                            <option value="single-choice">Einzelauswahl (z.B. Getränk zum Menü)</option>
                                            <option value="multiple-choice">Mehrfachauswahl (z.B. Pizzaauflagen)
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row form-group margin-top">
                                <div class="col-md-5">
                                    {!! Form::label('', 'Element-Name', array('class' => 'control-label')) !!}
                                    <span class="red">*</span>
                                    <input type="text" name="element-name-1" class="form-control"/>
                                </div>
                                <div class="col-md-5">
                                    {!! Form::label('', 'Element-Preis', array('class' => 'control-label')) !!}
                                    <span class="red">*</span>
                                    <input type="text" name="element-price-1" class="form-control" value="0.00"/>
                                </div>
                            </div>

                            <div class="form-group margin-top">
                                <button type="button" class="btn btn-sm btn-block btn-primary"
                                        id="extra-choice-form-add-element">Weiteres Element
                                </button>
                            </div>

                            <div id="extra-choice-form-additional-elements">

                            </div>

                            <div class="form-group margin-top">
                                <hr>
                                <input type="submit" value="Add" name="submit" class="btn btn-primary pull-right"/>

                                <p><span class="red">*</span> - Required Fields.</p>
                            </div>

                            {!! Form :: close() !!}

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </section>
    </div>

@endsection

@section('script')

    <script src="/assets/js/underscore-min.js"></script>
    <script>

        var index = 2;

        $(document).ready(function () {

            // Submenü Form
            $("#submenu-form").validate({
                rules: {
                    restro_name: {
                        required: true
                    },
                    menu_name: {
                        required: true
                    },
                    submenu_name: {
                        required: true
                    },
                    info: {
                        required: true
                    },
                    'price_types[]': {
                        required: true,
                        number: true
                    },
                    priority: {
                        required: true,
                        number: true
                    }
                },
                messages: {
                    restro_name: "Please select restaurant",
                    menu_name: "Please select menu name",
                    submenu_name: "Please enter submenu name",
                    info: "Please enter some information",
                    priority: "Please enter priority"
                },
            });

            // Menü-Form Validierung
            $("#menu-form").validate({
                rules: {
                    restro_name: {
                        required: true
                    },
                    menu_name: {
                        required: true
                    },
                    cat_name: {
                        required: true
                    },
                    info: {
                        required: true
                    },
                    'price_types[]': {
                        required: true,
                        number: true
                    },
                    menu_code: {
                        required: true
                    },
                    priority: {
                        required: true,
                        number: true

                    }
                },
                messages: {
                    restro_name: "Please select restaurant",
                    menu_name: "Please enter menu name",
                    cat_name: "Please enter category name",
                    menu_code: "Please enter menu code",
                    info: "Please enter some information",
                    priority: "Please enter priority"

                },
            });

            // Submenü Klappe
            $("#submenu-exist").click(function () {
                var checked = $(".submenu-exist").is(':checked');
                if (checked == true) {
                    $("#have-submenu").attr("style", "display:none;");
                }
                else {
                    $("#have-submenu").attr("style", "display:block;");
                }
            });

            $.ajax({
                url: base_url + '/ajax/gatPriceType',
                type: "POST",
                dataType: "json",
                data: {'restId': '{{$rest_detail->id}}', '_token': '{{csrf_token()}}'},

                success: function (data) {
                    if (data.priceTypesItem == '' || data.priceTypesItem == null) {
                        var priceType = '';
                    }
                    else {
                        var priceType = data.priceTypesSpan + data.priceTypesItem;
                    }
                    $('.price-type').html(priceType);
                }
            });

            $("#extra-choice-form-add-element").click(function () {
                var currentIndex = index;
                index++;
                var template = _.template($("#extra-choice-form-additional-element-template").html());
                var container = $("#extra-choice-form-additional-elements");
                container.append(template({
                    index: currentIndex
                }));
                var removeButton = $("#extra-choice-form-element-remove-" + currentIndex);
                removeButton.click(function (evt) {
                    var element = $("#extra-choice-form-additional-element-" + currentIndex);
                    element.remove();
                });
            });
        });

        function removeMenu(menuId) {
            $.ajax({
                url: "/root/menu/" + menuId,
                type: "DELETE",
                dataType: "json",
                data: {
                    "_token":"{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response.status === "failure") return alert(response.reason);
                    alert("Erfolgreich gespeichert!");
                    window.location.reload();
                }
            });
        }

        function saveExtraChoices(mealId) {
            var extras = [];
            $(".sub-menu-extra-choice-" + mealId).each(function (index) {
                var checkInput = $(this);
                if (checkInput.prop("checked")) {
                    var extra = checkInput.data("extra");
                    extras.push(extra);
                }
            });
            $.ajax({
                url: "{{ url('root/add_extra') }}",
                type: "post",
                dataType: "json",
                data: {
                    "sub_menu": mealId,
                    "extras": extras,
                    "_token":"{{ csrf_token() }}"
                },
                success: function(response) {
                    if (response.status === "failure") return alert(response.reason);
                    alert("Erfolgreich gespeichert!");
                }
            })
        }
    </script>

@endsection