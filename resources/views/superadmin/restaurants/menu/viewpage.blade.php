@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Menu
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restaurant.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Menu Detail</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-tags"></i> Menu Detail</h3>
                    <span class="pull-right">
                        <a class="btn btn-primary"  href="javascript:history.go(-1)">Back</a>
                    </span>
                </div>
                <!-- /.box-header -->
                
        @if(count($viewDetails))
            @foreach($viewDetails as $info)
                <div class="box-body">
                    <div class="row form-horizontal">
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Menu Name :</label></div>
                            <div class="col-md-8">{{ ucfirst($info->name)  }}</div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Restaurant Name:</label></div>
                            <div class="col-md-8">
                                {{$info->getResturant->f_name}} {{$info->getResturant->l_name}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Category Name:</label></div>
                            <div class="col-md-8">
                                @if($info->getCategory){{$info->getCategory->name}}@else{{''}}@endif
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Kitchen Name  :</label></div>
                            <div class="col-md-8">
                                @if($info->getKitchen){{$info->getKitchen->name}}@else{{''}} @endif
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Short Name  :</label></div>
                            <div class="col-md-8">
                                {{$info->short_name}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Menu Code  :</label></div>
                            <div class="col-md-8">
                                {{$info->code}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Status :</label></div>
                            <div class="col-md-8">
                                @if($info->status == 1) 
                                    <i class="fa fa-check text-success text-success"></i>
                                @else
                                    <i class="fa fa-ban text-danger text-danger" ></i>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Have Submenu :</label></div>
                            <div class="col-md-8">
                                @if($info->have_submenu){{'Yes'}}@else{{'No'}} @endif
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Is Alergic :</label></div>
                            <div class="col-md-8">
                                @if($info->is_alergic){{'Yes'}}@else{{'No'}} @endif
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Is Spicy :</label></div>
                            <div class="col-md-8">
                                @if($info->is_spicy){{'Yes'}}@else{{'No'}} @endif
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Description :</label></div>
                            <div class="col-md-8">
                                {{$info->description}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Delivery Price :</label></div>
                            <div class="col-md-8">
                                &euro; {{ \App\superadmin\RestMenu::getMenuDeliveryTakePrice($info->id,1,$info->rest_detail_id) }}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Pickup Price :</label></div>
                            <div class="col-md-8">
                                &euro; {{ \App\superadmin\RestMenu::getMenuDeliveryTakePrice($info->id,2,$info->rest_detail_id) }}
                            </div>
                        </div>
                        
                    </div>
                </div>
            @endforeach
        @endif
            </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
    </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

	
	<!--added by Rajlakshmi(02-06-16)-->
	<div id="download" style="display:none;" >
	</div>
   <!--added by Rajlakshmi(02-06-16)-->


    @endsection

@section('script')

@endsection





