@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')


<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Restaurant Owner
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		  <li><a href="{{route('root.restowner.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Edit Restaurant Owner</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">

<!--              <h3 class="box-title">Edit Restaurant Owner With Full Features</h3>-->

            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                

   

    {!! Form::model($restowner,['route'=>['root.restowner.update',$restowner['0']['id']],'method'=>'patch',
       'id' => 'edit-restowner-form','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}            
        <div class="panel panel-primary theme-border">
			<div class="panel-heading theme-bg">
				<h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> {{ $resturantname['0']['f_name'].' '.$resturantname['0']['l_name'] }}</h4>
			</div>
			
            <div class="panel-body">
                <div class="row">
                    <!--<div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Restuarant list') !!}
                            <select class="form-control"  name="restuarant" id="restuarant" onchange="hideErrorMsg('restuaranterr')">
                                <option value=''>Select Restuarant</option>
                                <?php
                                foreach($resturants as $resturant) { ?>
                                    <option value="<?=$resturant['id'] ?>" <?php if($restowner['0']['rest_detail_id']==$resturant['id']) { echo 'selected'; } ?>><?=$resturant['f_name'].' '.$resturant['l_name'] ?></option>
                                <?php
                                } ?>
                            </select>
                             <span id="restuaranterr" class="text-danger"></span>
                        </div>

                    </div>-->
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Firstname') !!}<span class="red">*</span>
                            {!! Form::text('firstname',$restowner['0']['firstname'],["id"=>'firstname',"class"=>"form-control" ,"onkeypress"=>"hideErrorMsg('firstnameerr')"]) !!}
                             <span id="firstnameerr" class="text-danger"></span>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Lastname') !!}<span class="red">*</span>
                            {!! Form::text('lastname',$restowner['0']['lastname'],["id"=>'lastname',"class"=>"form-control" ,"onkeypress"=>"hideErrorMsg('lastnameerr')"]) !!}
                            <span id="lastnameerr" class="text-danger"></span>
                        </div>
                    </div>            
                  
                </div>
                
          
                 <div class="row">
                  
                    <div class="col-md-6">
                        <div class="form-group">
                             {!! Form::label('Email') !!}<span class="red">*</span>
                             {!! Form::email('email',$restowner['0']['email'],["id"=>'email',"class"=>"form-control" ,"onkeypress"=>"hideErrorMsg('emailerr')"]) !!}
                             <span id="emailerr" class="text-danger"></span>
                        </div>
                    </div>
                   <div class="col-md-6">
                        <div class="form-group">
                           {!! Form::label('Contact no') !!}<span class="red">*</span>
                           {!! Form::email('contact',$restowner['0']['contact_no'],["id"=>'contact',"class"=>"form-control" ,"onkeypress"=>"hideErrorMsg('contacterr')"]) !!}
                           <span id="contacterr" class="text-danger"></span>
                        </div>
                </div>             
                   
                </div>  
               

                <div class="row">

                <div class="col-md-6">
                  <div class="form-group">
                                    {!! Form::label('Status') !!}
                                    {!! Form::select('status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ),$restowner['0']['status'], ['id' => 'status','class' => 'form-control']) !!}
                                    <span id="statusErr" class="text-danger"></span>
                  </div>
                </div>
                <div class="col-md-6">
                <div>Licence Information<span class="red">*</span></div>		        
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="validity" value="T" id ="validity" class="val_table" 
                            <?php if($restowner['0']['validity_type']=='T'){ echo 'checked';} ?> > <span>Package period wise</span></label>

                </div>
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="validity" value="C" id ="validity" class="val_date" 
                            <?php if($restowner['0']['validity_type']=='C'){ echo 'checked';} ?> > <span>package date wise</span></label>
                </div>
                
                <div class="clearfix"></div>
                <div class="form-group" id="select_s" <?php if($restowner[0]['validity_type']=='C')  {?> style="display:none;" <?php } ?>>
                    <select class="form-control" name="validity_table" id="validity_table" 
                    onchange="hideErrorMsg('validity_tableerr')">
                    <option value=''>Select</option>    
                                          <?php
                                foreach($time_validations as $time_validation) { ?>

                                    <option value="<?=$time_validation['id'] ?>" <?php if($restowner['0']['validity_table']==$time_validation['id']) { echo 'selected'; } ?>><?=$time_validation['name'] ?></option>
                              <?php
                              } ?>
                    </select>
                     <span id="validity_tableerr" class="text-danger"></span>
                </div>
                <div class="form-group col-sm-12" id="date_s" <?php if($restowner[0]['validity_type']=='T')  {?> style="display:none;" <?php } ?>>
                    <div class="row">
                        <div class="col-sm-6">
                          <div class="input-group date " id="datetimepicker25">
                            <input type="text" class="form-control" name="valid_from" id="valid_from" value="<?php echo $restowner['0']['valid_from'];?>" placeholder="Order time from" onClick="hideErrorMsg('valid_fromerr')">
                            <span class="input-group-addon"> 
                            <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                            <span id="valid_fromerr" class="text-danger"></span>
                    </div>
                    <div class="col-sm-6">
                          <div class="input-group date" id="datetimepicker26">
                            <input type="text" class="form-control" name="valid_to" id="valid_to" value="<?php echo $restowner['0']['valid_to'];?>" placeholder="Order time to"
                            ononClick="hideErrorMsg('valid_toerr')">
                            <span class="input-group-addon"> 
                            <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                            <span id="valid_toerr" class="text-danger"></span>
                    </div>
                    </div>
                </div>          
                        
            </div>
                 
                </div>
            
                <div class="form-group margin-top">
                    {!! Form::button('Update',["id"=>'edit',"class"=>"btn  btn-primary","onclick"=>"validate_restowner()"]) !!}
                    <hr>
                    <p><span class="red">*</span> - Required Fields.</p>
                </div>
            </div>
        </div>
            {!! Form::close() !!}
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
                
    </div>
   

    @endsection


    @section('script')
       <script type="text/javascript"> 
     
function validate_restowner()
     {
       var firstname = $("#firstname").val();
        
                 if(firstname=="")
                 {

                    $("#firstname").focus();
                    $("#firstnameerr").html('Please enter first name');
                    $("#firstnameerr").show();
                    return false;
                 }
        var iChars = "~`!@#$%^&*()+=-\_[]1234567890\\\';,./{}|\":<>?";
                for (var i = 0; i < $("#firstname").val().length; i++)
                {
                if (iChars.indexOf($("#firstname").val().charAt(i)) != -1)
                    {
                        $("#firstname").focus();
                        $("#firstnameerr").html('Please enter valid first name');
                        $("#firstnameerr").show();
                        return false;
                    }
                }
        var lastname = $("#lastname").val();
        
                 if(lastname=="")
                 {

                    $("#lastname").focus();
                    $("#lastnameerr").html('Please enter valid last name');
                    $("#lastnameerr").show();
                    return false;
                 }
        var iChars = "~`!@#$%^&*()+=-\_[]1234567890\\\';,./{}|\":<>?";
                for (var i = 0; i < $("#lastname").val().length; i++)
                {
                if (iChars.indexOf($("#lastname").val().charAt(i)) != -1)
                    {
                    $("#lastname").focus();
                    $("#lastnameerr").html('Please enter last name');
                    $("#lastnameerr").show();
                    return false;
                    }
                }
        var email = $("#email").val();
        
                 if(email=="")
                 {

                    $("#email").focus();
                    $("#emailerr").html('Please enter email');
                    $("#emailerr").show();
                    return false;
                 }
                 if (!(/^[a-zA-Z0-9_äÄöÖüÜß]+([\.-]?\w+)*\@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email)))
                {
                
                    $("#email").focus();
                    $("#emailerr").html('Please enter valid email');
                    $("#emailerr").show();
                    return false;
                }
        var contact = $("#contact").val();
        
                 if(contact=="")
                 {

                    $("#contact").focus();
                    $("#contacterr").html('Please enter contact no');
                    $("#contacterr").show();
                    return false;
                 }
                 if(isNaN(contact)){
                    
                    $("#contact").focus();
                    $("#contacterr").html('Please enter digits for contact no');
                    $("#contacterr").show();
                    return false;
                 }
				 if(contact.length<7){
			
					$("#contact").focus();
					$("#contacterr").html('Please enter valid phone number!');
					$("#contacterr").show();
					return false;
				}
				if(contact.length>11){
						
						$("#phone_no").focus();
						$("#contacterr").html('Please enter valid phone number!');
						$("#contacterr").show();
						return false;
					}
       
        var validity = $("#validity:checked").val();
        
        var validity_table = $("#validity_table").val();
        var valid_from = $("#valid_from").val();
        var valid_to = $("#valid_to").val();
                 if(validity=='T')
                 {
                    if(validity_table=='')
                    {
                      $("#validity_table").focus();
                      $("#validity_tableerr").html('Please select one validity title');
                      $("#validity_tableerr").show();
                    return false;
                    }
                   
                 }
                  if(validity=='C')
                 {
                    if(valid_from=='')
                    {
                      $("#valid_from").focus();
                      $("#valid_fromerr").html('Please enter valid from');
                      $("#valid_fromerr").show();
                    return false;
                    }
                    if(valid_to=='')
                    {
                      $("#valid_to").focus();
                      $("#valid_toerr").html('Please enter valid to');
                      $("#valid_toerr").show();
                    return false;
                    }
                   
                 }
       
                 document.forms["edit-restowner-form"].submit();
     }

     function hideErrorMsg(id){
    
      $('#'+id).attr('style', 'display:none');
  
    
     }
	 	 	 $(function() {
//start datetimepickers for add categories in restaureant mngmnt module
        $('#datetimepicker25').datetimepicker({
            format: 'YYYY/MM/DD',
            useCurrent: false,
            //minDate: new Date(currentYear, currentMonth, currentDate)
        });
        $('#datetimepicker26').datetimepicker({
            format: 'YYYY/MM/DD',
            useCurrent: false,
            //minDate: new Date(currentYear, currentMonth, currentDate)
        });
        $("#datetimepicker25").on("dp.change", function (e) {
            $('#datetimepicker26').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker26").on("dp.change", function (e) {
            $('#datetimepicker25').data("DateTimePicker").maxDate(e.date);
        });
        //end datetimepickers for add categories in restaureant mngmnt module
    });
</script>
 @endsection
   
   

