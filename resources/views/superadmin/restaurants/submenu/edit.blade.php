@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sub Menu
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.submenu.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Edit Sub Menu</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->

            {{--    Error Display--}}
            @if($errors->any())
				<div class="alert alert-danger ">
					<ul class="list-unstyled">
						@foreach($errors->all() as $error)
							<li> {{ $error }}</li>
						@endforeach
					</ul>
				</div>
			 @endif
                {{--    Error Display ends--}}

                @if(Session::has('category_error'))
				<div class="alert alert-danger " style="padding: 7px 15px;">
				  {{ Session::get('category_error') }}
				  {{ Session::put('category_error','') }}
				</div>
			    @endif
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 id="contactLabel" class="panel-title"><span class="glyphicon glyphicon-plus"></span> Edit Sub Menu.</h4>
                    </div>
                    
                     <div style="padding: 5px;" class="modal-body">
                     <div class="panel-body">
                    
                     {!! Form::model($submenus,['route'=>['root.submenu.update',$submenus->id],'method'=>'patch','class' => 'form','novalidate' => 'novalidate', 'files' => true,'id'=>'submenu-form']) !!}
                     
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Restaurant Name', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      <select class="form-control js-example-basic-multiple" id="restro-id" name="restro_name" onchange="getDataSingleTable(this.value,'rest_menus','id','name','rest_detail_id','menu')">

                      <option value="">Select</option>
                     @foreach($restro_names as $restro_name)

                      @if($restro_name->id==$submenus->rest_detail_id)
                      <option value="{{$restro_name->id}}" selected>{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                      @else
                      <option value="{{$restro_name->id}}">{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                      @endif
                      @endforeach

                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Menu Name', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      <select class="form-control" name="menu_name" id="menu">

                      @foreach($menus as $menu)

                      @if($menu->id==$submenus->menu_id)
                      <option value="{{$menu->id}}" selected>{{$menu->name}}</option>
                      @else
                      <option value="{{$menu->id}}">{{$menu->name}}</option>
                      @endif
                      @endforeach
                      
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Name', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      {!! Form :: text('submenu_name',$submenus->name,['placeholder'=>'Enter Sub Menu Name','class'=>'form-control','id'=>'sub-menu'])  !!}

                    </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Short Name', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('shortname',$submenus->short_name,['placeholder'=>'Enter Short Name','class'=>'form-control','id'=>'sub-menu'])  !!}
                    </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Code', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('code',$submenus->code,['placeholder'=>'Enter Code','class'=>'form-control','id'=>'sub-menu'])  !!}
                    </div>

                  </div>
                  <div class="col-md-6">  
                    <div class="form-group">
                    {!! Form::label('', 'Image', array('class' => '"control-label')) !!}
                    
                      <input class="form-control" type="file" name="file" style="border: none; font-size: 12px; padding: 0px;">
                    </div>

                    {!! Html::image('public/uploads/superadmin/submenu/'.$submenus->image, '', array('class' => 'thumb')) !!}
                  </div>
                  
                  
                  <div class="col-md-6">
                    <div class="form-group">
                    <label class="&quot;control-label" for="">Description</label>

                    {!! Form :: textarea('description',$submenus->description,['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
                      <!-- <textarea class="form-control"> </textarea> -->
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Info', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('info',$submenus->info,['placeholder'=>'Enter Information','class'=>'form-control','id'=>'sub-menu'])  !!}
                    </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Priority', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      {!! Form :: text('priority',$submenus->priority,['placeholder'=>'Enter Priority','class'=>'form-control','id'=>'priority'])  !!}
                    </div>

                  </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Status') !!}
                            {!! Form::select('status',
                            array(
                            '1'=>'Active',
                            '0'=>'Deactive',
                            ), $submenus->status, ['id' => 'status','class' => 'form-control']) !!}
                            <span id="statusErr" style="color:red"></span>
                        </div>

                    </div>

                    <div id="have-submenu">
                        <div class="col-md-12">
                            <div class="form-group" id="price-type">

                                <span><strong>Price Type</strong></span>
                                <?php if(count($priceTypesWithValue)>0) { ?>
                                @foreach($priceTypesWithValue as $priceTypeWithValue)
                                    <input type="text" name="price_types[]" placeholder="<?php echo $priceTypeWithValue->name ?> price" value="<?php echo $priceTypeWithValue->price ?>">
                                    <input type="hidden" name="price_types_id[]" value="<?php echo $priceTypeWithValue->id ?>">
                                @endforeach
                                <?php } else { ?>

                                @foreach($priceTypes as $priceType)
                                    <input type="text" name="price_types[]" placeholder="<?php echo $priceType->name ?> price" >
                                    <input type="hidden" name="price_types_id[]" value="<?php echo $priceType->id ?>">
                                @endforeach

                                <?php } ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                if($submenus->is_alergic==1){
                                    $check='checked';
                                    $displayAttr='block';
                                }
                                else{
                                    $check='';
                                    $displayAttr='none';
                                }
                                ?>

                                <label class="checkbox">{!! Form :: checkbox('alergic','1',$check,['class'=>'alergic-check','id'=>'alergic-check','onclick'=>'showValueOnCheckbox("alergic-check","alergic-contents")'])  !!}
                                    <span><strong>Is Alergic</strong></span>
                                </label>

                                <div class="row" id="alergic-contents" style="display:<?php echo $displayAttr ?>;">
                                    @foreach($alergic_contents as $alergic_content)
                                        <div class="col-md-3">
                                            @if(in_array($alergic_content->id,$allergicMaps))
                                                <label class="checkbox">{!! Form :: checkbox('alergic_contents[]',$alergic_content->id,'checked') !!}<span>{{$alergic_content->name}}</span></label>
                                            @else
                                                <label class="checkbox">{!! Form :: checkbox('alergic_contents[]',$alergic_content->id) !!}<span>{{$alergic_content->name}}</span></label>
                                            @endif
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                if($submenus->is_spicy==1){
                                    $check='checked';
                                    $displayAttr='block';
                                }
                                else{
                                    $check='';
                                    $displayAttr='none';
                                }
                                ?>

                                <label class="checkbox">{!! Form :: checkbox('spicy','1',$check,['class'=>'spicy-check','id'=>'spicy-check','onclick'=>'showValueOnCheckbox("spicy-check","spicy-contents")'])  !!}
                                    <span><strong>Is Spicy</strong></span>
                                </label>

                                <div class="row" id="spicy-contents" style="display:<?php echo $displayAttr ?>;">
                                    @foreach($spices as $spice)
                                        <div class="col-md-3">
                                            @if(in_array($spice->id,$spiceMaps))
                                                <label class="checkbox">{!! Form :: checkbox('spicy_content[]',$spice->id,'checked') !!}<span>{{$spice->name}}</span></label>
                                            @else
                                                <label class="checkbox">{!! Form :: checkbox('spicy_content[]',$spice->id) !!}<span>{{$spice->name}}</span></label>
                                            @endif
                                        </div>
                                    @endforeach


                                </div>
                            </div>
                        </div>
                        </div>

                </div>

                <div class="form-group margin-top">
              <hr>
              <input type="submit" value="Update" name="submit" class="btn btn-primary pull-right">
                
                  <!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->
                  
                  <p><span class="red">*</span> - Required Fields.</p>
                </div>
                {!! Form :: close() !!}
                
              <!-- </form> -->
            </div>
                     
                     </div>  
                      
                    </div>
            
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
     $("#submenu-form").validate({
      rules: {


        restro_name: {
            required: true
               },
         menu_name: {
            required: true
               },
         submenu_name: {
            required: true
               },
          info: {
              required: true
          },
          'price_types[]': {
              required: true,
              number: true
          },
         priority: {
            required: true,
             number: true
            
               }
         
         
        },
       messages: {
                    restro_name: "Please select restaurant",
                    menu_name: "Please select menu name",
                    submenu_name: "Please enter submenu name",
                    info: "Please enter some information",
                    priority: "Please enter priority"
                    
                },
         
     });


         $("#restro-id").change(function(){

             var restroId=$("#restro-id").val();

             var token=$("#token-value").val();

             $.ajax({
                 url: base_url+'/ajax/gatPriceType',
                 type: "POST",
                 dataType:"json",
                 data: {'restId':restroId,'_token':token},

                 success: function (data) {

                     if(data.priceTypesItem=='' || data.priceTypesItem==null){
                         var priceType='';

                     }
                     else{
                         var priceType=data.priceTypesSpan+data.priceTypesItem;

                     }

                     $('#price-type').html(priceType);

                 },
                 error: function (jqXHR, textStatus, errorThrown) {

                 }
             });
         });

});
    </script>

   
    @endsection



