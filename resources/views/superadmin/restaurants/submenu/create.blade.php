@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sub Menu
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.submenu.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Add Sub Menu</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
			   <div class="box-body">
            {{--    Error Display--}}
             @if($errors->any())
				<div class="alert alert-danger ">
					<ul class="list-unstyled">
						@foreach($errors->all() as $error)
							<li> {{ $error }}</li>
						@endforeach
					</ul>
				</div>
			 @endif
                {{--    Error Display ends--}}

                @if(Session::has('category_error'))
				<div class="alert alert-danger " style="padding: 7px 15px;">
				  {{ Session::get('category_error') }}
				  {{ Session::put('category_error','') }}
				</div>
			    @endif
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 id="contactLabel" class="panel-title"><span class="glyphicon glyphicon-plus"></span> Add New Sub Menu.</h4>
                    </div>
                    
                     <div style="padding: 5px;" class="modal-body">
                     <div class="panel-body">
                     {!! Form::open(array('route'=>'root.submenu.store','id'=>'submenu-form','files'=>true)) !!}
                     
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Restaurant Name', array('class' => '"control-label')) !!}<span class="red">*</span>
										
											<select class="form-control js-example-basic-multiple" id="restro-id" name="restro_name" onchange="getDataSingleTable(this.value,'rest_menus','id','name','rest_detail_id','menu')">

											<option value="">Select</option>
											@foreach($restro_names as $restro_name)
											<option value="{{$restro_name->id}}">{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
											@endforeach

											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Menu Name', array('class' => '"control-label')) !!}<span class="red">*</span>
										
											<select class="form-control" name="menu_name" id="menu">

											
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Name', array('class' => '"control-label')) !!}<span class="red">*</span>
										
											{!! Form :: text('submenu_name','',['placeholder'=>'Enter Sub Menu Name','class'=>'form-control','id'=>'sub-menu'])  !!}

										</div>

									</div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Short Name', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('shortname','',['placeholder'=>'Enter Short Name','class'=>'form-control','id'=>'sub-menu'])  !!}
                    </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Code', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('code','',['placeholder'=>'Enter Code','class'=>'form-control','id'=>'sub-menu'])  !!}
                    </div>

                  </div>
									<div class="col-md-6">	
										<div class="form-group">
										{!! Form::label('', 'Image', array('class' => '"control-label')) !!}
										
											<input class="form-control" type="file" name="file" style="border: none; font-size: 12px; padding: 0px;">
										</div>
									</div>
									
									
									<div class="col-md-6">
										<div class="form-group">
										<label class="&quot;control-label" for="">Description</label>

										{!! Form :: textarea('description','',['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
											<!-- <textarea class="form-control"> </textarea> -->
										</div>
									</div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Info', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('info','',['placeholder'=>'Enter Information','class'=>'form-control','id'=>'sub-menu'])  !!}
                    </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Priority', array('class' => '"control-label')) !!}<span class="red">*</span>
                    
                      {!! Form :: text('priority','',['placeholder'=>'Enter Priority','class'=>'form-control','id'=>'priority'])  !!}
                    </div>

                  </div>

                                    <div id="have-submenu">
                                        <div class="col-md-12">
                                            <div class="form-group" id="price-type">




                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">


                                                <label class="checkbox">{!! Form :: checkbox('alergic',1,'',['class'=>'alergic-check','id'=>'alergic-check','onclick'=>'showValueOnCheckbox("alergic-check","alergic-contents")'])  !!}
                                                    <span><strong>Is Alergic</strong></span>
                                                </label>

                                                <div class="row" id="alergic-contents" style="display:none;">
                                                    @foreach($alergic_contents as $alergic_content)
                                                        <div class="col-md-3">
                                                            <label class="checkbox">{!! Form :: checkbox('alergic_contents[]',$alergic_content->id) !!}<span>{{$alergic_content->name}}</span></label>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">


                                                <label class="checkbox">{!! Form :: checkbox('spicy',1,'',['class'=>'spicy-check','id'=>'spicy-check','onclick'=>'showValueOnCheckbox("spicy-check","spicy-contents")'])  !!}
                                                    <span><strong>Is Spicy</strong></span>
                                                </label>

                                                <div class="row" id="spicy-contents" style="display:none;">
                                                    @foreach($spices as $spice)
                                                        <div class="col-md-3">
                                                            <label class="checkbox">{!! Form :: checkbox('spicy_content[]',$spice->id) !!}<span>{{$spice->name}}</span></label>
                                                        </div>
                                                    @endforeach


                                                </div>
                                            </div>
                                        </div>
                                    </div>

								</div>

								<div class="form-group margin-top">
							<hr>
							<input type="submit" value="Add" name="submit" class="btn btn-primary pull-right" onclick='checkExistingValue("cat-name","restro-id","rest_categories","name","rest_detail_id","cat-exist-error","This category is already exist for this restaurant","")'>
								
									<!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->
									
									<p><span class="red">*</span> - Required Fields.</p>
								</div>
								{!! Form :: close() !!}
								
							<!-- </form> -->
						</div>
                     
                     </div>  
                      
                    </div>
			</div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
     $("#submenu-form").validate({
      rules: {


        restro_name: {
            required: true
               },
         menu_name: {
            required: true
               },
         submenu_name: {
            required: true
               },
          info: {
              required: true
          },
          'price_types[]': {
              required: true,
              number: true
          },
         priority: {
            required: true,
             number: true
            
               }
         
         
        },
       messages: {
                    restro_name: "Please select restaurant",
                    menu_name: "Please select menu name",
                    submenu_name: "Please enter submenu name",
                    info: "Please enter some information",
                    priority: "Please enter priority"
                    
                },
         
     });


         $("#restro-id").change(function(){

             var restroId=$("#restro-id").val();

             var token=$("#token-value").val();

             $.ajax({
                 url: base_url+'/ajax/gatPriceType',
                 type: "POST",
                 dataType:"json",
                 data: {'restId':restroId,'_token':token},

                 success: function (data) {

                     if(data.priceTypesItem=='' || data.priceTypesItem==null){
                         var priceType='';

                     }
                     else{
                         var priceType=data.priceTypesSpan+data.priceTypesItem;

                     }

                     $('#price-type').html(priceType);

                 },
                 error: function (jqXHR, textStatus, errorThrown) {

                 }
             });
         });

});
    </script>

   
    @endsection



