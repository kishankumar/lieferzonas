<?php
/**
 * Created by PhpStorm.
 * User: Aras
 * Date: 13.11.2016
 * Time: 20:26
 */
?>

<div id="sub-menu-extra-modal-{{$sub_menu_id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Extras auswählen</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    @foreach (\App\superadmin\ExtraChoice::getBySubMenu($sub_menu_id) as $extra_choice)
                        <div class="col-sm-12">
                            <p>{{$extra_choice->name}}</p>
                            @if ($extra_choice->type === "single-choice")
                                <select class="form-control extra-single-choice" data-extra="{{$extra_choice->id}}">
                                    @foreach (\App\superadmin\ExtraChoiceElement::getByExtraChoice($extra_choice->id) as $element)
                                        <option value="{{$element->id}}">{{$element->name}}</option>
                                    @endforeach
                                </select>
                            @else
                                @foreach (\App\superadmin\ExtraChoiceElement::getByExtraChoice($extra_choice->id) as $element)
                                    <div class="checkbox">
                                        <label><input type="checkbox" class="extra-multiple-choice" data-extra="{{$extra_choice->id}}" value="{{$element->id}}">{{$element->name}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Abbrechen</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="addItemToCart('{{$sub_menu_id}}')">Zum Warenkorb hinzufügen</button>
            </div>
        </div>
    </div>
</div>
