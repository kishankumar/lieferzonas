@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Restaurant List
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restaurant.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Restaurant</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Restaurant List With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/restaurant/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search using name"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/restaurant/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;"> <div class="btn-group">
                     <a class="btn btn-default" href="{{route('root.restaurant.create')}}"><span>Add</span></a>

                     <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                     <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="downloadRestaurant('resturant-details','restaurants_val')"><span>Download</span></a>

                     <a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable" onclick="deleteRestaurant('resturant-details','restaurants_val')"><span>Delete</span></a></div></div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>

            <div class="clearfix" style="margin: 20px 0;"></div>
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th> 
                            <a href="{{url('root/restaurant/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Restaurant <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/restaurant/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='addr' ? 'addr-desc' :'addr';?>">
                            Address <?php if($type=='addr'){ echo $class; } elseif($type=='addr-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Email Id</th>
                        <th>Mobile</th>
                        <th>Status</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
               
              
                 
                  @foreach($restro_details as $restro_detail)
                                    <tr>
                                        <td><div class="checkbox icheck">
                                        <label>
                                          <input type="checkbox" name="restaurants_val" value="{{$restro_detail->id}}" class="resturant-details checkBoxClass"> <span> </span>
                                        </label>
                                      </div></td>
                                      
                                    <td>
                                        <a href="{{url('root/restaurant/'.$restro_detail->id)}}">
                                            {{ ucfirst($restro_detail->f_name).' '.ucfirst($restro_detail->l_name)  }}
                                        </a>
                                    </td>
                                        <td>{{$restro_detail->add1}}</td>
                                        <td>  
                                        @foreach($restro_detail->emails as $email)
                                        @if($email->is_primary==1)
                                          {{$email->email}}
                                            @endif
                                        @endforeach
                                        </td>
                                        <td>  
                                        @foreach($restro_detail->mobiles as $mobile)
                                        @if($mobile->is_primary==1)
                                          {{$mobile->mobile}}
                                            @endif
                                        @endforeach
                                        </td>
                                        
                                        <td>@if($restro_detail->status=='1')
                                                Active
                                            @elseif($restro_detail->status=='0')
                                                Inactive
                                            @endif
                                        </td>
                                       <td>{!! Html::link('#','Manage Settings',array('id' => 'restro-setting','onclick'=>'show_modal("add-form",'.$restro_detail->id.')'), true) !!}

                                       		</td>

                                          <td>
                                          <a href="{{URL :: asset('root/restaurant/'.$restro_detail->id)}}/edit" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                          </td>

                                    </tr>
                                @endforeach
              
               
              
                </tbody>
                
              </table>
                {!! $restro_details->appends(Request::except('page'))->render() !!}
               		
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
	
<!-- ./wrapper -->

<!-- Set new form for each new button -->
{!! Form::open(array('route'=>'root.restaurant.create','id'=>'set-new-form','method'=>'GET')) !!}

{!! Form :: hidden('restaurant','',['id'=>'restro-detail-id'])  !!}

{!! Form::close() !!}







<div class="modal fade" id="add-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                    Manage
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    </div>
					<div class="panel-body">
                    <button onclick="setForm('{{url('root/kitchenmap/create')}}')" class="btn btn-primary">Manage Kitchen</button>
                    <button onclick="setForm('{{url('root/rootcatmap/create')}}')" class="btn btn-primary">Manage Root Category</button>
					</div>
                    </div>
                </div>
            </div>



<script>
    function show_modal(id,restro_id) {
        //alert(restro_id);
        $("#restro-detail-id").val(restro_id);
        $("#"+id).modal('show');

    }

    function setForm(pageName) {

        $("#set-new-form").attr("action", pageName);
        $("#set-new-form").submit();

    }

    function deleteRestaurant(classname,inputName){
       // var len = $("."+classname+":checked").length;
        //alert(len);
        var checked=$('.'+classname).is(':checked');
        var token=$("#token-value").val();

        if(checked==false){
            alert('Please select atleast one value to delete');
            return false;
        }

        var r = confirm("Are you sure to delete ?");
        if (r == true) {
            var selectedItems = new Array();
            $('input:checkbox[name="'+inputName+'"]:checked').each(function(){
                selectedItems.push($(this).val());
            });

            $.ajax({

                url: base_url+'/ajax/deleteRestaurants',
                type: "POST",
                dataType:"json",
                data: {'selectedItems':selectedItems, "_token":token},
                success: function(res){

                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('Record not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
	/*added by Rajlakshmi(31-05-16)*/
	function downloadRestaurant(classname,inputName){
       // var len = $("."+classname+":checked").length;
        //alert(len);
        var checked=$('.'+classname).is(':checked');
        var token=$("#token-value").val();

        if(checked==false){
            alert('Please select atleast one value to download');
            return false;
        }

        var r = confirm("Are you sure to download ?");
        if (r == true) {
            var selectedItems = new Array();
            $('input:checkbox[name="'+inputName+'"]:checked').each(function(){
                selectedItems.push($(this).val());
            });

            $.ajax({

                url: 'restaurant/download',
                type: "POST",
                dataType:"json",
                data: {'selectedItems':selectedItems, "_token":token},
               success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);

				doc.save('restaurant_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
    }
    
/*added by Rajlakshmi(31-05-16)*/
</script>

@endsection
