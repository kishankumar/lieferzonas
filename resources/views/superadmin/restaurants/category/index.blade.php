@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.category.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
            </div>
            <div class="col-md-8 form-group">
            {!! Form::open(array('route'=>'root.category.index','class'=>'form-horizontal','method'=>'GET')) !!}
            
               
                 <label class="col-sm-2 control-label">Restaurant Name</label>
                 <div class="col-sm-4">
               
                   <select class="form-control js-example-basic-multiple" name="restaurant">
                   <option value="">Select</option>

                   @foreach($restro_names as $restro_name)

                   @if($restro_name->id==$restro_detail_id)
                   <option value="{{$restro_name->id}}" selected >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @else
                   <option value="{{$restro_name->id}}" >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @endif
                   @endforeach
                   </select>

                 </div>
               
                 <div class="col-sm-4">
                   <button class="btn btn-primary" type="submit">Get Details</button>
                   <a class="btn btn-primary" href="{{url('root/category/')}}">Reset Search</a>
                 </div>
                
               
               
               {!! Form::close() !!}
               </div>
            <!-- /.box-header -->
            

             <div class="col-sm-4 text-right" style="margin-bottom:10px;"> <div class="btn-group"><a class="btn btn-default" href="{{route('root.category.create')}}"><span>Add</span></a><a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
			 <a class="btn btn-default DTTT_button_print" onclick="download_cat();" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>

             <a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable" onclick="delete_items('categories_class','categories','rest_categories')"><span>Delete</span></a></div></div>


            <div class="clearfix" style="margin: 20px 0;"></div>
            <div class="box-body">
			@if(Session::has('message'))
				<div class="alert alert-success" style="padding: 7px 15px;">
				  {{ Session::get('message') }}
				  {{ Session::put('message','') }}
				</div>
			@endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                  <th>Restaurant</th>
                  <th>Root Category</th>
                  <th>Category</th>
                  <th>Valid From</th>
                  <th>Valid To</th>
                   <th>Status</th>
                   <th>Action</th>
                  
                  
                </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
              @foreach($getRestroCategories as $getRestroCategory)
              <tr>
                <td><div class="checkbox icheck">
                  <label>
                   <!--  <input type="checkbox"> <span> </span> -->
                    {!! Form::checkbox('categories', $getRestroCategory->id, null, ['id'=>'category_id','class' => 'categories_class checkBoxClass']) !!}<span></span>
                  </label>
                </div></td>
                <td>{{$getRestroCategory->getResturant->f_name}} {{$getRestroCategory->getResturant->l_name}}</td>
                <td>@if($getRestroCategory->getRootCategory){{$getRestroCategory->getRootCategory->name}}@else {{''}}@endif</td>
                <td>{{$getRestroCategory->name}}</td>
                <td>{{date("d M Y",strtotime($getRestroCategory->valid_from))}}</td>
                <td>{{date("d M Y",strtotime($getRestroCategory->valid_to))}}</td>

                  <td>@if($getRestroCategory->status=='1')
                          Active
                      @elseif($getRestroCategory->status=='0')
                          Inactive
                      @endif
                  </td>

                <td>
                <a href="{{URL :: asset('root/category/'.$getRestroCategory->id)}}/edit" class="btn btn-success"><i class="fa fa-edit"></i></a>
                </td>
              </tr>
              <?php $i++; ?>
              @endforeach
              
                </tbody>
                
              </table>
              {!! $getRestroCategories->appends(['restaurant'=>$restro_detail_id])->render() !!}
               		
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->

@endsection
@section('script')
<script>
/*added by Rajlakshmi(31-05-16)*/
		function download_cat(){
            var len = $("#category_id:checked").length;
			//alert(len)
            if(len==0){
                alert('Please select atleast one Category');
                return false;
            }

            var r = confirm("Are you sure to download the selected Category.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="categories"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: '{{url('root/category/download')}}',
                    type: "post",
                    dataType:"json",
                    data: {'selectedCategories':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("category_list.pdf");
				}
            });
        } else {
            return false;
        }
	   }
	 
/*added by Rajlakshmi(31-05-16)*/
</script>
@endsection

