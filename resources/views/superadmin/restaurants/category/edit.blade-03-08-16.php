@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restaurant.index')}}"><i class="fa fa-dashboard"></i> Restaurant Managment</a></li>
        <li class="active">Add Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->

            {{--    Error Display--}}
            @if($errors->any())
                <ul class="alert">
                    @foreach($errors->all() as $error)
                        <li style="color:red;"> <b>{{ $error }}</b></li>
                    @endforeach
                </ul>
                @endif
                {{--    Error Display ends--}}

                 @if(Session::has('category_error'))
                   <li style="color:red;"> {{ Session::get('category_error') }}</li>
                    
                    {{ Session::put('category_error','') }}
                @endif
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 id="contactLabel" class="panel-title"><span class="glyphicon glyphicon-plus"></span> Edit Category.</h4>
                    </div>
                    
                     <div style="padding: 5px;" class="modal-body">
                     <div class="panel-body">
                     
                     {!! Form::model($categories,['route'=>['root.category.update',$categories->id],'method'=>'patch','class' => 'form','novalidate' => 'novalidate', 'files' => true,'id'=>'category-form']) !!}
                     
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Restaurant Name', array('class' => '"control-label')) !!}
										<span class="red">*</span>
											<select class="form-control" id="restro-id" name="restro_name" onchange="getData(this.value,'rest_root_cat_maps','root_categories','root-category','root_cat_id','name','rest_detail_id','id')">

											<option value="">Select</option>
											@foreach($restro_names as $restro_name)

                      @if($restro_name->id==$categories->rest_detail_id)
                      <option value="{{$restro_name->id}}" selected>{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                      @else
											<option value="{{$restro_name->id}}">{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                      @endif
											@endforeach

											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Root Category Name', array('class' => '"control-label')) !!}
										<span class="red">*</span>
											<select class="form-control" name="root_cat_name" id="root-category">
                      @foreach($rootCategories as $rootCategory)

                      @if($rootCategory->getRootCategory->id==$categories->root_cat_id)
                      <option value="{{$rootCategory->getRootCategory->id}}" selected>{{$rootCategory->getRootCategory->name}}</option>
                      @else
                      <option value="{{$rootCategory->getRootCategory->id}}">{{$rootCategory->getRootCategory->name}}</option>
                      @endif
                      @endforeach
											
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Name', array('class' => '"control-label')) !!}
										<span class="red">*</span>
											{!! Form :: text('cat_name',$categories->name,['placeholder'=>'Enter Category Name','class'=>'form-control','id'=>'cat-name','onchange'=>'checkExistingValue("cat-name","restro-id","rest_categories","name","rest_detail_id","cat-exist-error","This category is already exist for this restaurant",'.$categories->id.')'])  !!}

											<span id="cat-exist-error" style="color:red;"></span>
										</div>
									</div>
                                                                        
									<div class="col-md-6">	
                                                                            <div class="form-group">
										{!! Form::label('', 'Image', array('class' => '"control-label')) !!}
										<span class="red">*</span>
                                                                                <input class="form-control" type="file" name="file" style="border: none; font-size: 12px; padding: 0px;"></input>
                                                                            </div>
                                                                            @if ($categories->image)
                                                                                <img class="img-upload" src="{{asset('public/uploads/superadmin/category/'.$categories->image) }}" style="max-width: 25%">
                                                                            @endif
									</div>
                                                                    
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Valid From', array('class' => '"control-label')) !!}
                                        <span class="red">*</span>
                    <div class="input-group date" id="datetimepicker8">
                    {!! Form :: text('valid_from',$categories->valid_from,['placeholder'=>'Enter from date','class'=>'form-control','id'=>'valid-from'])  !!}
                   
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
										
											<!-- <input type="text" value="" name="valid_from" id="valid-from" class="form-control" placeholder="Enter First Name"> -->
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('valid_to', 'Valid To', array('class' => '"control-label')) !!}
										 <span class="red">*</span>
										<div class="input-group date" id="datetimepicker7">
                    {!! Form :: text('valid_to',$categories->valid_to,['placeholder'=>'Enter to date','class'=>'form-control','id'=>'valid-to'])  !!}
                    
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
											<!-- <input type="text" value="" name="valid_to" id="valid-to" class="form-control" placeholder="Enter First Name"> -->
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										<label class="&quot;control-label" for="">Description</label>

										{!! Form :: textarea('description',$categories->description,['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
											<!-- <textarea class="form-control"> </textarea> -->
										</div>
									</div>

                                    <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('Status') !!}
                                        {!! Form::select('status',
                                        array(
                                        '1'=>'Active',
                                        '0'=>'Deactive',
                                        ), $categories->status, ['id' => 'status','class' => 'form-control']) !!}
                                        <span id="statusErr" style="color:red"></span>
                                    </div>
                                        </div>

								</div>

								<div class="form-group margin-top">
							<hr>
							<input type="submit" value="Update" name="submit" class="btn btn-primary pull-right" onclick='checkExistingValue("cat-name","restro-id","rest_categories","name","rest_detail_id","cat-exist-error","This category is already exist for this restaurant",<?php echo $categories->id ?>)'>
								
									<!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->
									
									<p><span class="red">*</span> - Required Fields.</p>
								</div>
								{!! Form :: close() !!}
								
							<!-- </form> -->
						</div>
                     
                     </div>  
                      
                    </div>
            
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
     $("#category-form").validate({
      rules: {


        restro_name: {
            required: true
               },
        root_cat_name: {
            required: true
               },        
         cat_name: {
            required: true
               },
         valid_from: {
            required: true
               },
         valid_to: {
            required: true
            
               }
         
         
        },
       messages: {
                    restro_name: "Please select restaurant",
                    root_cat_name:"Please select root category",
                    cat_name: "Please enter category name",
                    valid_from: "Please enter valid from date",
                    valid_to: "Please enter valid to date"
                    
                },
         
     });
});
    </script>

    <script type="text/javascript">
   /* $(function() {
      $('#datetimepicker7').datetimepicker({
        format: 'YYYY/MM/DD',
        useCurrent: false
      });

      $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
      });
      $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
      });
    });*/

    
  </script>
    @endsection



