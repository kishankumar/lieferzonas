@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.category.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Add Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->

            {{--    Error Display--}}
            @if($errors->any())
				<div class="alert alert-danger ">
					<ul class="list-unstyled">
						@foreach($errors->all() as $error)
							<li> {{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
                {{--    Error Display ends--}}

                @if(Session::has('category_error'))
				<div class="alert alert-danger " style="padding: 7px 15px;">
				  {{ Session::get('category_error') }}
				  {{ Session::put('category_error','') }}
				</div>
			    @endif
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 id="contactLabel" class="panel-title"><span class="glyphicon glyphicon-plus"></span> Add New Category.</h4>
                    </div>
                    
                     <div style="padding: 5px;" class="modal-body">
                     <div class="panel-body">
                     {!! Form::open(array('route'=>'root.category.store','id'=>'category-form','files'=>true)) !!}
                     
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Restaurant Name', array('class' => '"control-label')) !!}<span class="red">*</span>
										
											<select class="form-control js-example-basic-multiple" id="restro-id" name="restro_name" onchange="getData(this.value,'rest_root_cat_maps','root_categories','root-category','root_cat_id','name','rest_detail_id','id')">

											<option value="">Select</option>
											@foreach($restro_names as $restro_name)
											<option value="{{$restro_name->id}}">{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
											@endforeach

											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Root Category Name', array('class' => '"control-label')) !!}<span class="red">*</span>
										
											<select class="form-control" name="root_cat_name" id="root-category">

											
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Name', array('class' => '"control-label')) !!}<span class="red">*</span>
										
											{!! Form :: text('cat_name','',['placeholder'=>'Enter Category Name','class'=>'form-control','id'=>'cat-name','onchange'=>'checkExistingValue("cat-name","restro-id","rest_categories","name","rest_detail_id","cat-exist-error","This category is already exist for this restaurant","")'])  !!}

											<span id="cat-exist-error" style="color:red;"></span>
										</div>

									</div>
									<div class="col-md-6">	
										<div class="form-group">
										{!! Form::label('', 'Image', array('class' => '"control-label')) !!}<span class="red">*</span>
										
											<input class="form-control" type="file" name="file" style="border: none; font-size: 12px; padding: 0px;"></input>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Valid From', array('class' => '"control-label')) !!}<span class="red">*</span>

                    <div class="input-group date" id="datetimepicker25">
                    {!! Form :: text('valid_from','',['placeholder'=>'Enter from date','class'=>'form-control','id'=>'valid-from'])  !!}
                    
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
										
											<!-- <input type="text" value="" name="valid_from" id="valid-from" class="form-control" placeholder="Enter First Name"> -->
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('valid_to', 'Valid To', array('class' => '"control-label')) !!}<span class="red">*</span>
										<div class="input-group date" id="datetimepicker26">
                    {!! Form :: text('valid_to','',['placeholder'=>'Enter to date','class'=>'form-control','id'=>'valid-to'])  !!}
                    
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
											<!-- <input type="text" value="" name="valid_to" id="valid-to" class="form-control" placeholder="Enter First Name"> -->
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										<label class="&quot;control-label" for="">Description</label>

										{!! Form :: textarea('description','',['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
											<!-- <textarea class="form-control"> </textarea> -->
										</div>
									</div>

								</div>

								<div class="form-group margin-top">
							<hr>
							<input type="submit" value="Add" name="submit" class="btn btn-primary pull-right" onclick='checkExistingValue("cat-name","restro-id","rest_categories","name","rest_detail_id","cat-exist-error","This category is already exist for this restaurant","")'>
								
									<!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->
									
									<p><span class="red">*</span> - Required Fields.</p>
								</div>
								{!! Form :: close() !!}
								
							<!-- </form> -->
						</div>
                     
                     </div>  
                      
                    </div>
            
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
     $("#category-form").validate({
      rules: {


        restro_name: {
            required: true
               },

        root_cat_name: {
            required: true
               },       
         cat_name: {
            required: true
               },
          file: {
              required: true
              //accept: 'png|jpg'
          },
         valid_from: {
            required: true
               },
         valid_to: {
            required: true
            
               }
         
         
        },
       messages: {
                    restro_name: "Please select restaurant",
                    root_cat_name:"Please select root category",
                    cat_name: "Please enter category name",
                    file: "Please select an image",
                    valid_from: "Please enter valid from date",
                    valid_to: "Please enter valid to date"
                    
                },
         
     });
});
    $(function() {
        //start datetimepickers for add categories in restaureant mngmnt module
        var date = new Date();
        var currentMonth = date.getMonth();
        var currentDate = date.getDate();
        var currentYear = date.getFullYear();
        $('#datetimepicker25').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false,
            minDate: new Date(currentYear, currentMonth, currentDate)
        });
        $('#datetimepicker26').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false,
            minDate: new Date(currentYear, currentMonth, currentDate)
        });
        $("#datetimepicker25").on("dp.change", function (e) {
            $('#datetimepicker26').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker26").on("dp.change", function (e) {
            $('#datetimepicker25').data("DateTimePicker").maxDate(e.date);
        });
        //end datetimepickers for add categories in restaureant mngmnt module
    });
  </script>
    @endsection



