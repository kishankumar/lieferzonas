@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurant
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('root.restaurant.index')}}"><i class="fa fa-dashboard"></i> Restaurant
                        Management</a></li>
                <li class="active">Edit Restaurant</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">


                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="box">
                        <div class="box-header">

                        </div>
                        <!-- /.box-header -->
                        @if(Session::has('message'))
                            <div style="color:red;">
                                {{ Session::get('message') }}

                                {{ Session::put('message','') }}
                            </div>
                        @endif
                        {{--    Error Display--}}
                        @if($errors->any())
                            <ul class="alert">
                                @foreach($errors->all() as $error)
                                    <li style="color:red;"><b>{{ $error }}</b></li>
                                @endforeach
                            </ul>
                        @endif
                        {{--    Error Display ends--}}


                        <div class="box-body">
                            <div class="panel panel-primary theme-border">
                                <div class="panel-heading theme-bg">

                                    <h4 class="panel-title" id="contactLabel"><span
                                                class="glyphicon glyphicon-plus"></span> Edit Restaurant.</h4>
                                </div>

                                <div class="modal-body" style="padding: 5px;">
                                    <div class="panel-body">
                                    {!! Form::model($restDetails,['route'=>['root.restaurant.update',$restDetails->id],'method'=>'patch','class' => 'form','novalidate' => 'novalidate','id'=>'restaurant-form','files' => true,]) !!}

                                    <!-- <form action="#" method="post" accept-charset="utf-8"> -->

                                        <div class="row">
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('', 'Name', array('class' => '"control-label')) !!}
                                                    <span class="red">*</span>
                                                    <!-- <label class="control-label">First Name</label> -->

                                                    {!! Form :: text('f_name',$restDetails->f_name,['placeholder'=>'Enter Name','class'=>'form-control','id'=>'f_name'])  !!}
                                                </div>


                                                <div class="form-group">
                                                    <a href="javascript:void(0)" id="field-add-div"
                                                       onclick='fieldAdd("restro-email-1","restro-parent-email",["<label class=\"control-label\"><i class=\"fa fa-envelope\"></i> Email</label> <span class=\"red\">*</span>"],[""],"remove-restro-email-new-div")'>Add
                                                        New Email</a>
                                                </div>

                                                <div id="restro-parent-email">
                                                    <?php
                                                    $i = 0;
                                                    $j = 1;

                                                    ?>
                                                    @foreach($emails as $email)
                                                        <div id="restro-email-<?php echo $j ?>" class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                <!--  {!! Form::label('', 'Email', array('class' => '"control-label')) !!} -->
                                                                    <label class='control-label'><i
                                                                                class='fa fa-envelope'></i>
                                                                        Email</label> <span class='red'>*</span>

                                                                    {!! Form :: text('restro_email[]',$email->email,['placeholder'=>'Enter Email','class'=>'form-control','id'=>'restro_email'])  !!}

                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Email Type</label>
                                                                    <select id="country" class="form-control"
                                                                            name="restro_email_type[]">
                                                                        <option value="">--Select Type--</option>
                                                                        @foreach($email_types as $email_type)

                                                                            @if($email_type->id==$email->email_type)
                                                                                <option value="{{$email_type->id}}"
                                                                                        selected>{{$email_type->name}}</option>
                                                                            @else
                                                                                <option value="{{$email_type->id}}">{{$email_type->name}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           style=" text-align: right;  width: 100%;">
                                                                        @if($email->is_primary==0)
                                                                            <a href="javascript:void(0)"
                                                                               id="div-remove<?php echo $j ?>"
                                                                               onclick="remove_div('restro-email-<?php echo $j ?>')"
                                                                               class="text-danger new-tag">Remove <i
                                                                                        class="fa fa-close"></i></a>
                                                                        @endif
                                                                        <a href="javascript:void(0)"
                                                                           onclick="remove_div('remove-restro-email-new-div')"
                                                                           class="text-danger" style="display:none;">Remove
                                                                            <i class="fa fa-close"></i></a></label>
                                                                    <label class="radiobox">
                                                                        @if($email->is_primary==1)
                                                                            {!! Form::radio('is_primary_email', $j,'checked') !!}
                                                                        @else
                                                                            {!! Form::radio('is_primary_email', $j) !!}
                                                                        @endif
                                                                        <span>Is Primary</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $i++;
                                                        $j++;

                                                        ?>
                                                    @endforeach
                                                </div>
                                            {!! Form :: hidden('restro_email_count',$i,['id'=>'restro-email-1-count'])  !!}

                                            <!-- <a href="javascript:void(0)" onclick="fieldAdd('restro-email','restro-parent-email',['<label class=\'control-label\'><i class=\'fa fa-envelope\'></i> Email</label><span class=\'red\'>*</span>'],[''])">Add New Email</a> -->


                                                <div class="form-group">
                                                    <a href="javascript:void(0)"
                                                       onclick='fieldAdd("restro-mobile-1","restro-parent-mobile",["<label class=\"control-label\">Mobile</label>"],[""],"remove-restro-mobile-new-div")'>Add
                                                        New Mobile</a>
                                                </div>
                                                <div id="restro-parent-mobile">
                                                    <?php
                                                    $i = 0;
                                                    $j = 1;

                                                    ?>
                                                    @foreach($mobiles as $mobile)
                                                        <div id="restro-mobile-<?php echo $j ?>" class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <label class="control-label">Mobile</label><span
                                                                            class="red">*</span>
                                                                    {!! Form :: text('restro_mobile[]',$mobile->mobile,['placeholder'=>'Enter mobile','class'=>'form-control','id'=>'mobile'])  !!}
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Name</label>
                                                                    {!! Form :: text('restro_mobile_desig[]',$mobile->name,['placeholder'=>'Enter name','class'=>'form-control','id'=>'mobile-designation'])  !!}
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    @if($mobile->is_primary==0)
                                                                        <a href="javascript:void(0)"
                                                                           id="div-remove<?php echo $j ?>"
                                                                           onclick="remove_div('restro-mobile-<?php echo $j ?>')"
                                                                           class="text-danger new-tag">Remove <i
                                                                                    class="fa fa-close"></i></a>
                                                                    @endif
                                                                    <label class="control-label"
                                                                           style=" text-align: right;  width: 100%;"> <a
                                                                                href="javascript:void(0)"
                                                                                onclick="remove_div('remove-restro-mobile-new-div')"
                                                                                class="text-danger"
                                                                                style="display:none;">Remove <i
                                                                                    class="fa fa-close"></i></a></label>
                                                                    <label class="radiobox">
                                                                        @if($mobile->is_primary==1)
                                                                            {!! Form::radio('is_primary_mobile', $j,'checked') !!}
                                                                        @else
                                                                            {!! Form::radio('is_primary_mobile', $j) !!}
                                                                        @endif
                                                                        <span>Is Primary</span>

                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $i++;
                                                        $j++;

                                                        ?>
                                                    @endforeach
                                                </div>


                                                {!! Form :: hidden('restro_mobile_count',$i,['id'=>'restro-mobile-1-count'])  !!}

                                                <div class="form-group">
                                                    <a href="javascript:void(0)"
                                                       onclick='fieldAdd("restro-phone-1","restro-parent-phone",["<label class=\"control-label\">Phone</label>"],[""],"remove-restro-phone-new-div")'>Add
                                                        New Phone</a>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Phone</label><span class="red">*</span>
                                                            <?php if (count($landLines) > 0) {

                                                                $landline = $landLines[0]->landline;
                                                            } else {
                                                                $landline = '';
                                                            }?>

                                                            {!! Form :: text('restro_landline',$landline,['placeholder'=>'Enter Phone','class'=>'form-control','id'=>'landline'])  !!}
                                                        </div>
                                                    </div>
                                                    <div id="restro-parent-phone">
                                                        <?php
                                                        $i = 0;
                                                        $j = 1;

                                                        ?>
                                                        @foreach($landLines as $landLine)
                                                            <div id="restro-phone-<?php echo $j ?>" class="phone-clear">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label"
                                                                               style=" text-align: right;  width: 100%;">
                                                                            &nbsp;</label>

                                                                        {!! Form :: text('restro_landlie_ext[]',$landLine->extension,['placeholder'=>'Enter Extension','class'=>'form-control','id'=>'landline'])  !!}

                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group">

                                                                        <label class="control-label"
                                                                               style=" text-align: right;  width: 100%;">
                                                                            &nbsp; <a href="javascript:void(0)"
                                                                                      onclick="remove_div('remove-restro-phone-new-div')"
                                                                                      class="text-danger"
                                                                                      style="display:none;">Remove <i
                                                                                        class="fa fa-close"></i></a></label>

                                                                        {!! Form :: text('restro_landlie_desig[]',$landLine->name,['placeholder'=>'Enter Designation','class'=>'form-control','id'=>'landline'])  !!}


                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            $i++;
                                                            $j++;

                                                            ?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                {!! Form :: hidden('restro_phone_count',$i,['id'=>'restro-phone-1-count'])  !!}

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="control-label">Mindestbestellwert</label>
                                                        {!! Form::text("restro_minimum_order", $delivery_costs->minimum_order_amount, ["class"=>"form-control"]) !!}
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="control-label">darunter</label>
                                                        {!! Form::text("restro_below_minimum_order", $delivery_costs->below_order_delivery_cost, ["class"=>"form-control"]) !!}
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="control-label">Zustellkosten</label>
                                                        {!! Form::text("restro_delivery_cost", $delivery_costs->above_order_delivery_cost, ["class"=>"form-control"]) !!}
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label class="control-label"><i class="fa fa-envelope"></i> Address1</label>
                                                    <span class="red">*</span>
                                                    {!! Form :: text('add1',$restDetails->add1,['placeholder'=>'Enter Address1','class'=>'form-control','id'=>'add1'])  !!}
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label"><i class="fa fa-envelope"></i> Address2</label>
                                                    <span class="red">*</span>
                                                    {!! Form :: text('add2',$restDetails->add2,['placeholder'=>'Enter Address2','class'=>'form-control','id'=>'add2'])  !!}
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Country</label><span
                                                            class="red">*</span>
                                                    <select id="country" class="form-control js-example-basic-multiple"
                                                            name="country"
                                                            onchange="getData(this.value,'countries','states','state','id','name','id','country_id')">
                                                        <option value="">- Select -</option>
                                                        @foreach($countries as $country)
                                                            @if($country->id==$restDetails->country)
                                                                <option value="{{$country->id}}"
                                                                        selected>{{$country->name}}</option>
                                                            @else
                                                                <option value="{{$country->id}}">{{$country->name}}</option>

                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('State') !!}<span class="red">*</span>
                                                    <select id="state" name="state"
                                                            class="form-control js-example-basic-multiple"
                                                            onchange="getData(this.value,'states','cities','city','id','name','id','state_id')">
                                                        <option value="">- Select -</option>
                                                        @foreach($states as $state)
                                                            @if($state->id==$restDetails->state)
                                                                <option value="{{$state->id}}"
                                                                        selected>{{$state->name}}</option>
                                                            @else
                                                                <option value="{{$state->id}}">{{$state->name}}</option>

                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('City') !!}<span class="red">*</span>
                                                    <select id="city" name="city"
                                                            class="form-control js-example-basic-multiple"
                                                            onchange="getData(this.value,'cities','zipcodes','zipcode','id','name','id','city_id')">
                                                        <option value="">- Select -</option>
                                                        @foreach($cities as $city)
                                                            @if($city->id==$restDetails->city)
                                                                <option value="{{$city->id}}"
                                                                        selected>{{$city->name}}</option>
                                                            @else
                                                                <option value="{{$city->id}}">{{$city->name}}</option>

                                                            @endif
                                                        @endforeach

                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('zipcode') !!}<span class="red">*</span>
                                                    <select id="zipcode" name="zipcode" class="form-control">
                                                        @foreach($zipcodes as $zipcode)
                                                            @if($zipcode->id==$restDetails->pincode)
                                                                <option value="{{$zipcode->id}}"
                                                                        selected>{{$zipcode->name}}</option>
                                                            @else
                                                                <option value="{{$zipcode->id}}">{{$zipcode->name}}</option>

                                                            @endif
                                                        @endforeach

                                                    </select>
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label">Fax</label>
                                                    {!! Form :: text('fax',$restDetails->fax,['placeholder'=>'Enter Phone','class'=>'form-control','id'=>'fax'])  !!}
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label">Price Types</label><span
                                                            class="red">*</span>
                                                    @foreach($priceTypes as $priceType)
                                                        @if(in_array($priceType->id,$priceTypesMaps))
                                                            <label class="checkbox checkbox-inline mt0">
                                                                {!! Form :: checkbox('price_types[]',$priceType->id,'checked',['class'=>'price-types','id'=>'price-types'])  !!}
                                                                <span>{{$priceType->name}}</span>
                                                            </label>
                                                        @else
                                                            <label class="checkbox checkbox-inline mt0">
                                                                {!! Form :: checkbox('price_types[]',$priceType->id,'',['class'=>'price-types','id'=>'price-types'])  !!}
                                                                <span>{{$priceType->name}}</span>
                                                            </label>
                                                        @endif
                                                    @endforeach
                                                    <div class="question"></div>
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('Status') !!}
                                                    {!! Form::select('status',
                                                    array(
                                                    '1'=>'Active',
                                                    '0'=>'Deactive',
                                                    ), $restDetails->status, ['id' => 'status','class' => 'form-control']) !!}
                                                    <span id="statusErr" style="color:red"></span>
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('', 'Upload Logo', array('class' => '"control-label')) !!}
                                                    <span class="red">*</span>

                                                    <input class="form-control" type="file" name="file"
                                                           style="border: none; font-size: 12px; padding: 0px;">
                                                </div>

                                                <div style="width:250px;">
                                                    {!! Html::image('public/uploads/superadmin/restaurants/'.$restDetails->logo, '', array('class' => 'img-responsive help-block')) !!}
                                                </div>


                                            </div>

                                        </div>


                                        <div class="form-group margin-top">

                                            {!! Form :: submit("Update",["class"=>"btn btn-primary pull-right","name"=>"submit"]) !!}
                                            <div class="clearfix"></div>
                                            <!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->
                                            <hr>
                                            <p><span class="red">*</span> - Required Fields.</p>
                                        </div>
                                    {!! Form::close() !!}
                                    <!-- </form> -->
                                    </div>

                                </div>


                            </div>
                        </div>
                        <!-- /.box-body -->


                        <!-- quick email widget -->


                </section>
                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')
    <script type="text/javascript">

        $(document).ready(function () {

            $.validator.addMethod("email", function (value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter valid email address.");

            $("#restaurant-form").validate({
                rules: {


                    f_name: {
                        required: true
                    },
                    'restro_email[]': {
                        required: true,
                        email: true
                    },

                    'restro_mobile[]': {
                        required: true,
                        minlength: 10,
                        maxlength: 10
                    },
                    add1: {
                        required: true

                    },
                    add2: {
                        required: true

                    },
                    country: {
                        required: true
                    },
                    state: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    zipcode: {
                        required: true
                    },

                    'price_types[]': {
                        required: true,

                    },
                    restro_landline: {
                        required: true

                    },


                },
                messages: {
                    f_name: "Please enter restaurant first name",
                    add1: "Please enter first address",
                    add2: "Please enter second address",
                    restro_landline: "Please enter landline with their extensions",

                    country: "Please select country",
                    state: "Please select state",
                    city: "Please select city",
                    zipcode: "Please select zipcode",
                    'price_types[]': {
                        required: "You must check at least 1 box"

                    }

                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "checkbox") {
                        error.insertAfter($('.question'));
                    } else {
                        error.insertAfter(element);
                        //error.insertBefore(element);
                    }
                }

            });
        });
    </script>


    <style>
        .mt0 {
            margin-top: 0;
        }
    </style>

@endsection

