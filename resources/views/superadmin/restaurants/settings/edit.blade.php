@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Restaurant Setting
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.restsetting.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
            <li class="active">Edit Restaurant Setting</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->

                    {{--    Error Display--}}
                    @if($errors->any())
                        <ul class="alert">
                            @foreach($errors->all() as $error)
                                <li style="color:red;"> <b>{{ $error }}</b></li>
                            @endforeach
                        </ul>
                    @endif
                    {{--    Error Display ends--}}

                    @if(Session::has('category_error'))
                        <li style="color:red;"> {{ Session::get('category_error') }}</li>

                        {{ Session::put('category_error','') }}
                    @endif

                    <div class="panel panel-primary theme-border">
                        <div class="panel-heading theme-bg">

                            <h4 id="contactLabel" class="panel-title"><span class="glyphicon glyphicon-plus"></span> Edit Restaurant Setting.</h4>
                        </div>

                        <div style="padding: 5px;" class="modal-body">
                            <div class="panel-body">

                                {!! Form::model($restSettings,['route'=>['root.restsetting.update',$restSettings->id],'method'=>'patch',
                                'class' => 'form','novalidate' => 'novalidate','id'=>'setting-form']) !!}
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Restaurant', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <span>{{$restSettings->getResturant->f_name}} {{$restSettings->getResturant->l_name}}</span>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Is Open', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            @if($restSettings->is_open==1)
                                                {!! Form :: checkbox('is_open' ,'1','checked')  !!}
                                            @else
                                                {!! Form :: checkbox('is_open','1')  !!}
                                            @endif
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Is New', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            @if($restSettings->is_new==1)
                                                {!! Form :: checkbox('is_new' ,'1','checked')  !!}
                                            @else
                                                {!! Form :: checkbox('is_new','1')  !!}
                                            @endif 
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Header Color', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            @foreach($colors as $color)
                                                @if($color->color_code==$restSettings->header_color)
                                                    <span> {{$color->color}}</span> {!! Form :: radio('header_color',$color->color_code,'checked')  !!}
                                                @else
                                                    <span> {{$color->color}}</span> {!! Form :: radio('header_color',$color->color_code)  !!}
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Body Color', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            @foreach($colors as $color)
                                                @if($color->color_code==$restSettings->body_color)
                                                    <span> {{$color->color}}</span> {!! Form :: radio('body_color',$color->color_code,'checked')  !!}
                                                @else
                                                    <span> {{$color->color}}</span> {!! Form :: radio('body_color',$color->color_code)  !!}
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Privilege', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            @foreach($privileges as $privilege)
                                                @if($privilege->id==$restSettings->privilege_id)
                                           <span> {{$privilege->name}}</span> {!! Form :: radio('privilege',$privilege->id,'checked')  !!}
                                                @else
                                                    <span> {{$privilege->name}}</span> {!! Form :: radio('privilege',$privilege->id)  !!}
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Allow Admin to change Category Image', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            @if($restSettings->allow_admin==1)
                                                {!! Form :: checkbox('allow_admin' ,'1','checked')  !!}
                                            @else
                                                {!! Form :: checkbox('allow_admin','1')  !!}
                                            @endif 
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if(strtotime($restSettings->is_new_valid)>0){
                                    $is_new_valid= date("m/d/Y", strtotime($restSettings->is_new_valid));
                                }
                                else{
                                    $is_new_valid='';
                                }
                                if(strtotime($restSettings->header_color_valid)>0){
                                    $header_color_valid= date("m/d/Y", strtotime($restSettings->header_color_valid));
                                }
                                else{
                                    $header_color_valid='';
                                }
                                if(strtotime($restSettings->body_color_valid)>0){
                                    $body_color_valid= date("m/d/Y", strtotime($restSettings->body_color_valid));
                                }
                                else{
                                    $body_color_valid='';
                                }
                                if(strtotime($restSettings->privilege_valid)>0){
                                    $privilege_valid= date("m/d/Y", strtotime($restSettings->privilege_valid));
                                }
                                else{
                                    $privilege_valid='';
                                }
                                ?>
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Is New Valid', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker17">
                                                {!! Form::text('is_new_valid',$is_new_valid,["id"=>'is_new_valid',"class"=>"form-control"]) !!}
                                                <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Header Color Valid', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker18">
                                                {!! Form::text('header_color_valid',$header_color_valid,["id"=>'header_color_valid',"class"=>"form-control"]) !!}
                                                <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Body Color Valid', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker19">
                                                {!! Form::text('body_color_valid',$body_color_valid,["id"=>'body_color_valid',"class"=>"form-control"]) !!}
                                                <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('', 'Privilege Color Valid', array('class' => 'control-label')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker20">
                                                {!! Form::text('privilege_valid',$privilege_valid,["id"=>'privilege_valid',"class"=>"form-control"]) !!}
                                                <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group margin-top">
                                    <hr>
                                    <input type="hidden" name="rest_id" value="{{$restSettings->rest_detail_id}}">
                                    <input type="submit" value="Update" name="submit" class="btn btn-primary pull-right")'>
                                </div>
                                {!! Form :: close() !!}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function(){
            $("#setting-form").validate({
                rules: {
                    header_color: {
                        required: true
                    },
                    body_color: {
                        required: true
                    },
                    privilege: {
                        required: true
                    },
                    is_new_valid: {
                        //required: true
                    },
                    header_color_valid: {
                        //required: true
                    },
                    body_color_valid: {
                        //required: true
                    },
                    privilege_valid: {
                        //required: true
                    },
                },
                messages: {
                    header_color:{
                        required: "Please select header color",
                    },
                    body_color:{
                        required: "Please select body color",
                    },
                    privilege:{
                        required: "Please select privilege",
                    },
                    is_new_valid:{
                        required: "Please select Is New Valid date",
                    },
                    header_color_valid:{
                        required: "Please select Header Color Valid date",
                    },
                    body_color_valid:{
                        required: "Please select Body Color Valid date",
                    },
                    privilege_valid:{
                        required: "Please select Privilege Color Valid date",
                    },
                },
            });
        });
    </script>
@endsection



