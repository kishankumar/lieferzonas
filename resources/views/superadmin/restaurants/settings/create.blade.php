@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1>
        Restaurant Setting
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restsetting.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Add Restaurant Setting</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->

            {{--    Error Display--}}
            @if($errors->any())
				<div class="alert alert-danger ">
					<ul class="list-unstyled">
						@foreach($errors->all() as $error)
							<li> {{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
                {{--    Error Display ends--}}

                 @if(Session::has('menu_error'))
                  <div class="alert alert-danger" style="padding: 7px 15px;">
					  {{ Session::get('menu_error') }}
					  {{ Session::put('menu_error','') }}
				  </div>
                @endif
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 id="contactLabel" class="panel-title"><span class="glyphicon glyphicon-plus"></span> Add Restaurant Setting.</h4>
                    </div>
                    
                     <div style="padding: 5px;" class="modal-body">
                     <div class="panel-body">
                     {!! Form::open(array('route'=>'root.menu.store','id'=>'menu-form','files'=>true)) !!}
                     
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Restaurant Name', array('class' => '"control-label')) !!}
										
											<select class="form-control" id="restro-id" name="restro_name" onchange="getCategoryAndKitchenByRestaurant(this.value,'category','kitchen')">

											<option value="">Select</option>
											@foreach($restro_names as $restro_name)
											<option value="{{$restro_name->id}}">{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
											@endforeach

											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Category Name', array('class' => '"control-label')) !!}
										
											<select class="form-control" name="cat_name" id="category">

											
											</select>
										</div>
									</div>
                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Kitchen Name', array('class' => '"control-label')) !!}
                    
                      <select class="form-control" name="kitchen_name" id="kitchen">

                      
                      </select>
                    </div>
                  </div>
									<div class="col-md-6">
										<div class="form-group">
										{!! Form::label('', 'Name', array('class' => '"control-label')) !!}
										
											{!! Form :: text('menu_name','',['placeholder'=>'Enter Category Name','class'=>'form-control','id'=>'cat-name'])  !!}

											<span id="cat-exist-error" style="color:red;"></span>
										</div>

									</div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Shot Name', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('short_menu_name','',['placeholder'=>'Enter Short Name','class'=>'form-control','id'=>'short-name'])  !!}

                    </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('', 'Menu Code', array('class' => '"control-label')) !!}
                    
                      {!! Form :: text('menu_code','',['placeholder'=>'Enter Code','class'=>'form-control','id'=>'code'])  !!}

                    </div>

                  </div>
									<div class="col-md-6">	
										<div class="form-group">
										{!! Form::label('', 'Image', array('class' => '"control-label')) !!}
										
											<input class="form-control" type="file" name="file" style="border: none; font-size: 12px; padding: 0px;"></input>
										</div>
									</div>
									
									
									<div class="col-md-6">
										<div class="form-group">
										<label class="&quot;control-label" for="">Description</label>

										{!! Form :: textarea('description','',['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description'])  !!}
										</div>
									</div>

                  <div class="col-md-6">
                    <div class="form-group">
                    <label class="&quot;control-label" for="">Info</label>

                    {!! Form :: text('info','',['placeholder'=>'Enter Info','class'=>'form-control','id'=>'info'])  !!}
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    <label class="&quot;control-label" for="">Priority</label>

                    {!! Form :: text('priority','',['placeholder'=>'Enter Priority','class'=>'form-control','id'=>'priority'])  !!}
                    </div>
                  </div>

								</div>

								<div class="form-group margin-top">
							<hr>
							<input type="submit" value="Add" name="submit" class="btn btn-primary pull-right">
								
									<!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->
									
									<p><span class="red">*</span> - Required Fields.</p>
								</div>
								{!! Form :: close() !!}
								
							<!-- </form> -->
						</div>
                     
                     </div>  
                      
                    </div>
            
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
     $("#menu-form").validate({
      rules: {


        restro_name: {
            required: true
               },
         menu_name: {
            required: true
               },
         cat_name: {
            required: true
               },
         menu_code: {
            required: true
               },
         priority: {
            required: true

               }
         
         
        },
       messages: {
                    restro_name: "Please select restaurant",
                    menu_name: "Please enter menu name",
                    cat_name: "Please enter category name",
                    menu_code: "Please enter menu code",
                    priority: "Please enter priority"
                    
                },
         
     });
});
    </script>

    <script type="text/javascript">
      
    function getCategoryAndKitchenByRestaurant(restro_id,firstAppendId,secondAppendId){

      var token=$("#token-value").val();

      $.ajax({

        url: base_url+'/ajax/getCategoryAndKitchenByResturant',
        type: "POST",
        dataType:"json",
        data: {'restroId':restro_id,'_token':token},
        
        success: function (data) {
         
         var catgory=data.option+data.categoryItem;
         var kitchen=data.option+data.kitchenItem;

         $('#'+firstAppendId).html(catgory);
         $('#'+secondAppendId).html(kitchen);
        
        },
        error: function (jqXHR, textStatus, errorThrown) {
          
        }

      });

    }


    </script>

    
    @endsection



