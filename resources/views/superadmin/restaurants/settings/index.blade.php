@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Restaurant Setting
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restsetting.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Restaurant Setting</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <div class="col-md-8">
                       
            {!! Form::open(array('route'=>'root.restsetting.index','class'=>'form-horizontal','method'=>'GET')) !!}

            <div class="col-md-11 form-group">
              <label class="col-sm-2 control-label">Restaurant Name</label>
              <div class="col-sm-5">

                <select class="form-control js-example-basic-multiple" name="restaurant">
                  <option value="">Select</option>

                  @foreach($restro_names as $restro_name)

                    @if($restro_name->id==$restro_detail_id)
                      <option value="{{$restro_name->id}}" selected >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                    @else
                      <option value="{{$restro_name->id}}" >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                    @endif
                  @endforeach
                </select>

              </div>

              <div class="col-sm-5">
                    <button class="btn btn-primary" type="submit">Get Details</button>
                    <a class="btn btn-primary" href="{{url('root/restsetting/')}}">Reset Search</a>
              </div>
			</div>
             {!! Form::close() !!}
			 
			 </div>
            
          
           
            
            <!-- /.box-header -->
            
           <div class="col-sm-4 text-right" style="margin-bottom:10px;"> 
			 <div class="btn-group">
			 
			 <a class="btn btn-default DTTT_button_print" onclick="window.print()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
			 <a class="btn btn-default DTTT_button_print" onclick="download_restsetting()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>

           
			</div></div>

            <div class="clearfix" style="margin: 20px 0;"></div>
            <div class="box-body">
			@if(Session::has('message'))
				<div class="alert alert-success" style="padding: 7px 15px;">
				  {{ Session::get('message') }}
				  {{ Session::put('message','') }}
				</div>
			@endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                 <th>Restaurant</th>
                  <th>Open/Close</th>
                  <th>New/Not New</th>
                  <th>Header Color</th>
                  <th>Body Color</th>
                  <th>Privilege</th>
                   <th>Action</th>
                  
                  
                </tr>
                </thead>
                <tbody>
                <?php $i=1?>
                @foreach($getSettingsData as $getSettingData)

                  <td> <div class="checkbox icheck">
                       <label>{!! Form::checkbox('restsetting',$getSettingData->getResturant->id,false,['id'=>'restsetting','class'=>'restsetting checkBoxClass']) !!} <span> </span></label>
                        </div>
				  </td>
                  <td>{{$getSettingData->getResturant->f_name}} {{$getSettingData->getResturant->l_name}}</td>
                  <td>@if($getSettingData->is_open=='1')
                      Open
                    @elseif($getSettingData->is_open=='0')
                      Close
                    @endif
                  </td>
                  <td>@if($getSettingData->is_new=='1')
                      New
                    @elseif($getSettingData->is_new=='0')
                      Not New
                    @endif
                  </td>
                  <td style="color:{{$getSettingData->header_color}};"> color</td>
                  <td style="color:{{$getSettingData->body_color}};"> color</td>
                  <td>@if($getSettingData->privilege_id=='1')
                      Normal
                    @elseif($getSettingData->privilege_id=='2')
                      Featured
                    @endif
                  </td>
                  <td>
                    <a href="{{URL :: asset('root/restsetting/'.$getSettingData->id)}}/edit" class="btn btn-success"><i class="fa fa-edit"></i></a>
                  </td>

                  <?php $i++; ?>
                  @endforeach



              
                </tbody>
                
              </table>

               		
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->

@endsection
@section('script')
<script>
/*added by Rajlakshmi(31-05-16)*/
		function download_restsetting(){
            var len = $(".restsetting:checked").length;
            if(len==0){
                alert('Please select atleast one Restuarant setting');
                return false;
            }

            var r = confirm("Are you sure to download the selected Restuarant setting.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="restsetting"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: '{{url('root/restsetting/download')}}',
                    type: "post",
                    dataType:"json",
                    data: {'selectedSettings':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("restsetting_list.pdf");
				}
            });
        } else {
            return false;
        }
	   }
	 
/*added by Rajlakshmi(31-05-16)*/
</script>
@endsection