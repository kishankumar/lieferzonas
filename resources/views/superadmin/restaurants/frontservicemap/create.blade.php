@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Front Service Map
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.servicemap.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Add Front Services</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
          <div class="box-body">
            <div class="box-header">
              <!--<h3 class="box-title">Data Table With Full Features</h3>-->
            </div>
            <!-- /.box-header -->

            <!--    Error Display-->
           @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!--    Error Display-end-->

            
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add New Services.</h4>
                    </div>
                    
                     
                     <div class="panel-body">

						 {!! Form::open(array('route'=>'root.servicemap.create','class'=>'form-horizontal','method'=>'GET','id' => 'servicemap')) !!}

						 {{--<form class="form-horizontal">--}}
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label">Restaurant Name<span class="red">*</span></label>
								 <div class="col-sm-4">
                 @if($restro_detail_id)
									 <select class="form-control" disabled name="restaurant" >
                   @else

                   <select class="form-control js-example-basic-multiple" name="restaurant" id="restaurant" onChange="hideErrorMsg('restauranterr');">

                   @endif
                   <option value="">Select</option>

                   @foreach($restro_names as $restro_name)

                   @if($restro_name->id==$restro_detail_id)
                   <option value="{{$restro_name->id}}" selected >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @else
                   <option value="{{$restro_name->id}}" >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @endif
                   @endforeach
                   </select>

                       <span id="restauranterr" class="text-danger"></span>
								 </div>
                 @if(!$restro_detail_id)
								 <div class="col-sm-4">
							 {!!Form::button('Get Services',["id"=>'add',"class"=>"btn  btn-primary","onclick"=>"validate_rest()"]) !!}

									
								 </div>
                 @endif
							 </div>
               <div class="clearfix"></div>
               {!! Form::close() !!}
							 
               @if($restro_detail_id)
               <div>
                {!! Form::open(array('route'=>'root.servicemap.store','class'=>'form-horizontal', 'id'=> 'serviceform')) !!}
							 <hr>
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label">&nbsp;</label>
								 <div class="col-sm-9">

                   @foreach($services as $service)
				  <label class="checkbox checkbox-inline">
                   @if(in_array($service->id,$service_maps))
                   <input type="checkbox" name="service[]" value="{{$service->id}}" checked >
                   @else
                   <input type="checkbox" name="service[]" value="{{$service->id}}" onclick="hideErrorMsg('serviceerr')" >
                   @endif
                   
                   <span>{{$service->name}} </span>
                     <a href="#" data-toggle="tooltip" data-placement="bottom" title="{{$service->valid_from}} To {{$service->valid_to}}">
                      <i class="fa fa-question-circle"></i> </a>
                   </label>
                   @endforeach
                  
                   {!! Form :: hidden('restaurant',$restro_detail_id,['id'=>'restro-detail-id'])  !!}

								 </div>
					
							 </div>
				     <span id="serviceerr" class="text-danger" ></span>
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label"></label>
								 <div class="col-sm-4">
								  {!! Form::button('Submit',["id"=>'edit',"class"=>"btn  btn-primary","onclick"=>"validate_service()"]) !!}
                                                                  <a class="btn btn-primary" href="{{url('root/servicemap/create/')}}">Cancel</a>
								 </div>
							 </div>
						  {!! Form::close() !!}
              </div>
              @endif
							<!-- </form> -->
						</div>
                     
                    
                      
                    </div>
            <!-- /.box-body -->
          </div>
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('script')
 <script type="text/javascript"> 
 function validate_rest()
 {
	 $restaurant = $('#restaurant').val();
	 if($restaurant=='')
	 {
		 $("#restaurant").focus();
         $("#restauranterr").html('Please select a restaurant');
         $("#restauranterr").show();
         return false;
	 }
	 document.forms["servicemap"].submit();
 }
 
 function validate_service()
 {
	 var service = document.getElementsByName('service[]');
	
	 var haschecked= false;
	 for(var i=0; i < service.length; i++)
	 {
		 //alert(service[i].checked);
		 if(service[i].checked)
		 {
			 haschecked = true;
			 document.forms["serviceform"].submit();
		 }
	 }
	 if(haschecked == false)
	 {
		 $("#serviceerr").html('Please check atleast one service');
         $("#serviceerr").show();
		
	 }
	 
 }
 
  function hideErrorMsg(id){

    
      $('#'+id).attr('style', 'display:none');
  
    
     }

 </script>
@endsection
