@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurants
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('root.restaurant.index')}}"><i class="fa fa-dashboard"></i> Restaurant
                        Management</a></li>
                <li class="active">Add Restaurant</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">


                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="box">
                        <div class="box-header">
                        </div>
                        <div class="panel-body">
                            <!-- /.box-header -->
                            @if(Session::has('message'))
                                <div style="color:red;">
                                    {{ Session::get('message') }}

                                    {{ Session::put('message','') }}
                                </div>
                            @endif
                            {{--    Error Display--}}
                            @if($errors->any())
                                <ul class="alert">
                                    @foreach($errors->all() as $error)
                                        <li style="color:red;"><b>{{ $error }}</b></li>
                                    @endforeach
                                </ul>
                            @endif
                            {{--    Error Display ends--}}


                            <div class="panel panel-primary theme-border">
                                <div class="panel-heading theme-bg">

                                    <h4 class="panel-title" id="contactLabel"><span
                                                class="glyphicon glyphicon-plus"></span> Add New Restaurant</h4>
                                </div>

                                <div class="panel-body">
                                {!! Form::open(array('route'=>'root.restaurant.store','id'=>'restaurant-form','files'=>true)) !!}
                                <!-- <form action="#" method="post" accept-charset="utf-8"> -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('', 'Name', array('class' => '"control-label')) !!}<span
                                                        class="red">*</span>
                                                <!-- <label class="control-label">First Name</label> -->
                                                {!! Form :: text('f_name','',['placeholder'=>'Enter Name','class'=>'form-control','id'=>'f_name'])  !!}
                                            </div>


                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                <a href="javascript:void(0)"
                                                   onclick='fieldAdd("restro-email-1","restro-parent-email",["<label class=\"control-label\"><i class=\"fa fa-envelope\"></i> Email</label> <span class=\"red\">*</span>"],[""],"remove-restro-email-new-div")'>Add
                                                    New Email</a>
                                            </div>

                                            <div id="restro-parent-email">
                                                <div id="restro-email-1" class="row">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                        <!--  {!! Form::label('', 'Email', array('class' => '"control-label')) !!} -->
                                                            <label class='control-label'><i class='fa fa-envelope'></i>
                                                                Email</label> <span class='red'>*</span>

                                                            {!! Form :: text('restro_email[]','',['placeholder'=>'Enter Email','class'=>'form-control','id'=>'restro_email'])  !!}

                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Email Type</label>
                                                            <select id="country" class="form-control"
                                                                    name="restro_email_type[]">
                                                                <option value="">--Select Type--</option>
                                                                @foreach($email_types as $email_type)

                                                                    <option value="{{$email_type->id}}">{{$email_type->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label"
                                                                   style=" text-align: right;  width: 100%;"> &nbsp; <a
                                                                        href="javascript:void(0)"
                                                                        onclick="remove_div('remove-restro-email-new-div')"
                                                                        class="text-danger" style="display:none;">Remove
                                                                    <i class="fa fa-close"></i></a></label>
                                                            <label class="radiobox">
                                                                {!! Form::radio('is_primary_email', '1') !!}
                                                                <span>Is Primary </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {!! Form :: hidden('restro_email_count','1',['id'=>'restro-email-1-count'])  !!}

                                        <!-- <a href="javascript:void(0)" onclick="fieldAdd('restro-email','restro-parent-email',['<label class=\'control-label\'><i class=\'fa fa-envelope\'></i> Email</label><span class=\'red\'>*</span>'],[''])">Add New Email</a> -->


                                            <div class="form-group">
                                                <a href="javascript:void(0)"
                                                   onclick='fieldAdd("restro-mobile-1","restro-parent-mobile",["<label class=\"control-label\">Mobile</label>"],[""],"remove-restro-mobile-new-div")'>Add
                                                    New Mobile</a>
                                            </div>
                                            <div id="restro-parent-mobile">
                                                <div id="restro-mobile-1" class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Mobile</label><span
                                                                    class="red">*</span>
                                                            {!! Form :: text('restro_mobile[]','',['placeholder'=>'Enter mobile','class'=>'form-control','id'=>'mobile'])  !!}
                                                        </div>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="control-label">Name</label>
                                                            {!! Form :: text('restro_mobile_desig[]','',['placeholder'=>'Enter name','class'=>'form-control','id'=>'mobile-designation'])  !!}
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="control-label"
                                                                   style=" text-align: right;  width: 100%;"> &nbsp; <a
                                                                        href="javascript:void(0)"
                                                                        onclick="remove_div('remove-restro-mobile-new-div')"
                                                                        class="text-danger" style="display:none;">Remove
                                                                    <i class="fa fa-close"></i></a></label>
                                                            <label class="radiobox">
                                                                {!! Form::radio('is_primary_mobile', '1') !!}
                                                                <span>Is Primary </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            {!! Form :: hidden('restro_mobile_count','1',['id'=>'restro-mobile-1-count'])  !!}

                                            <div class="form-group">
                                                <a href="javascript:void(0)"
                                                   onclick='fieldAdd("restro-phone-1","restro-parent-phone",["<label class=\"control-label\">Phone</label>"],[""],"remove-restro-phone-new-div")'>Add
                                                    New Phone</a>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Phone</label><span
                                                                class="red">*</span>
                                                        {!! Form :: text('restro_landline','',['placeholder'=>'Enter Phone','class'=>'form-control','id'=>'landline'])  !!}
                                                    </div>
                                                </div>
                                                <div id="restro-parent-phone">
                                                    <div id="restro-phone-1" class="phone-clear">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label"
                                                                       style=" text-align: right;  width: 100%;">
                                                                    &nbsp;</label>

                                                                {!! Form :: text('restro_landlie_ext[]','',['placeholder'=>'Enter Extension','class'=>'form-control','id'=>'landline'])  !!}

                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">

                                                                <label class="control-label"
                                                                       style=" text-align: right;  width: 100%;"> &nbsp;
                                                                    <a href="javascript:void(0)"
                                                                       onclick="remove_div('remove-restro-phone-new-div')"
                                                                       class="text-danger" style="display:none;">Remove
                                                                        <i class="fa fa-close"></i></a></label>

                                                                {!! Form :: text('restro_landlie_desig[]','',['placeholder'=>'Enter Designation','class'=>'form-control','id'=>'landline'])  !!}


                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            {!! Form :: hidden('restro_phone_count','1',['id'=>'restro-phone-1-count'])  !!}

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">Mindestbestellwert</label>
                                                    {!! Form::text("restro_minimum_order", "", ["class"=>"form-control"]) !!}
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">darunter</label>
                                                    {!! Form::text("restro_below_minimum_order", "", ["class"=>"form-control"]) !!}
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Zustellkosten</label>
                                                    {!! Form::text("restro_delivery_cost", "", ["class"=>"form-control"]) !!}
                                                </div>
                                            </div>


                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="control-label"><i class="fa fa-envelope"></i>
                                                    Address1</label> <span class="red">*</span>
                                                {!! Form :: text('add1','',['placeholder'=>'Enter Address1','class'=>'form-control','id'=>'add1'])  !!}
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label"><i class="fa fa-envelope"></i>
                                                    Address2</label> <span class="red">*</span>
                                                {!! Form :: text('add2','',['placeholder'=>'Enter Address2','class'=>'form-control','id'=>'add2'])  !!}
                                            </div>

                                            <div class="form-group">

                                                <label class="control-label">Country</label><span class="red">*</span>
                                                <select id="country" class="form-control js-example-basic-multiple"
                                                        name="country"
                                                        onchange="getData(this.value,'countries','states','state','id','name','id','country_id')">
                                                    <option value="">- Select -</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('State') !!}<span class="red">*</span>
                                                <select id="state" name="state"
                                                        class="form-control js-example-basic-multiple"
                                                        onchange="getData(this.value,'states','cities','city','id','name','id','state_id')">

                                                </select>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('City') !!}<span class="red">*</span>
                                                <select id="city" name="city"
                                                        class="form-control js-example-basic-multiple">


                                                </select>
                                            </div>

                                            <div class="form-group">

                                                <label class="control-label">Zipcode</label><span class="red">*</span>
                                                <select id="zipcode" class="form-control js-example-basic-multiple"
                                                        name="zipcode">
                                                    <option value="">- Select -</option>
                                                    @foreach($zipcodes as $zipcode)
                                                        <option value="{{$zipcode->id}}">{{$zipcode->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label">Fax</label>
                                                {!! Form :: text('fax','',['placeholder'=>'Enter Phone','class'=>'form-control','id'=>'fax'])  !!}
                                            </div>


                                            <div class="form-group">
                                                {!! Form::label('', 'Upload Logo', array('class' => '"control-label')) !!}
                                                <span class="red">*</span>

                                                <input class="form-control" type="file" name="file"
                                                       style="border: none; font-size: 12px; padding: 0px;">
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label ">Price Types</label><span
                                                        class="red">*</span>
                                                @foreach($priceTypes as $priceType)

                                                    <label class="checkbox checkbox-inline">
                                                        {!! Form :: checkbox('price_types[]',$priceType->id,'',['class'=>'price-types','id'=>'price-types'])  !!}
                                                        <span>{{$priceType->name}}</span>
                                                    </label>

                                                @endforeach
                                                <div class="question"></div>
                                            </div>


                                        </div>

                                    </div>


                                    <div class="form-group margin-top">


                                        <!-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add </button> -->
                                        <hr>
                                        {!! Form :: submit("Add",["class"=>"btn btn-primary pull-right","name"=>"submit"]) !!}

                                        {!! Form :: submit("Save And Assign Zipcodes",["class"=>"btn btn-primary pull-right","name"=>"assignZip"]) !!}
                                        <p><span class="red">*</span> - Required Fields.</p>
                                    </div>
                                {!! Form::close() !!}
                                <!-- </form> -->
                                </div>


                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>

                    <!-- quick email widget -->


                </section>
                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $.validator.addMethod("email", function (value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter valid email address.");

            $("#restaurant-form").validate({
                rules: {


                    f_name: {
                        required: true
                    },
                    'restro_email[]': {
                        required: true,
                        email: true
                    },

                    'restro_mobile[]': {
                        required: true,
                        minlength: 10,
                        maxlength: 10
                    },
                    add1: {
                        required: true

                    },
                    add2: {
                        required: true

                    },
                    country: {
                        required: true
                    },
                    state: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    zipcode: {
                        required: true
                    },

                    'price_types[]': {
                        required: true,

                    },
                    restro_landline: {
                        required: true

                    },
                    file: {
                        required: true
                        //extension: "png|gif|jpg|jpeg"
                    }

                },
                messages: {
                    f_name: "Please enter restaurant first name",
                    add1: "Please enter first address",
                    add2: "Please enter second address",
                    restro_landline: "Please enter landline with their extensions",
                    file: "Select valid image file",
                    country: "Please select country",
                    state: "Please select state",
                    city: "Please select city",
                    zipcode: "Please select zipcode",
                    'price_types[]': {
                        required: "You must check at least 1 box"

                    }

                },
                errorPlacement: function (error, element) {
                    if (element.attr("type") == "checkbox") {
                        error.insertAfter($('.question'));
                    } else {
                        error.insertAfter(element);
                        //error.insertBefore(element);
                    }
                }

            });
        });
    </script>


@endsection

