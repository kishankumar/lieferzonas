@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Root Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.rootcatmap.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
        <li class="active">Add Root Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->

            {{--    Error Display--}}
            @if($errors->any())
                <ul class="alert">
                    @foreach($errors->all() as $error)
                        <li style="color:red;"> <b>{{ $error }}</b></li>
                    @endforeach
                </ul>
                @endif
                {{--    Error Display ends--}}

            
            
            <div class="panel panel-primary theme-border">
                    <div class="panel-heading theme-bg">
                       
                        <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add Root Category.</h4>
                    </div>
                    
                     <div class="modal-body" style="padding: 5px;">
                     <div class="panel-body">
						 {!! Form::open(array('route'=>'root.rootcatmap.create','id'=>'searchingForm','class'=>'form-horizontal','method'=>'GET')) !!}
						 {{--<form class="form-horizontal">--}}
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label">Restaurant Name<span class="red">*</span></label>
								 <div class="col-sm-4">
                 @if($restro_detail_id)
									 <select class="form-control" disabled name="restaurant">
                   @else
                   <select class="form-control js-example-basic-multiple" name="restaurant" id="restaurant" onChange="hideErrorMsg('restauranterr')">
                   @endif
                   <option value="">Select</option>

                   @foreach($restro_names as $restro_name)

                   @if($restro_name->id==$restro_detail_id)
                   <option value="{{$restro_name->id}}" selected >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @else
                   <option value="{{$restro_name->id}}" >{{$restro_name->f_name}} {{$restro_name->l_name}}</option>
                   @endif
                   @endforeach
                   </select>
                   <span id="restauranterr" class="text-danger"></span>

								 </div>
                 @if(!$restro_detail_id)
								 <div class="col-sm-4">
                                                                        <button class="btn btn-primary" type="button" onclick="validate_rest()">Get Root Category</button>
								 </div>
                 @endif
							 </div>
               <div class="clearfix"></div>
               {!! Form::close() !!}
							 
               @if($restro_detail_id)
               <div>
                {!! Form::open(array('route'=>'root.rootcatmap.store','class'=>'form-horizontal', 'id'=> 'serviceform')) !!}
							 <hr>
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label">&nbsp;</label>
								 <div class="col-sm-9">

                 <!-- {!! Form::checkbox('agree', 1, null, ['class' => 'field']) !!} -->
									 <!-- <label class="checkbox checkbox-inline" style="padding-left:0;"><input type="checkbox"><span>typesetting </span></label>
									 <label class="checkbox checkbox-inline"><input type="checkbox"><span>typesetting </span></label>
									 <label class="checkbox checkbox-inline"><input type="checkbox"><span>typesetting </span></label>
									 <label class="checkbox checkbox-inline"><input type="checkbox"><span>typesetting </span></label> -->
                   @foreach($root_categories as $root_category)
									 <label class="checkbox checkbox-inline">
                   @if(in_array($root_category->id,$root_cat_maps))
                   <input type="checkbox" name="root_category[]" value="{{$root_category->id}}" checked >
                   @else
                   <input type="checkbox" name="root_category[]" value="{{$root_category->id}}" >
                   @endif

                   <span>{{$root_category->name}} </span>
                     <a href="#" data-toggle="tooltip" data-placement="bottom" title="{{$root_category->valid_from}} To {{$root_category->valid_to}}">
                      <i class="fa fa-question-circle"></i> </a>
                   </label>
                   @endforeach

                   {!! Form :: hidden('restaurant',$restro_detail_id,['id'=>'restro-detail-id'])  !!}

								 </div>
							 </div>
                                                         
                                                         <span id="serviceerr" class="text-danger" ></span>
                                                         
							 <div class="col-md-12 form-group">
								 <label class="col-sm-2 control-label"></label>
								 <div class="col-sm-4">
                                                                        <button class="btn btn-primary" type="button" onclick="validate_service()">Submit</button>
                                                                        <a class="btn btn-primary" href="{{url('root/rootcatmap/create/')}}">Cancel</a>
								 </div>
							 </div>
						  {!! Form::close() !!}
              </div>
              @endif
							<!-- </form> -->
						</div>
                     
                     </div>  
                      
                    </div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('script')
<script>
    function hideErrorMsg(id){
        $('#'+id).attr('style', 'display:none');
    }
    function validate_rest()
    {
        $restaurant = $('#restaurant').val();
        if($restaurant=='')
        {
            $("#restaurant").focus();
            $("#restauranterr").html('Please select a restaurant');
            $("#restauranterr").show();
            return false;
        }
        document.forms["searchingForm"].submit();
    }
    function validate_service()
    {
        var service = document.getElementsByName('root_category[]');
        var haschecked= false;
        for(var i=0; i < service.length; i++)
        {
            if(service[i].checked)
            {
                haschecked = true;
                document.forms["serviceform"].submit();
            }
        }
        if(haschecked == false)
        {
            $("#serviceerr").html('Please check atleast one Category');
            $("#serviceerr").show();
        }
    }
</script>
@endsection

