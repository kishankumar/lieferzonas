﻿@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Front Service
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.frontservice.index')}}"><i class="fa fa-dashboard"></i> Restaurant Management</a></li>
            <li class="active">Front Service</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Front Service</h3>
                    </div>
                    <!-- /.box-header -->
                    
                    <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                        {!! Form::open(array('url'=>"root/frontservice/",'class'=>'form-inline','method'=>'GET')) !!}
                            {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                            {!! Form::submit('Search',['id'=>'sssssssss','class'=>'btn btn-primary']) !!}
                            <a class="btn btn-primary" href="{{url('root/frontservice/')}}">Reset Search</a>
                        {!! Form::close() !!}
                    </div>

                    <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <a class="btn btn-default"  data-toggle="modal"  data-original-title href="#" onclick="frontservice(0)"><span>Add</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable" onclick="download_frontservice()" ><span>Download</span></a>
						    <a class="btn btn-danger DTTT_button_xls" onclick="delete_frontservice()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                             <span>Delete</span>
                            </a>
                        </div>
                    </div>
                    
                    <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
                    
                    <div class="clearfix" style="margin: 20px 0;"></div>

                    <div class="box-body">

                        
                        @if(Session::has('message'))
							<div class="alert alert-success" style="padding: 7px 15px;">
							  {{ Session::get('message') }}
							  {{ Session::put('message','') }}
							</div>
						@endif

					
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                                <th> 
                                    <a href="{{url('root/frontservice/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                                    Name <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('root/frontservice/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                                    Description <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('root/frontservice/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='from' ? 'from-desc' :'from';?>">
                                    Valid from <?php if($type=='from'){ echo $class; } elseif($type=='from-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('root/frontservice/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='to' ? 'to-desc' :'to';?>">
                                    Valid to <?php if($type=='to'){ echo $class; } elseif($type=='to-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($frontservices))
                            @foreach($frontservices as $frontservice)
                            <tr>
                                   <td>

                                    <div class="checkbox1 icheck">

                                        <label>
                                            {!! Form::checkbox('frontservice_id', $frontservice['id'], null, ['class' => 'frontservice checkBoxClass']) !!}
                                            <span class="button-checkbox"></span>
                                        </label>
                                    </div>
                                   </td>
                                    
                                    <td>{{$frontservice['name']}}</td>
									
									<td>{{ (strlen($frontservice['description']) > 50) ? substr($frontservice['description'],0,50).'...' : $frontservice['description']  }}</td>
                                    <td>{{ date("d M Y", strtotime($frontservice['valid_from']))  }}</td>
                                    <td>{{ date("d M Y", strtotime($frontservice['valid_to']))  }}</td>
                                    <td> @if($frontservice['status']==1) <i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($frontservice['status']==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>
                                    <td><a  class="btn btn-success" href="#" onclick="frontservice({{$frontservice['id']}})"> <i class="fa fa-edit"></i></a></td>
                            </tr>
                            @endforeach
                     @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>

                    </table>
                        {!! $frontservices->appends(Request::except('page'))->render() !!}
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="front-service-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Front Service
            </div>

                <div class="modal-body" style="padding: 5px;">
                    <div class="panel-body">
                        @if(Session::has('errmsg'))
						<div class="alert alert-danger" style="padding: 7px 15px;">
						  {{ Session::get('errmsg') }}
						  {{ Session::put('errmsg','') }}
						</div>
					    @endif
						@if($errors->any())
							<div class="alert alert-danger ">
								<ul class="list-unstyled">
									@foreach($errors->all() as $error)
										<li> {{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						
                       
                       {!! Form::open(array('url' => 'root/frontservice/add_frontservice','id'=>'front-service','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}



                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name</label><span class="red">*</span>
                                       
                                        {!! Form::text('name','',['id'=>'name','class'=>'form-control']) !!}
                                         {!! Form::hidden('frontservice_id','0',['id'=>'frontservice_id','class'=>'form-control']) !!}
                                    </div>
                                </div>
                             


                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="control-label">Status</label>
                                        {!! Form::select('status',['1'=>'Active','0'=>'Deactive'], '', ['id'=>'status','class'=>'form-control']) !!}
                                    </div>

                                </div>

                            </div>
							<div class="row">

                              
                             

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Description</label><span class="red">*</span>
                                        {!! Form::textarea('description','',['id'=>'description','class'=>'form-control required']) !!}
                                    </div>

                                </div>

                               

                            </div>
						<div class="row form-group">
                            <div class="col-sm-6">
							  <div class="input-group date " id="datetimepicker8">
								<input type="text" class="form-control" name="valid_from" id="valid_from" placeholder="Valid from" >
								<span class="input-group-addon"> 
								<span class="glyphicon glyphicon-calendar"></span> </span> </div>
								
                            </div>
                            <div class="col-sm-6">
							  <div class="input-group date" id="datetimepicker7">
								<input type="text" class="form-control" name="valid_to" id="valid_to" placeholder="Valid to">
								<span class="input-group-addon"> 
								<span class="glyphicon glyphicon-calendar"></span> </span> </div>
								
                            </div>
                        </div>

                            <div class="form-group margin-top">
                               
                                {!! Form::submit('Add ',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                                <hr>
                                <p><span class="red">*</span> - Required Fields.</p>
                            </div>
                        {{--</form>--}}
                        {!! Form::close() !!}
                    </div>

                </div>

        </div>
    </div>
</div>
<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@endsection

@section('script')


    <script type="text/javascript">

     $(document).ready(function(){
     $("#front-service").validate({
      rules: {


        name: {
            required: true
               },
        description: {
            required: true
               },
        valid_from: {
            required: true
               },
        valid_to: {
            required: true
               },
         
        },
       messages: {

                    name: "Please enter name",
                    description: "Please enter description",
                    valid_from:"Please enter date valid from",
                    valid_to:"Please enter date valid to",
                    agree: "Please accept our policy"
                },
         
     });
});


    function delete_frontservice(){
        var len = $(".frontservice:checked").length;
        if(len==0){
            alert('Please select atleast one Front service');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to delete Front services?");
        if (r == true) {
            var selectedFrontservices = new Array();
            $('input:checkbox[name="frontservice_id"]:checked').each(function(){
                selectedFrontservices.push($(this).val());
            });
            $.ajax({
                url: 'frontservice/delete',
                type: "post",
                dataType:"json",
                data: {'selectedFrontservices':selectedFrontservices, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('Records not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
	
	/*added by Rajlakshmi(31-05-16)*/
	function download_frontservice(){
        var len = $(".frontservice:checked").length;
        if(len==0){
            alert('Please select atleast one Front service');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to download Front services?");
        if (r == true) {
            var selectedFrontservices = new Array();
            $('input:checkbox[name="frontservice_id"]:checked').each(function(){
                selectedFrontservices.push($(this).val());
            });
            $.ajax({
                url: 'frontservice/download',
                type: "post",
                dataType:"json",
                data: {'selectedFrontservices':selectedFrontservices, "_token":"{{ csrf_token() }}"},
               success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                       var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Frontservice List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
						

				doc.save('Frontservice_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
    }
	
		 /*added by Rajlakshmi(31-05-16)*/
     
	 function frontservice(id)   
     {
       
        var frontservice_id = id;

        if(frontservice_id!='0')
        {
             $("#submit").val('Update');
             $.ajax({
                url: '{{ url('fetchdatas') }}',
                type: "GET",
                dataType:"json",
                data: {'frontservice_id':frontservice_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
                        
                        $('#name').val(res.name);
                        $('#description').val(res.description);
						$('#valid_from').val(res.valid_from);
						$('#valid_to').val(res.valid_to);
                        $('#status').val(res.status);
                        $('input[name=frontservice_id]').val(res.id);
                        showModal('front-service-form');
                    }
                    else
                    {
                        document.getElementById("front-service-form").reset();
                    }
                }
            });
        }
       else
       {
             
             showModal('front-service-form');
             $("#submit").val('Add');
             
             document.getElementById("front-service").reset();
            
       }
     }
     

    </script>
     @if($errors->any())
        <script type="text/javascript">
         showModal('front-service-form');
        </script>
     @endif

	 @if(Session::has('errmsg'))
	  <script type="text/javascript">
         showModal('front-service-form');
      </script>					
	 @endif
	 

@endsection