@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Special Offer Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('root.specialoffer.index')}}"><i class="fa fa-dashboard"></i>Special Offer</a></li>
        <li class="active">Special Offer Detail</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Special Offer Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-12 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
               
                     <!--<a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <a  href="" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable" >
                         <span >Download</span>
                     </a>
                     <a class="btn btn-danger DTTT_button_xls" onclick="delete_offers()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                         <span>Delete</span>
                     </a>-->
                </div>
             </div>
            
            <div class="panel-body">
            @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                <ul class="offer_box list-inline">
					<li><strong>Special Offer Name :</strong></li>
					<li>
						{{ $specialoffer['0']['name'] }} 
					</li>
				</ul>    
				<ul class="offer_box list-inline">
					<li><strong>Discount Type :</strong></li>
					<li>
						{{ $specialoffer['0']['discount_type_name'] }}: {{ $specialoffer['0']['discount_type_value'] }}
					</li>
				</ul>
				<ul class="offer_box list-inline">
					<li><strong>Category :</strong> </li>
                    <li>
                        	<div class="table-responsive">
                            <table class="table table-bordered table-striped" style="margin:0;">
							<?php if(count($listcat)>0 || count($listmenu)>0 || count($listsubmenu)>0 ) { ?>
                                <thead>
                                    <tr>
                                        <th width="21%">Category</th>
                                        <th width="16%">Menu</th>
                                        <th width="21%">Submenu</th>
                                       
                                    </tr>
                                </thead>
							<?php } ?>
                                <tbody>
                                @if(count($listcat))
                                @foreach($listcat as $listcats)
                                <?php $catrand = rand(10,100); ?>
                                <tr id='<?=$catrand?>'>
                                <td width="55"><?=$listcats['name']?><input name="catid1[]" value="<?=$listcats['id']?>" type="hidden"></td>
                                <td>All</td>
                                <td>All</td>
                              
                                </tr>
                                @endforeach
                                @endif
                                @if(count($listmenu))
                                @foreach($listmenu as $listmenus)
                                <?php $menurand = rand(10,100); ?>
                                <tr id='<?=$menurand?>'>
                                <td width="55"><?=$listmenus['catname']?><input name="catid2[]" value="<?=$listmenus['category_id']?>" type="hidden"></td>
                                <td width="55"><?=$listmenus['menuname']?><input name="menuid2[]" value="<?=$listmenus['menu_id']?>" type="hidden"></td>
                                <td>All</td>
                               
                                </tr>
                                @endforeach
                                @endif
                                @if(count($listsubmenu)>0)
                                @foreach($listsubmenu as $listsubmenus)
                                <?php $submenurand = rand(10,100); ?>
                                <tr id='<?=$submenurand?>'>
                                <td width="55"><?=$listsubmenus['catname']?><input name="catid3[]" value="<?=$listsubmenus['id']?>" type="hidden"></td>
                                 <td width="55"><?=$listsubmenus['menuname']?><input name="menuid3[]" value="<?=$listsubmenus['menu_id']?>" type="hidden"></td>
                                 <td width="55"><?=$listsubmenus['submenuname']?><input name="submenuid3[]" value="<?=$listsubmenus['sub_menu_id']?>" type="hidden"></td>
                                
                                </tr>
                                @endforeach
                                @endif
                             </tbody>               
                            </table>
                        </div>
						</li>
				</ul>
                    
				<ul class="offer_box list-inline">
					<li><strong>Valid From : </strong></li>
					<li class="col-sm-10">{{  date("d M Y", strtotime($specialoffer['0']['valid_from']))  }}</li>
				</ul>
				<ul class="offer_box list-inline">		  
					<li><strong>Valid To : </strong></li>
					<li>{{  date("d M Y", strtotime($specialoffer['0']['valid_to']))  }}</li>
				</ul>
				
                <ul class="offer_box list-inline">	
					<li><strong>Order Service :</strong> </li>
					<li>

						<ul style="padding-left:18px;">@foreach($orderservice as $orderservices) <li>{{ $orderservices['order_service_name'] }} </li> @endforeach</ul>

					</li>
				</ul>
				<br>
				<ul class="offer_box list-inline">	
                	<h4 class="text-success" style="margin:0;"> Special Offer starts </h4>
				</ul>
               
					  <?php  
					  for($i=0; $i < count($selected_daytimeall); $i++) 
					  {  
				       $day_id = $selected_daytimeall[$i];  
				       $dayname = \App\superadmin\Rest_day::dayname($day_id);?>
					   
						    <ul class="offer_box list-inline">	
								     <li><lable><?=$dayname['0']['day']?></lable></li>
									 <li>
									<?php
									  	 $daytime = \App\superadmin\Rest_day::daytime($day_id,$specialoffer['0']['id']);						
										 if(count($daytime)>0)
										 {
										 foreach($daytime as $daytimes)
										 {
											 if($daytimes['time_from']!='' && $daytimes['time_to']!='')
											 {
										?>  
										   
											
											<div class="col-sm-3"><label>Start Time :</label><?=$daytimes['time_from']?></div>
											<div class="col-sm-3"><label>End Time :</label><?=$daytimes['time_to']?></div>

											<div class="clearfix"></div>

											
									   <?php }  } } ?>	
								    </li>
							</ul>	
					  <?php } ?>
					  
				
                    
                <ul class="offer_box list-inline">	
					<li><strong class="text-success">Original Price: </strong></li>
					<li>
						{{ $specialoffer['0']['original_price'] }}
					</li>
				</ul>
                
                <ul class="offer_box list-inline">
					<li><strong class="text-success">Discount Price: </strong></li>
					<li>{{ $specialoffer['0']['discount_price'] }}</li>
				</ul>
                
                <div class="clearfix"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 



 <!-- /.modal -->
 @endsection


    @section('script')

  

    @endsection




