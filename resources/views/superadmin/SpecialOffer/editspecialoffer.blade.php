@extends('layouts.superadminbody')
@section('title')
@endsection
@section('body')
<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Specialoffer Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('root.specialoffer.index')}}"><i class="fa fa-dashboard"></i>Special Offer</a></li>
        <li class="active">Edit Special Offer</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">Edit Specialoffer With Full Features</h3>


            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
               @if(Session::has('errmsg'))
						<div class="alert alert-danger" style="padding: 7px 15px;">
						  {{ Session::get('errmsg') }}
						  {{ Session::put('errmsg','') }}
						</div>
					    @endif
						@if($errors->any())
							<div class="alert alert-danger ">
								<ul class="list-unstyled">
									@foreach($errors->all() as $error)
										<li> {{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
                
{!! Form::model($specialoffer,['route'=>['root.specialoffer.update',$specialoffer['0']['id']],'method'=>'patch','id' => 'specialoffer_form','class' => 'form','novalidate' => 'novalidate', 'files' => true])	!!}                

            <!--<form action="#" method="post" accept-charset="utf-8">-->
        
        <div class="panel-body">
            <div class="errors" id="errors" style="color: red"></div>
            <div class="row">
               
				
                <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('Name') !!}<span class="red">*</span>
                    {!! Form::text('name',$specialoffer['0']['name'],["id"=>'name',"class"=>"form-control" ,"onkeypress"=>"hideErrorMsg('nameerr')"]) !!}
                    </div>
                    <span id="nameerr" class="text-danger"></span>
                </div>
               
            </div>
            <div class="row">
                
				<div class="col-md-10">
                    <div class="form-group">
                    <h4>{!! Form::label('Discount type') !!}</h4>
					<div class="form-group row">
                    <?php
                     foreach($dis_types as $dis_type) { ?>
						<label class="radiobox radio-inline">
						<input type="radio" name='discount_type' value="{{ $dis_type['id']}}" 
						id="discount_type" <?php if($dis_type['id']==$specialoffer['0']['discount_type_id']) { echo 'checked'; }?>
						onclick="show('<?= $dis_type['name']?>')"> 
						<span>{{ $dis_type['name']}}</span></label>					
                    <?php
                              } ?> 
					</div>	
					{!! Form::text('discount',$specialoffer['0']['discount_type_value'],["id"=>'discount',"class"=>"form-control","placeholder"=>"Fixed","onkeypress"=>"hideErrorMsg('discounterr')"]) !!}
						
                    <span id="discounterr" class="text-danger"></span>
                    </div>
                      
                </div>
				
            
               
            </div>
			<div class="clearfix"></div>
			<div class="row">
                <div class="col-md-10">
                    <div class="form-group row">
                   
                        <label class="radiobox radio-inline"><input type="radio" name='type' value="1" class='cat' checked> <span>Category</span></label>
						<label class="radiobox radio-inline"><input type="radio" name='type' value="2" class='menu'  > <span>Menu</span></label>
					    <label class="radiobox radio-inline"><input type="radio" name='type' value="3" class='submenu'  > <span>Submenu</span></label>
                    </div>
             <div class="form-group row">
				<div class="form-group col-sm-3" id="cat">
                   <select class="form-control" name="category" id="category" 
							onchange="getDatatbl(this.value,'rest_categories','rest_menus','menus','id','name','id','rest_category_id','caterr')"  >

							<option value=''>Select</option>    
									  <?php
										foreach($categories as $category) { ?>

											<option value="<?=$category['id'] ?>" name="<?=$category['name'] ?>" ><?=$category['name'] ?></option>
									  <?php
									  } ?>
							</select>
							<span id="caterr" class="text-danger"></span>
                    <span id="caterr" class="text-danger"></span>
                </div>
                <div class="form-group col-sm-3" id="menu" style='display:none;'>
                   <select class="form-control" name="menu" id="menus" onchange="getDatatbl(this.value,'rest_menus','rest_sub_menus','submenus','id','name','id','menu_id','menuerr')" >
							</select>
							 <span id="menuerr" class="text-danger"></span>
                </div>
				<div class="form-group col-sm-3" id="submenu" style='display:none;'>
                   <select class="form-control" name="submenu" id="submenus" onclick="hideErrorMsg(submenuerr);" ></select>
					<span id="submenuerr" class="text-danger"></span>
                </div>
                 <div class="form-group col-sm-3">
                    <a class="btn  btn-primary"style="border:none;" onclick='addcatmenu();'>Add</a>
                </div>
					
					</div>

          
			<table class="table table-bordered table-striped" id='table'>
								<thead>
									<tr>
										<th width="21%">Category</th>
										<th width="16%">Menu</th>
										<th width="21%">Submenus</th>
										<th width="16%"></th>
										
									</tr>
								</thead>
                
				<tbody>
					@if(count($listcat))
					@foreach($listcat as $listcats)
					<?php $catrand = rand(10,100); ?>
					<tr id='<?=$catrand?>'>
					<td width="55"><?=$listcats['name']?><input name="catid1[]" value="<?=$listcats['id']?>" type="hidden"></td>
					<td>All</td>
					<td>All</td>
					<td><a href="#" onclick="removedata(<?=$catrand?>)">Delete</a></td>
					</tr>
					@endforeach
					@endif
					@if(count($listmenu))
					@foreach($listmenu as $listmenus)
					<?php $menurand = rand(10,100); ?>
					<tr id='<?=$menurand?>'>
					<td width="55"><?=$listmenus['catname']?><input name="catid2[]" value="<?=$listmenus['category_id']?>" type="hidden"></td>
					<td width="55"><?=$listmenus['menuname']?><input name="menuid2[]" value="<?=$listmenus['menu_id']?>" type="hidden"></td>
					<td>All</td>
					<td><a href="#" onclick="removedata(<?=$menurand?>)">Delete</a></td>
					</tr>
					@endforeach
					@endif
					@if(count($listsubmenu)>0)
					@foreach($listsubmenu as $listsubmenus)
					<?php $submenurand = rand(10,100); ?>
					<tr id='<?=$submenurand?>'>
					<td width="55"><?=$listsubmenus['catname']?><input name="catid3[]" value="<?=$listsubmenus['id']?>" type="hidden"></td>
					 <td width="55"><?=$listsubmenus['menuname']?><input name="menuid3[]" value="<?=$listsubmenus['menu_id']?>" type="hidden"></td>
					 <td width="55"><?=$listsubmenus['submenuname']?><input name="submenuid3[]" value="<?=$listsubmenus['sub_menu_id']?>" type="hidden"></td>
					<td><a href="#" onclick="removedata(<?=$submenurand?>)">Delete</a></td>
					</tr>
					@endforeach
					@endif
			    </tbody>
			</table>
			  <span id="listerr" class="text-danger"></span>
			<div class="row">
              
				
                <div class="col-md-10">
                    <h4>{!! Form::label('Order Service') !!}<span class="red">*</span></h4>
                    <div class="form-group row">
                    <?php
                     foreach($order_services as $order_service) { 
                     	 $select = in_array( $order_service['id'], $selected_order) ? 'checked' : null;
                     	 echo '<label class="checkbox checkbox-inline mt0">
						<input type="checkbox" name="order_service[]" value="'.$order_service['id'].'"  '.$select.' onclick="hideErrorMsg("order_serviceerr");" > 
						<span>'.$order_service['name'].'</span></label>';
                     
                    } ?>  
                  
                       <span id="order_serviceerr" class="text-danger"></span>
                    </div>

                </div>
               
            </div>


			
			<div class="row">
            
                        <br>
                <div class="form-group col-sm-3">
                      <label class="radiobox"><input type="radio" name="validity" value="T" id ="validity" class="val_table"
                       <?php if($specialoffer['0']['validity_type']=='T') {  echo 'checked'; } ?> > <span>Validity table</span></label>
                </div>
                <div class="form-group col-sm-3">
                            <label class="radiobox"><input type="radio" name="validity" value="C" id ="validity" class="val_date"  
                            <?php if($specialoffer['0']['validity_type']=='C') {  echo 'checked'; } ?> > <span>Date validity</span></label>
                </div>
                
                <div class="clearfix"></div>
                <div class="form-group col-sm-6" id="select_s" <?php if($specialoffer['0']['validity_type']=='C') { ?>style="display:none;" <?php } ?>>
                    <select class="form-control" name="validity_table" id="validity_table"  onchange="hideErrorMsg('validity_tableerr')">
                    <option value=''>Select</option>    
						      <?php
                                foreach($time_validations as $time_validation) { ?>

                                    <option value="<?=$time_validation['id'] ?>" <?php if($time_validation['id']==$specialoffer['0']['validity_table']) { echo 'selected'; }?>><?=$time_validation['name'] ?></option>
                              <?php
                              } ?>
                    </select>
                    <span id="validity_tableerr" class="text-danger"></span>
                </div>
                <div class="form-group col-sm-8" id="date_s" <?php if($specialoffer['0']['validity_type']=='T') { ?>style="display:none;" <?php } ?>>
                    <div class="row">
                        <div class="col-sm-5">
                          <div class="input-group date " id="datetimepicker25">
                            <input type="text" class="form-control" name="valid_from" id="valid_from" value="<?php if($specialoffer['0']['validity_type']=='C')  { echo 
                            	$specialoffer['0']['valid_from']; } ?>">
                            <span class="input-group-addon"> 
                            <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                            <span id="valid_fromerr" class="text-danger"></span>
                        </div>
						<div class="col-sm-5">
							  <div class="input-group date" id="datetimepicker26">
								<input type="text" class="form-control" name="valid_to" id="valid_to" value="<?php if($specialoffer['0']['validity_type']=='C')  { echo 
                            	$specialoffer['0']['valid_to']; } ?>" >
								<span class="input-group-addon"> 
								<span class="glyphicon glyphicon-calendar"></span> </span> </div>
								 <span id="valid_toerr" class="text-danger"></span>
						</div>
                    </div>
                </div>          
                        
            
				
            </div>
			<?php
		    foreach($days as $day) {  ?>
		
			<div class="row">
			<div class="col-sm-3">
				<label></label>
			    <label class="checkbox check_click">
				
				 <?php
                    
                     	$select = in_array($day['id'], $selected_daytimeall) ? 'checked' : null;
                     	echo '
						<input type="checkbox" name="days[]" value="'.$day['id'].'"  '.$select.'> 
						<span>'.$day['day'].'</span>';
						
                     
                  ?>  

					
                </label>
                </div>
                
                  <?php 
                    $daytime = \App\superadmin\Rest_day::daytime($day['id'],$specialoffer['0']['id']);
                  	if(count($daytime)>0)
                  	{
                	 foreach($daytime as $daytimes)
                     {
                     	 if($daytimes['time_from']!='' && $daytimes['time_to']!='')
                     	 {
                	?>
		                <div class="col-md-6" id='time<?=$day['id'] ?>'>
						<div class="row">
						<div class="col-sm-6">
						    
							<div class="form-group">
								<label for="Start Time">Start Time</label>

								{!! Form::select('start_time'.$day['id'].'[]',$fromtime, $daytimes['time_from'], ['id'=>'from'.$day['id'].'_start_time','class' => 'form-control']) !!}

								<span id="time_fromerr" class="text-danger"></span>
						    </div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="End Time">End Time</label>
								{!! Form::select('end_time'.$day['id'].'[]',$totime, $daytimes['time_to'], ['id'=>'to'.$day['id'].'_end_time','class' => 'form-control']) !!}


							 <span id="time_toerr" class="text-danger"></span>
							</div>
						</div>
						</div>
					     </div>
                 <?php } } } else {  
                 ?>
                 
                 <div class="col-md-6" id='time<?=$day['id'] ?>'>
					 <div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="Start Time">Start Time</label>

								{!! Form::select('start_time'.$day['id'].'[]',$fromtime, '', ['id'=>'from'.$day['id'].'_start_time','class' => 'form-control']) !!}
                 <span id="time_fromerr" class="text-danger"></span>
								
						    </div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="End Time">End Time</label>
								{!! Form::select('end_time'.$day['id'].'[]',$totime, '', ['id'=>'to'.$day['id'].'_end_time','class' => 'form-control']) !!}

								 <span id="time_toerr" class="text-danger"></span>

							</div>
						</div>
					 </div>

			     </div>

                <?php } ?>  
				 

					<div class="col-sm-3">
						<div>&nbsp;</div>
						<div><a href='javascript:void(0);' class="btn btn-default" onclick="showtime(<?=$day['id'] ?>)">Add more</a></div>
					</div>
					<div class="clearfix"></div>
			        <div id='add<?=$day['id'] ?>'>
			

			        </div>

			
			</div>
			<?php
				  } ?>
			<div class="row">
               
				
                <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('Original price') !!}<span class="red">*</span>
                    {!! Form::text('original_price',$specialoffer['0']['original_price'],["id"=>'original_price',"class"=>"form-control" ,"onkeypress"=>"hideErrorMsg('original_priceerr')"]) !!}
                    </div>
                    <span id="original_priceerr" class="text-danger"></span>
                </div>
               
            </div>
			<div class="row">
               
				
                <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('Discount price') !!}<span class="red">*</span>
                    {!! Form::text('discount_price',$specialoffer['0']['discount_price'],["id"=>'discount_price',"class"=>"form-control" ,"onkeypress"=>"hideErrorMsg('discount_priceerr')"]) !!}
                     <span id="discount_priceerr" class="text-danger"></span>
                    </div>

                </div>
               
            </div>
               
            
            <div class="form-group margin-top">
              {!!Form::button('Update',["id"=>'edit',"class"=>"btn  btn-primary","onclick"=>"validate_offer()"]) !!}
               
                <hr>
				<p><span class="red">*</span> - Required Fields.</p>
            </div>
        </div>
    </div>
            {!! Form::close() !!}
              
            </div>
            <!-- /.box-body -->
				</div>
				</div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>
    @endsection
    @section('script')
    <script type="text/javascript"> 
	$(document).ready(function() {
        $('.cat, .menu , .submenu').click(function(){
            if($('.cat').is(":checked")==true){
                 $('#cat').css({'display':'block'});
				 $('#menu').css({'display':'none'});
				 $('#submenu').css({'display':'none'});
            }
            if($('.menu').is(":checked")==true){
                 
				 $("#cat option[value='']").attr('selected','selected');
				 $('#cat').css({'display':'block'});
				 $('#menu').css({'display':'block'});
				 $('#submenu').css({'display':'none'});
            }
			if($('.submenu').is(":checked")==true){
                 $('#cat').css({'display':'block'});
				 $('#menu').css({'display':'block'});
				 $('#submenu').css({'display':'block'});
            }
    });
	
	
    });  
	function showtime(id)
	{
		 
		 $('#time'+id).clone().appendTo('#add'+id).addClass('col-md-offset-3');
		 
		 $('#add'+id+' '+'#from'+id+'_start_time').val('');
		 
		 $('#add'+id+' '+'#to'+id+'_end_time').val('');
	}
	
	
	function show(name)
	{
		
		$('#discount').attr('placeholder', name);
		
	}
	function getDatatbl(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn,err,selectedOption=0) {
    //alert(err);
	$.ajax({

		url: '{{url('getdatatbl')}}',
        type: "get",
        data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,
        'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn,'selectedOption':selectedOption},
        
	    success: function (data) {
		   
			$('#'+appendId).html(data);
			
		},
	    error: function (jqXHR, textStatus, errorThrown) {
	      
	    }
	});
	$('#'+err).attr('style', 'display:none');
	
}


function addcatmenu()
{
  $("#listerr").hide();
  var type =  $('input[name=type]:checked').val()
 
  if(type=='1')
  {
     var cat_id = $('#category').val();
     if(cat_id=='')
     {
                    $("#cat").focus();
                    $("#caterr").html('Please select category');
                    $("#caterr").show();
                    return false;
     }
     var cat = $('#category').find('option:selected').attr("name");
     var cat_id = $('#category').val();
     cat = '<input type="hidden" name="catid1[]" value="'+cat_id+'">'+cat;
     var menu = 'All';
     var submenu = 'All';
     var min = 100;
     var max = 999;
     var uniqueid = Math.floor(Math.random() * (max - min + 1)) + min;
  }
   if(type=='2')
  {
     var cat_id = $('#category').val();
      if(cat_id=='')
     {
                  $("#cat").focus();
                    $("#caterr").html('Please select category');
                    $("#caterr").show();
                    return false;
     }
     var menu_id = $('#menus').val();
     if(menu_id=='')
     {
                  $("#menus").focus();
                    $("#menuerr").html('Please select menu');
                    $("#menuerr").show();
                    return false;
     }
     var cat = $('#category').find('option:selected').attr("name");
     var cat_id = $('#category').val();
     cat = '<input type="hidden" name="catid2[]" value="'+cat_id+'">'+cat;
     var menu = $('#menus').find('option:selected').attr("name");
     var menu_id = $('#menus').val();
     menu = '<input type="hidden" name="menuid2[]" value="'+menu_id+'">'+menu;
     var submenu = 'All';
     var min = 100;
     var max = 999;
     var uniqueid = Math.floor(Math.random() * (max - min + 1)) + min;
  }
   if(type=='3')
  {
     var cat_id = $('#category').val();
     var menu_id = $('#menus').val();
     var submenu_id = $('#submenus').val();
     if(cat_id=='')
     {
                  $("#cat").focus();
                    $("#caterr").html('Please select category');
                    $("#caterr").show();
                    return false;
     }
     if(menu_id=='')
     {
                  $("#menus").focus();
                    $("#menuerr").html('Please select menu');
                    $("#menuerr").show();
                    return false;
     }
     if(submenu_id=='')
     {
                  $("#submenus").focus();
                    $("#submenuerr").html('Please select submenu');
                    $("#submenuerr").show();
                    return false;
     }
     var cat = $('#category').find('option:selected').attr("name");
     var cat_id = $('#category').val();
     cat = '<input type="hidden" name="catid3[]" value="'+cat_id+'">'+cat;
     var menu = $('#menus').find('option:selected').attr("name");
     var menu_id = $('#menus').val();
     menu = '<input type="hidden" name="menuid3[]" value="'+menu_id+'">'+menu;
     var submenu =  $('#submenus').find('option:selected').attr("name");
     var submenu_id = $('#submenus').val();
     submenu = '<input type="hidden" name="submenuid3[]" value="'+submenu_id+'">'+submenu;
     var min = 100;
     var max = 999;
     var uniqueid = Math.floor(Math.random() * (max - min + 1)) + min;
  }
     var  data ="<tr id="+ uniqueid + "><td width='55'>" + cat + "</td><td width='55'>" + menu + "</td><td width='55'>" + submenu + 
     "</td><td><a href='#' onclick='removedata("+uniqueid+")'>Delete</a></td></tr>";
     //alert(data);
     
     $('#table').append(data);
}
function removedata(uniqueid)
{
	 //alert(uniqueid);
	 $('#table tr#'+uniqueid).remove();
	// $('#table tbody tr #'id).remove();
}
	

  function validate_offer()
     {
		var name = $("#name").val();
        
                 if(name=="")
                 {

                    $("#name").focus();
                    $("#nameerr").html('Please enter name');
                    $("#nameerr").show();
                    return false;
                 }
        var discount_type = $("#discount_type").val();

        var discount = $("#discount").val();
        
                 if(discount=="")
                 {

                    $("#discount").focus();
                    $("#discounterr").html('Please select discount type value');
                    $("#discounterr").show();
                    return false;
                 }
				if(isNaN(discount)){
					
					$("#discount").focus();
					$("#discounterr").html('Please enter numeric value!');
				    $("#discounterr").show();
					return false;
				 }
	    var listing = document.getElementById("table").rows.length;
		if(listing<2)
		{
			
            $("#listerr").html('Please add atleast one combination of category,menu,submenu');
            $("#listerr").show();
            return false;
		}
       
        var order_service_length = $('[name="order_service[]"]:checked').length;
		if(order_service_length<1)
		{
			$("#order_service").focus();
            $("#order_serviceerr").html('Please check atleast one order service');
            $("#order_serviceerr").show();
            return false;
		}

        var validity = $("#validity:checked").val();
        
        var validity_table = $("#validity_table").val();
        var valid_from = $("#valid_from").val();
        var valid_to = $("#valid_to").val();
                 if(validity=='T')
                 {
                    if(validity_table=='')
                    {
                      $("#validity_table").focus();
                      $("#validity_tableerr").html('Please select one validity title');
                      $("#validity_tableerr").show();
                      return false;
                    }
                   
                 }
                  if(validity=='C')
                 {
                    if(valid_from=='')
                    {
                      $("#valid_from").focus();
                      $("#valid_fromerr").html('Please enter valid from');
                      $("#valid_fromerr").show();
                    return false;
                    }
                    if(valid_to=='')
                    {
                      $("#valid_to").focus();
                      $("#valid_toerr").html('Please enter valid to');
                      $("#valid_toerr").show();
                    return false;
                    }
                   
                 }
        var original_price = $("#original_price").val();
        
                 if(original_price=="")
                 {

                    $("#original_price").focus();
                    $("#original_priceerr").html('Please enter original price');
                    $("#original_priceerr").show();
                    return false;
                 }
				 if(isNaN(original_price)){
					
					$("#original_price").focus();
					$("#original_priceerr").html('Please enter numeric value!');
				    $("#original_priceerr").show();
					return false;
				 }
				 if(discount_type==1 &&  parseInt(original_price)<parseInt(discount))
				 {
					 $("#original_price").focus();
					 $("#original_priceerr").html('Please enter original price more than discount fixed value!');
				     $("#original_priceerr").show();
					 return false;
				 }
        var discount_price = $("#discount_price").val();
        
                 if(discount_price=="")
                 {

                    $("#discount_price").focus();
                    $("#discount_priceerr").html('Please enter discount price');
                    $("#discount_priceerr").show();
                    return false;
                 }
                if(isNaN(discount_price)){
					
					$("#discount_price").focus();
					$("#discount_priceerr").html('Please enter numeric value!');
				    $("#discount_priceerr").show();
					return false;
				 }
				  if( parseInt(original_price) < parseInt(discount_price))
				{
					$("#discount_price").focus();
					$("#discount_priceerr").html('Please enter discount price less than original price!');
				    $("#discount_priceerr").show();
					return false;
				}
				
				if(discount_type==1 &&  parseInt(discount_price)<parseInt(discount))
				 {
					 $("#discount_price").focus();
					 $("#discount_priceerr").html('Please enter discount price more than discount fixed value!');
				     $("#discount_priceerr").show();
					 return false;
				 }
                  document.forms["specialoffer_form"].submit();
     }


      function hideErrorMsg(id){
    
      $('#'+id).attr('style', 'display:none');
  
    
     }
	 	 $(function() {
//start datetimepickers for add categories in restaureant mngmnt module
        $('#datetimepicker25').datetimepicker({
            format: 'YYYY/MM/DD',
            useCurrent: false,
            //minDate: new Date(currentYear, currentMonth, currentDate)
        });
        $('#datetimepicker26').datetimepicker({
            format: 'YYYY/MM/DD',
            useCurrent: false,
            //minDate: new Date(currentYear, currentMonth, currentDate)
        });
        $("#datetimepicker25").on("dp.change", function (e) {
            $('#datetimepicker26').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker26").on("dp.change", function (e) {
            $('#datetimepicker25').data("DateTimePicker").maxDate(e.date);
        });
        //end datetimepickers for add categories in restaureant mngmnt module
    });

 </script>
<script>
	$(document).ready(function(){
		$('[id*="time"] + [id*="time"]').addClass("col-md-offset-3");
	});
</script>
    @endsection
   
