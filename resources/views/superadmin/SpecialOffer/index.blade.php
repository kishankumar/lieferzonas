﻿@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Special Offer Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Special Offer</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Special Offer Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="col-sm-10 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/specialoffer",'class'=>'form-horizontal','method'=>'GET')) !!}
                    <div class="col-md-8 form-group">
                        <label class="col-sm-3 control-label">Restaurant Name</label>
                        <div class="col-sm-4">
                            <select class="form-control js-example-basic-multiple" name="restaurant">
                            <option value="">Select</option>
                            @foreach($restro_names as $restro_name)
                                @if($restro_name->id==$restro_detail_id)
                                    <option value="{{$restro_name->id}}" selected >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                @else
                                    <option value="{{$restro_name->id}}" >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                @endif
                            @endforeach
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-primary" title="">Search</button>
                            <a class="btn btn-primary" href="{{url('root/shop/orders/')}}">Reset Search</a>
                        </div>
                    </div>
                    
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
             <div class="col-sm-12 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
               
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <a  href="#" class="btn btn-default DTTT_button_print" onclick="download_offers()" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable" >
                         <span >Download</span>
                     </a>
                     <a class="btn btn-danger DTTT_button_xls" onclick="delete_offers()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                         <span>Delete</span>
                     </a>
                </div>
             </div>
              <div class="clearfix"></div>
            <div class="box-body">
                
                 @if(Session::has('message'))
						<div class="alert alert-success" style="padding: 7px 15px;">
						  {{ Session::get('message') }}
						  {{ Session::put('message','') }}
						</div>
					@endif

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>Special offer name</th>
                        <th>Restaurant</th>
                        <th>Discount type</th>
                        <th>Valid from</th>
                        <th>Valid to</th>
                        <th>Discount type value</th>
                        <th>Original price</th>
                        <th>Discount price</th>
                        <th>Status(click here to change status)</th>
                        <th>Action</th>
						<th>View</th>
                    </tr>
                </thead>
                <tbody>
                    
               @if(count($specialoffers))
                    @foreach($specialoffers as $specialoffer)
                <tr>
                    <td>
                        <div class="checkbox1 icheck">
                            <label>
                                {!! Form::checkbox('offer_id',$specialoffer['id'], null, ['class' => 'offers checkBoxClass']) !!}
                                <span class="button-checkbox"></span>
                            </label>
                        </div>
                    </td>
					<td>{{ $specialoffer['name']}}</td>
                    <td>{{ $specialoffer['f_name'].' '.$specialoffer['l_name'] }}</td>
                    <td>@if( $specialoffer['discount_type_id']=='1'){{ 'Fixed'  }} @endif @if( $specialoffer['discount_type_id']=='2'){{ 'Percentage'  }}  @endif  @if( $specialoffer['discount_type_id']=='3') {{ 'One free on purchase'  }}  @endif </td>
                    <td>{{  date("d M Y", strtotime($specialoffer['valid_from']))  }}</td>
                    <td>{{  date("d M Y", strtotime($specialoffer['valid_to'])) }}</td>
                    
                    <td>&euro; {{ $specialoffer['discount_type_value'] }}</td>
                    <td>&euro; {{ $specialoffer['original_price'] }}</td>
                    <td>&euro; {{ $specialoffer['discount_price'] }}</td>
                   
                    <td> @if($specialoffer['status']==1) <i class="fa fa-check text-success fa-2xkc text-success" onclick="changeStatus(0,'<?php echo $specialoffer['id'] ?>')"></i> @endif  @if($specialoffer['status']==0) <i class="fa fa-ban text-danger fa-2xkc text-danger" onclick="changeStatus(1,'<?php echo $specialoffer['id']; ?>')"></i> 
                    @endif</td>
                    <td>
                        @if($specialoffer['status']==0) <a class="btn btn-success" href="{{url('root/specialoffer/'.$specialoffer['id'].'/edit')}}"> <i class="fa fa-edit"></i> @endif
                        
                    </a>
                    </td>
					<td>
                         <a class="btn btn-success" href="{{url('root/specialoffers/detail/'.$specialoffer['id'])}}"> View
                        
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
                {!! $specialoffers->render() !!}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->


 <!-- /.modal -->
 @endsection


    @section('script')

  <script>
    
     
    function delete_offers(){
        var len = $(".offers:checked").length;
        if(len==0){
            alert('Please select atleast one offer');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to delete offers?");
        if (r == true) {
            var selectedOffers = new Array();
            $('input:checkbox[name="offer_id"]:checked').each(function(){
                selectedOffers.push($(this).val());
            });
            $.ajax({
                url: 'specialoffers/delete',
                type: "post",
                dataType:"json",
                data: {'selectedOffers':selectedOffers, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('Records not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
	
	
	function changeStatus(status, id)
        {
            $.ajax({
                type	: "POST",
                url		: 'specialoffers/changeStatus',
                accepts	: 'application/json',
                dataType:"json",
                data	: {
                    id		    : id,
                    status		: status,
                    _token      :"{{ csrf_token() }}"
                },
                success: function (data) {
                    if(data.success==1)
                        location.reload();
                    else
                        alert('Error occured.!! Please try after some time.');
                }
            });
        }
		
		/*added by Rajlakshmi(01-06-16)*/					

	function download_offers() 
	{
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
       };
	     var len = $(".offers:checked").length;
        if(len==0){
            alert('Please select atleast one offer');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to download offers?");
        if (r == true) {
            var selectedOffers = new Array();
            $('input:checkbox[name="offer_id"]:checked').each(function(){
                selectedOffers.push($(this).val());
            });
            $.ajax({
                url: 'specialoffer/download',
                type: "post",
                dataType:"json",
                data: {'selectedOffers':selectedOffers, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);

				doc.save('specialoffer_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
		
	}
   
         /*added by Rajlakshmi(01-06-16)*/
</script>

    @endsection




