@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kitchens
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.kitchenfrontlist.index')}}"><i class="fa fa-dashboard"></i> Kitchen Management</a></li>
            <li class="active">List Kitchens</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Kitchens</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <div class="alert alert-danger" id="error" style="display:none"></div>
                        <table id="example1" class="table table-bordered table-striped" style="margin-top:10px;">
                            <thead>
                            <tr>
                                <th>Priority</th>
                                <th>Kitchen</th>
                            </tr>
                            </thead>
                            <tbody>

                            {!! Form::open(['route'=>'root.kitchenfrontlist.store','method'=>'post','onsubmit'=>'return save_changes()']) !!}
                            @foreach($data['map'] as $kitchen)
                                <tr>
                                    <td>{{$kitchen->priority}}</td>
                                    <td><div class="col-md-4">{!! Form::select('kitchen'.$kitchen->priority,$data['kitchen'], $kitchen->kitchen_id, ['class'=>'form-control']) !!} {!! Form::hidden('priority'.$kitchen->priority,$kitchen->priority) !!}</div></td>
                                </tr>
                            @endforeach
                            {!! Form::submit('Save Changes',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                            {!! Form::close() !!}

                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@endsection

@section('script')
        <script type="text/javascript">
        function save_changes()
        {
            $('#error').hide();
            var arr = [];
            for(var i = 1;i<11;i++)
            {
                var select = $('select[name=kitchen' + i + ']').val();
                if(arr.indexOf(select) != -1)
                {
                    $('#error').show();
                    $('#error').html('You cannot choose the same country twice. Duplicate found at priority '+i+' .');
                    return false;
                }
                else
                    arr.push(select);
            }
        }
    </script>
@endsection