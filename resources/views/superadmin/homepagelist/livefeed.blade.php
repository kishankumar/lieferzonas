@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Live Feed
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.livefeed.index')}}"><i class="fa fa-dashboard"></i> Live Feed Management</a></li>
            <li class="active">Live Feed Special Content</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Live Feed Content</h3>
                    </div>
                    <!-- /.box-header -->

                    {{--<div class="col-sm-12 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <a class="btn btn-default" data-toggle="modal" data-target="#add-form"
                               data-original-title><span>Add</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable"><span>Download</span></a>
                            <a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2"
                               tabindex="0" aria-controls="simpledatatable" onclick="delete_country()"><span>Delete</span></a></div>
                    </div>--}}

                    <div class="box-body">
						<div class="row">
							<div class="col-md-6">
							{!! Form::open(['route'=>'root.livefeed.store','method'=>'post']) !!}

							<div class="form-group">
								<label class="control-label">Subject</label><span class="red">*</span>
								{!! Form::text('subject',$feed->subject,['class'=>'form-control','required']) !!}
							</div>
							<div class="form-group">
								<label class="control-label">Topic</label><span class="red">*</span>
								{!! Form::text('topic',$feed->topic,['class'=>'form-control','required']) !!}
							</div>
							<div class="form-group">
								<label class="control-label">Description</label><span class="red">*</span>
								{!! Form::text('description',$feed->description,['class'=>'form-control','required']) !!}
							</div>
							<div class="form-group">
								<label class="control-label">Redirect Link</label><span class="red">*</span>
								{!! Form::text('link',$feed->link,['class'=>'form-control','required']) !!}
							</div>

							{!! Form::submit('Save Changes',['id'=>'submit','class'=>'btn  btn-primary']) !!}
							<hr>
							<p><span class="red">*</span> - Required Fields.</p>
							{!! Form::close() !!}
							</div>
						</div>
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@endsection

@section('script')

    {{--    {!! Html::script('resources/assets/js/jquery.validate.min.js') !!}
        {!! Html::script('resources/assets/js/additional-methods.js') !!}

        <script type="text/javascript">
        </script> --}}
@endsection