@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Additional Home Pages
        <small>Control panel</small>
      </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.home.pages.index')}}"><i class="fa fa-dashboard"></i>Additional Home Pages</a></li>
            <li class="active">Edit Home Page</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12">
        
          <!-- Custom tabs (Charts with tabs)-->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Edit Home Page</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
    {!! Form::model($home_page_detail,['route'=>['root.home.pages.update',$home_page_detail->id],'name'=>'editPage','id'=>'editPage','method'=>'patch','class'=>'form-horizontal form','novalidate'=>'novalidate','files'=>true]) !!}
        
        <?php
            $checkCity='';$checkState='';$checkCountry='';
            if($home_page_detail->is_type == 'city'){
                $checkCity='checked';
                $displayAttr1='block';$displayAttr2='none';$displayAttr3='none';
            }else if($home_page_detail->is_type == 'state'){
                $checkState='checked';
                $displayAttr1='none';$displayAttr2='block';$displayAttr3='none';
            }else if($home_page_detail->is_type == 'country'){
                $checkCountry='checked';
                $displayAttr1='none';$displayAttr2='none';$displayAttr3='block';
            }else{
                $checkCity='checked';
                $displayAttr1='block';$displayAttr2='none';$displayAttr3='none';
            }
        ?>
        
        <div class="form-group">
            <div class="col-md-2">
                <label class="radiobox">
                    <input type="radio" name="slugType" class="slugType" <?=$checkCity?> value="city"> 
                    <span>City</span></label>
            </div>
            <div class="col-md-2">
                <label class="radiobox">
                    <input type="radio" name="slugType" class="slugType" <?=$checkState?> value="state"> 
                    <span>State</span></label>
            </div>
            <div class="col-md-2">
                <label class="radiobox">
                    <input type="radio" name="slugType" class="slugType" <?=$checkCountry?> value="country">
                    <span>Country</span></label>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-5">
                <select class="js-example-basic-multiple form-control changeSlugData" id="selectedCity" name="selectedSlugCityId" style="display: <?php echo $displayAttr1 ?>">
                    <option value="">Select City</option>
                    @if(count($city))
                        @foreach($city as $info)
                            <option @if($home_page_detail->fk_id==$info->id) selected="selected" @endif value="{{ $info->id }}">{{ ucfirst($info->name)  }}</option>
                        @endforeach
                    @endif 
                </select>
                <select class="js-example-basic-multiple form-control changeSlugData" id="selectedState" name="selectedSlugStateId" style="display: <?php echo $displayAttr2 ?>">
                    <option value="">Select State</option>
                    @if(count($state))
                        @foreach($state as $info)
                            <option @if($home_page_detail->fk_id==$info->id) selected="selected" @endif value="{{ $info->id }}">{{ ucfirst($info->name)  }}</option>
                        @endforeach
                    @endif 
                </select>
                <select class="js-example-basic-multiple form-control changeSlugData" id="selectedCountry" name="selectedSlugCountryId" style="display: <?php echo $displayAttr3 ?>">
                    <option value="">Select Country</option>
                    @if(count($country))
                        @foreach($country as $info)
                            <option @if($home_page_detail->fk_id==$info->id) selected="selected" @endif value="{{ $info->id }}">{{ ucfirst($info->name)  }}</option>
                        @endforeach
                    @endif 
                </select>
                <span id="slugError" style="color:red"></span>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-2">Slug</div>
            <div class="col-md-4">
                <div class="form-control" id="slugName">{{ $home_page_detail->slug }}</div>
                {!! Form::hidden('slug_name',$home_page_detail->slug,['id'=>'slug_name']) !!}
                {!! Form::hidden('page_id',$home_page_detail->id,['id'=>'page_id']) !!}
                {!! Form::hidden('image_name',$home_page_detail->image,['id'=>'image_name']) !!}
                {!! Form::hidden('video_name',$home_page_detail->video,['id'=>'video_name']) !!}
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-2">Status</div>
            <div class="col-md-4">
                {!! Form::select('status',['1'=>'Active','0'=>'Inactive'], $home_page_detail->status, ['class'=>'form-control']) !!}
            </div>   
        </div>
    
    
        <div class="form-group">
            <div class="col-md-2">Upload File<span class="red">*</span></div>
            <div class="col-md-5">
                {!! Form::file('images','',array('id'=>'images','class'=>'form-control')) !!}
                <span id="imageError" style="color:red"></span>
                @if ($home_page_detail->image)
                    <img class="img-upload" src="{{asset('public/superadmin/homepage/images/'.$home_page_detail->image) }}" style="max-width: 25%">
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-2">Description 1<span class="red">*</span></div>
            <div class="col-md-8">
                {!! Form::textarea('description1',$home_page_detail->description1,['class'=>'form-control required','id'=>'description1']) !!}
                <span id="desc1Error" style="color:red"></span>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-2">Description 2<span class="red">*</span></div>
            <div class="col-md-8">
                {!! Form::textarea('description2',$home_page_detail->description2,['class'=>'form-control required','id'=>'description2']) !!}
                <span id="desc2Error" style="color:red"></span>
            </div>
        </div>
        
        <?php
            $checkUType='';$checkVType='';
            if($home_page_detail->vedio_upload_type == 'U'){
                $checkUType='checked';
                $displayAttrU='block';$displayAttrV='none';
            }else if($home_page_detail->vedio_upload_type == 'V'){
                $checkVType='checked';
                $displayAttrU='none';$displayAttrV='block';
            }else{
                $checkUType='checked';
                $displayAttrU='block';$displayAttrV='none';
            }
        ?>
        
        <div class="form-group">
            <div class="col-md-2">Video Type<span class="red">*</span></div>
            <div class="col-md-2">
                <label class="radiobox">
                    <input type="radio" name="video_type" class="urlclick" value="url" <?=$checkUType?> > 
                    <span>URL</span></label>
            </div>
            <div class="col-md-2">
                <label class="radiobox">
                    <input type="radio" name="video_type" class="uploadclick" value="ved" <?=$checkVType?>>
                    <span>Upload Video</span></label>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <!--{!! Form::text('urlName','',["id"=>'urlName','placeholder'=>'Enter Url for video',"class"=>"form-control urlshow"]) !!}-->
                
                <input type="text" name="urlName" value="<?=$home_page_detail->url?>" id="urlName" placeholder="Enter Url for video" style="display:<?=$displayAttrU?>;" class="form-control urlshow">
                <input type="file" name="upvideo" id="upvideo" style="display:<?=$displayAttrV?>;" class="uploadshow">
                
                <?php
                if($home_page_detail->video){ ?>
                    <video id="showvideo" controls src="{{asset('public/superadmin/homepage/allvedios/'.$home_page_detail->video) }}" type="video/mp4" style="display: <?=$displayAttrV?>">
                    </video>
                <?php } 
                ?>
                <span id="vedioError" style="color:red"></span>
            </div>
        </div>
        <div class="col-sm-12" style="padding:0;"><hr></div>
        <div class="form-group col-sm-12">
            {!! Form::button('Update',["id"=>'updateForm',"class"=>"btn  btn-primary"]) !!}
			<hr>
            <p><span class="red">*</span> - Required Fields.</p>
        </div>
    
    {!! Form::close() !!}
    
    </div>
        <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>
@endsection

@section('script')
    {!! Html::script('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}
    {!! Html::script('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') !!}
    
    <script>
        $city_slug = '<?php echo json_encode($city_slug);?>';
        $city_slug = JSON.parse($city_slug);
        $state_slug = '<?php echo json_encode($state_slug);?>';
        $state_slug = JSON.parse($state_slug);
        $country_slug = '<?php echo json_encode($country_slug);?>';
        $country_slug = JSON.parse($country_slug);
        
        $(function(){
            $('#updateForm').click(function() {
                $('#slugError,#desc1Error,#desc2Error,#imageError,#vedioError').html('');
                var slugType = $('input[name="slugType"]:checked').val();
                if(slugType == 'city'){
                    var selectedSlugId = $('select[name="selectedSlugCityId"]').val().trim();
                }else if(slugType == 'state'){
                    var selectedSlugId = $('select[name="selectedSlugStateId"]').val().trim();
                }else if(slugType == 'country'){
                    var selectedSlugId = $('select[name="selectedSlugCountryId"]').val().trim();
                }else{
                    var selectedSlugId = $('select[name="selectedSlugCityId"]').val().trim();
                }
                var description1=$("#description1").val().trim();
                var description2=$("#description2").val().trim();
                if(selectedSlugId == '' || selectedSlugId == 0){
                    $('#slugError').html("This field is required");
                    return false;
                }
                var slug_name = $('input[name="slug_name"]').val();
                if(slug_name == '' || slug_name == 0){
                    $('#slugError').html("Please select again");
                    return false;
                }
                
                var images = $('input[name="images"]').val().trim();
                if(images != ''){
                    var ext = $('input[name="images"]').val().split('.').pop().toLowerCase();
                    if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                        $('#imageError').html("Not Valid");
                        return false;
                    }
                }
                
                if(description1 == '' || description1 == 0){
                    $('#desc1Error').html("This field is required");
                    return false;
                }
                if(description2 == '' || description2 == 0){
                    $('#desc2Error').html("This field is required");
                    return false;
                }
                
                var video_type = $('input[name="video_type"]:checked').val();
                if(video_type == 'url'){
                    var urlName=$("#urlName").val().trim();
                    if(urlName == '' || urlName == 0){
                        $('#vedioError').html("This field is required");
                        return false;
                    }else if(urlName != ''){
                        var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
                        if (!re.test(urlName)) {
                            $('#vedioError').html("Not Valid");
                            return false;
                        }
                    }
                }else{
                    var upvideo = $('input[name="upvideo"]').val().trim();
                    if(upvideo != ''){
                        var upvideoext = $('input[name="upvideo"]').val().split('.').pop().toLowerCase();
                        if($.inArray(upvideoext, ['mp4','mpg','avi','m4v']) == -1) {
                            $('#vedioError').html("Not Valid");
                            return false;
                        }
                    }
                }
                $('#editPage').submit(); 
            });
        });
        
        function get_city_slug(city_id){
            currSlug='None';
            $.each($city_slug,function( key, value ) {
                if(key==city_id)
                    currSlug=value;
            });
            return currSlug;
        }
        function get_state_slug(state_id){
            currSlug='None';
            $.each($state_slug,function( key, value ) {
                if(key==state_id){
                    currSlug=value;
                }
            });
            return currSlug;
        }
        function get_country_slug(country_id){
            currSlug='None';
            $.each($country_slug,function( key, value ) {
                if(key==country_id){
                    currSlug=value;
                }
            });
            return currSlug;
        }
        
        $('#description1').ckeditor();
        $('#description2').ckeditor();
        
        $(function(){
            $(".changeSlugData").change(function() {
                slug='';
                var currVal = $(this).val();
                if($(this).val() != ''){
                    slugtype = $('input[name="slugType"]:checked').val();
                    if(slugtype == 'city'){
                        var slug = get_city_slug(currVal);
                    }else if(slugtype == 'state'){
                        var slug = get_state_slug(currVal);
                    }else if(slugtype == 'country'){
                        var slug = get_country_slug(currVal);
                    }
                }
                $('#slugName').text(slug);
                $('#slug_name').val(slug);
            });
            $('#selectedState + span.select2-container').hide();
			$('#selectedCountry + span.select2-container').hide();
            $(".slugType").change(function() {
                if($(this).val() == 'city'){
                    $('#selectedCity + span.select2-container').show();
                    $('#selectedState + span.select2-container').hide();
                    $('#selectedCountry + span.select2-container').hide();
                }
                else if($(this).val() == 'state'){
                    $('#selectedCity + span.select2-container').hide();
                    $('#selectedState + span.select2-container').show();
                    $('#selectedCountry + span.select2-container').hide();
                }else if($(this).val() == 'country'){
                    $('#selectedCity + span.select2-container').hide();
                    $('#selectedState + span.select2-container').hide();
                    $('#selectedCountry + span.select2-container').show();
                }else{
                    $('#selectedCity + span.select2-container').show();
                    $('#selectedState + span.select2-container').hide();
                    $('#selectedCountry + span.select2-container').hide();
                }
            });
            $('.urlclick, .uploadclick').click(function(){
                if($('.urlclick').is(':checked')){
                    $('.uploadshow').hide();
                    $('#showvideo').hide();
                    $('.urlshow').show();
                }
                else if($('.uploadclick').is(':checked')){
                    $('.urlshow').hide();
                    $('.uploadshow').show();
                    $('#showvideo').show();
                }
            });
        });
    </script>
    
@endsection


