@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Orders
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.front.orders.index')}}"><i class="fa fa-dashboard"></i> Restaurant Orders</a></li>
        <li class="active">Orders</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Restaurant Orders With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/front/orders/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/front/orders')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_orders()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
            
            <div class="box-body">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    {{ Session::put('message','') }}
                @endif
                
                <div id="delerrors" style="color: red"></div>
                
                <?php 
                    $class='<i class="fa fa-caret-down text-muted"></i>';
                    $class1='<i class="fa fa-caret-up text-muted"></i>';
                    $class2='<i class="fa fa-sort text-muted"></i>';
                ?>

            <!--<table id="example1" class="table table-bordered table-striped">-->
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        <div class="checkbox icheck">
                            <label>
                            {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                            <span class=""></span>
                            </label>
                        </div>
                    </th>
                    <th>
                        <a class="text-muted" href="{{url('root/front/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='created' ? 'created-desc' :'created';?>">
                        Date
                        <?php if($type=='created'){ echo $class; } elseif($type=='created-desc'){ echo $class1; } else { echo $class2; } ?>
                        </a>
                    </th>
                    <th>
                        <a class="text-muted" href="{{url('root/front/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='order' ? 'order-desc' :'order';?>">
                        Order ID
                        <?php if($type=='order'){ echo $class; } elseif($type=='order-desc'){ echo $class1; } else { echo $class2; } ?>
                        </a>
                    </th>
                    <th>
                        <a class="text-muted" href="{{url('root/front/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                        Name
                        <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                        </a>
                    </th>
                    <th>
                        <a class="text-muted" href="{{url('root/front/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='email' ? 'email-desc' :'email';?>">
                        Email
                        <?php if($type=='email'){ echo $class; } elseif($type=='email-desc'){ echo $class1; } else { echo $class2; } ?>
                        </a>
                    </th>
                    <th>
                        <a class="text-muted" href="{{url('root/front/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='number' ? 'number-desc' :'number';?>">
                        Number
                        <?php if($type=='number'){ echo $class; } elseif($type=='number-desc'){ echo $class1; } else { echo $class2; } ?>
                        </a>
                    </th>
                    <th>Order Status</th>
                    <th>Restaurant Name</th>
                    <th>Price</th>
                    <th>View Order</th>

					<th>Action</th>

                </tr>
            </thead>
            <tbody>
                @if(count($OrderInfo))
                    @foreach($OrderInfo as $info)
                        <tr>
                            <td>
                                <div class="checkbox icheck">
                                    <label>
                                        {!! Form::checkbox('pages', $info->id, null, ['class' => 'pages checkBoxClass']) !!}
                                        <span class="button-checkbox"></span>
                                    </label>
                                </div>
                            </td>
                            <td>{{ date("d M Y", strtotime($info->created_at))  }}</td>
                            <td>{{ ($info->order_id)  }}</td>
                            <td>{{ ($info->fname)  }}</td>
                            <td>{{ ($info->email)  }}</td>
                            <td>{{ ($info->mobile)  }}</td>
                            <td>{{ ($info->status_name)  }}</td>
                            <td>{{ ucfirst($info->f_name)  }} {{ ucfirst($info->l_name)  }}</td>
                            <td> &euro; {{ number_format($info->grand_total,2) }} </td>
                            <td>
                                <a href="{{url('root/front/orders/'.$info->order_id)}}">
                                    View Order
                                </a>
                            </td>
                            <?php if($info->front_order_status_id!=1 && $info->front_order_status_id!=3 && $info->front_order_status_id!=4) { ?>
							<td>
								<a href="{{url('root/front/orders/printbill/'.$info->order_id)}}" class="btn-border2">
								   Print Bill
								</a>
						    </td>
                            <?php } else { ?>
							
							<td>
								Not Available
						    </td>
                            <?php } ?>
                        </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Order Exist
                        </td>
                    </tr>
                 @endif
            </tbody>
        </table>
        {!! $OrderInfo->appends(Request::except('page'))->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    function delete_orders(){
        var len = $(".pages:checked").length;
        if(len==0){
            alert('Please select atleast one order');
            return false;
        }
        var r = confirm("Are you sure to delete orders?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="pages"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            $.ajax({
                url: '{{url('root/front/orders/delete')}}',
                type: "post",
                dataType:"json",
                data: {'selectedPages':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('orders not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
    
</script>
@endsection





