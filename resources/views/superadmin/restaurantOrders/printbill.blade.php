<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Print Page</title>
    {!! Html::style('resources/assets/css/bootstrap.min.css') !!}
    {!! Html::script('resources/assets/js/jQuery-2.2.0.min.js') !!}
	{!! Html::script('assets/js/jquery-ui.min.js') !!}
	{!! Html::script('resources/assets/js/bootstrap.min.js') !!}
    {!! Html::style('resources/assets/css/print-style.css') !!}
    {!! Html::style('resources/assets/css/font-awesome.css') !!}

    <style>
        td{
            width:25% !important;
            font-size:12px !important;
            line-height:22px !important;
            padding: 2px 20px !important;
        }
        .table{
            margin-bottom:0px !important;
        }
        .total{
            margin-bottom:0px !important;
        }
        .subtotal{
            border:none;
            margin:0px !important;
        }
    </style>
</head>

<body>
<!--===========================
			HEADER
 ============================== -->

<div class="container top">
    <div class="row head">
        
		
        <div class="col-sm-2 back-btn print-none">
		<a href="{{url('root/front/orders')}}" class="btn btn-default" >Back</a>
		</div>
		@if(count($rest_detail))
        <div class="col-sm-8 header">
            <h3>{{$rest_detail[0]['f_name'].' '.$rest_detail[0]['l_name']}}</h3>
            <address>
			{{ $rest_detail[0]['add1'].' '.$rest_detail[0]['add2'].' '.$rest_detail[0]['city_name'].' '.$rest_detail[0]['state_name'].
			' '.$rest_detail[0]['country_name'].' '.$rest_detail[0]['pincode'] }}
			</br>
                
            
            </address>
        </div>
        @endif
        <div class="col-sm-2 back-btn print-btn print-none">
           <input class="btn btn-default" type="button" value="Print" onClick="window.print()">
        </div>
        
       </div><!-- row ends -->
     @if(count($order_payment))
    <div class="row">

        <div class="col-sm-6 left-col">
            <ul>
                <li><strong>Order Type: </strong>{{$order_payment['0']['order_type_name']}}</li>
                <li><strong>Order#: </strong>{{$order_payment['0']['order_id']}}</li>
            </ul>
        </div>

        <div class="col-sm-6 right-col">
            <ul>
                <li><strong>User: </strong>{{$order_payment['0']['fname'].' '.$order_payment['0']['lname']}}</li>
            </ul>
        </div>


    </div><!-- row ends -->
	@endif

</div><!-- container ends -->


<!-- ======================================
            BODY
=========================================-->

<div class="container">
    <div class="row">

        
                <table class="table prod-total" id="kitchentable">

                <tbody>
				@if(count($item))
				<?php  $i=1; ?>
			    @foreach($item as $items)
				<?php $total_price = (int)$items['item_qty'] * (int)$items['item_price'];  ?>
				<tr id="printmaindiv<?=$i?>">
				<td id="printquantity4">{{$items['item_qty']}}</td>  
				<td id="printname4">{{ $items['item_name'] }}</td> 
				<td id="printprice4" class="printpricedata" style="text-align: right;">€ {{ $items['item_price'] }}</td>          
				</tr>
				<tr id="printsubul4"></tr>
				<?php $i++; ?>
				@endforeach
				@endif
				
				</tbody>
				</table>


    </div><!-- row ends -->

    @if(count($order_payment))
    <!-- subtotal table -->
    <div class="row">
        <div class="col-sm-12 subtotal table-responsive">
            <table class="table total">
                <tr><td>Discount Value</td>
                    <td style="text-align: right;">€ {{$order_payment['0']['discount_amount']}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 subtotal table-responsive">
            <table class="table total">
                <tr><td>Subtotal</td>
                    <td style="text-align: right;">€ {{$order_payment['0']['total_amount']}}</td>
                </tr>
            </table>
        </div>
    </div><!-- row ends -->
    

    <!-- vat table -->

    <div class="row">
        <div class="col-sm-12 subtotal table-responsive">
            <table class="table vat-total">
                <tr><td>Vat. Tax</td>
                    <td>
                            € 0
                    </td>
                    <td style="text-align: right;">
                            € 0
                    </td>
                </tr>
            </table>
        </div>
    </div><!-- row ends -->


    <!--service table -->

        <div class="row">
            <div class="col-sm-12 subtotal table-responsive">
                <table class="table vat-total">
                    <tr><td>Service Tax</td>
                        <td>
                        € 0
                        </td>
                        <td style=" text-align: right;">
                        € 0
                        </td>
                    </tr>
                </table>
            </div>
        </div>


    <div class="row">
        <div class="col-sm-12 subtotal table-responsive">
            <table class="table al-total">
                <tr><td>Grand Total</td>
                    <td style="text-align: right;">€ {{$order_payment['0']['grand_total']}}</td>
                </tr>
            </table>
        </div>
    </div><!-- row ends -->
    @endif
    
    @if(count($customer_detail))
    <!--customer address -->
   
    <div class="row">
        <div class="col-sm-12 customer">
            <h2>Customer Address</h2>
            <ul class="customer-table">
                <li>Name: {{$customer_detail['0']['booking_person_name']}} </li>
                <li>Phone: {{$customer_detail['0']['mobile']}}</li>
                <li>
				Address:{{$customer_detail['0']['address'].' '.$customer_detail['0']['landmark'].' '.$customer_detail['0']['city_name']
				.' '.$customer_detail['0']['state_name'].' '.$customer_detail['0']['country_name'].' '.$customer_detail['0']['zipcode']}} 
				</li>
                <!--<li>Driver Note: N/A</li>-->
            </ul>
        </div>
    </div><!-- row ends -->
    @endif
           

    <div class="row">
        <div class="col-sm-12 thanku">
            <p>     *************************************<br>
                              Accha Khao , Accha Khilao    </p>
        </div>
    </div><!-- row ends -->

</div><!-- container ends -->



</body>
</html>
