@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order Detail
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.front.orders.index')}}"><i class="fa fa-dashboard"></i> Restaurant Orders</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-tags"></i> Order Detail</h3>

					
                    <span class="pull-right">
                        <a class="btn btn-primary" href="{{url('root/front/orders')}}">Back</a>
                    </span>
					
					<span class="pull-right">
                        <a class="btn btn-primary" data-toggle="modal" data-target="#order-track" data-original-title><span>Track Order</span></a>
                    </span>

                </div>
                <!-- /.box-header -->
                
        @if(count($OrderInfo))
            @foreach($OrderInfo as $info)
                <div class="box-body">
                    <div class="row form-horizontal">
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Restaurant :</label></div>
                            <div class="col-md-8">{{ ucfirst($info->f_name).' '.ucfirst($info->l_name)  }}</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Order Id :</label></div>
                            <div class="col-md-8">{{$info->order_id}}</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>User Name :</label></div>
                            <div class="col-md-8">{{ ucfirst($info->fname)  }} {{ ucfirst($info->lname)  }}</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Email Id :</label></div>
                            <div class="col-md-8">{{$info->email}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Phone  :</label></div>
                            <div class="col-md-8">{{$info->mobile}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Created  :</label></div>
                            <div class="col-md-8">{{date('d M Y',strtotime($info->created_at))}} </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Address  :</label></div>
                            <div class="col-md-8"> {{$info->address}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Landmark  :</label></div>
                            <div class="col-md-8"> {{$info->landmark}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Country  :</label></div>
                            <div class="col-md-8"> {{$info->country_name}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>State  :</label></div>
                            <div class="col-md-8"> {{$info->state_name}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>City  :</label></div>
                            <div class="col-md-8"> {{$info->city_name}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Zip code  :</label></div>
                            <div class="col-md-8"> {{$info->zipcode}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Order Amount  :</label></div>
                            <div class="col-md-8">&euro; {{$info->grand_total}}	</div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Order Status  :</label></div>
                            <div class="col-md-8"> {{$info->status_name}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Order :</label></div>
                            <div class="col-md-8">
                                <?php if($info->time_type==1){
                                   echo 'Immediate Order'; 
                                }else{
                                    echo 'Pre Order';
                                }?>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Order Delivery Time  :</label></div>
                            <div class="col-md-8">
                                <?php if($info->time_type==2){
                                    if(($info->user_time !='0000-00-00 00:00:00') || (strtotime($info->user_time)>0)){
                                        echo date('d M Y h:i:s A',strtotime($info->user_time));
                                    }else{
                                        echo 'NO Time';
                                    }
                                }else{
                                    if(($info->delivery_time !='0000-00-00 00:00:00') || (strtotime($info->delivery_time)>0)){
                                        echo date('d M Y h:i:s A',strtotime($info->delivery_time));
                                    }else{
                                        echo 'No Time';
                                    }
                                }?>
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Payment Method  :</label></div>
                            <div class="col-md-8">
                                <?php if($info->payment_method==1){
                                   echo 'Cash on Delivery'; 
                                }else{
                                    echo 'Online';
                                }?>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Order Type  :</label></div>
                            <div class="col-md-8">
                                <?php if($info->order_type==1){
                                   echo 'Delivery'; 
                                }else{
                                    echo 'Takeaway';
                                }?>
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Order Paid  :</label></div>
                            <div class="col-md-8">
                                <?php if(count($TransactionInfo)>0){
                                   echo 'Yes'; 
                                }else{
                                    echo 'No';
                                }?>
                            </div>
                        </div>
                        
                        <?php  $grand_total =$info->grand_total ;  $discount_amount=$info->discount_amount; ?>
                    </div>
                </div>
            @endforeach
        @endif
        <div class="box-header">
            <h3 class="box-title text-success"> Item Detail :</h3>
            <hr>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline t-list">
                        @if(count($ItemInfo))
                            @foreach($ItemInfo as $info)
                                <li><i>{{ $info->item_qty }} </i>{{ ucfirst($info->item_name) }}</li>
                                <li> &euro; {{ number_format($info->item_price,2) }}</li>
<!--                                <li><i></i> <strong class="text-success">Extra desire </strong></li>
                                <li></li>
                                <li><i>+</i> Lorem Ipsum is simply </li>
                                <li>1.00 €</li>
                                <li><i>+</i> Lorem Ipsum is simply </li>
                                <li>1.00 €</li>
                                <li>&nbsp;</li>
                                <li>&nbsp;</li>-->
                            @endforeach
                        @endif
						<li><strong>Discount Amount</strong></li>
                        <li><strong>&euro;{{ $discount_amount }}</strong></li>
                        <li><strong>Total Amount</strong></li>
                        <li><strong>&euro; {{ number_format($grand_total,2) }}</strong></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
    </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

	
	<!--added by Rajlakshmi(02-06-16)-->
	<div id="download" style="display:none;" >
	</div>
   <!--added by Rajlakshmi(02-06-16)-->

<div class="modal fade" id="order-track" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="panel panel-primary theme-border">
      <div class="panel-heading theme-bg">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="panel-title" id="contactLabel"> Track</h4>
      </div>
      <div class="modal-body">
    <table class="table table-bordered table-striped">
   <thead>
    <tr>
     <th>Status</th>
     <th>Date</th>
    </tr>
   </thead>
   <tbody>
   @if(count($order_status))
   @foreach($order_status as $status)
    <tr>
     <td>{{ $status->status_name }}</td>
     <td>{{ date("d M Y H:i:s", strtotime($status->created_at)) }}</td>
    </tr>
   @endforeach
   @endif
   </tbody>
  </table>    
     </div>
 </div>
  </div>
</div>

@endsection

@section('script')

@endsection





