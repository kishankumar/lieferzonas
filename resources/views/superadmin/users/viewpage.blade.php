@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.user.index')}}"><i class="fa fa-dashboard"></i>Users</a></li>
        <li class="active">User Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-tags"></i> Users Detail</h3>
                    <span class="pull-right">
                        <a class="btn btn-primary" href="{{url('root/user/')}}">Back</a>
                    </span>
                </div>
                <!-- /.box-header -->
                
        @if(count($viewDetails))
            @foreach($viewDetails as $info)
                    
                <div class="box-body">
                    <div class="row form-horizontal">
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>User Name :</label></div>
                            <div class="col-md-8">{{ ucfirst($info->first_name).' '.ucfirst($info->last_name)  }}</div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Email Id :</label></div>
                            <div class="col-md-8">
                                {{$info->email}}
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Role  :</label></div>
                            <div class="col-md-8">
                                {{$info->role_name}}
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Phone  :</label></div>
                            <div class="col-md-8">{{$info->contact}}	</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>DOB  :</label></div>
                            <div class="col-md-8">
                                @if(strtotime($info->dob)>0)
                                    {{ date("d M Y", strtotime($info->dob))  }}
                                @else
                                    {{ 'None' }}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>DOJ  :</label></div>
                            <div class="col-md-8">
                                @if(strtotime($info->doj)>0)
                                    {{ date("d M Y", strtotime($info->doj))  }}
                                @else
                                    {{ 'None' }}
                                @endif	
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Address1 :</label></div>
                            <div class="col-md-8">
                                {{$info->add1}}
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Address2 :</label></div>
                            <div class="col-md-8">{{ ($info->add2)  }}</div>
                        </div>
                        
                        <?php 
                            $CounDetail = \App\superadmin\User::country_data($info->country); 

                            $Sdetail = \App\superadmin\User::state_data($info->state); 

                            $CDetail = \App\superadmin\User::city_data($info->city);

                            $ZipDetail = \App\superadmin\User::zip_data($info->zipcode);

                        ?>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Country :</label></div>
                            <div class="col-md-8">
                                <div class="col-md-8">{{ $CounDetail ? $CounDetail->name : ''  }}</div>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>State :</label></div>
                            <div class="col-md-8">
                                <div class="col-md-8">{{ ($Sdetail ? $Sdetail->name : '')  }}</div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>City :</label></div>
                            <div class="col-md-8">
                                <div class="col-md-8">{{ ($CDetail ? $CDetail->name : '')  }}</div>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Zip Code :</label></div>
                            <div class="col-md-8">
                                <div class="col-md-8">{{ ($ZipDetail ? $ZipDetail->name : '')  }}</div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Status :</label></div>
                            <div class="col-md-8">
                                @if($info->status == 1) 
                                    <i class="fa fa-check text-success text-success"></i>
                                @else
                                    <i class="fa fa-ban text-danger text-danger" ></i>
                                @endif
                            </div>
                        </div>
                        
                    </div>
                </div>
            @endforeach
        @endif
            </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
    </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

	
	<!--added by Rajlakshmi(02-06-16)-->
	<div id="download" style="display:none;" >
	</div>
   <!--added by Rajlakshmi(02-06-16)-->


    @endsection

@section('script')

@endsection





