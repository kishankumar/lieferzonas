@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Users Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.user.index')}}"><i class="fa fa-dashboard"></i>Users</a></li>
        <li class="active">User List</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/user/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/user/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    <a class="btn btn-default" data-toggle="modal" href="{{ url('root/user/create') }}" >
                        <span>Add</span>
                    </a>
              <!--  <a class="btn btn-default DTTT_button_csv" onclick="edit_users()" id="ToolTables_simpledatatable_1" tabindex="0" aria-controls="simpledatatable">
                        <span>Edit</span>
                    </a>-->
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>
                     <a class="btn btn-danger DTTT_button_xls" onclick="delete_users()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                         <span>Delete</span>
                     </a>
                </div>
            </div>
             <div class="clearfix" style=""></div>
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
					  {{ Session::get('message') }}
					  {{ Session::put('message','') }}
                    </div>
                @endif
                
                <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
                ?>
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div>
                                <label  class="checkbox icheck">
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th> <a href="{{url('root/user/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Name <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/user/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='email' ? 'email-desc' :'email';?>">
                            Email Id <?php if($type=='email'){ echo $class; } elseif($type=='email-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/user/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='role' ? 'role-desc' :'role';?>">
                            Role <?php if($type=='role'){ echo $class; } elseif($type=='role-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Contact</th>
                        <!--<th>City</th>
                        <th>State</th>-->
                        <th>Status</th>
                        <th>Action</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    
                @if(count($usersinfo))
                    @foreach($usersinfo as $info)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('user_id', $info['id'], null, ['class' => 'users checkBoxClass']) !!}
                                <span class="button-checkbox"></span>
                            </label>
                        </div>
                    </td>
                    <td>
                        <a href="{{url('root/user/'.$info['id'])}}">
                            {{ ucfirst($info['first_name'])  }}
                        </a>
                    </td>
                    <td>{{ $info['email']  }}</td>
                    <td>{{ $info['role_name']  }}</td>
                    <td>{{ $info['contact']  }}</td>
                    <!--<td>{{ $info['city']  }}</td>
                    <td>{{ $info['state']  }}</td>-->
                    <td> @if($info['status']==1)<i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($info['status']==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>
                    <td>
                        <a class="btn btn-success" href="{{url('root/user/'.$info['id'].'/edit')}}">
                        <i class="fa fa-edit"></i>
                        </a>
                    </td>
                 <td><a href="#"   onclick="showModal1('addform','<?php echo $info['id']; ?>')" >Set Permission</a></td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
                
                {!! $usersinfo->appends(Request::except('page'))->render() !!}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 


     <!-- /.modal -->
 
  <div class="modal fade" id="addform" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="panel panel-primary theme-border">
      <div class="panel-heading theme-bg">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add New</h4>
      </div>
      <div class="modal-body row">
        <div class="col-md-5">
          <div class="box move_list_1" id="move_list_1">
            <ul class="box-body list-unstyled" id="pagelist">
             
            </ul>
             
             
          </div>
         
        </div>
         {!! Form::open(array('url' => 'root/user/add_access','id'=>'access_form','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}
        <div class="col-md-2 text-center">
      <a href="javascript:void(0)" id="l_to_r"><i class="fa fa-long-arrow-right fa-lg"></i></a>
      <br><br>
      <div class="clearfix"></div>
      <a href="javascript:void(0)" id="l_to_l"><i class="fa fa-long-arrow-left fa-lg"></i></a>
    </div>

        <div class="col-md-5">
          <div class="box move_list_1" id="move_list_2">
            <ul class="box-body list-unstyled">
            </ul>
          </div>
        </div>
        <input type="hidden" name="userid"  id="userid">
        <div class="col-md-12 text-right">
      <button type="submit" class="btn  btn-primary"><i class="fa fa-plus"></i> Add </button>
    </div>
    </div>


     <div id="access_list"  style="display:none;" >
      <div>
         
         
          
           @foreach ($access as $access)
           <label class="checkbox-inline chk_1_ml"><input type="checkbox" 
           name="access[<?=$access['id'] ?>]" value="<?=$access['id'] ?>"><?=$access['access_name'] ?></label>
            
           @endforeach
   
      </div>
    </div>
	<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
    {!! Form::close() !!}
      </div>
    </div>
    
   

  </div>



 <!-- /.modal -->
 @endsection


    @section('script')

  <script>
    function showModal1(id,user_id)
    {
      
        $("#"+id).modal("show");
        
        $('#userid').val(user_id);
        
              $.ajax({
                url: '{{url('getaccess')}}',
                type: "GET",
                data: {'user_id':user_id},
                success: function(res)
                {
                     
                     var received =jQuery.parseJSON(res);
                    
                     $("#move_list_1").html(received['data1']);
                     $("#move_list_2").html(received['data']);
             
                    
                }
            });
 
        
    }

     
    function delete_users(){
        var len = $(".users:checked").length;
        if(len==0){
            alert('Please select atleast one user');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to delete users?");
        if (r == true) {
            var selectedUsers = new Array();
            $('input:checkbox[name="user_id"]:checked').each(function(){
                selectedUsers.push($(this).val());
            });
            $.ajax({
                url: 'users/delete',
                type: "post",
                dataType:"json",
                data: {'selectedUsers':selectedUsers, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
						
                    }
                    else
                    {
                        alert('Records not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
</script>

<script>

  
  function ltor(id)
  {
    
      if($("#"+id).hasClass('active'))
      {
        $("#"+id).removeClass('active');

      }
      else{
        $("#"+id).addClass('active');
      }
  };
 
  
  $('#l_to_r').click(function()
  {
	 var count = $('#move_list_1 #pagelist li.active').length;
	
    
    for(i=1;i<=count;i++)
	{
		var j = $('#move_list_1 li').closest('.active').attr('id');
		
		if($('#move_list_1 li#'+j).hasClass('active'))
		{
			data_access = $('#access_list').html();
		var actid = $('#move_list_1 li#'+j).closest('.active').attr('id');
		
	   
		var moveli = $('#move_list_1 ul').find('li.active#'+j).html();
		
		data_access = data_access.replace(/access/g,'access'+actid);
		
	  

		$('#move_list_2 > ul').append("<li id='right_id"+actid+"'  onclick='rtol("+actid+")'>"+ moveli + data_access +'</li>').removeClass('active');

		$('#move_list_1 > ul').find('li.active#'+j).remove();
		//var obj = $('#right_id'+actid+' checkbox');

		}
	}
    
  });
  

  function rtol(id)
  {
     
    
    if($("#right_id"+id).hasClass('active'))
    {
      $("#right_id"+id).removeClass('active');

    }
    else{
      $("#right_id"+id).addClass('active');
    }
  };
  
  $('#l_to_l').click(function()
  {
     var count = $('#move_list_2 #pagelist li.active').length;
   
    for(i=1;i<=count;i++)
	{
		var j = $('#move_list_2 li').closest('.active').attr('id');
		if($('#move_list_2 li').hasClass('active'))
		{
	        
			var moveli2_li = $('#move_list_2 ul li.active div').remove();
			
			var moveli2 = $('#move_list_2 ul').find('li.active').html();
			var actid = $('#move_list_2 li#'+j).closest('.active#'+j).attr('id');
			var actid = actid.substring(8);
		  
			

			$('#move_list_1 > ul').append("<li id='"+actid+"'  onclick='ltor("+actid+")' class=''>"+ moveli2  +'</li>').removeClass('active');

			$('#move_list_2 > ul').find('li.active#'+j).remove();
		

		}
	}
  });

    function add_user_id(id)
    {
        $('#user_id').val(id);
    }

    function showaccesslist()
    {
      $("#accesslist").show();
     
    }
	
	
	/*added by Rajlakshmi(31-05-16)*/					

	function download_users() 
	{
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
       };
	    
		var len = $(".users:checked").length;
        if(len==0){
            alert('Please select atleast one user');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        
        var r = confirm("Are you sure to downlod user list?");
        if (r == true) {
            var selectedUsers = new Array();
            $('input:checkbox[name="user_id"]:checked').each(function(){
                selectedUsers.push($(this).val());
            });
			 
            $.ajax({
                url: 'userss/download',
                type: "post",
                dataType:"json",
                data: {'selectedUsers':selectedUsers, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("userlist.pdf");
				}
            });
        } else {
            return false;
        }
		
	}
    
         /*added by Rajlakshmi(31-05-16)*/
</script>
    @endsection




