@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')


<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.user.index')}}"><i class="fa fa-dashboard"></i>Users</a></li>
        <li class="active">Edit User</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('errmessage'))
				<div style="color:red;">
                    {{ Session::get('errmessage') }}
                    
                    {{ Session::put('errmessage','') }}
				</div>
                @endif
                
                @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <?php
				
				  if(!empty($get_altemail))
				  {
					  $altemail = $get_altemail['0']['email']; 
				  }
				  else
				  {
					  $altemail= '';
				  }
				  
				  if(!empty($get_altcontact))
				  {
					  $altcontact = $get_altcontact['0']['contact']; 
				  }
				  else
				  {
					  $altcontact= '';
				  }
				?>
                
    {!! Form::model($users,['route'=>['root.user.update',$users->id],'method'=>'patch','id' => 'user_form','class' => 'form','novalidate' => 'novalidate', 'files' => true])	!!}
                
        <div class="modal-body" style="padding: 5px;">
            <div class="panel-body">
                      
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Firstname') !!}<span class="red">*</span>
                            {!! Form::text('firstname',$users->first_name,["id"=>'firstname',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                                
                      <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Lastname') !!}<span class="red">*</span>
                            {!! Form::text('lastname',$users->last_name,["id"=>'lastname',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Phone') !!}<span class="red">*</span>
                            {!! Form::text('phone',$users->contact,["id"=>'phone',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                                
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Alternate Phone') !!}
                            {!! Form::text('altphone',$altcontact,["id"=>'altphone',"class"=>"form-control","Onkeyup"=>"checkno()"]) !!}
                        </div>
                    </div>
                </div>  
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                             {!! Form::label('Email') !!}<span class="red">*</span>
                               {!! Form::email('email',$users->email,["id"=>'email',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                                
                    <div class="col-md-6">
                        <div class="form-group">
                           {!! Form::label('Alternate Email') !!}
                           {!! Form::email('altemail',$altemail,["id"=>'altemail',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                </div>  
                <div class="row">
                 <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Password') !!}<span class="red">*</span>
                           {!! Form::text('password',$users->password,array("id"=>'password',"class"=>"form-control")) !!}
                        </div>
                    </div>
                   <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Profile pic') !!}
                             <input type="file" name="pics" id="pics"  onchange="validateimage(this)" >
                            @if ($users->profile_pic)
                           <img class="img-upload" src="{{asset('public/superadmin/uploads/users/'.$users->profile_pic) }}" style="max-width: 25%">
                            @endif
                           
                            
                        </div>
                    </div>
                                
                    
                </div> 
                
                
                            
                <div class="form-group">
                    {!! Form::label('Address1') !!}<span class="red">*</span>
                    {!! Form::text('address1',$users->add1,["id"=>'address1',"class"=>"form-control"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Address2') !!}
                    {!! Form::text('address2',$users->add2,["id"=>'address2',"class"=>"form-control"]) !!}
               </div>
                 <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Country') !!}<span class="red">*</span>
                            <select id="country" name="country" class="form-control js-example-basic-multiple"  onchange="getData(this.value,'countries','states','state','id','name','id','country_id')">
                              <option value="">Select Country</option>
                                 @foreach($country as $country)
                                        <option <?php if($country['id'] == $users->country) echo "Selected='selected'" ;?> 
                                        value="<?=$country['id'] ?>"><?=$country['name'] ?>
                                </option>
                                    @endforeach  
                            </select>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('State') !!}<span class="red">*</span>
                             <select id="state" name="state" class="form-control js-example-basic-multiple" onchange="getData(this.value,'states','cities','city','id','name','id','state_id')">
                              <option value="">Select State</option>
                                 @foreach($state as $state)
                                        <option <?php if($state['id'] == $users->state) echo "Selected='selected'" ;?> 
                                        value="<?=$state['id'] ?>"><?=$state['name'] ?>
                                </option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                 
                </div>
                <div class="row">
                   
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('City') !!}<span class="red">*</span>
                           <select id="city" name="city" class="form-control js-example-basic-multiple" onchange="getData(this.value,'cities','zipcodes','zipcode','id','name','id','city_id')">
                               <option value="">Select City</option>
                                 @foreach($city as $city)
                                        <option <?php if($city['id'] == $users->city) echo "Selected='selected'" ;?> 
                                        value="<?=$city['id'] ?>"><?=$city['name'] ?>
                                </option>
                                    @endforeach 
                                
                           </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                            {!! Form::label('zipcode') !!}<span class="red">*</span>
                           <select id="zipcode" name="zipcode" class="form-control js-example-basic-multiple" >
                               <option value="">Select Zipcode</option>
                                 @foreach($zipcode as $zipcode)
                                        <option <?php if($zipcode['id'] == $users->zipcode) echo "Selected='selected'" ;?> 
                                        value="<?=$zipcode['id'] ?>"><?=$zipcode['name'] ?>
                                </option>
                                    @endforeach 
                                
                           </select>
                        </div>
                    </div>
                   
                </div>
                
               
							
            <div class="row">
            <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Role') !!}<span class="red">*</span>
                            <select id="role" name="role" class="form-control">
                                <option value="">Select Role</option>
                                    @foreach($roles as $role)
                                        <option <?php if($role['id'] == $users->user_role_id) echo "Selected='selected'" ;?> value="<?=$role['id'] ?>"><?=$role['role_name'] ?></option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('DOB') !!}<span class="red">*</span>
                      {!! Form::input('text', 'dob', $users->dob, ["id"=>"datetimepicker5","class" => 'form-control']) !!}
                    </div>
                </div>
               
              </div>
             
              <div class="row">
              <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('DOJ') !!}<span class="red">*</span>
                        {!! Form::input('text', 'doj', $users->doj, ["id"=>"datetimepicker6","class" => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                                    {!! Form::label('Status') !!}
                                {!! Form::select('status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ), $users->status, ['id' => 'status','class' => 'form-control']) !!}
                                    <span id="statusErr" style="color:red"></span>
                 </div>
                </div>
              </div>
                <div class="form-group margin-top">
                    {!! Form::submit('Update',["id"=>'submit',"class"=>"btn  btn-primary"]) !!}
                    <hr>
                    <p><span class="red">*</span> - Required Fields.</p>
                </div>
            </div>
        </div>
            {!! Form::close() !!}
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>
     @endsection


    @section('script')
     <script type="text/javascript"> 
     $(document).ready(function()
	 {
	  $.validator.addMethod("email", function(value, element)
	  {
	  return this.optional(element) || /^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
	  }, "Please enter a valid email address.");
     $("#user_form").validate({
      rules: {
		  firstname: {
            required: true
               },
        lastname: {
            required: true
               },
         password: {
            required: true
               },
         phone: {
            required: true,
            digits:true,
            minlength:10,
            maxlength:12
               },
         email: {
            required: true,
            email:true,
		
			
               },
		 altemail: {
		 email:true 
		 },
			
         role: {
            required: true
               },
         address1: {
            required: true
               },     
         city: {
            required: true
               },
         state: {
            required: true
               },
         country: {
            required: true
               },
         zipcode: {
            required: true
               },
         dob: {
            required: true
               },
         doj: {
            required: true
               }
         
        },
       messages: {

                    firstname: "Please enter firstname",
                    lastname: "Please enter lastname",
                    password: "Please enter password",
                    phone:{
                        required: "Please enter phone number",
                   
                        minlength: "Please enter valid phone number",
                        maxlength: "Please enter valid phone number"
                    },
                    email:{
                        required: "Please enter email",
                        email: "Please enter valid email"
                    },
					altemail:{
                        
                        email: "Please enter valid alternate email"
                    },
                    role: "Please enter role",
                    address1: "Please enter address",
                    city: "Please enter city",
                    state: "Please enter state",
                    country: "Please enter country",
					zipcode: "Please enter zipcode",
                   
                    dob:"Please enter date of birth",
                    doj:"Please enter date of joining",
                    agree: "Please accept our policy"
                },
         
     });
});
    function checkno()
	{
		var phone = $('#phone').val();
		var altphone = $('#altphone').val();
		if(phone == altphone)
		{
			alert('Alternate phone no. should not be same as phone no. ');
			$('#altphone').val('');
		}
	}
	
    /* function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn) {

         $.ajax({

                url: '{{url('getdata')}}',
                type: "get",
                data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn,
				'selectedOption':},
				
                
             success: function (data) {
             
           $('#'+appendId).html(data);
           
          },
             error: function (jqXHR, textStatus, errorThrown) {
               
             }
         });
   
      }*/
	  
	  function validateimage(fieldObj)
		{
			
			var FileName  = fieldObj.value;
			var FileExt = FileName.substr(FileName.lastIndexOf('.')+1);
			var FileSize = fieldObj.files[0].size;
			var FileSizeMB = (FileSize/5485760).toFixed(2);

		   if ( (FileExt != "jpg" && FileExt != "jpge" && FileExt != "png" && FileExt != "tif") )
			{
				var error = "File type : "+ FileExt+"\n\n";
				error += "Please make sure profile pic is in jpg,png,tif format.\n\n";

				alert(error);
				return false;
			}
			return true;
		}
    </script>

    @endsection
   
   

