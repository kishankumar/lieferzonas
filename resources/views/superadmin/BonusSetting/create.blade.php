@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Assign Customer Bonus
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.assignbonus.create')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
            <li class="active">Assign Customer Bonus</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
        
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Assign Customer Bonus With Full Features</h3>
                    </div>

                {{--    Error Display--}}
                @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{--    Error Display ends--}}

            <div class="panel-body">       
            <div class="panel panel-primary theme-border">
                <div class="panel-heading theme-bg">
                    <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Assign Customer Bonus</h4>
                </div>
                    
                <div class="modal-body" style="padding: 5px;">
                    <div class="panel-body">
                        
                        @if(Session::has('message'))
                            <div class="alert alert-success" style="padding: 7px 15px;">
                                {{ Session::get('message') }}
                                {{ Session::put('message','') }}
                            </div>
                        @endif
                                
                        {!! Form::open(array('route'=>'root.assignbonus.create','id'=>'searchingForm','class'=>'form-horizontal','method'=>'GET')) !!}
                        
                            <div class="col-md-12 form-group">
                                <label class="col-sm-2 control-label">Restaurant Name<span class="red">*</span></label>
                                <div class="col-sm-4">
                                    @if($restro_detail_id)
                                        <select class="form-control" disabled name="restaurant">
                                    @else
                                        <select class="form-control js-example-basic-multiple" name="restaurant" id="restaurant" onChange="hideErrorMsg('restauranterr')">
                                    @endif
                                    <option value="">Select</option>
                                    
                                    @foreach($restro_names as $restro_name)
                                        @if($restro_name->id==$restro_detail_id)
                                            <option value="{{$restro_name->id}}" selected >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                        @else
                                            <option value="{{$restro_name->id}}" >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                    <span id="restauranterr" class="text-danger"></span>
                                </div>
                                
                                @if(!$restro_detail_id)
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" type="button" onclick="validate_rest()">Get Customer Bonus Status</button>
                                    </div>
                                @endif
                            </div>
                            <div class="clearfix"></div>
                        {!! Form::close() !!}
							 
            @if($restro_detail_id)
                <div>
                    {!! Form::open(array('route'=>'root.assignbonus.store','class'=>'form-horizontal')) !!}
                        <hr>
                        <div class="col-md-12 form-group">
                            <label class="col-sm-2 control-label">&nbsp;&nbsp;</label>
                            <div class="col-sm-9">
                                <label class="radiobox">
                                    <input type="radio" name="bonus_status" value='1' class="val_table" <?php if($bonus_status=='1'){ echo 'checked';}?> > 
                                    <span>ON</span>
                                </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="radiobox">
                                    <input type="radio" name="bonus_status" value='0' class="val_date" <?php if($bonus_status=='0'){ echo 'checked';} ?>> 
                                    <span>OFF</span>
                                </label>
                            </div>
                        </div>
                        {!! Form :: hidden('restro_id',$restro_detail_id,['id'=>'restro_id'])  !!}
                        <div class="col-md-12 form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                <a class="btn btn-primary" href="{{url('root/assignbonus/create/')}}">Cancel</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            @endif
        </div>
    </div>  
    </div>
    </div>
    </div>
         
    <!-- quick email widget -->

    </section>
        
    <!-- right col -->
    </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('script')
<script>
    function hideErrorMsg(id){
        $('#'+id).attr('style', 'display:none');
    }
    function validate_rest()
    {
        restaurant = $('#restaurant').val();
        if(restaurant=='')
        {
            $("#restaurant").focus();
            $("#restauranterr").html('Please select restaurant');
            $("#restauranterr").show();
            return false;
        }
        document.forms["searchingForm"].submit();
    }
</script>
@endsection
