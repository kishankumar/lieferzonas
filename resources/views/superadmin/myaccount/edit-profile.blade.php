@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-12 text-right" id="asdfg" style="margin-bottom:10px;"> 
             
             </div>
            
             <div class="box-body">
    
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    <div class="alert alert-danger">
                                <ul>
                            @foreach($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                                </ul>
                    </div>
                    
                @endif
                
{!! Form::model($info,['route'=>['root.profile.update',$info['0']['id']],'method'=>'patch',
'id' => 'edit_profile','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
    <div class="modal-body" style="padding: 5px;">
        
        <div class="panel-body">
            <div class="errors" id="errors" style="color: red"></div>
            
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                    {!! Form::label('First name') !!}
                    {!! Form::text('f_name',$info['0']['first_name'],["id"=>'f_name',"class"=>"form-control"]) !!}
                  
                       
                    </div>

                </div>
                {!! Form::hidden('user_master_id',$info['0']['mid'],['id'=>'user_master_id']) !!}
				{!! Form::hidden('email_id',$info['0']['email'],['id'=>'email_id']) !!}
				{!! Form::hidden('mobile_id',$info['0']['contact'],['id'=>'mobile_id']) !!}
                <div class="col-md-5">
                    <div class="form-group">
                    {!! Form::label('Last name') !!}
                    {!! Form::text('l_name',$info['0']['last_name'],["id"=>'l_name',"class"=>"form-control"]) !!}
                  
                      
                    </div>

                </div>
            </div>
			
			<div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                    {!! Form::label('Email') !!}
                    {!! Form::text('email',$info['0']['email'],["id"=>'email',"class"=>"form-control"]) !!}
                  
                   
                    </div>

                </div>
				<div class="col-md-5">
                    <div class="form-group">
                    {!! Form::label('Mobile no') !!}
                    {!! Form::text('mobile_no',$info['0']['contact'],["id"=>'mobile_no',"class"=>"form-control"]) !!}
                  
                        
                    </div>

                </div>
                
            </div>
			
			 <div class="row">
                 <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('zipcode') !!}
                            {!! Form::text('zipcode',$info['0']['zipcode'],["id"=>'zipcode',"class"=>"form-control" ]) !!}
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('Country') !!}
                               <select id="country" name="country" class="form-control"  onchange="getData(this.value,'countries','states','state','id','name','id','country_id')">
                              <option value="">Select Country</option>
                                 @foreach($country as $country)
                                        <option <?php if($country['id'] == $info['0']['country']) echo "Selected='selected'" ;?> 
                                        value="<?=$country['id'] ?>"><?=$country['name'] ?>
                                </option>
                                    @endforeach  
                            </select>
                        </div>
                    </div>
                   
                </div>
               <div class="row">
                 <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('State') !!}
                             <select id="state" name="state" class="form-control" onchange="getData(this.value,'states','cities','city','id','name','id','state_id')">
                              <option value="">Select State</option>
                                 @foreach($state as $state)
                                        <option <?php if($state['id'] == $info['0']['state']) echo "Selected='selected'" ;?> 
                                        value="<?=$state['id'] ?>"><?=$state['name'] ?>
                                </option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('City') !!}
                           <select id="city" name="city" class="form-control" >
                               
                            <option value="">Select State</option>
                                 @foreach($city as $city)
                                        <option <?php if($city['id'] == $info['0']['city']) echo "Selected='selected'" ;?> 
                                        value="<?=$city['id'] ?>"><?=$city['name'] ?>
                                </option>
                                    @endforeach     
                            </select>
                        </div>
                    </div>
                   
                </div>
				<div class="row">
					 <div class="col-md-5">
						 <div class="form-group">
							{!! Form::label('Address1') !!}
							{!! Form::text('address1',$info['0']['add1'],["id"=>'address1',"class"=>"form-control"]) !!}
						</div>
					 </div>
					<div class="col-md-5">
						<div class="form-group">
							{!! Form::label('Address2') !!}
							{!! Form::text('address2',$info['0']['add2'],["id"=>'address2',"class"=>"form-control"]) !!}
						</div>
					</div>
				</div>
            
            <div class="form-group margin-top">
               {!!Form::submit('Edit',["id"=>'submit',"class"=>"btn  btn-primary"]) !!}
                <hr>
            </div>
        </div>
    </div>
            {!! Form::close() !!}
              
           
    </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 
 @endsection


    @section('script')

 
<script>

	 $(document).ready(function(){
     $("#edit_profile").validate({
		
      rules: {
        f_name: {
            required: true
               },
        l_name: {
            required: true
               },
        
       
         email: {
            required: true,
            email:true
               },
		 mobile_no: {
			 required:true,
			 digits:true,
			 minlength:10,
             maxlength:12
		 },
         
         zipcode: {
            required: true
               },
         country: {
            required: true
               },
         state: {
            required: true
               },			   
         city: {
            required: true
               },
         address1: {
            required: true
               }			   
         
         
         
        },
       messages: {
                    f_name: "Please enter firstname",
                    l_name: "Please enter lastname",
                    email:{
                        required: "Please enter email",
                        email: "Please enter valid email"
                    },
                    mobile_no:{
                        required: "Please enter mobile no",
                        digits: "Please enter valid mobile no",
						minlength: "Please enter valid phone number",
                        maxlength: "Please enter valid phone number"
                    },
                    zipcode: "Please enter zipcode",
					country: "Please enter country",
					state: "Please enter state",
					city: "Please enter city",
                    address1: "Please enter address",
                   
                    agree: "Please accept our policy"
                },
         
     });
});
	
	var doc = new jsPDF();
	var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
	};

	$('#cmd').click(function () {
		doc.fromHTML($('#example1').html(), 1, 1, {
			
				'elementHandlers': specialElementHandlers
		});
		doc.save('user-list.pdf');
	})
    
</script>
    @endsection




