﻿@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Profile</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-12 text-right" id="asdfg" style="margin-bottom:10px;"> 
                
             </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif

              <table class="table-condensed">
				 
											<tr>
												<th>Name:</th>
												<td>{{ ucfirst($info['0']['first_name']).' '.$info['0']['last_name']  }}</td>
											</tr>
											<tr>
												<th>Mobile:</th>
												<td>{{$info['0']['contact'] }}</td>
											</tr>
											<tr>
												<th>Email:</th>
												<td>{{$info['0']['email'] }}</td>
											</tr>
											<tr>
												<th>Address:</th>
												<td>{{$info['0']['add1'] }} <?php if ($info['0']['add2']!='') { echo ','; }?>{{ $info['0']['add2'] }}</td>
											</tr>
											
											<tr>
												<th>City:</th>
												<td>{{$info['0']['city_name'] }}</td>
											</tr>
											<tr>
												<th>State:</th>
												<td>{{$info['0']['state_name'] }}</td>
											</tr>
											<tr>
												<th>Pincode:</th>
												<td>{{$info['0']['zipcode'] }}</td>
											</tr>
											<tr>
												<th>Country:</th>
												<td>{{$info['0']['country_name'] }}</td>
											</tr>
											<tr><th><a  href="{{url('root/profile/'.$info['0']['id'].'/edit')}}">Edit</a></th></tr>
		    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 


 @endsection


    @section('script')

  <script>
    function showModal1(id,user_id)
    {
      
        $("#"+id).modal("show");
        
        $('#userid').val(user_id);
        
              $.ajax({
                url: '{{url('getaccess')}}',
                type: "GET",
                data: {'user_id':user_id},
                success: function(res)
                {
                     
                     var received =jQuery.parseJSON(res);
                    
                     $("#move_list_1").html(received['data1']);
                     $("#move_list_2").html(received['data']);
             
                    
                }
            });
 
        
    }

     
    function delete_users(){
        var len = $(".users:checked").length;
        if(len==0){
            alert('Please select atleast one user');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to delete users?");
        if (r == true) {
            var selectedUsers = new Array();
            $('input:checkbox[name="user_id"]:checked').each(function(){
                selectedUsers.push($(this).val());
            });
            $.ajax({
                url: 'users/delete',
                type: "post",
                dataType:"json",
                data: {'selectedUsers':selectedUsers, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('Records not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
</script>

<script>

  
  function ltor(id)
  {
    
      if($("#"+id).hasClass('active'))
      {
        $("#"+id).removeClass('active');

      }
      else{
        $("#"+id).addClass('active');
      }
  };
 
  
  $('#l_to_r').click(function()
  {
	 var count = $('#move_list_1 #pagelist li.active').length;
	
    
    for(i=1;i<=count;i++)
	{
		var j = $('#move_list_1 li').closest('.active').attr('id');
		
		if($('#move_list_1 li#'+j).hasClass('active'))
		{
			data_access = $('#access_list').html();
		var actid = $('#move_list_1 li#'+j).closest('.active').attr('id');
		
	   
		var moveli = $('#move_list_1 ul').find('li.active#'+j).html();
		
		data_access = data_access.replace(/access/g,'access'+actid);
		
	  

		$('#move_list_2 > ul').append("<li id='right_id"+actid+"'  onclick='rtol("+actid+")'>"+ moveli + data_access +'</li>').removeClass('active');

		$('#move_list_1 > ul').find('li.active#'+j).remove();
		//var obj = $('#right_id'+actid+' checkbox');

		}
	}
    
  });
  

  function rtol(id)
  {
     
    
    if($("#right_id"+id).hasClass('active'))
    {
      $("#right_id"+id).removeClass('active');

    }
    else{
      $("#right_id"+id).addClass('active');
    }
  };
  
  $('#l_to_l').click(function()
  {
     var count = $('#move_list_2 #pagelist li.active').length;
   
    for(i=1;i<=count;i++)
	{
		var j = $('#move_list_2 li').closest('.active').attr('id');
		if($('#move_list_2 li').hasClass('active'))
		{
	        
			var moveli2_li = $('#move_list_2 ul li.active div').remove();
			
			var moveli2 = $('#move_list_2 ul').find('li.active').html();
			var actid = $('#move_list_2 li#'+j).closest('.active#'+j).attr('id');
			var actid = actid.substring(8);
		  
			

			$('#move_list_1 > ul').append("<li id='"+actid+"'  onclick='ltor("+actid+")' class=''>"+ moveli2  +'</li>').removeClass('active');

			$('#move_list_2 > ul').find('li.active#'+j).remove();
		

		}
	}
  });

    function add_user_id(id)
    {
        $('#user_id').val(id);
    }

    function showaccesslist()
    {
      $("#accesslist").show();
     
    }
	
	
	var doc = new jsPDF();
	var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
	};

	$('#cmd').click(function () {
		doc.fromHTML($('#example1').html(), 1, 1, {
			
				'elementHandlers': specialElementHandlers
		});
		doc.save('user-list.pdf');
	})
    
</script>
    @endsection




