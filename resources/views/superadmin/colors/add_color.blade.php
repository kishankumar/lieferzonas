
<div class="modal fade" id="add-form1" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close modalClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="panel-title new_category_title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add Color .</h4>
            </div>
            
            {!! Form::open(['route'=>'root.alergic-contents.store','id'=>'add-types-form'])!!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
                <div class="modal-body" style="padding: 5px;">
                    
                    <div class="panel-body">
                        <div class="errors" id="errors" style="color: red"></div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Color Name') !!}<span class="red">*</span>
                                    {!! Form::text('color','',["id"=>'color',"class"=>"form-control",'onblur' => 'trimmer("color")']) !!}
                                    <span id="category_nameErr" style="color:red"></span>
                                </div>
                            </div>
                            {!! Form::hidden('color_id','',['id'=>'color_id']) !!}
                            <div class="col-md-6" id="show_status" style="display: none">
                                <div class="form-group">
                                    {!! Form::label('Status') !!}
                                    {!! Form::select('status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ), 1, ['id' => 'status','class' => 'form-control']) !!}
                                    <span id="statusErr" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('Color Code') !!}<span class="red">*</span>
                            <div class="input-group my-colorpicker2 colorpicker-element">
                                {!! Form::text('color_code','',["id"=>'color_code',"class"=>"form-control",'onblur' => 'trimmer("color_code")']) !!}
                                <div class="input-group-addon" id="change_color">
                                    <i></i>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group margin-top">
                            {!! Form::button('Add',["id"=>'add_new_category',"class"=>"btn add_category btn-primary"]) !!}
                            <hr>
							<p><span class="red">*</span> - Required Fields.</p>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
				
            </div>
        </div>
    </div>