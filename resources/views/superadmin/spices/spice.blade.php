@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Spice
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.spices.index')}}"><i class="fa fa-dashboard"></i>Spices</a></li>
            <li class="active">Spice</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
			
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Spices</h3>
                    </div>
					
                    <!-- /.box-header -->
                    
                    <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                        {!! Form::open(array('url'=>"root/spices/",'class'=>'form-inline','method'=>'GET')) !!}
                            {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                            {!! Form::submit('Search',['id'=>'ssss','class'=>'btn btn-primary']) !!}
                            <a class="btn btn-primary" href="{{url('root/spices/')}}">Reset Search</a>
                        {!! Form::close() !!}
                    </div>
                   
                    <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <a class="btn btn-default"  data-toggle="modal"  data-original-title href="#" onclick="spice(0)"><span>Add</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" onclick="download_spice()" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable"><span>Download</span></a>
                            <a class="btn btn-danger DTTT_button_xls" onclick="delete_spice()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                                <span>Delete</span>
                            </a>
                        </div>
                    </div>
                    
                     <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
                    <div class="clearfix" style="margin: 20px 0;"></div>

                    <div class="box-body">
					@if(Session::has('message'))
						<div class="alert alert-success" style="padding: 7px 15px;">
						  {{ Session::get('message') }}
						  {{ Session::put('message','') }}
						</div>
                    @endif
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                                    <div class="checkbox icheck">
                                        <label>
                                        {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                        <span class=""></span>
                                        </label>
                                    </div>
                                </th>
                                <th>
                                    <a href="{{url('root/spices/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                                    Name
                                    <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('root/spices/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                                    Description
                                    <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($spices))
                            @foreach($spices as $spice)
                            <tr>
                                <td>
                                    <div class="checkbox1 icheck">
                                        <label>
                                            {!! Form::checkbox('spice_id', $spice['id'], null, ['class' => 'spice checkBoxClass']) !!}
                                            <span class="button-checkbox"></span>
                                        </label>
                                    </div>

                                     </td>
                                    <td>{{$spice['name']}}</td>
                                    <td>{{ (strlen($spice['description']) > 50) ? substr($spice['description'],0,50).'...' : $spice['description']  }}</td>
                                    <td>@if ($spice['image']!='')
                                       <img class="img-upload" src="{{asset('public/superadmin/uploads/spices/'.$spice['image']) }}" style="max-width: 5%">
                                            @endif
                                    </td>
                                  

									
                                    <td> @if($spice['status']==1) <i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($spice['status']==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>

                                    <td><a  class="btn btn-success" href="#" onclick="spice({{$spice['id']}})"> <i class="fa fa-edit"></i></a></td>

                            </tr>
                            @endforeach
                            @else 
                                <tr>
                                    <td colspan="8" align="center">
                                        No Record Exist
                                    </td>
                                </tr>
                            @endif
                            </tbody>

                        </table>
                        
                        {!! $spices->appends(Request::except('page'))->render() !!}
                        
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<div class="modal fade" id="spice-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Spice
            </div>

                <div class="modal-body" style="padding: 5px;">
                    <div class="panel-body">
                        @if($errors->any())
						<div class="alert alert-danger ">
							<ul class="list-unstyled">
								@foreach($errors->all() as $error)
									<li> {{ $error }}</li>
								@endforeach
							</ul>
						</div>
                        @endif
						
						@if(Session::has('errmsg'))
						<div class="alert alert-danger" style="padding: 7px 15px;">
						  {{ Session::get('errmsg') }}
						  {{ Session::put('errmsg','') }}
						</div>
                        @endif
                       
                       {!! Form::open(array('url' => 'root/spices/add_spice','id'=>'spice','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}



                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name</label><span class="red">*</span>
                                       
                                        {!! Form::text('name','',['id'=>'name','class'=>'form-control']) !!}
                                         {!! Form::hidden('spice_id','0',['id'=>'spice_id','class'=>'form-control']) !!}
                                    </div>
                                </div>
                             


                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="control-label">Status</label>
                                        {!! Form::select('status',['1'=>'Active','0'=>'Deactive'], '', ['id'=>'status','class'=>'form-control']) !!}
                                    </div>

                                </div>

                            </div>
							<div class="row">

                              <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Image</label>
                                      
									    <input type="file" name="image" id="pics"  onchange="validateimage(this)" >
                                       <div id="image" style="display:none;"><img class="img-upload" src="" id="img" style="max-width: 25%"></div>
                                     
                           
                                    </div>

                                </div>
                             

                               

                               

                            </div>
					        <div class="row">

                             
                             

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Description</label><span class="red">*</span>
                                        {!! Form::textarea('description','',['id'=>'description','class'=>'form-control required']) !!}
                                    </div>

                                </div>

                               

                            </div>

                            <div class="form-group margin-top">
                               
                                {!! Form::submit('Add ',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                                <hr>
                                <p><span class="red">*</span> - Required Fields.</p>
                            </div>
                        {{--</form>--}}
                        {!! Form::close() !!}
                    </div>

                </div>

        </div>
    </div>
</div>

   <!--added by Rajlakshmi(02-06-16)-->
	<div id="download" style="display:none;" >
	</div>
   <!--added by Rajlakshmi(02-06-16)-->
   
@endsection

@section('script')
<script type="text/javascript">

 $(document).ready(function(){
     $("#spice").validate({
      rules: {
		  name: {
            required: true
               },
        description: {
            required: true
               },
         
        },
       messages: {

                    name: "Please enter name",
                    description: "Please enter description",
                    agree: "Please accept our policy"
                },
         
     });
});

function delete_spice(){
        var len = $(".spice:checked").length;
        if(len==0){
            alert('Please select atleast one spice');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to delete Spices?");
        if (r == true) {
            var selectedSpices = new Array();
            $('input:checkbox[name="spice_id"]:checked').each(function(){
                selectedSpices.push($(this).val());
                

            });
            $.ajax({
                url: 'spices/delete',
                type: "post",
                dataType:"json",
                data: {'selectedSpices':selectedSpices, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('Records not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
	
	
	function spice(id)   
     {
		
        var base_host = window.location.origin;
        var spice_id = id;
     
        if(spice_id !='0')
        {
			 $('#image').show();
             $("#submit").val('Update');
             $.ajax({
                url: '{{ url('fetchspicedatas') }}',
                type: "GET",
                dataType:"json",
                data: {'spice_id':spice_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
						 
						var image = res.image;
						if(image=='')
						{
							$('#image').hide();
						}
                        var image = base_host+"/lieferzonas/public/superadmin/uploads/spices/"+image;
						
                        $('#name').val(res.name);
						$("#img").attr("src", image);
                        $('#description').val(res.description);
						$('#status').val(res.status);
                        $('input[name=spice_id]').val(res.id);
                        showModal('spice-form');
                    }
                    else
                    {
						$('#image').hide();
                        document.getElementById("spice-form").reset();
                    }
                }
            });
        }
       else
       {
             
             showModal('spice-form');
             $("#submit").val('Add');
             $('#image').hide();
             document.getElementById("spice").reset();
            
       }
     }
	 
	 /*added by Rajlakshmi(02-06-16)*/
	
		function download_spice(){
        var len = $(".spice:checked").length;
        if(len==0){
            alert('Please select atleast one spice');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to download Spices?");
        if (r == true) {
            var selectedSpices = new Array();
            $('input:checkbox[name="spice_id"]:checked').each(function(){
                selectedSpices.push($(this).val());
                

            });
            $.ajax({
                url: 'spices/download',
                type: "post",
                dataType:"json",
                data: {'selectedSpices':selectedSpices, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                      $("#download").html(res.data1);
					    
                        var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Spices List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
					    doc.save("spices_list.pdf");
				}
            });
        } else {
            return false;
        }
    }
	function validateimage(fieldObj)
		{
			
			var FileName  = fieldObj.value;
			var FileExt = FileName.substr(FileName.lastIndexOf('.')+1);
			var FileSize = fieldObj.files[0].size;
			var FileSizeMB = (FileSize/5485760).toFixed(2);

		   if ( (FileExt != "jpg" && FileExt != "jpge" && FileExt != "png" && FileExt != "tif") )
			{
				var error = "File type : "+ FileExt+"\n\n";
				error += "Please make sure profile pic is in jpg,png,tif format.\n\n";

				alert(error);
				return false;
			}
			return true;
		}
	 /*added by Rajlakshmi(02-06-16)*/
	</script>
    @if($errors->any())
        <script type="text/javascript">
         showModal('spice-form');
        </script>
    @endif
	@if(Session::has('errmsg'))
	  <script type="text/javascript">
         showModal('spice-form');
      </script>					
	@endif
@endsection