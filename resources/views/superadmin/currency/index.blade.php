@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Currency
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.currency.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
        <li class="active">Currency</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Currency With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/currency/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/currency/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default"  data-toggle="modal" onclick="add_types(0)" data-original-title><span>Add</span></a>
                    <!--<a class="btn btn-default DTTT_button_csv"  onclick="add_types(1)" data-toggle="modal" data-original-title><span>Edit</span></a>-->
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_contents()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_contents()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
            </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            <div class="clearfix"></div>
            
            <div class="box-body">
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>
                            <a href="{{url('root/currency/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Currency
                            <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/currency/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='code' ? 'code-desc' :'code';?>">
                            Currency Code
                            <?php if($type=='code'){ echo $class; } elseif($type=='code-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php //echo '<pre>';print_r($categoies);die;?> 
                @if(count($currencie))
                    @foreach($currencie as $info)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('currencies', $info->id, null, ['class' => 'currencies checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ ucfirst($info->name)  }}</td>
                    <td>{{ ucfirst($info->currency_code)  }}</td>
                    <td>
                        @if($info->status == 1) 
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-success" href="javascript:void(0)" onclick="add_types({{$info->id}})">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $currencie->appends(Request::except('page'))->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
	<!--added by Rajlakshmi(02-06-16)-->
	<div id="download" style="display:none;" >
	</div>
   <!--added by Rajlakshmi(02-06-16)-->
	
    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    function add_types(edit=0){
        $('#currency_nameErr,#statusErr,#currency_codeErr').html('');
        $('#errors').html('');
        $('.error').html('');
        if(edit){
            $('.new_category_title').html('<span class="glyphicon glyphicon-plus"></span> Edit Currency');
            $('#add_new_currency').text('Update');
            $.ajax({
                url: '{{route("root.currency.edit")}}',
                type: "GET",
                dataType:"json",
                data: {'checkedCurrency':edit, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        $('#show_status').show();
                        $('#name').val(res.name);
                        $('#status').val(res.status);
                        $('#currency_code').val(res.currency_code);
                        $('input[name=currency_id]').val(res.id);
                        //$("#add-form1").modal("show");
                        showModal('add-form1');
                    }
                    else
                    {
                        document.getElementById("add-currency-form").reset();
                    }
                }
            });
        }else{
            document.getElementById("add-currency-form").reset();
            $('.new_category_title').html('<span class="glyphicon glyphicon-plus"></span> Add Currency.');
            $('#show_status').hide();
            $('#add_new_currency').text('Add');
            $('input[name=currency_id]').val(0);
            showModal('add-form1');
            //$("#add-form1").modal("show");
        }
    }
    
    function delete_contents(){
        var len = $(".currencies:checked").length;
        if(len==0){
            alert('Please select atleast one currency');
            return false;
        }
        var base_url1=base_url+'currency/delete';
        var r = confirm("Are you sure to delete currencies?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="currencies"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedCurrency':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('currencies not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
    
    $(function(){
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-zA-ZäÄöÖüÜß«» -]+$/i.test(value);
        }, "Name must contain only letters.");
        
        $("#add-currency-form").validate({
            rules:
            {
                name:
                {
                    required : true,
                    minlength:3,
                    maxlength:100,
                    loginRegex:true
                },
                currency_code:
                {
                    required : true,
                    minlength:3
                }
            },
            messages: {
                "name": {
                    //required: "You must enter a Username",
                    loginRegex: "Name format not valid"
                }
            }
        });
        
        $("#add_new_currency").unbind("click").click(function(){
            if( $("#add-currency-form").valid())
            {
                $.ajax({
                    url: '{{ url('root/currency') }}',  //working
                    type: "post",
                    dataType:"json",
                    data: $('#add-currency-form').serialize(),
                    success: function(res){
                        if(res.success==1)
                        {
                            document.getElementById("add-currency-form").reset();
                            location.reload();
                        }
                        else
                        {
                            var errorsHtml= '';
                            $.each(res.errors, function( key, value ) {
                                errorsHtml += value + '<br>'; 
                            });
                            $('#errors').html(errorsHtml);
                        }
                    }
                });
            }
        });
        
        $(".modalClose").click(function(){
            $('#currency_nameErr,#statusErr,#currency_codeErr').html('');
            $('.error').html('');
            $('#errors').html('');
            document.getElementById("add-currency-form").reset();
        });
    });
	
	/*added by Rajlakshmi(02-06-16)*/
		function download_contents(){
        var len = $(".currencies:checked").length;
        if(len==0){
            alert('Please select atleast one currency');
            return false;
        }
        var base_url1=base_url+'currency/download';
        var r = confirm("Are you sure to download currencies?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="currencies"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedCurrency':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("currency_list.pdf");
				}
            });
        } else {
            return false;
        }
    }
	
	 /*added by Rajlakshmi(02-06-16)*/
</script>
@endsection

@include('superadmin.currency.add_currency')





