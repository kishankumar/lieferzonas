@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Search Filters
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.searchfilter.index')}}"><i class="fa fa-dashboard"></i> Search Filters Management</a></li>
            <li class="active">View Search Filters</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Search Filters</h3>
                    </div>
                    <!-- /.box-header -->

                <div class="col-sm-12 text-right" style="margin-bottom:10px;">
                    <div class="btn-group">
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable" onclick="download_searchfilter()"><span>Download</span></a>

                    </div>
					</div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                                <th>Filter Name</th>
                                <th>Low Value</th>
                                <th>High Value</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Status (Click to change)</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($filters as $filter)
                                <tr>
                                    <td>
                                        <div class="checkbox icheck">
                                            <label>{!! Form::checkbox('filter_check',$filter->id,false,['id'=>'check','class'=>'filter_check checkBoxClass']) !!} <span> </span></label>
                                        </div>
                                    </td>
                                    <td>{{$filter->name}}</td>
                                    <td>{{$filter->low_value}}</td>
                                    <td>{{$filter->high_value}}</td>
                                    <td>{{$filter->users['username']}}</td>
                                    <td>{{date('d M Y',strtotime($filter->created_at))}}</td>
                                    <td><?php if($filter->status==1){ ?><i class="fa fa-check text-success fa-2xkc text-success" onclick="changeStatus(0,'<?php echo $filter->id ?>')"></i><?php } else{ ?><i class="fa fa-ban text-danger fa-2xkc text-danger" onclick="changeStatus(1,'<?php echo $filter->id; ?>')"></i><?php } ?></td>
                                    <td><a class="btn btn-default DTTT_button_csv" href="{{ "searchfilter/".$filter->id."/edit"}}"><span>Edit</span></a></td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
               

                <!-- quick email widget -->
				</div>
					
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
@endsection

@section('script')

    <script type="text/javascript">

        function changeStatus(status, id)
        {
            $.ajax({
                type	: "POST",
                url		: '{{url('root/searchfilter/changeStatus')}}',
                accepts	: 'application/json',
                dataType:"json",
                data	: {
                    id		    : id,
                    status		: status,
                    _token      :"{{ csrf_token() }}"
                },
                success: function (data) {
                    if(data.success==1)
                        location.reload();
                    else
                        alert('Error occured.!! Please try after some time.');
                }
            });
        }
		/*added by Rajlakshmi(31-05-16)*/
		function download_searchfilter(){
            var len = $(".filter_check:checked").length;
            if(len==0){
                alert('Please select atleast one Search filter');
                return false;
            }

            var r = confirm("Are you sure to download the selected Tips.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="filter_check"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: 'searchfilter/download',
                    type: "post",
                    dataType:"json",
                    data: {'selectedSearchfilter':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);

				doc.save('Searchfilter_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
    }
	
		 
		 /*added by Rajlakshmi(31-05-16)*/
    </script>
@endsection