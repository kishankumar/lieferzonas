@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Access
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('root.accesslist.index')}}"><i class="fa fa-dashboard"></i> Access Management</a></li>
                <li class="active">Edit Access List</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">


                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="box">
                        {{--<div class="box-header">
                            <h3 class="box-title">Super Admin Roles</h3>
                        </div>--}}
                        <div class="panel panel-primary theme-border">
                            <div class="panel-heading theme-bg">
                                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-edit"></span> Edit Access.</h4>
                            </div>

                            <div class="modal-body" style="padding: 5px;">
                                <div class="panel-body">
                                    @if($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li> {{$error}} </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {{--<form role="form">--}}
                                    {!! Form::model($access,
                                    ['route'=>['root.accesslist.update',$access->id],
                                    'method'=>'patch'

                                    ])

                                    !!}


                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Access Name</label>
                                                {{--<input type="text" id="name" class="form-control">--}}
                                                {!! Form::text('access_name',$access->access_name,['class'=>'form-control']) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label class="control-label">Description</label>
                                                {!! Form::textarea('description',$access->description,['class'=>'form-control']) !!}
                                            </div>

                                        </div>

                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label class="control-label">Status</label>
                                                {!! Form::select('status',['1'=>'Active','0'=>'Inactive'], $access->status, ['class'=>'form-control']) !!}
                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group margin-top">
                                        {{-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add</button>--}}
                                        {!! Form::submit('Save Changes',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                                        <hr>
                                        <p><span class="red">*</span> - Required Fields.</p>
                                    </div>
                                    {{--</form>--}}
                                    {!! Form::close() !!}
                                </div>

                            </div>

                        </div>

                </section>
                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

@endsection