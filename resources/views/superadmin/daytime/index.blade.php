@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Day Time Management
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.day-time.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
        <li class="active">Day Time Management</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">

              <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
<!--                    <div class="box-header">
                        <h3 class="box-title">Day Time Management With Full Features</h3>
                    </div>-->
                    <div class="box-header">
                        @foreach($restInfo as $infor)
                            <h3 class="box-title">Restaurant Name : {{ ucfirst($infor->f_name)  }} {{ ucfirst($infor->l_name)  }}</h3>
                        @endforeach
                    </div>
                    
                    <div class="box-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" style="padding: 7px 15px;">
                                {{ Session::get('message') }}
                                {{ Session::put('message','') }}
                            </div>
                        @endif
                        
                        @if($errors->any())
                            <div class="alert alert-danger ">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="clearfix"></div>  
                        
    <div class="col-lg-12">
        <div class="box">
            {!! Form::open(['route'=>'root.day-time.store','id'=>'add-time']) !!}
            {!! Form::hidden('restaurant_id',$restaurantid,['id'=>'restaurant_id']) !!}
            <div class="box-body">
                @foreach($days as $info)
                    <label class="checkbox checkbox-inline check_click">
                        {!! Form::checkbox('days_'.$info['day'], $info['day'].'-'.$info['id'], null, ['class' => 'days']) !!}
                        <span>{{ $info['day'] }}</span>
                    </label>
                    
                    <div id="check_show_<?=$info['day'];?>-<?=$info['id'];?>" style="margin-left:23px; display:none;">
                        <div class="row">
                            <div class="col-sm-6 col-md-3 col-lg-2" id="startTime-<?=$info['id']?>">
                                <div class="form-group">
                                    {!! Form::label('Start Time') !!}
                                    {!! Form::select('from_'.$info['day'].'[]',$fromtime,1,['id'=>'from-'.$info['day'].'-'.$info['id'].'-1','class'=>'fromTime form-control']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-2" id="endTime-<?=$info['id']?>">
                                <div class="form-group">
                                    {!! Form::label('End Time') !!}
                                    {!! Form::select('to_'.$info['day'].'[]',$totime,1,['id'=>'to-'.$info['day'].'-'.$info['id'].'-1','class'=>'toTime form-control']) !!}
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-3 col-lg-4">
                                <label >&nbsp;</label>
                                <label class="checkbox" style="margin: 5px 0px;">
                                    <a href="javascript:void(0)" onclick='addMoreTime("<?=$info['id']?>","<?=$info['day'];?>")'>Add More</a>
                                    {!! Form::checkbox('is_open_'.$info['day'], $info['day'].'-'.$info['id'],null,['id'=>'is_open-'.$info['id'],'class'=>'is_open']) !!}
                                    <span></span>Open For 24 Hours
                                </label>
                            </div>
                        </div>
                        <span class="status" id="statusErr_{{$info['day']}}-{{ $info['id'] }}-1" style="color:red"></span>
                        <div id="append_<?=$info['id']?>"></div>
                    </div>
                    <hr>
                @endforeach
            </div>
            <div class="form-group">
                {!! Form::button('ADD',["id"=>'add',"class"=>"btn  btn-primary"]) !!}
            </div>
            {!! Form::close()   !!}
        </div>
    </div>
                    
                    </div>
                </div>
            </section>
        </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
</div>
    <!-- /.content-wrapper -->

    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function addMoreTime(dayId,dayName){
        if(dayId=='' || dayId==0 || isNaN(dayId)){
            return false;
        }
        if($('#is_open-'+dayId).is(':checked')){
            alert('Restaurant is already opened for 24 Hours');
            return false;
        }
        var data1=$('#startTime-'+dayId).clone();
        var data2=$('#endTime-'+dayId).clone();
        //console.log(data1[0]['innerHTML']);
        var data1= data1[0]['innerHTML'];
        var data2 = data2[0]['innerHTML'];
        
        var count = $('select[name="from_'+dayName+'[]"]').length;
        var count1 = count+1;
        var html = '<div class="row '+dayName+'"><div class="from'+dayName+' col-sm-6 col-md-3 col-lg-2" id="cloneFromId_'+dayName+'-'+count1+'">'+data1+'</div>';
        html += '<div class="to'+dayName+' col-sm-6 col-md-3 col-lg-2" id="cloneToId_'+dayName+'-'+count1+'">'+data2+'</div>';
        html += '<div class="col-sm-6 col-md-3 col-lg-2"><div>&nbsp;</div><a onclick="removemember(this,'+dayId+',\''+dayName+'\');" class="" href="javascript:void(0);">Remove ×</a></div>';
        html += '<div class="clearfix"></div><span id="statusErr_'+dayName+'-'+dayId+'-'+count1+'" class="myStatusErr status" style="color:red;padding-left: 14px;"></span></div>';
        $(html).appendTo('#append_'+dayId);
        
        $('#cloneFromId_'+dayName+'-'+count1).find('select').each(function() {
            var resId1 = $(this).attr('id').split("-");
            var dayNo = parseInt(resId1[3])+count;
            $(this).attr('id','from-'+dayName+'-'+dayId+'-'+dayNo);
        });
        $('#cloneToId_'+dayName+'-'+count1).find('select').each(function() {
            var resId2 = $(this).attr('id').split("-");
            var dayNo = parseInt(resId2[3]) + count;
            $(this).attr('id','to-'+dayName+'-'+dayId+'-'+dayNo);
        });
        //$('#addRow_'+dayName+'-'+count).append('<span id="statusErr_'+dayName+'-'+count3+'" class="status" style="color:red">sssss</span>');
        //data[0]['id'] = data[0]['id'].replace(id,id+'-'+count);
    }
    
    function removemember(that,dayId,dayName)
    {
        $(that).parent().parent().remove();
        var count = $('.'+dayName).length;
        if(count>0){
            var start=1;
            $('.'+dayName).each(function(){
                that=$(this);
                start = start+1;
                $(that).find('.from'+dayName).attr('id','cloneFromId_'+dayName+'-'+start);
                $(that).find('.to'+dayName).attr('id','cloneToId_'+dayName+'-'+start);
                $(that).find('.fromTime').attr('id','from-'+dayName+'-'+dayId+'-'+start);
                $(that).find('.toTime').attr('id','to-'+dayName+'-'+dayId+'-'+start);
                $(that).find('span').attr('id','statusErr_'+dayName+'-'+dayId+'-'+start);
            });
        }
    }
    
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    $(document).ready(function(){
        $(".days").click(function(){
            if($(this).is(':checked')){
                $('#check_show_'+$(this).val()).show();
            }
            else{
                $('#check_show_'+$(this).val()).hide();
            }
        });
        
        $(".is_open").click(function(){
            var res = $(this).val().split("-");
            //var id = res[1];alert(id)
            if($(this).is(':checked')){
                $('#from-'+$(this).val()+'-1').val($('#from-'+$(this).val()+'-1'+' option:first').val());
                $('#to-'+$(this).val()+'-1').val($('#to-'+$(this).val()+'-1'+' option:last').val());
                $('#from-'+$(this).val()+'-1').attr("disabled", true);
                $('#to-'+$(this).val()+'-1').attr("disabled", true);
                //$('#statusErr_'+$(this).val()).html('');
                $('.status').html('');
                $('#append_'+res[1]).hide();
            }
            else{
                $('#from-'+$(this).val()+'-1').removeAttr('disabled');
                $('#to-'+$(this).val()+'-1').removeAttr('disabled');
                $('#to-'+$(this).val()+'-1').val($('#to-'+$(this).val()+'-1'+' option:first').val());
                $('#append_'+res[1]).show();
            }
        });
        
        $("#add").click(function(){
            $('.status').html('');
            var len = $(".days:checked").length;
            if(len==0){
                alert('Please set time for atleast one day');
                return false;
            }var error=0;
            $('.days:checked').each(function(){
                var res = $(this).val().split("-");
                var dayName = res[0];
                var dayId = res[1];
                if($('#is_open-'+dayId).is(':checked')){
                }else{
                    var count = $('select[name="from_'+dayName+'[]"]').length;
                    for(i=1;i<=count;i++)
                    {
                        var startTime = $('#from-'+dayName+'-'+dayId+'-'+i).val();
                        var endTime = $('#to-'+dayName+'-'+dayId+'-'+i).val();
                        st = minFromMidnight(startTime);
                        et = minFromMidnight(endTime);
                        if(st>=et){
                            var errorsHtml = 'End time must be greater than start time';
                            $('#statusErr_'+dayName+'-'+dayId+'-'+i).html(errorsHtml);
                            error = 1;
                        }
                    }
                }
            });
            if(error == 1){
                return false;
            }
            $('#add-time').submit();
        });
    });
    
    function minFromMidnight(tm){
        var ampm= tm.substr(-2);
        var clk = tm.substr(0, 5);
        var m  = parseInt(clk.match(/\d+$/)[0], 10);
        var h  = parseInt(clk.match(/^\d+/)[0], 10);
        if(ampm.match(/pm/i)){
            if(h != 12){
                h += (ampm.match(/pm/i))? 12: 0;
            }
        }
        return h*60+m;
    }
</script>
@endsection





