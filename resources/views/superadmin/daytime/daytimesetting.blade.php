@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Day Time Management
          <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
           <li><a href="{{route('root.day-time.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
          <li class="active">Day Time Management</li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
        <div class="row">
        <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Day Time Management With Full Features</h3>
                    </div>
                    <!-- /.box-header -->
                    
                    @if(Session::has('message'))
                        <div class="alert alert-success" style="padding: 7px 15px;">
                            {{ Session::get('message') }}
                            {{ Session::put('message','') }}
                        </div>
                    @endif
                    
                    <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                        {!! Form::open(array('url' => 'root/day/time/search','id'=>'searchingForm','class' => 'form-inline','method'=>'post')) !!}
                            <div class="form-group">
                                {!! Form::select('restaurantid',$restInfo, $restaurantid, ['id'=>'restaurantid','class' => 'form-control']) !!}
                            </div>
                            {!! Form::submit('Search',["id"=>'search',"class"=>"btn  btn-primary"]) !!}
                        {!! Form::close() !!}
                    </div>
                    
                    <div class="col-sm-4 text-right" style="margin-bottom:10px;"> 
                        <div class="btn-group">
                            @if(($restaurantid) && (count($restdetail)==0))
                                <a class="btn btn-default" href="{{ url('root/day/time/add-time/'.$restaurantid) }}" >
                                    <span>Add</span>
                                </a>
                            @endif
                            
                            @if(($restaurantid) && (count($restdetail)>0))
                                <a class="btn btn-default" href="{{url('/root/day-time/'.$restaurantid.'/edit')}}" >
                                    <span>Edit</span>
                                </a>
                            @endif
                        </div>
                    </div>
                 <div class="clearfix"></div> 
                    
                @if($restaurantid)
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Restaurant Name</th>
                                    <th>Day</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($restdetail))
                                    @foreach($restdetail as $info)
                                        <tr>
                                            <td>{{ ucfirst($info->f_name)  }} {{ ucfirst($info->l_name)  }}</td>
                                            <td>{{ ucfirst($info->day)  }}</td>
                                            <td>{{ (($info->open_time) ? $info->open_time : 'Closed') }}</td>
                                            <td>{{ (($info->close_time) ? $info->close_time : 'Closed') }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" align="center">
                                            No Record Exist
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                @endif
            <!-- /.box-body -->
            </div>
            
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@section('script')
<script>
    $("#searchingForm").validate({
        rules:
        {
            restaurantid:
            {
                required : true,
            },
        },
        messages: {
            restaurantid: {
                required: "Please Select Restaurant",
            },
        }
    });
</script>
@endsection
