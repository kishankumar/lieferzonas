
<div class="modal fade" id="add-form2" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close modalClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="panel-title new_category_title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add Email Template .</h4>
            </div>
            
            {!! Form::open(['route'=>'root.etemplate.store','id'=>'add-template-form'])!!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
                <div class="modal-body" style="padding: 5px;">
                    
                    <div class="panel-body">
                        <div class="errors" id="errors" style="color: red"></div>
                        
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::label('Title') !!}<span class="red">*</span>
                                    {!! Form::text('title','',["id"=>'title',"class"=>"form-control"]) !!}
                                    <span id="NameErr" style="color:red"></span>
                                </div>
                            </div>
                            {!! Form::hidden('template_id','',['id'=>'template_id']) !!}
                            <div class="col-md-4" id="show_status" style="display: none">
                                <div class="form-group">
                                    {!! Form::label('Status') !!}
                                    {!! Form::select('status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ), 1, ['id' => 'status','class' => 'form-control']) !!}
                                    <span id="statusErr" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('Template Type') !!} <span id="codeErr" style="color:red"></span>
                            <div class="col-md-12 form-group">
                                <div class="col-sm-9">
                                    <label class="radiobox">
                                        <input type="radio" name="type" value='1' class="type" <?php echo 'checked';?> > 
                                        <span>Superadmin</span>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="radiobox">
                                        <input type="radio" name="type" value='2' class="type"> 
                                        <span>Admin</span>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="radiobox">
                                        <input type="radio" name="type" value='3' class="type"> 
                                        <span>Users</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                           {!! Form::label('Text') !!}<span class="red">*</span> <span id="descriptionErr" style="color:#b90101"></span>
                            {!! Form :: textarea('text','',['style'=>'height:100px;','id'=>'text','placeholder'=>'Enter text ','class'=>'form-control'])  !!}
                        </div>
                        
                        <div class="form-group margin-top">
                            {!! Form::button('Add',["id"=>'add_email_templates',"class"=>"btn add_category btn-primary",'onclick'=>'add_template()']) !!}
                            <hr>
							<p><span class="red">*</span> - Required Fields.</p>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
				
            </div>
        </div>
    </div>