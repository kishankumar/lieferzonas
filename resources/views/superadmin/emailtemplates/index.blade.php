@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Email Templates
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.etemplate.index')}}"><i class="fa fa-dashboard"></i> Email Template</a></li>
        <li class="active">Email Template List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Email Template With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/etemplate/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/etemplate/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
             <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default"  data-toggle="modal" onclick="add_types()" data-original-title><span>Add</span></a>
                    <!--<a class="btn btn-default DTTT_button_csv"  onclick="add_types(1)" data-toggle="modal" data-original-title><span>Edit</span></a>-->
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_template()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_contents()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
             </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>

            <div class="clearfix"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                <div id="delerrors" style="color: red"></div>

            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>
                            <a href="{{url('root/etemplate/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='title' ? 'title-desc' :'title';?>">
                            Title
                            <?php if($type=='title'){ echo $class; } elseif($type=='title-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
<!--                        <th>
                            <a href="{{url('root/etemplate/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='text' ? 'text-desc' :'text';?>">
                            Text
                            <?php //if($type=='text'){ echo $class; } elseif($type=='text-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>-->
                        <th>Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                @if(count($TemplatesInfo))
                    @foreach($TemplatesInfo as $info)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('templates', $info->id, null, ['class' => 'templates checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ ucfirst($info->title)  }}</td>
                    <!--<td>{{ $info->text  }}</td>-->
                    <!--<td>{{ (strlen($info->text) > 30) ? strip_tags(substr($info->text,0,30)).'...' : strip_tags($info->text)  }}</td>-->
                    <td>
                        <?php if($info->type == 1)
                            echo 'Superadmin';
                        else if($info->type == 2) 
                            echo 'Admin';
                        else
                            echo 'Users';
                        ?>
                    </td>
                    <td>
                        @if($info->status == 1) 
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-success" href="javascript:void(0)" onclick="add_types({{$info->id}})">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
                {!! $TemplatesInfo->appends(Request::except('page'))->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
	<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
    @endsection

@section('script')
    {!! Html::script('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}
    {!! Html::script('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') !!}
<script>
    $('#text').ckeditor();
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function add_template(){
        if( $("#add-template-form").valid())
        {
//            var descrip = $('#text').val().trim();
//            var len = descrip.length;
//            if(descrip == ''){
//                $('#descriptionErr').html('Text is required.');
//                return false;
//            }else if(len < 50){
//                $('#descriptionErr').html('Please enter at least 50 characters.');
//                return false;
//            }else{
//                $('#descriptionErr').html('');
//            }
            $.ajax({
                url: '{{ url('root/etemplate') }}',  //working
                type: "post",
                dataType:"json",
                data: $('#add-template-form').serialize(),
                success: function(res){
                    if(res.success==1)
                    {
                        document.getElementById("add-template-form").reset();
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#errors').html(errorsHtml);
                    }
                }
            });
        }
    }
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    function add_types(edit=0){
        $('#NameErr,#statusErr,#codeErr,#descriptionErr').html('');
        $('#errors,#delerrors').html('');
        $('.error').html('');
        if(edit){
            $('.new_category_title').html('<span class="glyphicon glyphicon-plus"></span> Edit Email Template');
            $('#add_email_templates').text('Update');
            $.ajax({
                url: '{{route("root.etemplate.edit")}}',
                type: "GET",
                dataType:"json",
                data: {'checkedTemplate':edit, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        $('#show_status').show();
                        $('#title').val(res.title);
                        $('#status').val(res.status);
                        $('.type').each(function() {     
                            var valu = $(this).val();
                            if(valu != res.type){ 
                                $(this).prop('checked', false);
                            }
                            else{ 
                                $(this).prop('checked', true);
                            }
                        });
                        //$("input[name=type][value="+res.type+"]").attr('checked', true);
                        $('#text').val(res.text);
                        $('input[name=template_id]').val(res.id);
                        showModal('add-form2');
                    }
                    else
                    {
                        document.getElementById("add-template-form").reset();
                    }
                }
            });
        }else{
            document.getElementById("add-template-form").reset();
            $('.new_category_title').html('<span class="glyphicon glyphicon-plus"></span>Add Email Template.');
            $('#show_status').hide();
            $('#add_email_templates').text('Add');
            $("input[name=type][value=1]").attr('checked', true);
            $('input[name=template_id]').val(0);
            showModal('add-form2');
        }
    }
    
    function delete_contents(){
        var len = $(".templates:checked").length;
        if(len==0){
            alert('Please select atleast one template');
            return false;
        }
        var base_url1=base_url+'etemplate/delete';
        var r = confirm("Are you sure to delete templates ?");
        if (r == true) {
            var selectedTemplate = new Array();
            $('input:checkbox[name="templates"]:checked').each(function(){
                selectedTemplate.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedTemplate':selectedTemplate, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#delerrors').html(errorsHtml);
                    }
                }
            });
        } else {
            return false;
        }
    }
    
    $(function(){
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-zA-ZäÄöÖüÜß ]+$/i.test(value);
        }, "Title must contain only letters.");
        
        $("#add-template-form").validate({
            ignore: [],
            debug: false,
            rules:
            {
                title:
                {
                    required : true,
                    minlength:3,
                    maxlength:100,
                    loginRegex:true
                },
                type:
                {
                    required : true,
                },
                text:
                {
                    required: function() 
                    {
                        CKEDITOR.instances.text.updateElement();
                    },
                    minlength:50
                }
            },
            messages: {
                "title": {
                    required: "Please enter title.",
                    loginRegex: "Title must contain only letters."
                },
                "text": {
                    required: "Please enter text.",
                    //minlength: "Text must be atleast 50 characters"
                }
            }
        });
        
//        $("#add_new_category").unbind("click").click(function(){
//            
//        });
        
        $(".modalClose").click(function(){
            $('#NameErr,#statusErr,#codeErr,#descriptionErr').html('');
            $('#errors,#delerrors').html('');
            $('.error').html('');
            document.getElementById("add-template-form").reset();
        });
    });
	
	/*added by Rajlakshmi(01-06-16)*/					

	function download_template(){
        var len = $(".templates:checked").length;
        if(len==0){
            alert('Please select atleast one template');
            return false;
        }
        var base_url1=base_url+'etemplate/download';
        var r = confirm("Are you sure to download templates ?");
        if (r == true) {
            var selectedTemplate = new Array();
            $('input:checkbox[name="templates"]:checked').each(function(){
                selectedTemplate.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedTemplate':selectedTemplate, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                       var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Email Template List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
						
					    doc.save("emailtemplate_list.pdf");
				}
            });
        } else {
            return false;
        }
		
	}
         /*added by Rajlakshmi(01-06-16)*/
</script>
@endsection

@include('superadmin.emailtemplates.add_template')
