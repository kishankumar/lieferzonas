@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Roles
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.roles.index')}}"><i class="fa fa-dashboard"></i> Roles Management</a></li>
            <li class="active">View Roles</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Roles</h3>
                    </div>
                    <!-- /.box-header -->
                    
                    <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                        {!! Form::open(array('url'=>"root/roles/",'class'=>'form-inline','method'=>'GET')) !!}
                            {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search using name"]) !!}
                            {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                            <a class="btn btn-primary" href="{{url('root/roles/')}}">Reset Search</a>
                        {!! Form::close() !!}
                    </div>

                    <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <a class="btn btn-default" data-toggle="modal" data-target="#add-form"
                                                  data-original-title><span>Add</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" onclick="download_roles()" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable"><span>Download</span></a>
                            <a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2"
                                    tabindex="0" aria-controls="simpledatatable" onclick="delete_roles()"><span>Delete</span></a></div>
                    </div>
                    
                    <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
                    
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                                    <div class="checkbox icheck">
                                        <label>
                                        {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                        <span class=""></span>
                                        </label>
                                    </div>
                                </th>
                                <th>
                                    <a href="{{url('root/roles/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                                    Role Name <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('root/roles/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">

                                    Description <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                                </th>
                                <th>
                                    <a href="{{url('root/roles/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='created' ? 'created-desc' :'created';?>">
                                    Created At <?php if($type=='created'){ echo $class; } elseif($type=='created-desc'){ echo $class1; } else { echo $class2; } ?>

                                </th>
                                <th>Created By</th>
                                <th>Status (Click to change)</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                                
                            @foreach($roles as $role)
                            <tr>
                                    <td>
                                        <div class="checkbox icheck">
                                            <label>{!! Form::checkbox('roles_check',$role->id,false,['id'=>'check','class'=>'roles_check checkBoxClass']) !!} <span> </span></label>
                                        </div>
                                    </td>
                                    <td>{{$role->role_name}}</td>
                                    <td>{{$role->description}}</td>
                                    <td>{{date('d M Y',strtotime($role->created_at))}}</td>
                                    <td>{{$role->users['email']}}</td>
                                    <td><?php if($role->status==1){ ?><i class="fa fa-check text-success fa-2xkc text-success" onclick="changeStatus(0,'<?php echo $role->id ?>')"></i><?php } else{ ?><i class="fa fa-ban text-danger fa-2xkc text-danger" onclick="changeStatus(1,'<?php echo $role->id; ?>')"></i><?php } ?></td>
      <td><a class="btn btn-primary DTTT_button_csv" href="{{ "roles/".$role->id."/edit"}}"><span>Edit</span></a></td>
                            </tr>
                            @endforeach

                            </tbody>

                        </table>
                        
                        {!! $roles->appends(Request::except('page'))->render() !!}
                        
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<div class="modal fade" id="add-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add New
                    Role.</h4>
            </div>

                <div class="modal-body" style="padding: 5px;">
                    <div class="panel-body">
                        @if($errors->any())
                            {!! Html::script('resources/assets/js/jQuery-2.2.0.min.js') !!}
                            <script>
                                    $(document).ready(function(){
                                                $('#add-form').modal();
                                                     });
                            </script>
                            <div class="alert alert-danger">
                                <ul>
                            @foreach($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                                </ul>
                            </div>
                        @endif
                        {{--<form role="form">--}}
                        {!! Form::open(['route'=>'root.roles.store','method'=>'post','id'=>'roleform']) !!}


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Role Name</label><span class="red">*</span>
                                        {{--<input type="text" id="name" class="form-control">--}}
                                        {!! Form::text('role_name','',['class'=>'form-control required']) !!}
                                    </div>
                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Description</label><span class="red">*</span>
                                        {!! Form::textarea('description','',['class'=>'form-control required']) !!}
                                    </div>

                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Status</label>
                                        {!! Form::select('status',['1'=>'Active','0'=>'Inactive'], '1', ['class'=>'form-control']) !!}
                                    </div>

                                </div>

                            </div>

                            <div class="form-group margin-top">
                               {{-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add</button>--}}
                                {!! Form::submit('Add Role',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                                <hr>
                                <p><span class="red">*</span> - Required Fields.</p>
                            </div>
                        {{--</form>--}}
                        {!! Form::close() !!}
                    </div>

                </div>

        </div>
    </div>
</div>
<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
@endsection

@section('script')

    {!! Html::script('resources/assets/js/jquery.validate.min.js') !!}
    {!! Html::script('resources/assets/js/additional-methods.js') !!}

    <script type="text/javascript">

        $(document).ready(function(){
            $.validator.addMethod("checkSpecialLettersGerman", function(value, element) {
                return this.optional(element) || /^[a-zA-ZäÄöÖüÜß«» -]+$/i.test(value);
            }, "Role must contain only letters.");
            
            $("#roleform").validate({
                rules: {
                    role_name: {
                        //lettersonly: true,
                        checkSpecialLettersGerman: true
                    },
                    description: {
                        minlength: 10
                    }
                }
            });
        });

        function delete_roles(){
            var len = $(".roles_check:checked").length;
            if(len==0){
                alert('Please select atleast one Role');
                return false;
            }

            var r = confirm("Are you sure to delete the selected Roles.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="roles_check"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: '{{url('root/roles/delete')}}',
                    type: "post",
                    dataType:"json",
                    data: {'selectedRoles':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                        if(res.success==1)
                            location.reload();
                        else
                            alert('Error occured.!! Please try after some time.');
                    }
                });
            } else {
                return false;
            }
        }

        function changeStatus(status, id)
        {
            $.ajax({
                type	: "POST",
                url		: '{{url('root/roles/changeStatus')}}',
                accepts	: 'application/json',
                dataType:"json",
                data	: {
                    id		    : id,
                    status		: status,
                    _token      :"{{ csrf_token() }}"
                },
                success: function (data) {
                    if(data.success==1)
                        location.reload();
                    else
                        alert('Error occured.!! Please try after some time.');
                }
            });
        }
		/*added by Rajlakshmi(31-05-16)*/
		function download_roles() 
	   {
			var doc = new jsPDF();
			var specialElementHandlers = {
			'#editor': function (element, renderer) {
				return true;
			}
		   };
		
            var len = $(".roles_check:checked").length;
            if(len==0){
                alert('Please select atleast one Role');
                return false;
            }

            var r = confirm("Are you sure to download the selected Roles.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="roles_check"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
			
            $.ajax({
                url: 'roles/download',
                type: "post",
                dataType:"json",
                data: {'selectedRoles':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("role.pdf");
				}
            });
        } else {
            return false;
        }
	   }
		
/*added by Rajlakshmi(31-05-16)*/
    </script>
@endsection