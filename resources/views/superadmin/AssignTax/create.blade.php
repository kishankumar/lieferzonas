@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Assign Tax
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('root.taxassign.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
                <li class="active">Add Assign Tax</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
            <div class="row">
        <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
        
                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Assign Tax With Full Features</h3>
                    </div>
                <!-- /.box-header -->
                
                    {{--    Error Display--}}
                        @if($errors->any())
                            <div class="alert alert-danger ">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    {{--    Error Display ends--}}
            
                    <div class="panel panel-primary theme-border">
                        <div class="panel-heading theme-bg">
                            <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Assign Tax</h4>
                        </div>
                        
                        <div class="modal-body" style="padding: 5px;">
                            <div class="panel-body">
                                
                                @if(Session::has('message'))
                                    <div class="alert alert-success" style="padding: 7px 15px;">
                                        {{ Session::get('message') }}
                                        {{ Session::put('message','') }}
                                    </div>
                                @endif
                                
                                {!! Form::open(array('route'=>'root.taxassign.create','id'=>'searchingForm','class'=>'form-horizontal','method'=>'GET')) !!}
                                    <div class="col-md-12 form-group">
                                        <label class="col-sm-2 control-label">Restaurant Name<span class="red">*</span></label>
                                        <div class="col-sm-4">
                                            @if($restro_detail_id)
                                                <select class="form-control" disabled name="restaurant">
                                            @else
                                                <select class="form-control js-example-basic-multiple" name="restaurant">
                                            @endif
                                            <option value="">Select</option>
                                            
                                            @foreach($restro_names as $restro_name)
                                                @if($restro_name->id==$restro_detail_id)
                                                    <option value="{{$restro_name->id}}" selected >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                                @else
                                                    <option value="{{$restro_name->id}}" >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                                @endif
                                            @endforeach
                                            </select>
                                            <span id="restaurantErr" style="color:red"></span>
                                        </div>
                                        
                                        @if(!$restro_detail_id)
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" type="submit">Get Tax</button>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="clearfix"></div>
                                {!! Form::close() !!}
                                
                    @if($restro_detail_id)
                        <div>
                            {!! Form::open(array('route'=>'root.taxassign.store','id'=>'taxassignForm','class'=>'form-horizontal')) !!}
                            <div class="col-md-12 form-group">
                                <label class="col-sm-2 control-label">Select Tax</label>
                                <div class="col-sm-4">
                                    <select name="taxes" id="taxes" class="form-control">
                                        <option value="">-Select Tax-</option>
                                        <?php
                                        foreach($tax_codes as $tax_code){
                                        ?>
                                            <option value="<?=$tax_code->id;?>" <?=@(($tax_code->id==$sel_taxes->tax_setting_id) ? 'selected' :'');?> ><?=ucfirst($tax_code->tax_name);?></option>
                                        <?php
                                        } ?>
                                    </select>
                                    <span id="taxesErr" style="color:red"></span>
                                </div>
                            </div>
                            
                            <div class="col-md-12 form-group">
                                <label class="col-sm-2 control-label">Tax Type</label>
                                <div class="col-sm-4">
                                    <select name="tax_type" id="tax_type" class="form-control">
                                        <option value="p" <?=@(('p'==$sel_taxes->tax_type) ? 'selected' :'');?> >Percentage</option>
                                        <option value="f" <?=@(('f'==$sel_taxes->tax_type) ? 'selected' :'');?> >Fixed</option>
                                    </select>
                                    <span id="TaxTypeErr" style="color:red"></span>
                                </div>
                            </div>
                            
                            <div class="col-md-12 form-group">
                                <label class="col-sm-2 control-label">Tax Amount</label>
                                <div class="col-sm-4">
                                    <input type="text" name="tax_amount" class="form-control" id="tax_amount" value="<?=@$sel_taxes->tax_amount;?>"> 
                                    <span id="taxamountErr" style="color:red"></span>
                                </div>
                            </div>
                            
                            {!! Form :: hidden('restro_id',$restro_detail_id,['id'=>'restro_id'])  !!}
                            <div class="col-md-12 form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-4">
                                    <button class="btn btn-primary" type="button" onclick="assign_tax(<?=$restro_detail_id;?>)">Submit</button>
                                    <a class="btn btn-primary" href="{{url('root/taxassign')}}">Cancel</a>
                                </div>
                            </div>
                        {!! Form::close() !!}
                        </div>
                    @endif
                                                                <!-- </form> -->
                </div>
            </div>  
        </div>
            <!-- /.box-body -->
    </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
<script>
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    function assign_tax(restId){
        $('#restaurantErr,#taxesErr,#TaxTypeErr,#taxamountErr').html('');
        var taxes = $('select[name="taxes"]').val().trim();
        var tax_type = $('select[name="tax_type"]').val().trim();
        var tax_amount = $('input[name="tax_amount"]').val().trim();
        if(taxes == '' || taxes == 0){
            $('#taxesErr').html("This field is required");
            return false;
        }
        var quanV=/^[0-9]+([.][0-9]{1,2})?$/;
        var validQuan=quanV.test(tax_amount);
        if(tax_type == 'p'){
            if(tax_amount == '' || tax_amount == 0 || tax_amount<0 || tax_amount>100 || isNaN(tax_amount) || validQuan==false){
                $('#taxamountErr').html("Invalid Amount");
                return false;
            }
        }else if(tax_type == 'f'){
            if(tax_amount == '' || tax_amount == 0 || tax_amount<0 || isNaN(tax_amount) || validQuan==false){
                $('#taxamountErr').html("Invalid Amount");
                return false;
            }
        }else{
            return false;
        }
        $('#taxassignForm').submit();
    }
    
    $("#searchingForm").validate({
        rules:
        {
            restaurant:
            {
                required : true,
            },
        },
        messages: {
            restaurant: {
                required: "Please Select Restaurant"
            },
        }
    });
</script>
@endsection
