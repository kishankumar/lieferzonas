@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1>
        Assign Zip Code
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.zipcodes.setting.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
        <li class="active">Assign Zip Code</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Assign Zip Code With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/zipcodes/setting/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/zipcodes/setting/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    <a class="btn btn-default" href="{{route('root.zipcodes.setting.create')}}">
                        <span>Assign Zip Code</span>
                    </a>
                 </div>
            </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            <div class="clearfix" style=""></div>
            
            <div class="box-body">
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>S. No.</th>
                        <th>
                            <a href="{{url('root/zipcodes/setting/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Restaurant Name
                            <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Views</th>
                    </tr>
                </thead>
                <tbody>
               
              
                <?php $i=$pages['from']; ?>
                    
                @if(count($restro_names))
                    @foreach($restro_names as $info)
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>{{ ucfirst($info->f_name).' '.ucfirst($info->l_name) }} </td>
                         	<?php $restName = ucfirst($info->f_name).' '.ucfirst($info->l_name); ?>
                            <td>
                                <a class="" href="javascript:void(0)" onclick='show_modal("cat-metrics-view",<?=$info->id?>,"cat-metrics-list","category-name","<?=($restName)?>")'>View Assigned Zip Codes
                                </a>
                            </td>
                           {!! Form :: hidden('rest_name[]',$restName ,['id'=>'rest_name'])  !!}
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="4" align="center">
                            No Record Exist
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
            {!! $restro_names->appends(Request::except('page'))->render() !!}
        </div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

    <div class="modal fade" id="cat-metrics-view" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-primary theme-border">
                <div class="panel-heading theme-bg">
                    <span id="category-name"></span>
                    <button type="button" id="modalclose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="panel-body PopupPublished"  id="cat-metrics-list">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script>
    
    function show_modal(modelId,rest_id,appendId,categoryNameId,restName) {
	$.ajax({
            url: '{{ url('root/zipcodes/getdata') }}',
            type: "get",
            data: {'rest_id':rest_id, '_token': '{{ csrf_field() }}'},
	    success: function (data) {
                if(data=='' || data==null){
                    data='No Zip Code assigned';
                }
                else{
                    data=data;
                }
                document.getElementById(categoryNameId).innerHTML = restName;
                document.getElementById(appendId).innerHTML = data;
            }
	});
	$("#"+modelId).modal('show');
    }
    
    function setForm(pageName) {
        $("#set-new-form").attr("action", pageName);
        $("#set-new-form").submit();
    }
    
//    $(function(){
//       $("#modalclose").click(function(){
//            $('#category-name,#cat-metrics-list').html('');
//        }); 
//    });

</script>

@endsection


