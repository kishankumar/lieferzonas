
<div class="modal fade" id="add-form1" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close modalClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="panel-title new_category_title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add Information .</h4>
            </div>
            
            {!! Form::open(['route'=>'root.navigation.info.store','id'=>'add-info-form'])!!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
                <div class="modal-body" style="padding: 5px;">
                    
                    <div class="panel-body">
                        <div class="errors" id="errors" style="color: red"></div>
                        
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::label('Title') !!}<span class="red">*</span> 
                                    {!! Form::text('title','',["id"=>'title',"class"=>"form-control"]) !!}
                                    <span id="titleErr" style="color:red"></span>
                                </div>
                            </div>
                            {!! Form::hidden('title_id','',['id'=>'title_id']) !!}
                            <div class="col-md-4" id="show_status" style="display: none">
                                <div class="form-group">
                                    {!! Form::label('Status') !!}
                                    {!! Form::select('status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ), 1, ['id' => 'status','class' => 'form-control']) !!}
                                    <span id="statusErr" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('Link') !!}<span class="red">*</span> 
                            {!! Form::text('link','',["id"=>'link',"class"=>"form-control"]) !!}
                            <span id="linkErr" style="color:red"></span>
                        </div>
                        
                        <div class="form-group margin-top">
                            {!! Form::button('Add',["id"=>'add_new_info',"class"=>"btn add_category btn-primary"]) !!}
                            <hr>
							<p><span class="red">*</span> - Required Fields.</p>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>