@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Send Mail to Subscribed Users
        <small>Control panel</small>
      </h1>
       <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{route('root.fusersubscription.index')}}"><i class="fa fa-dashboard"></i>Subscribe User</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Send Mail to Subscribed Users</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
            </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
					<div class="alert alert-success" style="padding: 7px 15px;">
					  {{ Session::get('message') }}
					  {{ Session::put('message','') }}
					</div>
				@endif
                
               {!! Form::open(array('route' => 'root.fusersubscription.store','id'=>'mail_form','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}
                
        <div class="modal-body" style="padding: 5px;">
            <div class="panel-body">
                      
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Subject') !!}<span class="red">*</span>
                            {!! Form::text('subject','',["id"=>'subject',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                                
                    
                </div>
                 <div class="row">
                    
                                
                     <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Maessage') !!}<span class="red">*</span>
                            {!! Form::textarea('message','', ["id"=>'message','class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group margin-top">
                    {!! Form::submit('Sendmail',["id"=>'submit',"class"=>"btn  btn-primary"]) !!}
                    <hr>
                   <p><span class="red">*</span> - Required Fields.</p>
                </div>
            </div>
			 
        </div>
            {!! Form::close() !!}
                
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

 @endsection
@section('script')

  <script>
   function unsubscribe(id)
   {
	 var subcribed_id = id;
        $.ajax({
                url: '{{ url('unsubscribe') }}',
                type: "GET",
                dataType:"json",
                data: {'subcribed_id':subcribed_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
                        
                        location.reload();
                    }
                    else
                    {
                        
                    }
                }
            });
    } 
     

 
     $(document).ready(function(){
	
     $("#mail_form").validate({
      rules: {
		  subject: {
            required: true
               },
          message: {
            required: true
               },
         
        },
       messages: {

                    subject: "Please enter subject!",
                    message: "Please enter message!",
                    
                    agree: "Please accept our policy"
                },
         
     });
    });	 
  </script>
@endsection




