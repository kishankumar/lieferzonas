@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Subscribed Front Users Table
        <small>Control panel</small>
      </h1>
       <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{route('root.fusersubscription.index')}}"><i class="fa fa-dashboard"></i> Subscribed Front Users</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Subscribed Front Users Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
            </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <a  class="btn btn-default DTTT_button_print"  href="{{route('root.fusersubscription.create')}}"><span>Send mail</span></a>
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>S.No</th> 
                        <th>Name</th> 
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                @if(count($subscribeduser))
					<?php $i=1; ?>
                    @foreach($subscribeduser as $subscribedusers)
                <tr>
                    <td>{{ $i }}</td>
                    <td>
					<?php if($subscribedusers->front_user_id != 0) { ?>
					<a href="{{url('root/frontuser/detail/'.$subscribedusers->front_user_id)}}">
                        {{ $subscribedusers->name }}
                    </a>
					<?php } else { ?>
					{{ $subscribedusers->name }}
					<?php } ?>
					</td>
					
					<td>{{ $subscribedusers->email }}</td>
					
					
					<td>
					    <a class="btn btn-success" href="#" onclick="unsubscribe(<?=$subscribedusers->id?>);">
                        Unsubscribed
                        </a>
					</td>
					
                </tr>
				<?php $i++; ?>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
                
               {!! $subscribeduser->render() !!}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

 @endsection
@section('script')

  <script>
   function unsubscribe(id)
   {
	 var subcribed_id = id;
        $.ajax({
                url: '{{ url('unsubscribe') }}',
                type: "GET",
                dataType:"json",
                data: {'subcribed_id':subcribed_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
                        
                        location.reload();
                    }
                    else
                    {
                        
                    }
                }
            });
    } 
         
  </script>
@endsection




