@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Front Users Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{route('root.frontuser.index')}}"><i class="fa fa-dashboard"></i>Front User Detail</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Front Users Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
            </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <a class="btn btn-default DTTT_button_print" href="{{url('root/frontuser/showaddress/'.$fusers->id)}}">
                         <span>Show Address</span>
                     </a>
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
				
			<div class="listuser">	
				<div class="form-group">
					<label class="col-md-3">First Name:</label>
					<div class="col-md-9">{{ ucfirst($fusers->fname)}}</div>
				</div>   

				<div class="form-group">
					<label class="col-md-3">Last Name:</label>
					<div class="col-md-9">{{ ucfirst($fusers->lname) }}</div>
				</div> 
				<div class="form-group">
					<label class="col-md-3">Nick Name:</label>
					<div class="col-md-9">{{ $fusers->nickname }}</div>
				</div> 
				<div class="form-group">
					<label class="col-md-3">User Name:</label>
					<div class="col-md-9">{{ $fusers->username }}</div>
				</div> 
				<div class="form-group">
					<label class="col-md-3">Email:</label>
					<div class="col-md-9">{{ $fusers->email }}</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">Mobile:</label>
					<div class="col-md-9">{{ $fusers->mobile }}</div>
				</div>

				<div class="form-group">
					<label class="col-md-3">Gender:</label>
					<div class="col-md-9">@if($fusers->gender=='m') Male @elseif($fusers->gender=='f') Female @else Others @endif</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">Profile Pic:</label>
					<div class="col-md-9">@if ($fusers->profile_pic)
					<img class="img-upload" src="{{asset('public/front/uploads/users/'.$fusers->profile_pic) }}" style="max-width: 25%">
				@endif</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">Date Of Birth:</label>
					<div class="col-md-9">{{ date("d M Y", strtotime($fusers->dob)) }}</div>
				</div>
				<div class="form-group">
					<label class="col-md-3">Status:</label>
					<div class="col-md-9">@if($fusers->status ==1) Active @else Deactive @endif</div>
				</div>
				
		    </div>
			
		    </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('script')
@endsection




