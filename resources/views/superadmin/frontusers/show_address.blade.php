@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Front Users Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{route('root.frontuser.index')}}"><i class="fa fa-dashboard"></i>Show Address</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Front Users Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
            </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <a href="#" class="btn btn-default DTTT_button_print" data-target="#Add_address" data-toggle="modal">
					     <span>Add Address</span>
					 </a> 
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
             <div class="col-sm-8">
                <h3 class="green">Details</h3>
				<div class="row">
				@if(count($addressDetails))
				  @foreach($addressDetails as $addressDetail)
				  <div class="col-sm-6">
					  <div class="well add_box">
						<div class="row">
						  <?php //$udetail = \App\front\FrontUserDetail::userdetail(); ?>
						  <div class="col-md-9">{{$addressDetail['booking_person_name']}}</div>
						 <div class="col-md-3"><aside><span><a href="#" onclick="fetchAddressModal('{{$addressDetail['id']}}')" ><i class="fa fa-pencil"></i>
						 </a></span><span><a href="#" onclick="deleteAddress('{{$addressDetail['id']}}')"><i class="fa fa-trash"></i></a></span></aside></div>
						</div>
						
						<div class="form-group">
						  {{$addressDetail['address'].' '.$addressDetail['landmark'].' '.$addressDetail['city_name']
						  .' '.$addressDetail['state_name'].' '.$addressDetail['country_name'].' '.$addressDetail['zipcode']}}

						</div>
						<div class="form-group">
						  Phone: {{$addressDetail['mobile']}}
						</div>
						
					  </div>
				  </div>
				  @endforeach
                  @endif
				</div>
                <div class="form-group row">
		        <div class="col-md-3"> </div>  
		        </div>
		     </div>
	        </div>
        </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
	
	<div class="modal fade" id="Add_address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-md robotoregular">
		<div class="modal-content">
		  <div class="modal-header" style="background:#81c02f; color:#fff;">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title"> Add the shipping address </h4>
		  </div>
		   <div class="col-xs-12" id="s_msg" class="alert text-center" ></div>
		  @if($errors->any())
                           
			<div class="alert alert-danger">
				<ul>
			@foreach($errors->all() as $error)
				<li> {{$error}} </li>
			@endforeach
				</ul>
			</div>
          @endif
		  
		  <form id="add-address-form">
		  <div class="modal-body">
			<div class="form-group row">
			  <div class="col-md-3">Name<span class="red">*</span> </div>
			  <div class="col-md-9"><input type="text" class="form-control" id="sname"  name="name" onkeypress="hideErrorMsg('snameerr')">
			  <span id="snameerr" class="text-danger"></span></div>
			  
			</div>
			<div class="form-group row">
			  <div class="col-md-3">Country </div>
			  <div class="col-md-9">
			  <select  name="country" id="scountry" class="form-control" onchange="getData(this.value,'countries','states','sstate','id','name','id','country_id')">
                                <option value="">Select Country</option>
                                <?php
                                foreach($country1 as $country1) { ?>
                                    <option value="<?=$country1['id'] ?>"><?=$country1['name'] ?></option>
                                <?php
                                } ?>
              </select>
			  <span id="scountryerr" class="text-danger"></span>
			  </div>
			</div>
			<div class="form-group row">
			  <div class="col-md-3">State </div>
			  <div class="col-md-9">
			  <select name="state" id="sstate" class="form-control" onchange="getData(this.value,'states','cities','scity','id','name','id','state_id')">
                              
              </select>
			  <span id="sstateerr" class="text-danger"></span>
			  </div>
			</div>
			<div class="form-group row">
			 <div class="col-md-3">City </div>
			  <div class="col-md-9">
			  <select name="city" id="scity" class="form-control" onchange="getData(this.value,'cities','zipcodes','zipcode','id','name','id','city_id')" >
                               
                                
              </select>
			  <span id="scityerr" class="text-danger"></span>
			  </div>
			</div>
			<div class="form-group row">
			  <div class="col-md-3">Pinode<span class="red">*</span> </div>
			  <div class="col-md-9"><input type="text" class="form-control"  name="pincode" id="spincode" onkeypress="hideErrorMsg('spincodeerr')">
			  <span id="spincodeerr" class="text-danger"></span></div>
			</div>
			<div class="form-group row">
			  <div class="col-md-3">Address<span class="red">*</span> </div>
			  <div class="col-md-9"><textarea class="form-control"  name="address" id="saddress" onkeypress="hideErrorMsg('saddresserr')"></textarea>
			  <span id="saddresserr" class="text-danger"></span></div>
			</div>
			<div class="form-group row">
			  <div class="col-md-3">Landmark<span class="red">*</span> </div>
			  <div class="col-md-9"><input type="text" class="form-control" name="landmark" id="slandmark" onkeypress="hideErrorMsg('slandmarkerr')" >
			  <span id="slandmarkerr" class="text-danger"></span></div>
			</div>
			
			<div class="form-group row">
			  <div class="col-md-3">Phone<span class="red">*</span> </div>
			  <div class="col-md-9"><input type="text" class="form-control" id="smobile" name="mobile" onkeypress="hideErrorMsg('smobileerr')">
			  <span id="smobileerr" class="text-danger"></span></div>
			</div>
			<input type="hidden" id="sfront_user_id" value="<?=$fusers['id']?>">
			
			<div class="form-group row">
			  <div class="col-md-3"></div>
			  <div class="col-md-9"> 
			  <button type="button" class="btn btn-success" id="add-form" onclick="add_address();" >Save</button> 
			  <hr>
                    <p><span class="red">*</span> - Required Fields.</p>
			  </div>
			</div>
		  </div>
			</form>
		</div>
	  </div>
	</div>
	
	<div class="modal fade" id="Edit_address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-md robotoregular">
		<div class="modal-content">
		  <div class="modal-header" style="background:#81c02f; color:#fff;">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title"> Edit the shipping address </h4>
		  </div>
		   <div class="col-xs-12" id="s_msg" class="alert text-center" ></div>
		  @if($errors->any())
                           
			<div class="alert alert-danger">
				<ul>
			@foreach($errors->all() as $error)
				<li> {{$error}} </li>
			@endforeach
				</ul>
			</div>
          @endif
		  
		  <form id="edit-address-form">
		  <div class="modal-body">
			<div class="form-group row">
			  <div class="col-md-3">Name<span class="red">*</span> </div>
			  <div class="col-md-9"><input type="text" class="form-control" id="name" name="name" onkeypress="hideErrorMsg('nameerr')">
			  <span id="nameerr" class="text-danger"></span></div>
			  
			</div>
			<div class="form-group row">
			  <div class="col-md-3">Country </div>
			  <div class="col-md-9">
			  <select id="country" name="country" class="form-control" onchange="getData(this.value,'countries','states','state','id','name','id','country_id')">
                                <option value="">Select Country</option>
                                <?php
                                foreach($country as $country) { ?>
                                    <option value="<?=$country['id'] ?>"><?=$country['name'] ?></option>
                                <?php
                                } ?>
              </select>
			  <span id="countryerr" class="text-danger"></span>
			  </div>
			</div>
			<div class="form-group row">
			  <div class="col-md-3">State </div>
			  <div class="col-md-9">
			  <select id="state" name="state" id="state" class="form-control" onchange="getData(this.value,'states','cities','city','id','name','id','state_id')">
                <option value="">Select State</option>
                                 @foreach($state as $state)
                                        <option  value="<?=$state['id'] ?>"><?=$state['name'] ?>
                                </option>
                                @endforeach               
              </select>
			  <span id="stateerr" class="text-danger"></span>
			  </div>
			</div>
			<div class="form-group row">
			 <div class="col-md-3">City </div>
			  <div class="col-md-9">
			  <select id="city" name="city" class="form-control" onchange="getData(this.value,'cities','zipcodes','zipcode','id','name','id','city_id')">
                               <option value="">Select City</option>
                                 @foreach($city as $city)
                                        <option  value="<?=$city['id'] ?>"><?=$city['name'] ?>
                                </option>
                                @endforeach 
                                
             </select>
			  <span id="cityerr" class="text-danger"></span>
			  </div>
			</div>
			<div class="form-group row">
			  <div class="col-md-3">Pinode<span class="red">*</span> </div>
			  <div class="col-md-9"><input type="text" class="form-control" id="pincode" name="pincode" onkeypress="hideErrorMsg('pincodeerr')">
			  <span id="pincodeerr" class="text-danger"></span></div>
			</div>
			<div class="form-group row">
			  <div class="col-md-3">Address<span class="red">*</span> </div>
			  <div class="col-md-9"><textarea class="form-control" id="address" name="address" onkeypress="hideErrorMsg('addresserr')"></textarea>
			  <span id="addresserr" class="text-danger"></span></div>
			</div>
			<div class="form-group row">
			  <div class="col-md-3">Landmark <span class="red">*</span> </div>
			  <div class="col-md-9"><input type="text" class="form-control" id="landmark" onkeypress="hideErrorMsg('landmarkerr')" >
			  <span id="landmarkerr" class="text-danger"></span></div>
			</div>
			
			<div class="form-group row">
			  <div class="col-md-3">Phone <span class="red">*</span> </div>
			  <div class="col-md-9"><input type="text" class="form-control" id="mobile" name="mobile" onkeypress="hideErrorMsg('mobileerr')">
			  <span id="mobileerr" class="text-danger"></span></div>
			</div>
			<input type="hidden" id="front_user_id" value="<?=$fusers['id']?>">
			<input type="hidden" name="address_id" id="address_id" value="">
			<div class="form-group row">
			  <div class="col-md-3"></div>
			  <div class="col-md-9"> <button type="button" class="btn btn-success" id="edit-form" onclick="edit_address();" >Save</button> 
			  <hr>
                    <p><span class="red">*</span> - Required Fields.</p></div>
			</div>
		  </div>
			</form>
		</div>
	  </div>
	</div>
	
	
@endsection
@section('script')
<script>

 /*function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn) {

         $.ajax({

                url: '{{url('getdata')}}',
                type: "get",
                data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn,
				'selectedOption':},
                
             success: function (data) {
				 
             
           $('#'+appendId).html(data);
           
          },
             error: function (jqXHR, textStatus, errorThrown) {
               
             }
         });
   
      }*/
 function add_address()   
 {
   var name = $("#sname").val();
   //alert(name);
   var city = $("#scity").val();
   var country = $("#scountry").val();
   var state = $("#sstate").val();
   var front_user_id = $("#sfront_user_id").val();
   if(name=="")
    {
		$("#sname").focus();
		$("#snameerr").html('Please enter booking person name');
		$("#snameerr").show();
		return false;
    } 
  
   /*if(country=="")
    {
		$("#country").focus();
		$("#countryerr").html('Please enter country');
		$("#countryerr").show();
		return false;
    } 
  
   if(state=="")
    {
		$("#state").focus();
		$("#stateerr").html('Please enter state');
		$("#stateerr").show();
		return false;
    }
  
   if(city=="")
    {
		$("#city").focus();
		$("#cityerr").html('Please enter city');
		$("#cityerr").show();
		return false;
    } */
    var pincode = $("#spincode").val();
    if(pincode=="")
    {
		$("#spincode").focus();
		$("#spincodeerr").html('Please enter pincode');
		$("#spincodeerr").show();
		return false;
    } 
    var address = $("#saddress").val();
    if(address=="")
    {
		$("#saddress").focus();
		$("#saddresserr").html('Please enter address');
		$("#saddresserr").show();
		return false;
    } 
    var landmark = $("#slandmark").val();
    if(landmark=="")
    {
		$("#slandmark").focus();
		$("#slandmarkerr").html('Please enter landmark');
		$("#slandmarkerr").show();
		return false;
    } 
    var mobile = $("#smobile").val();
    if(mobile=="")
    {
		$("#smobile").focus();
		$("#smobileerr").html('Please enter phone');
		$("#smobileerr").show();
		return false;
    } 

    if(isNaN(mobile))
	{
	  $("#smobile").focus();
	  $("#smobileerr").html('Please enter digit for mobile');
	  $("#smobileerr").show();
	  return false;
    }
	if(mobile.length>11){
		  $("#smobile").focus();
		  $("#smobileerr").html('Please enter less than or equal to 11 digits for mobile');
		  $("#smobileerr").show();
		  return false;
	}
	if(mobile.length<7){
		  $("#smobile").focus();
		  $("#smobileerr").html('Please enter more than or equal to 7 digits for mobile');
		  $("#smobileerr").show();
		  return false;
	}

            $.ajax({
                url: '{{url('root/frontuser/add_address')}}',
                type: "post",
                dataType:"json",
                data: {'front_user_id':front_user_id,'name':name,'country':country,'state':state,'city':city,'address':address,'landmark':landmark 
				,'pincode':pincode,'mobile':mobile,"_token":"{{ csrf_token() }}"},
                success: function(res){
                   if(res.success==1)
                    {
                        
						location.reload(); 
						
                    }
                    else
                    {
                       
						$('#s_msg').html('There is some network problem,please try again');
                        $('#s_msg').show();
                    }
                }
            });	
                 
 }
 function edit_address()   
 {
   var name = $("#name").val();
   //alert(name);
   var city = $("#city").val();
   var country = $("#country").val();
   var state = $("#state").val();
   var front_user_id = $("#front_user_id").val();
    var address_id = $("#address_id").val();
   if(name=="")
    {
		$("#name").focus();
		$("#nameerr").html('Please enter booking person name');
		$("#nameerr").show();
		return false;
    } 
  
   /*if(country=="")
    {
		$("#country").focus();
		$("#countryerr").html('Please enter country');
		$("#countryerr").show();
		return false;
    } 
  
   if(state=="")
    {
		$("#state").focus();
		$("#stateerr").html('Please enter state');
		$("#stateerr").show();
		return false;
    }
  
   if(city=="")
    {
		$("#city").focus();
		$("#cityerr").html('Please enter city');
		$("#cityerr").show();
		return false;
    } */
    var pincode = $("#pincode").val();
    if(pincode=="")
    {
		$("#pincode").focus();
		$("#pincodeerr").html('Please enter pincode');
		$("#pincodeerr").show();
		return false;
    } 
    var address = $("#address").val();
    if(address=="")
    {
		$("#address").focus();
		$("#addresserr").html('Please enter address');
		$("#addresserr").show();
		return false;
    } 
    var landmark = $("#landmark").val();
    if(landmark=="")
    {
		$("#landmark").focus();
		$("#landmarkerr").html('Please enter landmark');
		$("#landmarkerr").show();
		return false;
    } 
    var mobile = $("#mobile").val();
    if(mobile=="")
    {
		$("#mobile").focus();
		$("#mobileerr").html('Please enter phone');
		$("#mobileerr").show();
		return false;
    } 

    if(isNaN(mobile))
	{
	  $("#mobile").focus();
	  $("#mobileerr").html('Please enter digit for mobile');
	  $("#mobileerr").show();
	  return false;
    }
	if(mobile.length>11){
		  $("#mobile").focus();
		  $("#mobileerr").html('Please enter less than or equal to 11 digits for mobile');
		  $("#mobileerr").show();
		  return false;
	}
	if(mobile.length<7){
		  $("#mobile").focus();
		  $("#mobileerr").html('Please enter more than or equal to 7 digits for mobile');
		  $("#mobileerr").show();
		  return false;
	}
	

            $.ajax({
                url: '{{url('root/frontuser/edit_address')}}',
                type: "post",
                dataType:"json",
                data: {'address_id':address_id,'front_user_id':front_user_id,'name':name,'country':country,'state':state,'city':city,'address':address,'landmark':landmark 
				,'pincode':pincode,'mobile':mobile,"_token":"{{ csrf_token() }}"},
                success: function(res){
                   if(res.success==1)
                    {
                        
						location.reload(); 
						
                    }
                    else
                    {
                       
						$('#s_msg').html('There is some network problem,please try again');
                        $('#s_msg').show();
                    }
                }
            });	
                 
 }
 function fetchAddressModal(id)
 {
	 
	 var address_id = id;
        $.ajax({
                url: '{{ url('fetchaddress') }}',
                type: "GET",
                dataType:"json",
                data: {'address_id':address_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
						showModal('Edit_address');
					    $('#name').val(res.name);
                        $('#country').val(res.country);
						$('#state').val(res.state);
                        $('#city').val(res.city);
						$('#pincode').val(res.pincode);
						$('#address').val(res.address);
                        $('#landmark').val(res.landmark);
						$('#mobile').val(res.mobile);
                        $('input[name=address_id]').val(res.id);
                       
						
                    }
                    else
                    {
                        //document.getElementById("edit-form").reset();
                    }
                }
            });
 } 
 function deleteAddress(id)
 {
	 var address_id = id;
        $.ajax({
                url: '{{ url('deleteaddress') }}',
                type: "GET",
                dataType:"json",
                data: {'address_id':address_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
                        
                        location.reload();
                    }
                    else
                    {
                        
                    }
                }
            });
 } 
 
 function hideErrorMsg(id){
    
      $('#'+id).attr('style', 'display:none');
  
    
     }
	 

</script>

@endsection




