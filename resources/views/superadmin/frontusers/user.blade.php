@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Front Users Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{route('root.frontuser.index')}}"><i class="fa fa-dashboard"></i>Front User</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Front Users Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
            </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>-->
                     <a class="btn btn-danger DTTT_button_xls" onclick="delete_users()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                         <span>Delete</span>
                     </a>
                </div>
            </div>
            
            <div class="clearfix" style="margin: 20px 0;"></div>
                    <div class="box-body">
					@if(Session::has('message'))
						<div class="alert alert-success" style="padding: 7px 15px;">
						  {{ Session::get('message') }}
						  {{ Session::put('message','') }}
						</div>
					@endif
                
                <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div>
                                <label  class="checkbox icheck">
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>First Name</th> 
                        <th>Last Name</th>
                        <th>User Name</th> 						
                        <th>Email Id</th>
                        <th>Mobile</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    
                @if(count($fusersinfo))
                    @foreach($fusersinfo as $info)
                <tr>
                    <td>
                        <div class="checkbox1 icheck">
                            <label>
                                {!! Form::checkbox('fuser_id', $info->id, null, ['class' => 'users checkBoxClass']) !!}
                                <span class="button-checkbox"></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ $info->fname }}</td>
					<td>{{ $info->lname }}</td>
					<td>{{ $info->username }}</td>
					<td>{{ $info->email }}</td>
					<td>{{ $info->mobile }}</td>
					<td>@if($info->status==1)<i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($info->status==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>
					<td>
					    <a class="btn btn-success" href="{{url('root/frontuser/'.$info->id.'/edit')}}">
                        <i class="fa fa-edit"></i>
                        </a>
					</td>
					<td>
					    <a class="btn btn-success" href="{{url('root/frontuser/detail/'.$info->id)}}">
                        View Detail
                        </a>
					</td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
                {!! $fusersinfo->render(); !!}
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

 @endsection
@section('script')

  <script>
   
     
    function delete_users(){
        var len = $(".users:checked").length;
        if(len==0){
            alert('Please select atleast one user');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to delete users?");
        if (r == true) {
            var selectedUsers = new Array();
            $('input:checkbox[name="fuser_id"]:checked').each(function(){
                selectedUsers.push($(this).val());
            });
            $.ajax({
                url: 'frontuser/delete',
                type: "post",
                dataType:"json",
                data: {'selectedUsers':selectedUsers, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
						
                    }
                    else
                    {
                        alert('Records not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
</script>
@endsection




