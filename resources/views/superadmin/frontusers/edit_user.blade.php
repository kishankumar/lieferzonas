@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')


<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Front Users Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{route('root.frontuser.index')}}"><i class="fa fa-dashboard"></i> Edit Front User</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Front Users Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('errmsg'))
					<div class="alert alert-danger" style="padding: 7px 15px;">
					  {{ Session::get('errmsg') }}
					  {{ Session::put('errmsg','') }}
					</div>
				@endif
				@if($errors->any())
					<div class="alert alert-danger ">
						<ul class="list-unstyled">
							@foreach($errors->all() as $error)
								<li> {{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
                
                
    {!! Form::model($fusers,['route'=>['root.frontuser.update',$fusers->id],'method'=>'patch','id' => 'user_form','class' => 'form','novalidate' => 'novalidate', 'files' => true])	!!}
                
        <div class="modal-body" style="padding: 5px;">
            <div class="panel-body">
                      
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Firstname') !!}<span class="red">*</span>
                            {!! Form::text('fname',$fusers->fname,["id"=>'firstname',"class"=>"form-control"]) !!}
							{!! Form::hidden('fuid',$fusers->front_user_id,["id"=>'fuid',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                                
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Lastname') !!}
                            {!! Form::text('lname',$fusers->lname,["id"=>'lastname',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Nick Name') !!}
                            {!! Form::text('nickname',$fusers->nickname,["id"=>'nickname',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('User Name') !!}
                            {!! Form::text('username',$fusers->username,["id"=>'username',"class"=>"form-control",'readonly']) !!}
                        </div>
                    </div>            
                    
                </div>  
                 <div class="row">
                    
					<div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Email') !!}<span class="red">*</span>
                            {!! Form::email('email',$fusers->email,["id"=>'email',"class"=>"form-control"]) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                           {!! Form::label('Mobile') !!}<span class="red">*</span>
                           {!! Form::text('mobile',$fusers->mobile,["id"=>'mobile',"class"=>"form-control"]) !!}
                        </div>
                    </div>
					
                </div>  
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Profile pic') !!}
                             <input type="file" name="pics" id="pics"  onchange="validateimage(this)" >
                            @if ($fusers->profile_pic)
                           <img class="img-upload" src="{{asset('public/front/uploads/users/'.$fusers->profile_pic) }}" style="max-width: 25%">
                            @endif
                           
                            
                        </div>
                    </div>
					<div class="col-md-6">
						<div class="form-group">
								{!! Form::label('Gender') !!}<span class="red">*</span>
								<select id="gender" name="gender" class="form-control">
									<option value="">Select Gender</option>
									<option value="m" <?php if($fusers->gender == 'm') { echo "Selected='selected'"; } ?> >Male</option>
									<option value="f" <?php if($fusers->gender == 'f') { echo "Selected='selected'"; } ?> >Female</option>
									<option value="o" <?php if($fusers->gender == 'o') { echo "Selected='selected'"; } ?> >Others</option>
								</select>
						</div>
                    </div>            
                    
                </div> 
      
             
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('DOB') !!}<span class="red">*</span>
                        {!! Form::input('text', 'dob', $fusers->dob, ["id"=>"datetimepicker5","class" => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                                    {!! Form::label('Status') !!}
                                {!! Form::select('status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ), $fusers->userstatus, ['id' => 'status','class' => 'form-control']) !!}
                                    <span id="statusErr" style="color:red"></span>
                 </div>
                </div>
              </div>
                <div class="form-group margin-top">
                    {!! Form::submit('Update',["id"=>'submit',"class"=>"btn  btn-primary"]) !!}
                    <hr>
                    <p><span class="red">*</span> - Required Fields.</p>
                </div>
            </div>
        </div>
            {!! Form::close() !!}
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>
     @endsection


    @section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
         
	$.validator.addMethod("email", function(value, element)
	{
		return this.optional(element) || /^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
	}, "Please enter valid email address.");
        
     $("#user_form").validate({
      rules: {
	    fname: {
            required: true
               },
        
        email: {
            required: true,
            email:true
               },
        mobile: {
            required: true,
            digits:true,
            minlength:10,
            maxlength:12
               },
        gender: {
            required: true
               },
       
         dob: {
            required: true
               }
        
        },
       messages: {

                    fname: "Please enter firstname",
                    email:{
                        required: "Please enter email",
                        email: "Please enter valid email"
                    },
                    mobile:{
                        required: "Please enter phone number",
                   
                        minlength: "Please enter valid phone number",
                        maxlength: "Please enter valid phone number"
                    },
                    gender: "Please enter gender",
                    dob:"Please enter date of birth",
                    agree: "Please accept our policy"
                },
         
     });
});

	  function validateimage(fieldObj)
		{
			
			var FileName  = fieldObj.value;
			var FileExt = FileName.substr(FileName.lastIndexOf('.')+1);
			var FileSize = fieldObj.files[0].size;
			var FileSizeMB = (FileSize/5485760).toFixed(2);

		   if ( (FileExt != "jpg" && FileExt != "jpge" && FileExt != "png" && FileExt != "tif") )
			{
				var error = "File type : "+ FileExt+"\n\n";
				error += "Please make sure profile pic is in jpg,png,tif format.\n\n";

				alert(error);
				return false;
			}
			return true;
		}
    </script>

    @endsection
   
   

