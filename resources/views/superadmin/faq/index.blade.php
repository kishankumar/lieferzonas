@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            FAQs
            <small>Control panel</small>
        </h1>
         <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.faq.index')}}"><i class="fa fa-dashboard"></i> FAQ Management</a></li>
            <li class="active">View FAQs</li>
         </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin FAQs</h3>
                    </div>
                    <!-- /.box-header -->
                    
                    <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                        {!! Form::open(array('url'=>"root/faq/",'class'=>'form-inline','method'=>'GET')) !!}
                            {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                            {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                            <a class="btn btn-primary" href="{{url('root/faq/')}}">Reset Search</a>
                        {!! Form::close() !!}
                    </div>

                    <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <a class="btn btn-default" data-toggle="modal" data-target="#add-form"
                               data-original-title><span>Add</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" onclick="download_faqs()" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable"><span>Download</span></a>
                            <a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2"
                               tabindex="0" aria-controls="simpledatatable" onclick="delete_faqs()"><span>Delete</span></a>
                        </div>
                    </div>
                    
                    <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>

                    <div class="clearfix" style="margin: 20px 0;"></div>
                    
                    <div class="box-body">
					@if(Session::has('message'))
						<div class="alert alert-success" style="padding: 7px 15px;">
						  {{ Session::get('message') }}
						  {{ Session::put('message','') }}
						</div>
					@endif
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                                    <div class="checkbox icheck">
                                        <label>
                                        {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                        <span class=""></span>
                                        </label>
                                    </div>
                                </th>
                                <th>
                                    <a href="{{url('root/faq/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='question' ? 'question-desc' :'question';?>">
                                    Question
                                    <?php if($type=='question'){ echo $class; } elseif($type=='question-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('root/faq/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='answer' ? 'answer-desc' :'answer';?>">
                                    Answer
                                    <?php if($type=='answer'){ echo $class; } elseif($type=='answer-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('root/faq/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='created' ? 'created-desc' :'created';?>">
                                    Created At
                                    <?php if($type=='created'){ echo $class; } elseif($type=='created-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>Category</th>
                                <!--<th>Created By</th>-->
                                <th>Status (Click to change)</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                                
                            @foreach($data['faq'] as $faq)
                                <tr>
                                    <td>
                                        <div class="checkbox icheck" style="margin:0;">
                                            <label>{!! Form::checkbox('faq_check',$faq->id,false,['id'=>'check','class'=>'faq_check checkBoxClass']) !!} <span> </span></label>
                                        </div>
                                    </td>
                                    <td>{{$faq->question}}</td>
                                    <td>{{$faq->answer}}</td>
                                    <td>{{date('d M Y',strtotime($faq->created_at))}}</td>
                                    <td>{{$faq->category['name']}}</td>
                                    <!--<td>{{$faq->users['email']}}</td>-->
                                    <td><?php if($faq->status==1){ ?><i class="fa fa-check text-success fa-2xkc text-success" onclick="changeStatus(0,'<?php echo $faq->id ?>')"></i><?php } else{ ?><i class="fa fa-ban text-danger fa-2xkc text-danger" onclick="changeStatus(1,'<?php echo $faq->id; ?>')"></i><?php } ?></td>
                                    <td><a class="btn btn-primary DTTT_button_csv" href="{{ "faq/".$faq->id."/edit"}}"><span>Edit</span></a></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {!! $data['faq']->appends(Request::except('page'))->render() !!}
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<div class="modal fade" id="add-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add New
                    FAQ.</h4>
            </div>

            <div class="modal-body" style="padding: 5px;">
                <div class="panel-body">
                    @if($errors->any())
                        {!! Html::script('resources/assets/js/jQuery-2.2.0.min.js') !!}
                        <script>
                            $(document).ready(function(){
                                $('#add-form').modal();
                            });
                        </script>
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li> {{$error}} </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{--<form role="form">--}}
                    {!! Form::open(['route'=>'root.faq.store','method'=>'post','id'=>'faqform']) !!}


                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Question</label><span class="red">*</span>
                                {{--<input type="text" id="name" class="form-control">--}}
                                {!! Form::text('question','',['class'=>'form-control required']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label">Answer</label><span class="red">*</span>
                                {!! Form::textarea('answer','',['class'=>'form-control required']) !!}
                            </div>

                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label">Category</label><span class="red">*</span>
                                {!! Form::select('category',$data['cat'], '', ['class'=>'form-control required']) !!}
                            </div>

                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label">Status</label>
                                {!! Form::select('status',['1'=>'Active','0'=>'Inactive'], '1', ['class'=>'form-control']) !!}
                            </div>

                        </div>

                    </div>

                    <div class="form-group margin-top">
                        {{-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add</button>--}}
                        {!! Form::submit('Add FAQ',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                        <hr>
                        <p><span class="red">*</span> - Required Fields.</p>
                    </div>
                    {{--</form>--}}
                    {!! Form::close() !!}
                </div>

            </div>

        </div>
    </div>
</div>
  
<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
@endsection

@section('script')

    {!! Html::script('resources/assets/js/jquery.validate.min.js') !!}
    {!! Html::script('resources/assets/js/additional-methods.js') !!}

    <script type="text/javascript">

        $(document).ready(function(){
            $.validator.addMethod("checkSpecialLettersGerman", function(value, element) {
		return this.optional(element) || /^[a-zA-ZäÄöÖüÜß«» -]+$/i.test(value);
            }, "Question must contain only letters.");
            
            $("#faqform").validate({
                rules: {
                    question: {
                        checkSpecialLettersGerman: true
                    },
                    answer: {
                        minlength: 10
                    }
                }
            });
        });

        function delete_faqs(){
            var len = $(".faq_check:checked").length;
            if(len==0){
                alert('Please select atleast one FAQ.');
                return false;
            }

            var r = confirm("Are you sure to delete the selected FAQs.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="faq_check"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: '{{url('root/faq/delete')}}',
                    type: "post",
                    dataType:"json",
                    data: {'selectedfaq':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                        if(res.success==1)
                            location.reload();
                        else
                            alert('Error occured.!! Please try after some time.');
                    }
                });
            } else {
                return false;
            }
        }

        function changeStatus(status, id)
        {
            $.ajax({
                type	: "POST",
                url		: '{{url('root/faq/changeStatus')}}',
                accepts	: 'application/json',
                dataType:"json",
                data	: {
                    id		    : id,
                    status		: status,
                    _token      :"{{ csrf_token() }}"
                },
                success: function (data) {
                    if(data.success==1)
                        location.reload();
                    else
                        alert('Error occured.!! Please try after some time.');
                }
            });
        }
		
	/*added by Rajlakshmi(31-05-16)*/
		function download_faqs(){
            var len = $(".faq_check:checked").length;
            if(len==0){
                alert('Please select atleast one FAQ.');
                return false;
            }

            var r = confirm("Are you sure to download the selected FAQs.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="faq_check"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: '{{url('root/faq/download')}}',
                    type: "post",
                    dataType:"json",
                    data: {'selectedfaq':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("faq_list.pdf");
				}
            });
        } else {
            return false;
        }
	   }
	 
/*added by Rajlakshmi(31-05-16)*/
    </script>
@endsection