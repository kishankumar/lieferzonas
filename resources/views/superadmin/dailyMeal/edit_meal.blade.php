﻿@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Restaurant Daily Meal
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active"><a href="{{route('root.daily.meal.index')}}"><i class="fa fa-dashboard"></i>Restaurant Daily Meal</a></li>
        <li class="active">Edit Daily Meal</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Meal With Full Features</h3>
                </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('message'))
                            <div class="alert alert-success" style="padding: 7px 15px;">
                                {{ Session::get('message') }}
                                {{ Session::put('message','') }}
                            </div>
                        @endif
                
                {{--    Error Display--}}
                @if($errors->any())
					<div class="alert alert-danger ">
						<ul class="list-unstyled">
							@foreach($errors->all() as $error)
								<li> {{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
                {{--    Error Display ends--}}
        
        {!! Form::model($mealInfo,['route'=>['root.daily.meal.update',$mealInfo->id],'method'=>'patch','id'=>'editMealForm']) !!}
            
            <div class="panel-body">
                <div class="errors" id="errors" style="color: red"></div>
                
                {!! Form::hidden('meal_id',$mealInfo->id,['id'=>'meal_id']) !!}
                {!! Form::hidden('restaurant',$mealInfo->rest_id,['id'=>'restaurant']) !!}
                
<!--                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4>{!! Form::label('Choose Restaurant') !!}</h4>
                            <div class="form-group row">
                                <select class="form-control" name="restaurant" id="restaurant" onchange="getCatByRestaurant(this.value,'category')">
                                    <option value="">Select Restaurant</option>
                                    @if(count($restInfo))
                                        @foreach($restInfo as $restro_name)
                                            <option value="{{$restro_name->id}}" <?=@(($restro_name->id==$mealInfo->rest_id) ? 'selected' :'');?> >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span id="restaurantErr" style="color:red"></span>
                            </div>
                        </div>
                    </div>
                </div>-->

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <h4>{!! Form::label('Restaurant Name') !!}</h4>
                        @if(count($restInfo))
                            @foreach($restInfo as $restro_name)
                                @if($restro_name->id==$mealInfo->rest_id)
                                    {{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}
                                @endif    
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group row">
                        <label class="col-sm-2 control-label"> <span>Choose Category</span></label>
                        <label class="col-sm-2 control-label"> <span>Choose Menu</span></label>
                        <label class="col-sm-2 control-label"><span>Choose Sub menu</span></label>
                    </div>
                    
                    <div class="form-group row">
                        <div class="form-group col-sm-3" id="cat">
                            <select class="form-control" name="category" id="category" 
                                onchange="getData(this.value,'rest_categories','rest_menus','menus','id','name','id','rest_category_id','categoryErr')">
                                <option value="">Select Category</option>
                                
                            </select>
                            <span id="categoryErr" class="text-danger"></span>
                        </div>
                        
                        <div class="form-group col-sm-3" id="menu">
                            <select class="form-control" name="menu" id="menus" onchange="getData(this.value,'rest_menus','rest_sub_menus','submenus','id','name','id','menu_id','menuErr')">
                                <option value="">Select Menu</option>
                            </select>
                            <span id="menuErr" class="text-danger"></span>
                        </div>
                        
                        <div class="form-group col-sm-3" id="submenu">
                            <select class="form-control" name="submenu" id="submenus" >
                                <option value="">Select Sub menu</option>
                            </select>
                            <span id="submenuErr" class="text-danger"></span>
                        </div>

                        <div class="form-group col-sm-3">
                            <a class="btn  btn-primary"style="border:none;" onclick='addcatmenu();'>Add</a>
                        </div>
                    </div>
                    
                    <div id="delerrors" style="color: red"></div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Item Name</th>
                                <th>Quantity</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="check_table">
                            <?php
                            foreach($itemMaps as $itemMap){
                               if($itemMap->item_type=='submenu'){ ?>
                                    <tr class='row-submenu<?=$itemMap->item_id;?>'>
                                        <td><?=$itemMap->item_name;?></td>
                                        <td>
                                            <input class="quantity" id="<?=$itemMap->item_type;?>-<?=$itemMap->item_id;?>-<?=$itemMap->rest_id;?>" value="<?=$itemMap->item_quantity;?>" name="quantitySub[<?=$itemMap->item_id;?>]" type="text">
                                            <span id="Err<?=$itemMap->item_id;?>" style="color:red"></span>
                                            <input name="selectedSubType[<?=$itemMap->item_id;?>]" value='<?=$itemMap->item_type;?>' type="hidden">
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" class="text-danger" onclick="remove_row('submenu<?=$itemMap->item_id;?>');">
                                            <i class="fa fa-close"></i> Delete</a>
                                        </td>
                                    </tr>
                                <?php }else { ?>
                                    <tr class='row-menu<?=$itemMap->item_id;?>'>
                                        <td><?=$itemMap->item_name;?></td>
                                        <td>
                                            <input class="quantity" id="<?=$itemMap->item_type;?>-<?=$itemMap->item_id;?>-<?=$itemMap->rest_id;?>" value="<?=$itemMap->item_quantity;?>" name="quantity[<?=$itemMap->item_id;?>]" type="text">
                                            <span id="Err<?=$itemMap->item_id;?>" style="color:red"></span>
                                            <input name="selectedType[<?=$itemMap->item_id;?>]" value='<?=$itemMap->item_type;?>' type="hidden">
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" class="text-danger" onclick="remove_row('menu<?=$itemMap->item_id;?>');">
                                            <i class="fa fa-close"></i> Delete</a>
                                        </td>
                                    </tr>
                                <?php } } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="form-group">
                {!! Form::label('Meal Name') !!}  <span id="meal_nameErr" style="color:red"></span>
                {!! Form::text('meal_name',$mealInfo->meal_name,["id"=>'meal_name',"class"=>"form-control"]) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('Description') !!} <span id="descriptionErr" style="color:red"></span>
                {!! Form :: textarea('description',$mealInfo->meal_desc,['style'=>'height:200px;','id'=>'description','placeholder'=>'Enter description ','class'=>'form-control'])  !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('Meal Price') !!} <span id="meal_priceErr" style="color:red"></span>
                {!! Form::text('meal_price',$mealInfo->meal_price,["id"=>'meal_price',"class"=>"form-control"]) !!}
            </div>
            
            <?php
            if((strtotime($mealInfo->valid_from)>0) || ($mealInfo->valid_from !='0000-00-00 00:00:00')){
                $valid_from= date("m/d/Y", strtotime($mealInfo->valid_from));
            }else{
               $valid_from= date("m/d/Y");
            }
            if((strtotime($mealInfo->valid_to)>0) || ($mealInfo->valid_to !='0000-00-00 00:00:00')){
                $valid_to= date("m/d/Y", strtotime($mealInfo->valid_to));
            }else{
                $valid_to= date("m/d/Y");
            }
            ?>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('Valid From') !!} <span id="validFrom" style="color:red"></span>
                        <div class="input-group date" id="datetimepicker23">
                            {!! Form::text('valid_from',$valid_from,['disabled'=>'disabled',"id"=>'valid_from',"class"=>"form-control","placeholder"=>"Valid-From"]) !!}
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('Valid To') !!} <span id="validTo" style="color:red"></span>
                            <div class="input-group date" id="datetimepicker24">
                            {!! Form::text('valid_to',$valid_to,["id"=>'valid_to',"class"=>"form-control","placeholder"=>"Valid-To"]) !!}
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6 col-md-3 col-lg-2">
                    <div class="form-group">
                        {!! Form::label('Active Time From') !!}
                        {!! Form::select('active_time_from',$fromtime, $mealInfo->active_time_from, ['id'=>'active_time_from','class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-2">
                    <div class="form-group">
                        {!! Form::label('Active Time To') !!}
                        {!! Form::select('active_time_to',$totime, $mealInfo->active_time_to, ['id'=>'active_time_to','class' => 'form-control']) !!}
                    </div>
                </div>
                <span id="timeErr" style="color:red"></span>
            </div>
            
            <div class="form-group">
                {!! Form::label('Status') !!}
                {!! Form::select('status',
                array(
                '1'=>'Active',
                '0'=>'Deactive',
                ), $mealInfo->status, ['id' => 'status','class' => 'form-control']) !!}
                <span id="statusErr" style="color:red"></span>
            </div>
            
            <div class="form-group">
                <div class="col-md-12 form-group">
                    <label class="col-sm-2 control-label">Choose Days for meal</label>
                    <div class="col-sm-9">
                        @foreach($days as $dayss)
                            <label class="checkbox checkbox-inline">
                                @if(in_array($dayss->id,$day_maps))
                                <input type="checkbox" class="selecteddays" name="selecteddays[]" value="{{$dayss->id}}" checked >
                                @else
                                    <input type="checkbox" class="selecteddays" name="selecteddays[]" value="{{$dayss->id}}" >
                                @endif
                                <span>{{$dayss->day}} </span>
                            </label>
                        @endforeach
                        <span id="daysErr" style="color:red"></span>
                    </div>
                </div>
            </div>
            
            <div class="form-group margin-top">
                <input type="button" value="Update" name="formEdit" onclick="edit_meal()" class="btn  btn-primary">
                <hr>
            </div>
            
        {!! Form::close() !!}
            
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
    </div>
</section>
				
    </div>
@endsection

@section('script')
<script type="text/javascript">
    
    var restaurantId = parseInt('<?=$mealInfo->rest_id;?>');
    if(restaurantId){
        getCatByRestaurant(restaurantId,'category');
    }
    
    function minFromMidnight(tm){
        var ampm= tm.substr(-2);
        var clk = tm.substr(0, 5);
        var m  = parseInt(clk.match(/\d+$/)[0], 10);
        var h  = parseInt(clk.match(/^\d+/)[0], 10);
        if(ampm.match(/pm/i)){
            if(h != 12){
                h += (ampm.match(/pm/i))? 12: 0;
            }
        }
        return h*60+m;
    }
    
    function edit_meal(){
        $('#timeErr,#daysErr,#restaurantErr,#categoryErr,#menuErr,#submenuErr,#delerrors,#meal_nameErr,#descriptionErr,#meal_priceErr,#validFrom,#validTo').html('');
//        var restaurant = $('#restaurant').val().trim();
//        if(restaurant=='' || restaurant==0 || isNaN(restaurant)){
//            $("#restaurant").focus();
//            $("#restaurantErr").html('Please select restaurant');
//            return false;
//        }
        var rowCount = $('.check_table tr').length;
        if(rowCount>0){
            $('#delerrors').html('');
            var count=0;
            $(".quantity").each(function() {
                var quanV=/^[0-9]+([,.][0-9]{1,2})?$/;
                var validQuan=quanV.test($(this).val());
                if($(this).val()=='' || $(this).val()< 1){
                    $(this).next().html("Quantity should be more than 0");
                    count=1;
                }
                else if(validQuan==false)
                {
                    $(this).next().html("Please enter valid quantity");
                    count=1;
                }
                else
                {
                    $(this).next().html("");
                }
            });
            if(count==1){
                return false;
            }
        }else{
            $('#delerrors').html('Please add atleat one item');
            return false;
        }
//        var meal_name=$("#meal_name").val().trim();
//        var description=$("#description").val().trim();
//        var meal_price=$("#meal_price").val().trim();
//        if(meal_name=='' || meal_name==0){
//            $("#meal_name").focus();
//            $("#meal_nameErr").html('Please enter meal name');
//            return false;
//        }
//        if(description=='' || description==0){
//            $("#description").focus();
//            $("#descriptionErr").html('Please enter description');
//            return false;
//        }
//        if(meal_price==''){
//            $("#meal_price").focus();
//            $("#meal_priceErr").html('Please enter meal price');
//            return false;
//        }
//        var quanV=/^[0-9]+([,.][0-9]{1,2})?$/;
//        var validQuan=quanV.test(meal_price);
//        if(meal_price < 1){
//            $("#meal_price").focus();
//            $("#meal_priceErr").html('Meal price must be greater than 1');
//            return false;
//        }
//        if(validQuan==false)
//        {
//            $("#meal_price").focus();
//            $("#meal_priceErr").html('Please enter valid meal price');
//            return false;
//        }
//        var valid_from=$("#valid_from").val().trim();
//        var valid_to=$("#valid_to").val().trim();
//        var active_time_from=$("#active_time_from").val().trim();
//        var active_time_to=$("#active_time_to").val().trim();
//        if(valid_from=='' || valid_from==0){
//            $("#valid_from").focus();
//            $("#validFrom").html('Please enter valid from date');
//            return false;
//        }
//        if(valid_to=='' || valid_to==0){
//            $("#valid_to").focus();
//            $("#validTo").html('Please enter valid to date');
//            return false;
//        }
//        var active_time_from=$("#active_time_from").val().trim();
//        var active_time_to=$("#active_time_to").val().trim();
//        st = minFromMidnight(active_time_from);
//        et = minFromMidnight(active_time_to);
//        if(st>=et){
//            var errorsHtml = 'End time must be greater than start time';
//            $('#timeErr').html(errorsHtml);
//            return false;
//        }
//        var daysLen = $(".selecteddays:checked").length;
//        if(daysLen<1){
//            $("#daysErr").html('Please check atleast one day for meal.');
//            return false;
//        }
        //document.forms["meal_form"].submit();
        $('#editMealForm').submit();
    }
    
    function addcatmenu()
    {
        $('#restaurantErr,#categoryErr,#menuErr,#submenuErr').html('');
        //var restaurant = $('#restaurant').val().trim();
        var category = $('#category').val().trim();
        var menus = $('#menus').val().trim();
        var submenus = $('#submenus').val().trim();
//        if(restaurant=='' || restaurant==0 || isNaN(restaurant)){
//            $("#restaurant").focus();
//            $("#restaurantErr").html('Please select restaurant');
//            return false;
//        }
        if(category=='' || category==0 || isNaN(category)){
            $("#category").focus();
            $("#categoryErr").html('Please select category');
            return false;
        }
        if(menus=='' || menus==0 || isNaN(menus))
        {
            $("#menus").focus();
            $("#menuErr").html('Please select menu');
            return false;
        }
        var submenuLen = $('#submenus option').length;
        var right=0;
        if(submenuLen>1){
            if(submenus=='' || submenus==0 || isNaN(submenus))
            {
                $("#submenus").focus();
                $("#submenuErr").html('Please select sub menu');
                return false;
            }else{
                var menuText=$("#submenus option:selected").text();
                var selectedId = submenus;
                var type='submenu';
                var right=1;
            }
        }else{
            var menuText=$("#menus option:selected").text();
            var selectedId = menus;
            var type='menu';
            var right=1;
        }
        var exist=0;var restExist=0;
        var ItemLen = $('.quantity').length;
        if(ItemLen>0){
            $('.quantity').each(function(){
                var str=$(this).attr('id');
                var res = str.split("-");
                var itemType = res[0];
                var itemId = res[1];
                if(itemType==type && itemId==selectedId){
                    exist=1;
                }
            });
        }
        
        if(exist){
            $("#delerrors").html('Item already exist in meal');
            return false;
        }
        
        if(right){
            if(type=='submenu')
            {
                $(".check_table").append('<tr class='+'row-submenu'+selectedId+'><td>'+menuText+'</td>\n\
                                <td><input class="quantity" id='+type+'-'+selectedId+' value="1" name="quantitySub['+selectedId+']" type="text"><span id="Err'+selectedId+'" style="color:red">\n\
                                </span><input name="selectedSubType['+selectedId+']" value='+type+' type="hidden"></td>\n\
                                <td><a href="javascript:void(0)" class="text-danger" onclick="remove_row(\'submenu'+selectedId+'\');">\n\
                                <i class="fa fa-close"></i> Delete</a></td></tr>');
            }else{
                $(".check_table").append('<tr class='+'row-menu'+selectedId+'><td>'+menuText+'</td>\n\
                                <td><input class="quantity" id='+type+'-'+selectedId+' value="1" name="quantity['+selectedId+']" type="text"><span id="Err'+selectedId+'" style="color:red">\n\
                                </span><input name="selectedType['+selectedId+']" value='+type+' type="hidden"></td>\n\
                                <td><a href="javascript:void(0)" class="text-danger" onclick="remove_row(\'menu'+selectedId+'\');">\n\
                                <i class="fa fa-close"></i> Delete</a></td></tr>');
            }
        }
    }
    
    function getCatByRestaurant(restro_id,firstAppendId){
        $.ajax({
            url: base_url+'/ajax/getCatKitchenPriceType',
            type: "POST",
            dataType:"json",
            data: {'restroId':restro_id, "_token":"{{ csrf_token() }}"},
            success: function (data) {
                var catgory=data.option+data.categoryItem;
                $('#'+firstAppendId).html(catgory);
                $('#menus').html(data.option);
                $('#submenus').html(data.option);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    }
    
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    function remove_row(checked){
        $('.check_table .row-'+checked).remove();
    }

</script>


@endsection
   
