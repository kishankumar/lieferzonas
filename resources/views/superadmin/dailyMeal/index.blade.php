﻿@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Restaurant Daily Meal
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active"><a href="{{route('root.daily.meal.index')}}"><i class="fa fa-dashboard"></i>Restaurant Daily Meal</a></li>
        <li class="active">Daily Meal List</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">Restaurant Daily Meal With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/daily/meal",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/daily/meal')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    <a class="btn btn-default" data-toggle="modal" href="{{ url('root/daily/meal/create') }}" >
                        <span>Add</span>
                    </a>
                    <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                        <span>Print</span>
                    </a>
                    <a  href="#" class="btn btn-default DTTT_button_print" onclick="download_meals()" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable" >
                        <span >Download</span>
                    </a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_meals()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
            </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            <div class="clearfix" style="margin: 20px 0;"></div>
            <div class="box-body">
                @if(Session::has('message'))
					<div class="alert alert-success" style="padding: 7px 15px;">
					  {{ Session::get('message') }}
					  {{ Session::put('message','') }}
					</div>
				@endif
                
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>
                                <div class="checkbox icheck">
                                    <label>
                                    {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                    <span class=""></span>
                                    </label>
                                </div>
                            </th>
                            <th>
                                <a href="{{url('root/daily/meal/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                                Meal Name
                                <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                                </a>
                            </th>
                            <th>
                                <a href="{{url('root/daily/meal/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                                Meal Description
                                <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                                </a>
                            </th>
                            <th>
                                <a href="{{url('root/daily/meal/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='price' ? 'price-desc' :'price';?>">
                                Meal Price
                                <?php if($type=='price'){ echo $class; } elseif($type=='price-desc'){ echo $class1; } else { echo $class2; } ?>
                                </a>
                            </th>
                            <th>
                                <a href="{{url('root/daily/meal/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='from' ? 'from-desc' :'from';?>">
                                Valid From
                                <?php if($type=='from'){ echo $class; } elseif($type=='from-desc'){ echo $class1; } else { echo $class2; } ?>
                                </a>
                            </th>
                            <th>
                                <a href="{{url('root/daily/meal/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='to' ? 'to-desc' :'to';?>">
                                Valid To
                                <?php if($type=='to'){ echo $class; } elseif($type=='to-desc'){ echo $class1; } else { echo $class2; } ?>
                                </a>
                            </th>
                            <th>Restaurant Name</th>
                            <th>Status</th>
                            <th>Action</th>
                            <!--<th>View</th>-->
                        </tr>
                    </thead>
                    
                    <tbody>
                    
                    @if(count($mealInfo))
                         @foreach($mealInfo as $info)
                            <tr>
                                <td>
                                    <div class="checkbox icheck">
                                        <label>
                                            {!! Form::checkbox('meals', $info->id, null, ['class' => 'meals checkBoxClass']) !!}
                                            <span class=""></span>
                                        </label>
                                    </div>
                                </td>
                                <td>{{ ucfirst($info->meal_name) }}</td>
                                <td>{{ (strlen($info->meal_desc) > 50) ? substr($info->meal_desc,0,50).'...' : $info->meal_desc  }}</td>
                                <td>&euro; {{ ($info->meal_price) }}</td>
                                <td>@if(strtotime($info->valid_from)>0)
                                    {{ date("d M Y", strtotime($info->valid_from))  }}
                                    @else
                                        {{ 'None' }}
                                    @endif
                                </td>
                                <td>@if(strtotime($info->valid_to)>0)
                                        {{ date("d M Y", strtotime($info->valid_to))  }}
                                    @else
                                        {{ 'None' }}
                                    @endif
                                </td>
                                <td>{{ ucfirst($info->getResturant->f_name) }} {{ ucfirst($info->getResturant->l_name) }} </td>
                                <td>
                                    @if($info->status == 1) 
                                        <i class="fa fa-check text-success fa-2xkc text-success"></i>
                                    @else
                                        <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-success" href="{{url('root/daily/meal/'.$info->id.'/edit')}}"> <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                                <!--<td></td>-->
                             </tr>
                         @endforeach
                     @else 
                         <tr>
                             <td colspan="10" align="center">
                                 No Record Exist
                             </td>
                         </tr>
                      @endif
                </tbody>
            </table>
                
            {!! $mealInfo->appends(Request::except('page'))->render() !!}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->


 <!-- /.modal -->
 @endsection

@section('script')
<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    function delete_meals(){
       var len = $(".meals:checked").length;
       if(len==0){
           alert('Please select atleast one meal');
           return false;
       }
       var base_url1=base_url+'daily/meal/delete';
       var r = confirm("Are you sure to delete meals?");
       if (r == true) {
           var selectedMeals = new Array();
           $('input:checkbox[name="meals"]:checked').each(function(){
               selectedMeals.push($(this).val());
           });
           $.ajax({
               url: base_url1,
               type: "post",
               dataType:"json",
               data: {'selectedMeals':selectedMeals, "_token":"{{ csrf_token() }}"},
               success: function(res){
                   if(res.success==1)
                   {
                       //location.reload();
                   }
                   else
                   {
                       alert('Meals not deleted');
                   }
               }
           });
       } else {
           return false;
       }
   }
	
	function download_meals() 
	{
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
       };
	    
	   var len = $(".meals:checked").length;
       if(len==0){
           alert('Please select atleast one meal');
           return false;
       }
      
       var r = confirm("Are you sure to download meals?");
        if (r == true) {
           var selectedMeals = new Array();
           $('input:checkbox[name="meals"]:checked').each(function(){
               selectedMeals.push($(this).val());
           });
			 
            $.ajax({
                url: 'meal/download',
                type: "post",
                dataType:"json",
                data: {'selectedMeals':selectedMeals, "_token":"{{ csrf_token() }}"},
                success: function(res){
                        $("#download").html(res.data1);
					    var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Daily Meal List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
						
                     doc.save("dailymeal.pdf");
				}
				
            });
			
        } else {
            return false;
        }
		
	}

</script>

@endsection




