@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Order Rating Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Order Rating Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
            </div>
            
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>-->
					 <a class="btn btn-danger DTTT_button_xls" onclick="delete_order_rate()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                         <span>Delete</span>
                     </a>
                    
                    
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('errmsg'))
					<div class="alert alert-danger" style="padding: 7px 15px;">
					  {{ Session::get('errmsg') }}
					  {{ Session::put('errmsg','') }}
					</div>
				@endif
				@if($errors->any())
					<div class="alert alert-danger ">
						<ul class="list-unstyled">
							@foreach($errors->all() as $error)
								<li> {{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
                
            <div class="row">
					<input type="hidden" name="user_id" id="user_id">
					<input type="hidden" name="rest_id" id="rest_id">
					<input type="hidden" name="order_id" id="order_id">
						<div class="col-xs-12">
							<div class="form-group qrating">
								Quality: <input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1"/>
							</div>
							<div class="form-group drating" >  
								Service: <input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1"/>
							</div>
							<div class="form-group">
								<textarea placeholder="Comment" id="comment" name="comment" class="form-control modal-input"></textarea>
							</div>
							<div class="form-group">
								<button  id="review"  class="btn btn-success btn-block brn-rg" onclick="add_review();">Submit</button>
							</div>
						</div>
					
			</div>
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 
@endsection
@section('script')
<script>
function delete_order_rate(){
        var len = $(".review:checked").length;
        if(len==0){
            alert('Please select atleast one review');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
       
        var r = confirm("Are you sure to delete reviews?");
        if (r == true) {
            var selectedReviews = new Array();
            $('input:checkbox[name="order_rate_id"]:checked').each(function(){
                selectedReviews.push($(this).val());
            });
            $.ajax({
                url: 'order_rating/delete',
                type: "post",
                dataType:"json",
                data: {'selectedReviews':selectedReviews, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
						
                    }
                    else
                    {
                        alert('Records not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
</script>
@endsection




