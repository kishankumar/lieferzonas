@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1>
        User Order Rating Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		  <li><a href="{{route('root.userorder.order_rating.index')}}"><i class="fa fa-dashboard"></i> Order Rating</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Order Rating Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
            </div>
            
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>-->
					 <a class="btn btn-danger DTTT_button_xls" onclick="delete_order_rate()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                         <span>Delete</span>
                     </a>
                    
                    
                </div>
            </div>
            <div class="clearfix" style="margin: 20px 0;"></div>
            <div class="box-body">
                
                @if(Session::has('message'))
					<div class="alert alert-success" style="padding: 7px 15px;">
					  {{ Session::get('message') }}
					  {{ Session::put('message','') }}
					</div>
				@endif
                
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
					<th>
                        <div>
							<label  class="checkbox icheck">
							{!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
							<span class=""></span>
							</label>
                        </div>
                    </th>
                    <th>Order No.</th>
					<th>Order Date</th>
					<th>Customer Name</th>
					<th>Restaurant Name</th>
					<th>Quality Rating</th>
					<th>Service Rating</th>
					<th>Reported</th>
					<th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                 @if(count($order_rate))
                    @foreach($order_rate as $order_rates)
                <tr>
				    <td>
                        <div class="checkbox1 icheck">
                            <label>
                                {!! Form::checkbox('order_rate_id', $order_rates->id, null, ['class' => 'review checkBoxClass']) !!}
                                <span class="button-checkbox"></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ $order_rates->order_id }}</td>
                    <td>{{ $order_rates->created_at }}</td>
                    <td>{{ $order_rates->booking_person_name }}</td>
                    <td>{{ $order_rates->f_name.' '.$order_rates->l_name }}</td>
                    <td>{{ $order_rates->quality_rating }}</td>
                    <td>{{ $order_rates->service_rating }}</td>
                    <td>@if($order_rates->is_reported==1)Yes @else No @endif </td>
                    <td>
                        <a href="#" class="btn btn-success" data-target="#" data-toggle="modal" onclick="edit_review(<?=$order_rates->id?>);">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                </tr>
                
                      @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
              
                </tbody>
                
              </table>
                 {!! $order_rate->render() !!}  
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 <div aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal_rating" class="modal fade">
	<div class="modal-dialog modal-md robotoregular">
		<div class="modal-content">
			<div style="background:#81c02f; color:#fff;" class="modal-header">
				<button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Edit Review</h4>
			</div>
			<div class="modal-body">
				
				@if(Session::has('errmsg'))
					<div class="alert alert-danger" style="padding: 7px 15px;">
					  {{ Session::get('errmsg') }}
					  {{ Session::put('errmsg','') }}
					</div>
				@endif
				@if($errors->any())
					<div class="alert alert-danger ">
						<ul class="list-unstyled">
							@foreach($errors->all() as $error)
								<li> {{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
                       {!! Form::model($order_rate,['route'=>['root.userorder.order_rating.update'],'method'=>'patch',
                       'id' => 'edit_form','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}
				<div class="row">
					<input type="hidden" name="user_id" id="user_id">
					<input type="hidden" name="rest_id" id="rest_id">
					<input type="hidden" name="order_id" id="order_id">
					<input type="hidden" name="review_id" id="review_id">
						<div class="col-xs-12">
						    <div class="form-group qrating">
								Order Id : <!--<input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1"/>-->
								<span id="order"></span>
							</div>
							<div class="form-group qrating">
								Customer Name : <!--<input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1"/>-->
								<span id="customer_name"></span>
							</div>
							<div class="form-group qrating">
								Restaurant Name: <!--<input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1"/>-->
								<span id="rest_name"></span>
							</div>
							<div class="form-group qrating">
								Quality Rating<span class="red">*</span>  : <!--<input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1"/>-->
								<input type="text" name="qrating" id="qrating" class="form-control modal-input">
							</div>
							<div class="form-group drating" >  
								Service Rating<span class="red">*</span> : <!--<input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1"/>-->
								<input type="text" name="srating" id="srating" class="form-control modal-input">
							</div>
							<div class="form-group">
								Comment<span class="red">*</span> :<textarea placeholder="Comment" id="comment" name="comment" class="form-control modal-input"></textarea>
							</div>
							<div class="form-group">
								{!! Form::submit('Update',['id'=>'submit','class'=>'btn  btn-primary']) !!}
								<hr>
                                <p><span class="red">*</span> - Required Fields.</p>
							</div>
						</div>
					
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
function delete_order_rate(){
        var len = $(".review:checked").length;
        if(len==0){
            alert('Please select atleast one review');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
       
        var r = confirm("Are you sure to delete reviews?");
        if (r == true) {
            var selectedReviews = new Array();
            $('input:checkbox[name="order_rate_id"]:checked').each(function(){
                selectedReviews.push($(this).val());
            });
            $.ajax({
                url: 'order_rating/delete',
                type: "post",
                dataType:"json",
                data: {'selectedReviews':selectedReviews, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
						
                    }
                    else
                    {
                        alert('Records not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
	function edit_review(review_id)
		 {
			var review_id = review_id;
            $.ajax({
                url: '{{ url('fetchreviewdata') }}',
                type: "GET",
                dataType:"json",
                data: {'review_id':review_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
						$('#rest_id').val(res.rest_id);
                        $('#user_id').val(res.user_id);
                        $('#order_id').val(res.order_id);
						$('#order').text(res.order_id);
						$('#rest_name').text(res.rest_name);
						$('#customer_name').text(res.booking_person_name);
                        $('#qrating').val(res.qrating);
                        $('#srating').val(res.srating);
                        $('#comment').val(res.description);
                        $('input[name=review_id]').val(res.id);
                        $('#myModal_rating').modal('show');
                    }
                    else
                    {
                        //document.getElementById("edit-form").reset();
                    }
                }
            });
			
		 }
		 
		  $(document).ready(function(){
     $("#edit_form").validate({
      rules: {

        qrating: {
            required: true,
            number:true,
			max:5
               },
	    srating: {
            required: true,
            number:true,
			max:5
               },
        comment: {
            required: true
               },
        
        
        },
       messages: {
                    qrating:{
					   required: "Please enter quality rating",
					   number: "Please enter digit for quality rating",
					   max: "Please enter quality rating less than and equal to 5"
				    } ,
					srating:{
					   required: "Please enter service rating",
					   number: "Please enter digit for service rating",
					   max: "Please enter service rating less than and equal to 5"
				    } ,
                    
                    comment: "Please enter description",
                    
                    agree: "Please accept our policy"
                },
         
     });
});
</script>
@if($errors->any())
    <script type="text/javascript">
      $('#myModal_rating').modal('show');
    </script>
    @endif
@endsection




