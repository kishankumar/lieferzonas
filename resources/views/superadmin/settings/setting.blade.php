@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Settings
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.setting.index')}}"><i class="fa fa-dashboard"></i>Super Admin</a></li>
            <li class="active">Settings</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Settings</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="col-sm-12 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                           
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                           
                        </div>
                    </div>

                     <div class="clearfix" style="margin: 20px 0;"></div>
					<div class="box-body">
						
						@if(Session::has('message'))
							<div class="alert alert-success" style="padding: 7px 15px;">
							  {{ Session::get('message') }}
							  {{ Session::put('message','') }}
							</div>
						@endif
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Cashback Point Value</th>
								<th>Bonus Point Value</th>
                                <th>Service Tax</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
							<?php $i=1; ?>
                            @if(count($setting))
                            @foreach($setting as $settings)
                            <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{$settings['cashback_point_value']}}</td>
									<td>{{$settings['bonus_point_value']}}</td>
                                    <td>{{$settings['service_tax']}}</td>
                                    <td> @if($settings['status']==1) <i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($settings['status']==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>
                                    <td><a  class="btn btn-success" href="#" onclick="editsetting({{$settings['id']}})"> <i class="fa fa-edit"></i></a></td>
                            </tr>
							<?php $i++; ?>
                            @endforeach
                             @else 
                                <tr>
                                    <td colspan="8" align="center">
                                        No Record Exist
                                    </td>
                                </tr>
                            @endif
                            </tbody>

                        </table>
                        {!! $setting->render() !!}
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<div class="modal fade" id="edit-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Edit Settings
            </div>

                <div class="modal-body" style="padding: 5px;">
                    <div class="panel-body">
                        @if(Session::has('errmsg'))
							<div class="alert alert-danger" style="padding: 7px 15px;">
							  {{ Session::get('errmsg') }}
							  {{ Session::put('errmsg','') }}
							</div>
						@endif
						@if($errors->any())
							<div class="alert alert-danger ">
								<ul class="list-unstyled">
									@foreach($errors->all() as $error)
										<li> {{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
                       
                       {!! Form::model($setting,['route'=>['root.setting.update'],'method'=>'patch',
                       'id' => 'edit_form','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Cashback point value</label><span class="red">*</span> 
                                       
                                        {!! Form::text('cashback_point_value','',['id'=>'cashback_point_value','class'=>'form-control']) !!}
                                         {!! Form::hidden('setting_id','',['id'=>'setting_id','class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Bonus point value</label><span class="red">*</span> 
                                        {!! Form::text('bonus_point_value','',['id'=>'bonus_point_value','class'=>'form-control']) !!}
                                    </div>

                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Service tax</label><span class="red">*</span> 
                                       
										{!! Form::text('service_tax','',['id'=>'service_tax','class'=>'form-control']) !!}
                                    </div>

                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Status</label>
                                        {!! Form::select('status',['1'=>'Active','0'=>'Deactive'], '', ['id'=>'status','class'=>'form-control']) !!}
                                    </div>

                                </div>

                            </div>

                            <div class="form-group margin-top">
                               
                                {!! Form::submit('Update',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                                <hr>
                                <p><span class="red">*</span> - Required Fields.</p>
                            </div>
                        {{--</form>--}}
                        {!! Form::close() !!}
                    </div>

                </div>

        </div>
    </div>
</div>

<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
	
@endsection

@section('script')


    <script type="text/javascript">
     $(document).ready(function(){
     $("#edit_form").validate({
      rules: {

        cashback_point_value: {
            required: true,
			digits:true
			},
        bonus_point_value: {
            required: true,
			digits:true
               },
        service_tax: {
            required: true,
			number : true
               },
        
        },
       messages: {

                   cashback_point_value:{
					   required: "Please enter Cashback point value",
					   digits: "Please enter digit for Cashback point value",
				   } ,
				   bonus_point_value:{
					   required: "Please enter Bonus point value",
					   digits: "Please enter digit for Bonus point value",
				   } ,
               
                   service_tax:{
					   required: "Please enter Service tax",
					   number: "Please enter digit for Service tax",
				   } ,
                    agree: "Please accept our policy"
                },
         
        });
    });


     function editsetting(id)   
     {
        
        var setting_id = id;
        $.ajax({
                url: '{{ url('getdataa') }}',
                type: "GET",
                dataType:"json",
                data: {'setting_id':setting_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
                        
                        $('#cashback_point_value').val(res.cashback_point_value);
                        $('#bonus_point_value').val(res.bonus_point_value);
                        $('#service_tax').val(res.service_tax);
                        $('#status').val(res.status);
                         $('input[name=setting_id]').val(res.id);
                        showModal('edit-form');
                    }
                    else
                    {
                        //document.getElementById("edit-form").reset();
                    }
                }
            });
     }
   

    </script>
     @if($errors->any())
        <script type="text/javascript">
         showModal('edit-form');
        </script>
     @endif
@endsection