@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shop Metrics Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
			 <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
			 <li><a href="{{route('root.shop.metricscatmap.index')}}"><i class="fa fa-dashboard"></i> Shop Management</a></li>
        <li class="active">Shop Metrics Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Shop Metrics Category With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/shop/metricscatmap/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/shop/metricscatmap/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
             <div class="col-sm-4 text-right" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    <a class="btn btn-primary" href="{{route('root.shop.metricscatmap.create')}}">
                        <span>Assign Metrics Category</span>
                    </a>
                 </div>
             </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>

            <div class="clearfix" style=""></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>S. No.</th>
                        <th>
                            <a href="{{url('root/shop/metricscatmap/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Category Name
                            <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Views</th>
                    </tr>
                </thead>
                <tbody>
               
                <?php $i=$pages['from']; ?>
                    
                @if(count($cat_names))    
                    @foreach($cat_names as $info)
                        <tr>
                            <td><?php echo $i; ?></td>
                            
                            <td>{{ ucfirst($info->name) }} </td>
                         	<?php $catName = $info->name; ?>
                            <td>
                                <a class="" href="javascript:void(0)" onclick='show_modal("cat-metrics-view",<?=$info->id?>,"cat-metrics-list","category-name","<?=ucfirst($catName)?>")'>View Assigned Metrics
                                </a>
                            </td>
                            
                           {!! Form :: hidden('category',$catName ,['id'=>'category_name'])  !!}
                          
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="4" align="center">
                            No Record Exist
                        </td>
                    </tr>
                @endif
            </tbody> 
        </table>
                {!! $cat_names->appends(Request::except('page'))->render() !!}
               		
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

    <div class="modal fade" id="cat-metrics-view" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-primary theme-border">
                <div class="panel-heading theme-bg">
                    <span id="category-name"></span>
                    <button type="button" id="modalclose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="panel-body">
				<div class="row PopupPublished"  id="cat-metrics-list">
                
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script>
    
    function show_modal(modelId,category_id,appendId,categoryNameId,categoryName) {
	$.ajax({
            url: '{{ url('shop/metricscatmap/getdata') }}',
            type: "get",
            data: {'category_id':category_id, '_token': '{{ csrf_field() }}'},
	    success: function (data) {
                if(data=='' || data==null){
                    data='No metrics assigned';
                }
                else{
                    data=data;
                }
                document.getElementById(categoryNameId).innerHTML = categoryName;
                document.getElementById(appendId).innerHTML = data;
            }
	});
	$("#"+modelId).modal('show');
    }
    
    function setForm(pageName) {
        $("#set-new-form").attr("action", pageName);
        $("#set-new-form").submit();
    }
    
//    $(function(){
//       $("#modalclose").click(function(){
//            $('#category-name,#cat-metrics-list').html('');
//        }); 
//    });

</script>

@endsection


