@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Shop Metrics Category
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
			 <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
			 <li><a href="{{route('root.shop.metricscatmap.index')}}"><i class="fa fa-dashboard"></i> Shop Management</a></li>
                <li class="active">Add Metrics</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
            <div class="row">
        <!-- Left col -->
                <section class="col-lg-12 connectedSortable">
        
                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                    </div>
                <!-- /.box-header -->

                    {{--    Error Display--}}
                        @if($errors->any())
                            <div class="alert alert-danger ">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    {{--    Error Display ends--}}
            
                    <div class="panel panel-primary theme-border">
                        <div class="panel-heading theme-bg">
                            <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add Metrics.</h4>
                        </div>
                        
                        <div class="modal-body" style="padding: 5px;">
                            <div class="panel-body">
                                
                                @if(Session::has('message'))
                                    <div class="alert alert-success" style="padding: 7px 15px;">
                                        {{ Session::get('message') }}
                                        {{ Session::put('message','') }}
                                    </div>
                                @endif
                
                
                                {!! Form::open(array('route'=>'root.shop.metricscatmap.create','id'=>'searchingForm','class'=>'form-horizontal','method'=>'GET')) !!}
                                    <div class="col-md-12 form-group">
                                        <label class="col-sm-2 control-label">Category Name<span class="red">*</span></label>
                                        <div class="col-sm-4">
                                            @if($category_id)
                                                <select class="form-control" disabled name="category">
                                            @else
                                                <select class="form-control js-example-basic-multiple" name="category" id="restaurant" onChange="hideErrorMsg('restauranterr')">
                                            @endif
                                            <option value="">Select</option>
                                            
                                            @foreach($category_names as $category_name)
                                            
                                            @if($category_name->id==$category_id)
                                            <option value="{{$category_name->id}}" selected >{{ucfirst($category_name->name)}}</option>
                                            @else
                                            <option value="{{$category_name->id}}" >{{ucfirst($category_name->name)}} </option>
                                            @endif
                                            @endforeach
                                            </select>
                                            <span id="restauranterr" class="text-danger"></span>
                                        </div>
                                        
                                        @if(!$category_id)
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" type="button" onclick="validate_rest()">Get Metrics</button>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="clearfix"></div>
                                {!! Form::close() !!}
                                
                    @if($category_id)
                        <div>
                            {!! Form::open(array('route'=>'root.shop.metricscatmap.store', 'id'=> 'serviceform','class'=>'form-horizontal')) !!}
                            <hr>
                            <div class="col-md-12 form-group">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-9">
                                    @foreach($metrics as $metric)
                                        <label class="checkbox checkbox-inline">
                                            @if(in_array($metric->id,$root_cat_maps))
                                                <input type="checkbox" name="metrics[]" value="{{$metric->id}}" checked >
                                            @else
                                                <input type="checkbox" name="metrics[]" value="{{$metric->id}}" >
                                            @endif
                                            <span>{{$metric->name}} </span>
                                        </label>
                                    @endforeach
                                    {!! Form :: hidden('categoryId',$category_id,['id'=>'categoryId'])  !!}
                                </div>
                            </div>
                            <span id="serviceerr" class="text-danger" ></span>
                            <div class="col-md-12 form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-4">
                                    <button class="btn btn-primary" type="button" onclick="validate_service()">Submit</button>
                                    <a class="btn btn-primary" href="{{url('root/shop/metricscatmap/')}}">Cancel</a>
                                </div>
                            </div>
                        {!! Form::close() !!}
                        </div>
                    @endif
                                                                <!-- </form> -->
                </div>
            </div>  
        </div>
            <!-- /.box-body -->
    </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')
<script>
    function hideErrorMsg(id){
        $('#'+id).attr('style', 'display:none');
    }
    function validate_rest()
    {
        restaurant = $('#restaurant').val();
        if(restaurant=='')
        {
            $("#restaurant").focus();
            $("#restauranterr").html('Please select a Category');
            $("#restauranterr").show();
            return false;
        }
        document.forms["searchingForm"].submit();
    }
    function validate_service()
    {
        var service = document.getElementsByName('metrics[]');
        var haschecked= false;
        for(var i=0; i < service.length; i++)
        {
            if(service[i].checked)
            {
                haschecked = true;
                document.forms["serviceform"].submit();
            }
        }
        if(haschecked == false)
        {
            $("#serviceerr").html('Please check atleast one metrics');
            $("#serviceerr").show();
        }
    }
</script>
@endsection

