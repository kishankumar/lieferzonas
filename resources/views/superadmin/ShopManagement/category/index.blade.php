@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shop Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.shop.categories.index')}}"><i class="fa fa-dashboard"></i> Shop Management</a></li>
        <li class="active">Shop Category</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Shop Category With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/shop/categories/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/shop/categories/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default"  data-toggle="modal" onclick="add_types(0)" data-original-title><span>Add</span></a>
                    <!--<a class="btn btn-default DTTT_button_csv"  onclick="add_types(1)" data-toggle="modal" data-original-title><span>Edit</span></a>-->
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_contents()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_contents()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
             </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>

            <div class="clearfix"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
            <div id="delerrors" style="color: red"></div>
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>
                            <a href="{{url('root/shop/categories/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Category Name
                            <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/shop/categories/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                            Description
                            <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                @if(count($categories))
                    @foreach($categories as $info)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('categories', $info->id, null, ['class' => 'categories checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ ucfirst($info->name)  }}</td>
                    <td>{{ (strlen($info->description) > 50) ? substr($info->description,0,50).'...' : $info->description  }}</td>
                    <td>
                        @if($info->status == 1) 
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-success" href="javascript:void(0)" onclick="add_types({{$info->id}})">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $categories->appends(Request::except('page'))->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
     
	 <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function add_shop_categories(){
        if( $("#add-category-form").valid())
        {
            $.ajax({
                url: '{{ url('root/shop/categories') }}',  //working
                type: "post",
                dataType:"json",
                data: $('#add-category-form').serialize(),
                success: function(res){
                    if(res.success==1)
                    {
                        document.getElementById("add-category-form").reset();
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#errors').html(errorsHtml);
                    }
                }
            });
        }
    }
    
//    function trimmer(id){
//        $('#'+id).val($('#'+id).val().trim());
//    }
    
    function add_types(edit=0){
        $('#NameErr,#statusErr,#descriptionErr').html('');
        $('#errors,#delerrors').html('');
        $('.error').html('');
        if(edit){
            $('.new_category_title').html('<span class="glyphicon glyphicon-plus"></span> Edit Shop Category.');
            $('#add_shop_category').text('Update');
            $.ajax({
                url: '{{route("root.shop.categories.edit")}}',
                type: "GET",
                dataType:"json",
                data: {'checkedCategories':edit, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        $('#show_status').show();
                        $('#name').val(res.name);
                        $('#status').val(res.status);
                        $('#description').val(res.description);
                        $('input[name=category_id]').val(res.id);
                        //$("#add-form1").modal("show");
                        showModal('Shop-form');
                    }
                    else
                    {
                        document.getElementById("add-category-form").reset();
                    }
                }
            });
        }else{
            document.getElementById("add-category-form").reset();
            $('.new_category_title').html('<span class="glyphicon glyphicon-plus"></span> Add Shop Category.');
            //$('#show_status').hide();
            $('#add_shop_category').text('Add');
            $('input[name=category_id]').val(0);
            showModal('Shop-form');
            //$("#add-form1").modal("show");
        }
    }
    
    function delete_contents(){
        var len = $(".categories:checked").length;
        if(len==0){
            alert('Please select atleast one category');
            return false;
        }
        var base_url1=base_url+'shop/categories/delete';
        var r = confirm("Are you sure to delete category?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="categories"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedCategories':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#delerrors').html(errorsHtml);
                    }
                }
            });
        } else {
            return false;
        }
    }
    
    $(function(){
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-zA-ZäÄöÖüÜß«» -]+$/i.test(value);
        }, "Name must contain only letters.");
        
        $("#add-category-form").validate({
            rules:
            {
                name:
                {
                    required : true,
                    minlength:3,
                    maxlength:100,
                    loginRegex:true
                },
                description:
                {
                    required : true,
                    minlength:6
                }
            },
            messages: {
                "name": {
                    loginRegex: "Name format not valid"
                }
            }
        });
        
        $(".modalClose").click(function(){
            $('#NameErr,#statusErr,#descriptionErr').html('');
            $('#errors,#delerrors').html('');
            $('.error').html('');
            document.getElementById("add-category-form").reset();
        });
    });
	
	/*added by Rajlakshmi(01-06-16)*/					

	function download_contents() 
	{
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
       };
	    
		var len = $(".categories:checked").length;
        if(len==0){
            alert('Please select atleast one category');
            return false;
        }
        var base_url1=base_url+'shop/categories/download';
        var r = confirm("Are you sure to download category?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="categories"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedCategories':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Shopcategory List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
				doc.save('shopcategory_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
		
	}
   
         /*added by Rajlakshmi(01-06-16)*/
</script>

@endsection

@include('superadmin.ShopManagement.category.add_category')





