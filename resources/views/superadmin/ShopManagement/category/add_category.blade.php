
<div class="modal fade" id="Shop-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close modalClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="panel-title new_category_title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add Shop Category .</h4>
            </div>
            
            {!! Form::open(['route'=>'root.shop.categories.store','id'=>'add-category-form'])!!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
                <div class="modal-body" style="padding: 5px;">
                    
                    <div class="panel-body">
                        <div class="errors" id="errors" style="color: red"></div>
                        
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::label('Shop Category Name') !!}<span class="red">*</span>
                                    {!! Form::text('name','',["id"=>'name',"class"=>"form-control",'onblur' => 'trimmer("name")']) !!}
                                    <span id="NameErr" style="color:red"></span>
                                </div>
                            </div>
                            {!! Form::hidden('category_id','',['id'=>'category_id']) !!}
                            <div class="col-md-4" id="show_status">
                                <div class="form-group">
                                    {!! Form::label('Status') !!}
                                    {!! Form::select('status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ), 1, ['id' => 'status','class' => 'form-control']) !!}
                                    <span id="statusErr" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                           {!! Form::label('Description') !!} <span id="descriptionErr" style="color:red"></span><span class="red">*</span>
                            {!! Form :: textarea('description','',['style'=>'height:100px;','id'=>'description','placeholder'=>'Enter Description ','class'=>'form-control'])  !!}
                        </div>
                        
                        <div class="form-group margin-top">
                            {!! Form::button('Add',["id"=>'add_shop_category',"class"=>"btn add_category btn-primary",'onclick'=>'add_shop_categories()']) !!}
                             <hr>
							<p><span class="red">*</span> - Required Fields.</p>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
				
            </div>
        </div>
    </div>