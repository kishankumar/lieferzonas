@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shop Items
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.shop.items.index')}}"><i class="fa fa-dashboard"></i> Shop Management</a></li>
        <li class="active">Shop Items</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Shop Items With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/shop/items/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/shop/items/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
             <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a href="{{URL :: asset('root/shop/items/create')}}" class="btn btn-default"><span>Add</span></a>
                    
                    <!--<a class="btn btn-default"  data-toggle="modal" onclick="add_types()" data-original-title><span>Add</span></a>-->
                    <!--<a class="btn btn-default DTTT_button_csv"  onclick="add_types(1)" data-toggle="modal" data-original-title><span>Edit</span></a>-->
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_contents()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_contents()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
             </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            <div class="clearfix" style=""></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                
            <div id="delerrors" style="color: red"></div>
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>

                        <th>
                            <a href="{{url('root/shop/items/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Item Name
                            <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/shop/items/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='inumber' ? 'inumber-desc' :'inumber';?>">
                            Item Number
                            <?php if($type=='inumber'){ echo $class; } elseif($type=='inumber-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/shop/items/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                            Description
                            <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/shop/items/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='price' ? 'price-desc' :'price';?>">
                            Price
                            <?php if($type=='price'){ echo $class; } elseif($type=='price-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/shop/items/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='fprice' ? 'fprice-desc' :'fprice';?>">
                            Front Price
                            <?php if($type=='fprice'){ echo $class; } elseif($type=='fprice-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>

                        <th>Item Category</th>
                        <th>Item Quantity</th>
                        <th>Item Metrics</th>
                        <!--<th>Status</th>-->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                @if(count($ItemInfo))
                    @foreach($ItemInfo as $info)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('items', $info->id, null, ['class' => 'items checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ ucfirst($info->name)  }}</td>
                    <td>{{ ($info->item_number)  }}</td>
                    <td>{{ (strlen($info->description) > 50) ? substr($info->description,0,50).'...' : $info->description  }}</td>
                    <td>&euro; {{ number_format($info->price,2)  }}</td>
                    <td>&euro; {{ number_format($info->front_price,2)  }}</td>
                    <td> {{ ucfirst($info->shop_category->name)  }}</td>
                    <td>{{ number_format($info->quantity,2)  }}</td>
                    <td>{{ ucfirst($info->metric_name->name)  }}</td>
<!--                    <td>
                        @if($info->status == 1)
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>-->
                    <td>
                        <a class="btn btn-success"  href="{{url('root/shop/items/'.$info->id.'/edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn" href="{{url('root/item/detail/'.$info->id.'/'.$info->shop_category_id.'/search')}}">Item Details
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="9" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $ItemInfo->appends(Request::except('page'))->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
    </div>

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

	<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    function delete_contents(){
        var len = $(".items:checked").length;
        if(len==0){
            alert('Please select atleast one item');
            return false;
        }
        var base_url1=base_url+'shop/items/delete';
        var r = confirm("Are you sure to delete items?");
        if (r == true) {
            var selectedItems = new Array();
            $('input:checkbox[name="items"]:checked').each(function(){
                selectedItems.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedItems':selectedItems, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#delerrors').html(errorsHtml);
                    }
                }
            });
        } else {
            return false;
        }
    }
    
    function add_shop_categories(){
        if( $("#add-category-form").valid())
        {
            $.ajax({
                url: '{{ url('root/shop/categories') }}',  //working
                type: "post",
                dataType:"json",
                data: $('#add-category-form').serialize(),
                success: function(res){
                    if(res.success==1)
                    {
                        document.getElementById("add-category-form").reset();
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#errors').html(errorsHtml);
                    }
                }
            });
        }
    }
    
    $(function(){
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
        }, "Name must contain only letters.");
        
        $("#add-category-form").validate({
            rules:
            {
                name:
                {
                    required : true,
                    minlength:3,
                    maxlength:100,
                    loginRegex:true
                },
                description:
                {
                    required : true,
                    minlength:6
                }
            },
            messages: {
                "name": {
                    loginRegex: "Name format not valid"
                }
            }
        });
    });
	
	/*added by Rajlakshmi(01-06-16)*/					

	function download_contents() 
	{
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
       };
	    
		var len = $(".items:checked").length;
        if(len==0){
            alert('Please select atleast one item');
            return false;
        }
        var base_url1=base_url+'shop/items/download';
        var r = confirm("Are you sure to download  items?");
        if (r == true) {
            var selectedItems = new Array();
            $('input:checkbox[name="items"]:checked').each(function(){
                selectedItems.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedItems':selectedItems, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Items List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });

				doc.save('items_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
		
	}
    
         /*added by Rajlakshmi(01-06-16)*/
</script>
@endsection





