@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Item Details
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
           <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.shop.items.index')}}"><i class="fa fa-dashboard"></i> Shop Management</a></li>
            <li class="active">Item Details</li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Item Details With Full Features</h3>
            </div>
              
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/item/detail/$item_id/$category_id/search",'class'=>'form-inline','method'=>'GET')) !!}
                    
                    <div class="form-group">
                        {!! Form::label('From') !!}
                        <div class="input-group date" id="datetimepicker1">
                            {!! Form::text('from',$from_date,["id"=>'from',"class"=>"form-control","placeholder"=>"Item-From"]) !!}
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('To') !!}
                        <div class="input-group date" id="datetimepicker2">
                            {!! Form::text('to',$to_date,["id"=>'to',"class"=>"form-control","placeholder"=>"Item-To"]) !!}
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-primary" title="">Search</button>
                    
                    <a class="btn btn-primary" href="{{url('root/item/detail/'.$item_id.'/'.$category_id.'/search')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    {{ Session::put('message','') }}
                @endif
                
            <div id="delerrors" style="color: red"></div>
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Sr.</th>
                        <th>Item Category</th>
                        <th>Item Name</th>
                        <th>Item Quantity</th>
                        <th>Item Metrics</th>
                        <th>Item Status</th>
                        <th>Price</th>
                        <th>Created Date</th>
                    </tr>
                </thead>
                <tbody>
                <?php $i=$pages['from']; ?>
                @if(count($InventoryInfo))
                    @foreach($InventoryInfo as $info)
                <tr>
                    <td><?php echo $i; ?></td>
                    <td> {{ ucfirst($info->shop_category->name)  }}</td>
                    <td>{{ ucfirst($info->item_name->name)  }}</td>
                    <td>{{ number_format($info->quantity,2)  }}</td>
                    <td>{{ ucfirst($info->metric_name->name)  }}</td>
                    <td>
                        @if($info->action == 'P')
                           {{ 'Items Added' }}
                        @else
                            {{ 'Items Used' }}
                        @endif
                    </td>
                    <td>&euro; {{ number_format($info->price,2)  }}</td>
                    <td>{{ date('j M Y',strtotime($info->created_at)) }}</td>
                </tr>
                    <?php $i++; ?>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $InventoryInfo->appends(Request::except('page'))->render() !!}
        </div>
        <!-- /.box-body -->
        </div>
        <!-- quick email widget -->
    </section>
    </div>
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @endsection

@section('script')
<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
</script>
@endsection





