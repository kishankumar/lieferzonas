@extends('layouts.superadminbody')

@section('title')
@endsection


@section('body')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shop Items
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.shop.items.index')}}"><i class="fa fa-dashboard"></i> Shop Management</a></li>
        <li class="active">Edit Shop Items</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Shop Items With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            @if(Session::has('message'))
                <div class="alert alert-success" style="padding: 7px 15px;">
                    {{ Session::get('message') }}
                    {{ Session::put('message','') }}
                </div>
            @endif
            
            {{--    Error Display--}}
                @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{--    Error Display ends--}}

            
            <div class="panel panel-primary theme-border">
                <div class="panel-heading theme-bg">
                    <h4 id="contactLabel" class="panel-title"><span class="glyphicon glyphicon-plus"></span> Edit Shop Item</h4>
                </div>
                    
                <div style="padding: 5px;" class="modal-body">
                    <div class="panel-body">
                        
                    {!! Form::model($item_detail,['route'=>['root.shop.items.update',$item_detail->id],'id'=>'edit-item','method'=>'patch','class'=>'form','files'=>true]) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                {!! Form::label('', 'Category Name', array('class' => '"control-label')) !!}<span class="red">*</span>
                                <select class="form-control js-example-basic-multiple" name="category_id" onchange="getData(this.value,'shop_metric_categories','metrics','metric-category','metric_id','name','shop_category_id','id')">
                                    <option value="">Select</option>
                                    @foreach($category_names as $category_name)
                                        <option @if($item_detail->shop_category_id==$category_name->id) selected="selected" @endif value="{{$category_name->id}}">{{ucfirst($category_name->name)}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            
                            {!! Form::hidden('item_id',$item_detail->id,['id'=>'item_id']) !!}
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                {!! Form::label('', 'Metric Name', array('class' => 'control-label')) !!}<span class="red">*</span>
                                    <select class="form-control" name="metric_id" id="metric-category">
                                        
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('', 'Name', array('class' => '"control-label')) !!}<span class="red">*</span>
                                    {!! Form :: text('item_name',$item_detail->name,['placeholder'=>'Enter Item Name','class'=>'form-control','id'=>'item_name','onblur' => 'trimmer("item_name")'])  !!}
                                </div>
                            </div>
                            
                            <div class="col-md-6" id="show_status" style="">	
                                <div class="form-group">
                                    {!! Form::label('Status') !!}
                                    {!! Form::select('status', 
                                        array(
                                        '1'=>'Active',
                                        '0'=>'Deactive',
                                        ), $item_detail->status, ['id' => 'status','class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('', 'Item Number', array('class' => '"control-label')) !!}<span class="red">*</span>
                                    {!! Form :: text('item_number',$item_detail->item_number,['placeholder'=>'Enter Item Number','class'=>'form-control','id'=>'item_number','onblur' => 'trimmer("item_number")'])  !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('', 'Price', array('class' => '"control-label')) !!}<span class="red">*</span>
                                    {!! Form :: text('price',$item_detail->price,['placeholder'=>'Enter Price','class'=>'form-control','id'=>'price','onblur' => 'trimmer("price")'])  !!}
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('', 'Front Price', array('class' => '"control-label')) !!}<span class="red">*</span>
                                    {!! Form :: text('front_price',$item_detail->front_price,['placeholder'=>'Enter Front Price','class'=>'form-control','id'=>'front_price'])  !!}
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('', 'Upload Item Image', array('class' => 'control-label')) !!}<span class="red">*</span>
                                    {!! Form :: file('image',['class'=>'form-control','id'=>'image','style'=>'border: none; font-size: 12px; padding: 0px;'])  !!} (jpg|jpeg|png|gif)
                                    @if ($item_detail->item_image)
                                        {!! Html::image(asset('public/uploads/superadmin/shop/items/'.$item_detail->item_image), '', array('class' => 'img-responsive','style'=>'max-width:25%;')) !!}
                                    @endif
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="">Description</label><span class="red">*</span>
                                    {!! Form :: textarea('description',$item_detail->description,['placeholder'=>'Enter Description','class'=>'form-control','id'=>'description','onblur' => 'trimmer("description")'])  !!}
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group margin-top">
                            <hr>
                            <input type="submit" value="Update" name="submit" class="btn btn-primary pull-right">
                            <a style="margin-right:10px;" class="btn btn-primary pull-right" href="{{url('root/shop/items/')}}">Cancel</a>
                            <p><span class="red">*</span> - Required Fields.</p>
                        </div>
                    {!! Form :: close() !!}
								
                    </div>
                </div>  
            </div>
            
            <!-- /.box-body -->
          </div>
         
          <!-- quick email widget -->
          

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
@endsection

@section('script')

<script type="text/javascript">
    
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    $(document).ready(function(){
        var cid = '<?=$item_detail->shop_category_id;?>';
        var selectedMetric = '<?=$item_detail->metric_id;?>';
        if(cid){
            getData(cid,'shop_metric_categories','metrics','metric-category','metric_id','name','shop_category_id','id',selectedMetric);
        }
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
        }, "Item name must contain only letters.");
        $.validator.addMethod('greater_than_zero', function(value, element, param) {
            return isNaN(value) || this.optional(element) || value > 0;
        }, 'Price must be greater than 0');
        
        $.validator.addMethod('greater_than_price', function(value, element, param) {
            //return this.optional(element) || value > $(param).val();
            var $otherElement = $(param);
            return parseFloat(value, 10) > parseFloat($otherElement.val(), 10);
        }, 'Front price must be greater than price');
            
        $.validator.addMethod("AlphaNumericData", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9_ ]+$/i.test(value);
        }, "Letters, numbers, spaces or underscores only please");
        $.validator.addMethod( "extension", function( value, element, param ) {
            param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
            return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
        }, $.validator.format( "Please enter a value with a valid extension." ) );
        
        $("#edit-item").validate({
            rules: {
                category_id: {
                    required: true
                },
                metric_id: {
                    required: true
                },
                item_name: {
                    required : true,
                    minlength:3,
                    maxlength:100
                },
                item_number: {
                    required : true,
                    minlength:5,
                    maxlength:100
                },
                price: {
                    required: true,
                    number: true,
                    greater_than_zero: true
                },
                front_price: {
                    required: true,
                    number: true,
                    greater_than_price:'#price'
                },
                image:{
                    @if($item_detail->item_image == '')
                        required:true,
                        extension: "jpg|jpeg|png|gif"
                    @elseif($item_detail->item_image != '')
                        extension: "jpg|jpeg|png|gif"
                    @endif
                },
                description:
                {
                    required : true,
                    minlength:6
                }
            },
            messages: {
                category_id: "Please select category",
                metric_id: "Please enter metric",
                item_name: {
                    required: "Please enter item name",
                    loginRegex: "Item name must contain only letters"
                },
                item_number: {
                    required: "Please enter item number"
                },
                price: {
                    required: "Please enter price",
                    number: "Price must contain only numbers"
                },
                front_price: {
                    required: "Please enter front price",
                    number: "front price must contain only numbers",
                    greater_than_price:"Front price must be greater than price"
                },
                image: {
                    required: "Please upload image",
                    extension: "Please upload valid image file"
                },
            },
        });
    });
    </script>
    
    @endsection



