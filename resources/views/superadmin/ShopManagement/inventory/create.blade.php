@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Shop Inventory
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.shop.inventory.index')}}"><i class="fa fa-dashboard"></i> Shop Management</a></li>
            <li class="active">Shop Inventory</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- Main row -->
        <div class="row">
        <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Shop Inventory With Full Features</h3>
                    </div>
                    <!-- /.box-header -->
            
                    <div class="box-body">
                        
                        <div id="delerrors" style="color: red"></div>
                        
                        {{--    Error Display--}}
                @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{--    Error Display ends--}}
                        
                        {!! Form::open(array('route'=>'root.shop.inventory.store','id'=>'inventory-form')) !!}
                            
                            <div class="form-group col-sm-4">
                                {!! Form::label('', 'Category Name', array('class' => 'control-label')) !!}
                                <select class="form-control js-example-basic-multiple" name="category_id" id="category_id" onchange="get_cat_items(this.value)">
                                    <option value="">Select</option>
                                    @foreach($category_names as $category_name)
                                        <option value="{{$category_name->id}}">{{ucfirst($category_name->name)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            
                            <div class="form-group" id="itemslisting" style="display:none;">
<!--                                <div class="col-sm-3 col-lg-2">
                                    <label class="checkbox"><input type="checkbox" value="1"><span>typesetting </span></label>
                                </div>-->
                            </div>
						
                            
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Categories</th>
                                    <th>Quantity</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody class="check_table">
                                
                            </tbody>
                        </table>
                        <div class="form-group margin-top" id="add_item" style="display: none">
                            {!! Form::button('Add',["id"=>'add_items',"class"=>"btn add_category btn-primary",'onclick'=>'add_all_items()']) !!}
                            <hr>
                        </div>
                        
                        {!! Form :: close() !!}
                    </div>
                    <!-- /.box-body -->
                    <!-- /.box-body -->
                </div>
            <!-- quick email widget -->
            </section>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    function get_cat_items(catId=0){
        selectedItems='';
        var rowCount = $('.check_table tr').length;
        if(rowCount>0){
            //var selectedItems = new Array();
            $('.check_table tr').each(function(){
                var str=$(this).attr('class');
                var res = str.split("-");
                var itemId = res[1];
                selectedItems+=itemId+',';
            });
            selectedItems = selectedItems.slice(0, -1);
        }
        if(catId=='' || catId==0){
            $('#itemslisting').css({'display':'none'});
        }
        else{
            $('#itemslisting').css({'display':'block'});
            $.ajax({
                url: '{{ url('root/shop/inventory/getCatItems') }}',  //working
                type: "post",
                dataType:"json",
                data: {'catId':catId,'selectedItems':selectedItems, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    $('#itemslisting').html(res.html);
                }
            });
        }
    }
    
    function remove_row(checked){
        $('.check_table .row-'+checked).remove();
        $('#itemslisting input[value='+checked+']').removeAttr('checked');
    }
    
    function add_all_items(){
        var rowCount = $('.check_table tr').length;
        if(rowCount>0){
            $('#delerrors').html('');
        }else{
            $('#delerrors').html('Please select atleast one item');
            return false;
        }
        var count=0;
        $(".quantity").each(function() {
//            var quanV=/^[0-9]+([,.][0-9]{1,2})?$/;
            var quanV=/^[0-9]+$/;
            var validQuan=quanV.test($(this).val());
            if($(this).val()=='' || $(this).val()< 1){
                $(this).next().html("Quantity should be equal to or more than 1");
                count=1;
            }
            else if(validQuan==false)
            {
                $(this).next().html("Please enter valid quantity");
                count=1;
            }
            else
            {
                $(this).next().html("");
            }
        });
        if(count==1){
            return false;
        }
        $('#inventory-form').submit();
    }
    
    $(function(){
        $('#itemslisting').unbind('change').on('change','.nitems',function(){
            var checked= $(this).val();
            var catText=$("#category_id option:selected").text();
            var itemText=$(this).next().text();
            if($(this).is(":checked")==true){
                $(".check_table").append('<tr class='+'row-'+checked+'><td>'+itemText+'</td><td>'+catText+'</td><td><input class="quantity" name="quantity['+checked+']" type="text"><span id="Err'+checked+'" style="color:red"></span></td><td><a href="javascript:void(0)" class="text-danger" onclick="remove_row('+checked+');"><i class="fa fa-close"></i> Delete</a></td></tr>');
            }
            else{
                $('.check_table .row-'+checked).remove();
            }
            var rowCount = $('.check_table tr').length;
            var len = $(".nitems:checked").length;
            if(len>0 || rowCount>0){
                $('#add_item').show();
            }else{
                $('#add_item').hide();
            }
        });
    });
</script>
@endsection





