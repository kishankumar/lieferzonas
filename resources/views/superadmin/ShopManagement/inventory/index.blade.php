@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shop Inventory
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
       <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.shop.inventory.index')}}"><i class="fa fa-dashboard"></i> Shop Management</a></li>
        <li class="active">Shop Inventory</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Shop Inventory With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/shop/inventory",'class'=>'form-inline','method'=>'GET')) !!}
                    
                    <div class="form-group">
                        {!! Form::label('From') !!}
                        <div class="input-group date" id="datetimepicker1">
                            {!! Form::text('from',$from_date,["id"=>'from',"class"=>"form-control","placeholder"=>"Item-From"]) !!}
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('To') !!}
                        <div class="input-group date" id="datetimepicker2">
                            {!! Form::text('to',$to_date,["id"=>'to',"class"=>"form-control","placeholder"=>"Item-To"]) !!}
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-primary" title="">Search</button>
                    <a class="btn btn-primary" href="{{url('root/shop/inventory/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a href="{{URL :: asset('root/shop/inventory/create')}}" class="btn btn-default"><span>Add</span></a>
                    
                    <!--<a class="btn btn-default"  data-toggle="modal" onclick="add_types()" data-original-title><span>Add</span></a>-->
                    <!--<a class="btn btn-default DTTT_button_csv"  onclick="add_types(1)" data-toggle="modal" data-original-title><span>Edit</span></a>-->
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_contents()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_contents()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
             </div>
            
            <div class="clearfix"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                
            <div id="delerrors" style="color: red"></div>
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>Item Category</th>
                        <th>Item Name</th>
                        <th>Item Quantity</th>
                        <th>Item Metrics</th>
                        <th>Price</th>
                        <th>Created Date</th>
                        <th>Item Status</th>
                        <!--<th>Status</th>-->
                        <!--<th>Action</th>-->
                    </tr>
                </thead>
                <tbody>
                    
                @if(count($InventoryInfo))
                    @foreach($InventoryInfo as $info)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('items', $info->id, null, ['class' => 'items checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>@if(count($info->shop_category))
                            {{ ucfirst($info->shop_category->name)  }}
                        @else
                            {{ 'None'  }}
                        @endif
                    </td>
                    <td>@if(count($info->item_name))
                            {{ ucfirst($info->item_name->name)  }}
                        @else
                            {{ 'None'  }}
                        @endif
                    </td>
                    <td>{{ number_format($info->quantity,2)  }}</td>
                    <td>@if(count($info->metric_name))
                            {{ ucfirst($info->metric_name->name)  }}
                        @else
                            {{ 'None'  }}
                        @endif
                    </td>
                    <td>&euro; {{ number_format($info->price,2)  }}</td>
                    <td>{{ date('j M Y',strtotime($info->created_at)) }}</td>
                    <td>
                        @if($info->action == 'P')
                           {{ 'Items Added' }}
                        @else
                            {{ 'Items Used' }}
                        @endif
                    </td>
<!--                    <td>
                        @if($info->status == 1) 
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>-->
<!--                    <td>
                        <a class="btn btn-success"  href="{{url('root/shop/inventory/'.$info->id.'/edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>-->
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $InventoryInfo->appends(Request::except('page'))->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
    </div>

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
   
   <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    function delete_contents(){
        var len = $(".items:checked").length;
        if(len==0){
            alert('Please select atleast one item');
            return false;
        }
        var base_url1=base_url+'shop/inventory/delete';
        var r = confirm("Are you sure to delete items?");
        if (r == true) {
            var selectedItems = new Array();
            $('input:checkbox[name="items"]:checked').each(function(){
                selectedItems.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedItems':selectedItems, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#delerrors').html(errorsHtml);
                    }
                }
            });
        } else {
            return false;
        }
    }
    
    function add_shop_categories(){
        if( $("#add-category-form").valid())
        {
            $.ajax({
                url: '{{ url('root/shop/categories') }}',  //working
                type: "post",
                dataType:"json",
                data: $('#add-category-form').serialize(),
                success: function(res){
                    if(res.success==1)
                    {
                        document.getElementById("add-category-form").reset();
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#errors').html(errorsHtml);
                    }
                }
            });
        }
    }
    
    $(function(){
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
        }, "Name must contain only letters.");
        
        $("#add-category-form").validate({
            rules:
            {
                name:
                {
                    required : true,
                    minlength:3,
                    maxlength:100,
                    loginRegex:true
                },
                description:
                {
                    required : true,
                    minlength:6
                }
            },
            messages: {
                "name": {
                    loginRegex: "Name format not valid"
                }
            }
        });
    });
	
	/*added by Rajlakshmi(01-06-16)*/					

	function download_contents() 
	{
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
       };
	    
		var len = $(".items:checked").length;
        if(len==0){
            alert('Please select atleast one item');
            return false;
        }
        var base_url1=base_url+'shop/inventory/download';
        var r = confirm("Are you sure to download  items?");
        if (r == true) {
            var selectedItems = new Array();
            $('input:checkbox[name="items"]:checked').each(function(){
                selectedItems.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedItems':selectedItems, "_token":"{{ csrf_token() }}"},
               success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);

				doc.save('inventory_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
		
	}
    
         /*added by Rajlakshmi(01-06-16)*/
</script>
@endsection





