@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Restaurant Suggestion Notification
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restsuggestion_notification.index')}}"><i class="fa fa-dashboard"></i>Notification</a></li>
        <li class="active">Restaurant Suggestion Notification</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Restaurant Suggestion Notification Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/restsuggestion_notification/",'class'=>'form-inline','method'=>'GET')) !!}
                   
                    
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                   
                </div>
             </div>
            
            <?php 
                $class='<i class="fa fa-caret-down"></i>';
                $class1='<i class="fa fa-caret-up"></i>';
            ?>
            <div class="clearfix" style="margin: 20px 0;"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif

            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <!--<th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>-->
                        <th>Name</th>
                        <th>Email</th>
						<th>Mobile</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                      
                @if(count($restsuggestion_notiInfo))
                @foreach($restsuggestion_notiInfo as $restsuggestion_notiInfos)
			    <?php if($restsuggestion_notiInfos['is_read']=='1') 
						{ 
					      $class="";
					    } 
					  else
					    {
						  $class="text-success";
					    }
			    ?>
                <tr class="<?=$class?>">
                   <!-- <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('notification',$restsuggestion_notiInfos['id'], null, ['class' => 'notification checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>-->
                    <td>{{ $restsuggestion_notiInfos['fname'].' '.$restsuggestion_notiInfos['lname']  }}</td>
                    <td>{{ $restsuggestion_notiInfos['email']  }}</td>
                    <td>{{ $restsuggestion_notiInfos['mobile']  }}</td>
                    
                    <td>
                        <a class="btn btn-success" href="#" onclick="viewdetail(<?=$restsuggestion_notiInfos['id']?>);">

                        View detail
                    </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $restsuggestion_notiInfo->render() !!}
        </div>

            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
    @endsection

@section('script')

<script>
 function viewdetail(id)
 {
	    var base_host = window.location.href;
		base_host = base_host.substring(0, base_host.lastIndexOf("/"));
        var base_url = base_host+'/restsuggestion_notification/suggestiondetail/'+id;
		
        var base_url1 = base_host+'/restsuggestion_notification/changereadstatus';
		$.ajax({
					url: '{{url('changereadstatus')}}',
					type: "post",
					dataType:"json",
					data: {'id':id,"_token":"{{ csrf_token() }}"},
					error:function (error)
					{
						location.reload(); 
						window.location.href = base_url;
						
					},
					success: function(res){
					   
						if(res.success==1)
						{
							location.reload(); 
							window.location.href = base_url;
							
						}
					   
					}
				});
 }
</script>

@endsection







