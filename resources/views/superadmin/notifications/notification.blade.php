@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Restaurant Notification
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
       <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restnotification.index')}}"><i class="fa fa-dashboard"></i>Notification</a></li>
        <li class="active">Restaurant Notification</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Restaurant Notification Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/restnotification/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/restnotification/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default"  data-toggle="modal"  data-original-title href="{{ url('root/restnotification/create') }}"><span>Add</span></a>
                 
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_restnotification()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_restnotification()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
             </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            <div class="clearfix" style="margin: 20px 0;"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
						<div class="alert alert-success" style="padding: 7px 15px;">
						  {{ Session::get('message') }}
						  {{ Session::put('message','') }}
						</div>
					@endif

            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>
                            <a href="{{url('root/restnotification/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='title' ? 'title-desc' :'title';?>">
                            Title
                            <?php if($type=='title'){ echo $class; } elseif($type=='title-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/restnotification/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='comment' ? 'comment-desc' :'comment';?>">
                            Comment
                            <?php if($type=='comment'){ echo $class; } elseif($type=='comment-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Is Restaurant specific</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                      
                @if(count($notiInfo))
                    @foreach($notiInfo as $notiInfos)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('notification', $notiInfos['id'], null, ['class' => 'notification checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ $notiInfos['title']  }}</td>
                    <td>{{ (strlen($notiInfos['comment']) > 100) ? substr($notiInfos['comment'],0,100).'...' : $notiInfos['comment']  }}</td>
                    <td>@if($notiInfos['send_type']==2) {{ 'Yes'  }} @else {{ 'No'  }} @endif</td>
                    <td> @if($notiInfos['status']==1) <i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($notiInfos['status']==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>
                    <td>
                        <a class="btn btn-success" href="{{url('root/restnotification/'.$notiInfos['id'].'/edit')}}">

                        <i class="fa fa-edit"></i>
                    </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $notiInfo->appends(Request::except('page'))->render() !!}
        </div>

            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/';
    
    function delete_restnotification()
    {
        var len = $(".notification:checked").length;
        if(len==0){
            alert('Please select atleast one notification');
            return false;
        }
        var base_url1=base_url+'root/restnotification/delete';
        var r = confirm("Are you sure to delete notifications?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="notification"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedNotification':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('notifications not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }

    /*added by Rajlakshmi(31-05-16)*/
	function download_restnotification()
     {
        var len = $(".notification:checked").length;
        if(len==0){
            alert('Please select atleast one notification');
            return false;
        }
        var base_url1=base_url+'root/restnotification/delete';
        var r = confirm("Are you sure to download notifications?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="notification"]:checked').each(function(){
                selectedPages.push($(this).val());
            });

            $.ajax({
                url: 'restnotification/download',
                type: "post",
                dataType:"json",
                data: {'selectedNotification':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Notification List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
					    doc.save("notification_list.pdf");
				}
            });
          
		}		
		else {
            return false;
        }
   
	}
	
		 
		 /*added by Rajlakshmi(31-05-16)*/
</script>

@endsection







