@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Restaurant Suggestion Notification Detail
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restsuggestion_notification.index')}}"><i class="fa fa-dashboard"></i>Notification</a></li>
        <li class="active">Restaurant Suggestion Notification Detail</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Restaurant Suggestion Notification Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/restsuggestion_notification/",'class'=>'form-inline','method'=>'GET')) !!}
                   
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    
                   
                </div>
             </div>
            
           
            <div class="clearfix" style="margin: 20px 0;"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif

				    <div class="form-group row">
						<div class="col-sm-2"><strong>Delivery Service : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['delivery_service'] }}</div>
					</div>

                    <div class="form-group row">
						<div class="col-sm-2"><strong>First Name : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['fname'] }}</div>
					</div>
					 <div class="form-group row">
						<div class="col-sm-2"><strong>Last Name : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['lname'] }}</div>
					</div>
					<div class="form-group row">		  
						<div class="col-sm-2"><strong>Email : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['email'] }}</div>
				  	</div>
					<div class="form-group row">
						<div class="col-sm-2"><strong>Mobile : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['mobile'] }}</div>
					</div>
					<div class="form-group row">		  
						<div class="col-sm-2"><strong>Phone number : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['phone_no'] }}</div>
				  	</div>
					<div class="form-group row">
						<div class="col-sm-2"><strong>House no : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['houseno'] }}</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-2"><strong>Street : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['street'] }}</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-2"><strong>Zipcode : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['zipcode'] }}</div>
					</div>
					<div class="form-group row">		  
						<div class="col-sm-2"><strong>Information : </strong></div>
						<div class="col-sm-10">{{ $restsuggestion_notiInfo[0]['information'] }}</div>
				  	</div>
				
           
        </div>

            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
    @endsection

@section('script')



@endsection







