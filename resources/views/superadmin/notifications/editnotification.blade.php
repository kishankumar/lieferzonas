@extends('layouts.superadminbody')
@section('title')
@endsection
@section('body')
<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Notification Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
       <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.restnotification.index')}}"><i class="fa fa-dashboard"></i>Notification</a></li>
        <li class="active">Edit Notification</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">Add Notification With Full Features</h3>


            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('errmsg'))
						<div class="alert alert-danger" style="padding: 7px 15px;">
						  {{ Session::get('errmsg') }}
						  {{ Session::put('errmsg','') }}
						</div>
					    @endif
						@if($errors->any())
							<div class="alert alert-danger ">
								<ul class="list-unstyled">
									@foreach($errors->all() as $error)
										<li> {{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
                
{!! Form::model($notification,['route'=>['root.restnotification.update',$notification['0']['id']],'method'=>'patch',
'id' => 'add-notification-form','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
    <div class="modal-body" style="padding: 5px;">
        
        <div class="panel-body">
            <div class="errors" id="errors" style="color: red"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('Title') !!}<span class="red">*</span>
                    {!! Form::text('title',$notification['0']['title'],["id"=>'title',"class"=>"form-control","onkeypress"=>"hideErrorMsg('titleerr')"]) !!}
                  
                     <span id="titleerr" class="text-danger"></span>   
                    </div>

                </div>
                {!! Form::hidden('notification_id','',['id'=>'notification_id']) !!}
                <div class="col-md-6" id="show_status" >
                    <div class="form-group">
                        {!! Form::label('Status') !!}
                        {!! Form::select('status', 
                        array(
                        '1'=>'Active',
                        '0'=>'Deactive',
                        ),  $notification[0]['status'], ['id' => 'status','class' => 'form-control']) !!}
                        
                    </div>
                </div>
            </div>
            <div class="row">
			  <div class="col-md-6">
				
				<div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="is_rest_specific"  
                            id="is_rest_specific" class="resturant" value='2' <?php if($notification[0]['send_type']==2){ echo 'checked';} ?>> <span>Resturant specific</span></label>
                </div>
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="is_rest_specific"  
                            id="is_rest_specific" class="everyone" value='1' <?php if($notification[0]['send_type']==1){ echo 'checked';} ?>> <span>Everyone</span></label>
                </div>
				
				<div class="clearfix"></div>
                                    
               <div id="multiplereslist" <?php if($notification[0]['send_type']==1)  {?> style="display:none;" <?php } ?>>
                    
                    <div class="col-sm-12" style="padding:0;"><hr></div>
                    <!--<div class="form-group col-sm-3">
                    <label class="radiobox"><input type="radio" name="1"> <span>Multiple</span></label>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="radiobox"><input type="radio" name="1"> <span> one</span></label>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="form-group col-sm-12">
                        <select class="js-example-basic-multiple corm-control" multiple="multiple" name="restlist[]" 
                        id="restlist">
                             <?php
                                foreach($resturants as $resturant) {
                                 $select = in_array( $resturant['id'], $selected_rest) ? ' selected' : null;
								 echo '<option value="'.$resturant['id'].'"'.$select.'>'.$resturant['f_name'].' '.$resturant['l_name'].'</option>'."\n"; 									
								?>
                                   
                                <?php
                                } ?>
                        </select>
                         <span id="restlisterr" class="text-danger"></span>
                    </div>
                </div>			
						
			  </div>
				
			</div>
			
          
              <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    {!! Form::label('comment') !!}
                    {!! Form::textarea('comment',$notification[0]['comment'],["id"=>'comment',"class"=>"form-control","onkeypress"=>"hideErrorMsg('commenterr')"]) !!}
                     <span id="commenterr" class="text-danger"></span>
                    </div>
                </div>
              </div>  
         
            
            <div class="form-group margin-top">
               {!!Form::button('Update',["id"=>'add',"class"=>"btn  btn-primary","onclick"=>"validate_notification()"]) !!}
                <hr>
				<p><span class="red">*</span> - Required Fields.</p>
            </div>
        </div>
    </div>
            {!! Form::close() !!}
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>
    @endsection
    @section('script')
    <script type="text/javascript"> 
    

function validate_notification()
     {


      var title = $("#title").val();
        
                 if(title=="")
                 {

                    $("#title").focus();
                    $("#titleerr").html('Please enter title');
                    $("#titleerr").show();
                    return false;
                 }
        var is_rest_specific = $("#is_rest_specific:checked").val();
        var restlist = $('.select2-selection__rendered li').length;
		
                 if(is_rest_specific=='2')
                 {
                    if(restlist=='1')
                   {
                      
                      $("#restlist").focus();
                      $("#restlisterr").html('Please select resturant');
                      $("#restlisterr").show();
                      return false;
                   }
				   else
				   {
					  $('#restlisterr').attr('style', 'display:none'); 
				   }
                 }
      
        
        var comment = $("#comment").val();
        
                 if(comment=="")
                 {

                    $("#comment").focus();
                    $("#commenterr").html('Please enter comment');
                    $("#commenterr").show();
                    return false;
                 }
                  document.forms["add-notification-form"].submit();
     }

     function hideErrorMsg(id){
    
      $('#'+id).attr('style', 'display:none');
  
    
     }
</script>
    @endsection
   
