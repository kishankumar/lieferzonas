@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')


<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Time validity Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.promocode.index')}}"><i class="fa fa-dashboard"></i>Promocode</a></li>
        <li class="active">Add Timevalidity</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">Add Time validity With Full Features</h3>


            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if($errors->any())
						<div class="alert alert-danger ">
							<ul class="list-unstyled">
								@foreach($errors->all() as $error)
									<li> {{ $error }}</li>
								@endforeach
							</ul>
						</div>
                        @endif
						
						@if(Session::has('errmsg'))
						<div class="alert alert-danger" style="padding: 7px 15px;">
						  {{ Session::get('errmsg') }}
						  {{ Session::put('errmsg','') }}
						</div>
						@endif
               
                
{!! Form::open(['route'=>'root.timevalidity.store','id'=>'add-validity-form'])!!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
    <div class="modal-body" style="padding: 5px;">
        
        <div class="panel-body">
            <div class="errors" id="errors" style="color: red"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('Name') !!}<span class="red">*</span>
                        {!! Form::text('name','',["id"=>'name',"class"=>"form-control",'onblur' => 'trimmer(name)']) !!}
                        
                    </div>
                </div>
                {!! Form::hidden('validity_id','',['id'=>'validity_id']) !!}
                <div class="col-md-6" id="show_status" >
                    <div class="form-group">
                        {!! Form::label('Status') !!}
                        {!! Form::select('status', 
                        array(
                        '1'=>'Active',
                        '0'=>'Deactive',
                        ), 1, ['id' => 'status','class' => 'form-control']) !!}
                        
                    </div>
                </div>
            </div>
            <div class="row">
				<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('Year') !!}
							<select id="year" name="year" class="form-control">
                            <option value="">Select Year</option>
                            <option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
                            </select>
							
						</div>
				</div>
				<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('Month') !!}
							<select id="month" name="month" class="form-control">
                            <option value="">Select Month</option>
                            <option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							
                            </select>
							
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('Week') !!}
							<select id="week" name="week" class="form-control">
                            <option value="">Select Week</option>
                            <option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							
                            </select>
							
						</div>
				</div>
				<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('Day') !!}
							<select id="day" name="day" class="form-control">
                            <option value="">Select Day</option>
                            <option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
				            <option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							
                            </select>
							
						</div>
				</div>
			</div>
            <div class="form-group">
                {!! Form::label('Description') !!}<span class="red">*</span>
                {!! Form::text('desc','',["id"=>'desc',"class"=>"form-control"]) !!}
                <span id="descErr" style="color:red"></span>
            </div>
            
            <div class="form-group margin-top">
               {!!Form::submit('Add',["id"=>'submit',"class"=>"btn  btn-primary"]) !!}
                <hr>
				<p><span class="red">*</span> - Required Fields.</p>
            </div>
        </div>
    </div>
            {!! Form::close() !!}
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>
   

    @endsection


    @section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
     $("#add-validity-form").validate({
      rules: {


        name: {
            required: true
               },
        desc:{ required: true
               }
         
        },
       messages: {

                    name: "Please enter name",
                    desc: "Please enter description",
                    agree: "Please accept our policy"
                },
         
     });
});

     function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn) {

         $.ajax({

                url: '{{url('getdata')}}',
                type: "get",
                data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn},
                
             success: function (data) {
             
           $('#'+appendId).html(data);
           
          },
             error: function (jqXHR, textStatus, errorThrown) {
               
             }
         });
   
      }
    </script>
    @endsection
   
   

