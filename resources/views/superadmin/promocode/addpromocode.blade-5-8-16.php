@extends('layouts.superadminbody')
@section('title')
@endsection
@section('body')
<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Promocode Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">Add Promocode With Full Features</h3>


            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    @foreach( $errors->all()  as $error)

                    <li> {{$error}} </li>
                    @endforeach
                @endif
                
{!! Form::open(['route'=>'root.promocode.store','id'=>'add-promocode-form'])!!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
    <div class="modal-body" style="padding: 5px;">
        
        <div class="panel-body">
            <div class="errors" id="errors" style="color: red"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('Coupon code') !!}<span class="red">*</span>
                    {!! Form::text('coupon_code','',["id"=>'coupon_code',"class"=>"form-control","onkeypress"=>"hideErrorMsg('coupon_codeerr')"]) !!}
                    {!!Form::button('Generate Code',["class"=>"form-group","onclick"=>"makecode('coupon_codeerr')"]) !!}
                     <span id="coupon_codeerr" class="text-danger"></span>   
                    </div>

                </div>
                {!! Form::hidden('promo_id','',['id'=>'promo_id']) !!}
                <div class="col-md-6" id="show_status" >
                    <div class="form-group">
                        {!! Form::label('Status') !!}
                        {!! Form::select('status', 
                        array(
                        '1'=>'Active',
                        '0'=>'Deactive',
                        ), 1, ['id' => 'status','class' => 'form-control']) !!}
                        
                    </div>
                </div>
            </div>
			<div class="row">
				<div class="col-md-6">
					   <div class="form-group">
					   {!! Form::label('Is New Customer') !!}
					   <label class="radiobox"><input type="radio" name="is_new_customer"  
                            id="is_new_customer" class="Yes" value='1' checked> <span>Yes</span></label>
					   <label class="radiobox"><input type="radio" name="is_new_customer"  
                            id="is_new_customer" class="Yes" value='0' checked> <span>No</span></label>
					   </div>
				</div>  
         
			</div>
			<div class="row">
               
				
                 <div class="col-md-6">
                   <div class="form-group">
                    {!! Form::label('Order Service') !!}<span class="red">*</span>
                    <?php
                     foreach($order_services as $order_service) { 
                         
                         ?>
						<label class="checkbox checkbox-inline mt0">
						<input type="checkbox" name='order_service[]'  value="{{ $order_service['id'] }}" 
						<?php if($order_service['id']=='1') { echo 'checked'; } ?>  onclick="hideErrorMsg('order_serviceerr');" > 
						<span>{{ $order_service['name']}}</span></label>
					
                    <?php
                    } ?>  
                    <span id="order_serviceerr" class="text-danger"></span>
                      
                    </div>

                </div>
				 <div class="col-md-6">
                   <div class="form-group">
                    {!! Form::label('Payment Method') !!}<span class="red">*</span>
                    
						<label class="checkbox checkbox-inline mt0">
						<input type="checkbox" name='payment_method[]'  value="0" checked  onclick="hideErrorMsg('pay_methoderr');"> 
						<span>COD</span></label>
					    <label class="checkbox checkbox-inline mt0">
						<input type="checkbox" name='payment_method[]'  value="1"  onclick="hideErrorMsg('pay_methoderr');" > 
						<span>Online</span></label>
                    <span id="pay_methoderr" class="text-danger"></span>
                      
                    </div>

                 </div>
               
            </div>
            <div class="row">
			  <div class="col-md-6">
						
				<div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="is_rest_specific"  
                            id="is_rest_specific" class="resturant" value='1'> <span>Restaurant specific</span></label>
                </div>
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="is_rest_specific"  
                            id="is_rest_specific" class="everyone" value='0' checked> <span>Everyone</span></label>
                </div>
				
				<div class="clearfix"></div>
                                    
                <div id="multiplereslist" style="display:none;">
                    
                    <div class="col-sm-12" style="padding:0;"><hr></div>
                    <!--<div class="form-group col-sm-3">
                    <label class="radiobox"><input type="radio" name="1"> <span>Multiple</span></label>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="radiobox"><input type="radio" name="1"> <span> one</span></label>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="form-group col-sm-12">
                        <select class="js-example-basic-multiple corm-control" multiple="multiple" name="restlist[]" 
                        id="restlist">
                            
                                <?php
                                foreach($resturants as $resturant) { ?>
                                    <option value="<?=$resturant['id'] ?>"><?=$resturant['f_name'].' '.$resturant['l_name'] ?></option>
                                <?php
                                } ?>
                        </select>
                         <span id="restlisterr" class="text-danger"></span>
                    </div>
                </div>			
						
			  </div>
			<div class="col-md-6">
                        
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="is_user_specific" 
                            id="is_user_specific" class="user" value='1'> <span>User specific</span></label>
                </div>
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="is_user_specific" 
                            id ="is_user_specific" class="every" value='0' checked> <span>Everyone</span></label>
                </div>
                
                <div class="clearfix"></div>
                                    
                <div id="multipleuserlist" style="display:none;">
                    
                    <div class="col-sm-12" style="padding:0;"><hr></div>
                    <!--<div class="form-group col-sm-3">
                    <label class="radiobox"><input type="radio" name="mul"> <span>Multiple</span></label>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="radiobox"><input type="radio" name="single"> <span> one</span></label>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="form-group col-sm-12">
					<div class="userlist">
                        <select class="js-example-basic-multiple corm-control" multiple="multiple" name="userlist[]"
                         id="userlist">
                          
                                <?php
                                foreach($users as $user) { ?>
                                    <option value="<?=$user['user_id'] ?>"><?=$user['first_name'] ?></option>
                                <?php
                                } ?>
                        </select>
					</div>
                        <span id="userlisterr" class="text-danger"></span>
                    </div>
                </div>          
                        
              </div>	
			</div>
			<div class="row">
			<div class="col-md-6">
                        
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="validity" value="T" id ="validity" class="val_table" > <span>Validity table</span></label>
                </div>
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="validity" value="C" id ="validity" class="val_date" checked > <span>Date validity</span></label>
                </div>
                
                <div class="clearfix"></div>
                <div class="form-group col-sm-12" id="select_s" style="display:none;">
                    <select class="form-control" name="validity_table" id="validity_table" 
                    onchange="hideErrorMsg('validity_tableerr')">
                    <option value=''>Select</option>    
						      <?php
                                foreach($time_validations as $time_validation) { ?>

                                    <option value="<?=$time_validation['id'] ?>"><?=$time_validation['name'] ?></option>
                              <?php
                              } ?>
                    </select>
                     <span id="validity_tableerr" class="text-danger"></span>
                </div>
                <div class="form-group col-sm-12" id="date_s" style="display:block;">
                    <div class="row">
                        <div class="col-sm-6">
                          <div class="input-group date " id="datetimepicker8" onClick="hideErrorMsg('valid_fromerr')">
                            <input type="text" class="form-control" name="valid_from" id="valid_from" placeholder="Valid from" >
                            <span class="input-group-addon"> 
                            <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                            <span id="valid_fromerr" class="text-danger"></span>
                    </div>
                    <div class="col-sm-6">
                          <div class="input-group date" id="datetimepicker7" onClick="hideErrorMsg('valid_toerr')">
                            <input type="text" class="form-control" name="valid_to" id="valid_to" placeholder="Valid to">
                            <span class="input-group-addon"> 
                            <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                            <span id="valid_toerr" class="text-danger"></span>
                    </div>
                    </div>
                </div>          
                        
            </div>
            <div class="col-md-6">
                        
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="payment_mode" id="payment_mode" class="percentage" value='P' checked> <span>Percentage</span></label>
                </div>
                <div class="form-group col-sm-6">
                            <label class="radiobox"><input type="radio" name="payment_mode" id="payment_mode" class="cash" value='C' > <span>Fixed amount</span></label>
                </div>
                
                <div class="clearfix"></div>
                                    
                <div id="discount">
                   
                    <div class="clearfix"></div>
                    <div class="form-group col-sm-12">
                    {!! Form::text('payment_amount','',["id"=>'payment_amount',"class"=>"form-control","onkeypress"=>"hideErrorMsg('payment_amounterr')"]) !!}
                    <span id="payment_amounterr" class="text-danger"></span>
                    </div>
                </div>          
                        
              </div>
			</div>
          
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('Minimum bill amount') !!}<span class="red">*</span>
                    {!! Form::text('min_amount','',["id"=>'min_amount',"class"=>"form-control","onkeypress"=>"hideErrorMsg('min_amounterr')"]) !!}
                     <span id="min_amounterr" class="text-danger"></span>
                    </div>
                </div>
              </div>  
         
            
            <div class="form-group margin-top">
               {!!Form::button('Add',["id"=>'add',"class"=>"btn  btn-primary","onclick"=>"validate_promocode()"]) !!}
                <hr>
				<p><span class="red">*</span> - Required Fields.</p>
            </div>
        </div>
    </div>
            {!! Form::close() !!}
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>
    @endsection
    @section('script')
    <script type="text/javascript"> 
    

function validate_promocode()
     {


        var coupon_code = $("#coupon_code").val();
        
                 if(coupon_code=="")
                 {

                    $("#coupon_code").focus();
                    $("#coupon_codeerr").html('Please select coupon code');
                    $("#coupon_codeerr").show();
                    return false;
                 }
		var order_service_length = $('[name="order_service[]"]:checked').length;
		if(order_service_length<1)
		{
			$("#order_service").focus();
            $("#order_serviceerr").html('Please check atleast one order service');
            $("#order_serviceerr").show();
            return false;
		}
		var payment_method_length = $('[name="payment_method[]"]:checked').length;
		//alert(payment_method_length);
		if(payment_method_length < 1)
		{
			$("#payment_method").focus();
            $("#pay_methoderr").html('Please check atleast one payment method');
            $("#pay_methoderr").show();
            return false;
		}
        var is_rest_specific = $("#is_rest_specific:checked").val();
        var restlist = $('.select2-selection__rendered li').length;
		var is_user_specific = $("#is_user_specific:checked").val();
        var userlist = $('.userlist .select2-selection__rendered li').length;
		
		
                 if(is_rest_specific=='1')
                 {
					
                    if(restlist=='2')
                   {
                      
                      $("#restlist").focus();
                      $("#restlisterr").html('Please select resturant');
                      $("#restlisterr").show();
                      return false;
                   }
				   else
				   {
					  $('#restlisterr').attr('style', 'display:none'); 
				   }
                 }
      
                 if(is_user_specific=='1')
                 {
                    if(userlist=='1')
                    {
                      $("#userlist").focus();
                      $("#userlisterr").html('Please select user');
                      $("#userlisterr").show();
                      return false;
                    }
					 else
				   {
					  $('#userlisterr').attr('style', 'display:none'); 
				   }
                 }
       

        var validity = $("#validity:checked").val();
        
        var validity_table = $("#validity_table").val();
        var valid_from = $("#valid_from").val();
        var valid_to = $("#valid_to").val();
                 if(validity=='T')
                 {
                    if(validity_table=='')
                    {
                      $("#validity_table").focus();
                      $("#validity_tableerr").html('Please select one validity title');
                      $("#validity_tableerr").show();
                    return false;
                    }
                   
                 }
                  if(validity=='C')
                 {
                    if(valid_from=='')
                    {
                      $("#valid_from").focus();
                      $("#valid_fromerr").html('Please enter valid from');
                      $("#valid_fromerr").show();
                    return false;
                    }
                    if(valid_to=='')
                    {
                      $("#valid_to").focus();
                      $("#valid_toerr").html('Please enter valid to');
                      $("#valid_toerr").show();
                    return false;
                    }
                   
                 }
	    var payment_mode = $("#payment_mode:checked").val();
        
        var payment_amount = $("#payment_amount").val();
		
		var min_amount = $("#min_amount").val();
                if(payment_amount=='')
				{
				  $("#payment_amount").focus();
				  $("#payment_amounterr").html('Please enter discount value amount');
				  $("#payment_amounterr").show();
				return false;
				}
				if(isNaN(payment_amount)){
				  $("#payment_amount").focus();
				  $("#payment_amounterr").html('Please enter numeric for discount value amount');
				  $("#payment_amounterr").show();
				  return false;
				}
                if(payment_mode=='P')
                {
					 if(parseInt(payment_amount)>100)
					 {
						  $("#payment_amount").focus();
						  $("#payment_amounterr").html('Please enter percentage for discount value amount less than equal to 100');
						  $("#payment_amounterr").show();
						  return false;
					 }
				} 
				if(payment_mode=='C')
				{
					if(parseInt(payment_amount)>parseInt(min_amount))
					{
						  $("#payment_amount").focus();
						  $("#payment_amounterr").html('Please enter discount value amount less than min amount');
						  $("#payment_amounterr").show();
						  return false;
					}
				}
											
             
        
                 if(min_amount=="")
                 {

                    $("#min_amount").focus();
                    $("#min_amounterr").html('Please enter minimum amount');
                    $("#min_amounterr").show();
                    return false;
                 }
				 if(isNaN(min_amount)){
					
					$("#min_amount").focus();
					$("#min_amounterr").html('Please enter numeric for minimum amount');
				    $("#min_amounterr").show();
					return false;
				 }
                  document.forms["add-promocode-form"].submit();
     }

     function hideErrorMsg(id){
    
      $('#'+id).attr('style', 'display:none');
  
    
     }
</script>
    @endsection
   
