@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')


<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Time validity Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.promocode.index')}}"><i class="fa fa-dashboard"></i>Promocode</a></li>
        <li class="active">Edit Timevalidity</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">

              <h3 class="box-title">Edit Time validity With Full Features</h3>


            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
               @if($errors->any())
						<div class="alert alert-danger ">
							<ul class="list-unstyled">
								@foreach($errors->all() as $error)
									<li> {{ $error }}</li>
								@endforeach
							</ul>
						</div>
                        @endif
						
						@if(Session::has('errmsg'))
						<div class="alert alert-danger" style="padding: 7px 15px;">
						  {{ Session::get('errmsg') }}
						  {{ Session::put('errmsg','') }}
						</div>
						@endif

{!! Form::model($timevalidity,['route'=>['root.timevalidity.update',$timevalidity['0']['id']],'method'=>'patch','id' => 'validity_form','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
   
        
        <div class="panel-body">
            <div class="errors" id="errors" style="color: red"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('Name') !!}
                        {!! Form::text('name',$timevalidity['0']['name'],["id"=>'name',"class"=>"form-control",'onblur' => 'trimmer(name)']) !!}
                        
                    </div>
                </div>
                {!! Form::hidden('validity_id',$timevalidity['0']['id'],['id'=>'validity_id']) !!}
                <div class="col-md-6" id="show_status" >
                    <div class="form-group">
                        {!! Form::label('Status') !!}
                        {!! Form::select('status', 
                        array(
                        '1'=>'Active',
                        '0'=>'Deactive',
                        ), $timevalidity['0']['status'], ['id' => 'status','class' => 'form-control']) !!}
                        
                    </div>
                </div>
            </div>
            <div class="row">
	<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('Year') !!}
				<select id="year" name="year" class="form-control">
        <option value="0">Select Year</option>
        <option value="1" <?php if($timevalidity['0']['year']=='1'){echo "Selected='selected'" ;} ?>>1</option>
				<option value="2" <?php if($timevalidity['0']['year']=='2'){echo "Selected='selected'" ;} ?>>2</option>
				<option value="3" <?php if($timevalidity['0']['year']=='3'){echo "Selected='selected'" ;} ?>>3</option>
				<option value="4" <?php if($timevalidity['0']['year']=='4'){echo "Selected='selected'" ;} ?>>4</option>
				<option value="5" <?php if($timevalidity['0']['year']=='5'){echo "Selected='selected'" ;} ?>>5</option>
				<option value="6" <?php if($timevalidity['0']['year']=='6'){echo "Selected='selected'" ;} ?>>6</option>
				<option value="7" <?php if($timevalidity['0']['year']=='7'){echo "Selected='selected'" ;} ?>>7</option>
				<option value="8" <?php if($timevalidity['0']['year']=='8'){echo "Selected='selected'" ;} ?>>8</option>
				<option value="9" <?php if($timevalidity['0']['year']=='9'){echo "Selected='selected'" ;} ?>>9</option>
				<option value="10" <?php if($timevalidity['0']['year']=='10'){echo "Selected='selected'" ;} ?>>10</option>
        </select>
				
			</div>
	</div>
	<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('Month') !!}
				<select id="month" name="month" class="form-control">
        <option value="0">Select Month</option>
        <option value="1" <?php if($timevalidity['0']['month']=='1'){echo "Selected='selected'" ;} ?>>1</option>
				<option value="2" <?php if($timevalidity['0']['month']=='2'){echo "Selected='selected'" ;} ?>>2</option>
				<option value="3" <?php if($timevalidity['0']['month']=='3'){echo "Selected='selected'" ;} ?>>3</option>
				<option value="4" <?php if($timevalidity['0']['month']=='4'){echo "Selected='selected'" ;} ?>>4</option>
				<option value="5" <?php if($timevalidity['0']['month']=='5'){echo "Selected='selected'" ;} ?>>5</option>
				<option value="6" <?php if($timevalidity['0']['month']=='6'){echo "Selected='selected'" ;} ?>>6</option>
				<option value="7" <?php if($timevalidity['0']['month']=='7'){echo "Selected='selected'" ;} ?>>7</option>
				<option value="8" <?php if($timevalidity['0']['month']=='8'){echo "Selected='selected'" ;} ?>>8</option>
				<option value="9" <?php if($timevalidity['0']['month']=='9'){echo "Selected='selected'" ;} ?>>9</option>
				<option value="10" <?php if($timevalidity['0']['month']=='10'){echo "Selected='selected'" ;} ?>>10</option>
				<option value="11" <?php if($timevalidity['0']['month']=='12'){echo "Selected='selected'" ;} ?>>11</option>
				
        </select>
				
			</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('Week') !!}
				<select id="week" name="week" class="form-control">
        <option value="0">Select Week</option>
        <option value="1" <?php if($timevalidity['0']['week']=='1'){echo "Selected='selected'" ;} ?>>1</option>
				<option value="2" <?php if($timevalidity['0']['week']=='2'){echo "Selected='selected'" ;} ?>>2</option>
				<option value="3" <?php if($timevalidity['0']['week']=='3'){echo "Selected='selected'" ;} ?>>3</option>
				<option value="4" <?php if($timevalidity['0']['week']=='4'){echo "Selected='selected'" ;} ?>>4</option>
				
        </select>
				
			</div>
	</div>
	<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('Day') !!}
				<select id="day" name="day" class="form-control">
        <option value="0">Select Day</option>
        <option value="1" <?php if($timevalidity['0']['day']=='1'){echo "Selected='selected'" ;} ?>>1</option>
				<option value="2" <?php if($timevalidity['0']['day']=='2'){echo "Selected='selected'" ;} ?>>2</option>
				<option value="3" <?php if($timevalidity['0']['day']=='3'){echo "Selected='selected'" ;} ?>>3</option>
				<option value="4" <?php if($timevalidity['0']['day']=='4'){echo "Selected='selected'" ;} ?>>4</option>
				<option value="5" <?php if($timevalidity['0']['day']=='5'){echo "Selected='selected'" ;} ?>>5</option>
				<option value="6" <?php if($timevalidity['0']['day']=='6'){echo "Selected='selected'" ;} ?>>6</option>
				<option value="7" <?php if($timevalidity['0']['day']=='7'){echo "Selected='selected'" ;} ?>>7</option>
				<option value="8"  <?php if($timevalidity['0']['day']=='8'){echo "Selected='selected'" ;} ?>>8</option>
				<option value="9"  <?php if($timevalidity['0']['day']=='9'){echo "Selected='selected'" ;} ?>>9</option>
				<option value="10" <?php if($timevalidity['0']['day']=='10'){echo "Selected='selected'" ;} ?>>10</option>
				<option value="11"  <?php if($timevalidity['0']['day']=='11'){echo "Selected='selected'" ;} ?>>11</option>
				<option value="12"  <?php if($timevalidity['0']['day']=='12'){echo "Selected='selected'" ;} ?>>12</option>
	      <option value="13"  <?php if($timevalidity['0']['day']=='13'){echo "Selected='selected'" ;} ?>>13</option>
				<option value="14"  <?php if($timevalidity['0']['day']=='14'){echo "Selected='selected'" ;} ?>>14</option>
				<option value="15"  <?php if($timevalidity['0']['day']=='15'){echo "Selected='selected'" ;} ?>>15</option>
				<option value="16"  <?php if($timevalidity['0']['day']=='16'){echo "Selected='selected'" ;} ?>>16</option>
				<option value="17"  <?php if($timevalidity['0']['day']=='17'){echo "Selected='selected'" ;} ?>>17</option>
				<option value="18"  <?php if($timevalidity['0']['day']=='18'){echo "Selected='selected'" ;} ?>>18</option>
				<option value="19"  <?php if($timevalidity['0']['day']=='19'){echo "Selected='selected'" ;} ?> >19</option>
				<option value="20"  <?php if($timevalidity['0']['day']=='20'){echo "Selected='selected'" ;} ?>>20</option>
				<option value="21"  <?php if($timevalidity['0']['day']=='21'){echo "Selected='selected'" ;} ?>>21</option>
				<option value="22"  <?php if($timevalidity['0']['day']=='22'){echo "Selected='selected'" ;} ?>>22</option>
				<option value="23"  <?php if($timevalidity['0']['day']=='23'){echo "Selected='selected'" ;} ?>>23</option>
				<option value="24"  <?php if($timevalidity['0']['day']=='24'){echo "Selected='selected'" ;} ?>>24</option>
				<option value="25"  <?php if($timevalidity['0']['day']=='25'){echo "Selected='selected'" ;} ?>>25</option>
				<option value="26"  <?php if($timevalidity['0']['day']=='26'){echo "Selected='selected'" ;} ?>>26</option>
				<option value="27"  <?php if($timevalidity['0']['day']=='27'){echo "Selected='selected'" ;} ?>>27</option>
				<option value="28"  <?php if($timevalidity['0']['day']=='28'){echo "Selected='selected'" ;} ?>>28</option>
				<option value="29"  <?php if($timevalidity['0']['day']=='29'){echo "Selected='selected'" ;} ?>>29</option>
				
        </select>
				
			</div>
	</div>
</div>
            <div class="form-group">
                {!! Form::label('Description') !!}
                {!! Form::text('desc',$timevalidity['0']['description'],["id"=>'desc',"class"=>"form-control"]) !!}
                <span id="descErr" style="color:red"></span>
            </div>
            
            <div class="form-group margin-top">
                {!!Form::submit('Update',["id"=>'submit',"class"=>"btn  btn-primary"]) !!}
                <hr>
            </div>
        </div>
 
            {!! Form::close() !!}
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>
   

    @endsection


    @section('script')
     <script type="text/javascript"> 
     $(document).ready(function(){
     $("#validity_form").validate({
      rules: {


        name: {
            required: true
               },
        desc:{ required: true
               }
         
        },
       messages: {

                    name: "Please enter firstname",
                    desc: "Please enter description",
                    agree: "Please accept our policy"
                },
         
     });
});


     function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn) {

         $.ajax({

                url: '{{url('getdata')}}',
                type: "get",
                data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn},
                
             success: function (data) {
             
           $('#'+appendId).html(data);
           
          },
             error: function (jqXHR, textStatus, errorThrown) {
               
             }
         });
   
      }
    </script>
    @endsection
   
   

