@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Promocode Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
       <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('root.promocode.index')}}"><i class="fa fa-dashboard"></i>Promocode</a></li>
        <li class="active">Promocode Detail</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Promocode Detail With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-12 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
               
                     <!--<a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <a  href="" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable" >
                         <span >Download</span>
                     </a>
                     <a class="btn btn-danger DTTT_button_xls" onclick="delete_offers()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                         <span>Delete</span>
                     </a>-->
                </div>
             </div>
            
            <div class="panel-body">
            @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                    
				<ul class="offer_box list-inline">
					<li><strong class="text-success">Coupon Code :</strong></li>
					<li>
					 {{ $promocode[0]['coupon_code'] }}
					</li>
				</ul>
			
				
                <ul class="offer_box list-inline">	
					<li><strong class="text-success">Is New Customer :</strong> </li>
					<li>
                         @if($promocode[0]['is_new_customer']==1) Yes @else No @endif
					</li>
				</ul>
				
                <ul class="offer_box list-inline">	
					<li><strong class="text-success">Order Service: </strong></li>
					<li>
						<?php
                     foreach($order_services as $order_service) { 
                         
                         ?>
						<label class="checkbox checkbox-inline mt0">
						<input type="checkbox" name='order_service[]'  value="{{ $order_service['id'] }}" 
						<?php if($order_service['id']==$promocode[0]['order_service_id'] || $promocode[0]['order_service_id']==3 ) { echo 'checked'; } ?> onclick="hideErrorMsg('order_serviceerr');" > 
						<span>{{ $order_service['name']}}</span></label>
					
                    <?php
                    } ?>  
					</li>
				</ul>
                
                <ul class="offer_box list-inline">
					<li><strong class="text-success">Payment Method: </strong></li>
					<li><label class="checkbox checkbox-inline mt0">
						<input type="checkbox" name='payment_method[]'  value="0" 
						<?php if($promocode[0]['payment_method']==0 || $promocode[0]['payment_method']==2) { echo 'checked'; } ?> onclick="hideErrorMsg('pay_methoderr');" > 
						<span>COD</span></label>
					    <label class="checkbox checkbox-inline mt0">
						<input type="checkbox" name='payment_method[]'  value="1" 
						<?php if($promocode[0]['payment_method']==1 || $promocode[0]['payment_method']==2) { echo 'checked'; } ?> onclick="hideErrorMsg('pay_methoderr');" > 
						<span>Online</span></label></li>
				</ul>
				<ul class="offer_box list-inline">
					<li><strong class="text-success">Valid From: </strong></li>
					<li>{{ date("d M Y", strtotime($promocode[0]['valid_from']))  }}</li>
				</ul>
				<ul class="offer_box list-inline">
					<li><strong class="text-success">Valid To: </strong></li>
					<li>{{ date("d M Y", strtotime($promocode[0]['valid_to']))  }}</li>
				</ul>
                <ul class="offer_box list-inline">
					<li><strong class="text-success">Discount Coupon Value: </strong></li>
					<li>{{$promocode[0]['payment_amount']}} @if($promocode[0]['payment_mode']=='P'){{ '%'  }} @else {{ '€'  }} @endif</li>
				</ul>
				<ul class="offer_box list-inline">
					<li><strong class="text-success">Order Minimun Value: </strong></li>
					<li>{{ $promocode[0]['min_amount'] }}</li>
				</ul>
				<ul class="offer_box list-inline">
					<li><strong class="text-success">Is Restaurant Specific: </strong></li>
					<li> @if($promocode[0]['is_rest_specific']==1) {{ 'Yes'  }}<a href="#" class="btn-border" data-target="#" data-toggle="modal" onclick="rest_list(<?=$promocode[0]['id']?>);">Show Restaurant List</a> @else {{ 'No'  }} @endif</li>
				</ul>
				<ul class="offer_box list-inline">
					<li><strong class="text-success">Is User Specific: </strong></li>
					<li> @if($promocode[0]['is_user_specific']==1) {{ 'Yes'  }} <a href="#" class="btn-border" data-target="#" data-toggle="modal" onclick="user_list(<?=$promocode[0]['id']?>);">Show User List</a> @else {{ 'No'  }} @endif</li>
				</ul>
				
                <div class="clearfix"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 
<div aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal_rating" class="modal fade">
	<div class="modal-dialog modal-md robotoregular">
		<div class="modal-content">
			<div style="background:#81c02f; color:#fff;" class="modal-header">
				<button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">User list</h4>
			</div>
			<div class="modal-body">
				
				    <div class="row">
					
						<div class="col-xs-12" id="user_list">
						
						</div>
					
		            </div>
				
			</div>
		</div>
	</div>
</div>
<div aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal_rating1" class="modal fade">
	<div class="modal-dialog modal-md robotoregular">
		<div class="modal-content">
			<div style="background:#81c02f; color:#fff;" class="modal-header">
				<button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Restaurant list</h4>
			</div>
			<div class="modal-body">
				
				    <div class="row">
					
						<div class="col-xs-12" id="rest_list">
						
						</div>
					
		            </div>
				
			</div>
		</div>
	</div>
</div>

 <!-- /.modal -->
 @endsection


    @section('script')
    <script>
function user_list(promo_id)
{
	var promo_id = promo_id;
	$.ajax({
			url: '{{ url('showuser_list') }}',
			type: "GET",
			dataType:"json",
			data: {'promo_id':promo_id},
			success: function(res)
			{
				if(res.success==1)
				{
					$('#myModal_rating').modal('show');
					$("#user_list").html(res.data);
					
				}
				
			}
            });
}
</script>
  
<script>
function rest_list(promo_id)
{
	var promo_id = promo_id;
	$.ajax({
			url: '{{ url('showrest_list') }}',
			type: "GET",
			dataType:"json",
			data: {'promo_id':promo_id},
			success: function(res)
			{
				if(res.success==1)
				{
					$('#myModal_rating1').modal('show');
					$("#rest_list").html(res.data);
					
				}
				
			}
            });
}
</script>
    @endsection




