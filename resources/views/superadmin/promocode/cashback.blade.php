@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cashback
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
           <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.promocode.index')}}"><i class="fa fa-dashboard"></i>Promocode</a></li>
            <li class="active">Cashback</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Cashback Points</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="col-sm-12 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                           
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" onclick="download_cashback()" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable"><span>Download</span></a>
                        </div>
                    </div>

                    <div class="box-body">
					@if(Session::has('message'))
						<div class="alert alert-success" style="padding: 7px 15px;">
						  {{ Session::get('message') }}
						  {{ Session::put('message','') }}
						</div>
					@endif
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                                    <div class="checkbox icheck">
                                        <label>
                                        {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                        <span class=""></span>
                                        </label>
                                    </div>
                                </th>
                                <th>Cashback Title</th>
								<th>Points</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($cashbackpoints))
                            @foreach($cashbackpoints as $cashbackpoint)
                            <tr>
                                    <td>
									<div class="checkbox icheck">
										<label>
											{!! Form::checkbox('cashback', $cashbackpoint['id'], null, ['class' => 'cashback checkBoxClass']) !!}
											<span class=""></span>
										</label>
									</div>
									</td>
                                    <td>{{$cashbackpoint['cashback_title']}}</td>
									<td>{{$cashbackpoint['points']}}</td>
                                    
									<td>{{ (strlen($cashbackpoint['description']) > 50) ? substr($cashbackpoint['description'],0,50).'...' : $cashbackpoint['description']  }}</td>
                                    <td> @if($cashbackpoint['status']==1) <i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($cashbackpoint['status']==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>
                                    <td><a  class="btn btn-success" href="#" onclick="editcashback({{$cashbackpoint['id']}})"> <i class="fa fa-edit"></i></a></td>
                            </tr>
                            @endforeach
                             @else 
                                <tr>
                                    <td colspan="8" align="center">
                                        No Record Exist
                                    </td>
                                </tr>
                            @endif
                            </tbody>

                        </table>
                        {!! $cashbackpoints->render() !!}
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<div class="modal fade" id="edit-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Edit Cashback Points
            </div>

                <div class="modal-body" style="padding: 5px;">
                    <div class="panel-body">
                        @if(Session::has('errmsg'))
						<div class="alert alert-danger" style="padding: 7px 15px;">
						  {{ Session::get('errmsg') }}
						  {{ Session::put('errmsg','') }}
						</div>
					    @endif
						@if($errors->any())
							<div class="alert alert-danger ">
								<ul class="list-unstyled">
									@foreach($errors->all() as $error)
										<li> {{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
                       
                       {!! Form::model($cashbackpoints,['route'=>['root.cashback.update'],'method'=>'patch',
                       'id' => 'edit_form','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Cashback title</label>
                                       
                                        {!! Form::text('cashback_title','',['id'=>'cashback_title','class'=>'form-control','readonly'=>'true']) !!}
                                         {!! Form::hidden('cashback_id','',['id'=>'cashback_id','class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Points</label><span class="red">*</span>
                                        {!! Form::text('points','',['id'=>'points','class'=>'form-control required']) !!}
                                    </div>

                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Description</label><span class="red">*</span>
                                        {!! Form::textarea('description','',['id'=>'description','class'=>'form-control required']) !!}
                                    </div>

                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Status</label>
                                        {!! Form::select('status',['1'=>'Active','0'=>'Deactive'], '', ['id'=>'status','class'=>'form-control']) !!}
                                    </div>

                                </div>

                            </div>

                            <div class="form-group margin-top">
                               
                                {!! Form::submit('Update ',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                                <hr>
                               <p><span class="red">*</span> - Required Fields.</p>
                            </div>
                        {{--</form>--}}
                        {!! Form::close() !!}
                    </div>

                </div>

        </div>
    </div>
</div>

<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
<!--added by Rajlakshmi(31-05-16)-->
	
@endsection

@section('script')


    <script type="text/javascript">

     function editcashback(id)   
     {
        
        var cashback_id = id;
        $.ajax({
                url: '{{ url('fetchdataa') }}',
                type: "GET",
                dataType:"json",
                data: {'cashback_id':cashback_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
                        
                        $('#cashback_title').val(res.cashback_title);
                        $('#points').val(res.points);
                        $('#description').val(res.description);
                        $('#status').val(res.status);
                         $('input[name=cashback_id]').val(res.id);
                        showModal('edit-form');
                    }
                    else
                    {
                        //document.getElementById("edit-form").reset();
                    }
                }
            });
     }
      $(document).ready(function(){
     $("#edit_form").validate({
      rules: {

        points: {
            required: true,
            digits:true
               },
        description: {
            required: true
               },
        
        
        },
       messages: {

                   points:{
					   required: "Please enter cashback points",
					   digits: "Please enter digit for cashback points",
				   } ,
               
                    description: "Please enter description",
                    
                    agree: "Please accept our policy"
                },
         
     });
});

/*added by Rajlakshmi(01-06-16)*/
     function download_cashback()
     {
        var len = $(".cashback:checked").length;
        if(len==0){
            alert('Please select atleast one cashback');
            return false;
        }
        var base_url1=base_url+'root/cashback/download';
        var r = confirm("Are you sure to download cashback?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="cashback"]:checked').each(function(){
                selectedPages.push($(this).val());
            });

            $.ajax({
                url: 'cashback/download',
                type: "post",
                dataType:"json",
                data: {'selectedCashback':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                     
                     
                    
                      $("#download").html(res.data1);
					 
                        var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Cashbackpoint List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
					    doc.save("cashbackpoint_list.pdf");
				}
            });
        } else {
            return false;
        }
		
	}
	
	
    
         /*added by Rajlakshmi(01-06-16)*/

    </script>
     @if($errors->any())
        <script type="text/javascript">
         showModal('edit-form');
        </script>
     @endif
@endsection