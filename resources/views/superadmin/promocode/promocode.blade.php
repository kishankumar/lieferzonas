@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Promocode
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
       <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('root.promocode.index')}}"><i class="fa fa-dashboard"></i>Promocode</a></li>
         <li><a href="#"><i class="fa fa-dashboard"></i>Promocode</a></li>
        <li class="active">Promocode</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Promocode Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/promocode/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/promocode/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default"  data-toggle="modal"  data-original-title href="{{ url('root/promocode/create') }}"><span>Add</span></a>

                 
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_promocode()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>

                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_promocode()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
            </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            
            <div class="clearfix" style="margin: 20px 0;"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
					  {{ Session::get('message') }}
					  {{ Session::put('message','') }}
                    </div>
                    @endif
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                                    <div class="checkbox icheck">
                                        <label>
                                        {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                        <span class=""></span>
                                        </label>
                                    </div>
                                </th>

                        <th>
                            <a href="{{url('root/promocode/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='code' ? 'code-desc' :'code';?>">
                            Coupon code
                            <?php if($type=='code'){ echo $class; } elseif($type=='code-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/promocode/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='from' ? 'from-desc' :'from';?>">
                            Valid from
                            <?php if($type=='from'){ echo $class; } elseif($type=='from-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/promocode/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='to' ? 'to-desc' :'to';?>">
                            Valid to
                            <?php if($type=='to'){ echo $class; } elseif($type=='to-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/promocode/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='pay' ? 'pay-desc' :'pay';?>">

                            Discount Coupon value
                            <?php if($type=='pay'){ echo $class; } elseif($type=='pay-desc'){ echo $class1; } else { echo $class2; } ?>

                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/promocode/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='mini' ? 'mini-desc' :'mini';?>">

                            Order minimum value
                            <?php if($type=='mini'){ echo $class; } elseif($type=='mini-desc'){ echo $class1; } else { echo $class2; } ?>

                            </a>
                        </th>
                        <th>Is Restaurant specific</th>
                        <th>Is User specific</th>
                        <th>Status</th>
                        <th>Action</th>
						<th>View</th>
                    </tr>
                </thead>
                <tbody>
                      
                @if(count($promoInfo))
                    @foreach($promoInfo as $promoInfos)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('promocode', $promoInfos['id'], null, ['class' => 'promocode checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ $promoInfos['coupon_code']  }}</td>
                    <td>{{ date("d M Y", strtotime($promoInfos['valid_from']))  }}</td>
                    <td>{{ date("d M Y", strtotime($promoInfos['valid_to']))  }}</td>

                    <td>{{ $promoInfos['payment_amount']  }}(@if($promoInfos['payment_mode']=='P') {{ '%'  }} @else {{ '€'  }} @endif)</td>
                    <td>{{ $promoInfos['min_amount']  }}</td>


                    <td>@if($promoInfos['is_rest_specific']==1) {{ 'Yes'  }} @else {{ 'No'  }} @endif</td>
                    <td>@if($promoInfos['is_user_specific']==1) {{ 'Yes'  }} @else {{ 'No'  }} @endif</td>

                    
                    <td> @if($promoInfos['status']==1) <i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($promoInfos['status']==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>

                    <td>
                        <a class="btn btn-success" href="{{url('root/promocode/'.$promoInfos['id'].'/edit')}}">

                        <i class="fa fa-edit"></i>
                    </a>
                    </td>
					<td>
                         <a class="btn btn-success" href="{{url('root/promocode/detail/'.$promoInfos['id'])}}"> View
                        
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
                {!! $promoInfo->appends(Request::except('page'))->render() !!}
        </div>

            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

	<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
	
    @endsection

@section('script')
<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/';
 
	
	 function delete_promocode()
     {
        var len = $(".promocode:checked").length;
        if(len==0){
            alert('Please select atleast one promocode');
            return false;
        }
        var base_url1=base_url+'root/promocode/delete';
        var r = confirm("Are you sure to delete promocode?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="promocode"]:checked').each(function(){
                selectedPages.push($(this).val());
            });

            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedPromocode':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('promocode not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
	
	/*added by Rajlakshmi(01-06-16)*/
     function download_promocode()
     {
        var len = $(".promocode:checked").length;
        if(len==0){
            alert('Please select atleast one promocode');
            return false;
        }
        var base_url1=base_url+'root/promocode/download';
        var r = confirm("Are you sure to download promocode?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="promocode"]:checked').each(function(){
                selectedPages.push($(this).val());
            });

            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedPromocode':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                     
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("promocode_list.pdf");
				}
            });
        } else {
            return false;
        }
		
	}
    
         /*added by Rajlakshmi(01-06-16)*/
</script>

@endsection







