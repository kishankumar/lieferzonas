@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Email-Types
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.email-types.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
        <li class="active">Email-Types</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Email-Types With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/email-types/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/email-types/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default"  data-toggle="modal" onclick="add_types(0)" data-original-title><span>Add</span></a>
                    <!--<a class="btn btn-default DTTT_button_csv"  onclick="add_types(1)" data-toggle="modal" data-original-title><span>Edit</span></a>-->
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_types()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_types()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
            </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            <div class="clearfix" style=""></div>
            
            <div class="box-body">
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>
                            <a href="{{url('root/email-types/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Email type
                            <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/email-types/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                            Description
                            <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php //echo '<pre>';print_r($categoies);die;?> 
                @if(count($types))
                    @foreach($types as $info)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('email_types', $info->id, null, ['class' => 'email_types checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ ucfirst($info->name)  }}</td>
                    <td>{{ (strlen($info->description) > 50) ? substr($info->description,0,50).'...' : $info->description  }}</td>
                    <td>
                        @if($info->status == 1) 
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-success" href="javascript:void(0)" onclick="add_types({{$info->id}})">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $types->appends(Request::except('page'))->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

	<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
	
    @endsection
    
@section('script')


<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function add_types(edit=0){
        $('#category_nameErr,#statusErr,#descriptionErr').html('');
        $('#errors').html('');
        if(edit){
            $('.new_category_title').html('<span class="glyphicon glyphicon-plus"></span> Edit Email Type');
            $('#add_new_category').text('Update');
            $.ajax({
                url: '{{route("root.email-types.edit")}}',
                type: "GET",
                dataType:"json",
                data: {'checkedEmail':edit, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        $('#name').val(res.name);
                        $('#status').val(res.status);
                        $('#description').val(res.description);
                        $('input[name=email_types_id]').val(res.id);
                        showModal('add-form1');
                        //$("#add-form1").modal("show");
                    }
                    else
                    {
                        document.getElementById("add-types-form").reset();
                    }
                }
            });
        }else{
            document.getElementById("add-types-form").reset();
            $('.new_category_title').html('<span class="glyphicon glyphicon-plus"></span> Add New Email Type.');
            $('#add_new_category').text('Add');
            $('input[name=email_types_id]').val(0);
            showModal('add-form1');
            //$("#add-form1").modal("show");
        }
    }
    
    function delete_types(){
        var len = $(".email_types:checked").length;
        if(len==0){
            alert('Please select atleast one email type');
            return false;
        }
        var base_url1=base_url+'email_types/delete';
        var r = confirm("Are you sure to delete email types?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="email_types"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedTypes':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('Email types not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
    
    $(function(){
        $("#add_new_category").unbind("click").click(function(){
            var type_name = $('#name').val().trim();
            var description=$("#description").val().trim();
            var status=$("#status").val();
            var arr = [type_name, description,status];
            var span=[category_nameErr,descriptionErr,statusErr];
            for(i=0;i<arr.length;i++)
            {
                if(arr[i]=='' || arr[i]==null)
                {
                    $(span[i]).html("This field is required");
                    $(span[i]).parent('div').children('input').focus();
                    return false;
                }
                else if(i==0)
                {
                    var alpha=/^[a-zA-ZäÄöÖüÜß«» -]*$/;
                    var validtext=alpha.test(arr[i]);
                    if(validtext==false)
                    {
                        $(span[i]).html("Please enter only alphabet");
                        $(span[i]).parent('div').children('input').focus();
                        return false;
                    }
                    else
                    {
                      $(span[i]).html("");
                    }
                }
                else
                {
                    $(span[i]).html("");
                }
            }
            $.ajax({
                url: '{{ url('root/email-types') }}',  //working
                type: "post",
                dataType:"json",
                data: $('#add-types-form').serialize(),
                success: function(res){
                    if(res.success==1)
                    {
                        document.getElementById("add-types-form").reset();
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#errors').html(errorsHtml);
                    }
                }
            });
        });
        
        $(".modalClose").click(function(){
            $('#category_nameErr,#statusErr,#descriptionErr').html('');
            $('#errors').html('');
            document.getElementById("add-types-form").reset();
        });
    });
	
	
		/*added by Rajlakshmi(01-06-16)*/					

	function download_types(){
        var len = $(".email_types:checked").length;
        if(len==0){
            alert('Please select atleast one email type');
            return false;
        }
        var base_url1=base_url+'email_types/download';
        var r = confirm("Are you sure to download email types?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="email_types"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedTypes':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Email Type List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
					    doc.save("emailtype_list.pdf");
				}
            });
        } else {
            return false;
        }
		
	}
    
         /*added by Rajlakshmi(01-06-16)*/
</script>
@endsection

@include('superadmin.email_types.add_email_types')





