<div class="modal fade" id="edit-form1" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close modalClose1" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span>  Edit Kitchen Detail.</h4>
            </div>
            
            {!! Form::open(['route'=>'root.kitchen.store','id'=>'edit-kitchen-form'])!!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
                <div class="modal-body" style="padding: 5px;">
                    <div class="panel-body">
                        <div class="errors" id="edit_errors" style="color: red"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Kitchen Name') !!}<span class="red">*</span>
                                    {!! Form::text('edit_name','',["id"=>'edit_name',"class"=>"form-control"]) !!}
                                    <span id="Editkitchen_nameErr" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                {!! Form::hidden('kitchen_id','',['id'=>'kitchen_id']) !!}
                                <div class="form-group">
                                    {!! Form::label('Status') !!}
                                    {!! Form::select('edit_status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ), 1, ['id' => 'edit_status','class' => 'form-control']) !!}
                                    <span id="editStatusErr" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                           {!! Form::label('Kitchen Description') !!} <span id="EditdescriptionErr" style="color:red"></span><span class="red">*</span>
                            {!! Form :: textarea('edit_description','',['style'=>'height:100px;','id'=>'edit_description','placeholder'=>'Enter Description ','class'=>'form-control'])  !!}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('Set Priority') !!} <span id="EditpriorityErr" style="color:red"></span><span class="red">*</span>
                            {!! Form::text('edit_priority','',["id"=>'edit_priority',"class"=>"form-control"]) !!}
                        </div>
							
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Valid From') !!} <span id="Editvalid_fromErr" style="color:red"></span><span class="red">*</span>
                                    <div class="input-group date" id="datetimepicker15">
                                        {!! Form::text('edit_valid_from','',["id"=>'edit_valid_from',"class"=>"form-control","placeholder"=>"Valid-From"]) !!}
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Valid To') !!} <span id="Editvalid_toErr" style="color:red"></span><span class="red">*</span>
                                        <div class="input-group date" id="datetimepicker16">
                                        {!! Form::text('edit_valid_to','',["id"=>'edit_valid_to',"class"=>"form-control","placeholder"=>"Valid-To"]) !!}
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
                        <div class="form-group margin-top">
                            {!! Form::button('Update',["id"=>'edit_new_kitchen',"class"=>"btn  btn-primary"]) !!}
                            <button id="add_new_kitchen" class="btn btn-primary modalClose" data-dismiss="modal" type="button">Cancel</button>
                            <hr>
							<p><span class="red">*</span> - Required Fields.</p>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
				
            </div>
        </div>
    </div>