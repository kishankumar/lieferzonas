
<div class="modal fade" id="add-form1" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close modalClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add New Kitchen.</h4>
            </div>
            
            {!! Form::open(['route'=>'root.kitchen.store','id'=>'add-kitchen-form'])!!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
                <div class="modal-body" style="padding: 5px;">
                    
                    <div class="panel-body">
                        <div class="errors" id="errors" style="color: red"></div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Kitchen Name') !!}<span class="red">*</span>
                                    {!! Form::text('name','',["id"=>'name',"class"=>"form-control"]) !!}
                                    <span id="kitchen_nameErr" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Status') !!}
                                    {!! Form::select('status', 
                                    array(
                                    '1'=>'Active',
                                    '0'=>'Deactive',
                                    ), 1, ['id' => 'status','class' => 'form-control']) !!}
                                    <span id="statusErr" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                           {!! Form::label('Kitchen Description') !!} <span id="descriptionErr" style="color:red"></span><span class="red">*</span>
                            {!! Form :: textarea('description','',['style'=>'height:100px;','id'=>'description','placeholder'=>'Enter Description ','class'=>'form-control'])  !!}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('Set Priority') !!} <span id="priorityErr" style="color:red"></span><span class="red">*</span>
                            {!! Form::text('priority','',["id"=>'priority',"class"=>"form-control"]) !!}
                        </div>
					    
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Valid From') !!} <span id="validFrom" style="color:red"></span><span class="red">*</span>
                                    <div class="input-group date" id="datetimepicker13">
                                        {!! Form::text('valid_from','',["id"=>'valid_from',"class"=>"form-control","placeholder"=>"Valid-From"]) !!}
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Valid To') !!} <span id="validTo" style="color:red"></span><span class="red">*</span>
                                        <div class="input-group date" id="datetimepicker14">
                                        {!! Form::text('valid_to','',["id"=>'valid_to',"class"=>"form-control","placeholder"=>"Valid-To"]) !!}
                                        <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
			
                        <div class="form-group margin-top">
                            {!! Form::button('Add',["id"=>'add_new_kitchen',"class"=>"btn  btn-primary"]) !!}
                            <button id="add_new_kitchen" class="btn btn-primary modalClose" data-dismiss="modal" type="button">Cancel</button>
                            <hr>
							<p><span class="red">*</span> - Required Fields.</p>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
				
            </div>
        </div>
    </div>