@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kitchen
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.kitchen.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
            <li class="active">Kitchen</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Kitchen Table With Full Features</h3>
                </div>
            <!-- /.box-header -->
            
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/kitchen/",'class'=>'form-inline','method'=>'GET')) !!}
                    {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                    {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                    <a class="btn btn-primary" href="{{url('root/kitchen/')}}">Reset Search</a>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-4 text-right" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    <a class="btn btn-default"  data-toggle="modal" onclick="add_kitchens()" data-target="#add-form" data-original-title><span>Add</span></a>
                    <!--<a class="btn btn-default DTTT_button_csv"  onclick="edit_kitchens()" data-toggle="modal" data-target="#edit-form" data-original-title><span>Edit</span></a>-->
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" onclick="download_kitchens()" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                
<!--                    <a class="btn btn-default" data-toggle="modal" href="{{ url('root/kitchen/create') }}" >
                        <span>Add</span>
                    </a>-->
                     
                    <a class="btn btn-danger DTTT_button_xls" onclick="delete_kitchens()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                        <span>Delete</span>
                    </a>
                </div>
            </div>
            
            <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
            
            <div class="clearfix" style=""></div>
            
            <div class="box-body">
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                        <th>
                            <a href="{{url('root/kitchen/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                            Kitchen
                            <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/kitchen/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                            Description
                            <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; }?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/kitchen/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='priority' ? 'priority-desc' :'priority';?>">
                            Priority
                            <?php if($type=='priority'){ echo $class; } elseif($type=='priority-desc'){ echo $class1; } else { echo $class2; }?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/kitchen/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='from' ? 'from-desc' :'from';?>">
                            Valid From
                            <?php if($type=='from'){ echo $class; } elseif($type=='from-desc'){ echo $class1; } else { echo $class2; }?>
                            </a>
                        </th>
                        <th>
                            <a href="{{url('root/kitchen/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='to' ? 'to-desc' :'to';?>">
                            Valid To
                            <?php if($type=='to'){ echo $class; } elseif($type=='to-desc'){ echo $class1; } else { echo $class2; }?>
                            </a>
                        </th>
                        <th>Kitchen Validity</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @if(count($kitcheninfo))
                    @foreach($kitcheninfo as $info)
                <tr>
                    <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('kitchens', $info->id, null, ['class' => 'kitchens checkBoxClass']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ ucfirst($info->name)  }}</td>
                    <td>{{ (strlen($info->description) > 50) ? substr($info->description,0,50).'...' : $info->description  }}</td>
                    <td>{{ ($info->priority)  }}</td>
                    
                    <td>@if(strtotime($info->valid_from)>0)
                        {{ date("d M Y", strtotime($info->valid_from))  }}
                        @else
                            {{ 'None' }}
                        @endif    
                    </td>
                    <td>@if(strtotime($info->valid_to)>0)
                         {{ date("d M Y", strtotime($info->valid_to))  }}
                        @else
                            {{ 'None' }}
                        @endif    
                    </td>
                    
                    <td>
                        <?php
                            if(strtotime($info->valid_to)>0){
                                $Cdate = date("Y-m-d");
                                $Cdate = strtotime($Cdate);
                                if (strtotime($info->valid_to) > $Cdate) {
                                    echo 'Valid';
                                }else{
                                    echo 'Expired';
                                }
                            }else{
                                echo 'Expired';
                            }
                        ?>
                    </td>
                    
                    <td>
                        @if($info->status == 1) 
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>
                    
                    <td>
                        <a class="btn btn-success" href="javascript:void(0)" onclick="edit_kitchens({{$info->id}})">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $kitcheninfo->appends(Request::except('page'))->render() !!}
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
     
   <!--added by Rajlakshmi(02-06-16)-->
	<div id="download" style="display:none;" >
	</div>
   <!--added by Rajlakshmi(02-06-16)-->
	 
    @endsection
    
@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function add_kitchens(){
        document.getElementById("add-kitchen-form").reset();
        $('#kitchen_nameErr,#statusErr,#descriptionErr,#validFrom,#validTo,#errors,#priorityErr').html('');
        $("#edit_valid_from").prop('disabled',false);
        showModal('add-form1');
        //$("#add-form1").modal("show");
    }
    function edit_kitchens(edit=0){
        if(!edit){
            return false;
        }
        $.ajax({
            url: 'kitchens/fetchdata',
            type: "post",
            dataType:"json",
            data: {'checkedKitchen':edit, "_token":"{{ csrf_token() }}"},
            success: function(res){
                if(res.success==1)
                {
                    $('#edit_name').val(res.name);
                    $('#edit_status').val(res.status);
                    $('#edit_description').val(res.description);
                    $('#edit_priority').val(res.priority);
                    $('#edit_valid_from').val(res.valid_from);
                    $('#edit_valid_to').val(res.valid_to);
                    $('input[name=kitchen_id]').val(res.id);
                    $("#edit_valid_from").prop('disabled',true);
                    showModal('edit-form1');
                    //$("#edit-form1").modal("show");
                }
                else
                {
                    document.getElementById("edit-kitchen-form").reset();
                    $("#edit_valid_from").prop('disabled',false);
                }
            }
        });
    }
    function delete_kitchens(){
        var len = $(".kitchens:checked").length;
        if(len==0){
            alert('Please select atleast one Kitchen');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to delete kitchens?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="kitchens"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            //var base_url = base_host + SITE_URL_PRE_FIX + '/root/kitchens/delete';
            $.ajax({
                //url: '{{ url('/root/kitchen') }}' + '/' + selectedPages,  type: "DELETE",//working
                url: 'kitchens/delete',  //working
                type: "POST",
                dataType:"json",
                data: {'selectedKitchens':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('kitchens not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }
    
    $(function(){
        $("#add_new_kitchen").unbind("click").click(function(){
            var kitchen_name = $('#name').val().trim();
            var description=$("#description").val().trim();
            var status=$("#status").val();
            var priority=$("#priority").val();
            var valid_from=$("#valid_from").val();
            var valid_to=$("#valid_to").val();
            var arr = [kitchen_name, description,priority,status,valid_from,valid_to];
            var span=[kitchen_nameErr,descriptionErr,priorityErr,statusErr,validFrom,validTo];
            for(i=0;i<arr.length;i++)
            {
                if(arr[i]=='' || arr[i]==null)
                {
                    $(span[i]).html("This field is required");
                    $(span[i]).parent('div').children('input').focus();
                    return false;
                }
                else if(i==0)
                {
                    var alpha=/^[a-zA-ZäÄöÖüÜß«» -]*$/;
                    var validtext=alpha.test(arr[i]);
                    if(validtext==false)
                    {
                        $(span[i]).html("Please enter only alphabet");
                        $(span[i]).parent('div').children('input').focus();
                        return false;
                    }
                    else
                    {
                      $(span[i]).html("");
                    }
                }
                else if(i==2)
                {
                    //var priority=parseInt(priority);
                    if(isNaN(priority) || priority==0 || priority<0)
                    {
                        $(span[i]).html("Please enter number greater than 0");
                        $(span[i]).parent('div').children('input').focus();
                        return false;
                    }
                    else
                    {
                      $(span[i]).html("");
                    }
                }
                else
                {
                    $(span[i]).html("");
                }
            }
            $.ajax({
                url: '{{route("root.kitchen.store")}}',
                //url: '{{ url('root/kitchen') }}',  //working
                type: "post",
                dataType:"json",
                data: $('#add-kitchen-form').serialize(),
                success: function(res){
                    if(res.success==1)
                    {
                        document.getElementById("add-kitchen-form").reset();
                        location.reload();
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            //errorsHtml += '<li>' + value + '</li>'; 
                            errorsHtml += value + '<br>'; 
                        });
                        $('#errors').html(errorsHtml);
                    }
                }
            });
        });
        
        $(".modalClose").click(function(){
            $('#kitchen_nameErr,#statusErr,#descriptionErr,#validFrom,#validTo,#priorityErr,#errors').html('');
            document.getElementById("add-kitchen-form").reset();
        });
        $(".modalClose1").click(function(){
            $('#Editkitchen_nameErr,#editStatusErr,#EditdescriptionErr,#Editvalid_fromErr,#Editvalid_toErr,#EditpriorityErr,#edit_errors').html('');
            document.getElementById("edit-kitchen-form").reset();
        });
        
        $("#edit_new_kitchen").unbind("click").click(function(){
            var kitchen_name = $('#edit_name').val().trim();
            var description=$("#edit_description").val().trim();
            var priority=$("#edit_priority").val();
            var status=$("#edit_status").val();
            var valid_from=$("#edit_valid_from").val();
            var valid_to=$("#edit_valid_to").val();
            var arr = [kitchen_name, description,priority,status,valid_from,valid_to];
            var span=[Editkitchen_nameErr,EditdescriptionErr,EditpriorityErr,editStatusErr,Editvalid_fromErr,Editvalid_toErr];
            for(i=0;i<arr.length;i++)
            {
                if(arr[i]=='' || arr[i]==null)
                {
                    $(span[i]).html("This field is required");
                    $(span[i]).parent('div').children('input').focus();
                    return false;
                }
                else if(i==0)
                {
                    var alpha=/^[a-zA-ZäÄöÖüÜß«» -]*$/;
                    var validtext=alpha.test(arr[i]);
                    if(validtext==false)
                    {
                        $(span[i]).html("Please enter only alphabet");
                        $(span[i]).parent('div').children('input').focus();
                        return false;
                    }
                    else
                    {
                      $(span[i]).html("");
                    }
                }
                else if(i==2)
                {
                    //var priority=parseInt(priority);
                    if(isNaN(priority) || priority==0 || priority<0)
                    {
                        $(span[i]).html("Please enter number greater than 0");
                        $(span[i]).parent('div').children('input').focus();
                        return false;
                    }
                    else
                    {
                      $(span[i]).html("");
                    }
                }
                else
                {
                    $(span[i]).html("");
                }
            }
            kitchen_id = $('input[name=kitchen_id]').val();
            $.ajax({
                url: 'kitchens/update/'+kitchen_id,
                type: "post",
                dataType:"json",
                data: $('#edit-kitchen-form').serialize(),
                success: function(res){
                    if(res.success==1)
                    {
                        document.getElementById("edit-kitchen-form").reset();
                        location.reload();
                    }
                    else if(res.success==2)
                    {
                        $('#edit_errors').html(res.error);
                    }
                    else
                    {
                        var errorsHtml= '';
                        $.each(res.errors, function( key, value ) {
                            errorsHtml += value + '<br>'; 
                        });
                        $('#edit_errors').html(errorsHtml);
                    }
                }
            });
        });
    });
	
	
	 /*added by Rajlakshmi(02-06-16)*/
		function download_kitchens(){
        var len = $(".kitchens:checked").length;
        if(len==0){
            alert('Please select atleast one Kitchen');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        
        var r = confirm("Are you sure to download kitchens?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="kitchens"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
           
            $.ajax({
                
                url: 'kitchens/download',  //working
                type: "POST",
                dataType:"json",
                data: {'selectedKitchens':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("Kitchen List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });
					    doc.save("kitchen_list.pdf");
				}
            });
        } else {
            return false;
        }
    }
	/*added by Rajlakshmi(02-06-16)*/
	
		 
</script>
@endsection

@include('superadmin.kitchens.add_kitchen')
@include('superadmin.kitchens.edit_kitchen')




