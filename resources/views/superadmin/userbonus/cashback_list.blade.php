@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         User Cashback Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.userbonus.cashback_list.index')}}"><i class="fa fa-dashboard"></i>Cashback</a></li>
        <li class="active">Cashback list</li>
      </ol>
    </section>



    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Cashback Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
             </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>-->
                    
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
					<th>Customer Name</th>
                    <th>Total Awarded Point</th>
					<th>Total Used Point</th>
					<th>Total Expire Point</th>
					<th>Total Available Point</th>
					</tr>
                </thead>
                <tbody>
                    
                
                @if(count($cpoint))
                    @foreach($cpoint as $cpoints)
                <tr>
                    <td>{{ $cpoints->fname.' '.$cpoints->lname }}</td>
					<?php $apoint = \App\front\UserCashbackPoint::awardedpoint($cpoints->user_id); ?>
					<td>{{ $apoint }}</td>
					 <?php $upoint = \App\front\UserCashbackPoint::usedpoint($cpoints->user_id); ?>
					<td>{{ $upoint }}</td>
					 <?php $epoint = \App\front\UserCashbackPoint::expirepoint($cpoints->user_id); ?>
					<td>{{ $epoint }}</td>
					 <?php $tpoint = \App\front\UserCashbackPoint::availablepoint($cpoints->user_id); ?>
					<td>{{ $tpoint }}</td>
					
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
              {!! $cpoint->render() !!}  
                
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 
 @endsection
@section('script')

@endsection




