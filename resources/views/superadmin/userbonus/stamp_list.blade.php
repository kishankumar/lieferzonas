@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Stamp Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.userbonus.stamp.index')}}"><i class="fa fa-dashboard"></i>Stamps</a></li>
        <li class="active">Stamp list</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Stamp Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
             </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>-->
					 
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
					
                    <th>User Name</th>
					<th>User Email</th>
					<th>User Phone No</th>
					<th>Restaurant Name</th>
					<th>Restaurant Email</th>
					<th>Restaurant Phone No</th>
					<th>Stamp Count</th>
                    </tr>
                </thead>
                <tbody>
                    
                @if(count($stamplist))
                    @foreach($stamplist as $stamplists)
                <tr>
				    
                    <td>{{ $stamplists->fname.' '.$stamplists->lname }}</td>
					<td>{{ $stamplists->uemail}}</td>
					<td>{{ $stamplists->umobile}}</td>
					<td>{{ $stamplists->f_name.' '.$stamplists->l_name }}</td>
					<td>{{ $stamplists->remail}}</td>
					<td>{{ $stamplists->mobile}}</td>
					<td>{{ $stamplists->stamp_count. '/'. $stampcount['0']['stamp_count'] }}</td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
              {!! $stamplist->render() !!}  
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 
 @endsection
@section('script')

@endsection




