@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       New Customer Bonus Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.userbonus.new_customer_bonus.index')}}"><i class="fa fa-dashboard"></i> New Customer Bonus</a></li>
        <li class="active">New Customer Bonus</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">New Customer Bonus Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
            </div>
            
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>-->
					 <a class="btn btn-danger DTTT_button_xls" onclick="delete_order_rate()" id="ToolTables_simpledatatable_2" tabindex="0" aria-controls="simpledatatable">
                         <span>Delete</span>
                     </a>
                    
                    
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
					
                    
					<th>User Name</th>
					<th>Restaurant Name</th>
					<th>Order Id</th>
					<th>Bonus Value</th>
					<th>Date</th>
					
                    </tr>
                </thead>
                <tbody>
                    
                 @if(count($newcustbonus))
                    @foreach($newcustbonus as $custbonus)
                <tr>
				  
                    <td>{{ $custbonus->fname.' '.$custbonus->lname }}</td>
					<td>{{ $custbonus->f_name.' '.$custbonus->l_name }}</td>
					<td>{{ $custbonus->order_id }}</td>
					<td>{{ $custbonus->bonus_value}}</td>
					<td>{{ $custbonus->created_at }}</td>
                </tr>
                
                      @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
              
                </tbody>
                
              </table>
                 {!! $newcustbonus->render() !!}  
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
@section('script')

@endsection




