@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Cashback Point History Table
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.userbonus.cashback_point_history.index')}}"><i class="fa fa-dashboard"></i>Cashback History</a></li>
        <li class="active">Cashback History</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Cashback Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-8 text-left" style="margin-bottom:10px;">
             <select name="users" id="users" class="form-control js-example-basic-multiple" onchange="cashback_history(this.value)";>
			 <option value="">Select Users</option>
                                <?php
                                foreach($fuserlist as $fuserlists) { ?>
                                    <option value="<?=$fuserlists->front_user_id ?>"><?=$fuserlists->fname.' '.$fuserlists->lname ?></option>
                                <?php
                                } ?>
             </select>	   
             </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>-->
                    
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                
            <table id="example1" class="table table-bordered table-striped">
               
                
              </table>
             
                
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 
@endsection
@section('script')
<script>
function cashback_history(user_id)
{
	var user_id = user_id;
	
		$.ajax({
			url: '{{ url('root/cashback_point/history') }}',
			type: "GET",
			dataType:"json",
			data: {'user_id':user_id},
			success: function(res)
			{
				if(res)
				{
					$("#example1").html(res.data);
					
				}
				
			}
            });

	
}
</script>
@endsection




