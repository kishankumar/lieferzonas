@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Favourites
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.userbonus.favourites.index')}}"><i class="fa fa-dashboard"></i>Favourites</a></li>
        <li class="active">Favourites</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Favourites Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                
             </div>
            
            <div class="col-sm-4 text-right" id="asdfg" style="margin-bottom:10px;"> 
                <div class="btn-group">
                    
                     <a  href="javascript:window.print()" class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#"  onclick="download_users()" class="btn btn-default DTTT_button_print" id="cmd" title="View print view" tabindex="0" aria-controls="simpledatatable"  >
                         <span >Download</span>
                     </a>-->
					 
                </div>
            </div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
					<th>Restaurant Name</th>
					<th>Total User</th>
					<th>Action</th>
					</tr>
                </thead>
                <tbody>
                    
                @if(count($favlist))
                    @foreach($favlist as $favlists)
                <tr>
				    
                    <td>{{ $favlists->f_name.' '.$favlists->l_name }}</td>
					<td>{{ $favlists->user_count}}</td>
					
					<td><a href="#" class="btn-border" data-target="#" data-toggle="modal" onclick="show_user(<?=$favlists->rest_id?>);">Detail</a></td>
					
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
              {!! $favlist->render() !!}  
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
  <div aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal_rating" class="modal fade">
	<div class="modal-dialog modal-md robotoregular">
		<div class="modal-content">
			<div style="background:#81c02f; color:#fff;" class="modal-header">
				<button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">User list</h4>
			</div>
			<div class="modal-body">
				
				    <div class="row">
					
						<div class="col-xs-12" id="user_list">
						
						</div>
					
		            </div>
				
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
function show_user(rest_id)
{
	var rest_id = rest_id;
	$.ajax({
			url: '{{ url('showuser') }}',
			type: "GET",
			dataType:"json",
			data: {'rest_id':rest_id},
			success: function(res)
			{
				if(res.success==1)
				{
					$('#myModal_rating').modal('show');
					$("#user_list").html(res.data);
					
				}
				
			}
            });
}
</script>
@endsection




