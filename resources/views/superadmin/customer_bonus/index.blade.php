@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        New Customer Bonus
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		 <li><a href="{{route('root.custbonus.index')}}"><i class="fa fa-dashboard"></i> Customer Bonus</a></li>
        <li class="active">Customer Bonus List</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Customer Bonus With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-12 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" onclick="download_bonus()" title="View print view" tabindex="0" aria-controls="simpledatatable"><span>Download</span></a>
                    </a>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif

            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th> </th>
                        <th>Bonus Value</th>
                        <th>Bonus Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @if(count($bonus))
                    @foreach($bonus as $info)
                <tr>
				   <td> <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('bonus', $info->id, null, ['class' => 'bonus']) !!}
                                <span class=""></span>
                            </label>
                    </div></td>
                    <td>{{ number_format($info->value,2)  }}</td>
                    <td>Percentage</td>
                    <td>
                        @if($info->status == 1) 
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-success" href="javascript:void(0)" onclick="edit_bonus({{$info->id}})">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $bonus->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
	
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
	
    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    function edit_bonus(edit=0){
        $('#valueErr,#statusErr').html('');
        $('#errors').html('');
        $('.error').html('');
        var edit=1;
        if(edit){
            $.ajax({
                url: '{{route("root.custbonus.edit")}}',
                type: "GET",
                dataType:"json",
                data: {'selectedBonus':edit, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        $('#show_status').show();
                        $('#value').val(res.value.toFixed(2));
                        $('#status').val(res.status);
                        $('input[name=bonus_id]').val(res.id);
                        showModal('add-form1');
                    }
                    else
                    {
                        document.getElementById("add-types-form").reset();
                    }
                }
            });
        }
    }
    
    $(function(){
        $.validator.addMethod("check_value", function(value, element) {
            return this.optional(element) || /^[0-9]+([.][0-9]{1,2})?$/.test(value);
        }, "Stamp value not valid.");
        $.validator.addMethod('greater_than_zero', function(value, element, param) {
            return isNaN(value) || this.optional(element) || value > 0;
        }, 'Bonus amount must be greater than 0');
        
        $("#add-types-form").validate({
            rules:
            {
                value:
                {
                    required : true,
                    greater_than_zero : true,
                    check_value: true
                }
            },
            messages: {
                value: {
                    required: "Please enter bonus amount",
                    check_value: "Bonus format not valid."
                },
            },
        });
        
        $("#add_new_category").unbind("click").click(function(){
            if( $("#add-types-form").valid())
            {
                $.ajax({
                    url: '{{ url('root/custbonus') }}',  //working
                    type: "post",
                    dataType:"json",
                    data: $('#add-types-form').serialize(),
                    success: function(res){
                        if(res.success==1)
                        {
                            document.getElementById("add-types-form").reset();
                            location.reload();
                        }
                        else
                        {
                            var errorsHtml= '';
                            $.each(res.errors, function( key, value ) {
                                errorsHtml += value + '<br>'; 
                            });
                            $('#errors').html(errorsHtml);
                        }
                    }
                });
            }
        });
        
        $(".modalClose").click(function(){
            $('#valueErr,#statusErr').html('');
            $('#errors').html('');
            $('.error').html('');
            document.getElementById("add-types-form").reset();
        });
    });
	
	
	function download_bonus() 
	{
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
       };
	    
		var len = $(".bonus:checked").length;
        if(len==0){
            alert('Please select atleast one customer bonus');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        
        var r = confirm("Are you sure to downlod customer bonus?");
        if (r == true) {
            var selectedBonus = new Array();
            $('input:checkbox[name="bonus"]:checked').each(function(){
                selectedBonus.push($(this).val());
            });
			 
            $.ajax({
                url: 'custbonus/download',
                type: "post",
                dataType:"json",
                data: {'selectedBonus':selectedBonus, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
				        doc.save('customerbonus_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
		
	}
    function tableToJson(table) {
		var data = [];

		// first row needs to be headers
		var headers = [];
		for (var i=0; i<table.rows[0].cells.length; i++) {
			headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi,'');
		}

		// go through cells
		for (var i=0; i<table.rows.length; i++) {

			var tableRow = table.rows[i];
			var rowData = {};

			for (var j=0; j<tableRow.cells.length; j++) {

				rowData[ headers[j] ] = tableRow.cells[j].innerHTML;

			}

			data.push(rowData);
		}       

        return data; }
         /*added by Rajlakshmi(31-05-16)*/
</script>
@endsection

@include('superadmin.customer_bonus.add_bonus')





