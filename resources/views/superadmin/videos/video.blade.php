@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Video
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.video.index')}}"><i class="fa fa-dashboard"></i>Video</a></li>
            <li class="active">Video upload</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin Video upload</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="col-sm-12 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                           
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                            <!--<a class="btn btn-default DTTT_button_print" onclick="download_bonus()" id="ToolTables_simpledatatable_4"
                                    title="View print view" tabindex="0"
                                    aria-controls="simpledatatable"><span>Download</span></a>-->
                        </div>
                    </div>

                    <div class="box-body">
					 @if(Session::has('message'))
						<div class="alert alert-success" style="padding: 7px 15px;">
						  {{ Session::get('message') }}
						  {{ Session::put('message','') }}
						</div>
					@endif
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                               
                                <th>Title</th>
								<th>Video</th>
                                
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                             @if(count($videos))
                            @foreach($videos as $video)
                            <tr>
                                    
									
                                    <td>{{$video['video_title']}}</td>
									<td><video  id="showvideo" controls src="{{asset('public/superadmin/uploads/videos/'.$video['video_name']) }}" type="video/mp4">
										  
								    </video> </td>
                                    
									
                                    <td> @if($video['status']==1) <i class="fa fa-check text-success fa-2xkc text-success"></i>  @endif  @if($video['status']==0)  <i class="fa fa-ban text-danger fa-2xkc text-danger"></i> @endif</td>
                                    <td><a  class="btn btn-success" href="#" onclick="editvideo({{$video['id']}})"><i class="fa fa-edit"></i></a></td>
                            </tr>
                            @endforeach
                              @else 
                                <tr>
                                    <td colspan="8" align="center">
                                        No Record Exist
                                    </td>
                                </tr>
                            @endif
                            </tbody>

                        </table>
                        {!! $videos->render() !!}
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<div class="modal fade" id="edit-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Edit Video
            </div>

                <div class="modal-body" style="padding: 5px;">
                    <div class="panel-body">
                        @if(Session::has('errmsg'))
						<div class="alert alert-danger" style="padding: 7px 15px;">
						  {{ Session::get('errmsg') }}
						  {{ Session::put('errmsg','') }}
						</div>
					    @endif
						@if($errors->any())
							<div class="alert alert-danger ">
								<ul class="list-unstyled">
									@foreach($errors->all() as $error)
										<li> {{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
                       
                       {!! Form::model($videos,['route'=>['root.video.update',$video['id']],'method'=>'patch',
                       'id' => 'edit_form','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}


                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Video title</label>
                                       
                                        {!! Form::text('video_title','',['id'=>'video_title','class'=>'form-control','readonly'=>'true']) !!}
                                        {!! Form::hidden('video_id','',['id'=>'video_id','class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Video upload</label>
                                       
										
										 <input type="file" name="upvideo" id="video"  onchange="validatevideo(this)" >
										<video  id="showvideo" controls src="" type="video/mp4">
										</video> 
									
                                    </div>

                                </div>

                               

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="control-label">Status</label>
                                        {!! Form::select('status',['1'=>'Active','0'=>'Deactive'], '', ['id'=>'status','class'=>'form-control']) !!}
                                    </div>

                                </div>

                            </div>

                            <div class="form-group margin-top">
                               
                                {!! Form::submit('Update',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                                <hr>
                                <!--<p><span class="red">*</span> - Required Fields.</p>-->
                            </div>
                        {{--</form>--}}
                        {!! Form::close() !!}
                    </div>

                </div>

        </div>
    </div>
</div>

<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->

@endsection

@section('script')


    <script type="text/javascript">
   
     function editvideo(id)   
     {
        
        var video_id = id;
        $.ajax({
                url: '{{ url('fetchvdatas') }}',
                type: "GET",
                dataType:"json",
                data: {'video_id':video_id},
                success: function(res)
                {
                    if(res.success==1)
                    {
                        
                        $('#video_title').val(res.video_title);
                        var video = res.video;
						
						if(video=='')
						{
							$('#video').hide();
						}
                        var video = base_host+"/lieferzonas/public/superadmin/uploads/videos/"+video;
						//alert(video);
                        $('#name').val(res.name);
						$("#showvideo ").attr("src", video);
                        $('#status').val(res.status);
                        $('input[name=video_id]').val(res.id);
                        showModal('edit-form');
                    }
                    else
                    {
                        //document.getElementById("edit-form").reset();
                    }
                }
            });
     } 


    $(document).ready(function(){
     $("#edit_form").validate({
      rules: {

        points: {
            required: true,
            digits:true
               },
        description: {
            required: true
               },
        
        
        },
       messages: {

                    required: "Please enter bonus points",
                    digits: "Please enter bonus points",
                    description: "Please enter description",
                    
                    agree: "Please accept our policy"
                },
         
     });
});

function validatevideo(fieldObj)
		{
			
			var FileName  = fieldObj.value;
			var FileExt = FileName.substr(FileName.lastIndexOf('.')+1);
			var FileSize = fieldObj.files[0].size;
			var FileSizeMB = (FileSize/5485760).toFixed(2);

		   if ( (FileExt != "avi" && FileExt != "mp4" && FileExt != "mpeg" ) )
			{
				var error = "File type : "+ FileExt+"\n\n";
				error += "Please make sure video is in avi,mp4,mpeg format.\n\n";

				alert(error);
				return false;
			}
			return true;
		}

    </script>

    @if($errors->any())
    <script type="text/javascript">
     showModal('edit-form');
    </script>
   @endif
   
@endsection