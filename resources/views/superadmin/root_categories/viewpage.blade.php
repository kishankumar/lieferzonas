@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Root Category
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.categories.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
        <li class="active">Root Category</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-tags"></i> Root Category Detail</h3>
                    <span class="pull-right"> 
                        <a class="btn btn-primary" href="{{url('root/categories/')}}">Back</a>
                    </span>
                </div>
                <!-- /.box-header -->
                
        @if(count($root_cat))
            @foreach($root_cat as $info)
                    
                <div class="box-body">
                    <div class="row form-horizontal">
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Category Name :</label></div>
                            <div class="col-md-8">{{ ucfirst($info->name)  }}</div>
                        </div>

                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Status :</label></div>
                            <div class="col-md-8">
                                @if($info->status == 1) 
                                    <i class="fa fa-check text-success text-success"></i>
                                @else
                                    <i class="fa fa-ban text-danger text-danger" ></i>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Priority :</label></div>
                            <div class="col-md-8">{{ ($info->priority)  }}</div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Valid From :</label></div>
                            <div class="col-md-8">
                                @if(strtotime($info->valid_from)>0)
                                    {{ date("d M Y", strtotime($info->valid_from))  }}
                                @else
                                    {{ 'None' }}
                                @endif 
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Valid To :</label></div>
                            <div class="col-md-8">
                                @if(strtotime($info->valid_to)>0)
                                    {{ date("d M Y", strtotime($info->valid_to))  }}
                                @else
                                    {{ 'None' }}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="col-md-4"><label>Category Description :</label></div>
                            <div class="col-md-8">{{ $info->description }}  </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
            </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
    </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    
    <!--added by Rajlakshmi(02-06-16)-->
    <div id="download" style="display:none;" >
    </div>
   <!--added by Rajlakshmi(02-06-16)-->


    @endsection

@section('script')

@endsection

@include('superadmin.root_categories.add_categories')





