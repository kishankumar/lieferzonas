<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Welcome to Lieferzonas!</h2>
		
        <table width="700" style="border-collapse:collapse; border: 2px solid #81C02F; font-family:arial; font-size:14px; margin:15px auto;">
  <thead>
  	<tr>
  		<td>
        	<div style="padding:15px; border-bottom: 5px solid #FFEA00; font-size:18px; background:#81C02F"><img src=logo.png alt="" width="200"> </div>
        </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
      	<div style="padding:15px;">
      		<table width="100%" border="0">
              <tbody>
                <tr>
                  <td><h5 style="margin:0; color:#81C02F;">Dear {{ $uname }} </h5></td>
                </tr>
                <tr>
                    <td>
                        <div style="min-height:150px; margin-left:20px; color:#555;">
                          <p style="margin:10px 0; font-size:13px;">{{ $msg }}
						  
                        </div>
                    </td>
                </tr>
                <tr>
                  <td><p style="margin:10px 0; font-size:13px;">Thanks.</p></td>
                </tr>
              </tbody>
            </table>
        </div>
      </td>
    </tr>
  </tbody>
  <tfoot>
	<tr>
  		<td>
        	<table width="100%" style="padding:7px; background:#81C02F; color:#fff; font-size:13px;">
            	<tr>
                	<td>Copyright 2016 lieferzonas.at</td>
                    <td align="right">Powered By: Lieferzonas </td>
                </tr>
            </table>
        </td>
    </tr>
  </tfoot>
</table>
    </body>
</html>

