<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Superadmin | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  {!! Html::style('resources/assets/css/bootstrap.min.css') !!}
  {!! Html::style('resources/assets/css/font-awesome.css') !!}
  {!! Html::style('resources/assets/css/AdminLTE.min.css') !!}
  {!! Html::style('resources/assets/css/flat/blue.css') !!}
  {!! Html::style('resources/assets/css/style.css') !!}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="">
        {!!Html::image('resources/assets/img/logo.png','logo','')!!}
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    @if($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li> {{$error}} </li>
        @endforeach
      </ul>
    </div>
    @endif

      {!! Form::open(['url'=>'auth/login','method'=>'post','id'=>'login_form']) !!}
      
    <!--<form action="{{ action("Auth\AuthController@getLogin") }}" method="POST">-->
        <!--{!! csrf_field() !!}-->
            
      <div class="form-group has-feedback">
        {{--<input type="email" class="form-control" placeholder="Email">--}}
          {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Email']) !!}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        {{--<input type="password" class="form-control" placeholder="Password">--}}
          {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              {{--<input type="checkbox"> <span>Remember Me</span>--}}
                {!! Form::checkbox('remember','',false,['id'=>'remember']) !!}
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          {{--<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>--}}
            {!! Form::submit('Sign In',['id'=>'login','class'=>'btn btn-primary btn-block btn-flat']) !!}
        </div>
        <!-- /.col -->
      </div>
    {!! Form::close() !!}
 
	  
    <!--</form>-->

   <!-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>-->
    <!-- /.social-auth-links -->

<!--    <a href="javascript:void(0);" id="forgotclick">I forgot my password</a><br>-->
    

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

{!! Html::script('resources/assets/admin/js/jQuery-2.2.0.min.js') !!}
{!! Html::script('resources/assets/admin/js/jquery.min.js') !!}
{!! Html::script('resources/assets/admin/js/jquery.validate.js') !!}
{!! Html::script('resources/assets/admin/js/additional-methods.js') !!}

<script>
//    $(function () {
//        $('input').iCheck({
//            checkboxClass: 'icheckbox_square-blue',
//            radioClass: 'iradio_square-blue',
//            increaseArea: '20%' // optional
//        });
//    });
    
    $(document).ready(function(){
        $("#login_form").validate({
            rules: {
                email: {
                    required: true,
                    email:true
                },
                password: {
                    required : true,
                    minlength:3,
                },
            },
            messages: {
                email:{
                    required: "Please enter email",
                    email: "Please enter valid email",
                },
                password: {
                    required: "Please enter password",
                    minlength: "Password must be atleast 3 letters",
                },
            }
        });
    });
</script>
</body>
</html>
