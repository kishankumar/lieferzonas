@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            States
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.state.index')}}"><i class="fa fa-dashboard"></i> States Management</a></li>
            <li class="active">View States</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">


                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Super Admin States</h3>
                    </div>
                    <!-- /.box-header -->
                    
                    <div class="col-sm-8 text-left" style="margin-bottom:10px;">
                        {!! Form::open(array('url'=>"root/state/",'class'=>'form-inline','method'=>'GET')) !!}
                            {!! Form::text('search',$search,["id"=>'search','style'=>'width:50%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                            {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-primary']) !!}
                            <a class="btn btn-primary" href="{{url('root/state/')}}">Reset Search</a>
                        {!! Form::close() !!}
                    </div>

                    <div class="col-sm-4 text-right" style="margin-bottom:10px;">
                        <div class="btn-group">
                            <a class="btn btn-default" data-toggle="modal" data-target="#add-form"
                               data-original-title><span>Add</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                            <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4"
                               title="View print view" tabindex="0"
                               aria-controls="simpledatatable" onclick="download_state()"><span>Download</span></a>
                            <a class="btn btn-danger DTTT_button_xls" id="ToolTables_simpledatatable_2"
                               tabindex="0" aria-controls="simpledatatable" onclick="delete_state()"><span>Delete</span></a>
                        </div>
                    </div>
                    
                    <?php 
                $class='<i class="fa fa-caret-down text-muted"></i>';
                $class1='<i class="fa fa-caret-up text-muted"></i>';
                $class2='<i class="fa fa-sort text-muted"></i>';
            ?>
                    <div class="clearfix" style="margin: 20px 0;"></div>

                    <div class="box-body">
					@if(Session::has('message'))
						<div class="alert alert-success" style="padding: 7px 15px;">
						  {{ Session::get('message') }}
						  {{ Session::put('message','') }}
						</div>
					@endif
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                            <div class="checkbox icheck">
                                <label>
                                {!! Form::checkbox('selectAll','',null,['id'=>'selectAll','class'=>'selectAll','onclick'=>'SelectDeselectAllCheckbox()']) !!}
                                <span class=""></span>
                                </label>
                            </div>
                        </th>
                                <th>
                                    <a href="{{url('root/state/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                                    State Name
                                    <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    Slug
                                </th>
                                <th>
                                    <a href="{{url('root/state/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='descrip' ? 'descrip-desc' :'descrip';?>">
                                    Description
                                    <?php if($type=='descrip'){ echo $class; } elseif($type=='descrip-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>Country Name</th>
                                <th>
                                    <a href="{{url('root/state/')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='created' ? 'created-desc' :'created';?>">
                                    Created At
                                    <?php if($type=='created'){ echo $class; } elseif($type=='created-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <!--<th>Created By</th>-->
                                <th>Status (Click to change)</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($data['state'] as $state)
                                <tr>
                                    <td>
                                        <div class="checkbox icheck">
                                            <label>{!! Form::checkbox('state_check',$state->id,false,['id'=>'check','class'=>'state_check checkBoxClass']) !!} <span> </span></label>
                                        </div>
                                    </td>
                                    <td>{{$state->name}}</td>
                                    <td>{{$state->slug}}</td>
                                    <td>{{ (strlen($state->description) > 50) ? substr($state->description,0,50).'...' : $state->description  }}</td>
                                    <td>{{$state->country['name']}}</td>
                                    <!--<td>{{$state->users['username']}}</td>-->
                                    <td>{{date('d M Y',strtotime($state->created_at))}}</td>
                                    <!--<td>{{$state->users['email']}}</td>-->
                                    <td><?php if($state->status==1){ ?><i class="fa fa-check text-success fa-2xkc text-success" onclick="changeStatus(0,'<?php echo $state->id ?>')"></i><?php } else{ ?><i class="fa fa-ban text-danger fa-2xkc text-danger" onclick="changeStatus(1,'<?php echo $state->id; ?>')"></i><?php } ?></td>
                                    <td><a class="btn btn-primary DTTT_button_csv" href="{{ "state/".$state->id."/edit"}}"><span>Edit</span></a></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {!! $data['state']->appends(Request::except('page'))->render() !!}
                    </div>
                    <!-- /.box-body -->
                </div>

                <!-- quick email widget -->


            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<div class="modal fade" id="add-form" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="panel panel-primary theme-border">
            <div class="panel-heading theme-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="panel-title" id="contactLabel"><span class="glyphicon glyphicon-plus"></span> Add New
                    State.</h4>
            </div>

            <div class="modal-body" style="padding: 5px;">
                <div class="panel-body">
                    @if($errors->any())
                        {!! Html::script('resources/assets/js/jQuery-2.2.0.min.js') !!}
                        <script>
                            $(document).ready(function(){
                                $('#add-form').modal();
                            });
                        </script>
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li> {{$error}} </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{--<form role="form">--}}
                    {!! Form::open(['route'=>'root.state.store','method'=>'post','id'=>'stateform']) !!}


                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">State Name</label><span class="red">*</span>
                                {!! Form::text('name','',['class'=>'form-control required','id'=>'myname']) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <div id="slug"></div>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label">Description</label><span class="red">*</span>
                                {!! Form::textarea('description','',['class'=>'form-control']) !!}
                            </div>

                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label">Select Country</label><span class="red">*</span>
                                {!! Form::select('country',$data['country'], '', ['class'=>'form-control required']) !!}
                            </div>

                        </div>

                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="control-label">Status</label>
                                {!! Form::select('status',['1'=>'Active','0'=>'Inactive'], '1', ['class'=>'form-control']) !!}
                            </div>

                        </div>

                    </div>

                    <div class="form-group margin-top">
                        {{-- <button class="btn  btn-primary" type="button"><i class="fa fa-plus"></i> Add</button>--}}
                        {!! Form::submit('Add State',['id'=>'submit','class'=>'btn  btn-primary']) !!}
                        <hr>
                        <p><span class="red">*</span> - Required Fields.</p>
                    </div>
                    {{--</form>--}}
                    {!! Form::close() !!}
                </div>

            </div>

        </div>
    </div>
</div>
<!--added by Rajlakshmi(31-05-16)-->
    <div id="download" style="display:none;" >
    </div>
<!--added by Rajlakshmi(31-05-16)-->
@endsection

@section('script')

    {!! Html::script('resources/assets/js/jquery.validate.min.js') !!}
    {!! Html::script('resources/assets/js/additional-methods.js') !!}

    <script type="text/javascript">

        $(document).ready(function(){
            $.validator.addMethod("checkSpecialLettersGerman", function(value, element) {
                return this.optional(element) || /^[a-zA-ZäÄöÖüÜß«» -]+$/i.test(value);
            }, "Name must contain only letters.");
            $("#stateform").validate({
                rules: {
                    name: {
                        //lettersonly: true,
                        checkSpecialLettersGerman: true
                    }
                    //description: {
                        //minlength: 5
                    //}
                }
            });
        });

        function delete_state(){
            var len = $(".state_check:checked").length;
            if(len==0){
                alert('Please select atleast one State');
                return false;
            }

            var r = confirm("Are you sure to delete the selected States.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="state_check"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: '{{url('root/state/delete')}}',
                    type: "post",
                    dataType:"json",
                    data: {'selecteStates':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                        if(res.success==1)
                            location.reload();
                        else
                            alert('Error occured.!! Please try after some time.');
                    }
                });
            } else {
                return false;
            }
        }

        function changeStatus(status, id)
        {
            $.ajax({
                type    : "POST",
                url     : '{{url('root/state/changeStatus')}}',
                accepts : 'application/json',
                dataType:"json",
                data    : {
                    id          : id,
                    status      : status,
                    _token      :"{{ csrf_token() }}"
                },
                success: function (data) {
                    if(data.success==1)
                        location.reload();
                    else
                        alert('Error occured.!! Please try after some time.');
                }
            });
        }
        
        $('#myname').keyup(function(){
            var title = $('#myname').val();
            if(title==''){
                $('#slug').html('');
            }
            else{
                $.ajax({
                    type    : "POST",
                    url     : '{{url('root/country/generateSlug')}}',
                    data    : { rawslug:title, _token:"{{ csrf_token() }}" },
                    success: function (data) {
                        $('#slug').html(data);
                    }
                });
            }
        });

        function editSlug(){
            var title = $('#myslug').text();
            $('#slug').html('<div class="form-group"><label for="" class="col-md-3 col-sm-3 col-xs-12 control-label">Slug : </label><div class="col-md-6 col-sm-6 col-xs-12"><input type="text" name="myslug" id="myeditslug" value="'+title+'"> <button class="btn btn-default btn-xs" type="button" onclick="updateSlug()" id="updateslug"> OK </button> <button class="btn-link" type="button" onclick="cancelSlug()"> Cancel </button></div></div><input type="hidden" name="myslug" value="'+title+'" /></div>');
            $('#myeditslug').focus();
        }

        function updateSlug(){
            var title = $('#myeditslug').val();
            $.ajax({
                type    : "POST",
                url     : '{{url('root/country/generateSlug')}}',
                data    : { rawslug : title, _token:"{{ csrf_token() }}" },
                success: function (data) {
                    $('#slug').html(data);
                }
            });
        }

        function cancelSlug(){
            var title = $('#myeditslug').val();
            $('#slug').html('<div class="form-group"><label for="" class="col-md-3 col-sm-3 col-xs-12 control-label">Slug : </label><div class="col-md-6 col-sm-6 col-xs-12"><span id="myslug">'+title+'</span>&nbsp;&nbsp;<button class="btn btn-default btn-xs" type="button" onclick="editSlug()"> Edit </button></div><input type="hidden" name="myslug" value="'+title+'" /></div>');
        }
        
         /*added by Rajlakshmi(31-05-16)*/
        function download_state(){
            var len = $(".state_check:checked").length;
            if(len==0){
                alert('Please select atleast one State');
                return false;
            }

            var r = confirm("Are you sure to download the selected States.?");
            if (r == true) {
                var selectedPages = new Array();
                $('input:checkbox[name="state_check"]:checked').each(function(){
                    selectedPages.push($(this).val());
                });
                $.ajax({
                    url: 'state/download',
                    type: "post",
                    dataType:"json",
                    data: {'selecteStates':selectedPages, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
                     
                        var pdfsize = 'a4';
                        var doc = new jsPDF('1', 'pt',pdfsize);
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						
						var header = function(data) {
						doc.setFontSize(18);
						doc.setTextColor(40);
						doc.setFontStyle('normal');
						//doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 20, 50, 50);
						doc.text("State List", data.settings.margin.left, 50);
					  }

					  doc.autoTable(res.columns, res.data, {

						beforePageContent: header,
						startY: 60,
						drawHeaderRow: function(row, data) {
						  row.height = 46;
						},
						drawHeaderCell: function(cell, data) {
						  doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
						  doc.setFillColor(230);
						  doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
						  doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y, {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  doc.setTextColor(100);
						  var text = data.table.rows[0].cells[data.column.dataKey].text;
						  doc.autoTableText(text, cell.textPos.x, cell.textPos.y + (cell.height / 2), {
							halign: cell.styles.halign,
							valign: cell.styles.valign
						  });
						  return false;
						},
						drawRow: function(row, data) {
						  if (row.index === 0) return false;
						},
						margin: {
						  top: 60
						},
						styles: {
						  overflow: 'linebreak',
						  fontSize: 7,
						  tableWidth: 'auto',
						  columnWidth: 'auto',
						},
						columnStyles: {
						  1: {
							columnWidth: 'auto'
						  }
						},


					  });

                doc.save('State_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
    }
    
    
         
         /*added by Rajlakshmi(31-05-16)*/
    </script>
@endsection