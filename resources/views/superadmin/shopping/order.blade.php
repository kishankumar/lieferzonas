@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shop Orders
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{route('root.shop.orders.index')}}"><i class="fa fa-dashboard"></i> Shop Orders</a></li>
        <li class="active">Orders</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Shop Orders With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="col-sm-10 text-left" style="margin-bottom:10px;">
                {!! Form::open(array('url'=>"root/shop/orders",'class'=>'form-horizontal','method'=>'GET')) !!}
                    <div class="col-md-8 form-group">
                        <label class="col-sm-3 control-label">Restaurant Name</label>
                        <div class="col-sm-4">
                            <select class="form-control js-example-basic-multiple" name="restaurant">
                            <option value="">Select</option>
                            @foreach($restro_names as $restro_name)
                                @if($restro_name->id==$restro_detail_id)
                                    <option value="{{$restro_name->id}}" selected >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                @else
                                    <option value="{{$restro_name->id}}" >{{ucfirst($restro_name->f_name)}} {{ucfirst($restro_name->l_name)}}</option>
                                @endif
                            @endforeach
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-primary" title="">Search</button>
                            <a class="btn btn-primary" href="{{url('root/shop/orders/')}}">Reset Search</a>
                        </div>
                    </div>
                    
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                <div id="delerrors" style="color: red"></div>

            <!--<table id="example1" class="table table-bordered table-striped">-->
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="15%">Restaurant Name</th>
                    <th width="10%">Order</th>
                    <th width="10%">Price</th>
                    <th width="10%">Order Date</th>
                    <th width="20%">Address</th>
                    <th width="10%">Phone No.</th>
                    <!--<th width="17%">Email Id</th>-->
                    <th width="10%">Order Status</th>
                    <th width="15%">Views</th>
                </tr>
            </thead>
            <tbody>
                @if(count($OrderInfo))
                    @foreach($OrderInfo as $info)
                        <?php
                        $shopOrderAddress='';$shopOrderMobileNumber='None';
                        $shopOrderAddress = \App\admin\ShopOrder::get_shop_order_address("$info->order_id"); 
                        $shopOrderMobileNumber = \App\admin\ShopOrder::get_shop_order_number("$info->order_id"); 
                        $shopOrderStatus = \App\admin\ShopOrder::get_shop_order_status($info->shop_order_status_id); 
                        ?>
                        <tr>
                            <td>
                                <a href="{{url('root/shop/orders/'.$info->rest_detail_id)}}">
                                    {{ ucfirst($info->restaurant_address->f_name).' '.ucfirst($info->restaurant_address->l_name)  }}
                                </a>
                            </td>
                            <td>{{ ($info->order_id)  }}</td>
                            <td> &euro; {{ number_format($info->grand_total,2) }} </td>
                            <td>{{ date("d M Y", strtotime($info->created_at))  }}</td>
                            <!--<td>{{ date('l', strtotime($info->created_at)) }} <br>{{ date('d/m/Y', strtotime($info->created_at)) }} </td>-->
                            <td>{{ $shopOrderAddress }} </td>
                            <td>{{$shopOrderMobileNumber}}
<!--                                @foreach($info->mobiles as $mobile)
                                    @if($mobile->is_primary==1)
                                      {{$mobile->mobile}}
                                    @endif
                                @endforeach-->
                            </td>
<!--                            <td>
                                @foreach($info->emails as $email)
                                    @if($email->is_primary==1)
                                        {{$email->email}}
                                    @endif
                                @endforeach
                            </td>-->
                            <td><span class="text-success">{{ $shopOrderStatus }}</span></td>
                            <td>
                                <a href="javascript:void(0)" onclick='show_modal("show-orders-view","order-number","order-item-list","<?=@$info->order_id?>",<?=@$info->id?>,<?=@$info->rest_detail_id;?>)'>View Order
                                </a> 
								<?php if($info->shop_order_status_id!='3' && $info->shop_order_status_id!='4' && $info->shop_order_status_id!='7') {?>
                                <a href="javascript:void(0)" onclick='change_order_status("show-orders-status","order-number-status","order-item-status","<?=@$info->order_id?>",<?=@$info->id?>,<?=@$info->rest_detail_id;?>,<?=@$info->shop_order_status_id?>)'>
                                    /Change Status
                                </a>
								<?php } ?>
                            </td>
                        </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Order Exist
                        </td>
                    </tr>
                 @endif
            </tbody>
        </table>
        {!! $OrderInfo->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
    <div class="modal fade" id="show-orders-view" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-primary theme-border">
                <div class="panel-heading theme-bg">
                    <span id="order-number"></span>
                    <button type="button" id="modalclose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="panel-body"  id="order-item-list">

                </div>
            </div>
        </div>
    </div>
    
    
    <!--//modal for change statuses-->
    <div class="modal fade" id="show-orders-status" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-primary theme-border">
                <div class="panel-heading theme-bg">
                    <span id="order-number-status"></span>
                    <button type="button" id="modalclose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="panel-body"  id="order-item-status">
                    {!! Form::open(array('route' => 'root.shop.orders.store','id'=>'status_form','class' => 'form-horizontal',)) !!}
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('Status') !!}
                                <select class="form-control" name="order_status" id="order_status">
                                    <option value="">Select Status</option>
                                    @if(count($orderStatus))
                                        @foreach($orderStatus as $orderStatuses)
                                            <option value="{{$orderStatuses->id}}" >{{($orderStatuses->status_name)}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span id="statusErr" style="color:red"></span>
                            </div>
                            <div class="form-group">
                                {!! Form::hidden('shopOrderTableId','',['id'=>'shopOrderTableId']) !!}
                                {!! Form::hidden('shopOrderId','',['id'=>'shopOrderId']) !!}
                                {!! Form::button('Submit',["id"=>'add_status','onclick'=>'assign_status()',"class"=>"btn  btn-success"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!--end modal for change status-->

    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    function change_order_status(modelId,orderNumber,orderList,orderId,ordertableId,restId,orderStatusId) {
        $('#statusErr').html('');
        document.getElementById(orderNumber).innerHTML = 'Order : '+orderId;
        $("#order_status").val(orderStatusId);
        $("#shopOrderTableId").val(ordertableId);
        $("#shopOrderId").val(orderId);
	$("#"+modelId).modal('show');
    }
    
    function assign_status(){
        var orderStatus=$("#order_status").val();
        if(orderStatus == 0 || orderStatus=='' || isNaN(orderStatus)){
            $('#statusErr').html("Please select order status");
            return false;
        }
        var shopOrderTableId = $("#shopOrderTableId").val();
        var shopOrderId = $("#shopOrderId").val();
        var formAction = $('#status_form').attr('action');
        $.ajax({
            url: formAction,
            type: "post",
            dataType:"json",
            data: {'shopOrderId':shopOrderId,'shopOrderTableId':shopOrderTableId,'orderStatus':orderStatus, "_token":"{{ csrf_token() }}"},
	    success: function (data) {
                if(data.success==1)
                {
                    location.reload();
                }
                else
                {
                    alert('status not changed');
                }
            }
	});
    }
    
    function show_modal(modelId,orderNumber,orderList,orderId,ordertableId,restId) {
	$.ajax({
            url: '{{ url('root/shop/orders/getdata') }}',
            type: "post",
            data: {'orderId':orderId,'ordertableId':ordertableId,'restId':restId, "_token":"{{ csrf_token() }}"},
	    success: function (data) {
                if(data=='' || data==null){
                    data='No items assigned';
                }
                else{
                    data=data;
                }
                document.getElementById(orderNumber).innerHTML = orderId;
                document.getElementById(orderList).innerHTML = data;
            }
	});
	$("#"+modelId).modal('show');
    }
</script>
@endsection





