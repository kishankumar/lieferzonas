@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurants
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('root.shop.orders.index')}}"><i class="fa fa-dashboard"></i> Shop Orders</a>
                <li class="active">Restaurant</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-lg-12 connectedSortable">

                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-tags"></i> Restaurant Detail</h3>
                            <span class="pull-right">
                        <a class="btn btn-primary" href="javascript:history.go(-1)">Back</a>
                    </span>
                        </div>
                        <!-- /.box-header -->

                        @if(count($viewDetails))
                            @foreach($viewDetails as $info)
                                <div class="box-body">
                                    <div class="row form-horizontal">
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>Restaurant Name :</label></div>
                                            <div class="col-md-8">{{ ucfirst($info->f_name).' '.ucfirst($info->l_name)  }}</div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>Email Id :</label></div>
                                            <div class="col-md-8">
                                                @foreach($info->emails as $email)
                                                    @if($email->is_primary==1)
                                                        {{$email->email}}
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>Mobile :</label></div>
                                            <div class="col-md-8">
                                                @foreach($info->mobiles as $mobile)
                                                    @if($mobile->is_primary==1)
                                                        {{$mobile->mobile}}
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>Address :</label></div>
                                            <div class="col-md-8">{{$info->add1}} {{$info->add2}}    </div>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>Status :</label></div>
                                            <div class="col-md-8">
                                                @if($info->status == 1)
                                                    <i class="fa fa-check text-success text-success"></i>
                                                @else
                                                    <i class="fa fa-ban text-danger text-danger"></i>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>City :</label></div>
                                            <div class="col-md-8">
                                                @if(count($info->city_data))
                                                    {{ ucfirst($info->city_data->name)  }}
                                                @else
                                                    {{ 'Not Given'  }}
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>State :</label></div>
                                            <div class="col-md-8">
                                                @if(count($info->state_data))
                                                    {{ ucfirst($info->state_data->name)  }}
                                                @else
                                                    {{ 'Not Given'  }}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>Country :</label></div>
                                            <div class="col-md-8">
                                                @if(count($info->country_data))
                                                    {{ ucfirst($info->country_data->name)  }}
                                                @else
                                                    {{ 'Not Given'  }}
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <div class="col-md-4"><label>Pin Code :</label></div>
                                            <div class="col-md-8">
                                                @if(count($info->zipcode_data))
                                                    {{ ucfirst($info->zipcode_data->name)  }}
                                                @else
                                                    {{ 'Not Given'  }}
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <button
                                        type="button" class="btn btn-success"
                                        onclick="window.location.href='{{url('root/edit_restaurant_menu/' . $info->id)}}'"
                                        style="margin: 12px;">Restaurant-Men&uuml; bearbeiten</button>
                            @endforeach
                        @endif
                    </div>
                    <!-- quick email widget -->
                </section>
                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!--added by Rajlakshmi(02-06-16)-->
    <div id="download" style="display:none;">
    </div>
    <!--added by Rajlakshmi(02-06-16)-->


@endsection

@section('script')

@endsection





