@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')
            
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
      <h1>
        Restaurant Stamp Card
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{route('root.stamps.index')}}"><i class="fa fa-dashboard"></i> Stamp Card</a></li>
        <li class="active">Stamp Card List</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Stamp Card With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
             <div class="col-sm-12 text-right" style="margin-bottom:10px;">
                <div class="btn-group">
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable" onclick="window.print()"><span>Print</span></a>
                    <a class="btn btn-default DTTT_button_print" id="ToolTables_simpledatatable_4" onclick="download_stamps()" title="View print view" tabindex="0" aria-controls="simpledatatable" ><span>Download</span></a>
                    </a>
                </div>
             </div>
            
            <div class="clearfix"></div>
            
            <div class="box-body">
                
                @if(Session::has('message'))
                            <div class="alert alert-success" style="padding: 7px 15px;">
                                {{ Session::get('message') }}
                                {{ Session::put('message','') }}
                            </div>
                        @endif

            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th> </th>
                        <th>Stamp Count</th>
                        <th>Stamp Value</th>
                        <th>Stamp Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @if(count($stamp))
                    @foreach($stamp as $info)
                <tr>
				     <td>
                        <div class="checkbox icheck">
                            <label>
                                {!! Form::checkbox('stamps', $info->id, null, ['class' => 'stamps']) !!}
                                <span class=""></span>
                            </label>
                        </div>
                    </td>
                    <td>{{ $info->stamp_count  }}</td>
                    <td>{{ number_format($info->value,2)  }}</td>
                    <td>Percentage</td>
                    <td>
                        @if($info->status == 1) 
                            <i class="fa fa-check text-success fa-2xkc text-success"></i>
                        @else
                            <i class="fa fa-ban text-danger fa-2xkc text-danger" ></i>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-success" href="javascript:void(0)" onclick="edit_stamp({{$info->id}})">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
            {!! $stamp->render() !!}
        </div>
            <!-- /.box-body -->
        </div>
          <!-- quick email widget -->
    </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
	<!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
	
    @endsection

@section('script')

<script>
    var base_host = window.location.origin;  //give http://localhost
    var base_url = base_host + SITE_URL_PRE_FIX + '/root/';
    
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    function edit_stamp(edit=0){
        $('#countErr,#statusErr,#valueErr').html('');
        $('#errors').html('');
        $('.error').html('');
        var edit=1;
        if(edit){
            $.ajax({
                url: '{{route("root.stamps.edit")}}',
                type: "GET",
                dataType:"json",
                data: {'selectedStamp':edit, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        $('#show_status').show();
                        $('#stamp_count').val(res.stamp_count);
                        $('#status').val(res.status);
                        $('#value').val(res.value.toFixed(2));
                        $('input[name=stamp_id]').val(res.id);
                        showModal('add-form1');
                    }
                    else
                    {
                        document.getElementById("add-types-form").reset();
                    }
                }
            });
        }
    }
    
    $(function(){
        $.validator.addMethod("check_value", function(value, element) {
            return this.optional(element) || /^[0-9]+([.][0-9]{1,2})?$/.test(value);
        }, "Stamp value not valid.");
        $.validator.addMethod('greater_than_zero', function(value, element, param) {
            return isNaN(value) || this.optional(element) || value > 0;
        }, 'Stamp count must be greater than 0');
        
        $.validator.addMethod('greater_than_zero_value', function(value, element, param) {
            return isNaN(value) || this.optional(element) || value > 0;
        }, 'Stamp value must be greater than 0');
        
        $("#add-types-form").validate({
            rules:
            {
                stamp_count:
                {
                    required : true,
                    digits:true,
                    greater_than_zero: true
                },
                value:
                {
                    required : true,
                    greater_than_zero_value: true,
                    check_value: true
                }
            },
            messages: {
                value: {
                    required: "Please enter stamp value",
                    check_value: "Stamp value format not valid."
                },
            },
        });
        
        $("#add_new_category").unbind("click").click(function(){
            if( $("#add-types-form").valid())
            {
                $.ajax({
                    url: '{{ url('root/stamps') }}',  //working
                    type: "post",
                    dataType:"json",
                    data: $('#add-types-form').serialize(),
                    success: function(res){
                        if(res.success==1)
                        {
                            document.getElementById("add-types-form").reset();
                            location.reload();
                        }
                        else
                        {
                            var errorsHtml= '';
                            $.each(res.errors, function( key, value ) {
                                errorsHtml += value + '<br>'; 
                            });
                            $('#errors').html(errorsHtml);
                        }
                    }
                });
            }
        });
        
        $(".modalClose").click(function(){
            $('#countErr,#statusErr,#valueErr').html('');
            $('#errors').html('');
            $('.error').html('');
            document.getElementById("add-types-form").reset();
        });
    });
	
	/*added by Rajlakshmi(01-06-16)*/					

	function download_stamps(){
        var len = $(".stamps:checked").length;
        if(len==0){
            alert('Please select atleast one stamp');
            return false;
        }
        var base_url1=base_url+'stamps/download';
        var r = confirm("Are you sure to download stamps?");
        if (r == true) {
            var selectedPages = new Array();
            $('input:checkbox[name="stamps"]:checked').each(function(){
                selectedPages.push($(this).val());
            });
            $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'selectedStamps':selectedPages, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);
					    doc.save("stamp_list.pdf");
				}
				
            });
        } else {
            return false;
        }
		
	}
  
         /*added by Rajlakshmi(01-06-16)*/
</script>
@endsection

@include('superadmin.stamps.add_stamps')





