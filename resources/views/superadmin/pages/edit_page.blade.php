@extends('layouts.superadminbody')

@section('title')
@endsection

@section('body')


<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Management
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="{{url('root/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('root.pages.index')}}"><i class="fa fa-dashboard"></i> Restaurant Settings</a></li>
            <li class="active">Edit Pages Management</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
        
          <!-- Custom tabs (Charts with tabs)-->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Edit pages With Full Features</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
    
    {!! Form::model($pages,['route'=>['root.pages.update',$pages->id],'id' => 'edit-page','method'=>'patch','class' => 'form'])	!!}
    
        
            <div class="">
                      
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Page Name') !!}<span class="red">*</span>
                            {!! Form::text('page_name',$pages->page_name,["id"=>'page_name','placeholder'=>'Enter Page Name',"class"=>"form-control"]) !!}
                            <span id="page_nameErr" style="color:red"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Status') !!}
                            {!! Form::select('status', 
                            array(
                            '1'=>'Active',
                            '0'=>'Deactive',
                            ), $pages->status, ['id' => 'status','class' => 'form-control']) !!}
                            <span id="statusErr" style="color:red"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                           {!! Form::label('Page Description') !!}  <span id="descriptionErr" style="color:red"></span><span class="red">*</span>
                            {!! Form :: textarea('description',$pages->description,['style'=>'height:260px;','id'=>'description','placeholder'=>'Enter Description ','class'=>'form-control'])  !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                
                <div class="form-group margin-top">
                    {!! Form::button('Update Page',["id"=>'edit_new_page',"class"=>"btn  btn-primary"]) !!}
                    <hr>
					<p><span class="red">*</span> - Required Fields.</p>
                </div>
            </div>
        
            {!! Form::close() !!}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- quick email widget -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
				
    </div>

    @endsection
    </body>
</html>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>
    $(function(){
        $("#edit_new_page").click(function(){
            var page_name = $('#page_name').val().trim();
            var description=$("#description").val().trim();
            var status=$("#status").val();
            var arr = [page_name, description,status];
            var span=[page_nameErr,descriptionErr,,statusErr];
            for(i=0;i<arr.length;i++)
            {
                if(arr[i]=='' || arr[i]==null)
                {
                    $(span[i]).html("This field is required");
                    $(span[i]).parent('div').children('input').focus();
                    return false;
                }
                else if(i==0)
                {
                    var alpha=/^[a-zA-ZäÄöÖüÜß«» -]*$/;
                    var validtext=alpha.test(arr[i]);
                    if(validtext==false)
                    {
                        $(span[i]).html("Please enter only alphabet");
                        $(span[i]).parent('div').children('input').focus();
                        return false;
                    }
                    else
                    {
                      $(span[i]).html("");
                    }
                }
                else
                {
                    $(span[i]).html("");
                }
            }
            $('#edit-page').submit();
        });
    });
</script>