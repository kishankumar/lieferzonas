<html>
<head>
    <title>
        @yield('title')
    </title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- JQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- JQuery Validate -->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
    <!-- JQuery Validate Bootstrap Tooltip -->
    <script src="http://thrilleratplay.github.io/jquery-validation-bootstrap-tooltip/js/jquery-validate.bootstrap-tooltip.js"></script>

    <style>

        /* General */

        html, body {
            height: 100%;
            padding: 0px;
        }

        body {
            padding-top: 50px;
            font-family: "Roboto";
            background-color: #f6f6f6;
        }

        .lie-wrap {
            min-height: 100%;
        }

        .lie-main {
            overflow: auto;
            padding-bottom: 260px;
        }

        /* ------- */

        /* Messages */

        .lie-floating-messages {
            position: absolute;
            top: 65px;
            right: 15px;
        }

        .close {
            margin-right: 15px;
        }

        /* -------- */

        /* Footer */

        #lie-bottom-sticky {
            position: relative;
            margin-top: -178px;
            height: 178px;
            clear: both;
        }

        #lie-footer * {
            color: white;
        }

        #lie-footer-top-outer-container {
            width: 100%;
            background-color: #FFFFFF;
            text-align: center;
        }

        #lie-footer-top-inner-container {
            display: inline-block;
            min-width: 1024px;
            width: 80%;
        }

        #lie-footer-top {
            padding: 50px 15px;
            display: flex;
            justify-content: space-between;
        }

        #lie-footer-top > a {
            color: #27ae60;
            font-weight: 700;
            margin-left: 15px;
        }

        #lie-footer-outer-container {
            width: 100%;
            background-color: #27ae60;
            text-align: center;
        }

        #lie-footer-inner-container {
            display: inline-block;
            min-width: 1024px;
            width: 80%;
        }

        #lie-footer {
            padding: 15px;
            text-align: center;
            display: flex;
            justify-content: space-between;
        }

        #lie-footer > span {
            margin-left: 15px;
        }

        .lie-sm-icon {
            background-color: inherit;
            border-radius: 20px;
            border: 2px solid white;
            transition: background-color 0.5s;
            font-size: 14px;
            width: 30px;
            padding: 7px 0px;
            text-align: center;
        }

        .lie-sm-icon:hover {
            background-color: #177e40;
        }

        /* ------ */

        /* Navbar */

        .lie-navbar {
            background-color: #27ae60;
            border: none;
            border-radius: 0px;
            margin-bottom: 0px;
            overflow: visible !important;
            z-index: 1;
            transition: all 0.3s;
        }

        .lie-navbar .navbar-brand {
            overflow: visible !important;
        }

        .lie-navbar a,
        .lie-navbar a:link,
        .lie-navbar a:visited,
        .lie-navbar a:hover,
        .lie-navbar a:active {
            color: white;
        }

        .lie-navbar-wrapper-outer {
            overflow-y: visible;
        }

        .lie-navbar-link {
            font-family: Harabara-Bold;
            font-weight: bolder;
            letter-spacing: 1px;
        }

        .lie-navbar-scrolled {
            background-color: #f6f6f6;
            box-shadow: 0px 2px 7px 1px rgba(0, 0, 0, 0.5);
        }

        .lie-navbar-scrolled a,
        .lie-navbar-scrolled a:link,
        .lie-navbar-scrolled a:visited,
        .lie-navbar-scrolled a:hover,
        .lie-navbar-scrolled a:active {
            color: #444f74;
        }

        .open .dropdown-toggle {
            background-color: #47Ce80;
        }

        .dropdown-menu a,
        .dropdown-menu a:link,
        .dropdown-menu a:visited,
        .dropdown-menu a:hover,
        .dropdown-menu a:active {
            color: black;
        }

        .navbar-brand > img {
            display: inline;
        }

        .lie-navbar-brand-logo {
            margin-top: -15px;
            height: 80px;
        }

        .lie-navbar-user-name {
            font-family: Harabara-Bold;
            font-weight: bolder;
            font-size: 16px;
            letter-spacing: 2px;
        }

        .lie-navbar-user-profile-pic {
            display: inline-block;
            position: relative;
        }

        .lie-navbar-user-profile-pic-background {
            position: absolute;
            bottom: -15px;
            z-index: -1;
            left: -10px;
            width: 45px;
            height: 52px;
            transition: opacity 0.3s;
        }

        .lie-navbar-user-profile-pic-background.scrolled {
            opacity: 0;
        }

        .lie-navbar-brand-text {
            font-family: Harabara-Bold;
            font-size: 22px;
        }

        /* ------ */

        /* Register */

        .lie-register-page-button {
            background-color: #da4f4a;
            color: white;
            text-align: center;
            border: none;
            padding: 15px 20px;
        }

        .lie-register-page-input {
            width: 100%;
            height: 45px;
            margin-bottom: 10px;
            padding-left: 8px;
        }

        /* -------- */

        /* Navbar Dropdown */

        .lie-navbar-dropdown-header {
            padding: 15px 0px;
            text-align: center;
            width: 240px;
            border-bottom: 1px solid lightgrey;
        }

        .lie-navbar-dropdown-header-image {
            display: inline-block;
        }

        .lie-navbar-dropdown-header-text {
            font-size: 18px;
            font-weight: bolder;
        }

        .lie-user-dropdown-links-container {

        }

        .lie-user-dropdown-link {
            display: block;
            padding: 7px 15px;
            transition: all 0.3s;
            border-left: 3px solid transparent;
            font-weight: bold;
        }

        .lie-user-dropdown-link:link,
        .lie-user-dropdown-link:visited,
        .lie-user-dropdown-link:hover,
        .lie-user-dropdown-link:active {
            color: #54657e;
        }

        .lie-user-dropdown-link:hover {
            border-left: 3px solid #27ae60;
            background-color: lightgrey;
            text-decoration: none;
        }

        .lie-user-dropdown-link-icon {
            width: 36px;
            padding-right: 10px;
        }

        .lie-user-dropdown-link-empty-icon {
            display: inline-block;
            width: 36px;
            height: 24px;
            vertical-align: middle;
            padding-right: 10px;
        }

        .lie-user-dropdown-footer {
            border-top: 1px solid lightgrey;
        }

        .lie-user-dropdown-footer-cell {
            padding: 6px;
            text-align: center;
            color: grey;
            cursor: pointer;
            transition: all 0.3s;
        }

        .lie-user-dropdown-footer-cell:hover {
            color: lightgrey;
        }

        .lie-login-body {
            padding: 0px;
            text-align: center;
        }

        .lie-login-body-input {
            display: block;
            margin-bottom: 5px;
            width: 100%;
            border: 1px solid lightgrey;
            border-left: 3px solid transparent;
            border-right: 3px solid transparent;
            font-size: 16px;
            font-weight: lighter;
            padding: 8px 15px;
            transition: all 0.3s;
        }

        .lie-login-body-input:focus {
            border-left: 3px solid #27ae60;
            outline: none;
        }

        .lie-login-button-container {
            text-align: center;
            margin-top: 20px;
        }

        .lie-login-button {
            background-color: #27ae60;
            font-size: 18px;
            color: white;
            border: none;
            padding: 7px 15px;
        }

        .lie-login-forgot-container {
            text-align: right;
            width: 100%;
            padding: 10px;
        }

        .lie-login-forgot-container a,
        .lie-login-forgot-container a:link,
        .lie-login-forgot-container a:visited,
        .lie-login-forgot-container a:hover,
        .lie-login-forgot-container a:active {
            color: #27ae60;
        }

        .lie-login-social-text {
            font-family: Harabara-Bold;
            font-size: 18px;
            color: #444f74;
            padding-bottom: 5px;
        }

        .lie-login-social-button {
            display: block;
            text-align: center;
            border: none;
            color: white;
            width: 100%;
            padding: 10px;
            font-family: Harabara-Bold;
            font-size: 18px;
        }

        .lie-login-social-button.facebook {
            background-color: #3b5998;
        }

        .lie-login-social-button.twitter {
            background-color: #55acee;
        }

        .lie-login-social-button.google-plus {
            background-color: #dd4b39;
        }

        /* -------------- */

    </style>


    {!! Html::style('public/assets/css/lieferzonas_commons.css') !!}


    @yield('head')

</head>
<body>
<div class="lie-wrap">
    <div class="lie-main">
        <!-- Navigation Bar -->
        <nav class="navbar lie-navbar navbar-fixed-top" role="navigation">
            <div class="lie-navbar-wrapper-outer">
                <div class="" style="white-space: nowrap; text-align: center; width: 100%;">
                    <div class="lie-contents" style="margin: 0px; white-space: nowrap">

                        <div class="navbar-header">
                            <a class="navbar-brand" href="{{ url("/") }}">
                                {!! Html::image('/public/front/logo_v3.png', '', array('class' => 'lie-navbar-brand-logo')) !!}

                            </a>
                            <a class="navbar-brand" href="{{ url("/") }}">
                            <span class="lie-navbar-brand-text"> <span
                                        style="font-family: Harabara-Bold;">Lieferzonas</span>.<span
                                        style="font-family: Roboto-Thin">at</span> </span>
                            </a>
                        </div>

                        <?php
                        $logged_in = \Illuminate\Support\Facades\Auth::check('front');
                        $user = \Illuminate\Support\Facades\Auth::user('front');
                        $user_detail = \App\front\FrontUserDetail::userdetail();
                        $profile_pic = \App\front\FrontUserDetail::getActiveUserProfilePictureForDisplay();
                        ?>

                        <ul class="nav navbar-nav navbar-right" style="width:auto;white-space: nowrap;">
                            @if ($logged_in)
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">
                                        <span class="lie-navbar-user-name">{{ $user_detail->fname }}</span>
                                        &nbsp; &nbsp;
                                        <div class="lie-navbar-user-profile-pic">
                                            {!! Html::image('/public/front/navbar_profile_pic_background.png', '', array('class' => 'lie-navbar-user-profile-pic-background')) !!}

                                            <img
                                                    src="{{ $profile_pic }}"
                                                    class="img-circle"
                                                    width="24"
                                                    height="24">
                                        </div>
                                        &nbsp; &nbsp;
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <div class="lie-navbar-dropdown-header">
                                            <img class="lie-navbar-dropdown-header-image img-circle"
                                                 src="{{ $profile_pic }}" width="80" height="80">
                                            <br>
                                            <br>
                                            <nowrap>
                                            <span class="lie-navbar-dropdown-header-text">
                                                {{ $user_detail->fname . " " . $user_detail->lname }}
                                            </span>
                                            </nowrap>
                                        </div>
                                        <div class="lie-user-dropdown-links-container">
                                            <a class="lie-user-dropdown-link" href="{{url('front/dashboard')}}">
                                                {!! Html::image('/public/front/user_dropdown_links/dashboard.png', '', array('class' => 'lie-user-dropdown-link-icon')) !!}

                                                Dashboard
                                            </a>
                                            <a class="lie-user-dropdown-link" href="{{url('front/order')}}">
                                                {!! Html::image('/public/front/user_dropdown_links/orders.png', '', array('class' => 'lie-user-dropdown-link-icon')) !!}

                                                Bestellungen
                                            </a>
                                            <a class="lie-user-dropdown-link" href="{{url('front/orderrate')}}">
                                                {!! Html::image('/public/front/user_dropdown_links/reviews.png', '', array('class' => 'lie-user-dropdown-link-icon')) !!}

                                                Bewertungen
                                            </a>
                                            <a class="lie-user-dropdown-link" href="{{url('front/favourite')}}">
                                                {!! Html::image('/public/front/user_dropdown_links/favorites.png', '', array('class' => 'lie-user-dropdown-link-icon')) !!}

                                                Favoriten
                                            </a>
                                            <a class="lie-user-dropdown-link" href="{{url('front/stamp')}}">
                                                {!! Html::image('/public/front/user_dropdown_links/stamps.png', '', array('class' => 'lie-user-dropdown-link-icon')) !!}

                                                Stempelkarten
                                            </a>
                                            <a class="lie-user-dropdown-link" href="{{url('front/bonuspoint')}}">
                                                {!! Html::image('/public/front/user_dropdown_links/bonus.png', '', array('class' => 'lie-user-dropdown-link-icon')) !!}

                                                Treuepunkte
                                            </a>
                                            <a class="lie-user-dropdown-link" href="{{url('front/cashbackpoint')}}">
                                                {!! Html::image('/public/front/user_dropdown_links/cashback.png', '', array('class' => 'lie-user-dropdown-link-icon')) !!}

                                                Cashbackpunkte
                                            </a>
                                            <a class="lie-user-dropdown-link" href="{{url('front/help')}}">
                                                <div class="lie-user-dropdown-link-empty-icon"></div>
                                                Hilfe
                                            </a>
                                            <a class="lie-user-dropdown-link" href="javascript:void(0);" onclick="showInviteModal();">
                                                <div class="lie-user-dropdown-link-empty-icon"></div>
                                                Freunde einladen
                                            </a>
                                        <!--
                                        <a class="lie-user-dropdown-link" href="/front/profile/{{ $user->id }}/edit">
                                            Meine Daten
                                        </a>
                                        -->
                                            <!--
                                            <a class="lie-user-dropdown-link" href="/front/address">
                                                Zustelladressen
                                            </a>
                                            -->
                                        </div>
                                        <div class="lie-user-dropdown-footer">
                                            <table width="100%">
                                                <tbody>
                                                <tr>
                                                    <td class="lie-user-dropdown-footer-cell"
                                                        onclick="window.location.href='{{ url('frontuser/logout') }}'">
                                                        <i class="fa fa-2x fa-sign-out"></i>
                                                    </td>
                                                    <td class="lie-user-dropdown-footer-cell"
                                                        onclick="window.location.href='{{ url('front/settings') }}'"
                                                        style="border-left: 1px solid lightgrey">
                                                        <i class="fa fa-2x fa-cog"></i>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </ul>
                                </li>
                            @else
                                <li><a class="lie-navbar-link" href="javascript:void(0)" data-toggle="modal"
                                       data-target="#lie-register-modal">REGISTRIEREN</a></li>
                                <!-- Einloggen Dropdown -->
                                <li class="dropdown">
                                    <a class="lie-navbar-link" href="#" class="dropdown-toggle" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        EINLOGGEN
                                    </a>
                                    <ul class="dropdown-menu">
                                        <form action="{{ url("/frontuser/login") }}" method="POST" name="login">
                                            {!! csrf_field() !!}
                                            <li>
                                                <div class="lie-navbar-dropdown-header">
                                                    <img class="lie-navbar-dropdown-header-image img-circle"
                                                         src="public/uploads/front/users/default.png" width="80" height="80">
                                                    <br>
                                                    <br>
                                                    <span class="lie-navbar-dropdown-header-text">Einloggen</span>
                                                </div>
                                                <hr style="margin: 5px 0px;">
                                                <div class="lie-login-body">
                                                    <br>
                                                    Gib bitte deine Zugangsdaten ein!<br>
                                                    <br>
                                                    <input type="text" name="email" class="lie-login-body-input"
                                                           placeholder="E-Mail">
                                                    <input type="password" name="password" class="lie-login-body-input"
                                                           placeholder="Password">
                                                    <div class="lie-login-forgot-container"
                                                         style="text-align: right; width: 100%">
                                                        <a href="/forgot">Passwort vergessen?</a>
                                                    </div>
                                                    {!! lie_general_checkbox("remember", "Auf diesem Computer merken!") !!}
                                                    <div class="lie-login-button-container">
                                                        <button type="submit" class="lie-login-button">Einloggen</button>
                                                    </div>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <hr>
                                                            </td>
                                                            <td width="1%">oder</td>
                                                            <td>
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="lie-login-social-text">Social Login</div>
                                                    <button class="lie-login-social-button facebook">
                                                    <span class="pull-left">
                                                        <i class="fa fa-facebook" style="vertical-align: middle;"></i>
                                                    </span>
                                                        Facebook
                                                    </button>
                                                    <button class="lie-login-social-button twitter">
                                                    <span class="pull-left">
                                                        <i class="fa fa-twitter" style="vertical-align: middle;"></i>
                                                    </span>
                                                        Twitter
                                                    </button>
                                                    <button class="lie-login-social-button google-plus">
                                                    <span class="pull-left">
                                                        <i class="fa fa-google-plus"
                                                           style="vertical-align: middle;"></i>
                                                    </span>
                                                        Google+
                                                    </button>
                                                </div>
                                            </li>
                                        </form>
                                    </ul>
                                </li>
                                <!-- ------------------ -->
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="lie-contents-wrapper">
            @yield('contents')
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="lie-invite-friends-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lade deine Freunde ein!</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('url' => 'front/send_invite/sms', 'method' => 'post')) !!}
                <input type="text" class="form-control" placeholder="Handynummer" name="number">
                <br>
                <button type="submit" class="btn btn-success btn-block">SMS Senden</button>
                <br>
                {!! Form::close() !!}
                <br>
                {!! Form::open(array('url' => 'front/send_invite/email', 'method' => 'post')) !!}
                <input type="text" class="form-control" placeholder="E-Mail Adresse" name="address">
                <br>
                <button type="submit" class="btn btn-success btn-block">E-Mail Senden</button>
                <br>
                {!! Form::close() !!}

                <br>
                <button type="button" onclick="sendRequestViaMultiFriendSelector(); return false;" class="btn btn-success btn-block">Facebook</button>
            </div>
        </div>
    </div>
</div>

<div id="lie-bottom-sticky">
    <div id="lie-footer-top-outer-container">
        <div id="lie-footer-top-inner-container">
            <div id="lie-footer-top">
                <a href="#">Jobs</a>
                <a href="#">
                    <nobr>Affiliate-Programm</nobr>
                </a>
                <a href="#">AGB</a>
                <a href="#">Datenschutz</a>
                <a href="#">Impressum</a>
                <a href="#">Presse</a>
                <a href="#">Partner</a>
                <a href="#">Hilfe</a>
                <a href="#">
                    <nobr>Restaurant hinzufügen</nobr>
                </a>
            </div>
        </div>
    </div>

    <div id="lie-footer-outer-container">
        <div id="lie-footer-inner-container">
            <div id="lie-footer">
                <span><a href="#"><nobr>&copy; 2017 Lieferzonas.at</nobr></a></span>
                <span><a href="#"><nobr>Sitemap</nobr></a></span>
                <span><a href="#"><nobr>Mobile Version</nobr></a></span>
                <span>
                    <nobr>
                        <a href="https://www.instagram.com/lieferzonas.at/"><i class="fa fa-instagram fa-lg lie-sm-icon"></i></a>
                        <a href="https://www.linkedin.com/company/lieferzonas-at"><i
                                    class="fa fa-linkedin fa-lg lie-sm-icon"></i></a>
                        <a href="https://www.facebook.com/Lieferzonas.at/"><i class="fa fa-facebook fa-lg lie-sm-icon"></i></a>
                        <a href="https://twitter.com/lieferzonasat"><i class="fa fa-twitter fa-lg lie-sm-icon"></i></a>
                        <a href="https://plus.google.com/+Lieferzonas"><i class="fa fa-google-plus fa-lg lie-sm-icon"></i></a>
                        <a href="https://www.youtube.com/channel/UC_cA0SOKAgMD_h0yD5GqjuA"><i class="fa fa-youtube fa-lg lie-sm-icon"></i></a>
                        <a href="https://www.xing.com/companies/lieferzonas.at"><i class="fa fa-xing fa-lg lie-sm-icon"></i></a>
                        <a href="https://www.pinterest.com/lieferzonasat/"><i class="fa fa-pinterest fa-lg lie-sm-icon"></i></a>
                        <a href="#"><i class="fa fa-envelope fa-lg lie-sm-icon"></i></a>
                    </nobr>
                </span>
            </div>
        </div>
    </div>
</div>

@include("front_new.includes.register_modal")

<div class="lie-floating-messages">
    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" style="padding: 7px 15px; margin-bottom: 0px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}
            {{ Session::put('message','') }}
        </div>
    @endif
    @if(Session::has('errmessage'))
        <div class="alert alert-danger alert-dismissible" style="padding: 7px 15px; margin-bottom: 0px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('errmessage') }}
            {{ Session::put('errmessage','') }}
        </div>
    @endif
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible" style="padding: 7px 15px; margin-bottom: 0px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $error }}
            </div>
        @endforeach
    @endif
</div>

{!! Html::script('public/assets/js/lieferzonas_commons.js') !!}
@yield('scripts')

<script src="http://connect.facebook.net/en_US/all.js"></script>

<script>
    FB.init({
        appId  : '142207662866810',
        frictionlessRequests: true
    });

    function sendRequestViaMultiFriendSelector() {
        FB.ui({
            method: 'send',
            link: 'http://lieferzonas.at'
        }, requestCallback);
    }

    function requestCallback(response) {
        // Handle callback here
    }
</script>

<!-- Cart Management -->
<script>

    function showInviteModal() {
        $("#lie-invite-friends-modal").modal("show");
    }

    function clearCart(restaurantId, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "clear"
            },
            success: function (response) {
                if (success) success(response);
            },
            error: function (xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }

    function addToCartItemCount(restaurantId, itemIndex, amount, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "add_count",
                "item_index": itemIndex,
                "amount": amount
            },
            success: function (response) {
                if (success) success(response);
            },
            error: function (xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }

    function setCartItemCount(restaurantId, itemIndex, amount, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "set_count",
                "item_index": itemIndex,
                "amount": amount
            },
            success: function (response) {
                if (success) success(response);
            },
            error: function (xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }

    function removeCartItem(restaurantId, itemIndex, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "remove",
                "item_index": itemIndex
            },
            success: function (response) {
                if (success) success(response);
            },
            error: function (xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }

    function addCartItem(restaurantId, subMenu, extras, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "add",
                "submenu": subMenu,
                "extras": extras
            },
            success: function (response) {
                if (success) success(response);
            },
            error: function (xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }

    function getCartJson(restaurantId, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "get",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}'
            },
            success: function (response) {
                if (success) success(response);
            },
            error: function (xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }

    $(window).on("scroll", function () {
        if ($(window).scrollTop() > 50) {
            $(".lie-navbar").addClass("lie-navbar-scrolled");
            $(".lie-navbar-logo-background").children().addClass("scrolled");
            $(".lie-navbar-user-profile-pic-background").addClass("scrolled");
        } else {
            $(".lie-navbar").removeClass("lie-navbar-scrolled");
            $(".lie-navbar-logo-background").children().removeClass("scrolled");
            $(".lie-navbar-user-profile-pic-background").removeClass("scrolled");
        }
    });

    $(function () {
        $("form[name='login']").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email: {
                    required: "Bitte gib deine E-Mail Adresse an",
                    email: "Keine gültige E-Mail"
                },
                password: "Bitte gib dein Passwort ein"
            },
            submitHandler: function (form) {
                form.submit();
            },
            tooltip_options: {
                email: {placement: 'left'},
                password: {placement: 'left'},
            }
        });
    });

</script>

</body>
</html>
