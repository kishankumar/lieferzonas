@extends('front_new.layout')

@section('title')
    Lieferzonas - Login
@endsection

@section('head')

@endsection

@section('contents')
    {!! Form::open(['url'=>'frontuser/login','method'=>'post']) !!}
    {!! Form::text('email','',['class'=>'form-control','placeholder'=>'E-Mail']) !!}
    {!! Form::password('password',['class'=>'form-control','placeholder'=>'Passwort']) !!}
    <div style="text-align: center">
        {!! Form::submit('Login',['id'=>'login','class'=>'btn btn-success']) !!}
        <a href="#">Passwort vergessen?</a>
        <a href="#">Kundendienst</a>
    </div>
    {!! Form::close() !!}
@endsection

@section('scripts')

@endsection