@extends("front_new.layout")

@section("title")
    Lieferzonas - User Einstellungen
@endsection

@section("head")
<style>

    .lie-settings-popout-button {
        display: block;
        background-color: white;
        height: auto;
        width: 100%;
        padding: 20px 80px;
        border: none;
        text-align: left;
        border-bottom: 2px solid lightgrey;
        outline: none;
    }

    .lie-settings-input-text {
        border-radius: 2px;
        border: 1px solid #47Ce80;
        padding: 2px;
        margin-bottom: 16px;
        width: 100%;
    }

    .lie-settings-container {
        padding: 20px 0px;
    }

    .lie-settings-select {
        border-radius: 2px;
        border: 1px solid #47Ce80;
        background-color: white;
        outline: none;
        margin-bottom: 16px;
        width: 100%;
    }

    .lie-settings-gender-button {
        border-radius: 2px;
        border: 1px solid #47Ce80;
        background-color: white;
        outline: none;
        margin-bottom: 16px;
        transition: background-color 0.5s;
    }

    .lie-settings-gender-button.selected {
        background-color: beige;
    }

    .fa-male {
        color: #47Ce80;
    }

    .fa-female {
        color: hotpink;
    }

    .lie-settings-save-button {
        background-color: #47Ce80;
        color: white;
        font-weight: bolder;
        border: none;
        border-radius: 2px;
        padding: 6px 15px;
        outline: none;
        transition: all 0.5s;
        margin-bottom: 25px;
    }

    .lie-settings-save-button:hover {
        background-color: #67EeA0;
    }

    .lie-profile-title {
        font-size: 30px;
        font-weight: normal;
    }

    .lie-profile-subtitle {
        padding-left: 15px;
        font-size: 16px;
    }

    .lie-verification-header {
        font-size: 18px;
        color: #47Ce80;
    }

    .lie-notification-toggle {
        opacity: 0.5;
    }

    .lie-notification-toggle.active {
        opacity: 1;
    }

</style>
@endsection

@section("contents")
    <div class="lie-contents">
        <button class="lie-settings-popout-button" data-toggle="collapse" data-target="#lie-settings-general">
            Allgemein
        </button>

        <?php
            $user = Auth::user("front");
            $user_detail = \App\front\FrontUserDetail::getUserDetailsById(\Illuminate\Support\Facades\Auth::user("front")->id);
            $primary_address = \App\front\FrontUserAddress::getActiveUserPrimaryAddress();
        ?>

        <div class="lie-settings-container collapse" id="lie-settings-general">
            <div class="container-fluid">
                <div style="padding: 10px 10%;">
                    <div class="row">
                        <div class="col-xs-3">
                            Vorname
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $user_detail->fname }}">
                        </div>
                        <div class="col-xs-3">
                            Straße
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $primary_address ? $primary_address->street : "" }}">
                        </div>
                        <div class="col-xs-3">
                            Nachname
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $user_detail->lname }}">
                        </div>
                        <div class="col-xs-3">
                            Hausnummer
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $primary_address ? $primary_address->house : "" }}">
                        </div>
                        <div class="col-xs-3">
                            Telefon
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $user_detail->mobile }}">
                        </div>
                        <div class="col-xs-3">
                            Stiege
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $primary_address ? $primary_address->staircase : "" }}">
                        </div>
                        <div class="col-xs-3">
                            Stadt
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $primary_address ? $primary_address->city : "" }}">
                        </div>
                        <div class="col-xs-3">
                            Etage
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $primary_address ? $primary_address->floor : "" }}">
                        </div>
                        <div class="col-xs-3">
                            Postleitzahl
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $primary_address ? $primary_address->zipcode : "" }}">
                        </div>
                        <div class="col-xs-3">
                            Firma
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="lie-settings-input-text" value="{{ $primary_address ? $primary_address->company : "" }}">
                        </div>
                        <div class="col-xs-3">
                            Info
                        </div>
                        <div class="col-xs-9">
                            <textarea class="lie-settings-input-text" value="{{ $primary_address ? $primary_address->landmark : "" }}"></textarea>
                        </div>
                        <div class="col-xs-3">
                            Geburtsdatum
                        </div>
                        <div class="col-xs-9">
                            <input type="date" class="lie-settings-input-text" value="{{ $user_detail->dob }}" style="width: 33%;">
                        </div>
                        <div class="col-xs-3">
                            Geschlecht
                        </div>
                        <div class="col-xs-9">
                            <button type="button"
                                    class="lie-settings-gender-button {{ $user_detail->gender == "m" ? "selected" : "" }}"
                                    id="lie-gender-button-male">
                                <i class="fa fa-male"></i> Männlich
                            </button>
                            <button type="button"
                                    class="lie-settings-gender-button {{ $user_detail->gender == "f" ? "selected" : "" }}"
                                    id="lie-gender-button-female">
                                <i class="fa fa-female"></i> Weiblich
                            </button>
                            <input type="hidden" name="gender" value="{{ $user_detail->gender ? $user_detail->gender : "" }}">
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-9">
                            <button type="button" class="lie-settings-save-button">Speichern</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button class="lie-settings-popout-button" data-toggle="collapse" data-target="#lie-settings-profile">
            Profil
        </button>

        <div class="lie-settings-container collapse" id="lie-settings-profile">
            <div class="container-fluid">
                <div style="padding: 10px 10%;">
                    <div class="row">
                        <div class="col-xs-3">
                            E-Mail Adresse
                        </div>
                        <div class="col-xs-9">
                            <input type="text" class="lie-settings-input-text" name="email">
                        </div>
                        <div class="col-xs-3">
                            Neues Passwort
                        </div>
                        <div class="col-xs-9">
                            <input type="password" class="lie-settings-input-text" name="password">
                        </div>
                        <div class="col-xs-3">
                            Passwort wiederholen
                        </div>
                        <div class="col-xs-9">
                            <input type="password" class="lie-settings-input-text" name="password_confirm">
                        </div>
                        <div class="col-xs-12">
                            <span class="lie-profile-title">Profil verifizieren</span> <span class="lie-profile-subtitle">durch SMS und E-Mail</span>
                        </div>
                        <div class="col-xs-3">
                            &nbsp;
                        </div>
                        <div class="col-xs-9 lie-verification-header">
                            Verifiziere deine Handynummer und erhalte einmalig 30 Treuepunkte
                        </div>
                        <div class="col-xs-3">
                            SMS
                        </div>
                        <div class="col-xs-5" style="text-align: center">
                            <input type="text" class="lie-settings-input-text" name="sms_number" placeholder="Handynummer"><br>
                            <button type="button" class="lie-settings-save-button">Senden</button>
                        </div>
                        <div class="col-xs-4" style="text-align: center">
                            <input type="text" class="lie-settings-input-text" name="sms_code" placeholder="SMS Code"><br>
                            <button type="button" class="lie-settings-save-button">Senden</button>
                        </div>
                        <div class="col-xs-3">
                            &nbsp;
                        </div>
                        <div class="col-xs-9 lie-verification-header">
                            Verifiziere deine E-Mail Adresse und erhalte einmalig 25 Treuepunkte
                        </div>
                        <div class="col-xs-3">
                            E-Mail
                        </div>
                        <div class="col-xs-5" style="text-align: center">
                            <input type="text" class="lie-settings-input-text" name="email_address" placeholder="E-Mail Adresse"><br>
                            <button type="button" class="lie-settings-save-button">Senden</button>
                        </div>
                        <div class="col-xs-4" style="text-align: center">
                            <input type="text" class="lie-settings-input-text" name="email_code" placeholder="E-Mail Code"><br>
                            <button type="button" class="lie-settings-save-button">Senden</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button class="lie-settings-popout-button" data-toggle="collapse" data-target="#lie-settings-notifications">
            Benachrichtigungen
        </button>

        <?php
            $notification_types = \App\front\UserNotification::where("status", 1)->get();
        ?>

        <div class="lie-settings-container collapse" id="lie-settings-notifications">
            <div class="container-fluid">
                <div style="padding: 10px 10%;">
                    <table width="100%">
                        <thead>
                        <tr>
                            <th width="30%"></th>
                            <th width="20%">Browser</th>
                            <th width="20%">E-Mail</th>
                            <th width="20%">SMS</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($notification_types as $notification_type)
                            <tr style="border-bottom: 1px solid lightgrey">
                                <td width="30%"><div {!! lie_tooltip($notification_type->description) !!} style="margin: 15px 0px;">{{ $notification_type->name }}</div></td>
                                <td width="20%">
                                    <img class="lie-notification-toggle" src="/front/browser_notification.png" width="24">
                                </td>
                                <td width="20%">
                                    <img class="lie-notification-toggle" src="/front/mail_notification.png" width="24">
                                </td>
                                <td width="20%">
                                    <img class="lie-notification-toggle" src="/front/sms_notification.png" width="24">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <button class="lie-settings-popout-button" data-toggle="collapse" data-target="#lie-settings-language">
            Sprache
        </button>

        <div class="lie-settings-container collapse" id="lie-settings-language">
            <div class="container-fluid">
                <div style="padding: 10px 10%;">
                    <div class="row">
                        <div class="col-xs-3">
                            Profilsprache
                        </div>
                        <div class="col-xs-9">
                            <select class="lie-settings-select">
                                <option>Deutsch</option>
                            </select>
                        </div>
                        <div class="col-xs-3">

                        </div>
                        <div class="col-xs-9">
                            <button type="button" class="lie-settings-save-button">Speichern</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section("scripts")
<script>
    $(function() {
        $("#lie-gender-button-male").click(function(evt) {
            $(".lie-settings-gender-button").removeClass("selected");
            $(this).addClass("selected");
            $("input[name='gender']").val("m");
        });
        $("#lie-gender-button-female").click(function(evt) {
            $(".lie-settings-gender-button").removeClass("selected");
            $(this).addClass("selected");
            $("input[name='gender']").val("f");
        });
    });
</script>
@endsection