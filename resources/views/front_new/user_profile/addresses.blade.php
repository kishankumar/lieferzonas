@extends("front_new.user_profile.layout")

@section("user_head")

@endsection

@section("user_contents")

    <div class="lie-container">
        <div class="content-heading">Zustelladressen</div>
    </div>

    <div id="lie-addresses-container">

    </div>

    @include('front_new.includes.add_address_modal', ['callback' => 'addAddressCallback'])

    @include('front_new.includes.edit_address_modal', ['callback' => 'editAddressCallback'])

@endsection

@section("user_scripts")
    {!! Html::script('/public/assets/js/underscore-min.js') !!}


    @include("front_new.includes.address_display_templates")

    <script>
        $("#lie-sidebar-addresses").addClass("active");

        var addressElementTemplate = _.template($("#lie-address-element-template").html());
        var addAddressTemplate = _.template($("#lie-add-address-template").html());

        var addresses = null;

        function onClickEditAddress(addressId) {
            var address = addresses.find(function(element) {
                return element.id == addressId;
            });
            if (address) {
                showEditAddressModal(address);
            }
        }

        function editAddressCallback(response) {
            if (response.status == "success") {
                refreshAddresses();
            }
            else if(response.reason) {
                alert(response.reason);
            }
        }

        function addAddressCallback(response) {
            if (response.status == "success") {
                refreshAddresses();
            }
            else if(response.reason) {
                alert(response.reason);
            }
        }

        function deleteAddressCallback(response) {
            if (response.status == "success") {
                refreshAddresses();
            }
            else if(response.reason) {
                alert(response.reason);
            }
        }

        function onAddressesChanged() {
            $("#lie-addresses-container").empty();
            for (var i = 0; i < addresses.length; i++) {
                var address = addresses[i];
                $("#lie-addresses-container").append(addressElementTemplate({"address": address}));
            }
            $("#lie-addresses-container").append(addAddressTemplate({}));
        }

        function refreshAddresses() {
            $.ajax({
                url: '{{ url("front/json/addresses") }}',
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    if (response.status == "failure") return console.error("Konnte keine Adressen laden: " + response.reason + "!");
                    addresses = response;
                    onAddressesChanged();
                }
            });
        }

        $(function() {
            refreshAddresses();
        });

    </script>
@endsection