@extends("front_new.user_profile.layout")

@section("user_head")

    <style>
        .lie-restaurant-header {
            font-size: 18px;
            font-family: Harabara-Bold;
        }
    </style>

@endsection

<?php
$stamp_restaurants = \App\front\UserStampRestCurrent::getActiveUserStampedCards();
?>

@section("user_contents")

    <div class="lie-container">
        <div class="content-heading">Stempelkarten</div>
    </div>

    @foreach($stamp_restaurants as $stamp_restaurant)
        <?php
        $display_information = \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($stamp_restaurant->rest_detail_id);
        ?>
    <div class="lie-container" style="padding: 10px;">
        <div class="media-left">
            <img src="{{ $display_information["display_logo"] }}" width="128px" class="img-rounded">
        </div>
        <div class="media-body">
            <span class="lie-restaurant-header">{{ $display_information["name"] }}</span>
            <span class="pull-right lie-rating-golden">{!! lie_rating_stars($display_information["rating"]) . " " . lie_rating_format($display_information["rating"]) !!}</span>
            <br>
            {!! lie_minimum_order_text($display_information["minimum_order_amount"]) !!} &nbsp; &nbsp; &nbsp;
            @if ($display_information["below_minimum_delivery_cost"])
                {!! lie_below_minimum_order_text($display_information["below_minimum_delivery_cost"]) !!}
            @endif
            <br>
            {!! lie_above_minimum_order_text($display_information["above_minimum_delivery_cost"]) !!}
            <br>
            @if ($display_information["has_stamp_cards"])
                <span class="lie-stamp-color">
                {!! lie_stamp_bullets($display_information["user_stamps"], $display_information["max_stamps"]) !!}
                    &nbsp; &nbsp; &nbsp;
                    {!! lie_stamp_text($display_information["user_stamps"], $display_information["max_stamps"]) !!}
                    @if ($display_information["full_stamp_cards"])
                        &nbsp; &nbsp; &nbsp;
                        ({!! lie_full_stamps_text($display_information["full_stamp_cards"])  !!})
                    @endif
            </span>
            @endif
            <span class="pull-right">{!! lie_favorite_bundle($stamp_restaurant->rest_detail_id, $display_information) !!}</span>
        </div>
    </div>
    @endforeach

@endsection

@section("user_scripts")
    <script>
        $("#lie-sidebar-stamps").addClass("active");
    </script>
@endsection