@extends('front_new.user_profile.layout')

@section('user_head')
    <style>
        .lie-numeric-bullet-list {
            list-style-type: none;
            padding-left: 0px;
            margin-bottom: 0px;
            font-weight: bolder;
        }

        .lie-bullet-number {
            float: left;
            width: 40px;
        }

        .lie-numeric-bullet-list > li {
            padding-bottom: 5px;
            padding-top: 5px;
        }
    </style>
@endsection

<?php
$addresses_count = \App\front\FrontUserAddress::user_address_count();
if ($addresses_count == 1) {
    $addresses_count_label = "Lieferadresse";
} else {
    $addresses_count_label = "Lieferadressen";
}
$favorite_count = \App\front\UserRestFavourite::user_rest_favourite_count();
if ($favorite_count == 1) {
    $favorite_count_label = "Favorit";
} else {
    $favorite_count_label = "Favoriten";
}
$order_count = \App\front\UserOrder::user_order_count();
if ($order_count == 1) {
    $order_count_label = "Bestellung";
} else {
    $order_count_label = "Bestellungen";
}
$order_count_unrated = \App\front\UserOrder::user_order_count_unrated();
if ($order_count_unrated == 1) {
    $order_count_unrated_label = "offene Bewertung";
} else {
    $order_count_unrated_label = "offene Bewertungen";
}
$totalbonuspoint = \App\front\UserBonusPoint::bonuspoint();
if ($totalbonuspoint == 1) {
    $totalbonuspoint_label = "Bonuspunkt";
} else {
    $totalbonuspoint_label = "Bonuspunkte";
}
$totalcashbackpoint = \App\front\UserCashbackPoint::cashbakpoint();
if ($totalcashbackpoint == 1) {
    $totalcashbackpoint_label = "Cashbackpunkt";
} else {
    $totalcashbackpoint_label = "Cashbackpunkte";
}
?>

@section('user_contents')
    <div class="lie-container">
        <div class="content-heading">Meine Daten</div>
    </div>

    {!! Form::model($profile,['route'=>['front.profile.update',$profile['0']['id']],'method'=>'patch','id' => 'profile','class' => 'prof-form form','novalidate' => 'novalidate', 'files' => true])	!!}

    <div class="lie-container" style="padding: 10px; margin-bottom: 10px;">
        <div class="media-left">
            @if ($profile[0]["profile_pic"] && \Illuminate\Support\Facades\File::exists(public_path() . "/uploads/front/users/" . $profile[0]["profile_pic"]))
                <img id="lie-image-upload-preview" src="{{ "/uploads/front/users/" . $profile[0]["profile_pic"] }}" width="128" height="128"><br/>
            @else
                <img id="lie-image-upload-preview" src="{{url('/public/front/uploads/users/default.png')}}" width="128" height="128"><br/>
            @endif
        </div>
        <div class="media-body">
            <div class="row">
                <div class="col-xs-6" style="padding-left: 35px">
                    <ul class="lie-numeric-bullet-list">
                        <li>
                            <div class="lie-bullet-number">{{ $addresses_count }}</div>
                            {{ $addresses_count_label }}
                        </li>
                        <li>
                            <div class="lie-bullet-number">{{ $favorite_count }}</div>
                            {{ $favorite_count_label }}
                        </li>
                        <li>
                            <div class="lie-bullet-number">{{ $order_count }}</div>
                            {{ $order_count_label }}
                        </li>
                        <li>
                            <div class="lie-bullet-number">{{ $order_count_unrated }}</div>
                            {{ $order_count_unrated_label }}
                        </li>
                    </ul>
                </div>
                <div class="col-xs-6">
                    <ul class="lie-numeric-bullet-list">
                        <li>
                            <div class="lie-bullet-number">{{$totalbonuspoint}}</div>
                            {{$totalbonuspoint_label}}
                        </li>
                        <li>
                            <div class="lie-bullet-number">{{$totalcashbackpoint}}</div>
                            {{$totalcashbackpoint_label}}
                        </li>
                        <li>
                            <div class="lie-bullet-number">0</div>
                            Angefangene Stempelkarten
                        </li>
                        <li>
                            <div class="lie-bullet-number">0</div>
                            Volle Stempelkarten
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="lie-container" style="padding: 10px;">

        <div class="row">
            <div class="col-xs-6">
                <div class="require_star"><span class="red">*</span>
                    {!! Form::text('firstname',$profile['0']['fname'],["id"=>'firstname',"class"=>"form-control","placeholder"=>"Vorname"]) !!}
                    {!! Form::hidden('id',$profile['0']['id'],["id"=>'fuid',"class"=>"form-control","placeholder"=>"First Name"]) !!}
                </div>
                <div class="require_star"><span class="red">*</span>
                    {!! Form::text('nickname',$profile['0']['nickname'],["id"=>'nickname',"class"=>"form-control","placeholder"=>"Benutzername"]) !!}
                </div>
                <div class="require_star"><span class="red">*</span>
                    {!! Form::text('mobile',$profile['0']['mobile'],["id"=>'mobile',"class"=>"form-control","placeholder"=>"Mobilnummer"]) !!}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="require_star"><span class="red">*</span>
                    {!! Form::text('lastname',$profile['0']['lname'],["id"=>'lastname',"class"=>"form-control","placeholder"=>"Nachname"]) !!}
                </div>
                <div class="require_star"><span class="red">*</span>

                    {!! Form::date('dob', $profile['0']['dob'], ["class" => 'form-control', "placeholder"=>"Geburtsdatum"]) !!}
                </div>
                <div class="require_star"><span class="red">*</span>

                    <select name="gender" id="gender" class="form-control">
                        <option value=''>Select</option>
                        <option value='m' <?php if ($profile['0']['gender'] == 'm') echo "Selected='selected'";?> >
                            Male
                        </option>
                        <option value='f' <?php if ($profile['0']['gender'] == 'f') echo "Selected='selected'";?> >
                            Female
                        </option>
                        <option value='o' <?php if ($profile['0']['gender'] == 'o') echo "Selected='selected'";?> >
                            Other
                        </option>
                    </select>
                </div>
            </div>
        </div>

        <div class="require_star">
            <div class="media">
                <div class="pull-left">
                    @if ($profile['0']['profile_pic'])
                        <img class="img-upload"
                             src="{{asset('public/front/uploads/users/'.$profile['0']['profile_pic']) }}"
                             style="max-width: 70px">
                    @endif
                </div>
                <div class="media-body">
                    <input type="file" name="pics" id="pics" onchange="validateimage(this)">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="form-group margin-top text-center">
            <input type="submit" class="btn btn-success" value="Änderungen speichern">
            <hr>
            <p><span class="red">*</span> - Required Fields.</p>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('user_scripts')
    <script>
        $("#lie-sidebar-data").addClass("active");
    </script>
@endsection