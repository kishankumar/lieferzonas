@extends('front_new.user_profile.layout')

@section('title')
    Lieferzonas - Favoriten
@endsection

@section('user_head')
    <style>
        .restaurant-container {
            position: relative;
            height: 158px;
            min-width: 700px;
        }

        .restaurant-header {
            margin-top: 5px;
            font-size: 24px;
        }

        .restaurant-image-container {
            position: absolute;
            left: 15px;
            top: 15px;
        }

        .restaurant-text-container {
            position: absolute;
            left: 158px;
            top: 15px;
            line-height: 10px;
            height: 128px;
        }

        .restaurant-container-right-part {
            position: absolute;
            right: 15px;
            top: 15px;
            height: 128px;
        }

        .restaurant-rating {
            color: #F0E050
        }

        .restaurant-favorite {
            position: absolute;
            bottom: 0;
            right: 0;
            color: #da4f4a;
        }

        .lie-restaurant-header {
            font-size: 18px;
            font-family: Harabara-Bold;
        }
    </style>
@endsection

<?php
$favorite_restaurants = \App\front\UserRestFavourite::user_rest_favourites();
?>

@section('user_contents')

    <div class="lie-container">
        <div class="content-heading">Favoriten</div>
    </div>

    @foreach($favorite_restaurants as $favorite_restaurant)
        <?php $display_information = \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($favorite_restaurant->rest_id) ?>
        <div class="lie-container" style="padding: 8px;">
            <div class="media-left">
                <img src="{{ $display_information["display_logo"] }}" width="128" class="img-rounded">
            </div>
            <div class="media-body">
                <span class="lie-restaurant-header">{{ $display_information["name"] }}</span>
                <span class="pull-right lie-rating-golden">{!! lie_rating_bundle($display_information["rating"]) !!}</span>
                <br>
                <span>{{ $display_information["kitchens_text"] }}</span>
                <br>
                @if($display_information["is_open_now"])
                <span class="lie-open-color">
                @else
                <span class="lie-closed-color">
                @endif
                {!! lie_open_hours_text($display_information["open_hours"], $display_information["closed_today"]) !!}
                </span>
                    <br>
                        {!! lie_minimum_order_text($display_information["minimum_order_amount"]) !!} &nbsp; &nbsp; &nbsp;
                        @if ($display_information["below_minimum_delivery_cost"])
                            {!! lie_below_minimum_order_text($display_information["below_minimum_delivery_cost"]) !!}
                        @endif
                        <br>
                        {!! lie_above_minimum_order_text($display_information["above_minimum_delivery_cost"]) !!}
                        <br>
                    <br>
                        @if ($display_information["has_stamp_cards"])
                            <span class="lie-stamp-color">
                                {!! lie_stamp_bullets($display_information["user_stamps"], $display_information["max_stamps"]) !!}
                                &nbsp; &nbsp; &nbsp;
                                {!! lie_stamp_text($display_information["user_stamps"], $display_information["max_stamps"]) !!}
                                @if ($display_information["full_stamp_cards"])
                                &nbsp; &nbsp; &nbsp;
                                ({!! lie_full_stamps_text($display_information["full_stamp_cards"])  !!})
                                @endif
            </span>
                @endif
                        <span class="pull-right">{!! lie_favorite_bundle($favorite_restaurant->rest_id, $display_information) !!}</span>
            </div>
        </div>
    @endforeach

@endsection

@section('user_scripts')
    <script>
        $("#lie-sidebar-favorites").addClass("active");

        function toggleFavoriteButtonColor(restaurant_id) {
            var favouriteButton = $("#lie-favourite-button-" + restaurant_id);
            if (favouriteButton.attr("style")) {
                favouriteButton.removeAttr("style");
            }
            else {
                favouriteButton.attr("style", "color: #AAAAAA;");
            }
        }

        function toggleFavorite(restaurant_id) {

            $.ajax({
                url: "{{url("front/favourite/toggle")}}",
                data: {
                    "_token": "{{csrf_token()}}",
                    "rest_id": restaurant_id
                },
                type: "POST",
                dataType: "json",
                success: function(response) {
                    if (response.status == "success") {
                        toggleFavoriteButtonColor(restaurant_id);
                    }
                }
            });
        }
    </script>
@endsection