<div class="lie-restaurant-display-container lie-restaurant-div" data-rest-link="{{ $restaurant->id }}">
    @if ($display_information["is_open_now"])
        <span class="lie-open-color">Geöffnet</span>
    @else
        <span class="lie-closed-color">Geschlossen</span>
    @endif
        <div class="media-left">
            <img src="{{ $display_information["display_logo"] }}" width="130" class="lie-restaurant-logo">
        </div>
    <div class="media-body">
        <span class="lie-restaurant-display-name">{{ $restaurant->f_name }}</span>
        <span class="pull-right"><span class="lie-rating-golden">{!! lie_rating_stars($display_information["rating"]) !!}
                &nbsp; {{ lie_rating_format($display_information["rating"]) }}</span></span>
        <br>
        {!! lie_minimum_order_text($display_information["minimum_order_amount"]) !!} &nbsp; &nbsp; &nbsp;
        @if ($display_information["below_minimum_delivery_cost"])
            {!! lie_below_minimum_order_text($display_information["below_minimum_delivery_cost"]) !!}
        @endif
        <br>
        {!! lie_above_minimum_order_text($display_information["above_minimum_delivery_cost"]) !!}
        <br>
        {!! lie_average_delivery_time_text($display_information["average_completion_time"]) !!}
        <br>
        <br>
        @if ($display_information["has_stamp_cards"])
            <span class="lie-stamp-color">
                {!! lie_stamp_bullets($display_information["user_stamps"], $display_information["max_stamps"]) !!}
                &nbsp; &nbsp; &nbsp;
                {!! lie_stamp_text($display_information["user_stamps"], $display_information["max_stamps"]) !!}
                @if ($display_information["full_stamp_cards"])
                    &nbsp; &nbsp; &nbsp;
                    (
                    {!! lie_full_stamps_text($display_information["full_stamp_cards"])  !!})
                @endif
            </span>
        @endif
    </div>
</div>