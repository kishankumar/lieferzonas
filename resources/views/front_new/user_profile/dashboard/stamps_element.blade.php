<div class="lie-restaurant-display-container lie-restaurant-div" data-rest-link="{{ $display_information["id"] }}">
    <div class="row">
        <div class="col-xs-5">
            <div class="media-left">
                {!! lie_colored_open_text($display_information["is_open_now"]) !!}
                <img src="{{ $display_information["display_logo"] }}" width="100" class="img-rounded">
                <span class="lie-rating-golden">
                    {!! lie_rating_stars($display_information["rating"]) !!}
                    &nbsp;
                    {{ lie_rating_format($display_information["rating"]) }}
                </span>
            </div>
            <div class="media-body">
                <span class="lie-restaurant-display-name">{{ $display_information["name"] }}</span>
                <br>
                {!! lie_minimum_order_text($display_information["minimum_order_amount"]) !!}
                {!! lie_below_minimum_order_text($display_information["below_minimum_delivery_cost"]) !!}
                <br>
                {!! lie_above_minimum_order_text($display_information["above_minimum_delivery_cost"]) !!}
                <br>
                {!! lie_average_delivery_time_text($display_information["average_completion_time"]) !!}
                <br>

            </div>
        </div>
        <div class="col-xs-7">
            @if ($display_information["has_stamp_cards"])
                <span class="lie-stamp-color">
                {!! lie_stamp_bullets($display_information["user_stamps"], $display_information["max_stamps"]) !!}
                &nbsp; &nbsp; &nbsp;
                {!! lie_stamp_text($display_information["user_stamps"], $display_information["max_stamps"]) !!}
                    <br>
                    @if ($display_information["full_stamp_cards"])
                        {!! lie_full_stamps_text($display_information["full_stamp_cards"])  !!}
                    @endif
                </span>
                <br>
            @endif
        </div>
    </div>
</div>