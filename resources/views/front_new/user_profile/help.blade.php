@extends("front_new.user_profile.layout")

@section("title")
    Lieferzonas - Hilfe
@endsection

@section("user_head")
    <style>
        .lie-faq-question-button {
            text-align: left;
        }

        .lie-faq-question-answer {
            padding: 6px;
        }
    </style>
@endsection

<?php
$faq = \App\superadmin\FaqQuestion::all();
?>

@section("user_contents")

    <div class="lie-container">
        <div class="content-heading">Häufig gestellte Fragen</div>
    </div>

    <div class="lie-container">

        <img src="{{url('/public/front/help_header.png')}}" width="100%">

        <div style="padding: 25px;">
            <div class="row">
                @foreach($faq as $entry)
                    <div class="col-xs-6">
                        <button class="btn btn-default btn-block lie-faq-question-button" data-toggle="collapse" data-target="#lie-faq-{{ $entry->id }}">
                            <i class="fa fa-plus"></i> {{ $entry->question }}
                        </button>
                        <div id="lie-faq-{{ $entry->id }}" class="collapse lie-faq-question-answer">
                            {{ $entry->answer }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div style="padding: 15px;">

        <div class="row">
            <div class="col-xs-6">

            </div>
            <div class="col-xs-6">

            </div>
        </div>

    </div>

@endsection

@section("user_scripts")
    <script>
        $("#lie-sidebar-help").addClass("active");
    </script>
@endsection