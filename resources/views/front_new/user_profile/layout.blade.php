@extends('front_new.layout')

@section('title')
    Lieferzonas - User Bereich
@endsection

@section('head')
    <style>
        .navbar-default {
            margin-bottom: 10px;
        }

        .sidebar-nav {
            list-style-type: none;
            padding-left: 0px;
            padding-top: 14px;
            padding-bottom: 14px;
            margin-bottom: 0px;
        }

        .lie-point-floater {
            float: right;
            font-weight: bold;
            padding-right: 36px;
        }

        .sidebar-container {
            padding: 0px;
        }

        .sidebar-nav > li {
            background-color: #FFFFFF;
            padding-left: 12px;
            padding-top: 12px;
            padding-bottom: 12px;
            cursor: pointer;
        }

        .sidebar-nav > li:hover {
            background-color: #91D5AF;
        }

        .sidebar-nav > li.active {
            background-color: #27ae60;
            color: #FFFFFF;
        }

        .content-heading {
            padding: 24px;
            font-size: 24px;
            font-weight: lighter;
        }

        .lie-nopad-column {
            padding: 0px;
        }

        .table-borderless tbody tr td,
        .table-borderless tbody tr th,
        .table-borderless thead tr th,
        .table-borderless thead tr td,
        .table-borderless tfoot tr th,
        .table-borderless tfoot tr td {
            border: none;
        }

        .lie-container-left-part {
            padding-left: 40px;
            text-align: center;
        }

        .lie-container-right-part {
            padding-right: 40px;
            border-left-style: solid;
            border-left-color: #EEEEEE;
            border-left-width: 1px;
        }

        .table-striped .dark-striped > tbody > tr:nth-child(2n+1) > td, .table-striped .dark-striped > tbody > tr:nth-child(2n+1) > th {
            background-color: #444444;
        }

    </style>
    @yield('user_head')
@endsection

<?php
$totalbonuspoint = \App\front\UserBonusPoint::bonuspoint();
$totalcashbackpoint = \App\front\UserCashbackPoint::cashbakpoint();
$user_id = \Illuminate\Support\Facades\Auth::user('front')->id;
?>

@section('contents')
    <div class="lie-contents">
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-9">
                @yield('page_header')
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <div class="lie-container sidebar-container">
                    <ul class="sidebar-nav">
                        <li class="lie-sidebar-button" id="lie-sidebar-dashboard" data-href="{{url('front/dashboard')}}">
                            Dashboard
                        </li>
                        <li class="lie-sidebar-button" id="lie-sidebar-data" data-href="{{url('front/profile/'.$user_id.'/edit')}}">
                            Meine Daten
                        </li>
                        <li class="lie-sidebar-button" id="lie-sidebar-bonus" data-href="{{url('front/bonuspoint')}}">
                            <nobr>Bonus Punkte
                                <div class="lie-point-floater">{{ $totalbonuspoint }}</div>
                            </nobr>
                        </li>
                        <li class="lie-sidebar-button" id="lie-sidebar-cashback" data-href="{{url('front/cashbackpoint')}}">
                            <nobr>Cashback
                                <div class="lie-point-floater">{{ $totalcashbackpoint }}</div>
                            </nobr>
                        </li>
                        <li class="lie-sidebar-button" id="lie-sidebar-stamps" data-href="{{url('front/stamp')}}">Stempelkarten</li>
                        <li class="lie-sidebar-button" id="lie-sidebar-addresses" data-href="{{url('front/address')}}">
                            Zustelladressen
                        </li>
                        <li class="lie-sidebar-button" id="lie-sidebar-favorites" data-href="{{url('front/favourite')}}">Favoriten
                        </li>
                        <li class="lie-sidebar-button" id="lie-sidebar-orders" data-href="{{url('front/order')}}">Bestellungen</li>
                        <li class="lie-sidebar-button" id="lie-sidebar-reviews" data-href="{{url('front/orderrate')}}">Bestellungen
                            bewerten
                        </li>
                        <li class="lie-sidebar-button" id="lie-sidebar-help" data-href="{{url('front/help')}}">Hilfe</li>
                    </ul>
                </div>
                @yield('user_below_sidebar')
            </div>
            <div class="col-xs-9">
                @yield('user_contents')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(".lie-sidebar-button").click(function () {
            var link = $(this).data("href");
            window.location.href = link;
        });
    </script>
    @yield('user_scripts')
@endsection

</body>
</html>
