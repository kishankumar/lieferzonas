@extends('front_new.user_profile.layout')

@section('title')
    Lieferzonas - Meine Bestellungen
@endsection

@section('page_header')
    <!--
    Meine Bestellungen <span class="pull-right">sortieren <i class="fa fa-search"></i></span>
    -->
@endsection

@section('user_head')
    <style>
        .lie-order-container {
            padding: 8px;
        }

        .lie-delivery-time-img {
            width: 50%;
        }
    </style>
@endsection

@section('user_contents')
    <div id="lie-orders-container">
        @foreach($userorder as $order)
            <?php $display_information = \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($order->rest_detail_id); ?>
            <div class="lie-container lie-order-container">
                <div class="row">
                    <div class="col-xs-3">
                        <span style="font-size: 12px">{{ $order->created_at }}</span> <br>
                        {{ $display_information["name"] }} <br>
                        <img src="{{ $display_information["display_logo"] }}" style="width: 100%"><br>
                        <span style="font-size: 12px">
                        Es schliesst in {{ $display_information["closes_in"] }}
                    </span>
                    </div>
                    <div class="col-xs-9">
                        <div>
                            <div class="row">
                                <div class="col-xs-4" style="text-align: left">
                                    @if($order->order_type == 1)
                                        {{ $order->address }}<br>
                                        {{ $order->zipcode }} {{ $order->city_name }}
                                    @else
                                        Abholung
                                    @endif
                                </div>
                                <div class="col-xs-3" style="text-align: center">
                                    @if($order->front_order_status_id == 2 or $order->front_order_status_id == 5 or $order->front_order_status_id == 6 or $order->front_order_status_id == 7)
                                        {{$order->rest_given_time}}<br>
                                        <b>Minuten</b>
                                    @endif
                                </div>
                                <div class="col-xs-2" style="text-align: right">
                                    <b>{{ currency_format($order->grand_total) }}</b></div>
                                <div class="col-xs-3 lie-rating-golden"
                                     style="text-align: right">{!! lie_rating_stars($display_information["rating"]) . " " . lie_rating_format($display_information["rating"]) !!}</div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-6">Bestellnummer:</div>
                                        <div class="col-xs-6">#{{ $order->order_id }}</div>
                                    </div>
                                    <div style="padding-top: 50px;">
                                    <span style="vertical-align: middle">
                                        @if($order->front_order_status_id == 1)
                                            Das Restaurant hat die Bestellung noch nicht bestätigt!
                                        @elseif($order->front_order_status_id == 2)
                                            Das Restaurant hat die Bestellung bestätigt!
                                        @elseif($order->front_order_status_id == 3)
                                            Das Restaurant hat die Bestellung abgelehnt!
                                        @elseif($order->front_order_status_id == 4)
                                            Die Bestellung ist storniert!
                                        @elseif($order->front_order_status_id == 5)
                                            Die Bestellung wird gerade zubereitet!
                                        @elseif($order->front_order_status_id == 6)
                                            Die Bestellung wird gerade geliefert!
                                        @elseif($order->front_order_status_id == 7)
                                            Die Bestellung wurde zugestellt!
                                        @endif
                                    </span>
                                    </div>
                                </div>
                                <div class="col-xs-4" style="text-align: center">
                                    @if($order->front_order_status_id == 2 or $order->front_order_status_id == 5 or $order->front_order_status_id == 6)
                                        <?php
                                            $elapsed_time = time() - \App\front\UserOrder::getOrderConfirmationTime($order->order_id);
                                            $elapsed_minutes = floor($elapsed_time / 60);
                                            $given_minutes = $order->rest_given_time;
                                            if ($elapsed_minutes < $given_minutes * 0.5) {
                                                $time_status = 1;
                                            } elseif ($elapsed_minutes < $given_minutes * 0.8) {
                                                $time_status = 2;
                                            } elseif ($elapsed_minutes <= $given_minutes) {
                                                $time_status = 3;
                                            } elseif ($elapsed_minutes < $given_minutes * 1.25) {
                                                $time_status = 4;
                                            } else {
                                                $time_status = 5;
                                            }
                                        ?>
                                        <span style="font-size: 24px">{{ $elapsed_minutes }}</span><br>
                                        <span style="font-size: 20px">Minuten</span><br>
                                        <img class="lie-delivery-time-img" src="{{url('/public/front/delivery_time_'.$time_status.'.png')}}">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('user_scripts')
    <script>
        $("#lie-sidebar-orders").addClass("active");
    </script>
@endsection