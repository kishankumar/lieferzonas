@extends('front_new.user_profile.layout')

@section('title')
    Lieferzonas - Cashbackpunkte
@endsection

@if(count($creditcpoint))
    <?php $cpoints = '0'; ?>
    @foreach($creditcpoint as $creditcpoints)
        <?php
        $cpoints = $cpoints + (int)$creditbpoints->awarded_points;
        ?>
    @endforeach
@else
    <?php $cpoints = 0; ?>
@endif

@if(count($expirecpoint))
    <?php $upoints = '0';
    $apoints = '0';
    ?>
    @foreach($expirecpoint as $expirecpoints)
        <?php
        $upoints = $upoints + (int)$expirecpoints->used_points;
        $apoints = $apoints + (int)$expirecpoints->awarded_points;

        ?>
    @endforeach
    <?php $epoints = ($apoints - $upoints); ?>
@else
    <?php $epoints = 0; ?>
@endif

@if(count($debitcpoint))
    <?php $dpoints = '0'; ?>
    @foreach($debitcpoint as $debitcpoints)
        <?php
        $dpoints = $dpoints + (int)$debitcpoints->used_points;
        ?>
    @endforeach
@else
    <?php $dpoints = 0; ?>
@endif

@if(count($totalcpoint))
    <?php $upoints = '0';
    $apoints = '0';
    ?>
    @foreach($totalcpoint as $totalcpoints)
        <?php
        $upoints = $upoints + (int)$totalcpoints->used_points;
        $apoints = $apoints + (int)$totalcpoints->awarded_points;

        ?>
    @endforeach
    <?php $tpoints = ($apoints - $upoints); ?>
@else
    <?php $tpoints = 0; ?>
@endif

@section('user_contents')
    <div class="lie-container">
        <div class="content-heading">Cashbackpunkte</div>
    </div>

    <div class="lie-container">
        <div class="row">
            <div class="col-xs-6 lie-container-left-part" style="line-height: 0px;">
                <h1 style="margin-top: 40px; font-size: 48px;">{{ $tpoints }}</h1> <br/>
                <h3 style="margin-top: 0px;">Cashbackpunkte</h3>
            </div>
            <div class="col-xs-6 lie-container-right-part">
                <table class="table table-striped dark-striped table-borderless" style="margin: 10px;">
                    <tbody>
                    <tr>
                        <td>Erhaltene Cashbackpunkte:</td>
                        <td>{{$cpoints}}</td>
                    </tr>
                    <tr>
                        <td>Abgelaufene Cashbackpunkte:</td>
                        <td>{{$epoints}}</td>
                    </tr>
                    <tr>
                        <td>Ausgegebene Cashbackpunkte:</td>
                        <td>{{$dpoints}}</td>
                    </tr>
                    <tr>
                        <td>Übrige Cashbackpunkte:</td>
                        <td>{{$tpoints}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="lie-container">
    </div>

    <div class="lie-container">
        <div class="content-heading">Verlauf</div>
        <table class="table" style="margin: 15px;">
            <tbody>
            <thead>
            <tr>
                <th>Grund</th>
                <th>Bestellung</th>
                <th>Menge</th>
            </tr>
            </thead>
            @foreach($cashbackhistory as $entry)
                <tr @if($entry->creditordebit==1) class="success" @else class="danger" @endif >
                    <td>{{ $entry->activity_title }}</td>
                    <td>{{ $entry->order_id }}</td>
                    <td>{{ $entry->amount }}</td>
                </tr>
                @endforeach
                </tbody>
        </table>
    </div>

@endsection

@section('user_scripts')
    <script>
        $("#lie-sidebar-cashback").addClass("active");
    </script>
@endsection