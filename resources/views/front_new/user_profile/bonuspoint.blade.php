@extends('front_new.user_profile.layout')

@if(count($creditbpoint))
    <?php $cpoints = '0'; ?>
    @foreach($creditbpoint as $creditbpoints)
        <?php
        $cpoints = $cpoints + (int)$creditbpoints->awarded_points;
        ?>
    @endforeach
@else
    <?php $cpoints = 0; ?>
@endif

@if(count($expirebpoint))
    <?php $upoints = '0';
    $apoints = '0';
    ?>
    @foreach($expirebpoint as $expirebpoints)
        <?php
        $upoints = $upoints + (int)$expirebpoints->used_points;
        $apoints = $apoints + (int)$expirebpoints->awarded_points;

        ?>
    @endforeach
    <?php $epoints = ($apoints - $upoints); ?>
@else
    <?php $epoints = 0; ?>
@endif

@if(count($debitbpoint))
    <?php $dpoints = '0'; ?>
    @foreach($debitbpoint as $debitbpoints)
        <?php
        $dpoints = $dpoints + (int)$debitbpoints->used_points;
        ?>
    @endforeach
@else
    <?php $dpoints = 0; ?>
@endif

@if(count($totalbpoint))
    <?php $upoints = '0';
    $apoints = '0';
    ?>
    @foreach($totalbpoint as $totalbpoints)
        <?php
        $upoints = $upoints + (int)$totalbpoints->used_points;
        $apoints = $apoints + (int)$totalbpoints->awarded_points;

        ?>
    @endforeach
    <?php $tpoints = ($apoints - $upoints); ?>
@else
    <?php $tpoints = 0; ?>
@endif

@section('user_contents')
    <div class="lie-container">
        <div class="content-heading">Treuepunkte</div>
    </div>

    <div class="lie-container">
        <div class="row">
            <div class="col-xs-6 lie-container-left-part" style="line-height: 0px;">
                <h1 style="margin-top: 40px; font-size: 48px;">{{ $tpoints }}</h1> <br/>
                <h3 style="margin-top: 0px;">Treuepunkte</h3>
            </div>
            <div class="col-xs-6 lie-container-right-part">
                <table class="table table-striped dark-striped table-borderless" style="margin: 10px;">
                    <tbody>
                    <tr>
                        <td>Erhaltene Treuepunkte:</td>
                        <td>{{$cpoints}}</td>
                    </tr>
                    <tr>
                        <td>Abgelaufene Treuepunkte:</td>
                        <td>{{$epoints}}</td>
                    </tr>
                    <tr>
                        <td>Ausgegebene Treuepunkte:</td>
                        <td>{{$dpoints}}</td>
                    </tr>
                    <tr>
                        <td>Übrige Treuepunkte:</td>
                        <td>{{$tpoints}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="lie-container">
    </div>

    <div class="lie-container">
        <div class="content-heading">Verlauf</div>
        <table class="table" style="margin: 15px;">
            <tbody>
            <thead>
            <tr>
                <th>Grund</th>
                <th>Bestellung</th>
                <th>Menge</th>
            </tr>
            </thead>
            @foreach($bonushistory as $entry)
                <tr @if($entry->creditordebit==1) class="success" @else class="danger" @endif >
                    <td>{{ $entry->activity_title }}</td>
                    <td>{{ $entry->order_id }}</td>
                    <td>{{ $entry->amount }}</td>
                </tr>
                @endforeach
                </tbody>
        </table>
    </div>

@endsection

@section('user_scripts')
    <script>
        $("#lie-sidebar-bonus").addClass("active");
    </script>
@endsection