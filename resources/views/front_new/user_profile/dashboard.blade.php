@extends('front_new.user_profile.layout')

@section('user_head')
    <style>

        .lie-dashboard-header {
            position: relative;
            font-size: 26px;
            background: url("../public/front/dashboard_header.png") no-repeat center;
            background-size: cover;
            height: 250px;
        }

        #lie-dashboard-header-title {
            position: absolute;
            left: 6%;
            top: 10px;
            color: white;
            font-size: 26px;
            font-family: Harabara-Bold;
            text-shadow: 0 0 5px black;
        }

        #lie-dashboard-header-lines-container {
            position: absolute;
            top: 80px;
            right: 26%;
        }

        #lie-dashboard-header-line-1 {
            position: absolute;
            right: 0px;
            top: 0px;
            width: 206px;
            padding: 3px 8px;
            background-color: #47d5ad;
            color: white;
        }

        #lie-dashboard-header-line-2 {
            position: absolute;
            right: 0px;
            top: 50px;
            width: 431px;
            padding: 3px 8px;
            background-color: black;
            color: white;
        }

        #lie-header-zipcode-entry-form {
            position: absolute;
            bottom: 0px;
            text-align: center;
            zoom: 0.8;
            width: 100%;
        }

        .lie-dashboard-segment-header {
            font-size: 16px;
            font-weight: bold;
        }

        .lie-dashboard-segment-subtitle {
            color: grey;
            font-size: 13px;
            font-weight: lighter;
        }

        .lie-dashboard-segment-help {
            background-color: #FFFFFF;
            padding: 2px 8px;
            border-radius: 100px;
            font-weight: bold;
            color: black;
            cursor: help;
        }

        .lie-dashboard-segment-column {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .lie-sidebar-segment {
            overflow: hidden;
            margin-top: 20px;
            border-radius: 5px;
            background-color: white;
        }

        .lie-sidebar-segment-header {
            background-color: #27ae60;
            color: white;
            padding: 15px;
        }

        .lie-sidebar-segment-new-address {
            border-radius: 2px;
            border: 1px solid lightgrey;
            margin: 10px;
            padding: 8px;
            text-align: center;
            cursor: pointer;
        }

        .lie-sidebar-segment-new-address:hover {
            background-color: #F6F6F6;
        }

        .lie-sidebar-segment-address-more {
            border-radius: 2px;
            border: 1px solid lightgrey;
            margin: 10px;
            padding: 6px;
            text-align: center;
            cursor: pointer;
        }

        .lie-sidebar-segment-address-more:hover {
            background-color: #F6F6F6;
        }

        .lie-sidebar-segment-address {
            margin: 10px;
            border-bottom: 1px solid lightgrey;
            padding-bottom: 10px;
        }

        .lie-sidebar-segment-address-zipcode {
            color: grey;
        }

        .lie-sidebar-segment-address-street {
        }

        .lie-sidebar-segment-address-arrow {
            color: #27ae60;
        }

        .lie-last-order-table {
            font-size: 12px;
            color: grey;
        }

        .lie-last-order-value-cell {
            width: 1%;
            white-space: nowrap;
        }

        .lie-dashboard-more {
            cursor: pointer;
            transition: background-color 0.5s;
        }

        .lie-dashboard-more:hover {
            background-color: #F0F0F0;
        }

    </style>
@endsection

@section('user_below_sidebar')

    <?php
    $addressesToDisplay = \App\front\FrontUserAddress::getUserAddresses(2);
    $addressCount = \App\front\FrontUserAddress::user_address_count();
    ?>

    <div class="lie-sidebar-segment">
        <div class="lie-sidebar-segment-header">
            Meine Adressen
            <span class="pull-right lie-dashboard-segment-help" {!! lie_tooltip("Hier siehst du deine jüngsten Adressen") !!}> ? </span>
        </div>
        @foreach($addressesToDisplay as $address)
            <div class="lie-sidebar-segment-address">
                <span class="lie-sidebar-segment-address-zipcode">{{ $address->zipcode }}</span>
                <span class="lie-sidebar-segment-address-street">{{ lie_format_address($address->street, $address->house, $address->staircase, $address->floor, $address->apartment) }}</span>
                <span class="pull-right lie-sidebar-segment-address-arrow"><i class="glyphicon glyphicon-chevron-right"></i></span>
            </div>
        @endforeach
        <div class="lie-sidebar-segment-new-address" onclick="showAddAddressModal()">
            NEUE ADRESSE HINZUFÜGEN
        </div>
    </div>

    <?php
        $lastOrder = \App\front\UserOrder::getLastOrder();
        if ($lastOrder) {
            $lastOrderStatus = \App\front\FrontOrderStatusMap::getOrderStatus($lastOrder->order_id);
            $lastOrderAddress = \App\front\FrontUserAddress::find($lastOrder->front_user_address_id);
            $lastOrderRestaurant = \App\superadmin\RestDetail::find($lastOrder->rest_detail_id);
        }
    ?>

    <div class="lie-sidebar-segment">
        <div class="lie-sidebar-segment-header">
            Letzte Bestellung
            <span class="pull-right lie-dashboard-segment-help" {!! lie_tooltip("Hier siehst du deine letzte Bestellung") !!}> ? </span>
        </div>
        @if ($lastOrder)
            <div style="padding: 10px">
                <div style="text-align: center">
                    {{ $lastOrderRestaurant->f_name }}
                </div>
                <table class="lie-last-order-table">
                    <tbody>
                    <tr>
                        <td>Gesamtsumme:</td>
                        <td class="lie-last-order-value-cell">{{ currency_format($lastOrder->grand_total) }}</td>
                    </tr>
                    <tr>
                        <td>Bestellnummer:</td>
                        <td class="lie-last-order-value-cell">#{{ $lastOrder->order_id }}</td>
                    </tr>
                    <tr>
                        <td>Bestellzeit:</td>
                        <td class="lie-last-order-value-cell">{{ $lastOrder->created_at }}</td>
                    </tr>
                    <tr>
                        <td>Zustelladresse:</td>
                        <td class="lie-last-order-value-cell"><span class="lie-nowrap-ellipsis">{{ $lastOrderAddress->address }}</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    @include("front_new.includes.add_address_modal", ["callback" => "addAddressCallback"])
@endsection

<?php
$favorites = \App\front\UserRestFavourite::user_rest_favourites();
$stampedCards = \App\front\UserStampRestCurrent::getActiveUserStampedCards();
$recentVisits = \App\front\UserRecentRestVisit::getActiveUser4RecentRestaurantVisits();
?>

@section('user_contents')
    <div class="container-fluid">
        <div class="lie-dashboard-header">
            <div id="lie-dashboard-header-title">Dashboard</div>
            <div id="lie-dashboard-header-lines-container">
                <div id="lie-dashboard-header-line-1">
                    <span class="lie-header-thin">Wien, wie es isst</span>
                </div>
                <div id="lie-dashboard-header-line-2">
                    <span class="lie-header-bold">Jetzt einfach</span> <span class="lie-header-thin">Online Essen Bestellen</span>
                </div>
            </div>
            <div id="lie-header-zipcode-entry-form">
                {!! lie_zipcode_entry_form("lg") !!}
            </div>
        </div>

        @include('front_new.includes.explore_restaurants', ["head_line" => "Restaurants die CA\$HBACK anbieten"])

        <div class="row">
            <div class="col-xs-12 lie-dashboard-segment-column">
                <span class="lie-dashboard-segment-header">Favoriten</span> <span
                        class="pull-right lie-dashboard-segment-help" {!! lie_tooltip("Hier findest du alle Restaurants, die du als Favorit markiert hast") !!}> ? </span> <br/>
                <span class="lie-dashboard-segment-subtitle">Hier findest du alle Restaurants, die du als Favorit markiert hast</span>
                <div class="lie-dashboard-favorites-container">
                    @if ($favorites->count())
                        @foreach($favorites as $favorite)
                            @include('front_new.user_profile.dashboard.favorite_element', ["restaurant" => $favorite, "display_information" => \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($favorite->rest_id)])
                        @endforeach
                        <div class="lie-dashboard-more" onclick="window.location.href='{{ url('front/favourite') }}'">
                            mehr
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 lie-dashboard-segment-column">
                <span class="lie-dashboard-segment-header">Stempelkarten</span> <span
                        class="pull-right lie-dashboard-segment-help" {!! lie_tooltip("Hier siehst du alle Restaurants, bei denen du angefangene Stempelkarten hast") !!}> ? </span><br/>
                <span class="lie-dashboard-segment-subtitle">Hier siehst du alle Restaurants, bei denen du angefangene Stempelkarten hast</span>
                <div class="lie-dashboard-stamps-container">
                    @if ($stampedCards->count())
                        @foreach($stampedCards as $stampedCard)
                            @include('front_new.user_profile.dashboard.stamps_element', ["stamped_card" => $stampedCard, "display_information" => \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($stampedCard->rest_detail_id)])
                        @endforeach
                        <div class="lie-dashboard-more" onclick="window.location.href='{{ url('front/stamp') }}'">
                            mehr
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 lie-dashboard-segment-column">
                Restaurant Besuche
                @if ($recentVisits->count())
                    @foreach($recentVisits as $visit)
                        <?php
                        $display_information = \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($visit->rest_detail_id);
                        ?>
                        @include('front_new.user_profile.dashboard.recent_visit_element', ["display_information" => $display_information])
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection

@section('user_scripts')
    <script>
        $("#lie-sidebar-dashboard").addClass("active");

        function addAddressCallback(response) {
            if (response.status == "success") {
                window.loocation.reload();
            }
            else if(response.reason) {
                alert(response.reason);
            }
        }

    </script>
@endsection
