<div class="lie-explore-restaurants">
    <div class="lie-explore-restaurants-headline">
        <b>{{ $head_line }}</b>
    </div>
    <div class="lie-explore-restaurants-elements-container"><!--
    <?php $restaurant_index = 0; ?>
    @foreach(\App\superadmin\RestDetail::getSuggestedRestaurants() as $restaurant)
        <?php $restaurant_index++; $display_information = \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($restaurant->id) ?>
                -->
            <div class="lie-explore-restaurant lie-restaurant-div" data-rest-link="{{ $restaurant->id }}" data-index="{{$restaurant_index}}"
                 @if ($restaurant_index > 3)
                 style="display: none"
                    @endif
            >
                {!! lie_colored_open_text($display_information["is_open_now"]) !!}
                <div class="media-left">
                    <img src="{{ $display_information["display_logo"] }}" width="80" class="img-rounded">
                </div>
                <div class="media-body" style="text-align: left">
                    <span class="lie-nowrap-ellipsis" style="font-size: 16px; color: black; font-family: Harabara-Bold">{{ $display_information["name"] }}</span><br>
                    <span class="lie-nowrap-ellipsis" style="font-size: 10px; color: #666666;">{{ $display_information["kitchens_text"] }}</span><br>
                    <span class="lie-rating-golden">
                        {!! lie_rating_stars($display_information["rating"]) . " " . lie_rating_format($display_information["rating"]) !!}
                    </span>
                </div>
                <span class="lie-closed-color">+ {{ $display_information["cashback"] }}</span> % CA$HBACK
            </div><!--
        @endforeach
    --></div>
</div>