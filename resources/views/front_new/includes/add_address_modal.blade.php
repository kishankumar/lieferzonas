<div class="modal fade" tabindex="-1" role="dialog" id="lie-add-address-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adresse hinzufügen</h4>
            </div>
            <div class="modal-body">
                <form id="lie-add-address-modal-form">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>
                                    Name
                                </label>
                                <input type="text" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Straße
                                </label>
                                <input type="text" class="form-control" name="street">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Hausnummer
                                </label>
                                <input type="text" class="form-control" name="house">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Stiege
                                </label>
                                <input type="text" class="form-control" name="staircase">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Stock
                                </label>
                                <input type="text" class="form-control" name="floor">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Tür
                                </label>
                                <input type="text" class="form-control" name="apartment">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Postleitzahl
                                </label>
                                <input type="text" class="form-control" name="zipcode">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Stadt
                                </label>
                                <input type="text" class="form-control" name="city">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Telefonnummer
                                </label>
                                <input type="text" class="form-control" name="phone_number">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>
                                    Firma
                                </label>
                                <input type="text" class="form-control" name="company">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>
                                    Info
                                </label>
                                <textarea class="form-control" name="info"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"
                        onclick="onClickConfirmAddAddressButton('{{ csrf_token() }}', {{ $callback }})">
                    Speichern
                </button>
            </div>
        </div>
    </div>
</div>