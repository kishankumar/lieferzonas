<script type="text/template" id="lie-address-element-template">
    <div class="lie-container lie-address-container" data-address="<%- address.id %>">
        <div class="lie-delete-address-button" onclick="deleteAddress('{{ csrf_token() }}', <%- address.id %>, deleteAddressCallback)">
            <span class="glyphicon glyphicon-remove" style="padding-bottom: 2px;"></span>
        </div>
        <div class="lie-open-color">
            <br>
            <span style="font-size: 24px;"><%- address.zipcode %> <%- address.city_name %></span><br>
            <span style="font-size: 20px;"><%- address.zipcode_name %></span><br>
        </div>
        <br>
        <div style="position: relative">
            <table class="lie-name-phone-table">
                <tbody>
                <tr>
                    <td width="50">
                        <a href="javascript:void(0)" onclick="onClickEditAddress(<%- address.id %>)"><img src="{{url('/public/front/address_edit_icon.png')}}" width="40"></a>
                    </td>
                    <td>
                        <b><%- address.booking_person_name %></b><br>
                        <span style="color: #999"><%- address.mobile %></span>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="lie-gray-bottom-line">
            </div>
        </div>
        <br>
        <div style="padding: 10px; margin-bottom: 40px;">
            <table class="lie-address-data-table" width="100%">
                <tbody>
                <tr>
                    <td width="30%">Straße:</td>
                    <td class="lie-address-data-value-cell"><%- address.street %></td>
                </tr>
                <tr>
                    <td>Haus:</td>
                    <td class="lie-address-data-value-cell"><%- address.house %></td>
                </tr>
                <% if (address.apartment) { %>
                <tr>
                    <td>Tür:</td>
                    <td class="lie-address-data-value-cell"><%- address.apartment %></td>
                </tr>
                <% } %>
                <% if (address.staircase) { %>
                <tr>
                    <td>Stiege:</td>
                    <td class="lie-address-data-value-cell"><%- address.staircase %></td>
                </tr>
                <% } %>
                <% if (address.floor) { %>
                <tr>
                    <td>Stock:</td>
                    <td class="lie-address-data-value-cell"><%- address.floor %></td>
                </tr>
                <% } %>
                <% if (address.company) { %>
                <tr>
                    <td>Firma:</td>
                    <td class="lie-address-data-value-cell"><%- address.company %></td>
                </tr>
                <% } %>
                </tbody>
            </table>
        </div>
        <div class="lie-order-here-link">
            <span class="lie-order-here-span" onclick="selectAddress(<%- address.id %>)">Hierhin <span class="lie-open-color">bestellen</span></span><br>
        </div>
    </div>
</script>

<script type="text/template" id="lie-add-address-template">
    <div class="lie-container lie-add-address-container">
        <br>
        <span style="font-size: 18px">Neue Adresse</span>
        <br>
        <span class="lie-open-color" style="font-size: 16px">Hinzufügen</span>
        <br>
        <br>
        <br>
        <div style="position: relative">
            <div class="lie-add-address-icon-container" style="position: relative; z-index: 1; margin-bottom: 40px;">
                <a href="javascript:void(0)" onclick="showAddAddressModal()"><img src="{{url('/public/front/add_address.png')}}" width="96"></a>
            </div>
            <div class="lie-add-address-gray-line" style="z-index: 0"></div>
        </div>
    </div>
</script>