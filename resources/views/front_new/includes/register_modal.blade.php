<!-- Register Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="lie-register-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="text-align: center;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Registriere dich bei <b>Lieferzonas</b>.at</h4>
            </div>
            <div class="modal-body">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><input type="text" class="lie-register-page-input" placeholder="E-Mail Adresse"
                                               id="lie-user-email-input"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Vorname"
                                               id="lie-user-first-name-input"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Nachname"
                                               id="lie-user-last-name-input"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Telefonnummer"
                                               id="lie-user-phone-input"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><input type="date" class="lie-register-page-input" id="lie-user-dob-input"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td width="25%"></td>
                        <td width="50%"><input type="password" class="lie-register-page-input" placeholder="Passwort"
                                               id="lie-user-password-input"></td>
                        <td width="25%"></td>
                    </tr>
                    </tbody>
                </table>

                <div style="width: 100%; text-align: center;">
                    <a href="javascript:void(0)" id="lie-show-password-link">Passwort anzeigen</a>
                    <a href="javascript:void(0)" id="lie-hide-password-link" style="display: none;">Passwort
                        verbergen</a>
                </div>

                <div>
                    <table>
                        <tbody>
                        <tr>
                            <td>{!! lie_general_checkbox("accept_terms_and_agreements", "", false, "lie-accept-terms") !!}</td>
                            <td>Ich habe die AGB und die Datenschutzbestimmungen von Lieferzonas.at gelesen und
                                akzeptiert.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div id="lie-register-user-separator">
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td>
                                <hr>
                            </td>
                            <td width="1%"> &nbsp; oder &nbsp; </td>
                            <td>
                                <hr>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <table width="100%">
                    <tbody>
                    <tr>
                        <td width="32%">
                            <button type="button" class="lie-register-page-button"
                                    style="background-color: #3b5999; width: 100%;"><i class="fa fa-facebook"></i>
                            </button>
                        </td>
                        <td width="32%">
                            <button type="button" class="lie-register-page-button"
                                    style="background-color: #55acef; width: 100%;"><i class="fa fa-twitter"></i>
                            </button>
                        </td>
                        <td width="32%">
                            <button type="button" class="lie-register-page-button"
                                    style="background-color: #db4838; width: 100%;"><i class="fa fa-google-plus"></i>
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <br>

                <table width="100%">
                    <tbody>
                    <tr>
                        <td width="32%"></td>
                        <td width="32%">
                            <button type="button" class="lie-register-page-button" id="lie-user-submit-button"
                                    style="width: 100%;">
                                REGISTRIEREN
                            </button>
                        </td>
                        <td width="32%"></td>
                    </tr>
                    </tbody>
                </table>

                <br>

                <div style="text-align: center">
                    Du hast schon einen Account? Jetzt <a href="{{url('/frontuser/login')}}">Einloggen</a>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        $("#lie-show-password-link").click(function () {
            $("#lie-user-password-input").attr("type", "text");
            $("#lie-hide-password-link").show();
            $("#lie-show-password-link").hide();

        });
        $("#lie-hide-password-link").click(function () {
            $("#lie-user-password-input").attr("type", "password");
            $("#lie-hide-password-link").hide();
            $("#lie-show-password-link").show();
        });

        function onRegisterSuccess() {
            window.location.reload(true);
        }

        function onRegisterError(reason) {
            window.alert(reason);
        }

        $(function () {
            onButtonClick("#lie-user-submit-button", function (evt) {
                var keys = ["email", "first-name", "last-name", "phone", "dob", "password"];
                var data = {
                    "_token": "{{ csrf_token() }}"
                };
                for (var i = 0; i < keys.length; i++) {
                    data[keys[i]] = $("#lie-user-" + keys[i] + "-input").val();
                }
                $.ajax({
                    url: "{{ url("/front/register_ajax") }}",
                    data: data,
                    type: "POST",
                    dataType: "json",
                    success: function (response) {
                        if (response.success) return onRegisterSuccess();
                        if (response.reason) {
                            onRegisterError(response.reason);
                        }
                    },
                    error: function () {
                        onRegisterError("Es trat ein Fehler beim Anlegen des Benutzer-Kontos auf. Bitte versuche es später noch einmal!");
                    }
                });
            });
        });

    });

</script>