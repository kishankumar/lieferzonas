<html>
<head>
    <title>
        @yield('title')
    </title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- JQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Bootstrap Toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <style>

        html, body {
            height: 100%
        }

        body {
            font-family: "Roboto";
            background-color: #f6f6f6;
        }

        .lie-wrap {
            min-height: 100%;
        }

        .lie-main {
            overflow: auto;
            padding-bottom: 170px;
        }

        /* Footer */

        #lie-bottom-sticky {
            position: relative;
            margin-top: -170px;
            height: 170px;
            clear: both;
        }

        #lie-footer * {
            color: #27ae60;
        }

        #lie-footer-top {
            padding-top: 50px;
            padding-left: 75px;
            padding-right: 75px;
            padding-bottom: 50px;
            display: flex;
            justify-content: space-between;
            background-color: #FFFFFF;
        }

        #lie-footer-top > a {
            color: #27ae60;
            font-weight: 700;
            margin-left: 15px;
        }

        #lie-footer {
            padding: 15px;
            display: flex;
            justify-content: space-between;
        }

        #lie-footer > span {
            margin-left: 15px;
        }

        .lie-sm-icon {
            padding-right: 5px;
            padding-left: 5px;
        }

        /* Navbar */

        .navbar-user-image {
            -webkit-clip-path: circle(12px at center);
            clip-path: circle(12px at center);
        }

        .navbar-default {
            background-color: #27ae60;
            border: none;
            border-radius: 0px;
            margin-bottom: 0px;
            overflow: visible !important;
        }

        .navbar-default .navbar-brand {
            color: #FFFFFF;
            overflow: visible !important;
        }

        .navbar-default .navbar-nav > li > a {
            color: #FFFFFF;
        }

        .lie-navbar-logo {
            z-index: 10000;
            margin-top: -75px;
        }

    </style>

    <link rel="stylesheet" href="/assets/css/lieferzonas_commons.css">

    @yield('head')

</head>
<body>

<div class="lie-wrap">

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img class="lie-navbar-logo" src="/front/logo_new.png">
                    <p>Lieferzonas.at</p>
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                @if (\Illuminate\Support\Facades\Auth::check('front'))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            {{ \App\front\FrontUserDetail::userdetail()->fname }}
                            &nbsp; &nbsp;
                            <img
                                    src="http://tr3.cbsistatic.com/fly/bundles/techrepubliccore/images/icons/standard/icon-user-default.png"
                                    class="navbar-user-image"
                                    width="24"
                                    height="24">
                            &nbsp; &nbsp;
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/front/dashboard">Dashboard</a></li>
                            <li><a href="/front/profile/{{\Illuminate\Support\Facades\Auth::user("front")->id}}/edit">Meine Daten</a></li>
                            <li><a href="/front/bonuspoint">Treuepunkte</a></li>
                            <li><a href="/front/cashbackpoint">Cashbackpunkte</a></li>
                            <li><a href="/front/stamp">Stempelkarten</a></li>
                            <li><a href="/front/address">Zustelladressen</a></li>
                            <li><a href="/front/favourite">Favoriten</a></li>
                            <li><a href="/front/order">Bestellungen</a></li>
                            <li><a href="/front/orderrate">Bestellungen bewerten</a></li>
                            <li><a href="{{url('frontuser/logout')}}">Abmelden</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="/frontuser/register">REGISTRIEREN</a></li>
                    <li><a href="/frontuser/login">EINLOGGEN</a></li>
                @endif
            </ul>
        </div>
    </nav>

    <div class="lie-main">

        @if(Session::has('message'))
            <div class="alert alert-success" style="padding: 7px 15px; margin-bottom: 0px;">
                {{ Session::get('message') }}
                {{ Session::put('message','') }}
            </div>
        @endif
        @if(Session::has('errmessage'))
            <div class="alert alert-danger" style="padding: 7px 15px; margin-bottom: 0px;">
                {{ Session::get('errmessage') }}
                {{ Session::put('errmessage','') }}
            </div>
        @endif
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="alert alert-danger" style="padding: 7px 15px; margin-bottom: 0px;">
                    {{ $error }}
                </div>
            @endforeach
        @endif

        @yield('contents')
    </div>
</div>

<div id="lie-bottom-sticky">

    <div id="lie-footer-top">
        <a href="#">Jobs</a>
        <a href="#">
            <nobr>Affiliate-Programm</nobr>
        </a>
        <a href="#">AGB</a>
        <a href="#">Datenschutz</a>
        <a href="#">Impressum</a>
        <a href="#">Presse</a>
        <a href="#">Partner</a>
        <a href="#">Hilfe</a>
        <a href="#">
            <nobr>Restaurant hinzufügen</nobr>
        </a>
    </div>

    <div id="lie-footer" class="container">
        <span><a href="#"><nobr>&copy; 2017 Lieferzonas.at</nobr></a></span>
        <span><a href="#"><nobr>Sitemap</nobr></a></span>
        <span><a href="#"><nobr>Mobile Version</nobr></a></span>
        <span>
        <nobr>
            <a href="#"><i class="fa fa-instagram fa-lg lie-sm-icon"></i></a>
            <a href="#"><i class="fa fa-linkedin fa-lg lie-sm-icon"></i></a>
            <a href="#"><i class="fa fa-facebook fa-lg lie-sm-icon"></i></a>
            <a href="#"><i class="fa fa-twitter fa-lg lie-sm-icon"></i></a>
            <a href="#"><i class="fa fa-google-plus fa-lg lie-sm-icon"></i></a>
            <a href="#"><i class="fa fa-youtube fa-lg lie-sm-icon"></i></a>
            <a href="#"><i class="fa fa-envelope fa-lg lie-sm-icon"></i></a>
        </nobr>
    </span>
    </div>

</div>

<script src="/assets/js/lieferzonas_commons.js"></script>
@yield('scripts')

<!-- Cart Management -->
<script>
    function clearCart(restaurantId, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "clear"
            },
            success: function(response) {
                if(success) success(response);
            },
            error: function(xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }
    function addToCartItemCount(restaurantId, itemIndex, amount, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "add_count",
                "item_index": itemIndex,
                "amount": amount
            },
            success: function(response) {
                if(success) success(response);
            },
            error: function(xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }
    function setCartItemCount(restaurantId, itemIndex, amount, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "set_count",
                "item_index": itemIndex,
                "amount": amount
            },
            success: function(response) {
                if(success) success(response);
            },
            error: function(xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }
    function removeCartItem(restaurantId, itemIndex, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "remove",
                "item_index": itemIndex
            },
            success: function(response) {
                if(success) success(response);
            },
            error: function(xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }
    function addCartItem(restaurantId, subMenu, extras, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "post",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}',
                "operation": "add",
                "submenu": subMenu,
                "extras": extras
            },
            success: function(response) {
                if(success) success(response);
            },
            error: function(xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }
    function getCartJson(restaurantId, success, error) {
        $.ajax({
            url: '{{ url("restaurant/cart") }}/' + restaurantId,
            type: "get",
            dataType: "json",
            data: {
                "_token": '{{ csrf_token() }}'
            },
            success: function(response) {
                if(success) success(response);
            },
            error: function(xhr, errorText, errorThrown) {
                if (error) error(xhr, errorText, errorThrown);
            }
        });
    }

</script>

</body>
</html>
