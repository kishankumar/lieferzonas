@extends('front_new.layout')

@section('title')
    Lieferzonas
@endsection

@section('head')
    <style>

        .container-fluid {
            padding: 0px;
        }

        #lie-main-container {
            background: url("public/front/main-banner.jpg") scroll;
            background-position-x: 50%;
            background-position-y: 50%;
            background-repeat-x: no-repeat;
            background-repeat-y: no-repeat;
            background-size: cover;
            height: 550px;
            position: relative;
            text-align: center;
            z-index: 0;
        }

        #lie-hunger-text-container {
            color: #27ae60;
            font-weight: 900;
            font-size: 76px;
            padding-top: 55px;
        }

        #order-online-container {
            background-color: #000000;
            color: #FFFFFF;
            display: inline-block;
            font-size: 44px;
            font-weight: 900;
            padding-left: 12px;
            padding-right: 12px;
            margin-top: 55px;
        }

        #zipcode-entry-container {
            margin-top: 24px;
        }

        .toggle {
            border-radius: 0px;
        }

        #lie-zipcode-subtext {
            font-size: 16px;
            color: #FFFFFF;
            margin-top: 10px;
            background-color: #000000;
            padding-left: 10px;
            padding-right: 10px;
            padding-top: 5px;
            padding-bottom: 5px;
            display: inline-block;
        }

        #lie-payment-methods-container {
            background-color: #FFFFFF;
            padding-top: 40px;
            padding-bottom: 40px;
            text-align: center;
        }

        #lie-mobile-app-container {
            border-bottom: solid;
            border-width: 5px;
            background-color: #e9e9e9;
            text-align: right;
        }

        #lie-download-app-button {
            background-color: #333333;
            color: #27ae60;
            border-radius: 0px;
            font-size: 20px;
            font-weight: 100;
            padding-left: 20px;
            padding-right: 20px;
            position: absolute;
            top: 150px;
            left: 120px;
        }

        #lie-mobile-app-description {
            font-size: 20px;
            position: relative;
        }

        .lie-mobile-app-description-line {
            position: absolute;
            top: 50px;
            left: 120px;
            text-align: left;
        }

        #lie-mobile-app-image {
            display: inline-block;
        }

        #lie-how-to-container {
            background-color: #FFFFFF;
            text-align: center;
        }

        #lie-how-to-header {
            margin-top: 50px;
            margin-bottom: 50px;
            font-size: 24px;
        }

        .lie-how-to-text {
            margin-top: 15px;
            font-size: 20px;
            min-height: 70px;
        }

        .lie-how-to-number {
            font-size: 20px;
            color: #ffb8b5;
            -moz-border-radius: 20px;
            border-radius: 20px;
            border: solid;
            border-width: 2px;
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 11px;
            padding-right: 11px;
        }

        .lie-how-to-video-link {
            font-size: 22px;
            font-weight: 900;
        }

        #lie-advantages-container {
            text-align: center;
            background-color: #FFFFFF;
            padding-bottom: 30px;
        }

        #lie-advantages-image-container {
            background: url("public/front/advantages-banner.png") scroll;
            background-position-x: 50%;
            background-position-y: 50%;
            background-repeat-x: no-repeat;
            background-repeat-y: no-repeat;
            background-size: cover;
            height: 670px;
            position: relative;
            text-align: center;
        }

        .lie-advantage-number {
            font-size: 20px;
            padding-left: 16px;
            padding-right: 16px;
            padding-top: 10px;
            padding-bottom: 10px;
            -moz-border-radius: 25px;
            border-radius: 25px;
            border: solid;
            border-width: 2px;
        }

        #lie-advantages-header {
            margin-top: 30px;
            font-size: 20px;
            margin-bottom: 30px;
        }

        .advantage-row {
            height: 75px;
            margin-bottom: 20px;
        }

        #lie-advantage-col-right > table > tbody > tr > td {
            text-align: right;
        }

        .lie-advantage-number {
            color: #8cd06c;
        }

        #lie-help-container {
            border-top: solid;
            border-width: 1px;
            border-color: #CCCCCC;
            padding-top: 40px;
            padding-bottom: 50px;
            text-align: center;
        }

        .lie-help-link {
            color: #AAAAAA;
            font-size: 24px;
            font-weight: normal;
        }

        #lie-contact-container {
            border-top: solid;
            border-width: 1px;
            border-color: #CCCCCC;
            text-align: center;
            padding-top: 40px;
            padding-bottom: 50px;
            background-color: #FFFFFF;
        }

        #lie-mail-us-message {
            font-size: 20px;
        }

        #lie-mail-us-input {
            padding: 10px;
            border: 1px solid #F0F0F0;
            border-right-style: none;
        }

        #lie-mail-us-button {
            border-radius: 0px;
            padding: 10px;
            background-color: #27ae60;
            font-weight: bolder;
            border-radius: 0px;
            color: #FFFFFF;
        }

        .lie-meal-element,
        .lie-service-element {
            padding-bottom: 10px;
        }

        .lie-meal-element > a,
        .lie-service-element > a {
            color: #333333;
            font-size: 16px;
        }

        #lie-service-heading,
        #lie-meal-heading {
            padding-bottom: 20px;
        }

        #lie-services-container {
            padding-top: 45px;
            padding-bottom: 30px;
        }

        #lie-cities-container {
            border-bottom: 5px #000000 solid;
        }

        #lie-cities-image-container {
            background: url("public/front/advantages-banner.png") scroll;
            background-position-x: 50%;
            background-position-y: 50%;
            background-repeat-x: no-repeat;
            background-repeat-y: no-repeat;
            background-size: cover;
            height: 545px;
            position: relative;
            text-align: center;
        }

        #lie-cities-image-tint {
            background: rgba(0, 0, 0, 0.5);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        #lie-cities-search-text {
            padding-top: 240px;
            color: #FFFFFF;
            font-size: 20px;
        }

        #lie-cities-search-button {
            margin-top: 40px;
            padding: 20px;
            font-size: 16px;
            font-weight: bold;
            color: #FFFFFF;
            background-color: #40d47e;
            border-radius: 0px;
        }

        .lie-city-link {
            padding-bottom: 10px;
            font-size: 16px;
        }

        .lie-city-link > a {
            color: #333333;
        }

        #lie-cities-links-container {
            padding-top: 30px;
            padding-bottom: 30px;
        }

    </style>
@endsection

@section('contents')

    <div class="container-fluid">
        <div id="lie-main-container">

        <span id="hunger-text">
        </span>

            <div id="lie-hunger-text-container">
                H<span style="color: #40d47e">un</span>ger<span style="color: #FFFFFF">?...</span>
            </div>

            <div id="order-online-container">
                <nobr>Jetzt einfach Online Essen Bestellen</nobr>
            </div>

            <div id="zipcode-entry-container">
                {!! lie_zipcode_entry_form("lg") !!}
            </div>

            <div id="lie-zipcode-subtext">
                Jetzt Restaurants in Deiner Umgebung finden und gleich bestellen
            </div>

        </div>
        <div id="lie-payment-methods-container">
            {!! Html::image('/public/front/payment-methods.PNG', '', array('class' => '')) !!}

        </div>
        <div id="lie-mobile-app-container">
            <div class="row">
                <div class="col-xs-6">
                    {!! Html::image('/public/front/mobile-app.png', '', array('id' => 'lie-mobile-app-image')) !!}

                </div>
                <div class="col-xs-6" id="lie-mobile-app-description">
                    <p class="lie-mobile-app-description-line">Lieferzonas gibt's demnächst auch <br/> für dein Smartphone</p>

                    <button type="button" class="btn" id="lie-download-app-button">Bald verfügbar
                        &nbsp; <i style="color: #dbdbdb" class="fa fa-apple"></i>
                        &nbsp; <i style="color: #b3c833" class="fa fa-android"></i>
                        &nbsp; <i style="color: #0cb3ee" class="fa fa-windows"></i></button>
                </div>
            </div>
        </div>
        <div id="lie-how-to-container">
            <div class="container">
                <p id="lie-how-to-header">So funktioniert's</p>
                <div class="row">
                    <div class="col-xs-3">
                        {!! Html::image('/public/front/step-1.png', '', array('class' => 'lie-step-image')) !!}
                        <br/>
                        <p class="lie-how-to-text">Gib deine Postleitzahl oder Stadt ein</p>
                        <span class="lie-how-to-number">1</span> <br/> <br/>
                    </div>
                    <div class="col-xs-3">
                        {!! Html::image('/public/front/step-2.png', '', array('class' => 'lie-step-image')) !!}
                         <br/>
                        <p class="lie-how-to-text">Wähle ein Restaurant</p>
                        <span class="lie-how-to-number">2</span> <br/> <br/>
                    </div>
                    <div class="col-xs-3">
                        {!! Html::image('/public/front/step-3.png', '', array('class' => 'lie-step-image')) !!}
                         <br/>
                        <p class="lie-how-to-text">Such deine Speisen aus</p>
                        <span class="lie-how-to-number">3</span> <br/> <br/>
                    </div>
                    <div class="col-xs-3">
                        {!! Html::image('/public/front/step-4.png', '', array('class' => 'lie-step-image')) !!}
                         <br/>
                        <p class="lie-how-to-text">Dein Essen ist unterwegs</p>
                        <br/> <br/>
                    </div>
                </div>
                <br/> <br/>
                <span class="lie-how-to-video-link"><nobr><i style="color: #40d47e"
                                                       class="fa fa-play-circle"></i> TV Spot <span
                            style="color: #40d47e">ansehen</span></nobr></span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <span class="lie-how-to-video-link"><nobr><i style="color: #40d47e"
                                                       class="fa fa-play-circle"></i> Lieferzonas <span
                            style="color: #40d47e">Beschreibung</span></nobr></span>
                <br/> <br/>
            </div>
        </div>
        <div id="lie-advantages-container">
            <div id="lie-advantages-image-container"></div>
            <p id="lie-advantages-header">Deine Vorteile bei Lieferzonas.at</p> <br/>
            <div class="container">
                <div class="row">
                    <div class="col-xs-5" id="lie-advantage-col-left">
                        <table>
                            <tbody>
                            <tr class="advantage-row">
                                <td width="70px"><span class="lie-advantage-number">1</span></td>
                                <td><span class="lie-advantage-text">Mehr als 600 Restaurants</span></td>
                            </tr>
                            <tr class="advantage-row">
                                <td width="70px"><span class="lie-advantage-number">2</span></td>
                                <td><span class="lie-advantage-text">86 Städte in Österreich</span></td>
                            </tr>
                            <tr class="advantage-row">
                                <td width="70px"><span class="lie-advantage-number">3</span></td>
                                <td><span class="lie-advantage-text">Über 60.000 Gerichte</span></td>
                            </tr>
                            <tr class="advantage-row">
                                <td width="70px"><span class="lie-advantage-number">4</span></td>
                                <td><span class="lie-advantage-text">Bei jeder Bestellung Treuepunkte erhalten! Einlösen und Sparen!</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-5" id="lie-advantage-col-right">
                        <table>
                            <tbody>
                            <tr class="advantage-row">
                                <td>
                                    <span class="lie-advantage-text">Preisgarantie auf alle Gerichte der Online-Menükarten</span>
                                </td>
                                <td width="70px"><span class="lie-advantage-number">5</span></td>
                            </tr>
                            <tr class="advantage-row">
                                <td>
                                    <span class="lie-advantage-text">Gebührfrei Essen bestellen! Keine Servicegebühr</span>
                                </td>
                                <td width="70px"><span class="lie-advantage-number">6</span></td>
                            </tr>
                            <tr class="advantage-row">
                                <td><span class="lie-advantage-text">Lieferung in 30-60 Minuten</span></td>
                                <td width="70px"><span class="lie-advantage-number">7</span></td>
                            </tr>
                            <tr class="advantage-row">
                                <td><span class="lie-advantage-text">Zahle in BAR oder gleich ONLINE</span></td>
                                <td width="70px"><span class="lie-advantage-number">8</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="lie-cities-container">
            <div id="lie-cities-image-container">
                <div id="lie-cities-image-tint">
                    <p id="lie-cities-search-text">Suche Restaurants nach Ort</p>
                    <button type="button" class="btn" id="lie-cities-search-button">WEITERE STÄDTE</button>
                </div>
            </div>
            <div class="container" id="lie-cities-links-container">
                <div class="row">
                    <div class="col-xs-2 lie-city-link"><a href="#">Wien</a></div>
                    <div class="col-xs-2 lie-city-link"><a href="#">Linz</a></div>
                    <div class="col-xs-2 lie-city-link"><a href="#">Innsbruck</a></div>
                    <div class="col-xs-2 lie-city-link"><a href="#">Wels</a></div>
                    <div class="col-xs-4 lie-city-link"><a href="#">Wiener Neustadt</a></div>
                    <div class="col-xs-2 lie-city-link"><a href="#">Graz</a></div>
                    <div class="col-xs-2 lie-city-link"><a href="#">Salzburg</a></div>
                    <div class="col-xs-2 lie-city-link"><a href="#">Villach</a></div>
                    <div class="col-xs-2 lie-city-link"><a href="#">Dornbirn</a></div>
                    <div class="col-xs-4 lie-city-link"><a href="#">Steyr</a></div>
                </div>
            </div>
        </div>
        <div id="lie-services-container">
            <div class="container">
                <h3 id="lie-service-heading">Dienste: </h3>
                <div class="row">
                    @foreach(\App\superadmin\FrontService::getActiveServices() as $active_service)
                        <div class="col-xs-3 lie-service-element">
                            <a href="{{ url("/service/" . $active_service->id) }}">
                                {{ $active_service->name }}
                            </a>
                        </div>
                    @endforeach
                </div>
                <h3 id="lie-meal-heading">Populäre Gerichte: </h3>
                <div class="row">
                    <div class="col-xs-3 lie-meal-element"><a href="#">Pizza</a></div>
                    <div class="col-xs-3 lie-meal-element"><a href="#">Wiener Schnitzel</a></div>
                    <div class="col-xs-3 lie-meal-element"><a href="#">Burger</a></div>
                    <div class="col-xs-3 lie-meal-element"><a href="#">Kebab</a></div>
                    <div class="col-xs-3 lie-meal-element"><a href="#">Pasta</a></div>
                    <div class="col-xs-3 lie-meal-element"><a href="#">Spareribs</a></div>
                    <div class="col-xs-3 lie-meal-element"><a href="#">Sushi</a></div>
                    <div class="col-xs-3 lie-meal-element"><a href="#">alle Speisen</a></div>
                </div>
            </div>
        </div>
        <div id="lie-contact-container">
            <p id="lie-mail-us-message">Kontaktiere uns</p>
            <input type="text" placeholder="Deine E-Mail Adresse" id="lie-mail-us-input"><button type="button" class="btn" id="lie-mail-us-button" data-toggle="modal" data-target="#lie-send-email-modal">WEITER</button>
            <div class="modal fade" tabindex="-1" role="dialog" id="lie-send-email-modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">E-Mail</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group" style="text-align: left">
                                <label>Deine E-Mail Adresse: </label>
                                <input type="text" class="form-control" id="lie-send-email-address">
                            </div>
                            <div class="form-group" style="text-align: left">
                                <label>Inhalt: </label>
                                <textarea class="form-control" rows="5" id="lie-send-email-text"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                                <button type="button" class="btn btn-success" id="lie-do-send-email-button">Senden</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="lie-help-container">
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <a href="#" class="lie-help-link"><nobr>Häufig gestellte Fragen</nobr></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <a href="#" class="lie-help-link"><nobr>Restaurant vorschlagen</nobr></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <div class="fb-like" data-href="http://lieferzonas.at" data-width="200" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(function() {

            registerPickupDeliveryToggle('{{csrf_token()}}');

            $("#lie-mail-us-button").click(function(e) {
                $("#lie-send-email-address").val($("#lie-mail-us-input").val());
            });

            $("#lie-do-send-email-button").click(function(e) {
                $.ajax({
                    url: '{{ url("front/send_email") }}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        "_token": '{{ csrf_token() }}',
                        "sender": $("#lie-send-email-address").val(),
                        "text": $("#lie-send-email-text").val()
                    },
                    success: function(response) {
                        if (response.status != "success") {
                            alert("Konnte keine E-Mail verschicken: \n" + response.reason);
                        }
                    }
                })
            });

        });
    </script>
@endsection