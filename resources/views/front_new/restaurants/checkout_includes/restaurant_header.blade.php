<?php
$display_information = \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($restaurant_id);
?>

<div class="lie-container" style="padding: 24px">
    <div class="media-left">
        <img src="{{ $display_information["display_logo"] }}" width="180">
    </div>
    <div class="media-body">
        <span class="lie-restaurant-header-name">{{ $display_information["name"] }}</span>
        <span class="pull-right">{!! lie_rating_stars($display_information["rating"]) !!} {{ lie_rating_format($display_information["rating"]) }}</span>
        <br>
        <span class="lie-restaurant-header-kitchens">&nbsp; {{ $display_information["kitchens_text"] }}</span>
        <br>
        @if ($display_information["is_open_now"])
            <span class="lie-open-color">
        @else
            <span class="lie-closed-color">
        @endif
        {!! lie_open_hours_text($display_information["open_hours"], $display_information["closed_today"]) !!} &nbsp;
        ({{ lie_open_text($display_information["is_open_now"], $display_information["closed_today"]) }})
        </span>
        <br>
        {!! lie_minimum_order_text($display_information["minimum_order_amount"]) !!}
        {!! lie_below_minimum_order_text($display_information["below_minimum_delivery_cost"]) !!}
        <br>
        {!! lie_above_minimum_order_text($display_information["above_minimum_delivery_cost"]) !!}
        <br>
        {!! lie_average_delivery_time_text(\App\front\UserOrder::getAverageCompletionTimeByRestaurant($restaurant_id, 10)) !!}
        <br>
        @if ($display_information["has_stamp_cards"])
            {!! lie_stamp_bullets($display_information["user_stamps"], $display_information["max_stamps"]) !!}
            &nbsp; &nbsp; &nbsp;
            {!! lie_stamp_text($display_information["user_stamps"], $display_information["max_stamps"]) !!}
            &nbsp; &nbsp; &nbsp;
            {!! lie_full_stamps_text($display_information["full_stamp_cards"]) !!}
        @endif
        @if (Auth::check("front"))
            <span class="pull-right">{!! lie_favorite_bundle($restaurant_id, $display_information) !!}</span>
        @endif
    </div>
</div>
