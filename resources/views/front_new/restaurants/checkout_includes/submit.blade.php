<div class="lie-container" style="padding: 10px;">
    <table>
        <tbody>
        <tr>
            <td width="50px" height="50px" style="text-align: center">
                {!! lie_general_checkbox("accept_terms_and_conditions", "", false, "lie-accept-terms") !!}
            </td>
            <td style="font-size: 14px; padding: 5px;">
                Ja, ich akzeptiere die Allgemeinen Geschäftsbedingungen von Lieferzonas.at
            </td>
            <td style="padding: 5px;">
                <button type="button" class="btn btn-success" id="lie-submit-order-button">Zahlungspflichtig bestellen</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>