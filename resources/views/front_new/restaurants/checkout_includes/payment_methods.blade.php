<div style="padding: 10px">
    <span class="lie-paragraph-header">Wie würdest du gerne bezahlen?</span>
    <br>
    <br>
    <div class="lie-payment-methods-container">
        <div class="lie-payment-method-box" onclick="selectPaymentMethod('cash')" data-method="cash">
            <img class="lie-payment-method-logo" src="{{url('/public/front/payment_methods/cash.png')}}"><br><br>
            <span class="lie-payment-method-text">Beim Zusteller zahlen</span>
        </div>
        <div class="lie-payment-method-box" onclick="selectPaymentMethod('instant_transfer')" data-method="instant_transfer">
            <img class="lie-payment-method-logo" src="{{url('/public/front/payment_methods/instant_transfer.png')}}"><br><br>
            <span class="lie-payment-method-text">Sofort-Überweisung</span>
        </div>
        <div class="lie-payment-method-box" onclick="selectPaymentMethod('paypal')" data-method="paypal">
            <img class="lie-payment-method-logo" src="{{url('/public/front/payment_methods/paypal.png')}}"><br><br>
            <span class="lie-payment-method-text">Paypal</span>
        </div>
        <div class="lie-payment-method-box" onclick="selectPaymentMethod('visa')" data-method="visa">
            <img class="lie-payment-method-logo" src="{{url('/public/front/payment_methods/visa.png')}}"><br><br>
            <span class="lie-payment-method-text">Visa</span>
        </div>
        <div class="lie-payment-method-box" onclick="selectPaymentMethod('master_card')" data-method="master_card">
            <img class="lie-payment-method-logo" src="{{url('/public/front/payment_methods/master_card.png')}}"><br><br>
            <span class="lie-payment-method-text">Master Card</span>
        </div>
        <div class="lie-payment-method-box" onclick="selectPaymentMethod('american_express')" data-method="american_express">
            <img class="lie-payment-method-logo" src="{{url('/public/front/payment_methods/american_express.png')}}"><br><br>
            <span class="lie-payment-method-text">American Express</span>
        </div>
    </div>
</div>