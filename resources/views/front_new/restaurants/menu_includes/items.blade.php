@foreach (\App\superadmin\RestCategory::getByRestaurant($rest_detail->id) as $category)
    @if($category->image and \Illuminate\Support\Facades\File::exists(public_path() . "/uploads/superadmin/category/" . $category->image))
        <img src="{{url('public/uploads/superadmin/category/'.$category->image)}}" style="width: 100%">
    @endif
    @foreach(\App\superadmin\RestMenu::getMenuByCategory($category->id) as $menu)
        @if($menu["image"] and \Illuminate\Support\Facades\File::exists(public_path() . "/uploads/superadmin/menu/" . $menu["image"]))
            <img src="{{url('/public/uploads/superadmin/menu/'.$menu["image"])}}" style="width: 100%">
        @endif
        <header>
            <h3 class="lie-menu-title" id="lie-menu-{{ $menu["id"] }}">{{ $menu["name"] }}</h3>
            <p class="lie-menu-subtitle">{{ $menu["info"] }}</p>
        </header>
        <table width="100%" class="lie-menu-table">
            <col width="relative_length">
            <col width="115">
            <tbody>
            @foreach(\App\superadmin\RestSubMenu::getSubMenuByMenu($menu["id"]) as $submenu)
                <?php
                $price_row = \App\superadmin\RestSubMenu::getSubMenuCurrentPrice($submenu["id"]);
                $hasAnyExtras = \App\superadmin\SubMenuExtraMap::hasSubMenuAnyExtras($submenu["id"]);
                $extras = \App\superadmin\ExtraChoice::getBySubMenu($submenu["id"]);
                $allergenics = \App\superadmin\RestSubmenuAllergicMap::getSubmenuAllergenics($submenu["id"]);
                ?>
                <tr class="lie-submenu-row"
                    @if ($price_row)
                    @if ($hasAnyExtras)
                    onclick="$('#lie-submenu-extra-modal-{{ $submenu["id"] }}').modal('show')"
                    @else
                    onclick="addCartItem({{ $rest_detail->id }}, {{ $submenu["id"] }}, [], cartChangeSuccess)"
                    @endif
                    @endif
                >
                    <td class="lie-submenu-description-cell">
                        <span class="lie-menu-name">{{ $submenu["name"] }}</span>
                        <span class="pull-right">
                        @foreach($allergenics as $allergenic_id)
                            {!! lie_allergic_symbol($allergenic_id, 16) !!}
                        @endforeach
                        &nbsp; &nbsp; &nbsp;
                        </span>
                        <br/>
                        <span class="lie-menu-info">{{ $submenu["info"] }}</span>
                    </td>
                    <td class="lie-submenu-price-cell">
                        @if ($price_row)
                            {{ currency_format($price_row->price) }}
                        @else
                            @if (\Illuminate\Support\Facades\Session::get("is_pickup"))
                                Nur Lieferung
                            @else
                                Nur Abholung
                            @endif
                        @endif
                    </td>
                </tr>
                @if ($hasAnyExtras)
                    <div class="modal fade" tabindex="-1" role="dialog"
                         id="lie-submenu-extra-modal-{{ $submenu["id"] }}">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Extras für {{ $submenu["name"] }}</h4>
                                </div>
                                <div class="modal-body">
                                    @foreach($extras as $extra)
                                        <h4 class="lie-extra-header">{{ $extra->name }}</h4>
                                        <div class="lie-extra-container" data-extra="{{ $extra->id }}"
                                             data-type="{{ $extra->type }}">
                                            @if($extra->type == "multiple-choice")
                                                <div class="row">
                                                    @foreach(\App\superadmin\ExtraChoiceElement::getByExtraChoice($extra->id) as $element)
                                                        <div class="col-xs-6">
                                                            {!! lie_checkbox($element->id, $element->name . " <span class='lie-extra-price-span'>(+ " . currency_format($element->price) . ")</span>") !!}
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @else
                                                <select class="form-control">
                                                    @foreach(\App\superadmin\ExtraChoiceElement::getByExtraChoice($extra->id) as $element)
                                                        <option value="{{ $element->id }}">{{ $element->name }}
                                                            (+ {{currency_format($element->price)}}
                                                            )
                                                        </option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen
                                    </button>
                                    <button type="button" class="btn btn-success" data-dismiss="modal"
                                            onclick="addCartItemWithExtras({{ $rest_detail->id }}, {{ $submenu["id"] }})">
                                        Zum Warenkorb
                                        hinzufügen
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <tr class="lie-submenu-spacer">
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endforeach
@endforeach

<div style="margin-top: 10px; border-top: 1px solid lightgrey;">

    <div style="text-align: center; font-size: 22px;">
        <br>
        Information über Allergene u. Zusatzstoffe
        <br>
        <br>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/gluten.png')}}">
                <br>
                <br>
                GLUTENHALTIGES GETREIDE UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/eier.png')}}">
                <br>
                <br>
                EIER VON GEFLÜGEL UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/fisch.png')}}">
                <br>
                <br>
                FISCH UND DARAUS GEWONNENE ERZEUGNISSE (AUSSER FISCHGELATINE)
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/schalentiere.png')}}">
                <br>
                <br>
                KREBSTIERE UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/erdnuss.png')}}">
                <br>
                <br>
                ERDNÜSSE UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/nuesse.png')}}">
                <br>
                <br>
                SCHALENFRÜCHTE UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/sesam.png')}}">
                <br>
                <br>
                SESAMSAMEN UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/soja.png')}}">
                <br>
                <br>
                SOJABOHNEN UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <br>
            <br>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/milch.png')}}">
                <br>
                <br>
                MILCH VON SÄUGETIEREN UND MILCHERZEUGNISSE (INKLUSIVE LAKTOSE)
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/senf.png')}}">
                <br>
                <br>
                SENF UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/sellerie.png')}}">
                <br>
                <br>
                SELLERIE UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/schwefeldioxid.png')}}">
                <br>
                <br>
                SCHWEFELDIOXID UND SULFITE
            </div>
        </div>

        <br>
        <br>
        <div class="row">
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/lupinen.png')}}">
                <br>
                <br>
                LUPINEN UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-3" style="text-align:center;">
                <img width="80%" src="{{url('/public/front/allergen_bilder/weichtiere.png')}}">
                <br>
                <br>
                WEICHTIERE WIE SCHNECKEN, MUSCHELN, TINTENFISCHE UND DARAUS GEWONNENE ERZEUGNISSE
            </div>
            <div class="col-xs-6" style="text-align:center; padding: 20px;">
                <br>
                <span style="font-size: 12px;">Trotz größtmöglicher Sorgfalt bei der Herstellung können Spuren von allergenen Stoffen nicht ausgeschlossen werden</span>
                <br>
                <br>
                <span style="font-size: 24px;">Bei Fragen sind wir jederzeit gerne für sie da.</span>
                <br>
                <br>
                <span style="font-size: 24px;">info@lieferzonas.at</span>
            </div>
        </div>
        <br>
        <br>
        <div style="text-align: center; font-size: 16px;">
            Lieferzonas.at ist ein Mitglied der österreichischen Wirtschaftskammer
        </div>
    </div>


</div>