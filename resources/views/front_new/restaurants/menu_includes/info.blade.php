<?php
$dayTimeMaps = \App\superadmin\RestDayTimeMap::getRestaurantOpenHours($rest_detail->id);
$dayTimeArray = array(
        "1" => array(),
        "2" => array(),
        "3" => array(),
        "4" => array(),
        "5" => array(),
        "6" => array(),
        "7" => array()
);

if ($dayTimeMaps->count() > 0) {
    foreach ($dayTimeMaps as $dayTimeMap) {
        array_push($dayTimeArray[$dayTimeMap->day_id], $dayTimeMap->open_time . " - " . $dayTimeMap->close_time);
    }
}

function echoHoursArray($array)
{
    if (count($array) == 0) {
        echo "Geschlossen";
        return;
    }
    $counter = 0;
    foreach ($array as $interval) {
        if ($counter > 0) {
            echo ", ";
        }
        echo $interval;
        $counter++;
    }
}

$rest_owner = \App\superadmin\RestOwner::getByRestaurantId($rest_detail->id);

?>

<div style="height: 410px;" id="lie-map-container">

</div>

<div class="lie-restaurant-info">
    <br/>
    Öffnungszeiten:<br/>
    <br/>
    <table class="lie-open-hours-table" width="100%">
        <tbody>
        <tr>
            <td>Montag:</td>
            <td>{{ echoHoursArray($dayTimeArray["1"]) }}</td>
        </tr>
        <tr>
            <td>Dienstag:</td>
            <td>{{ echoHoursArray($dayTimeArray["2"]) }}</td>
        </tr>
        <tr>
            <td>Mittwoch:</td>
            <td>{{ echoHoursArray($dayTimeArray["3"]) }}</td>
        </tr>
        <tr>
            <td>Donnerstag:</td>
            <td>{{ echoHoursArray($dayTimeArray["4"]) }}</td>
        </tr>
        <tr>
            <td>Freitag:</td>
            <td>{{ echoHoursArray($dayTimeArray["5"]) }}</td>
        </tr>
        <tr>
            <td>Samstag:</td>
            <td>{{ echoHoursArray($dayTimeArray["6"]) }}</td>
        </tr>
        <tr>
            <td>Sonntag:</td>
            <td>{{ echoHoursArray($dayTimeArray["7"]) }}</td>
        </tr>
        </tbody>
    </table>
    <br/>
    Dieser Zustelldienst liefert in folgende Postleitzahlen:<br/>
    <div class="row">
        @foreach(\App\superadmin\RestZipcodeDeliveryMap::getRestaurantDeliveredZipcodes($rest_detail->id) as $zipcode)
            <div class="col-xs-4">{{ $zipcode->zipcode . " " . $zipcode->description . "" }}</div>
        @endforeach
    </div>
    <br/>
    Restaurant Impressum:<br/>
    <br/>
    <div class="row">
        <div class="col-xs-4">
            Firma:<br/>
            Inhaber:<br/>
            Steuernummer:<br/>
            E-Mail:<br/>
        </div>
        <div class="col-xs-8">
            @if ($rest_owner)
                [Firma]<br/>
                {{ $rest_owner->firstname }} {{ $rest_owner->lastname }}<br/>
                [Steuernummer]<br/>
                {{ $rest_owner->email }}<br/>
            @endif
        </div>
    </div>
</div>