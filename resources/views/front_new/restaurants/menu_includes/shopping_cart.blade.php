<div class="lie-white-rounded" id="lie-shopping-cart-header">
    Warenkorb
</div>
<div class="lie-white-rounded" id="lie-shopping-cart-pickup-toggle">
    <input type="checkbox" class="lie-pickup-toggle"
           {{ \Illuminate\Support\Facades\Session::get("is_pickup", false) ? "" : "checked" }}
           data-toggle="toggle"
           data-on="Zustellung" data-off="Abholung"
           data-onstyle="success" data-offstyle="success"
           data-height="20">
</div>
<div class="lie-white-rounded" id="lie-shopping-cart-my-order">
    Meine Bestellung <span class="pull-right"><i class="fa fa-trash-o"
                                                 onclick="clearCart({{ $rest_detail->id }}, cartChangeSuccess)"
                                                 style="cursor: pointer"></i></span>
</div>

<script type="text/template" id="lie-shopping-cart-template">
    <% for (var i = 0; i < cart.items.length; i++) { var element = cart.items[i]; %>
    <div class="lie-white-rounded lie-cart-element-container">
        <span class="lie-cart-item-count"><%- element.amount %></span> &nbsp; <span class="lie-cart-item-name"><%- element.submenu_display.name %></span>
        <span class="pull-right lie-cart-item-price"><%- currency_format(element.submenu_display.price) %></span> <br/>

        <% for (var e = 0; e < element.extras_display.length; e++) { var extra = element.extras_display[e]; %>
        <span class="lie-cart-extra-bullet">+</span> <span class="lie-cart-extra-name"><%- extra.name %></span> <span
                class="pull-right lie-cart-extra-price"><%- currency_format(extra.price) %></span> <br/>
        <% } %>
        <% if (element.amount <= 1) { %>
        <% if (element.display_total_price != element.submenu_display.price) { %>
        <span class="pull-right"><%- currency_format(element.display_total_price) %></span><br/>
        <% } %>
        <% } else { %>
        <span class="pull-right"><%- element.amount %>&times; <%- currency_format(element.display_single_price) %></span><br/>
        <span class="pull-right"><%- currency_format(element.display_total_price) %></span>
        <% } %>
        <br>
        <span class="lie-cart-item-buttons" style="display: inline-block; text-align: center; width: 100%;">
            <i class="fa fa-minus" style="cursor: pointer" onclick="addToCartItemCount({{ $rest_detail->id }}, <%- i %>, -1, cartChangeSuccess)"></i>
            <i class="fa fa-plus" style="cursor: pointer" onclick="addToCartItemCount({{ $rest_detail->id }}, <%- i %>, +1, cartChangeSuccess)"></i>
            <i class="fa fa-trash" style="cursor: pointer" onclick="removeCartItem({{ $rest_detail->id }}, <%- i %>, cartChangeSuccess)"></i>
        </span>
    </div>
    <% } %>

    <div class="lie-white-rounded lie-cart-element-container">
        <table width="100%" class="lie-cart-calc-table">
            <tbody>
            <% if (cart.minimum_order_amount) { %>
            <tr>
                <td>Mindestbestellwert:</td>
                <td class="lie-cart-calc-price-cell"><%- currency_format(cart.minimum_order_amount) %></td>
            </tr>
            <% } if (cart.below_order_delivery_cost) { %>
            <tr>
                <td>Zustellgebühr unter dem Mindestbestellwert:</td>
                <td class="lie-cart-calc-price-cell"><%- currency_format(cart.below_order_delivery_cost) %></td>
            </tr>
            <% } if (cart.missing_to_minimum) { %>
            <tr>
                <td>Zum Mindestbestellwert fehlen:</td>
                <td class="lie-cart-calc-price-cell"><%- currency_format(cart.missing_to_minimum) %></td>
            </tr>
            <% } if (cart.delivery_cost) { %>
            <tr>
                <td>Zustellgebühr:</td>
                <td class="lie-cart-calc-price-cell"><%- currency_format(cart.delivery_cost) %></td>
            </tr>
            <% } %>
            </tbody>
        </table>
    </div>

    <div class="lie-white-rounded lie-cart-element-container">
        <span class="lie-cart-sum-text">Summe:</span> <span class="pull-right lie-cart-sum-amount"><%- currency_format(cart.cart_total) %></span>
        <div style="text-align: center; padding-bottom: 15px;">
            <button type="button" class="btn btn-success" onclick="window.location.href=baseUrl+'/checkout/{{ $rest_detail->id }}'">ZAHLEN</button>
        </div>
        <img src="{{url('public/front/payment-methods.PNG')}}" style="width: 100%">
    </div>
</script>