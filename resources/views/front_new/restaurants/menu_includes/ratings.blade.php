<?php
$restaurant_rating = \App\front\UserOrderReview::rest_review_rating($rest_detail->id);
$restaurant_rating_count = \App\front\UserOrderReview::rest_review_count($rest_detail->id);
$restaurant_rating_keys = \App\front\UserOrderReview::rest_review_keys($rest_detail->id);

$rating_counts = [$restaurant_rating_keys[0], $restaurant_rating_keys[1], $restaurant_rating_keys[2], $restaurant_rating_keys[3], $restaurant_rating_keys[4]];
if ($restaurant_rating_count == 0) {
    $rating_percentages = [0, 0, 0, 0, 0];
}
else {
    $rating_percentages = [
            round(100 * ($rating_counts[0] / $restaurant_rating_keys[5])),
            round(100 * ($rating_counts[1] / $restaurant_rating_keys[5])),
            round(100 * ($rating_counts[2] / $restaurant_rating_keys[5])),
            round(100 * ($rating_counts[3] / $restaurant_rating_keys[5])),
            round(100 * ($rating_counts[4] / $restaurant_rating_keys[5]))
    ];
}


$quality_rating = \App\front\UserOrderReview::rest_quality_rating($rest_detail->id);
$service_rating = \App\front\UserOrderReview::rest_service_rating($rest_detail->id);

$restaurant_reviews = \App\front\UserOrderReview::getRestaurantReviews($rest_detail->id);

?>

<div id="lie-ratings-top">
    <div class="row">
        <div class="col-xs-4" style="text-align: center">
            <div style="font-size: 24px; padding-top: 16px;"><b>{{ lie_rating_text($restaurant_rating) }}</b></div>
            <div style="font-size: 24px">
                <span class="lie-rating-golden">
                    {!! lie_rating_stars($restaurant_rating) !!}
                </span>
            </div>
            <div style="font-size: 18px">
                <span>{{ lie_rating_format($restaurant_rating) }} / {{ lie_rating_format(5.0) }}</span>
            </div>
        </div>
        <div class="col-xs-8">
            <div class="row">
                <div class="col-xs-5">
                    <nobr><b>5 Sterne</b> <span class="lie-gray-subtext">({{ lie_rating_text(5) }})</span></nobr>
                </div>
                <div class="col-xs-5">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                             aria-valuemax="100" style="width:{{ $rating_percentages[4] }}%">
                            <span class="sr-only">70% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1" style="text-align: center">
                    {{ $rating_counts[4] }}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5">
                    <nobr><b>4 Sterne</b> <span class="lie-gray-subtext">({{ lie_rating_text(4) }})</span></nobr>
                </div>
                <div class="col-xs-5">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                             aria-valuemax="100" style="width:{{ $rating_percentages[3] }}%">
                            <span class="sr-only">70% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1" style="text-align: center">
                    {{ $rating_counts[3] }}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5">
                    <nobr><b>3 Sterne</b> <span class="lie-gray-subtext">({{ lie_rating_text(3) }})</span></nobr>
                </div>
                <div class="col-xs-5">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                             aria-valuemax="100" style="width:{{ $rating_percentages[2] }}%">
                            <span class="sr-only">70% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1" style="text-align: center">
                    {{ $rating_counts[2] }}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5">
                    <nobr><b>2 Sterne</b> <span class="lie-gray-subtext">({{ lie_rating_text(2) }})</span></nobr>
                </div>
                <div class="col-xs-5">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                             aria-valuemax="100" style="width:{{ $rating_percentages[1] }}%">
                            <span class="sr-only">70% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1" style="text-align: center">
                    {{ $rating_counts[1] }}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5">
                    <nobr><b>1 Stern</b> &nbsp; <span class="lie-gray-subtext">({{ lie_rating_text(1) }})</span></nobr>
                </div>
                <div class="col-xs-5">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0"
                             aria-valuemax="100" style="width:{{ $rating_percentages[0] }}%">
                            <span class="sr-only">70% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1" style="text-align: center">
                    {{ $rating_counts[0] }}
                </div>
            </div>
        </div>
    </div>

    <br/>
    <br/>

    <table width="100%">
        <tbody>
        <tr>
            <td width="60%" class="lie-rating-key-cell">Qualität</td>
            <td width="40%" class="lie-rating-rating-cell"><span class="lie-rating-golden">{!! lie_rating_stars($quality_rating) !!}</span> &nbsp; &nbsp; {{ lie_rating_format($quality_rating) }} {{ lie_rating_text($quality_rating) }}</td>
        </tr>
        <tr>
            <td class="lie-rating-key-cell">Service</td>
            <td class="lie-rating-rating-cell"><span class="lie-rating-golden">{!! lie_rating_stars($service_rating) !!}</span> &nbsp; &nbsp; {{ lie_rating_format($service_rating) }} {{ lie_rating_text($service_rating) }}</td>
        </tr>
        </tbody>
    </table>

</div>
<div id="lie-ratings-bottom">
    <div class="lie-rating-single">

    </div>
    <div class="lie-rating-single">

    </div>
    @foreach($restaurant_reviews as $review)
        <?php
            $user_details = \App\front\FrontUserDetail::getUserDetailsById($review->user_id);
            $rating = ($review->quality_rating + $review->service_rating) / 2;
        ?>
        <div class="lie-rating-single">
            <div class="lie-rating-single-top">
                <span class="lie-rating-golden">
                    {!! lie_rating_stars($rating) !!}
                </span>
                    &nbsp; &nbsp; &nbsp;
                <span class="lie-gray-subtext">
                    ({{ lie_rating_text($rating) }})
                </span>
            </div>
            <div class="lie-rating-single-main">
                <div class="media-left">
                    <img class="lie-user-profile-pic-64" width="64" height="64" src="{{ \App\front\FrontUserDetail::getProfilePictureForDisplay($review->user_id) }}">
                </div>
                <div class="media-body">
                    {{ $review->comment }}
                </div>
            </div>
            <div class="lie-rating-single-bottom">
                von: {{ $user_details->fname }} {{ $user_details->lname }} am {{ strftime("%d. %B %Y %H:%M", strtotime($review->created_at)) }}
            </div>
        </div>
    @endforeach
</div>