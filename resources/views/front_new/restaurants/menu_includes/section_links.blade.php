<ul>
    @foreach(\App\superadmin\RestCategory::getByRestaurant($rest_detail->id) as $category)
        @foreach(\App\superadmin\RestMenu::getMenuByCategory($category->id) as $menu)
            <li><a class="lie-section-link" href="#lie-menu-{{ $menu["id"] }}">{{ $menu["name"] }}</a></li>
        @endforeach
    @endforeach
</ul>