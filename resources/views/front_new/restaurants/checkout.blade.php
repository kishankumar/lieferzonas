@extends('front_new.layout')

@section('title')
    Lieferzonas - Checkout
@endsection

@section('head')
    <style>

        .lie-address-form-row {
            width: 100%;
            margin-bottom: 10px;
        }

        .lie-status-container {
            display: inline-block;
            width: 3%;
            text-align: center;
        }

        .lie-required-container {
            display: inline-block;
            width: 3%;
            text-align: center;
        }

        .lie-input-container {
            display: inline-block;
            width: 43%;
        }

        .lie-space {
            display: inline-block;
            width: 2%;
        }

        .lie-newsletter-label-strong {
            font-weight: bolder;
        }

        .lie-newsletter-label-weak {
            font-weight: lighter;
            font-size: 12px;
        }

        .lie-preorder-time-control {
            color: #5cb85c;
            border: 1px solid #47d5ad;
        }

    </style>
@endsection

<?php
$form_statuses = \Illuminate\Support\Facades\Session::get("form_statuses");

?>

@section('contents')
    <div class="lie-contents">
        @include("front_new.restaurants.checkout_includes.restaurant_header")
        <div class="row">
            <div class="col-xs-8">

                <!-- Dateneingabe -->
                <div class="lie-container" style="padding: 25px">

                    <!-- Vorname / Nachname -->
                    <div class="lie-address-form-row">
                        <div class="lie-status-container" data-field="first-name"></div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Vorname" class="form-control" id="lie-input-first-name">
                        </div><div class="lie-space"></div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Nachname" class="form-control" id="lie-input-last-name">
                        </div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-status-container" data-field="last-name"></div>
                    </div>

                    <!-- Strasse / Hausnummer -->
                    <div class="lie-address-form-row">
                        <div class="lie-status-container" data-field="street"></div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Strasse" class="form-control" id="lie-input-street">
                        </div><div class="lie-space"></div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Hausnummer" class="form-control" id="lie-input-house">
                        </div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-status-container" data-field="house"></div>
                    </div>

                    <!-- Stiege / Stock -->
                    <div class="lie-address-form-row">
                        <div class="lie-status-container" data-field="staircase"></div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Stiege" class="form-control" id="lie-input-staircase">
                        </div><div class="lie-space"></div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Stock" class="form-control" id="lie-input-floor">
                        </div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-status-container" data-field="floor"></div>
                    </div>

                    <!-- Türnummer / Postleitzahl -->
                    <div class="lie-address-form-row">
                        <div class="lie-status-container" data-field="apartment"></div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Türnummer" class="form-control" id="lie-input-apartment">
                        </div><div class="lie-space"></div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Postleitzahl" class="form-control" id="lie-input-zipcode">
                        </div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-status-container" data-field="zipcode"></div>
                    </div>

                    <!-- Stadt / Firma -->
                    <div class="lie-address-form-row">
                        <div class="lie-status-container" data-field="city"></div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Stadt" class="form-control" id="lie-input-city">
                        </div><div class="lie-space"></div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Firma" class="form-control" id="lie-input-company">
                        </div><!--
                        --><div class="lie-required-container"></div><!--
                        --><div class="lie-status-container" data-field="company"></div>
                    </div>

                    <!-- Geburtsdatum / Telefonnummer -->
                    <div class="lie-address-form-row">
                        <div class="lie-status-container" data-field="dob"></div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-input-container">
                            <input type="date" class="form-control" id="lie-input-dob" {!! lie_tooltip("Geburtsdatum") !!}>
                        </div><div class="lie-space"></div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="Telefonnummer" class="form-control" id="lie-input-phone">
                        </div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-status-container" data-field="phone"></div>
                    </div>

                    <!-- Geburtsdatum / Telefonnummer -->
                    <div class="lie-address-form-row">
                        <div class="lie-status-container" data-field="email"></div><!--
                        --><div class="lie-required-container">*</div><!--
                        --><div class="lie-input-container">
                            <input type="text" placeholder="E-Mail" class="form-control" id="lie-input-email">
                        </div><div class="lie-space"></div><!--
                        --><div class="lie-input-container"></div><!--
                        --><div class="lie-required-container"></div><!--
                        --><div class="lie-status-container"></div>
                    </div>

                    <!-- Kommentar / Newsletter -->
                    <div class="lie-address-form-row">
                        <div class="lie-status-container"></div><!--
                        --><div class="lie-required-container"></div><!--
                        --><div class="lie-input-container" style="vertical-align: top;">
                            <textarea placeholder="Kommentar schreiben" class="form-control" id="lie-input-comment"></textarea>
                        </div><div class="lie-space"></div><!--
                        --><div class="lie-input-container">
                            <table width="100%">
                                <tbody>
                                <tr>
                                    <td width="20%" style="text-align: center;">
                                        {!! lie_general_checkbox("subscription", "") !!}
                                    </td>
                                    <td width="80%" style="padding: 15px 0px;">
                                        <span class="lie-newsletter-label-strong">Ja, Ich möchte über aktuelle</span><br>
                                        <span class="lie-newsletter-label-weak">Aktionen, Neukunden-Rabatte, Stempelkartenprogramme, CA$HBACK, Treuepunkte,
                                Mittagsmenüs, neue Restaurants und Premium-Restaurants</span><br>
                                        <span class="lie-newsletter-label-strong">informiert werden</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!--
                        --><div class="lie-required-container"></div><!--
                        --><div class="lie-status-container"></div>
                    </div>

                    * - Erforderliches Feld

                </div>

                <!-- Bestellzeit -->
                <div class="lie-container">
                    <table>
                        <tbody>
                        <tr>
                            <td class="col-xs-8">
                                <div style="padding: 15px;">
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="10%" style="text-align: center"></td>
                                            <td width="25%" style="text-align: left">Sofort</td>
                                            <td width="35%" style="text-align: center">{!! lie_general_radio("order_date_time", "1", "", true) !!}</td>
                                            <td width="30%" style="text-align: center"></td>
                                        </tr>
                                        <tr>
                                            <td><div style="padding-top: 7px"></div></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr style="padding: 5px 0px; vertical-align: top;">
                                            <td style="text-align: center"></td>
                                            <td style="text-align: left">Vorbestellen</td>
                                            <td style="text-align: center">{!! lie_general_radio("order_date_time", "2", "") !!}</td>
                                            <td style="text-align: center">
                                                <nobr>
                                                    <select class="form-control lie-preorder-time-control" name="preorder_day" id="lie-preorder-day-selection">
                                                        <option value="today">Heute</option>
                                                        <option value="tomorrow">Morgen</option>
                                                        <option value="day_after_tomorrow">Übermorgen</option>
                                                    </select>
                                                    <?php $preorder_times = \App\superadmin\RestDayTimeMap::getPreorderTimes($restaurant_id); ?>
                                                    <select class="form-control lie-preorder-time-control" id="lie-preorder-time-today">
                                                        @foreach($preorder_times["today"] as $preorder_time)
                                                            <option>{{ $preorder_time }}</option>
                                                        @endforeach
                                                    </select>
                                                    <select class="form-control lie-preorder-time-control" id="lie-preorder-time-tomorrow" style="display: none">
                                                        @foreach($preorder_times["tomorrow"] as $preorder_time)
                                                            <option>{{ $preorder_time }}</option>
                                                        @endforeach
                                                    </select>
                                                    <select class="form-control lie-preorder-time-control" id="lie-preorder-time-day-after-tomorrow" style="display: none">
                                                        @foreach($preorder_times["day_after_tomorrow"] as $preorder_time)
                                                            <option>{{ $preorder_time }}</option>
                                                        @endforeach
                                                    </select>
                                                </nobr>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                            <td style="font-family: 'Harabara-Hand'; font-weight: bolder; font-size: 15px;">
                                Wann sollen wir es dir zustellen???
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <!-- Payment Method -->
                <div class="lie-container">
                    @include("front_new.restaurants.checkout_includes.payment_methods")
                </div>

                <!-- Bestellen -->
                @include("front_new.restaurants.checkout_includes.submit")

            </div>
            <div class="col-xs-4">
                @include('front_new.restaurants.checkout_includes.shopping_cart')
                <div id="lie-shopping-cart-container">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {!! Html::script('/public/assets/js/underscore-min.js') !!}

    <script>
        var selectedPaymentMethod = null;

        var cartTemplate = _.template($("#lie-shopping-cart-template").html());

        function selectPaymentMethod(paymentMethod) {
            selectedPaymentMethod = paymentMethod;
            $(".lie-payment-method-box").removeClass("active");
            $(".lie-payment-method-box[data-method='" + paymentMethod + "']").addClass("active");
        }

        function cartChangeSuccess(response) {
            if (response.status == "success") {
                console.dir(JSON.parse(response.cart_json));
                $("#lie-shopping-cart-container").empty();
                $("#lie-shopping-cart-container").html(cartTemplate({"cart": JSON.parse(response.cart_json)}));
            }
        }

        $(function () {
            $("#lie-preorder-day-selection").change(function(evt) {
                var day = $(this).val();
                if (day == "today") {
                    $("#lie-preorder-time-today").show();
                    $("#lie-preorder-time-tomorrow").hide();
                    $("#lie-preorder-time-day-after-tomorrow").hide();
                } else if(day == "tomorrow") {
                    $("#lie-preorder-time-today").hide();
                    $("#lie-preorder-time-tomorrow").show();
                    $("#lie-preorder-time-day-after-tomorrow").hide();
                } else {
                    $("#lie-preorder-time-today").hide();
                    $("#lie-preorder-time-tomorrow").hide();
                    $("#lie-preorder-time-day-after-tomorrow").show();
                }
            });

            getCartJson({{ $restaurant_id }}, cartChangeSuccess);

            $("#lie-submit-order-button").click(function(evt) {

                var fields = ["email","first-name","last-name","street","house","staircase","floor","apartment","zipcode","city","company","dob","phone","comment"];

                var data = {};

                for (var i = 0; i < fields.length; i++) {
                    data[fields[i]] = $("#lie-input-" + fields[i]).val();
                }

                data["restaurant_id"] = '{{ $restaurant_id }}';
                data["order_type"] = "1";

                data["order_date_time"] = $("input[name='order_date_time']:checked").val();
                data["payment_method"] = (selectedPaymentMethod == 'cash' ? 1 : 2);
                data["bonus_point"] = false;
                data["cashback_point"] = false;

                lie_sync_request("{{ csrf_token() }}", "{{ url("/checkout/place_order") }}", data, "POST");
            });
        });

    </script>
@endsection