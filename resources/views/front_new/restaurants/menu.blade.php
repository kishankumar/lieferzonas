@extends('front_new.layout')

@section('title')
    Lieferzonas - Restaurantmenü
@endsection

@section('head')
    <style>

        .lie-white-rounded {
            border-radius: 2px;
            background-color: #FFFFFF;
            margin-bottom: 10px;
            padding: 25px;
            overflow: hidden;
        }

        .lie-restaurant-name {
            font-weight: bold;
        }

        .lie-premium-span {
            color: #bcbcbc;
            margin-right: 58px;
        }

        .lie-bright-star-span {
            color: #fbcf37;
        }

        .lie-dark-star-span {
            color: #bcbcbc;
            margin-right: 18px;
        }

        .lie-rating {
            margin-right: 18px;
            color: #fbcf37;
        }

        .lie-rating-count {

        }

        .navbar-default {
            margin-bottom: 10px;
        }

        .lie-open-hours {
            color: #47d5ad;
        }

        .lie-closed-hours {
            color: #b40000;
        }

        .lie-stamped-circles {
            color: #e63396;
        }

        .lie-unstamped-circles {
            color: #e9e9e9;
            margin-right: 30px;
        }

        .lie-stamp-text {
            color: #e63396;
            font-weight: bold;
        }

        .lie-restaurant-logo {
            padding-right: 25px;
        }

        .tab-content {
            border: 1px solid #ddd;
            border-top: none;
        }

        .lie-open-hours-table {
            font-size: inherit;
        }

        .lie-menu-title {
            margin-bottom: 2px;
        }

        .lie-menu-name {
            font-size: 14px;
            font-weight: bolder;
        }

        .lie-menu-info {
            font-size: 12px;
            font-weight: lighter;
        }

        .lie-submenu-spacer {
            height: 10px;
        }

        /* Section links */
        .lie-section-link, .lie-section-link:hover {
            color: #5b5a5a;
        }

        /* Menu Table */
        .lie-submenu-row {
            cursor: pointer;
        }

        .lie-submenu-row:hover {
            background-color: #edffed;
        }

        .lie-submenu-price-cell {
            background-color: white;
            border: 2px solid #27ae60;
            text-align: center;
            color: black;
        }

        .lie-submenu-row:hover .lie-submenu-price-cell {
            background-color: #27ae60;
            border: 2px solid #27ae60;
            color: white;
        }

        .lie-extra-price-span {
            color: #AAAAAA;
        }

        .progress {
            margin-bottom: 5px;
        }

        .progress-bar {
            background-color: #ffda10;
        }

        .lie-rating-single {
            margin: 20px;
            border-bottom: 1px solid #51df96;
        }

        .lie-rating-single-top {
            padding-bottom: 10px;
        }
        .lie-rating-single-main {
            padding-bottom: 10px;
        }
        .lie-rating-single-bottom {
            padding-bottom: 10px;
        }

    </style>
@endsection

<?php
$rating = \App\front\UserOrderReview::rest_review_rating($rest_detail->id);
$rating_count = \App\front\UserOrderReview::rest_review_count($rest_detail->id);
?>

@section('contents')
    <div class="lie-contents">

        <!-- Header -->
        <div class="row">
            <div class="col-xs-2">
            </div>
            <div class="col-xs-7">
                <div class="lie-white-rounded">
                    @if ($rest_detail->logo && \Illuminate\Support\Facades\File::exists(public_path() . "/uploads/superadmin/restaurants/" . $rest_detail->logo))
                        <div class="media-left lie-restaurant-logo">
                            <img src="{{url('/public/uploads/superadmin/restaurants/'.$rest_detail->logo)}}" width="130"
                                 height="120">
                        </div>
                    @endif
                    <div class="media-body">

                        <span class="lie-restaurant-name">
                            {{ $rest_detail->f_name }}
                        </span>
                        <span class="pull-right">
                            <!--
                            <span class="lie-premium-span">
                                Premium
                            </span>
                            -->
                            <span class="lie-bright-star-span">
                                @for($i = 0; $i < round($rating); $i++)
                                    <i class="fa fa-star"></i>
                                @endfor
                            </span>
                            <span class="lie-dark-star-span">
                                @for($i = 5; $i > round($rating); $i--)
                                    <i class="fa fa-star"></i>
                                @endfor
                            </span>
                            <span class="lie-rating">
                                {{ number_format($rating, 1) }}
                            </span>
                            <span class="lie-rating-count">
                                ({{ $rating_count }})
                            </span>
                        </span>

                        <br/>

                        <span
                                @if ($is_open_now)
                                class="lie-open-hours"
                                @else
                                class="lie-closed-hours"
                                @endif
                        >
                        @if ($closed_today)
                                Heute geschlossen
                            @else
                                <i class="fa fa-clock-o"></i>
                                <?php $interval_counter = 0; ?>
                                @foreach ($open_hours as $interval)
                                    <?php if ($interval_counter > 0) echo ", "; $interval_counter++; ?>
                                    {{ $interval }}
                                @endforeach
                                @if ($is_open_now)
                                    (Geöffnet)
                                @else
                                (Geschlossen)
                                @endif
                            @endif
                        </span>

                        <br/>

                        <span class="lie-minimum-order-span">
                            Mindestbestellwert: {{ currency_format($minimum_order_amount) }}
                            @if($below_minimum_delivery_cost)
                                darunter: {{ currency_format($below_minimum_delivery_cost) }} Zustellgebühr
                            @endif
                        </span>
                        <br/>
                        <span class="lie-delivery-cost-span">
                            Zustellkosten: {{ currency_format($above_minimum_delivery_cost) }}
                        </span>
                        <span class="pull-right lie-distance-text">

                        </span>
                        <br/>
                        <br/>

                        @if($has_stamp_cards)
                            <span class="lie-stamped-circles">
                                @for ($i = 0; $i < $user_stamps; $i++)
                                    <i class="fa fa-dot-circle-o"></i>
                                @endfor
                            </span>
                            <span class="lie-unstamped-circles">
                                @for ($i = $max_stamps; $i > $user_stamps; $i--)
                                    <i class="fa fa-circle-o"></i>
                                @endfor
                            </span>
                            <span class="lie-stamp-text">
                                {{ $user_stamps }} / {{ $max_stamps }} Stempel gesammelt
                            </span>
                        @endif
                    </div>
                </div>

            </div>
            <div class="col-xs-3">
            </div>
        </div>

        <!-- Body -->
        <div class="row">

            <div class="col-xs-2">
                @include('front_new.restaurants.menu_includes.section_links')
            </div>
            <div class="col-xs-7">
                <ul class="nav nav-tabs nav-justified">
                    <li class="lie-tab active"><a data-toggle="tab" href="#items">Menü</a></li>
                    <li class="lie-tab"><a data-toggle="tab" href="#deals">Aktionen</a></li>
                    <li class="lie-tab"><a data-toggle="tab" href="#info" onclick="initializeMapView();">Info</a></li>
                    <li class="lie-tab"><a data-toggle="tab" href="#ratings">Bewertungen</a></li>
                </ul>
                <div class="tab-content lie-white-rounded">
                    <div id="items" class="tab-pane fade in active">
                        @include("front_new.restaurants.menu_includes.items")
                    </div>
                    <div id="deals" class="tab-pane fade">
                        @include("front_new.restaurants.menu_includes.deals")
                    </div>
                    <div id="info" class="tab-pane fade">
                        @include("front_new.restaurants.menu_includes.info")
                    </div>
                    <div id="ratings" class="tab-pane fade">
                        @include("front_new.restaurants.menu_includes.ratings")
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                @include('front_new.restaurants.menu_includes.shopping_cart')
                <div id="lie-shopping-cart-container">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script>
        registerPickupDeliveryToggle('{{ csrf_token() }}',
        function(is_pickup) {
            window.location.reload();
        },
        function() {

        });

        var cartTemplate = _.template($("#lie-shopping-cart-template").html());

        function cartChangeSuccess(response) {
            if (response.status == "success") {
                $("#lie-shopping-cart-container").empty();
                $("#lie-shopping-cart-container").html(cartTemplate({"cart": JSON.parse(response.cart_json)}));
            }
        }

        function addCartItemWithExtras(restaurantId, submenuId) {
            var extras = {};
            $("#lie-submenu-extra-modal-" + submenuId).find(".lie-extra-container").each(function(index, element) {
                var extraId = $(element).data("extra");
                var extraType = $(element).data("type");
                extras[extraId] = [];
                if (extraType == "multiple-choice") {
                    $(element).find("input[type='checkbox']:checked").each(function(index2, element2) {
                        extras[extraId].push($(element2).val());
                    });
                }
                else {
                    extras[extraId] = $(element).find("select").val();
                }
            });
            addCartItem(restaurantId, submenuId, extras, cartChangeSuccess);
        }

        function initializeMapView() {
            setTimeout(function () {
                $.ajax({
                    url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + '{{ $rest_detail->add1 . "+" . $rest_detail->add2 }}'.replace(/\s+/, "+"),
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        if (data.results.length == 0) {
                            console.error("Konnte keine Position für " + this.address + " ermitteln");
                        }
                        else {
                            var latitude = data.results[0].geometry.location.lat;
                            var longitude = data.results[0].geometry.location.lng;
                            console.dir([latitude, longitude]);
                            var map = new google.maps.Map(document.getElementById('lie-map-container'), {
                                zoom: 15,
                                center: {lat: latitude, lng: longitude}
                            });
                            var marker = new google.maps.Marker({
                                position: {lat: latitude, lng: longitude},
                                map: map,
                                title: '{{ $rest_detail->f_name }}'
                            });
                        }
                    }
                });
            }, 300);
        }

        $('#info').on('shown', function (evt) {
            initializeMapView(); // google map init function
        });

        $(function() {
            getCartJson({{ $rest_detail->id }}, cartChangeSuccess);
        });
    </script>

    <!--
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1PwoqBZYkvTjNybm3YvXi3ppqOOO5W2Y&callback=initializeMapView"></script>
    -->

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1PwoqBZYkvTjNybm3YvXi3ppqOOO5W2Y"></script>

@endsection