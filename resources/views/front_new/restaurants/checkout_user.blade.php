@extends('front_new.layout')

@section('title')
    Lieferzonas - Checkout
@endsection

@section('head')
    <style>
        .navbar {
            margin-bottom: 10px;
        }

        .lie-address-row {
            display: table;
            padding: 0px;
            margin: 0px;
            width: 100%;
        }

        .lie-newsletter-label-strong {
            font-weight: bolder;
        }

        .lie-newsletter-label-weak {
            font-weight: lighter;
            font-size: 12px;
        }

        .lie-preorder-time-control {
            color: #5cb85c;
            border: 1px solid #47d5ad;
        }

        #lie-addresses-container {
            overflow-x: scroll;
            white-space: nowrap;
            width: 100%;
        }

    </style>
@endsection

@section('contents')
    <div class="lie-contents">

        @include("front_new.restaurants.checkout_includes.restaurant_header")

        <form id="lie-order-form">
            <div class="row">
                <div class="col-xs-8">
                    <!-- Addresses -->
                    <div>
                        <div id="lie-addresses-container">

                        </div>
                    </div>
                    <!-- Newsletter -->
                    <div class="lie-container" style="margin-top: 5px;">
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="20%" style="text-align: center;">
                                    {!! lie_general_checkbox("subscription", "") !!}
                                </td>
                                <td width="80%" style="padding: 15px 0px;">
                                    <span class="lie-newsletter-label-strong">Ja, Ich möchte über aktuelle</span><br>
                                    <span class="lie-newsletter-label-weak">Aktionen, Neukunden-Rabatte, Stempelkartenprogramme, CA$HBACK, Treuepunkte,
                                Mittagsmenüs, neue Restaurants und Premium-Restaurants</span><br>
                                    <span class="lie-newsletter-label-strong">informiert werden</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- Delivery Time -->
                    <div class="lie-container">
                        <table>
                            <tbody>
                            <tr>
                                <td class="col-xs-8">
                                    <div style="padding: 15px;">
                                        <table width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="10%" style="text-align: center"></td>
                                                <td width="25%" style="text-align: left">Sofort</td>
                                                <td width="35%" style="text-align: center">{!! lie_general_radio("order_date_time", "1", "", true) !!}</td>
                                                <td width="30%" style="text-align: center"></td>
                                            </tr>
                                            <tr>
                                                <td><div style="padding-top: 7px"></div></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr style="padding: 5px 0px; vertical-align: top;">
                                                <td style="text-align: center"></td>
                                                <td style="text-align: left">Vorbestellen</td>
                                                <td style="text-align: center">{!! lie_general_radio("order_date_time", "2", "") !!}</td>
                                                <td style="text-align: center">
                                                    <nobr>
                                                        <select class="form-control lie-preorder-time-control" name="preorder_day" id="lie-preorder-day-selection">
                                                            <option value="today">Heute</option>
                                                            <option value="tomorrow">Morgen</option>
                                                            <option value="day_after_tomorrow">Übermorgen</option>
                                                        </select>
                                                        <?php $preorder_times = \App\superadmin\RestDayTimeMap::getPreorderTimes($restaurant_id); ?>
                                                        <select class="form-control lie-preorder-time-control" id="lie-preorder-time-today">
                                                            @foreach($preorder_times["today"] as $preorder_time)
                                                                <option>{{ $preorder_time }}</option>
                                                            @endforeach
                                                        </select>
                                                        <select class="form-control lie-preorder-time-control" id="lie-preorder-time-tomorrow" style="display: none">
                                                            @foreach($preorder_times["tomorrow"] as $preorder_time)
                                                                <option>{{ $preorder_time }}</option>
                                                            @endforeach
                                                        </select>
                                                        <select class="form-control lie-preorder-time-control" id="lie-preorder-time-day-after-tomorrow" style="display: none">
                                                            @foreach($preorder_times["day_after_tomorrow"] as $preorder_time)
                                                                <option>{{ $preorder_time }}</option>
                                                            @endforeach
                                                        </select>
                                                    </nobr>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                                <td style="font-family: 'Harabara-Hand'; font-weight: bolder; font-size: 15px;">
                                    Wann sollen wir es dir zustellen???
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="lie-container" style="padding: 15px;">
                        <!-- Points -->
                        <?php
                            $availableBonusPoints = \App\front\UserBonusPoint::availablepoint(Auth::user('front')->id);
                            $availableCashbackPoints = \App\front\UserCashbackPoint::availablepoint(Auth::user('front')->id);
                            $pointValue = \App\superadmin\SuperAdminSetting::where('status', 1)->select('cashback_point_value', 'bonus_point_value')->first();
                            $bonusPointsValue = $availableBonusPoints / $pointValue->bonus_point_value;
                            $cashbackPointsValue = $availableCashbackPoints / $pointValue->cashback_point_value;
                            $bonusPointThreshold = 500;
                            $cashbackPointThreshold = 500;
                        ?>
                        <div class="row">
                            <div class="col-xs-6">
                                <span class="lie-paragraph-header">Möchtest du deine Punkte einlösen?</span><br>
                                und dafür eine Gutschrift erhalten!<br>
                                <br>
                                <span class="lie-paragraph-header">Du hast derzeit</span><br>
                                <span style="font-weight: bold;"><span class="lie-open-color">{{ $availableBonusPoints }}</span> Treuepunkte</span><br>
                                <span style="font-weight: bold;"><span class="lie-open-color">{{ $availableCashbackPoints }}</span> CA$HBACK-Punkte</span><br>
                            </div>
                            <div class="col-xs-6">
                                <div class="row">
                                    @if($availableBonusPoints >= $bonusPointThreshold)
                                    <div class="col-xs-2">
                                        {!! lie_general_checkbox("redeem_bonus", "", false, "lie-redeem-bonus-checkbox") !!}
                                    </div>
                                    <div class="col-xs-10">
                                        <span class="lie-paragraph-header">Ja, ich möchte meine Treuepunkte einlösen.</span><br>
                                        <span class="lie-open-color">Mir werden bis zu {{ currency_format($bonusPointsValue) }} von der Bestellung abgezogen</span>
                                    </div>
                                    @else
                                    <div class="col-xs-2">

                                    </div>
                                    <div class="col-xs-10">
                                        <span class="lie-paragraph-header">Mindestens {{ $bonusPointThreshold }} Treuepunkte zum einlösen erforderlich</span>
                                        <br/>
                                        <span class="lie-open-color">Bewerte Bestellungen um Treuepunkte zu erhalten</span>
                                        <br/>
                                    </div>
                                    @endif
                                    @if($availableCashbackPoints >= $cashbackPointThreshold)
                                    <div class="col-xs-2">
                                        {!! lie_general_checkbox("redeem_cashback", "", false, "lie-redeem-cashback-checkbox") !!}
                                    </div>
                                    <div class="col-xs-10">
                                        <span class="lie-paragraph-header">Ja, ich möchte meine CA$HBACK-Punkte einlösen.</span><br>
                                        <span class="lie-open-color">Mir werden bis zu {{ currency_format($cashbackPointsValue) }} von der Bestellung abgezogen</span>
                                    </div>
                                    @else
                                        <div class="col-xs-2">

                                        </div>
                                        <div class="col-xs-10">
                                            <span class="lie-paragraph-header">Mindestens {{ $cashbackPointThreshold }} Cashback-Punkte zum einlösen erforderlich</span>
                                            <br/>
                                            <span class="lie-open-color">Bestelle bei Restaurants, die CA$HBACK anbieten um CA$HBACK-Punkte zu erhalten</span>
                                            <br/>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- Payment -->
                        <br>
                        <br>
                        @include("front_new.restaurants.checkout_includes.payment_methods")

                    </div>
                    <!-- Submit -->
                    @include("front_new.restaurants.checkout_includes.submit")

                </div>
                <div class="col-xs-4">
                    @include('front_new.restaurants.checkout_includes.shopping_cart')
                    <div id="lie-shopping-cart-container">

                    </div>
                </div>
            </div>
        </form>
    </div>

    @include('front_new.includes.add_address_modal', ['callback' => 'addAddressCallback'])

    @include('front_new.includes.edit_address_modal', ['callback' => 'editAddressCallback'])

@endsection

@section('scripts')
    {!! Html::script('/public/assets/js/underscore-min.js') !!}


    @include("front_new.includes.address_display_templates")

    <script>
        var addresses = null;

        var addressElementTemplate = _.template($("#lie-address-element-template").html());
        var addAddressTemplate = _.template($("#lie-add-address-template").html());
        var cartTemplate = _.template($("#lie-shopping-cart-template").html());

        var selectedPaymentMethod = null;
        var selectedAddress = null;

        function onClickEditAddress(addressId) {
            var address = addresses.find(function(element) {
                return element.id == addressId;
            });
            if (address) {
                showEditAddressModal(address);
            }
        }

        function editAddressCallback(response) {
            if (response.status == "success") {
                refreshAddresses();
            }
            else if(response.reason) {
                alert(response.reason);
            }
        }

        function addAddressCallback(response) {
            if (response.status == "success") {
                refreshAddresses();
            }
            else if(response.reason) {
                alert(response.reason);
            }
        }

        function deleteAddressCallback(response) {
            if (response.status == "success") {
                refreshAddresses();
            }
            else if(response.reason) {
                alert(response.reason);
            }
        }

        function selectAddress(addressId) {
            selectedAddress = addressId;
            $(".lie-address-container").removeClass("active");
            $(".lie-address-container[data-address='" + addressId + "']").addClass("active");
            var address = addresses.find(function(element) {
                return element.id == addressId;
            });
            if (address) {
                setAddressInCart(address);
            }
        }

        function selectPaymentMethod(paymentMethod) {
            selectedPaymentMethod = paymentMethod;
            $(".lie-payment-method-box").removeClass("active");
            $(".lie-payment-method-box[data-method='" + paymentMethod + "']").addClass("active");
        }

        function refreshAddresses() {
            $.ajax({
                url: '{{ url("front/json/addresses") }}',
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    if (response.status == "failure") return console.error("Konnte keine Adressen laden: " + response.reason + "!");
                    addresses = response;
                    $("#lie-addresses-container").empty();
                    for (var i = 0; i < response.length; i++) {
                        var address = response[i];
                        $("#lie-addresses-container").append(addressElementTemplate({"address": address}));
                    }
                    $("#lie-addresses-container").append(addAddressTemplate({}));
                }
            });
        }

        function cartChangeSuccess(response) {
            if (response.status == "success") {
                console.dir(JSON.parse(response.cart_json));
                $("#lie-shopping-cart-container").empty();
                $("#lie-shopping-cart-container").html(cartTemplate({"cart": JSON.parse(response.cart_json)}));
            }
        }

        $(function () {
            refreshAddresses();

            $("#lie-preorder-day-selection").change(function(evt) {
                var day = $(this).val();
                if (day == "today") {
                    $("#lie-preorder-time-today").show();
                    $("#lie-preorder-time-tomorrow").hide();
                    $("#lie-preorder-time-day-after-tomorrow").hide();
                } else if(day == "tomorrow") {
                    $("#lie-preorder-time-today").hide();
                    $("#lie-preorder-time-tomorrow").show();
                    $("#lie-preorder-time-day-after-tomorrow").hide();
                } else {
                    $("#lie-preorder-time-today").hide();
                    $("#lie-preorder-time-tomorrow").hide();
                    $("#lie-preorder-time-day-after-tomorrow").show();
                }
            });

            getCartJson({{ $restaurant_id }}, cartChangeSuccess);
        });

        $(function() {

            $("#lie-submit-order-button").click(function() {
                lie_sync_request("{{ csrf_token() }}", "{{ url("/checkout/place_order") }}", {
                    "restaurant_id": '{{ $restaurant_id }}',
                    "order_type": "1",
                    "order_date_time": $("input[name='order_date_time']:checked").val(),
                    "accept_terms_and_conditions": $("#lie-accept-terms").prop("checked"),
                    "payment_method": (selectedPaymentMethod == 'cash' ? 1 : 2),
                    "address": selectedAddress,
                    "bonus_point": $("#lie-redeem-bonus-checkbox").prop("checked"),
                    "cashback_point": $("#lie-redeem-cashback-checkbox").prop("checked")
                }, "POST");
            });



        });
    </script>
@endsection