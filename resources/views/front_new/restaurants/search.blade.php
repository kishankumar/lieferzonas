@extends('front_new.layout')

@section('head')

    <!-- noUISlider -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/9.0.0/nouislider.min.css">

    <style>

        .white-rounded {
            border-radius: 2px;
            background-color: #FFFFFF;
            margin-bottom: 10px;
            padding-left: 0px;
            padding-right: 0px;
            overflow: hidden;
        }

        .lie-restaurant-entry-container {
            padding: 12px;
            font-size: 13px;
            position: relative;
            cursor: pointer;
            text-align: left;
        }

        .lie-restaurant-closed-overlay {
            position: absolute;
            left: 0px;
            right: 0px;
            top: 0px;
            bottom: 0px;
            background-color: rgba(255, 255, 255, 0.5);
        }

        .navbar-default {
            margin-bottom: 7px;
        }

        /* Filter segments */
        #lie-all-filters {
            padding: 15px;
        }

        .lie-filter-header {
            padding: 7px 15px;
            border-top: 1px solid lightgrey;
        }

        .lie-filter-header-table {
            width: 100%;
        }

        .lie-filter-header-label {

        }

        .lie-filter-header-reset {
            padding: 5px;
            background-color: #edffed;
            text-align: right;
            width: 1%;
            white-space: nowrap;
            color: #27ae60;
            cursor: pointer;
        }

        .lie-filter-body {
            padding: 15px;
            border-top: 1px solid lightgrey;
        }

        /* Checkbox styling */

        .lie-filter-checkbox {
            display: block;
            font-weight: lighter;
            padding-left: 10px;
            border: 1px solid rgba(0, 0, 0, 0);
        }

        .lie-filter-checkbox.lie-filter-checked {
            color: #27ae60;
            border: 1px solid #27ae60;
        }

        .lie-checkbox-column {
            padding-left: 5px;
            padding-right: 5px;
        }

        .lie-filter-checkbox-input {
            display: none;
        }

        .lie-checkbox-icon {
            padding-right: 6px;
        }

        /* Slider styling */

        .noUi-base {
            margin-top: 5px;
            background: lightgrey;
            height: 2px;
        }

        .noUi-connect {
            background: #27ae60;
            height: 2px;
        }

        .noUi-target,
        .noUi-marker,
        .noUi-background {
            background: rgba(0, 0, 0, 0);
            border: none;
            height: 0px;
        }

        .noUi-handle {
            background-color: white;
            border: 1px solid #27ae60;
            box-shadow: none;
            border-radius: 20px;
        }

        .noUi-horizontal .noUi-handle {
            width: 15px;
            height: 15px;
            left: 0px;
            right: 0px;
        }

        .noUi-handle:before,
        .noUi-handle:after {
            content: none;
        }

        .lie-double-sided-slider {
            margin-left: 5px;
            margin-right: 20px;
            margin-bottom: 10px;
        }

        .slider-label-left {
            width: 30%;
            text-align: left;
        }

        .slider-label-middle {
            width: 40%;
            text-align: center;
        }

        .slider-label-right {
            width: 30%;
            text-align: right;
        }

        .slider-label {
            width: 100%;
            font-size: 12px;
        }

        /* Bonus boxes */

        .lie-deal-boxes-container {
            height: 60px;
        }

        .lie-aktion-box, .lie-new-customer-box, .lie-lunch-menu-box, .lie-stamp-box, .lie-no-deals-box {
            display: inline-block;
            margin: 10px;
            margin-left: 0px;
            padding: 3px 10px;
            text-align: center;
            color: white;
        }

        .lie-aktion-box {
            background-color: #fd4c45;
        }

        .lie-new-customer-box {
            background-color: #b5d992;
        }

        .lie-lunch-menu-box {
            background-color: #009ec2;
        }

        .lie-stamp-box {
            background-color: #e63396;
        }

        .lie-no-deals-box {
            background-color: #888888;
        }

        #lie-restaurants-top-bar {
            display: flex;
            justify-content: space-between;
        }

        .lie-restaurants-top-bar-element {
            display: inline-block;
            margin-left: 18px;
            margin-right: 18px;
            vertical-align: middle;
            text-align: center;
        }

        #lie-restaurant-search-text {
            border: 1px solid darkgray;
            height: 32px;
            border-right: none;
            padding: 5px;
            margin-right: 0px;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        #lie-restaurant-search-button {
            border: 1px solid #27ae60;
            height: 32px;
            margin-top: -2px;
            background-color: #27ae60;
            color: white;
            margin-left: 0px;
            padding: 8px;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        #lie-map-view {
            height: 600px;
            width: 100%;
        }

        #lie-map-view-symbol {
            border-radius: 2px;
            border: 2px solid rgba(0, 0, 0, 0);
        }

        #lie-map-view-symbol.active {
            border: 2px solid #27ae60;
        }

        #lie-restaurant-list-container {
            text-align: center;
        }

        .lie-restaurant-closed-modal-body {
            background-color: #edffed;
            height: 180px;
            padding: 0px;
        }

        .lie-restaurant-closed-modal-main {
            text-align: center;
            padding: 15px;
            padding-left: 30px;
        }

        .lie-restaurant-closed-modal-favs {
            border-left: 1px solid grey;
            height: 50%;
            padding-top: 20px;
        }

        .lie-restaurant-closed-modal-orders {
            border-left: 1px solid grey;
            border-top: 1px solid grey;
            height: 50%;
            padding-top: 20px;
        }

        .lie-restaurant-closed-modal-button {
            padding: 5px;
            margin: 0px 10px;
        }

        .lie-list-display-icon {
            font-size: 24px;
            color: #27ae60;
            cursor: pointer;
        }

    </style>

@endsection

@section('contents')
    <div class="lie-contents">

        <div class="row">

            <!-- Filter sidebar -->
            <div class="col-xs-4 white-rounded">

                <div id="lie-all-filters">
                    <table class="lie-filter-header-table">
                        <tr>
                            <td class="lie-filter-header-label">FILTER</td>
                            <td class="lie-filter-header-reset" onclick="resetFilters()">alles zurücksetzen</td>
                        </tr>
                    </table>
                </div>

                <div class="lie-filter-segment">
                    <div class="lie-filter-header">
                        <table class="lie-filter-header-table">
                            <tr>
                                <td class="lie-filter-header-label">Küchenrichtungen</td>
                                <td class="lie-filter-header-reset" onclick="resetFilters('kitchen')">zurücksetzen</td>
                            </tr>
                        </table>
                    </div>
                    <div class="lie-filter-body">
                        <div class="row">
                            @foreach($all_kitchens as $kitchen)
                                <div class="col-xs-6 lie-checkbox-column">
                                    <label class="lie-filter-checkbox">
                                        <input type="checkbox" class="lie-filter-checkbox-input"
                                               data-kitchen="{{ $kitchen->id }}" data-target="kitchen"/>
                                        <span class="lie-checkbox-icon"><i class='fa fa-square-o'></i></span>
                                        {{ $kitchen->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="lie-filter-segment">
                    <div class="lie-filter-header">
                        <table class="lie-filter-header-table">
                            <tr>
                                <td class="lie-filter-header-label">Entfernung</td>
                                <td class="lie-filter-header-reset" onclick="resetFilters('distance')">zurücksetzen</td>
                            </tr>
                        </table>
                    </div>
                    <div class="lie-filter-body">
                        <div class="lie-double-sided-slider" data-target="distance" data-step="0.1" data-min="0"
                             data-max="100"
                             data-label="lie-distance-label"></div>
                        <table class="slider-label">
                            <tbody>
                            <tr>
                                <td class="slider-label-left"><span id="lie-distance-lower-bound">0</span> km</td>
                                <td class="slider-label-middle" id="lie-distance-label"></td>
                                <td class="slider-label-right"><span id="lie-distance-upper-bound">100</span> km</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="lie-filter-segment">
                    <div class="lie-filter-header">
                        <table class="lie-filter-header-table">
                            <tr>
                                <td class="lie-filter-header-label">Mindestbestellwert</td>
                                <td class="lie-filter-header-reset" onclick="resetFilters('minimum-order')">zurücksetzen
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="lie-filter-body">
                        <div class="lie-double-sided-slider" data-target="minimum-order"
                             data-min="{{ $delivery_cost_ranges['min_minimum_order'] }}"
                             data-max="{{ $delivery_cost_ranges['max_minimum_order'] }}"
                             data-label="lie-minimum-order-label"></div>
                        <table class="slider-label">
                            <tbody>
                            <tr>
                                <td class="slider-label-left">{{ $delivery_cost_ranges['min_minimum_order'] }} €</td>
                                <td class="slider-label-middle" id="lie-minimum-order-label"></td>
                                <td class="slider-label-right">{{ $delivery_cost_ranges['max_minimum_order'] }} €</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="lie-filter-segment">
                    <div class="lie-filter-header">
                        <table class="lie-filter-header-table">
                            <tr>
                                <td class="lie-filter-header-label">Zustellkosten</td>
                                <td class="lie-filter-header-reset" onclick="resetFilters('delivery-cost')">zurücksetzen
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="lie-filter-body">
                        <div class="lie-double-sided-slider" data-target="delivery-cost"
                             data-min="{{ $delivery_cost_ranges['min_delivery_cost'] }}"
                             data-max="{{ $delivery_cost_ranges['max_delivery_cost'] }}"
                             data-label="lie-delivery-tax-label"></div>
                        <table class="slider-label">
                            <tbody>
                            <tr>
                                <td class="slider-label-left">{{ $delivery_cost_ranges['min_delivery_cost'] }} €</td>
                                <td class="slider-label-middle" id="lie-delivery-tax-label"></td>
                                <td class="slider-label-right">{{ $delivery_cost_ranges['max_delivery_cost'] }} €</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="lie-filter-segment">
                    <div class="lie-filter-header">
                        <table class="lie-filter-header-table">
                            <tr>
                                <td class="lie-filter-header-label">Bewertung</td>
                                <td class="lie-filter-header-reset" onclick="resetFilters('rating')">zurücksetzen</td>
                            </tr>
                        </table>
                    </div>
                    <div class="lie-filter-body">
                        <div class="lie-double-sided-slider" data-target="rating" data-step="0.1"
                             data-min="{{ $min_rating }}" data-max="{{ $max_rating }}"
                             data-label="lie-rating-label"></div>
                        <table class="slider-label">
                            <tbody>
                            <tr>
                                <td class="slider-label-left">{{ $min_rating }}</td>
                                <td class="slider-label-middle" id="lie-rating-label"></td>
                                <td class="slider-label-right">{{ $max_rating }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="lie-filter-segment">
                    <div class="lie-filter-header">
                        <table class="lie-filter-header-table">
                            <tr>
                                <td class="lie-filter-header-label">Anzahl der Bewertungen</td>
                                <td class="lie-filter-header-reset" onclick="resetFilters('rating-count')">
                                    zurücksetzen
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="lie-filter-body">
                        <div class="lie-double-sided-slider" data-target="rating-count" data-step="1"
                             data-min="{{ $min_rating_count }}" data-max="{{ $max_rating_count }}"
                             data-label="lie-rating-count-label"></div>
                        <table class="slider-label">
                            <tbody>
                            <tr>
                                <td class="slider-label-left">{{ $min_rating_count }}</td>
                                <td class="slider-label-middle" id="lie-rating-count-label"></td>
                                <td class="slider-label-right">{{ $max_rating_count }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="lie-filter-segment">
                    <div class="lie-filter-header">
                        <table class="lie-filter-header-table">
                            <tr>
                                <td class="lie-filter-header-label">Aktionen</td>
                                <td class="lie-filter-header-reset" onclick="resetFilters('aktionen')">zurücksetzen</td>
                            </tr>
                        </table>
                    </div>
                    <div class="lie-filter-body">
                        <div class="row">
                            <div class="col-xs-6 lie-checkbox-column">
                                <label class="lie-filter-checkbox">
                                    <input type="checkbox" class="lie-filter-checkbox-input" data-target="aktionen"/>
                                    <span class="lie-checkbox-icon"><i class='fa fa-square-o'></i></span>
                                    Neukunden-Rabatt
                                </label>
                            </div>
                            <div class="col-xs-6 lie-checkbox-column">
                                <label class="lie-filter-checkbox">
                                    <input type="checkbox" class="lie-filter-checkbox-input" data-target="aktionen"/>
                                    <span class="lie-checkbox-icon"><i class='fa fa-square-o'></i></span>
                                    Mittagsmenü
                                </label>
                            </div>
                            <div class="col-xs-6 lie-checkbox-column">
                                <label class="lie-filter-checkbox">
                                    <input type="checkbox" class="lie-filter-checkbox-input" data-target="aktionen"/>
                                    <span class="lie-checkbox-icon"><i class='fa fa-square-o'></i></span>
                                    Rabatt
                                </label>
                            </div>
                            <div class="col-xs-6 lie-checkbox-column">
                                <label class="lie-filter-checkbox">
                                    <input type="checkbox" class="lie-filter-checkbox-input" data-target="aktionen"/>
                                    <span class="lie-checkbox-icon"><i class='fa fa-square-o'></i></span>
                                    Stempelkarten
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Main container -->
            <div class="col-xs-8">
                <div class="white-rounded" id="lie-restaurants-top-bar">
                    <div class="lie-restaurants-top-bar-element" style="padding-top: 5px;">
                        Deine Postleitzahl <br/> {{$zipcode}}
                    </div>
                    <div class="lie-restaurants-top-bar-element" style="padding-top: 7px;">
                        <input type="checkbox" class="lie-pickup-toggle"
                               {{ \Illuminate\Support\Facades\Session::get("is_pickup", false) ? "" : "checked" }}
                               data-toggle="toggle"
                               data-on="Zustellung" data-off="Abholung"
                               data-onstyle="success" data-offstyle="success"
                               data-height="20">
                    </div>
                    <div class="lie-restaurants-top-bar-element" style="padding-top: 12px">
                        <i class="fa fa-align-justify lie-list-display-icon"></i>
                        <i class="fa fa-th lie-list-display-icon" style="display: none;"></i>
                    </div>
                    <div class="lie-restaurants-top-bar-element">
                        <img src="{{url('/public/front/map_symbol.png')}}" id="lie-map-view-symbol" onclick="swapMapView()">
                    </div>
                    <div class="lie-restaurants-top-bar-element" style="padding-top: 8px;">
                        <nobr>
                        <input type="text" placeholder="Was möchten Sie essen?" id="lie-restaurant-search-text"><!--
                        --><button type="button" class="btn" id="lie-restaurant-search-button"><i class="fa fa-search"></i></button>
                        </nobr>
                    </div>
                </div>

                <div id="lie-restaurant-list-container">

                </div>

                <div id="lie-map-view" style="position: absolute; left: -1000%;">

                </div>

            </div>

        </div>

    </div>
@endsection

@section('scripts')

    <!-- Restaurant Entry Template -->
    <script type="text/template" id="lie-restaurant-template">
        <div class="white-rounded lie-restaurant-entry-container"
        <% if (!restaurant.isOpen) { %>
        onclick="showRestaurantClosedModal('<%- restaurant.id %>')"
        <% } else { %>
        onclick="window.location.href='/restaurant/<%- restaurant.id %>/details'"
        <% } %>
        >
        <% if (!restaurant.isOpen) { %>
        <div class="lie-restaurant-closed-overlay"></div>
        <% } %>

        <% if (restaurant.imageUrl) { %>
        <div class="media-left">
            <img src="<%- restaurant.imageUrl %>" width="130" class="lie-restaurant-logo">
        </div>
        <% } %>

        <div class="media-body">

            <span class="lie-restaurant-entry-name"><%- restaurant.name %></span>
            <span class="pull-right">
                    <span style="color: #f0cd4b">
                    <% for (var i = 0; i < Math.round(restaurant.rating); i++) { %>
                    <i class="fa fa-star"></i>
                    <% } %>
                    </span>
                    <span style="color: #AAAAAA">
                    <% for (var i = 5; i > Math.round(restaurant.rating); i--) { %>
                    <i class="fa fa-star"></i>
                    <% } %>
                    </span>
                    &nbsp; <span style="color: #f0cd4b"><%- restaurant.rating %></span> &nbsp; &nbsp;
                    <span style="color: #AAAAAA;">(<%- restaurant.ratingCount %>)</span>
                </span> <br/>

            <span
                    class="lie-restaurant-entry-opening-hours"
                    style="color: <%- restaurant.isOpen ? '#74c157' : '#da4f4a' %>">
                    <% if (restaurant.closedToday) { %>
                        Heute geschlossen
                    <% } else { %>
                        <% for (var i = 0; i < restaurant.openingHours.length; i++) { %>
                            <%- i > 0 ? ', ' : '' %> <%- restaurant.openingHours[i] %>
                        <% } %>
                    <% } %>
                </span> <br/>

            <div class="lie-deal-boxes-container">
                <!--
                <div class="lie-aktion-box">
                    <b>Aktion</b> <br/>
                    bis -20%
                </div>
                <div class="lie-new-customer-box">
                    <b>Neukundenrabatt</b> <br/>
                    -10%
                </div>
                <div class="lie-lunch-menu-box">
                    <b>Mittagsmenü</b> <br/>
                    Mo - Fr
                </div>
                -->
                <% if (restaurant.hasStampCards) { %>
                <div class="lie-stamp-box">
                    <b>Stempelkarte</b> <br/>
                    <%- restaurant.userStamps %> / <%- restaurant.maxStamps %>
                </div>
                <% } %>
                <% if (!restaurant.hasStampCards) { %>
                <div class="lie-no-deals-box">
                    <b>Keine Aktionen</b> <br/>
                </div>
                <% } %>
            </div>

            <span>
                    Mindestbestellwert: <%- currency_format(restaurant.minimumOrder) %>
                    <% if (restaurant.belowOrder) { %>
                        darunter <%- currency_format(restaurant.belowOrder) %>
                    <% } %>
                    &nbsp; &nbsp; &nbsp; Zustellkosten: <%- currency_format(restaurant.deliveryCost) %>
                    <span class="pull-right">
                        <% if ("distance" in restaurant) { %>
                        Entfernung: <b><%- restaurant.distance.toFixed(2) %> km</b> &nbsp; &nbsp;
                        <% } %>
                        @if(\Illuminate\Support\Facades\Auth::check('front'))
                            <i style="cursor: pointer; color: <%- restaurant.isFavorite ? '#da4f48' : '#d6cfca' %>"
                               data-restaurant="<%- restaurant.id %>"
                               class="lie-toggle-favorite-button fa fa-heart fa-2x"></i>
                        @endif
                    </span>
                </span>

        </div>
        </div>
    </script>

    <script type="text/template" id="lie-restaurant-show-more-template">
        <button class="btn" type="button" id="lie-show-more-button"
                onclick="restaurantLimit += 8; updateRestaurantList();">
            Mehr anzeigen
        </button>
    </script>

    <script type="text/template" id="lie-restaurant-closed-modal-template">
        <div class="modal fade" id="lie-restaurant-closed-modal-<%- restaurant.id %>">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body lie-restaurant-closed-modal-body">
                        <div class="row">
                            <div class="col-xs-10 lie-restaurant-closed-modal-main">
                                <% if (restaurant.imageUrl) { %>
                                <div class="media-left"><img src="<%- restaurant.imageUrl %>" width="130" height="120">
                                </div>
                                <% } %>
                                <div class="media-body" style="text-align: left;">
                                    <b><%- restaurant.name %></b> <span class="pull-right" style="color: #d2335c;">GESCHLOSSEN</span><br/><br/>
                                    Dieses Restaurant hat zurzeit geschlossen,
                                    wenn du möchtest kannst du ein anderes Restaurant auswählen oder vorbestellen
                                    <br/><br/>
                                    <button type="button" class="btn lie-restaurant-closed-modal-button"
                                            data-dismiss="modal">Anderes Restaurant wählen
                                    </button>
                                    <button type="button" class="btn lie-restaurant-closed-modal-button"
                                            onclick="window.location.href=baseUrl+'/restaurant/<%- restaurant.id %>/details'">
                                        Vorbestellen
                                    </button>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="lie-restaurant-closed-modal-favs">
                                    <i class="fa fa-heart" style="color: #d2335c;"></i><br/>
                                    <%- restaurant.favoriteCount %>
                                </div>
                                <div class="lie-restaurant-closed-modal-orders">
                                    <i class="fa fa-star" style="color: #ffdc0f;"></i><br/>
                                    <%- restaurant.orderCount %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </script>

    <!-- noUISlider -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/9.0.0/nouislider.min.js"></script>

    <!-- Underscore -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>

    <script>

        var showingMap = false;
        function swapMapView() {
            $("#lie-map-view").attr("style", "position: relative");
            if (!showingMap) {
                $("#lie-map-view-symbol").addClass("active");
                $("#lie-restaurant-list-container").hide();
                $("#lie-map-view").show();
            }
            else {
                $("#lie-map-view-symbol").removeClass("active");
                $("#lie-restaurant-list-container").show();
                $("#lie-map-view").hide();
            }
            showingMap = !showingMap;
        }

        var sliderValues = {};

        var searchingPickup = {{ \Illuminate\Support\Facades\Session::get("is_pickup") ? 'true' : 'false' }};

        var restaurantTemplate = _.template($("#lie-restaurant-template").html());
        var showMoreTemplate = _.template($("#lie-restaurant-show-more-template").html());
        var restaurantClosedModalTemplate = _.template($("#lie-restaurant-closed-modal-template").html());

        var restaurants = [
                @foreach($rest as $current_rest)
                <?php
                $deliveryCosts = \App\superadmin\RestDeliveryCost::getRestaurantDeliveryCosts($current_rest->id);
                $address = preg_replace('/\s+/', "+", $current_rest->add1 . "+" . $current_rest->add2);
                $order_types = \App\superadmin\RestPriceMap::getRestaurantOrderTypes($current_rest->id);
                $keywords = \App\superadmin\RestDetail::getRestaurantSearchKeywords($current_rest->id);
                $openingHours = array();
                $daytimeRows = \App\superadmin\RestDayTimeMap::daytime($current_rest->id);
                $isOpen = false;
                $closedToday = false;
                $currentTime = time();

                $hasStampCards = \App\superadmin\RestStampMap::hasRestaurantStampCards($current_rest->id);
                $userStamps = \App\front\UserStampRestCurrent::getActiveUserStamps($current_rest->id);
                $maxStamps = \App\superadmin\RestStampMap::getStampCardTarget($current_rest->id);

                if (\App\superadmin\RestSetting::isClosedToday($current_rest->id) || count($daytimeRows) == 0) {
                    $closedToday = true;
                    $isOpen = false;
                } else {
                    foreach ($daytimeRows as $time) {
                        if (!$time["is_open"]) {
                            $openingHours = array();
                            $closedToday = true;
                            $isOpen = false;
                            break;
                        }
                        $openTime = $time["open_time"];
                        $closeTime = $time["close_time"];
                        if (substr($openTime, 0, 2) == "00") $openTime = "12" . substr($openTime, 2);
                        if (substr($closeTime, 0, 2) == "00") $closeTime = "12" . substr($closeTime, 2);
                        array_push($openingHours, date('H:i', strtotime($openTime)) . " - " . date('H:i', strtotime($closeTime)));
                        if ($currentTime > strtotime($openTime) && $currentTime < strtotime($closeTime)) {
                            $isOpen = true;
                        }
                    }
                }
                ?>
            {
                id: {{ $current_rest->id }},
                name: '{{ $current_rest->f_name }}',
                kitchens: [
                    @if($rest_kitchen_maps[$current_rest->id])
                    @foreach($rest_kitchen_maps[$current_rest->id] as $kitchen_id)
                    {{ $kitchen_id }},
                    @endforeach
                    @endif
                ],
                rating: {{ number_format(\App\front\UserOrderReview::rest_review_rating($current_rest->id), 1) }},
                ratingCount: {{ \App\front\UserOrderReview::rest_review_count($current_rest->id) }},
                openingHours: [
                    @foreach($openingHours as $time)
                            "{{ $time }}",
                    @endforeach
                ],
                isOpen: {{ $isOpen ? "true" : "false" }},
                closedToday: {{ $closedToday ? "true" : "false" }},
                isFavorite: {{\App\front\UserRestFavourite::is_rest_user_favourite($current_rest->id) ? "true" : "false"}},
                minimumOrder: {{ number_format($deliveryCosts[0], 2) }},
                belowOrder: {{ $deliveryCosts[1] != null ? number_format($deliveryCosts[1], 2)  : 'null' }},
                deliveryCost: {{ number_format($deliveryCosts[2], 2) }},
                address: '{{ $address }}',
                hasDelivery: {{ $order_types[0] ? 'true' : 'false' }},
                hasPickup: {{ $order_types[1] ? 'true' : 'false' }},
                keywords: [
                    @foreach($keywords as $keyword)
                            '{{ $keyword }}',
                    @endforeach
                ],
                // imageUrl: '{{ ($current_rest->logo != '' and \Illuminate\Support\Facades\File::exists(public_path() . "/uploads/superadmin/restaurants/" . $current_rest->logo)) ? ("/uploads/superadmin/restaurants/" . $current_rest->logo) : ''}}',
                imageUrl: '{{ \App\superadmin\RestDetail::getRestaurantLogoForDisplay($current_rest->id) }}',
                    favoriteCount: {{ \App\front\UserRestFavourite::rest_favourite_count($current_rest->id) }},
                orderCount: {{ \App\front\UserOrder::rest_order_count($current_rest->id) }},
                hasStampCards: {{ $hasStampCards ? 'true' : 'false' }},
                userStamps: {{ $userStamps }},
                maxStamps: {{ $maxStamps }}
            },
            @endforeach
        ];

        var filteredRestaurants = restaurants;

        $(".lie-filter-checkbox-input").change(function () {
            var checkbox = $(this);
            var label = checkbox.parent();
            var iconContainer = label.children().eq(1);
            var checkedClass = "lie-filter-checked";
            if (checkbox.prop("checked")) {
                label.addClass(checkedClass);
                iconContainer.html("<i class='fa fa-check'></i>");
            }
            else {
                label.removeClass(checkedClass);
                iconContainer.html("<i class='fa fa-square-o'></i>");
            }
        });

        // Initialize sliders
        $(function () {
            var sliders = document.getElementsByClassName('lie-double-sided-slider');
            for (var i = 0; i < sliders.length; i++) {
                var min = parseFloat($(sliders[i]).data("min"));
                var max = parseFloat($(sliders[i]).data("max"));
                if (max <= min) {
                    $(sliders[i]).parent().parent().attr("style", "display: none");
                    continue;
                }
                var step = 1;
                if (sliders[i].hasAttribute("data-step")) {
                    step = $(sliders[i]).data("step");
                }
                noUiSlider.create(sliders[i], {
                    start: [min, max],
                    connect: true,
                    step: step,
                    orientation: "horizontal",
                    range: {
                        min: min,
                        max: max
                    }
                });

                var currentSlider = sliders[i];

                sliders[i].noUiSlider.on('update', function () {
                    var values = this.noUiSlider.get();
                    var label = $("#" + $(this).data("label"));
                    label.html(values[0] + " - " + values[1]);
                    var target = $(this).data("target");
                    sliderValues[target] = {
                        min: values[0],
                        max: values[1]
                    };
                    updateRestaurantList();
                }.bind(currentSlider));
            }
        });

        // Filtering
        function applyKitchensFilter() {
            var kitchenInputs = $(".lie-filter-checkbox-input[data-kitchen]");
            var selectedKitchens = [];
            kitchenInputs.each(function (index, element) {
                if ($(element).prop("checked")) {
                    selectedKitchens.push($(element).data("kitchen"));
                }
            });
            if (selectedKitchens.length == 0) return;
            filteredRestaurants = filteredRestaurants.filter(function (element) {
                for (var i = 0; i < selectedKitchens.length; i++) {
                    if (element.kitchens.indexOf(selectedKitchens[i]) >= 0) return true;
                }
                return false;
            });
        }

        function applySliderFilter(target, attribute) {
            var range = sliderValues[target];
            if (range) {
                filteredRestaurants = filteredRestaurants.filter(function (element) {
                    if (!(attribute in element)) return true;
                    return element[attribute] >= range.min && element[attribute] <= range.max;
                });
            }
        }

        // Search field
        var searchText = "";

        $("#lie-restaurant-search-button").click(function (evt) {
            searchText = $("#lie-restaurant-search-text").val().trim();
            updateRestaurantList();
        });

        $('#lie-restaurant-search-text').keypress(function (e) {
            if (e.which == 13) {
                searchText = $("#lie-restaurant-search-text").val().trim();
                updateRestaurantList();
            }
        });

        // Show more
        var restaurantLimit = 8;

        function applyFilters() {
            filteredRestaurants = restaurants;
            applyKitchensFilter();

            applySliderFilter("distance", "distance");
            applySliderFilter("minimum-order", "minimumOrder");
            applySliderFilter("delivery-cost", "deliveryCost");
            applySliderFilter("rating", "rating");
            applySliderFilter("rating-count", "ratingCount");

            if (searchText != "") {
                filteredRestaurants = filteredRestaurants.filter(function (element, index, array) {
                    for (var i = 0; i < element.keywords.length; i++) {
                        var keyword = element.keywords[i];
                        if (keyword.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
                            return true;
                        }
                    }
                    return false;
                });
            }

            filteredRestaurants = filteredRestaurants.filter(function (element, index, array) {
                if (searchingPickup) return element.hasPickup;
                else return element.hasDelivery;
            });
        }

        function renderRestaurantList() {
            var container = $("#lie-restaurant-list-container");
            container.empty();
            for (var i = 0; i < filteredRestaurants.length && i < restaurantLimit; i++) {
                if (filteredRestaurants[i].isOpen) {
                    var restaurantHtml = restaurantTemplate({restaurant: filteredRestaurants[i]});
                    container.append(restaurantHtml);
                }
            }
            for (var i = 0; i < filteredRestaurants.length && i < restaurantLimit; i++) {
                if (!filteredRestaurants[i].isOpen) {
                    var restaurantHtml = restaurantTemplate({restaurant: filteredRestaurants[i]});
                    container.append(restaurantHtml);
                    var closedModalHtml = restaurantClosedModalTemplate({restaurant: filteredRestaurants[i]});
                    container.append(closedModalHtml);
                }
            }
            if (filteredRestaurants.length > restaurantLimit) {
                var showMoreHtml = showMoreTemplate({});
                container.append(showMoreHtml);
            }
            registerToggleFavoriteButtons("{{ csrf_token() }}", toggleFavoriteButtonColor);
        }

        function updateRestaurantList() {
            applyFilters();
            renderRestaurantList();
        }

        $(function () {
            $(".lie-filter-checkbox-input").change(updateRestaurantList);
        });

        $(function () {
            updateRestaurantList();
        });

        function toggleFavoriteButtonColor(restaurantId) {
            var restaurant = restaurants.find(function (element, index, array) {
                return element.id == restaurantId;
            });
            if (restaurant) {
                restaurant.isFavorite = !restaurant.isFavorite;
            }
            updateRestaurantList();
        }

        function deg2rad(deg) {
            rad = deg * Math.PI / 180; // radians = degrees * pi/180
            return rad;
        }

        function round(x) {
            return Math.round(x * 1000) / 1000;
        }

        function createMap() {
            var map = new google.maps.Map(document.getElementById('lie-map-view'), {
                zoom: 12,
                center: {lat: restaurants[0].latitude, lng: restaurants[0].longitude}
            });
            for (var i = 0; i < restaurants.length; i++) {
                var restaurant = restaurants[i];
                var marker = new google.maps.Marker({
                    position: {lat: restaurant.latitude, lng: restaurant.longitude},
                    map: map,
                    title: restaurant.name
                });
            }
        }

        var distanceRangesSet = false;
        var googleMapsLoaded = false;

        function updateDistanceRanges() {
            var minDistance = Number.MAX_VALUE;
            var maxDistance = 0;
            for (var i = 0; i < restaurants.length; i++) {
                if (!("distance" in restaurants[i])) continue;
                minDistance = Math.min(minDistance, restaurants[i].distance);
                maxDistance = Math.max(maxDistance, restaurants[i].distance);
            }
            var slider = $(".lie-double-sided-slider[data-target='distance']")[0];
            $("#lie-distance-lower-bound").html(Math.floor(minDistance));
            $("#lie-distance-upper-bound").html(Math.ceil(maxDistance));
            slider.noUiSlider.updateOptions({
                start: [Math.floor(minDistance), Math.ceil(maxDistance)],
                range: {
                    min: Math.floor(minDistance),
                    max: Math.ceil(maxDistance)
                }
            });
            distanceRangesSet = true;
            if (googleMapsLoaded) {
                createMap();
            }
        }

        $(function () {
            if (!window.navigator) {
                return;
            }
            window.navigator.geolocation.getCurrentPosition(function (geolocation) {
                var selfLatitude = geolocation.coords.latitude;
                var selfLongitude = geolocation.coords.longitude;
                var targetCount = restaurants.length;
                var currentCount = 0;
                for (var i = 0; i < restaurants.length; i++) {
                    $.ajax({
                        url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + restaurants[i].address,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            currentCount++;
                            if (data.results.length == 0) {
                                console.error("Konnte keine Position für " + this.address + " ermitteln");
                            }
                            else {
                                this.latitude = data.results[0].geometry.location.lat;
                                this.longitude = data.results[0].geometry.location.lng;
                                this.distance = findDistance(this.latitude, this.longitude, selfLatitude, selfLongitude);
                            }
                            if (currentCount == targetCount) {
                                updateDistanceRanges();
                                updateRestaurantList();
                            }
                        }.bind(restaurants[i])
                    });
                }
            });
        });

        function resetFilter(element) {
            var jq = $(element);
            if (jq.is("div")) { // slider
                if (!element.noUiSlider) return;
                var min = element.noUiSlider.options.range.min;
                var max = element.noUiSlider.options.range.max;
                element.noUiSlider.updateOptions({
                    start: [min, max]
                });
            }
            else if (jq.is("input")) { // checkbox
                jq.prop("checked", false);
                jq.trigger("change");
            }
            else {
                console.error("Unrecognized data-target filter!: ");
                console.dir(element);
            }
        }

        function resetFilters(target) {
            if (target) {
                $("[data-target='" + target + "']").each(function (index, element) {
                    resetFilter(element);
                });
            }
            else {
                $("[data-target]").each(function (index, element) {
                    resetFilter(element);
                });
            }
        }

        function onPickupDeliveryToggle(isPickup) {
            searchingPickup = isPickup;
            updateRestaurantList();
        }

        $(function () {
            registerPickupDeliveryToggle('{{ csrf_token() }}', onPickupDeliveryToggle);
        });

        function initializeMapView() {
            googleMapsLoaded = true;
            if (distanceRangesSet) {
                createMap();
            }
        }

        function showRestaurantClosedModal(restaurantId) {
            var modal = $("#lie-restaurant-closed-modal-" + restaurantId).modal("show");
        }

    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1PwoqBZYkvTjNybm3YvXi3ppqOOO5W2Y&callback=initializeMapView"></script>

@endsection