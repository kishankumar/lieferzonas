@extends('front_new.layout')

@section('title')
    Lieferzonas
@endsection

@section('head')

    <style>

        .navbar {
            margin-bottom: 10px;
        }

        #lie-slugs-sidebar {
            padding: 0px;
        }

        #lie-slugs-list {
            background-color: white;
            height: 600px;
        }

        .lie-slug-icon {
            margin-left: 15px;
            margin-right: 15px;
        }

        #lie-main-container {
        }

        .lie-slug-header {
            position: relative;
            font-size: 26px;
            background: url("/front/slug_header.png") no-repeat;
            background-size: cover;
            background-position: center;
            height: 250px;
        }

        #lie-header-city {
            position: absolute;
            left: 6%;
            top: 10px;
            color: white;
            font-family: Harabara-Bold;
        }

        #lie-header-lines-container {
            position: absolute;
            top: 80px;
            right: 26%;
        }

        #lie-header-line-1 {
            position: absolute;
            right: 0px;
            top: 0px;
            width: 206px;
            padding: 3px 8px;
            background-color: #47d5ad;
            color: white;
        }

        #lie-header-line-2 {
            position: absolute;
            right: 0px;
            top: 50px;
            width: 431px;
            padding: 3px 8px;
            background-color: black;
            color: white;
        }

        #lie-header-zipcode-entry-form {
            position: absolute;
            bottom: 0px;
            text-align: center;
            zoom: 0.8;
            width: 100%;
        }

    </style>

    @yield('slug_head')

@endsection

@section('contents')
    <div class="lie-contents">
        <div class="row">

            <div class="col-xs-1" id="lie-slugs-sidebar">
                <br>
                <hr>
                <br>
                <a href="javascript:void(0)" onclick="show('service')"><img src="/front/slug_services.png" width="50"
                                                                            class="lie-slug-icon"></a>
                <br>
                <hr>
                <br>
                <a href="javascript:void(0)" onclick="show('x')"><img src="/front/slug_2.png" width="50"
                                                                      class="lie-slug-icon"></a>
                <br>
                <hr>
                <br>
                <a href="javascript:void(0)" onclick="show('y')"><img src="/front/slug_3.png" width="50"
                                                                      class="lie-slug-icon"></a>
                <br>
                <hr>
                <br>
                <a href="javascript:void(0)" onclick="show('z')"><img src="/front/slug_4.png" width="50"
                                                                      class="lie-slug-icon"></a>
                <br>
                <hr>
                <br>
            </div>

            <!--
            <div class="col-xs-2" id="lie-slugs-list">
                <div id="lie-service-list">
                    <span style="font-family: Harabara-Bold; font-size: 22px;">Unsere Dienste</span><br>
                    @foreach(\App\superadmin\FrontService::getActiveServices() as $activeService)
                        <span style="font-size: 10px"><i class="fa fa-circle"></i></span>
                        &nbsp;
                        <a href="{{ url("service/" . $activeService->id) }}">
                            {{ $activeService->name }}
                        </a> <br>
                    @endforeach
                </div>
            </div>
            -->

            <div class="col-xs-11" id="lie-main-container">
                <div class="lie-slug-header">
                    <div id="lie-header-city">
                        Wien
                    </div>
                    <div id="lie-header-lines-container">
                        <div id="lie-header-line-1">
                            <span class="lie-header-thin">Wien, wie es isst</span>
                        </div>
                        <div id="lie-header-line-2">
                            <span class="lie-header-bold">Jetzt einfach</span> <span class="lie-header-thin">Online Essen Bestellen</span>
                        </div>
                    </div>
                    <div id="lie-header-zipcode-entry-form">
                        {!! lie_zipcode_entry_form("lg") !!}
                    </div>
                </div>

                @include('front_new.includes.explore_restaurants', ["head_line" => "Finden Sie jetzt ihr neues Lieblingsrestaurant und probieren Sie neue Küchenvarianten"])

                @yield('slug_contents')
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>

        var explorePointer = 0;

        var allSlugs = ["service"];

        function hideSlugList(slugType) {
            $("#lie-" + slugType + "-list").hide();
        }

        function showSlugList(slugType) {
            $("#lie-" + slugType + "-list").show();
        }

        function show(slugType) {
            for (var i = 0; i < allSlugs.length; i++) {
                hideSlugList(allSlugs[i]);
            }
            showSlugList(slugType);
        }

    </script>

    @yield('slug_scripts')

@endsection