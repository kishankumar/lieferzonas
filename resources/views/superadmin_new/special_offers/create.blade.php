@extends('layouts.superadminbody')

@section('title')
    Lieferzonas Administration - Spezialangebot erstellen
@endsection

@section('body')
    <div class="content-wrapper">

        <section class="content-header">
            <h1>Spezialangebot erstellen</h1>
        </section>

        <section class="content">

            <div class="form-group">
                <label>Name: </label>
                <input type="text" class="form-control" placeholder="Name" id="lie-name-input">
            </div>

            <div class="form-group">
                <label>Betrifft: </label>

                <button type="button" class="btn btn-primary" id="lie-add-category">Kategorie hinzufügen</button>
                <button type="button" class="btn btn-primary" id="lie-add-menu">Menü hinzufügen</button>
                <button type="button" class="btn btn-primary" id="lie-add-submenu">Gericht hinzufügen</button>

                <table class="table" id="lie-target-table">
                    <thead>
                    <tr>
                        <th>Typ</th>
                        <th>Name</th>
                        <th><i class="fa fa-times"></i></th>
                    </tr>
                    </thead>
                    <tbody id="lie-target-tbody">
                    </tbody>
                </table>
            </div>

            <div class="form-group">
                <label>Typ: </label>
                <select class="form-control" id="lie-type-selection">
                    <option value="1">Fixpreis</option>
                    <option value="2">Prozent-Rabatt</option>
                    <option value="3">Gratis Gericht</option>
                </select>
            </div>

            <div class="form-group lie-amount-form" data-type="1" style="display:none">
                <label>Preis: </label>
                <input type="text" class="form-control lie-value-input" data-type="1" placeholder="0,00" id="lie-fixed-input">
            </div>

            <div class="form-group lie-amount-form" data-type="2" style="display:none">
                <label>Prozent: </label>
                <input type="text" class="form-control lie-value-input" data-type="2" placeholder="0,00" id="lie-percentage-input">
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Gültig von:</label>
                        <input type="date" class="form-control" id="lie-from-date" value="{{ date("Y-m-d", time()) }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Gültig bis:</label>
                        <input type="date" class="form-control" id="lie-to-date" value="2099-01-01">
                    </div>
                </div>
            </div>

            <?php
            $times = array();
            for ($i = 0; $i < 24; $i++) {
                if ($i > 12) {
                    $hour = $i - 12;
                } else {
                    $hour = $i;
                }
                if ($i > 11) {
                    $meridie = "PM";
                } else {
                    $meridie = "AM";
                }
                array_push($times, $hour . ":00 " . $meridie);
                array_push($times, $hour . ":30 " . $meridie);
            }
            array_push($times, "11:59 PM");
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Gültig von:</label>
                        <select class="form-control" id="lie-valid-from-daytime">
                            @foreach($times as $time)
                                <option value="{{ $time }}">{{ $time }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Gültig bis:</label>
                        <select class="form-control" id="lie-valid-to-daytime">
                            @foreach($times as $time)
                                <option value="{{ $time }}">{{ $time }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Gültig am:</label>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label><input type="checkbox" class="lie-weekday-selection" value="1"> Montag</label>
                    </div>
                    <div class="form-group">
                        <label><input type="checkbox" class="lie-weekday-selection" value="4"> Donnerstag</label>
                    </div>
                    <div class="form-group">
                        <label><input type="checkbox" class="lie-weekday-selection" value="7"> Sonntag</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><input type="checkbox" class="lie-weekday-selection" value="2"> Dienstag</label>
                    </div>
                    <div class="form-group">
                        <label><input type="checkbox" class="lie-weekday-selection" value="5"> Freitag</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><input type="checkbox" class="lie-weekday-selection" value="3"> Mittwoch</label>
                    </div>
                    <div class="form-group">
                        <label><input type="checkbox" class="lie-weekday-selection" value="6"> Samstag</label>
                    </div>
                </div>
            </div>

            <div style="text-align: center">
                <button class="btn btn-success" type="button" id="lie-save-special-offer"><i class="fa fa-floppy-o"></i>
                    Speichern
                </button>
            </div>

        </section>

    </div>

    <!-- Kategorie hinzufügen - Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="lie-add-category-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Kategorie hinzufügen</h4>
                </div>
                <div class="modal-body">
                    <select class="form-control" id="lie-add-category-select">
                        @foreach(\App\superadmin\RestCategory::getByRestaurant($restaurant_id) as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="lie-add-category-confirm">
                        Hinzufügen
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Menü hinzufügen - Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="lie-add-menu-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Menü hinzufügen</h4>
                </div>
                <div class="modal-body">
                    <select class="form-control" id="lie-add-menu-select">
                        @foreach(\App\superadmin\RestCategory::getByRestaurant($restaurant_id) as $category)
                            <optgroup label="{{ $category->name }}">
                                @foreach(\App\superadmin\RestMenu::getMenuByCategory($category->id) as $menu)
                                    <option value="{{ $menu['id'] }}">{{ $menu['name'] }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="lie-add-menu-confirm">
                        Hinzufügen
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Gericht hinzufügen - Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="lie-add-submenu-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Gericht hinzufügen</h4>
                </div>
                <div class="modal-body">
                    <select class="form-control" id="lie-add-submenu-select">
                        @foreach(\App\superadmin\RestCategory::getByRestaurant($restaurant_id) as $category)
                            @foreach(\App\superadmin\RestMenu::getMenuByCategory($category->id) as $menu)
                                <optgroup label="{{ $category->name }}">
                                    @foreach(\App\superadmin\RestSubMenu::getSubMenuByMenu($menu['id']) as $submenu)
                                        <option value="{{ $submenu['id'] }}">{{ $submenu['name'] }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="lie-add-submenu-confirm">
                        Hinzufügen
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/template" id="lie-target-element-template">
        <tr data-category="<%- item.id %>">
            <td><%- type %></td>
            <td><%- item.name %></td>
            <td>
                <button type="button" class="btn btn-danger" onclick="<%- removeFunction %>('<%- item.id %>')"><i
                            class="fa fa-times"></i></button>
            </td>
        </tr>
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>

    <script>

        var targetTemplate = _.template($("#lie-target-element-template").html());

        var targets = {
            "categories": [],
            "menus": [],
            "submenus": []
        };

        function removeCategory(id) {
            targets.categories = targets.categories.filter(function (element, index, array) {
                return element.id != id;
            });
            renderTargetTable();
        }

        function removeMenu(id) {
            targets.menus = targets.menus.filter(function (element, index, array) {
                return element.id != id;
            });
            renderTargetTable();
        }

        function removeSubmenu(id) {
            targets.submenus = targets.submenus.filter(function (element, index, array) {
                return element.id != id;
            });
            renderTargetTable();
        }

        function addCategory() {
            $("#lie-add-category-modal").modal("show");
        }

        function addMenu() {
            $("#lie-add-menu-modal").modal("show");
        }

        function addSubmenu() {
            $("#lie-add-submenu-modal").modal("show");
        }

        function renderTargetTable() {
            $("#lie-target-tbody").empty();
            for (var i = 0; i < targets.categories.length; i++) {
                var target = targets.categories[i];
                $("#lie-target-tbody").append(targetTemplate({
                    "type": "Kategorie",
                    "removeFunction": "removeCategory",
                    "item": {
                        "id": target.id,
                        "name": target.name
                    }
                }));
            }
            for (var i = 0; i < targets.menus.length; i++) {
                var target = targets.menus[i];
                $("#lie-target-tbody").append(targetTemplate({
                    "type": "Menü",
                    "removeFunction": "removeMenu",
                    "item": {
                        "id": target.id,
                        "name": target.name
                    }
                }));
            }
            for (var i = 0; i < targets.submenus.length; i++) {
                var target = targets.submenus[i];
                $("#lie-target-tbody").append(targetTemplate({
                    "type": "Gericht",
                    "removeFunction": "removeSubmenu",
                    "item": {
                        "id": target.id,
                        "name": target.name
                    }
                }));
            }
        }

        function confirmAddCategory() {
            var selection = $("#lie-add-category-select").val();
            var name = $("#lie-add-category-select option:selected").text();
            targets.categories.push({"id": selection, "name": name});
            renderTargetTable();
        }

        function confirmAddMenu() {
            var selection = $("#lie-add-menu-select").val();
            var name = $("#lie-add-menu-select option:selected").text();
            targets.menus.push({"id": selection, "name": name});
            renderTargetTable();
        }

        function confirmAddSubmenu() {
            var selection = $("#lie-add-submenu-select").val();
            var name = $("#lie-add-submenu-select option:selected").text();
            targets.submenus.push({"id": selection, "name": name});
            renderTargetTable();
        }

        $("#lie-add-category").click(addCategory);
        $("#lie-add-menu").click(addMenu);
        $("#lie-add-submenu").click(addSubmenu);

        $("#lie-add-category-confirm").click(confirmAddCategory);
        $("#lie-add-menu-confirm").click(confirmAddMenu);
        $("#lie-add-submenu-confirm").click(confirmAddSubmenu);

        $(".lie-amount-form").hide();
        $("#lie-type-selection").change(function () {
            $(".lie-amount-form").hide();
            var selection = $(this).val();
            $(".lie-amount-form[data-type='" + selection + "']").show();
        });
        $("#lie-type-selection").trigger("change");

        $("#lie-save-special-offer").click(function () {

            var validWeekdays = [];
            $(".lie-weekday-selection:checked").each(function(index, element) {
                validWeekdays.push($(this).val());
            });

            $.ajax({
                url: '{{ url("root/special_offer") }}',
                type: "post",
                dataType: "json",
                data: {
                    "_token": '{{ csrf_token() }}',
                    "restaurant": '{{ $restaurant_id }}',
                    "name": $("#lie-name-input").val(),
                    "categories": targets.categories.map(function (element, index, array) { return element.id }),
                    "menus": targets.menus.map(function (element, index, array) { return element.id }),
                    "submenus": targets.submenus.map(function (element, index, array) { return element.id }),
                    "type": $("#lie-type-selection").val(),
                    "value": $(".lie-value-input[data-type='" + $("#lie-type-selection").val() + "']").val(),
                    "valid_from": $("#lie-from-date").val(),
                    "valid_to": $("#lie-to-date").val(),
                    "valid_weekdays": validWeekdays,
                    "start_time": $("#lie-valid-from-daytime").val(),
                    "end_time": $("#lie-valid-to-daytime").val(),
                },
                success: function (response) {
                    console.dir(response);
                }
            })
        });

    </script>
@endsection