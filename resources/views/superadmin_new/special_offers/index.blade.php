@extends('layouts.superadminbody')

@section('title')
    Lieferzonas Administration - Spezialangebote
@endsection

@section('body')
    <div class="content-wrapper">
        <h1>Spezialangebote</h1>

        <!-- Spezialangebot Tabelle -->
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Restaurant</th>
                <th>Typ</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection

@section('script')
@endsection