@extends('front_new.layout')

@section('title')
    Lieferzonas - Registrieren
@endsection

@section('head')
<style>
    #lie-register-form-container {
        border-radius: 5px;
        background-color: #FFFFFF;
        padding: 10px;
        margin-top: 55px;
    }
    .lie-image-upload-button {
        position: relative;
        overflow: hidden;
        margin: 10px;
    }
    .lie-image-upload-button input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</style>
@endsection

@section('contents')
    <div class="container">

        <div id="lie-register-form-container">
            {!! Form::open(['url'=>'frontuser/register','method'=>'post','files'=>'true']) !!}

            <div style="text-align: center">
                <img id="lie-image-upload-preview" src="/front/uploads/users/default.png" width="128" height="128"><br/>
                <div class="lie-image-upload-button btn btn-primary" style="text-align: center">
                    Upload
                    {!! Form::file("image", ["class" => "upload", "id" => "lie-image-upload-input"]) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::text('email','',['class'=>'form-control','placeholder'=>'E-Mail']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::text('nickname','',['class'=>'form-control','placeholder'=>'Benutzername']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::text('fname','',['class'=>'form-control','placeholder'=>'Vorname']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::text('lname','',['class'=>'form-control','placeholder'=>'Nachname']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'Passwort']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'Passwort wiederholen']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::date('dob', \Carbon\Carbon::now(),['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::select('gender', array("m" => "Männlich", "f" => "Weiblich", "o" => "Anderes"), null, ['class'=>'form-control','placeholder'=>'Geschlecht']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::text('mobile','',['class'=>'form-control','placeholder'=>'Mobilnummer']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>

            <div style="text-align: center">
                {!! Form::submit('Registrieren',['class'=>'btn btn-success']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
<script>
    function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#lie-image-upload-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#lie-image-upload-input").change(function(){
        previewImage(this);
    });
</script>
@endsection