@extends('layouts.frontbody')

@section('title')
    Lieferzonas
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

    <div class="container">
        <?php if($orderDetails->front_order_status_id == 1){ ?>
        <div class="alert alert-success text-center">Your order has been successfully submitted.<br>Order number: <strong>{{ $orderDetails->order_id }}</strong></div>
        {!! Html::image('resources/assets/front/img/load-Lieferzonas.at-burger-eat.gif','') !!}
        <div class="alert alert-info text-center">Please wait till the restaurant accepts your order.</div>
        <?php } elseif($orderDetails->front_order_status_id == 3){ ?>
        <div class="alert alert-danger text-center">Your order has been rejected by the restaurant.<br>Order number: <strong>{{ $orderDetails->order_id }}</strong></div>
        <?php } elseif($orderDetails->front_order_status_id == 2){ header('Location: http://www.google.com/'); } ?>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        setInterval(function()
        {
            $.ajax({
                url: '{{url('check-order-status')}}',
                type: "post",
                data: {'userOrderId':"{{ $orderDetails->id }}", '_token':"{{ csrf_token() }}"},
                success: function(data){
                    if(data==2)
                        window.location.assign('{{url('live-route')}}/{{ $orderDetails->id }}');
                    else if(data==3)
                        location.reload();
                    else {}
                }
            });
        }, 10000);
    </script>
@endsection



