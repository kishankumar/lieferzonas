 @extends('layouts.frontbody')

@section('title')
    Lieferzonas
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

    <div class="container">
        <div class="container-fluid" style="position:relative;">
            <div class="row">
                <div class="full-map">
                    <div id="dvMap" style="width: 100%; height: 600px"></div>
                </div>
            </div>
            <div class="full-map-text">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="map-details _map-details">
                                <a href="#" class="md-close"> {!! Html::image('resources/assets/front/img/close.png','') !!} </a>
                                <h2>Thank you for your order.</h2>
                                <p>Your Order has been accepted and will be delivered to you <?php if($orderDetails->rest_given_time != "") echo "in ".$orderDetails->rest_given_time." minutes"; else echo "by ".date_format(date_create($orderDetails->delivery_time),'d M Y g:i A'); ?>.</p>
                                <div class="row form-group">
									<div class="media">
										<div class="pull-left">
											{!! Html::image('public/uploads/superadmin/restaurants/'.$orderDetails->rest_detail->logo,'',['height'=>'100px','width'=>'120px']) !!} 
										</div>
										<div class="media-body">
											<h3>{{ $orderDetails->rest_detail->f_name }}</h3>
											<h4>Your order number is: <strong>{{ $orderDetails->order_id }}</strong> </h4>
										</div>
									</div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-block btn-success" onclick="get_route()">Route Betrachten</button>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        Forgot what?<br>
                                        <a href="#">Place an order</a>
                                    </div>
                                    <div class="col-sm-3">
                                        Help?<br>
                                        <a href="#">Make Contact</a>
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Html::image('resources/assets/front/img/fb.png','') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Html::image('resources/assets/front/img/gp1.png','') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection

@section('script')
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDqdEM24ioKzBvpjYUUQKpm3QxXBejqkoI"></script>

    <script type="text/javascript">
        function get_route() {

            if(navigator.geolocation)
            {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
            else
            {
                alert('Error in fetching location. Please try again after some time.');
            }

            function showPosition(position)
            {
                $('.full-map-text').hide();
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;

                var markers = [
                    {
                        "title": 'Alibaug',
                        "lat": latitude,
                        "lng": longitude,
                        "description": '<div class="address_open_user"><div class="media"><figure class="pull-left">{!! Html::image('resources/assets/front/img/face.png','',['width'=>'50','height'=>'50','class'=>'img-circle']) !!}</figure><div class="media-body">You are here</div></div></div>'
                    }
                    ,
                    {
                        "title": 'Mumbai',
                        "lat": '{{ $orderDetails->rest_detail->latitude }}',
                        "lng": '{{ $orderDetails->rest_detail->longitude }}',
                        "description": '<div class="address_open">{!! Html::image('resources/assets/front/img/'.$orderDetails->rest_detail->logo,'') !!}{{ $orderDetails->rest_detail->f_name }}</div>'
                    }
                ];

                var mapOptions = {
                    center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
                var infoWindow = new google.maps.InfoWindow();
                var lat_lng = new Array();
                var latlngbounds = new google.maps.LatLngBounds();
                for (i = 0; i < markers.length; i++) {
                    var data = markers[i]
                    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                    lat_lng.push(myLatlng);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        title: data.title,
                        icon: '{{ url("resources/assets/front/img/mapicon.png") }}'
                    });
                    latlngbounds.extend(marker.position);
                    (function (marker, data) {
                        google.maps.event.addListener(marker, "click", function (e) {
                            infoWindow.setContent(data.description);
                            infoWindow.open(map, marker);
                        });
                    })(marker, data);
                }
                map.setCenter(latlngbounds.getCenter());
                map.fitBounds(latlngbounds);

                //***********ROUTING****************//

                //Initialize the Path Array
                var path = new google.maps.MVCArray();

                //Initialize the Direction Service
                var service = new google.maps.DirectionsService();

                //Set the Path Stroke Color
                var poly = new google.maps.Polyline({map: map, strokeColor: '#4986E7'});

                //Loop and Draw Path Route between the Points on MAP
                for (var i = 0; i < lat_lng.length; i++) {
                    if ((i + 1) < lat_lng.length) {
                        var src = lat_lng[i];
                        var des = lat_lng[i + 1];
                        path.push(src);
                        poly.setPath(path);
                        service.route({
                            origin: src,
                            destination: des,
                            travelMode: google.maps.DirectionsTravelMode.DRIVING
                        }, function (result, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                    path.push(result.routes[0].overview_path[i]);
                                }
                            }
                        });
                    }
                }
            }


        }
    </script>

@endsection