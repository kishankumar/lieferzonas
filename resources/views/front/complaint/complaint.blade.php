@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')
<section class="main-dash-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="bonus-point">
          <h3>Restaurant Complaint </h3>
        </div>
      </div>
    </div>
  </div>
</section>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-8 col-lg-8 mt25">
                
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                {{--    Error Display--}}
                @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{--    Error Display ends--}}
                
            {!! Form::open(array('route' => 'front.complaint.store','id'=>'complaint_form','class' => 'form-horizontal prof-form','novalidate' => 'novalidate', 'files' => true)) !!}
                <div class="form-group">
                    <label class="col-sm-3 control-label">Restaurant<span class="red">*</span> </label>
                    <div class="col-sm-6">
                        <select class="form-control js-example-basic-multiple" name="restaurant" id="restaurant" onchange="getData(this.value,'orders')">
                        <option value="">Select Restaurant</option>
                            @if(count($restro_names))
                                @foreach($restro_names as $restro_name)
                                    <option value="{{$restro_name->id}}" >{{ucfirst($restro_name->f_name)}}</option>
                                @endforeach
                            @endif
                        </select>
                        <span id="restaurantErr" style="color:red"></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">Orders<span class="red">*</span> </label>
                    <div class="col-sm-6">
                        <select class="form-control" name="orders" id="orders">
                        <option value="">Select Order</option>
<!--                            @if(count($order_detail))
                                @foreach($order_detail as $order_details)
                                    <option value="{{$order_details->order_id}}" >{{($order_details->order_id)}}</option>
                                @endforeach
                            @endif-->
                        </select>
                        <span id="orderErr" style="color:red"></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">{!! Form::label('Topic') !!}  <span id="topicErr" style="color:red"></span><span class="red">*</span>
					</label>
					<div class="col-sm-6">
                    	{!! Form::text('topic','',["id"=>'topic','placeholder'=>'Enter topic',"class"=>"form-control",'onblur' => 'trimmer("topic")']) !!}
					</div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">{!! Form::label('Description') !!} <span id="descriptionErr" style="color:red"></span><span class="red">*</span>
					</label>
					<div class="col-sm-6">
                    {!! Form :: textarea('description','',['style'=>'height:200px;','id'=>'description','onblur' => 'trimmer("description")','placeholder'=>'Enter description ','class'=>'form-control'])  !!}
					</div>
                </div>
                
                <div class="form-group">
					<label class="col-sm-3 control-label">
                    {!! Form::label('Name') !!} <span id="nameErr" style="color:red"></span><span class="red">*</span>
					</label>
					<div class="col-sm-6">
                    {!! Form::text('name',$user_details->fname,["id"=>'name',"class"=>"form-control",'onblur' => 'trimmer("name")']) !!}
					</div>
                </div>
                
                <div class="form-group">
					<label class="col-sm-3 control-label">
                    {!! Form::label('Email') !!} <span id="emailErr" style="color:red"></span><span class="red">*</span>
					</label>
					<div class="col-sm-6">
                    {!! Form::text('email',$user_details->email,["id"=>'email',"class"=>"form-control",'onblur' => 'trimmer("email")']) !!}
					</div>
                </div>
                
                <div class="form-group">
					<label class="col-sm-3 control-label">
                    {!! Form::label('Phone') !!} <span id="phoneErr" style="color:red"></span><span class="red">*</span>
					</label>
					<div class="col-sm-6">
                    {!! Form::text('phone',$user_details->mobile,["id"=>'phone',"class"=>"form-control",'onblur' => 'trimmer("phone")']) !!}
					</div>
                </div>
                
                <div class="form-group margin-top">
					<label class="col-sm-3 control-label"></label>
					<div class="col-sm-6">
                    {!! Form::submit('Add',["id"=>'submit',"class"=>"btn  btn-success btn-lg"]) !!}
					<hr>
		            <p><span class="red">*</span> - Required Fields.</p>
					</div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<style>
    .prof-form textarea{color: #333;}	
</style>
@endsection

@section('script')
<script type="text/javascript">
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    
    function getData(val,appendId) {
        $.ajax({
            url: '{{url('front/complaint/getdata')}}',
            type: "post",
            data: {'restId':val, "_token":"{{ csrf_token() }}"},
            success: function (data) {
                $('#'+appendId).html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    }
    
    $(document).ready(function(){
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-zA-ZäÄöÖüÜß«» -]+$/i.test(value);
        }, "Topic must contain only letters.");
        
        $.validator.addMethod("email", function(value, element)
	{
            return this.optional(element) || /^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
	}, "Please enter valid email address.");
        
        $("#complaint_form").validate({
            rules: {
                restaurant: {
                    required: true
                },
                orders: {
                    required: true
                },
                topic: {
                    required : true,
                    minlength:3,
                    maxlength:100,
                    loginRegex:true
                },
                description: {
                    required: true,
                    minlength:6
                },
                name: {
                    required : true,
                    minlength:3,
                    loginRegex:true
                },
                email: {
                    required: true,
                    email:true
                },
                phone: {
                    required:true,
                    number:true,
                    minlength:10,
                    maxlength:12
                },
            },
            messages: {
                restaurant:{
                    required: "Please select restaurant",
                },
                orders:{
                    required: "Please select order",
                },
                topic:{
                    required: "Please enter topic",
                    loginRegex: "Topic name must contain only letters.",
                },
                description:{
                    required: "Please enter description",
                },
                name:{
                    required: "Please enter name",
                    loginRegex: "Name must contain only letters.",
                },
                email:{
                    required: "Please enter email",
                    email: "Please enter valid email"
                },
                message:{
                    required: "Please enter message",
                },
                phone: {
                    required: "Please enter phone number",
                    number: "Please enter valid phone number",
                    minlength: "Please enter valid phone number",
                    maxlength: "Please enter valid phone number"
                },
            },
        });
   });
</script>
@endsection
   
   


