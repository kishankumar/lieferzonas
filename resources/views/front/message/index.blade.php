@extends('layouts.frontbody')

@section('title')
@endsection

@section('body')
    <div class="container">
        <div class="row">
            @if($message == "done")
                <div class="alert alert-success">
                    Verification successful. Please login to access your account.
                </div>
            @endif
            @elseif($message == "error")
                    <div class="alert alert-danger">
                        Sorry error occurred. Please try again after some time.
                    </div>
            @endif
            @elseif($message == "already")
                    <div class="alert alert-success">
                        You are already a verified customer. Please login to access your account.
                    </div>
            @endif
            @else
                    <div class="alert alert-danger">
                        Sorry this verification does not exist.
                    </div>
            @endif
        </div>
    </div>
@endsection

@section('script')
@endsection