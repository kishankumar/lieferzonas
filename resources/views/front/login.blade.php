@extends('front_new.layout')

@section('title')
Lieferzonas - Login
@endsection

@section('head')
<style>

    .navbar-default {
        margin-bottom: 15px;
    }

    .lie-social-login-container {
        text-align: center;
        padding: 12px 0px;
        font-weight: bolder;
        cursor: pointer;
    }

    .lie-facebook-login-container {
        background-color: #3b5998;
        color: #ffffff;
    }

    .lie-twitter-login-container {
        background-color: #55acee;
        color: #FFFFFF;
    }

    .lie-social-logins-container {
        margin: 15px 0px;
    }

    .lie-contents {
        margin-top: 80px;
        width: 640px;
        min-width: 640px;
    }

</style>
@endsection

@section('contents')

<div class="lie-contents">

    {!! Form::open(['url'=>'frontuser/login','method'=>'post']) !!}

    <div class="form-group">
        <label>Email: </label>
        <input type="text" placeholder="Email" name="email" class="form-control">
    </div>

    <div class="form-group">
        <label>Passwort: </label>
        <input type="password" placeholder="Passwort" name="password" class="form-control">
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-block btn-success" value="Login">
    </div>

    <a href="#">Passwort vergessen?</a> <br>
    <a href="#">Kundendienst kontaktieren</a> <br>

    <div class="row lie-social-logins-container">
        <div class="col-xs-6 lie-social-login-container lie-facebook-login-container">
            <i class="fa fa-facebook"></i> Facebook
        </div>
        <div class="col-xs-6 lie-social-login-container lie-twitter-login-container">
            <i class="fa fa-twitter"></i> Twitter
        </div>
    </div>

    {!! Form::close() !!}
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $(".lie-facebook-login-container").click(function(evt) {
            // window.location.href = "{{ url('auth/facebook') }}";
        });
        $(".lie-twitter-login-container").click(function(evt) {
            // window.location.href = "{{ url('auth/twitter') }}";
        });
    });
</script>
@endsection