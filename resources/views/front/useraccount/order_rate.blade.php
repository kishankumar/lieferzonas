@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
 inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
    <div class="col-sm-8">
      <div class="rating_title mt25">Meine Bewertungen </div>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th width="20%" class="text-center">Zustellservice </th>
              <th width="25%" class="text-center">Lieferadresse </th>
              <th width="17%" class="text-center">Bestellzeit </th>
              <th width="15%" class="text-center">Preis </th>
              <th width="23%" class="text-center">Bewertung </th>
            </tr>
          </thead>
          <tbody>
		  <?php  
		  $current_time = strtotime(date('Y-m-d H:i:s')); 
		  ?>
		  @if(count($userorder))
            @foreach($userorder as $userorders)
		   <?php
		     $user_id = $userorders->front_user_id;
			 $rest_id = $userorders->rest_detail_id;
			 $order_id = $userorders->id;
			 $userorders->delivery_time;
			 $user_time = $userorders->user_time;
			 $delivery_time = strtotime($userorders->delivery_time);
			 $created_at = strtotime($userorders->created_at);
			
			 if($user_time=='0000-00-00 00:00:00')
			 {
				 $diff = ($delivery_time-$created_at)/60;
				 $diff =  (int)$diff.' '.'Minuten';
			 }
			 else
			 {
				  $diff ='PreOrder';
			 }
			 $rest_id = $userorders->rest_detail_id; 
			 $totalrating = \App\front\UserOrderReview::totalrating($rest_id);
			 if($totalrating != '')
			 {
				 $count = $totalrating/10;
				 $rating = \App\front\UserOrderReview::rating($rest_id);
					$quality_rating = '0';
					$service_rating = '0';
					$starrating =0;
				 foreach($rating as $ratings)
				 {
					 $quality_rating  = (float)$ratings->quality_rating + (float)$quality_rating;
					 $service_rating  = (float)$ratings->service_rating+(float)$service_rating;
					 $total_rating = $quality_rating+$service_rating;
					 
				 }
				 $starrating = $total_rating/($count*2);
			 }
			 
			
            ?> 
            <tr>
             <td>{{ date("d M Y h:i", strtotime( $userorders->created_at))  }} 
                <div> <img class="media-object" src="{{asset('public/uploads/superadmin/restaurants/'.$userorders->logo) }}" alt="profile" width="100"> </a>
				<strong>{{ $userorders->f_name.' '.$userorders->l_name }}
				</strong> </div>
				<div>#{{ $userorders->order_id }}</div>
              <td colspan="3" class="tbox"><table class="text-center">
                  <tr class="green">
                    <td width="44%"><div> {{ $userorders->booking_person_name }} </div>
			 
			  {{ $userorders->address.' '.$userorders->landmark.' '.$userorders->city_name.' '.$userorders->state_name.' '.$userorders->country_name.' '.$userorders->zipcode }}</td>
                    <td width="31%">{{ $diff }}</td>
                    <td width="33%">€ {{ $userorders->total_amount }}</td>
                  </tr>
				  <?php $orderrating = \App\front\UserOrderReview::orderrating($user_id,$userorders->order_id); 
				  //print_r($orderrating); count($orderrating); die;
				  if(count($orderrating)=='') { ?>
                  <tr>
                    <td colspan="3"><small class="fix-wt">Wir hoffen, dass alles zu ihre Zufriedenheit verlaufen ist
                      und würden gerne Ihre Meinung hören. Jede einzelne Information kann für andere 
                      Kunden, die auf der Suche nach dem perfekten Zustellservice sind, hilfreich sein.</small>
                      <div class="clearfix"></div>
                      <a href="#" class="btn-border" data-target="#" data-toggle="modal" onclick="open_modal(<?=$user_id?>,<?=$rest_id?>,'<?=$userorders->order_id?>');">Bewerte Deine Bestellung Jetzt</a> &nbsp; 5 Treuepunkte </td>
                  </tr>
				  <?php } else {
					  
					  $quality_rating  = $orderrating['0']['quality_rating'];
					  $service_rating  = $orderrating['0']['service_rating'];
				  ?>
				   <tr>
                    <td colspan="3">
						<h4><strong>Deine Bewertung</strong></h4>	
						<p>Quality: <span class="star2"> 
					    <?php	 
					          $qreview=floor($quality_rating);
							 
							 
							  for($i=1; $i<=$qreview; $i++)
							  {
								  
								  echo '<i class="fa fa-star"></i>';
							  }
							   
						   
							  if(ceil($quality_rating) > $qreview )
							  {
								 
								 echo '<i class="fa fa-star-half-o"></i>';
							   
							  }
							  
							  if(5 > ceil($quality_rating) )
							  {
							   
								for($i = ceil($quality_rating); $i<5; $i++)
								{
									
									echo '<i class="fa fa-star-o text-gray"></i>';
								}
								
							  }
							
			            ?>
						</span>   &nbsp; &nbsp; 
					     Service:<span class="star2"> 
						 <?php	 
					          $sreview=floor($service_rating);
							 
							 
							  for($i=1; $i<=$sreview; $i++)
							  {
								  
								  echo '<i class="fa fa-star"></i>';
							  }
							   
						   
							  if(ceil($service_rating) > $sreview )
							  {
								 
								 echo '<i class="fa fa-star-half-o"></i>';
							   
							  }
							  
							  if(5 > ceil($service_rating) )
							  {
							   
								for($i = ceil($service_rating); $i<5; $i++)
								{
									
									echo '<i class="fa fa-star-o text-gray"></i>';
								}
								
							  }
							
			            ?>
						 </span>   </p>
                      <a href="#" class="btn-border">5 Get Bonus Points</a> </td>
                   </tr>
				  <?php } ?>
                </table></td>
              <td><span class="star2">  
			  <?php
			  
			  if($totalrating != '0')
			  {
				  $review=floor($starrating);
				 
				 
				  for($i=1; $i<=$review; $i++)
				  {
					  
					  echo '<i class="fa fa-star"></i>';
				  }
				   
			   
				  if(ceil($starrating) > $review )
				  {
					 
					 echo '<i class="fa fa-star-half-o"></i>';
				   
				  }
				  
				  if(5 > ceil($starrating) )
				  {
				   
					for($i = ceil($starrating); $i<5; $i++)
					{
						
						echo '<i class="fa fa-star-o text-gray"></i>';
					}
					
				  }
				  ?>
				  </span> @if($totalrating != '') ( {{$count}}   ) @endif<br>
			      <?php } 
				  else { ?>
				  <span class="review-s">No Review Yet</span><br>
				  <?php } ?>
                <div class="tb_bt"></div>
                <a href="{{url('restaurant/'.$userorders->rest_detail_id.'/search')}}" class="btn-border2">WIEDER BESTELLEN</a></td>
            </tr>
			<?php
			$timediff = \App\front\UserOrderReview::restopentimediff($rest_id);
			?>
			@if($timediff!=0)
					<?php
					$hours = floor($timediff / 60);
                    $minutes = ($timediff % 60);
					?>
					<tr>
                    <td colspan="5"><h4><strong class="red">Es Öffnet in {{ $hours }} Stunden {{ $minutes }} Minuten</strong> Lieferung ab: 10,00 €, Lieferkosten: 0,00 €</h4></td>
                    </tr>
					
			        @else
					<tr>
                    <td colspan="5"><h4><strong class="red">Open</strong> Lieferung ab: 10,00 €, Lieferkosten: 0,00 €</h4></td>
                    </tr>
					@endif
			@endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
           
            <!--<tr>
              <td> 18. June 2015, 01:55
                <div class="logo_nun"> Logo <strong>#48LUD2</strong> </div></td>
              <td colspan="3" class="tbox"><table class="text-center">
                  <tr class="green">
                    <td width="44%">Margaretenstrasse 105 10117 Berlin (Mitte)</td>
                    <td width="31%">67 Minuten</td>
                    <td width="33%">€ 15,95</td>
                  </tr>
                  <tr>
                    <td colspan="3"><small class="fix-wt">Wir hoffen, dass alles zu ihre Zufriedenheit verlaufen ist
                      und würden gerne Ihre Meinung hören. Jede einzelne Information kann für andere 
                      Kunden, die auf der Suche nach dem perfekten Zustellservice sind, hilfreich sein.</small>
                      <div class="clearfix"></div>
                      <a href="#" class="btn-border">Bewerte Deine Bestellung Jetzt</a> &nbsp; 5 Bonuspunkte </td>
                  </tr>
                </table></td>
              <td><span class="star2"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span> (1382) <br>
                <div class="tb_bt"></div>
                <a href="#" class="btn-border2">WIEDER BESTELLEN</a></td>
            </tr>
            <tr>
              <td colspan="5"><h4><strong class="red">Es Öffnet in 8 Stunden 53 Minuten</strong> Lieferung ab: 10,00 €, Lieferkosten: 0,00 €</h4></td>
            </tr>
            <tr>
              <td> 18. June 2015, 01:55
                <div class="logo_nun"> Logo <strong>#48LUD2</strong> </div></td>
              <td colspan="3" class="tbox"><table class="text-center">
                  <tr class="green">
                    <td width="44%">Margaretenstrasse 105 10117 Berlin (Mitte)</td>
                    <td width="31%">67 Minuten</td>
                    <td width="33%">€ 15,95</td>
                  </tr>
                  <tr>
                    <td colspan="3"><small class="fix-wt">Wir hoffen, dass alles zu ihre Zufriedenheit verlaufen ist
                      und würden gerne Ihre Meinung hören. Jede einzelne Information kann für andere 
                      Kunden, die auf der Suche nach dem perfekten Zustellservice sind, hilfreich sein.</small>
                      <div class="clearfix"></div>
                      <a href="#" class="btn-border">Bewerte Deine Bestellung Jetzt</a> &nbsp; 5 Bonuspunkte </td>
                  </tr>
                </table></td>
              <td><span class="star2"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span> (1382) <br>
                <div class="tb_bt"></div>
                <a href="#" class="btn-border2">WIEDER BESTELLEN</a></td>
            </tr>
            <tr>
              <td colspan="5"><h4><strong class="red">Es Öffnet in 8 Stunden 53 Minuten</strong> Lieferung ab: 10,00 €, Lieferkosten: 0,00 €</h4></td>
            </tr>
            <tr>
              <td> 18. June 2015, 01:55
                <div class="logo_nun"> Logo <strong>#48LUD2</strong> </div></td>
              <td colspan="3" class="tbox"><table class="text-center">
                  <tr class="green">
                    <td width="44%">Margaretenstrasse 105 10117 Berlin (Mitte)</td>
                    <td width="31%">67 Minuten</td>
                    <td width="33%">€ 15,95</td>
                  </tr>
                  <tr>
                    <td colspan="3">
						<h4><strong>Deine Bewertung</strong></h4>	
						<p>Quality: <span class="star2"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>  (1382) &nbsp; &nbsp; 
					    Delivery:<span class="star2"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>  (1382) </p>
                      <a href="#" class="btn-border">5 Get Bonus Points</a> </td>
                  </tr>
                </table></td>
              <td><span class="star2"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span> (1382) <br>
                <div class="tb_bt"></div>
                <a href="#" class="btn-border2">WIEDER BESTELLEN</a></td>
            </tr>
            <tr>
              <td colspan="5"><h4><strong class="red">Es Öffnet in 8 Stunden 53 Minuten</strong> Lieferung ab: 10,00 €, Lieferkosten: 0,00 €</h4></td>
            </tr>-->
          </tbody>
        </table>
		  {!! $userorder->render() !!}    
      </div>
    </div>

    @include('includes/frontusersidebar')

    </div>
    </div>
<div aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal_rating" class="modal fade">
	<div class="modal-dialog modal-md robotoregular">
		<div class="modal-content">
			<div style="background:#81c02f; color:#fff;" class="modal-header">
				<button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Review your order now</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" name="user_id" id="user_id">
					<input type="hidden" name="rest_id" id="rest_id">
					<input type="hidden" name="order_id" id="order_id">
						<div class="col-xs-12">
							<div class="form-group qrating" >
								Quality: <input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1" onkeyup="hideErrorMsg('qratingerr')"/>
								<div class="clearfix"></div>
								<div id="qratingerr" class="text-danger text-right"></div>
							</div>
							<div class="form-group drating" onClick="hideErrorMsg('dratingerr')" >  
								Service: <input id="kartik" class="rating" data-stars="5" data-size="xs" data-step="0.1"/>
								<div class="clearfix"></div>
								<div id="dratingerr" class="text-danger text-right"></div>
							</div>
							<div class="form-group">
								<textarea placeholder="Comment" id="comment" name="comment" class="form-control modal-input"></textarea>
							</div>
							<div class="form-group">
								<button  id="review"  class="btn btn-success btn-block brn-rg" onclick="add_review();">Submit</button>
							</div>
						</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')

@endsection

