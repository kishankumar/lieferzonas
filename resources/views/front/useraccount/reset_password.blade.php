@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

  
    <div class="container">
        <div class="row">
		
   <div class="col-sm-8 col-md-8 col-lg-8 mt25">
    @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    @foreach( $errors->all()  as $error)
                    <li>{{ $error }}</li>
                    @endforeach
         @endif
     {!! Form::open(array('route' => 'front.forgotpass.store','id'=>'reset_pass','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}
     
        <h3>Reset Password</h3>
		
        <div class="require_star"><span class="red">*</span>
		 {!! Form::password('newpassword',["id"=>'newpassword',"class"=>"form-control","placeholder"=>"New Password","onkeypress"=>"hideErrorMsg('npasserr')"]) !!}
		 {!! Form::hidden('act_key',$act_key,["id"=>'act_key',"class"=>"form-control"]) !!}
		<div id="npasserr" class="text-danger"></div>
        </div>
		 <div class="clearfix"></div>
        <div class="require_star"> <span class="red">*</span>
         {!! Form::password('conpassword',["id"=>'conpassword',"class"=>"form-control","placeholder"=>"Confirm Password","onkeypress"=>"hideErrorMsg('cpasserr')"]) !!}
		  <div id="cpasserr" class="text-danger"></div>
        </div>
		 <div class="clearfix"></div>
       
        <div class="require_star"> <span class="red"></span>
		{!!Form::button('Reset Password',["id"=>'changepass',"class"=>"btn btn-success","onclick"=>"validate_pass()"]) !!}
          <hr>
		  <p><span class="red">*</span> - Required Fields.</p>
        </div>
       {!! Form::close() !!}
     
    </div>
   

    </div>
    </div>

@endsection

@section('script')
<script>

</script>
@endsection