@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
 inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
    <div class="col-sm-8 col-md-8 col-lg-8">
        <div class="rating_title mt25"> Miene Adressen <span class="pull-right"><a href="address/create"><i class="fa fa-plus"></i></a></span> 
        </div>
        
        @if(Session::has('message'))
            <div class="alert alert-success" style="padding: 7px 15px;">
                {{ Session::get('message') }}
                {{ Session::put('message','') }}
            </div>
        @endif
        
        <table class="table table-hover table-striped">
            <thead>
            <tr>
			    <th>Mobile no </th>
                <th>Address </th>
                <th>City </th>
                <th>State </th>
                <th>Country</th>
                <th>Zipcode</th>
            </tr>
            </thead>
            <tbody>
			@if(count($address))
                    @foreach($address as $useraddress)
				
            <tr>
                <td>{{ $useraddress['mobile'] }}</td>
                <td>{{ $useraddress['address'] }} {{ $useraddress['landmark'] }}</td>
                <td>{{ $useraddress['city_name'] }}</td>
				<td>{{ $useraddress['state_name'] }}</td>
                <td>{{ $useraddress['country_name'] }}</td>
                <td>{{ $useraddress['zipcode_name'] }}</td>
                <td align="right">
				<a href="{{url('front/address/'.$useraddress['id'].'/edit')}}" class="text-default"> &nbsp; <i class="fa fa-wrench"></i></a> 
				<a href="{{url('front/address/delete/'.$useraddress['id'].'')}}" onclick="return confirm('Are You Sure, want To Delete!')" class="text-danger"> &nbsp; <i class="fa fa-trash-o"></i></a></td>
            </tr>
            @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
             
    </div>

    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

@section('script')
@endsection