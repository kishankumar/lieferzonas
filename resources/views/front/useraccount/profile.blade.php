@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
    <div class="row">
	
    <div class="col-sm-8 col-md-8 col-lg-8 mt25">
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                @if($errors->any())
                    <div class="alert alert-danger">
                                <ul>
                            @foreach($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                                </ul>
                    </div>
                    
                @endif
     {!! Form::model($profile,['route'=>['front.profile.update',$profile['0']['id']],'method'=>'patch','id' => 'profile','class' => 'prof-form form','novalidate' => 'novalidate', 'files' => true])	!!}
        <!--<div class="form-group">
          <label class="radio-inline">
            <input type="radio" name="female">
            <span> Ms.</span></label>
          <label class="radio-inline">
            <input type="radio" name="female">
            <span>Mrs.</span></label>
        </div>-->
        <div class="require_star"> <span class="red">*</span>
          
		  {!! Form::text('firstname',$profile['0']['fname'],["id"=>'firstname',"class"=>"form-control","placeholder"=>"First Name"]) !!}
		  {!! Form::hidden('id',$profile['0']['id'],["id"=>'fuid',"class"=>"form-control","placeholder"=>"First Name"]) !!}
        </div>
        <div class="require_star"> <span class="red">*</span>
        
		  {!! Form::text('lastname',$profile['0']['lname'],["id"=>'lastname',"class"=>"form-control","placeholder"=>"Last Name"]) !!}
        </div>
		 <div class="require_star"><span class="red">*</span>
        
		  {!! Form::text('nickname',$profile['0']['nickname'],["id"=>'nickname',"class"=>"form-control","placeholder"=>"Nick Name"]) !!}
        </div>
		<div class="require_star"> <span class="red">*</span>
        
		 <select name="gender" id="gender" class="form-control" >
		 <option value=''>Select</option>
		 <option value='m' <?php if($profile['0']['gender']=='m') echo "Selected='selected'" ;?> >Male</option>
		 <option value='f' <?php if($profile['0']['gender']=='f') echo "Selected='selected'" ;?> >Female</option>
		 <option value='o' <?php if($profile['0']['gender']=='o') echo "Selected='selected'" ;?> >Other</option>
		 </select>
        </div>
        <div class="require_star"> <span class="red">*</span>
          
		  {!! Form::input('text', 'dob', $profile['0']['dob'], ["id"=>"datetimepicker5","class" => 'form-control input_gray',"placeholder"=>"DOB"]) !!}
        </div>
        <div class="require_star"><span class="red">*</span>
         
		  {!! Form::text('mobile',$profile['0']['mobile'],["id"=>'mobile',"class"=>"form-control","placeholder"=>"Mobile No"]) !!}
        </div>
		<div class="require_star">
          <div class="media">
        	<div class="pull-left">
			@if ($profile['0']['profile_pic'])
		    <img class="img-upload" src="{{asset('public/front/uploads/users/'.$profile['0']['profile_pic']) }}" style="max-width: 70px">
			@endif
            </div>
        	<div class="media-body">
		    <input type="file" name="pics" id="pics"  onchange="validateimage(this)" >
            </div>
          </div>
        </div>
        
        <!--<div class="require_aria">
          <div class="form-group">
            <textarea class="form-control" id="" rows="4" placeholder=" Further information"></textarea>
          </div>
        </div>-->
        <div class="clearfix"></div>
        <div class="form-group margin-top text-center">
          <input type="submit" class="btn btn-border-big" value="Save Changes">
		   <hr>
		  <p><span class="red">*</span> - Required Fields.</p>
        </div>
     {!! Form::close() !!}
      
    </div>

    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

@section('script')

@endsection