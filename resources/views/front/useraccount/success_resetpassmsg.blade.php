@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

  
    <div class="container">
        <div class="row">
		
   <div class="col-sm-8 col-md-8 col-lg-8 mt25">
  
     {!! Form::open(array('route' => 'front.forgotpass.store','id'=>'reset_pass','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}
     
        <h3>Password reset successfully</h3>
		
       {!! Form::close() !!}
     
    </div>
   

    </div>
    </div>

@endsection

@section('script')
<script>

</script>
@endsection