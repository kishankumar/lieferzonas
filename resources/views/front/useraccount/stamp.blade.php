@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
 inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
     <div class="col-sm-8 col-md-8 col-lg-8">
      <div class="tabbing">
        <ul class="media-list media-1">
		@if(count($stamplist))
		@foreach($stamplist as $stamplists)
	    <?php if($stamplists->logo!='')
		{
			$logo = $stamplists->logo;
		}
		else{
			$logo = 'logo_de.png';
		}
		?>
          <li class="media"> <a class="pull-left" href="#"> <img class="media-object" src="{{asset('public/uploads/superadmin/restaurants/'.$logo) }}" alt="profile" width="100"> </a>
            <div class="media-body">
              <div class="well well-lg">
                <div class="row">
                  <div class="col-sm-7 col-lg-8">
                    <h4 class="media-heading text-uppercase reviews">{{$stamplists->f_name.' '.$stamplists->l_name}}</h4>
					<?php 
			          $day_time = \App\superadmin\RestDayTimeMap::daytime($stamplists->id);
					  if(count($day_time)>0)
					  {
					?>
                    <ul class="media-date text-uppercase reviews list-inline">
                      <li class="dd">@if($day_time['0']['is_open']==1) {{ $day_time['0']['open_time'] }} bis {{ $day_time['0']['close_time'] }} Folgetag @else closed @endif </li>
                    </ul>
                    <p class="media-comment"> Lieferung ab: 10,00 €, Lieferkosten: 0,00 €. </p>
                    </div>
                    <div class="col-sm-5 col-lg-4">
					<?php } ?>
				   <?php
				   $rest_id = $stamplists->rest_detail_id;
				   $totalrating = \App\front\UserOrderReview::totalrating($rest_id);
					 if($totalrating != '')
					 {
						 $count = $totalrating/10;
						 $rating = \App\front\UserOrderReview::rating($rest_id);
							$quality_rating = '0';
							$service_rating = '0';
							$starrating =0;
						 foreach($rating as $ratings)
						 {
							 $quality_rating  = (float)$ratings->quality_rating + (float)$quality_rating;
							 $service_rating  = (float)$ratings->service_rating+(float)$service_rating;
							 $total_rating = $quality_rating+$service_rating;
							 
						 }
						 $starrating = $total_rating/($count*2);
					 }
					 
				  ?>
                    <div class="rate-ap stars">
                      <ul class="list-inline star">
                       <?php
			  
						  if($totalrating != '0')
						  {
							  $review=floor($starrating);
							 
							 
							  for($i=1; $i<=$review; $i++)
							  {
								  
								  echo '<li><i class="fa fa-star"></i></li>';
							  }
							   
						   
							  if(ceil($starrating) > $review )
							  {
								 
								 echo '<li><i class="fa fa-star-half-o"></i></li>';
							   
							  }
							  
							  if(5 > ceil($starrating) )
							  {
							   
								for($i = ceil($starrating); $i<5; $i++)
								{
									
									echo '<li><i class="fa fa-star-o text-gray"></i></li>';
								}
								
							  }
						?>
                      </ul>
                    <small>({{ $count }})</small>
				    <?php } else { ?>
					 No Review Yet
					<?php } ?>
					</div>
                    <div class="clearfix"></div>
                    <a href="#" class="list-arro"><i class="fa fa-angle-right"></i></a>
                    <div class="clearfix"></div>
                  </div>
                  <div class="col-sm-12 text-center">
                    <ul class="list-inline stamp">
					<?php $activecount = $stamplists->stamp_count; ?>
					 @if(count($stampcount))
					 <?php for($i=0;$i<$stampcount['0']['stamp_count']; $i++ ) 
					 {  
				        if($activecount>0)
						{
					?>
						<li  class="act" ><img src="{{asset('public/front/uploads/stamp/stamp.png') }}"></li>
						 
					<?php $activecount--;	}
						
						else{
				     ?>
				       
                      <li  class="" ><img src="{{asset('public/front/uploads/stamp/stamp.png') }}"></li>
					
					 <?php }  }?>
				     @endif
                      
                    </ul>
					{!! $stamplist->render() !!}
                  </div>
                </div>
              </div>
            </div>
          </li>
		  @endforeach
		  @endif
          
        </ul>
        <!--<div class="text-center">
          <ul class="pagination">
            <li class=""><a href="#"><i class="fa fa-angle-left"></i></a></li>
            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
          </ul>
        </div>-->
      </div>
    
	  	
    </div>

    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

@section('script')
@endsection