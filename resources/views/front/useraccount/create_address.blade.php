@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
		 
   <div class="col-sm-8 col-md-8 col-lg-8 mt25">
   @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    <div class="alert alert-danger">
                                <ul>
                            @foreach($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                                </ul>
                    </div>
                    
                @endif
     {!! Form::open(array('route' => 'front.address.store','id'=>'address_form','class' => 'prof-form form','novalidate' => 'novalidate', 'files' => true)) !!}
     
        <h3>Add Address</h3>
		<div class="require_star"> <span class="red">*</span>
          {!! Form::text('mobile','',["id"=>'mobile',"class"=>"form-control ","placeholder"=>"Mobile"]) !!}
        </div>
        <div class="require_star"> <span class="red">*</span>
		 {!! Form::text('address','',["id"=>'address',"class"=>"form-control","placeholder"=>"Address"]) !!}
        </div>
        <div class="require_star"> 
         {!! Form::text('landmark','',["id"=>'landmark',"class"=>"form-control","placeholder"=>"Landmark"]) !!}
        </div>
		<div class="require_star"> <span class="red">*</span>
          <select id="country" name="country" class="form-control js-example-basic-multiple" onchange="getdatas(this.value,'countries','states','state','id','name','id','country_id')" >
                                <option value="">Select Country</option>
                                <?php
                                foreach($country as $country) { ?>
                                    <option value="<?=$country['id'] ?>"><?=$country['name'] ?></option>
                                <?php
                                } ?>
          </select>
        </div>
		<div class="require_star"> <span class="red">*</span>
         <select id="state" name="state" class="form-control js-example-basic-multiple" onchange="getdatas(this.value,'states','cities','city','id','name','id','state_id')" >
                              
         </select>
        </div>
		<div class="require_star"> <span class="red">*</span>
           <select id="city" name="city" class="form-control js-example-basic-multiple" onchange="getdatas(this.value,'cities','zipcodes','zipcode','id','name','id','city_id')" >
                               
                                
           </select>
        </div>
         <div class="require_star"> <span class="red">*</span>
		    <select id="zipcode" name="zipcode" class="form-control js-example-basic-multiple"  >
                               
                                
           </select>
         
        </div>
        <div class="clearfix"></div>
        <div class="form-group margin-top">
          {!! Form::submit('Save Changes',["id"=>'submit',"class"=>"btn btn-green btn-lg"]) !!}
		  <hr>
		  <p><span class="red">*</span> - Required Fields.</p>
        </div>
       {!! Form::close() !!}
     
    </div>
    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

@section('script')
<script>



</script> 
@endsection