@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
 inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
         <div class="col-sm-8 col-md-8 col-lg-8">
        <div class="rating_title mt25">Cashback Point History</div>
        <table class="table table-hover table-striped">
            <thead>
            <tr>
			    <th>S.No.</th>
                <th>Event</th>
                <th>Order Id</th>
                <th>Transaction Type</th>
                <th>Amount</th>
                
            </tr>
            </thead>
            <tbody>
			@if(count($cashbackhistory))
			<?php $i=1; ?>
                    @foreach($cashbackhistory as $history)
				
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $history->cashback_title }}</td>
                <td>{{ $history->order_id }}</td>
				<td>@if($history->creditordebit==1)  Credit  @else  Debit  @endif </td>
                <td>{{ $history->amount }}</td>
               
                
            </tr>
			<?php $i++; ?>
            @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
             
    </div>

    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

@section('script')
@endsection