@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="big-content">
                    <?php
                    $totalbonuspoint = \App\front\UserBonusPoint::bonuspoint();

                    ?>
                    <h2 class="big-title">{{ $totalbonuspoint }}<span>TREUEPUNKTE </span></h2>
                    <hr>
                </div>
                <ul class="green-list">
                    <li> Erhaltene Treuepunkte:</li>
                    @if(count($creditbpoint))
                        <?php $cpoints = '0'; ?>
                        @foreach($creditbpoint as $creditbpoints)
                            <?php
                            $cpoints = $cpoints + (int)$creditbpoints->awarded_points;
                            ?>
                        @endforeach
                    @else
                        <?php $cpoints = 0; ?>
                    @endif
                    <li> {{$cpoints}}</li>

                    <li> Abgelaufene Treuepunkte:</li>
                    @if(count($expirebpoint))
                        <?php $upoints = '0';
                        $apoints = '0';
                        ?>
                        @foreach($expirebpoint as $expirebpoints)
                            <?php
                            $upoints = $upoints + (int)$expirebpoints->used_points;
                            $apoints = $apoints + (int)$expirebpoints->awarded_points;

                            ?>
                        @endforeach
                        <?php $epoints = ($apoints - $upoints); ?>
                    @else
                        <?php $epoints = 0; ?>
                    @endif
                    <li> {{$epoints}}</li>

                    <li> Ausgegebene Treuepunkte:</li>
                    @if(count($debitbpoint))
                        <?php $dpoints = '0'; ?>
                        @foreach($debitbpoint as $debitbpoints)
                            <?php
                            $dpoints = $dpoints + (int)$debitbpoints->used_points;
                            ?>
                        @endforeach
                    @else
                        <?php $dpoints = 0; ?>
                    @endif
                    <li> {{$dpoints}}</li>

                    <li> Zwischensumme Treuepunkte:</li>
                    @if(count($totalbpoint))
                        <?php $upoints = '0';
                        $apoints = '0';
                        ?>
                        @foreach($totalbpoint as $totalbpoints)
                            <?php
                            $upoints = $upoints + (int)$totalbpoints->used_points;
                            $apoints = $apoints + (int)$totalbpoints->awarded_points;

                            ?>
                        @endforeach
                        <?php $tpoints = ($apoints - $upoints); ?>
                    @else
                        <?php $tpoints = 0; ?>
                    @endif
                    <li> {{$tpoints}}</li>

                </ul>
                <div class="clearfix"></div>

                <div class="rating_title mt25">Bonus Point History</div>
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Event</th>
                        <th>Order Id</th>
                        <th>Transaction Type</th>
                        <th>Amount</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(count($bonushistory))
                        <?php $i = 1; ?>
                        @foreach($bonushistory as $history)

                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $history->activity_title }}</td>
                                <td>{{ $history->order_id }}</td>
                                <td>@if($history->creditordebit==1)  Credit  @else  Debit  @endif </td>
                                <td>{{ $history->amount }}</td>


                            </tr>
                            <?php $i++; ?>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" align="center">
                                No Record Exist
                            </td>
                        </tr>
                    @endif
                    </tbody>

                </table>

            </div>

            @include('includes/frontusersidebar')

        </div>
    </div>

@endsection

@section('script')
@endsection