@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
 inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
    <div class="row">
	<div class="col-sm-12">

	  <div><span><b>Order No:</b>{{ $order_id }}</span></div> 
	   <div class="row bs-wizard" style="border-bottom:0;">
	    @if(count($order_status))
			<?php $i=1; $striker = 0;  
		    //echo'<pre>'; print_r($order_status); die; ?>
		    @foreach($order_status as $status)	
			
			  <?php  
			 
			  $select = in_array( $status->id, $order_track) ;
			   if($select)
			   {
				   $class="complete";
			   }
			  else
			  {  
		          
				  if(in_array( 4, $order_track) && ($status->id != 3) ){
                   $cdate = \App\front\FrontOrderStatusMap::status_date($order_id,4);	
				 
					if(($key = array_search(4, $order_track)) != false) {
							unset($order_track[$key]);
						}
					
				  ?>
					 <div class="bs-wizard-step cancelled">
					 
					 <div class="text-center bs-wizard-stepnum">Step {{ $i }}</div>
					  <div class="progress"><div class="progress-bar"></div></div>
					  <a href="#" class="bs-wizard-dot"></a>
					   <div class="bs-wizard-info text-center">Cancelled </div>
					  <div class="bs-wizard-info text-center">{{ date("d M Y H:i:s", strtotime($cdate['0']['created_at'])) }}</div>
					 </div>
						<?php $i++; 
						$striker = 1;
				  }
				  //echo $striker;
				  //$class = ($striker == 0) ? 'disabled' : 'disabled striker hide';
				 
				  $class = ($striker == 0) ? 'disabled striker' : 'disabled striker hide';
				
			  }
			  
			  if(!(in_array( 3, $order_track) && $status->id == 2 )){ 
			   ?>
               @if(($status->id == 3 && $class== "complete")  ||( $status->id != 3 && $status->id != 4) )
				<?php if($status->id == 3) { $class="cancelled"; $striker = 1; } ?>
					<div class="bs-wizard-step <?=$class?>">
					 
					 <div class="text-center bs-wizard-stepnum">Step {{ $i }}</div>
					  <div class="progress"><div class="progress-bar"></div></div>
					  <a href="#" class="bs-wizard-dot"></a>
					   <div class="bs-wizard-info text-center">{{ $status->status_name }} </div>
			    <?php  
			          $status_id = $status->id;
					  $order_id;
			          $co_date = \App\front\FrontOrderStatusMap::status_date($order_id,$status_id); 
					  $curr_date='';
					  if(count($co_date)){
						  $curr_date= date("d M Y H:i:s", strtotime($co_date[0]['created_at']));
					  }
			    ?>
					  <div class="bs-wizard-info text-center">
						{{ $curr_date }}
					  </div>
					</div>
						<?php $i++; ?>
					
				@endif
			  <?php } ?>
		    @endforeach 
		  
				<!--<div class="bs-wizard-step complete">
				  <div class="text-center bs-wizard-stepnum">Step 2</div>
				  <div class="progress"><div class="progress-bar"></div></div>
				  <a href="#" class="bs-wizard-dot"></a>
				  <div class="bs-wizard-info text-center"></div>
				</div>
			  
				<div class="bs-wizard-step disabled">
				  <div class="text-center bs-wizard-stepnum">Step 2</div>
				  <div class="progress"><div class="progress-bar"></div></div>
				  <a href="#" class="bs-wizard-dot"></a>
				  <div class="bs-wizard-info text-center"></div>
				</div>

				<div class="bs-wizard-step disabled">
				  <div class="text-center bs-wizard-stepnum">Step 3</div>
				  <div class="progress"><div class="progress-bar"></div></div>
				  <a href="#" class="bs-wizard-dot"></a>
				  <div class="bs-wizard-info text-center"></div>
				</div>

				<div class="bs-wizard-step disabled">
				  <div class="text-center bs-wizard-stepnum">Step 4</div>
				  <div class="progress"><div class="progress-bar"></div></div>
				  <a href="#" class="bs-wizard-dot"></a>
				  <div class="bs-wizard-info text-center"> </div>
				</div>-->
		  @endif
		  
		  
		  
			
		</div>
    </div>  
  </div>
    </div>

@endsection

@section('script')
@endsection