@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
		
   <div class="col-sm-8 col-md-8 col-lg-8 mt25">
    @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    @foreach( $errors->all()  as $error)
                    <li>{{ $error }}</li>
                    @endforeach
         @endif
     {!! Form::open(array('route' => 'front.changepass.store','id'=>'change_pass','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}
     
        <h3>Change Password</h3>
		<div class="require_star"> <span class="red">*</span>
          {!! Form::password('oldpassword',["id"=>'oldpassword',"class"=>"form-control input_gray","placeholder"=>"Old Password","onkeypress"=>"hideErrorMsg('opasserr')"]) !!}
		 
        </div>
		 <span id="opasserr" class="text-danger"></span>
	   <div class="clearfix"></div>
        <div class="require_star"> <span class="red">*</span>
		 {!! Form::password('newpassword',["id"=>'newpassword',"class"=>"form-control input_gray","placeholder"=>"New Password","onkeypress"=>"hideErrorMsg('npasserr')"]) !!}
		  
        </div>
		 <span id="npasserr" class="text-danger"></span>
	   <div class="clearfix"></div>
        <div class="require_star"> <span class="red">*</span>
         {!! Form::password('conpassword',["id"=>'conpassword',"class"=>"form-control input_gray","placeholder"=>"Confirm Password","onkeypress"=>"hideErrorMsg('cpasserr')"]) !!}
		  
        </div>
		 <span id="cpasserr" class="text-danger"></span>
       <div class="clearfix"></div>
        <div class="require_star"> <span>&nbsp;</span>
		{!!Form::button('Change Password',["id"=>'changepass',"class"=>"btn btn-green2","onclick"=>"pass_validation()"]) !!}
          <hr>
		  <p><span class="red">*</span> - Required Fields.</p>
        </div>
       {!! Form::close() !!}
     
    </div>
    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

@section('script')
<script>
function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn) 
{

         $.ajax({

                url: '{{url('getdatas')}}',
                type: "get",
                data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn,
				'selectedOption':''},
                
             success: function (data) {
             
           $('#'+appendId).html(data);
           
          },
             error: function (jqXHR, textStatus, errorThrown) {
               
             }
         });
}


			
	function hideErrorMsg(id){
    
      $('#'+id).attr('style', 'display:none');
  
    
     }
</script>
@endsection