@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
    <div class="row">
	
    <div class="col-sm-8 col-md-8 col-lg-8 mt25">
     @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    @foreach( $errors->all()  as $error)
                    <li>{{ $error }}</li>
                    @endforeach
    @endif
	
     <div class="tabbing" style="margin-top:50px;">
        <ul class="media-list media-1">
		@if(count($restuarant))
            @foreach($restuarant as $rest)
		<?php 
		if($rest['f_id']=='')
		{
			$heartclass = 'fa fa-heart';
		}
		else
		{
			$heartclass = 'fa fa-heart active';
		}
		$auth_id = Auth::user('front')->id; 
		if($rest['logo']!='')
		{
			$logo = $rest['logo'];
		}
		else{
			$logo = 'logo_de.png';
		}
		?>
		
          <li class="media"> <a class="pull-left" href="{{url('restaurant/'.$rest['id'].'/search')}}"> <img class="media-object" src="{{asset('public/uploads/superadmin/restaurants/'.$logo) }}" alt="profile" width="100"> </a>
            <div class="media-body">
              <div class="well well-lg">
                <div class="row">
                  <div class="col-sm-7 col-lg-8">
                    <h4 class="media-heading text-uppercase reviews">{{ $rest['f_name'] }} {{ $rest['l_name'] }}</h4>
					<?php 
			          $day_time = \App\superadmin\RestDayTimeMap::daytime($rest['id']);
					  //print_r($day_time); die;
					    if(count($day_time)>0)
						{
							$open_time = $day_time['0']['open_time']; 
							$open_time = date('H:i', strtotime($open_time));
							$current_time =date('H:i', time());
							if($open_time>$current_time)
							{
								$diff = (strtotime($open_time)-strtotime($current_time))/60; 
							}
							else
							{
								$diff =0;
							}
						
							
                    ?>
                    <ul class="media-date text-uppercase reviews list-inline">
                      <li class="dd"> @if($day_time['0']['is_open']==1) {{ $day_time['0']['open_time'] }} bis {{ $day_time['0']['close_time'] }} @else closed @endif</li>
                    </ul>
					<?php } ?>
                    <p class="media-comment"> Lieferung ab: 10,00 €, Lieferkosten: 0,00 € </p>
					@if(count($day_time)>0)
						@if($diff!=0)
						<?php
						$hours = floor($open_timediff / 60);
						$minutes = ($open_timediff % 60);
						?>
						<p>@if($day_time['0']['is_open']==1) {{ $hours }} Hours {{ $minutes }} Minutes  remains to open this restaurant</p>
						@else
						<p><strong class="red">Open</strong></p>
						@endif
						@endif
					@endif
                  </div>
                  <div class="col-sm-5 col-lg-4">
				  <?php
				  $rest_id = $rest['id'];
				   $totalrating = \App\front\UserOrderReview::totalrating($rest_id); 
					 if($totalrating != 0)
					 {
						 $count = $totalrating/10;
						 $rating = \App\front\UserOrderReview::rating($rest_id);
							$quality_rating = '0';
							$service_rating = '0';
							$starrating =0;
						 foreach($rating as $ratings)
						 {
							 $quality_rating  = (float)$ratings->quality_rating + (float)$quality_rating;
							 $service_rating  = (float)$ratings->service_rating+(float)$service_rating;
							 $total_rating = $quality_rating+$service_rating;
							 
						 }
						 $starrating = $total_rating/($count*2);
					 }
					 else{
						 $count = 0;
					 }
				  ?>
                    <div class="rate-ap stars">
                      <ul class="list-inline star">
                        <!--<li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star-half-o"></i></li>-->
						<?php
			  
						  if($totalrating != '0')
						  {
							  $review=floor($starrating);
							 
							 
							  for($i=1; $i<=$review; $i++)
							  {
								  
								  echo '<li><i class="fa fa-star"></i></li>';
							  }
							   
						   
							  if(ceil($starrating) > $review )
							  {
								 
								 echo '<li><i class="fa fa-star-half-o"></i></li>';
							   
							  }
							  
							  if(5 > ceil($starrating) )
							  {
							   
								for($i = ceil($starrating); $i<5; $i++)
								{
									
									echo '<li><i class="fa fa-star-o text-gray"></i></li>';
								}
								
							  }
						    }
					    ?>
                      </ul>
                      <small>({{ $count }})</small> 
					</div>
                    <div class="clearfix"></div>
                    <a href="#" class="list-arro"><i class="fa fa-angle-right"></i></a>
                    <p class="heart" onclick="favourite(<?=$rest['id']?>,<?=$auth_id?>);"><i class="<?=$heartclass?>"></i></p>
				
                  </div>
                </div>
              </div>
            </div>
          </li>
		    @endforeach
           
            @else 
                    </li>
                        
                    </li>
                 @endif
        </ul>
		{!! $restuarant->render(); !!}
		</div>
		
    </div>

    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

