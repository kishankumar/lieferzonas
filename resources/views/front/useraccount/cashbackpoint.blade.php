@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
 inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
   <div class="col-sm-8">
      <div class="big-content">
	  <?php 
	$totalcashbackpoint = \App\front\UserCashbackPoint::cashbakpoint(); 
    ?>
        <h2 class="big-title">{{ $totalcashbackpoint }}<span>CASHBACKPUNKTE </span></h2>
        <hr>
      </div>
      <ul class="green-list">
        <li> Erhaltene Cashbackpunkte: </li>
		 @if(count($creditcpoint))
			<?php $cpoints = '0'; ?>
		@foreach($creditcpoint as $creditcpoints)
	    <?php 
		$cpoints = $cpoints + (int)$creditcpoints->awarded_points;
		?>
		@endforeach
		@else
		<?php $cpoints=0; ?>
		@endif
        <li> {{$cpoints}}</li>
		
		
        <li> Abgelaufene Cashbackpunkte: </li>
        @if(count($expirecpoint))
		<?php $upoints = '0'; 
	          $apoints = '0';
	    ?>
		@foreach($expirecpoint as $expirecpoints)
	    <?php 
		$upoints = $upoints + (int)$expirecpoints->used_points;
		$apoints = $apoints + (int)$expirecpoints->awarded_points;
		
		?>
		@endforeach
		<?php $epoints = ($apoints-$upoints); ?>
		@else
		<?php $epoints = 0; ?>	
		@endif
		<li> {{$epoints}}</li>
		
        <li> Ausgegebene Cashbackpunkte: </li>
        @if(count($debitcpoint))
		<?php $dpoints = '0'; ?>
		@foreach($debitcpoint as $debitcpoints)
	    <?php 
		$dpoints = $dpoints + (int)$debitcpoints->used_points;
		?>
		@endforeach
		@else
		<?php $dpoints=0; ?>
		@endif
		
        <li> {{$dpoints}}</li>
		
		
        <li> Zwischensumme Cashbackpunkte: </li>
		
		@if(count($totalcpoint))
		<?php $upoints = '0'; 
	          $apoints = '0';
	    ?>
		@foreach($totalcpoint as $totalcpoints)
	    <?php 
		$upoints = $upoints + (int)$totalcpoints->used_points;
		$apoints = $apoints + (int)$totalcpoints->awarded_points;
		
		?>
		@endforeach
		<?php $tpoints = ($apoints-$upoints); ?>
		@else
		<?php $tpoints=0; ?>
		@endif
		<li> {{$tpoints}}</li>
		
      </ul>
      <div class="clearfix"></div>
      <hr>
      <div class="table-responsive">
        <!--<table class="table table-striped table-hover table-defaultt">
          <thead>
            <tr>
              <th width="20%" class="text-center">Zustellservice </th>
              <th width="25%" class="text-center">Lieferadresse </th>
              <th width="17%" class="text-center">Bestellzeit </th>
              <th width="15%" class="text-center">Preis </th>
              <th width="23%" class="text-center">Bewertung </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>-->
      </div>
    </div>

    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

@section('script')
@endsection