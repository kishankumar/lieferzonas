@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
 inner-header
@endsection

@section('body')

    @include('includes/frontuserheader')
    <div class="container">
        <div class="row">
    <div class="col-sm-8 col-md-8 col-lg-8">
        <div class="rating_title mt25"> Miene Adressen <span class="pull-right"><a href="address/create"><i class="fa fa-plus"></i></a></span> </div>
        <table class="table table-striped table-hover table-defaultt">
          <thead>
            <tr>
              <th width="20%" class="text-center">Zustellservice </th>
              <th width="25%" class="text-center">Lieferadresse </th>
              <th width="17%" class="text-center">Bestellzeit </th>
              <th width="15%" class="text-center">Preis </th>
              <th width="23%" class="text-center">Bewertung </th>
            </tr>
          </thead>
          <tbody>
		  <?php  
		  $current_time = strtotime(date('Y-m-d H:i:s')); 
		  ?>
		    @if(count($userorder))
            @foreach($userorder as $userorders)
		     <?php
			 $userorders->delivery_time;
			 $delivery_time = strtotime($userorders->delivery_time);
			 
			
			 if($delivery_time > $current_time)
			 {
				 $diff = ($delivery_time-$current_time)/60;
			 }
			 else
			 {
				  $diff =0;
			 }
			 $rest_id = $userorders->rest_detail_id; 
			 $totalrating = \App\front\UserOrderReview::totalrating($rest_id);
			  if($totalrating != '')
			  {
				 $count = $totalrating/10;
				 $rating = \App\front\UserOrderReview::rating($rest_id);
					$quality_rating = '0';
					$service_rating = '0';
					$starrating =0;
				 foreach($rating as $ratings)
				 {
					 $quality_rating  = (float)$ratings->quality_rating + (float)$quality_rating;
					 $service_rating  = (float)$ratings->service_rating+(float)$service_rating;
					 $total_rating = $quality_rating+$service_rating;
					 
				 }
				 $starrating = $total_rating/($count*2);
			  }
            ?> 
            <tr style="padding:10px 0;">
              <td>{{ date("d M Y h:i:s A", strtotime( $userorders->created_at))  }} 
                <div>
                    <a href="{{url('restaurant/'.$userorders->rest_detail_id.'/search')}}">
                        <img class="media-object" src="{{asset('public/uploads/superadmin/restaurants/'.$userorders->logo) }}" alt="profile" width="100">
                        <strong>{{ $userorders->f_name.' '.$userorders->l_name }}</strong> 
                    </a>
                </div>
                  
				<div>#{{ $userorders->order_id }}</div>
				<div> <a class="btn-xs" href="{{url('front/order/detail/'.$userorders->id)}}">Details</a></div>
                <div class="border-rgt"></div></td>
				
              <td class="vam">
			  <div> {{ $userorders->booking_person_name }} </div>
			 
			  {{ $userorders->address.' '.$userorders->landmark.' '.$userorders->city_name.' '.$userorders->state_name.' '.$userorders->country_name.' '.$userorders->zipcode }} 
                <div class="border-rgt"></div></td>
              <td class="vam">{{ (int)$diff }} Minuten
                <div class="border-rgt"></div></td>
              <td class="vam">€ {{ $userorders->total_amount }}
                <div class="border-rgt"></div></td>
			
              <td><span class="star2">
			  <?php
			  
			  if($totalrating != '0')
			  {
				  $review=floor($starrating);
				 
				 
				  for($i=1; $i<=$review; $i++)
				  {
					  
					  echo '<i class="fa fa-star"></i>';
				  }
				   
			   
				  if(ceil($starrating) > $review )
				  {
					 
					 echo '<i class="fa fa-star-half-o"></i>';
				   
				  }
				  
				  if(5 > ceil($starrating) )
				  {
				   
					for($i = ceil($starrating); $i<5; $i++)
					{
						
						echo '<i class="fa fa-star-o text-gray"></i>';
					}
					
				  }
			  
			?>
			  
			   </span><span class="review-s"> Ø {{ $total_rating  }} Sterne
                von total {{ $totalrating }} Bewertungen</span> <br>  
			  <?php } 
			  else { ?>
			  <span class="review-s">No Review Yet</span><br>
			  <?php } ?>
                <div class="tb_bt"></div>
				<a href="{{url('front/order/track/'.$userorders->order_id)}}" class="btn-border">Track Order</a>
				<?php if($userorders->front_order_status_id!=1 && $userorders->front_order_status_id!=3 && $userorders->front_order_status_id!=4) { ?>
				<a href="{{url('front/order/printbill/'.$userorders->order_id)}}" class="btn-border">Print Bill</a>
				<?php } ?>
				
				<?php if($userorders->front_order_status_id==1 ) { ?>
				<a href="#" class="btn-border" onclick="cancel_order('<?=$userorders->order_id?>');">Cancel Order</a>
				<?php } ?>
				<div class="tb_bt"></div>
                <a href="{{url('restaurant/'.$userorders->rest_detail_id.'/search')}}" class="btn-border2">WIEDER BESTELLEN</a></td>
            </tr>
			 @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
           
          </tbody>
        </table>
         {!! $userorder->render() !!}  
    </div>
     
    @include('includes/frontusersidebar')

    </div>
    </div>

@endsection

@section('script')
	
@endsection