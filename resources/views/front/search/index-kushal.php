@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')
    <section class="main-dash-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="row">
                        <div class="col-xs-7 col-sm-5 ">
                            <div class="media">
                            <div class="pull-left">
                                <a href="javascript:getLocation()" class="map-marker"><i class="fa fa-map-marker"></i></a>
                            </div>
                            <div class="media-body">
                                
                                {!! Form::open(['url'=>'search','class'=>'form-horizontal','method'=>'post','id'=>'zip_form']) !!}
                                <div class="media mt0">
                                <div class="pull-right">
                                {!! Form::button('Weiter', ['class'=>'btn btn-search btn-border4','onclick'=>'submit_zipcode()']) !!}
                                </div>
                                <div class="media-body">
                                {!! Form::text('zipcode',$zipcode,['class'=>'btn-border3 form-control', 'placeholder'=>'Postleitzahl', 'id'=>'zipcode']) !!}
                                </div>

                                </div>
                                {!! Form::close() !!}
                                <div style="display:none" class="alert alert-danger text-center" id="zip_error"></div>
                            </div>
                            </div>
                        </div>
                        <div class="col-xs-5 col-sm-3 media mt0">
                            <div class="pull-right">
                                <a href="#" class="map-marker grid_list"><i class="fa fa-th"></i></a>
                            </div>
                            <div class="media-body">
                                {{--<input class="btn-border4 btn text-center btn-block" type="submit" value="Map">--}}
                                {{--<input class="btn-border4 btn text-center btn-block" data-toggle="modal" data-target="#myModal-res-map" type="submit" value="Map">--}}
                                <a href="javascript:void(0);" id="addpath" class="btn-border4 btn text-center btn-block show2 myModal-map">Map</a>
                            </div>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <div class="sort-down dropdown">
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sort  <i class="fa fa-chevron-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-hand-o-right"></i> Our Recommendation </a></li>
                                    <li><a href="#"><i class="fa fa-hand-o-right"></i> Our Recommendation </a></li>
                                    <li><a href="#"><i class="flaticon-road"></i> distance </a></li>
                                    <li><a href="#"><i class="fa fa-eur"></i> Minimum value </a></li>
                                    <li><a href="#"><i class="fa fa-star"></i> rating </a></li>
                                    <li><a href="#"><i class="flaticon-game"></i> Alphabetical </a></li>
                                    <li><a href="#"><i class="fa fa-heart"></i> Favourites </a></li>
                                    <li><a href="#"><i class="fa fa-clipboard"></i> Stempelkarte </a></li>
                                    <li><a href="#"><i class="flaticon-tag"></i> Discounts </a></li>
                                    <li><a href="#"><i class="flaticon-tool"></i> Rabatte  </a></li>
                                </ul>--}}
                                <select class="selectpicker" id="sel_sort" name="sel_sort" onchange="kitchen_search()">
                                    <option <?php if($sort=='recommend') echo 'selected'; ?> value="recommend">Our Recommendation</option>
                                    <option <?php if($sort=='distance') echo 'selected'; ?> value="distance">Distance</option>
                                    <option <?php if($sort=='minimum') echo 'selected'; ?> value="minimum">Minimum Value</option>
                                    <option <?php if($sort=='rating') echo 'selected'; ?> value="rating">Rating</option>
                                    <option <?php if($sort=='alphabetical') echo 'selected'; ?> value="alphabetical">Alphabetical</option>
                                    <option <?php if($sort=='favourite') echo 'selected'; ?> value="favourite">Favourites</option>
                                    <option <?php if($sort=='stampcard') echo 'selected'; ?> value="stampcard">Stempelkarte</option>
                                    <option <?php if($sort=='discount') echo 'selected'; ?> value="discount">Discounts</option>
                                    <option <?php if($sort=='rabatte') echo 'selected'; ?> value="rabatte">Rabatte</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php //$rest_kitchen = App\superadmin\RestZipcodeDeliveryMap::rest_kitchen(2); echo "<pre>"; print_r($rest_kitchen);exit; ?>

    <div class="container">
    <div class="row">
    <?php   $all_kitchen = array(); 
            $map = array();
                    $map_lat = "";
                    $map_long = "";
                    $map_name = "";
                    $map_image = "";
                    $map_address = "";
                    $map_time = "";
                    $map_kitchen = "";
    ?>
        <div class="col-xs-12 col-md-8 filtr-lst">
			<div>
            @if(count($rest) > 0)
            @foreach($rest as $val)
                <?php

                    $show_rest = 0;
                        $rest_kitchen = App\superadmin\RestZipcodeDeliveryMap::rest_kitchen($val->rest_id);


                        //echo print_r($rest_kitchen);exit;
                    if(count($kitchen_filter) > 0)
                    {
                        if($kitchen_filter[0] != "")
                        {
                            $filter = $rest_kitchen->toArray();
                            foreach($filter as $v)
                                if(in_array($v['kitchen_id'],$kitchen_filter))
                                    $show_rest = 1;
                        }
                        else
                            $show_rest = 1;
                    }
                    else
                        $show_rest = 1;
                    //echo "kdfijfjifg";exit;
                    //dd($val);
                ?>
                @if($val->status == 1)
                    <?php $rest_set = App\superadmin\RestZipcodeDeliveryMap::rest_setting($val->rest_id);
                ?>
                @if($show_rest == 1)

                    <?php
                        
                         $map_lat = $val->latitude;
                         $map_long = $val->longitude;
                    ?>

			
			<div class="grid">
            <section>
                <div class="search_top">
                    <div class="row">
                        <div class="col-xs-6 full543"><h4><strong><?php $map_name = $val->f_name; echo $map_name; ?> </strong></h4></div>
                        <div class="col-xs-6 full543">
                            <div class="search_top_r media">
                                {{--<img src="img/top.png" class="pull-left" alt="">--}}
                                @if($rest_set[0]['privilege_id'] == 1)
                                {!! Html::image('resources/assets/front/img/top.png','',['class'=>'pull-left']) !!}
    `                           @endif
                                @if($rest_set[0]['is_new'] == 1)
                                    <div class="pull-right new">NEW</div>
                                @endif
                                <div class="media-body">
                                  <span>
                                      <i class="yellow">
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star-half-o"></i>
                                      </i>
                                      (1382)
                                  </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="search_min">
                    <div class="row">
                        <div class="col-xs-12 col-sm-9 grid_full">
                            <div class="media">
                                <figure class="pull-left">{{--<img class="img-responsive" alt="" src="img/logo_de.png" width="220">--}} {!! Html::image('public/uploads/superadmin/restaurants/'.$val->logo,'',['class'=>'img-responsive','width'=>'150']) !!} <?php $map_image = 'public/uploads/superadmin/restaurants/'.$val->logo; ?> </figure>
                                <aside class="media-body">
                                <div class="mleft">
                                    {{--Mediterran, Frische Salate--}}
                                    <?php $count = 0; $map_kitchen="";?>
                                    @foreach($rest_kitchen as $kit)
                                        @if($count != 0)
                                            <?php $map_kitchen .= ', '; ?>
                                        @endif
                                        <?php $map_kitchen .= $kit->getKitchen->name; ?>
                                        <?php
                                            if(in_array($kit->kitchen_id,$all_kitchen) == false)
                                            {
                                                $all_kitchen[$count]['id'] = $kit->kitchen_id;
                                                $all_kitchen[$count]['name'] = $kit->getKitchen->name;
                                            }
                                            $count++;
                                            ?>
                                    @endforeach
                                    {{ $map_kitchen }}
                                    <div class="green mt25"><i class="fa fa-clock-o"></i>
                                        <?php $rest_time = App\superadmin\RestZipcodeDeliveryMap::rest_time($val->rest_id,date('N')); ?>
                                        @if(!empty($rest_time))
                                            @if($rest_time[0]['is_open'] == 1)
                                                <?php $map_time = $rest_time[0]['open_time']." bis  ".$rest_time[0]['close_time']; echo $map_time; ?>
                                            @else
                                                <?php $map_time = 'Closed'; echo $map_time; ?>
                                            @endif
                                        @endif
                                    </div>
                                    <?php $map_address = $val->add1." ".$val->add2." ".$val->cname." ".$val->sname." ".$val->cityname.", ".$val->zname; echo $map_address; ?>
                                    <br>
                                </div>
                                    {{--<img alt="" src="img/card.png" class="img-responsive mt25">--}}
                                    {!! Html::image('resources/assets/front/img/card.png','',['class'=>'img-responsive mt25']) !!}
                                </aside>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 grid_full">
                            <div class="search_r">
                                <div class="live">
                                    <i class="fa fa-clock-o fa-2x"></i> <strong>Live</strong><i class="fa fa-comment"></i>
                                </div>
                                <?php
                                    $rest_fav = App\superadmin\RestZipcodeDeliveryMap::rest_fav($val->rest_id);
                                    if(Auth::check('front')){
                                        $rest_user_fav = App\superadmin\RestZipcodeDeliveryMap::rest_user_fav($val->rest_id,Auth::user('front')->id);
                                        $isfav = 0;
                                        if(count($rest_user_fav) > 0)
                                            $isfav = 1;
                                ?>
                                <a href="javascript:void(0);" style="text-decoration: none;"> <i id="<?php echo $val->rest_id ; ?>" onClick="fav_rest(this.id);" title="<?php if($isfav == 1) echo 'Remove from Favourite'; else echo 'Add to Favourite'; ?>" class="fa fa-heart fa-2x red <?php if($isfav == 1) echo 'active'; ?>"> <span id="fav_count{{ $val->rest_id }}">{{ count($rest_fav) }}</span> </i> </a>
                                <?php } else { ?>
                                    <i id="" class="fa fa-heart fa-2x red">{{ count($rest_fav) }}</i>
                                <?php } ?>
                                <br>
                                <aside class="s_detail">
                                   <?php
                                if(Auth::check('front'))
                                {
                                    $auth_id = Auth::user('front')->id;
                                ?>
                                    <a href="javascript:void(0)" style="text-decoration: none;" class="btn btn-green2 mt10" onclick="recent_visit(<?=$val->rest_id?>,<?=$auth_id?>);"><strong>The Menu</strong></a>
                                <?php } else
                                { ?>
                                
                                    <a href="{{ url('restaurant/'.$val->rest_id.'/search') }}" class="btn btn-green2 mt10" ><strong>The Menu</strong></a>
                                <?php }
                                ?>
                                    <a href="#" class="btn btn-red2 mt10"><strong>4 X Action</strong></a>
                                    <ul>
                                        <li>Neukunden Rabatt</li>
                                        <li>Rabatt %</li>
                                        <li>Stempel karte</li>
                                    </ul>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                    {!! Html::image('resources/assets/front/img/car.png') !!} Free delivery    &nbsp; &nbsp;
                    {!! Html::image('resources/assets/front/img/bag.png') !!} Minimum order value 10,00 € including delivery fee 2:00
                </p>
                <div class="clearfix"></div>
            </section>
			</div>
			
                        <?php array_push($map,array($map_lat,$map_long,$map_name,$map_image,$map_address,$map_time,$map_kitchen)); ?>
                        @endif
                    @endif


                        
            @endforeach
            @else
                <div class="alert alert-danger text-center"></div>
            @endif

        </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="content-box">
                <div class="tabbing-side menu3">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#Filter" data-toggle="tab">Filter </a></li>
                        <li><a href="#Help" data-toggle="tab">Help</a></li>
                        <li><a href="#Follow" data-toggle="tab">Follow Us</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Filter">
                            <div class="action_menu">
                                <h4>Küchenrichtungen
                                    <span class="mt10"><a href="#"><i class="fa fa-undo" style="color:#888"></i></a></span>
                                    <span class="Reset mt10"><a href="#">Reset</a></span>
                                </h4>
                            </div>
                            <div class="checkbox3_list">
                                @foreach($all_kitchen as $kit)
                                    <label class="checkbox"> <input type="checkbox" name="check_kitchen" <?php if(in_array($kit['id'],$kitchen_filter)) echo'checked'; ?> onchange="kitchen_search()" value="{{ $kit['id'] }}" > <span> {{ $kit['name'] }} </span></label>
                                @endforeach
                            </div>
                            <div class="clearfix"></div>


                            <div class="action_menu mt10">
                                <h4>Mindestbestellwert
                                    <span class="mt10"><a href="#"><i class="fa fa-undo" style="color:#888"></i></a></span>
                                    <span class="Reset mt10"><a href="#">Reset</a></span>
                                </h4>
                            </div>
                            <section class="mt10">
                                <input id="ex1" type="text" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[0,1000]"/>
                                <div class="row">
                                    <div class="col-md-3">5.90 €</div>
                                    <div class="col-md-6 text-center">Alles auswählen</div>
                                    <div class="col-md-3 text-right">100.00 €</div>
                                </div>
                            </section>
                            <section class="mt10">
                                <input id="ex2" type="text" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[0,1000]"/>
                                <div class="row">
                                    <div class="col-md-3">5.90 €</div>
                                    <div class="col-md-6 text-center">Alles auswählen</div>
                                    <div class="col-md-3 text-right">100.00 €</div>
                                </div>
                            </section>
                            <section class="mt10">
                                <input id="ex3" type="text" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[0,1000]"/>
                                <div class="row">
                                    <div class="col-md-3">5.90 €</div>
                                    <div class="col-md-6 text-center">Alles auswählen</div>
                                    <div class="col-md-3 text-right">100.00 €</div>
                                </div>
                            </section>
                            <section class="mt10">
                                <input id="ex4" type="text" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[0,1000]"/>
                                <div class="row">
                                    <div class="col-md-3">5.90 €</div>
                                    <div class="col-md-6 text-center">Alles auswählen</div>
                                    <div class="col-md-3 text-right">100.00 €</div>
                                </div>
                            </section>

                            <div class="action_menu mt25">
                                <h4>Filter
                                    <span class="mt10"><a href="#"><i class="fa fa-undo" style="color:#888"></i></a></span>
                                    <span class="Reset mt10"><a href="#">Reset</a></span>
                                </h4>
                            </div>
                            <div class="checkbox2_list">
                                {{--<label class="checkbox"> <input type="checkbox" name="2"><span>Alle Restaurants </span></label>--}}

                                <label class="checkbox"> <input type="checkbox" name="check_restype" <?php if(in_array('open',$restype_filter)) echo'checked'; ?> onchange="kitchen_search()" value="open" ><span>Geöffnet </span></label>
                                {{--<label class="checkbox"> <input type="checkbox" name="2"><span>Abhollung </span></label>--}}
                                <label class="checkbox"> <input type="checkbox" name="check_restype" <?php if(in_array('new',$restype_filter)) echo'checked'; ?> onchange="kitchen_search()" value="new" ><span>Neue Restaurant  </span></label>

                                {{--<label class="checkbox"> <input type="checkbox" name="2"><span>Gourmet - Restaurant </span></label>--}}
                            </div>
                            <div class="clearfix"></div>
                            <div class="action_menu mt25">
                                <h4>% Aktionen
                                    <span class="mt10"><a href="#"><i class="fa fa-undo" style="color:#888"></i></a></span>
                                    <span class="Reset mt10"><a href="#">Reset</a></span>
                                </h4>
                            </div>
                            <div class="checkbox2_list">
                                <label class="checkbox"> <input type="checkbox" name="2"><span>Neukunden-Rabatt </span></label>
                                <label class="checkbox"> <input type="checkbox" name="2"><span>Rabatt </span></label>
                                <label class="checkbox"> <input type="checkbox" name="2"><span>Mittagsmenü-Rabatt </span></label>
                                <label class="checkbox"> <input type="checkbox" name="2"><span>Stempelkarte </span></label>
                            </div>

                            <div class="clearfix"></div>
                            <div class="action_menu mt25">
                                <h4> Suche
                                    <span class="mt10"><a href="#"><i class="fa fa-undo" style="color:#888"></i></a></span>
                                    <span class="Reset mt10"><a href="#">Reset</a></span>
                                </h4>
                            </div>
                            <h4 class="text-center">Restaurant, Küche oder Speise suchen</h4>
                            <div class="input-group green-search">
                                <input type="text" class="form-control input-lg">
                                <span class="input-group-addon"><button class="btn"> &nbsp; <i class="fa fa-search"></i> &nbsp; </button></span>
                            </div>
                        </div>
                        <div class="tab-pane" id="Help">
                            <div class="action_menu mt25"><h4> You need help?</h4></div>

                            <?php $i=1; foreach($faq as $value){ ?>
                            <div class="row_border">
                                    <h4 onclick="accordion({{ $i }})" class="openDown arrow1">{{ $value->question }}</h4>
                                    <p class="accordion-text1" style="display:none;">{{ $value->answer }}</p>
                            </div>
                            <?php $i++; } ?>
                        </div>
                        <div class="tab-pane" id="Follow">
                            <!-- <div class="action_menu">
                                <h4>Quitzdorf Fruits & Vegetables - L <span>200<br>
                    <i class="fa fa-heart"></i></span> </h4>
                                <div class="media">
                                    <figure class="pull-left"> {{--<img src="img/logo_de.png" alt="" class="img-responsive">--}} {!! Html::image('resources/assets/front/img/logo_de.png','',['class'=>'img-responsive']) !!} </figure>
                                    <aside class="media-body"> <span class="green"><i class="fa fa-clock-o"></i> 08:00 bis 13:30 </span><br>
                                        Lieferkosten: ab 0,00 € <br>
                                        Lieferung ab: 9,00 € <br>
                                        <small>siehe info: </small> </aside>
                                    <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <small>(1382)</small></p>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    {!! Form::open(['url'=>'search','method'=>'post','id'=>'filter_form']) !!}
        {!! Form::hidden('kitchen_filter','',['id'=>'kitchen_filter']) !!}
        {!! Form::hidden('restype_filter','',['id'=>'restype_filter']) !!}
        {!! Form::hidden('sort','',['id'=>'sort']) !!}
    {!! Form::close() !!}

    {{--{!! Form::open(['url'=>'search','method'=>'post','id'=>'locateme_form']) !!}
        {!! Form::hidden('zipcode','',['id'=>'zipcode']) !!}
    {!! Form::close() !!}--}}

    

    <div id="myModal-map">
    <div class="modal-dialog modal-lg robotoregular">
        <div class="modal-content">
            <div style="background:#81c02f; color:#fff; padding: 5px 15px;" class="modal-header">
                <button style="color:#81c02f; opacity:1; font-size: 34px;" class="modalclose close" type="button">
                    <span>×</span></button>
                <h4 class="modal-title">Map</h4>
            </div>
            <div class="modal-body">
                <div id="map-canvas"></div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('script')

  
		
    <script type="text/javascript">
        function submit_zipcode(i)
        {
            if (typeof(i)==='undefined') i = 0;
            $('#zip_error').hide();
            var zip = $('#zipcode').val();
            //alert(zip);
            $.ajax({
                url: '{{url('checkzip')}}',
                type: "post",
                data: {zipcode:zip, "_token":"{{ csrf_token() }}"},
                success: function(data){
                    if(data != "")
                        $('#zip_form').submit();
                    else
                    {
                        if(i == 1)
                            $('#zip_error').html('No Restaurant found for your current location.!!');
                        else
                            $('#zip_error').html('No Restaurant Found.!!');

                        $('#zip_error').show();
                    }
                }
            });
        }

        function kitchen_search()
        {
            var checkedKitchen = new Array();
            $('input:checkbox[name="check_kitchen"]:checked').each(function(){
                checkedKitchen.push($(this).val());
            });
            $('#kitchen_filter').val(checkedKitchen);

            var checkedRestype = new Array();
            $('input:checkbox[name="check_restype"]:checked').each(function(){
                checkedRestype.push($(this).val());
            });
            $('#restype_filter').val(checkedRestype);

            var selectedSort = $("#sel_sort").val();
            $("#sort").val(selectedSort);

            //alert(selectedSort);
            //alert(checkedKitchen);
            //alert(checkedRestype);

            $('#filter_form').submit();
        }

        function fav_rest(id)
        {
            //alert('hello');
            var uid =  '<?php if(Auth::check('front')) echo Auth::user('front')->id ?>';

            var check = $('#'+id).hasClass('active');
            if(check == false)
            {
                $.ajax({
                    url: '{{url('userfav')}}',
                    type: "post",
                    data: {id:id, uid:uid, fav:'1', "_token":"{{ csrf_token() }}"},
                    success: function(data){
                        $('#'+id).addClass('active');
                        var count = $('#fav_count'+id).text();
                        $('#fav_count'+id).text(parseInt(count)+1);
                        $('#'+id).attr('title', 'Remove from Favourite');
                    }
                });
            }
            else
            {
                $.ajax({
                    url: '{{url('userfav')}}',
                    type: "post",
                    data: {id:id, uid:uid, "_token":"{{ csrf_token() }}"},
                    success: function(data){
                        $('#'+id).removeClass('active');
                        var count = $('#fav_count'+id).text();
                        $('#fav_count'+id).text(count-1);
                        $('#'+id).attr('title', 'Add to Favourite');
                    }
                });
            }
        }

        function getLocation()
        {
            if(navigator.geolocation)
            {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
            else
            {
                alert('Error in fetching location. Please try again after some time.');
            }
        }

        function showPosition(position)
        {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;

            $.ajax({
                type    : "POST",
                url     : 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&sensor=true',
                //url       : 'http://maps.googleapis.com/maps/api/geocode/json?latlng=48.2079336,16.3305928&sensor=true',
                accepts : 'application/json',
                dataType:"json",
                data    : { },
                success: function (data) {
                    var len = data.results[0]['address_components'].length;
                    var zipcode = data.results[0]['address_components'][len-1]['long_name'];

                    $('#zipcode').val(zipcode);
                    submit_zipcode(1);
                }
            });
        }
    </script>

    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script type="text/javascript">
        /*$(document).ready(function(){
         $('#addpath').click(function(){
         $('#pop2').show();
         //$(".modbox").addClass('animated zoomIn').removeClass('zoomOut');
         $('._mapview .simplePopupClose').hide();
         var myButton = document.getElementById('addpath');
         google.maps.event.addDomListener(myButton,'click', setTimeout(initialize1,1000));
         });
         });*/

        google.maps.event.addDomListener(window, 'load', initialize1);

        var map;
        var _lastInfoWindow;
        var LocationData1 = <?php echo json_encode($map);  ?>

        function initialize1()
        {
            var map = new google.maps.Map(document.getElementById('map-canvas'));
            var bounds = new google.maps.LatLngBounds();
            for (var i in LocationData1)
            {
                var p = LocationData1[i];
                addInfoWindow(map,LocationData1[i],bounds);
            }
            map.fitBounds(bounds);
        }

        function addInfoWindow(map,location,bounds)
        {
            var p = location;

            var content = '<div class="map-details">' +
                   
                    '<h4 class="green">'+p[2]+'</h4>' +
                    '<div class="media"><figure class="pull-left">' +
                    '<img width="120" src="'+p[3]+'" alt="" class="img-responsive"> </figure><aside class="media-body">'+p[6]+'<div class="green"><i class="fa fa-clock-o"></i>'+p[5]+'</div>'+p[4]+'</aside></div></div>';

            var infowindow = new google.maps.InfoWindow({
                content: content,
                maxWidth: 500,
            });
            var lat=  p[0];
            lat=lat.toString();
            var lang=p[1];
            lang=lang.toString() ;
            var latlng = new google.maps.LatLng(lat,lang);
            bounds.extend(latlng);

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: p[2]
            });
            google.maps.event.addListener(marker, 'click', function() {
                if(_lastInfoWindow){
                    _lastInfoWindow.close();
                }
                infowindow.open(map, this);
                _lastInfoWindow = infowindow;
            });
        }
        function recent_visit(rest_id,user_id)
        {
            var base_host = window.location.href;
            base_host = base_host.substring(0, base_host.lastIndexOf("/"));
           
            var $url = base_host+'/restaurant/'+rest_id+'/search'
             
            
               $.ajax({
                url: '{{ url('recent_visit') }}',
                type: "GET",
                dataType:"json",
                data: {'rest_id':rest_id,'user_id':user_id},
                error: function (request, error) {
                    window.location.href = $url;
                },
                success: function(res)
                {
                    if(res.success==1)
                     {
                        window.location.href = $url;
                     }
                     else
                     {
                        window.location.href = $url;
                     }
                     
                }
                
            });
            
            
            
        }
    </script>

  <script type="text/javascript">
	
		$('#myModal-map').css({"height":"1px","overflow":"hidden"});
	
		$('.myModal-map').click(function(){
			$('#myModal-map').addClass('modal fade in').css({"height":"auto","overflow":"visible","display":"block"});
			$('body').addClass('modal-open');
			$('body').append('<div class="modal-backdrop fade in"></div>');
		});
		$('.modalclose').click(function(){
			$('#myModal-map').removeClass('modal fade in').css({"height":"1px","overflow":"hidden","display":""});
			$('body').removeClass('modal-open');
			$('.modal-backdrop').remove();
		});
	</script>	

<script>
    $(document).ready(function() {
        $("#ex1").slider();        
        $("#ex2").slider();        
        $("#ex3").slider();        
        $("#ex4").slider();        
    });
</script>
<script>
	$(document).ready(function(){
		$('.grid_list').click(function(){
			$(this).children('i').toggleClass('fa-th').toggleClass('fa-list');
			$('.grid').toggleClass('col-md-6 gridcss');
			$('.grid').parent().toggleClass('row');
		});
	});
</script>

@endsection