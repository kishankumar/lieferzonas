@extends('layouts.frontbody')

@section('title')
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

@include('includes/frontuserheader')

<div class="container">
@foreach($uorderdetail as $uorderdetails)
  <div class="row">
	<?php
	$order_id = $uorderdetails['order_id']; 
	$uorderitem = \App\front\OrderItem::userorderitem($order_id); ?>
	<div class="col-sm-4  col-md-4 col-lg-4">
      <div class="well add_box mt25">
		  <div class="form-group green"><strong>{{ $uorderdetails['booking_person_name'] }}</strong></div>

		  <div class="form-group">
			  {{ $uorderdetails['address'].' '.$uorderdetails['landmark'].' '.$uorderdetails['zipcode'].' '.$uorderdetails['city_name'].' '.$uorderdetails['state_name'].' '.$uorderdetails['country_name']  }}
		  </div>
		  <div class="form-group">
			  Phone: {{ $uorderdetails['mobile'] }}
		  </div>
		  
	  </div>
    </div>  
    <div class="col-sm-8 mt25">
	    @foreach($uorderitem as $uorderitems)
		<div class="item_list">
		  <div class="pull-right"><strong>{{ $uorderitems->item_price.' €'  }}</strong> </div>{{ $uorderitems->item_name }}
		</div>
	     @endforeach
		<div class="clearfix"></div>
		<h4><strong>Total <span class="pull-right">{{ $uorderdetails['total_amount'] }} €</span></strong></h4>
    </div>
  </div>
  @endforeach
</div>
@endsection

@section('script')
@endsection