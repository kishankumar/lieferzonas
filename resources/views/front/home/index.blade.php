@extends('layouts.frontbody')

@section('title')
    Lieferzonas 
@endsection

@section('homeheader')
    <div class="head-bar">
        <div class="container" id="header-container" style="display:none">
            <div class="row">
                <div class="col-sm-12">
                    <h3>So easy Works Order online food!</h3>
                </div>
                <div class="col-xs-3 t_box">
                    {{--<img src="img/top_map.png" alt="">--}}
                    {!! Html::image('resources/assets/front/img/top_map.png') !!}
                    <br>
                    <p><strong>Simply enter your zip code or city and off you lot....</strong> </p>
                    <span>1</span>
                </div>
                <div class="col-xs-3 t_box">
                    {{--<img src="img/top_res.png" alt="">--}}
                    {!! Html::image('resources/assets/front/img/top_res.png') !!}
                    <br>
                    <p><strong>Restaurant search</strong></p>
                    <span>2</span>
                </div>
                <div class="col-xs-3 t_box">
                    {{--<img src="img/top_wallet.png" alt="">--}}
                    {!! Html::image('resources/assets/front/img/top_wallet.png') !!}
                    <br>
                    <p><strong>Choose payment method </strong></p>
                    <span>3</span>
                </div>
                <div class="col-xs-3 t_box">
                    {{--<img src="img/top_delivery.png" alt="">--}}
                    {!! Html::image('resources/assets/front/img/top_delivery.png') !!}
                    <br>
                    <p><strong>The food is now under way </strong></p>
                    <span>4</span>
                </div>
            </div>
        </div>
        <a class="btn-grn-top" href="#">wie funktioniert lieferzonas</a>
    </div>
@endsection

@section('headerclass')
    full-head
@endsection

@section('homeheadersearch')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="wow animated lightSpeedIn" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: lightSpeedIn;">
                    <label class="switch-btn no-radius">
                        <input class="checked-switch" type="checkbox" id="lie-pickup-delivery-switch" />
                        <span class="text-switch" data-yes="Abholung" data-no="Zustellung"></span>
                        <span class="toggle-btn"></span>
                    </label>
                    <a class="map-marker" href="javascript:getLocation()"><i class="fa fa-map-marker"></i></a>
                    <div class="alert alert-danger" style="display:none" id="lie-geolocation-error-alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <strong>Fehler:</strong> Konnte Position nicht ermitteln!
                    </div>
                </div>
                <!-- <img src="img/Switch-On.png" alt="">
               <input type="checkbox" checked data-toggle="toggle" data-on=" OFF" data-off=" Zustellung">-->
            </div>
            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div class="bannr-form wow animated bounceInDown" data-wow-delay="0.2s">
                    <div class="form-bg">
                        <h2>Jetzt Lieferservices in Deiner Umgebung finden:</h2>
                        {{--<form class="form-horizontal">--}}
                        {!! Form::open(['url'=>'search','class'=>'form-horizontal','method'=>'post','id'=>'zip_form']) !!}
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-form-add" id="adv-search">
                                        {!! Form::text('zipcode','',['class'=>'form-control', 'placeholder'=>'Postleitzahl', 'id'=>'zipcode']) !!}
                                        {!! Form::button('Weiter', ['class'=>'form-control btn btn-search btn-primary','onclick'=>'submit_zipcode()']) !!}
                                    </div>
                                    <div style="display:none; margin-top: 60px; z-index: 999;" class="alert alert-danger text-center" id="zip_error">No Restaurant Found.!!</div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                        {{--</form>--}}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="bnr-fom-fot">
                                    <h3>Bezahle per Paypal, Lastschrift, Kreditkarte oder einfach in Bar.</h3>
                                    <ul class="paypal-icon-bnr wow animated bounceInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: bounceInUp;">
                                        <li><a href="#" class="paypal-btn">Barzahlung</a></li>
                                        <li>{!! Html::image('resources/assets/front/img/paypal-icon.png','paypal',['width'=>'55']) !!}</li>
                                        <li>{!! Html::image('resources/assets/front/img/sofort-icon.png','sofort',['width'=>'55']) !!}</li>
                                        <li>{!! Html::image('resources/assets/front/img/eps-icon.png','eps',['width'=>'55']) !!}</li>
                                        <li>{!! Html::image('resources/assets/front/img/visa-icon.png','visa',['width'=>'55']) !!}</li>
                                        <li>{!! Html::image('resources/assets/front/img/master-icon.png','master',['width'=>'55']) !!}</li>
                                        <li>{!! Html::image('resources/assets/front/img/american-icon.png','american',['width'=>'55']) !!}</li>
                                        <li>{!! Html::image('resources/assets/front/img/skrill-icon.png','skrill',['width'=>'55']) !!}</li>
                                        <li>{!! Html::image('resources/assets/front/img/bitcoin-icon.png','bitcoin',['width'=>'55']) !!}</li>
                                        {{--<li><img src="img/paypal-icon.png" alt="paypal" width="55"></li>
                                        <li><img src="img/sofort-icon.png" alt="paypal" width="55"></li>
                                        <li><img src="img/eps-icon.png" alt="paypal" width="55"></li>
                                        <li><img src="img/visa-icon.png" alt="paypal" width="55"></li>
                                        <li><img src="img/master-icon.png" alt="paypal" width="55"></li>
                                        <li><img src="img/american-icon.png" alt="paypal" width="55"></li>
                                        <li><img src="img/skrill-icon.png" alt="paypal" width="55"></li>
                                        <li><img src="img/bitcoin-icon.png" alt="paypal" width="55"></li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('body')

 <div class="container">
        <div class="row">
            <div class="col-sm-6 wow animated lightSpeedIn" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: lightSpeedIn;">  {!! Html::image('resources/assets/front/img/app.png','',['class'=>'app-img']) !!} </div>
            <div class="col-sm-6">
                <section class="apptext text-center">
                    <figure class=" wow animated fadeInRight" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInRight;">
                        <a href="#"> {!! Html::image('resources/assets/front/img/as.png') !!} </a>
                        <a href="#"> {!! Html::image('resources/assets/front/img/gp.png') !!} </a>
                        <a href="#"> {!! Html::image('resources/assets/front/img/wp.png') !!} </a>
                        {{--<a href="#"><img src="img/as.png" alt=""></a>
                        <a href="#"><img src="img/gp.png" alt=""></a>
                        <a href="#"><img src="img/wp.png" alt=""></a>--}}
                    </figure>
                    <h2><strong>Lieferzonas gibt's demnächst auch</strong> für dein Smartphone </h2>
                </section>
            </div>
        </div>
    </div>
    <div class="so_bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="title">So funktioniert's...</h2>
                    <div class="video3box first wow animated fadeInLeft" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInLeft;">
                        <h4>Wähle ein Restaurant aus</h4>
                        <i>1</i>
                        <div class="clearfix"></div>
                        {!! Html::image('resources/assets/front/img/icon1.png') !!} </div>
                    <div class="video3box second wow animated zoomIn" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: zoomIn;">
                        <h4>Wähle ein Restaurant aus</h4>
                        <i>1</i>
                        <div class="clearfix"></div>
                        {!! Html::image('resources/assets/front/img/icon1.png') !!} </div>
                    <div class="video3box third wow animated fadeInRight" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInRight;">
                        <h4>Wähle ein Restaurant aus</h4>
                        <i>1</i>
                        <div class="clearfix"></div>
                        {!! Html::image('resources/assets/front/img/icon1.png') !!} </div>
                    <div class="clearfix"></div>
                    <a href="#" class="btn-video wow animated bounceInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceInUp;"> <i class="fa fa-play-circle"></i> Video Ansehen</a>
                </div>
            </div>
        </div>
    </div>
    <div class="video-section wow animated fadeIn" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: fadeIn;"> {!! Html::image('resources/assets/front/img/video-section.png','',['class'=>'img-responsive']) !!}  </div>
    <div class="deine_box_main">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title2 text-center">DEINE VORTEILE BEI Lieferzonas.at</h2>
                    <br>
                    <div class="row">
                        <div class="col-sm-6 col-md-5 col-md-offset-1">
                            <ul class="deine_box list-inline">
                                <li class="wow animated fadeInLeft" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                    <div class="media one">
                                        <div class="pull-left">
                                            <small>1</small>
                                            <span>VORTEIL</span></div>
                                        <div class="media-body">
                                            <h3>Mehr als 600 Restaurants</h3>
                                        </div>
                                    </div>
                                </li>

                                <li class="wow animated fadeInLeft" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                    <div class="media two">
                                        <div class="pull-left">
                                            <small>2</small>
                                            <span>VORTEIL</span></div>
                                        <div class="media-body">
                                            <h3>Mehr als 600 Restaurants</h3>
                                        </div>
                                    </div>
                                </li>

                                <li class="wow animated fadeInLeft" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInLeft;">
                                    <div class="media three">
                                        <div class="pull-left">
                                            <small>3</small>
                                            <span>VORTEIL</span></div>
                                        <div class="media-body">
                                            <h3>Mehr als 600 Restaurants</h3>
                                        </div>
                                    </div>
                                </li>

                                <li class="wow animated fadeInLeft" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInLeft;">
                                    <div class="media four">
                                        <div class="pull-left">
                                            <small>4</small>
                                            <span>VORTEIL</span></div>
                                        <div class="media-body">
                                            <h3>Mehr als 600 Restaurants</h3>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-5">
                            <ul class="deine_box list-inline">
                                <li class="wow animated fadeInRight" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                                    <div class="media five right">
                                        <div class="pull-right"> <span>VORTEIL</span>
                                            <small>5</small>
                                        </div>
                                        <div class="media-body">
                                            <h3>Mehr als 600 Restaurants</h3>
                                        </div>
                                    </div>
                                </li>
                                <li class="wow animated fadeInRight" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInRight;">
                                    <div class="media six right">
                                        <div class="pull-right">
                                            <small>6</small>
                                            <span>VORTEIL</span></div>
                                        <div class="media-body">
                                            <h3>Mehr als 600 Restaurants</h3>
                                        </div>
                                    </div>
                                </li>
                                <li class="wow animated fadeInRight" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInRight;">
                                    <div class="media seven right">
                                        <div class="pull-right">
                                            <small>7</small>
                                            <span>VORTEIL</span></div>
                                        <div class="media-body">
                                            <h3>Mehr als 600 Restaurants</h3>
                                        </div>
                                    </div>
                                </li>
                                <li class="wow animated fadeInRight" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInRight;">
                                    <div class="media eight right">
                                        <div class="pull-right">
                                            <small>8</small>
                                            <span>VORTEIL</span></div>
                                        <div class="media-body">
                                            <h3>Mehr als 600 Restaurants</h3>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


        
    <div class="links_box_bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="links_box">
                            <h3 class="text-center wow animated fadeInLeft" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInLeft;">
                                <strong>Zustellservice in your State:</strong>
                            </h3>
                            <br>
                            <div class="row">
                                <div class="col-sm-5">
                                    <ul class="list-unstyled wow animated fadeInRight" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                                        {{--{{ dd($state) }}--}}
                                        @for($i=0;$i<5;$i++)
                                            <li> <a href="{{url('front/home/slug/')}}?slug=<?=$state[$i]->slug;?>"> {{ $state[$i]->state->name }} </a> </li>
                                        @endfor
                                    </ul>
                                </div>
                                <div class="col-sm-5 col-sm-offset-2">
                                    <ul class="list-unstyled wow animated fadeInLeft" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                        @for($i=5;$i<10;$i++)
                                            <li> <a href="{{url('front/home/slug/')}}?slug=<?=$state[$i]->slug;?>"> {{ $state[$i]->state->name }} </a> </li>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="links_box last">
                            <h3 class="text-center"><strong>Most popular cities:</strong></h3>
                            <br>
                            <div class="row">
                                <div class="col-sm-5">
                                    <ul class="list-unstyled wow animated fadeInRight" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                                        @for($i=0;$i<5;$i++)
                                            <li> <a href="{{url('front/home/slug/')}}?slug=<?=$city[$i]->slug;?>"> {{ $city[$i]->city->name }} </a> </li>
                                        @endfor
                                    </ul>
                                </div>
                                
                                <div class="col-sm-5 col-sm-offset-2">
                                    <ul class="list-unstyled wow animated fadeInLeft" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                        @for($i=5;$i<10;$i++)
                                            <li> <a href="{{url('front/home/slug/')}}?slug=<?=$city[$i]->slug;?>"> {{ $city[$i]->city->name }} </a> </li>
                                        @endfor
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-3">
                        <div class="links_box pd">
                            <h3 class="text-center"><strong>Popular Nationalities: </strong></h3>
                            <br>
                            <ul class="list-unstyled wow animated fadeInRight" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInRight;">
                                @foreach($country as $coun)
                                    <li><a href="{{url('front/homepage/')}}?slug=<?=$coun->slug;?>"> {{ $coun->country->name }} </a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="links_box pd">
                            <h3 class="text-center"><strong>Popular Dishes:</strong></h3>
                            <br>
                            <ul class="list-unstyled wow animated fadeInLeft" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                    <li><a href="#">Pizza </a></li>
                                    <li><a href="#">Pasta </a></li>
                                    <li><a href="#">Wiener Schnitzel </a></li>
                                    <li><a href="#">Spearribs </a></li>
                                    <li><a href="#">Burger </a></li>
                                    <li><a href="#">Sushi </a></li>
                                    <li><a href="#">Kebap </a></li>
                                    <li><a href="#">alle Speisen </a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="links_box pd">
                            <h3 class="text-center"><strong>Our recommendations:</strong></h3>
                            <br>
                            <ul class="list-unstyled wow animated fadeInRight" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInRight;">
                                <li><a href="#">2 € Gutschein </a></li>
                                <li><a href="#">24/7 Stunden Zustellservice </a></li>
                                <li><a href="#">Nacht -Zustellservice </a></li>
                                <li><a href="#">Lieferzonas Werbung </a></li>
                                <li><a href="#">Vorteile von Lieferzonas.at </a></li>
                                <li><a href="#">Lieferzonas.at Apps </a></li>
                            </ul>
                        </div>
                    </div>
                    
                        <div class="col-sm-3">
                            <div class="links_box last pd">
                                <h3 class="text-center"><strong>Services:</strong></h3>
                                <br>
                                <ul class="list-unstyled wow animated fadeInLeft" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft ;">
                                    <li><a href="#">Heimservice </a></li>
                                    <li><a href="#">Pizzaservice </a></li>
                                    <li><a href="#">Bestellservice </a></li>
                                    <li><a href="#">Zustellservice </a></li>
                                    <li><a href="#">Pizza - Pasta Service </a></li>
                                    <li><a href="#">Zustelldienst </a></li>
                                    <li><a href="#">Bringdienst </a></li>
                                    <li><a href="#">Lieferservice </a></li>
                                    <li><a href="#">Essen Service </a></li>
                                    <li><a href="#">Sushi Service </a></li>
                                    <li><a href="#">Online Service </a></li>
                                </ul>
                            </div>
                        </div>
                    
                        <div class="col-sm-12">
                                <hr>
                        </div>
                    </div>
                
            <div class="row">
                <div class="col-sm-5 col-md-6">
                    <div class="links_box text-center" style="border:none; padding:0;">
                        <h3 class="text-center"><strong>TOP RESTAURANTS IN </strong></h3>
                        <br>
                        <ul class="list-unstyled wow animated bounceInDown" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceInDown;">
                            <li><a href="#">WIEN</a></li>
                            <li><a href="#">Niederösterreich</a></li>
                            <li><a href="#">Burgenland</a></li>
                            <li><a href="#">Oberösterreich</a></li>
                            <li><a href="#">Steiermarkt</a></li>
                            <li><a href="#">Salzburg</a></li>
                            <li><a href="#">Kärnten</a></li>
                            <li><a href="#">Tirol</a></li>
                            <li><a href="#">Vorarlberg</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-7 col-md-6">
                    <aside class="live_box">
                        <h2>Live</h2>
                        <ul class="list-unstyled panel-body wow animated fadeInRight" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
                            <li>
                                <div class="media">
                                    <div class="pull-left">{!! Html::image('resources/assets/front/img/lg.png') !!} </div>
                                    <div class="media-body">
                                        <div class="textb"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="pull-left">{!! Html::image('resources/assets/front/img/map.png') !!} </div>
                                    <div class="media-body">
                                        <div class="textb"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="pull-left">{!! Html::image('resources/assets/front/img/lg.png') !!} </div>
                                    <div class="media-body">
                                        <div class="textb"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="pull-left">{!! Html::image('resources/assets/front/img/map.png') !!}</div>
                                    <div class="media-body">
                                        <div class="textb"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="pull-left">{!! Html::image('resources/assets/front/img/lg.png') !!} </div>
                                    <div class="media-body">
                                        <div class="textb"></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>We welcome you warm at Lieferzone.at, your fast delivery service with comfort.</h3>
                <p>With us interested can the delivery of more than 600 certified partner restaurants throughout Austria Order.</p>
                <p>No desire or time to cook? Lieferzonas.at provides a variety of different restaurants, various kitchens.</p>
                <p>Not only lovers of local and Mediterranean cuisine will get their money, even asiatische-, orientalische-, indische- or Mexican dishes are available here problem.</p>
                <p><a href="#" style=" color: #81c02f;">more</a></p>
            </div>
        </div>
    </div>

    
            </div>
        </div>
    </div>

 @endsection

@section('script')
    {!! Html::script('assets/js/jquery-ui.min.js') !!}
    <script type="text/javascript">

        $("#lie-pickup-delivery-switch").change(function(evt) {
            var checked = $(this).prop("checked");
            // true: Pickup
            // false: Delivery
            $.ajax({
                url: "{{ url("set_pickup") }}",
                type: "POST",
                dataType: "json",
                data: {
                    "is_pickup": checked,
                    "_token": "{{ csrf_token() }}"
                },
                success: function(response) {
                    console.dir(response);
                }
            })
        });

        /*$(document).ready(function() {
            $( "#zipcode" ).autocomplete({
                minLength: 2,
                source: '{{url('/get_zipcodes')}}',
                focus: function(event, ui) {
                    $("#zipcode").val(ui.item.name);
                    return false;
                },
                select: function(event, ui) {
                    $("#zipcode").val(ui.item.name);
                    return false;
                }
            })
            .autocomplete("instance")._renderItem = function(ul, item) {
                return $("<li>")
                        .append( "<a>" + item.name + " - " + item.desc + "</a>" )
                        .appendTo( ul );
            }
        });*/

	$(document).ready(function() {
	    $("#header-container").removeAttr("style");
            $( "#zipcode" ).autocomplete({
                source: function( request, response ) {
                    var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ) );
                    $.ajax({
                        url: '{{url('/get_zipcodes')}}',
                        dataType: "json",
                        success: function( data ) {
                            response( $.grep( data, function( item ){
                                return matcher.test( item.label );
                            }) );
                        }
                    });

                },
                minLength: 2,
                select: function(event, ui) {
                    event.preventDefault();
                    $("#zipcode").val(ui.item.label);


                }
                ,
                focus: function(event, ui) {
                    event.preventDefault();
                    $("#zipcode").val(ui.item.label);
                    //alert(ui.item.label);
                }
            })
            .autocomplete("instance")._renderItem = function(ul, item) {
                return $("<li>")
                        .append( "<a>" + item.label + " - " + item.value + "</a>" )
                        .appendTo( ul );
            }
        });


        function submit_zipcode(i)
        {
            if (typeof(i)==='undefined') i = 0;
            $('#zip_error').hide();
            var zip = $('#zipcode').val();
            //alert(zip);
            $.ajax({
                url: '{{url('checkzip')}}',
                type: "post",
                data: {zipcode:zip, "_token":"{{ csrf_token() }}"},
                success: function(data){
                    if(data != "")
                        $('#zip_form').submit();
                    else
                    {
                        if(i == 1)
                            $('#zip_error').html('No Restaurant found for your current location.!!');
                        else
                            $('#zip_error').html('No Restaurant Found.!!');

                        $('#zip_error').show();
                    }
                }
            });
        }

        function getLocation()
        {
            if(navigator.geolocation && false)
            {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
            else
            {
                $("#lie-geolocation-error-alert").show();
                // alert('Error in fetching location. Please try again after some time.');
            }
        }

        function showPosition(position)
        {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;

            $.ajax({
                type    : "POST",
                url     : 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&sensor=true',
                //url       : 'http://maps.googleapis.com/maps/api/geocode/json?latlng=48.2079336,16.3305928&sensor=true',
                accepts : 'application/json',
                dataType:"json",
                data    : { },
                success: function (data) {
                    var len = data.results[0]['address_components'].length;
                    var zipcode = data.results[0]['address_components'][len-1]['long_name'];
                    $('#zipcode').val(zipcode);
                    submit_zipcode(1);
                }
            });
        }
        
    </script>

@endsection
