<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin | Log in </title>
    {!! Html::style('resources/assets/admin/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/admin/css/font-awesome.css') !!}
    {!! Html::style('resources/assets/admin/css/style.css') !!}
</head>
<body>

</body>
    <div class="login_section">
        <div class="effect">
            <div class="login">
                <section>
                    <figure class="logo_icon">
                        {!!Html::image('resources/assets/admin/img/logo-icon.png','','')!!}
                    </figure>
                    <figure class="text-center">
                        {!!Html::image('resources/assets/admin/img/demo.png','','')!!}
                    </figure>
                    <figure class="text-center">
                        {!!Html::image('resources/assets/admin/img/logo2.png','','')!!}
                    </figure>
                    
                    {!! Form::open(['url'=>'frontuser/login','method'=>'post']) !!}
                    <!--<form action="{{ action("Auth\AdminAuthController@getLogin") }}" method="POST">-->
                    <!--{!! csrf_field() !!}-->
                        <hr>
                        Front User
                        @if($errors->any())
                            <div class="alert alert-danger">
                              <ul>
                                @foreach($errors->all() as $error)
                                  <li> {{$error}} </li>
                                @endforeach
                              </ul>
                            </div>
                        @endif
                        
                        {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Email']) !!}
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                        <div class="text-center">
                            {!! Form::submit('Login',['id'=>'login','class'=>'btn btn-success']) !!}
                            <h4><a href="#">Forgot your password ?</a></h4>
                            <h3><a href="#">Contact service center</a></h3>
                        </div>
                        <!--</form>-->
                    {!! Form::close() !!}
                </section>
            </div>
            <div class="copy">Copyright &copy; 2016 Lieferzonas.at</div>
        </div>
    </div>

<style>
    html, body, .login_section{height: 100%;}	
</style>	

{!! Html::script('resources/assets/admin/js/jQuery-2.2.0.min.js') !!}
{!! Html::script('resources/assets/admin/js/jquery.min.js') !!}
{!! Html::script('resources/assets/admin/js/bootstrap.js') !!}
</html>