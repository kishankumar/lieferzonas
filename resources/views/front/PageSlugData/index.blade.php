@extends('layouts.frontbody')

@section('title')
    Lieferzonas 
@endsection

@section('headerclass')
    inner-header
@endsection
    

@section('body')
    
    <section class="bannner-section">
        <div class="container">
            <div class="bannr">
                <div class="row">
                    <div class="col-sm-8 col-md-7 col-md-offset-1 col-lg-8">
                        <div class="form-bg">
                            <h2>Now Delivery Services find in your area:</h2>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-form-add" id="adv-search">
                                            <input type="text" class="form-control" placeholder="zip code" />
                                            <input type="submit" class="form-control btn btn-search btn-primary" value="Continue" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="checkk">
                                        {!! Html::image('resources/assets/front/img/check.png','check') !!}
                                        9,000 Lifeservices</div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="checkk">
                                        {!! Html::image('resources/assets/front/img/check.png','check') !!}
                                        Bargeldios bezahlen</div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="checkk">
                                        {!! Html::image('resources/assets/front/img/check.png','check') !!}
                                        Apps fur unterwegs</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3 col-lg-2">
                        <div class="star-img">
                            <?php
                            if($slugDetails->image){ ?>
                                <img alt="star" src="{{asset('public/superadmin/homepage/images/'.$slugDetails->image) }}">
                            <?php } else{ ?>
                                {!! Html::image('resources/assets/front/img/star-bannr.png','star') !!}
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="work-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-8 col-lg-8">
                    <div class="content-box">
                        <p> {{ strip_tags($slugDetails->description1) }}</p>
                        <?php
                        if($slugDetails->vedio_upload_type == 'U'){ ?>
                            <div class="video-img">
                                <video id="showvideo" controls src="{{ $slugDetails->url }}" type="video/mp4" >
                                </video>
                            </div>
                        <?php }else{
                                if($slugDetails->video){ ?>
                                    <div class="video-img">
                                        <video id="showvideo" controls src="{{asset('public/superadmin/homepage/allvedios/'.$slugDetails->video) }}" type="video/mp4" >
                                        </video>
                                    </div>
                                <?php } else{ ?>
                                    <div class="video-img">
                                        {!! Html::image('resources/assets/front/img/video-img.png','video') !!}
                                    </div>
                                <?php } 
                            } 
                        ?>
                      </div>
                </div>
                <div class="col-sm-4  col-md-4 col-lg-4">
                    <div class="content-box">
                        <h2>Navigation</h2>
                        <ul class="side-link">
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                        </ul>
                        <h2>Links</h2>
                        <ul class="side-link">
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                        </ul>
                        <h2>Restaurants</h2>
                        <ul class="side-link">
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                            <li> <a href="javascript:void(0)">Bringdienst Berlin </a> </li>
                            <li> <a href="javascript:void(0)">Lieferservice Berlin Kreuzberg </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="content-box" style="margin:20px 0;">
                        <p> {{ strip_tags($slugDetails->description2) }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')
    <script type="text/javascript">
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 100) {
                $("#menu").addClass("nav-fixed");
            } else {
                $("#menu").removeClass("nav-fixed");
            }
        });
    </script>
@endsection