<!--Form for adyen payment gateway-->
    <?php 
        $skinCode        = "RZ22FtjU";
        $merchantAccount = "MeisterbetriebFiratKGAT";
        $hmacKey         = "422EB64F5D60F370877BD963AF805AF377030BFB0EB705AE05A98FD2C821987B";
        $orderData = base64_encode(gzencode("Order Id : 123456 <br>Pizza Burger Cold Drinks"));
        $params = array(
            "merchantReference" => "ORDER-".$OrderId.'-'.date("Y-m-d-H:i:s"),

            "merchantAccount"   =>  $merchantAccount, 
            "currencyCode"      => "EUR",
            "paymentAmount"     => $paymentAmount, 
            "sessionValidity"   => date("c",strtotime("+1 days")), 
            "shipBeforeDate"    => date("Y-m-d",strtotime("+1 days")), 
            "shopperLocale"     => "en_GB", 
            "skinCode"          => $skinCode,
            //"orderData" => $orderData,
        );
        $escapeval = function($val) {
            return str_replace(':','\\:',str_replace('\\','\\\\',$val));
        };
        ksort($params, SORT_STRING);
        $signData = implode(":",array_map($escapeval,array_merge(array_keys($params), array_values($params))));
        $merchantSig = base64_encode(hash_hmac('sha256',$signData,pack("H*" , $hmacKey),true));
        $params["merchantSig"] = $merchantSig;
    ?>
    <form name="adyenForm"  id="adyen-encrypted-form" action="https://test.adyen.com/hpp/select.shtml" method="post">
        <?php
            foreach ($params as $key => $value){
                echo '<input type="hidden" name="' .htmlspecialchars($key,   ENT_COMPAT | ENT_HTML401 ,'UTF-8'). 
                        '" value="' .htmlspecialchars($value, ENT_COMPAT | ENT_HTML401 ,'UTF-8') . '" />' ."\n" ;
            }
        ?>
    </form>
    <script type="text/javascript">
        var adyen = document.getElementById('adyen-encrypted-form');
        adyen.submit();
    </script>
<!--Form for adyen payment gateway-->