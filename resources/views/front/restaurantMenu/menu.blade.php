@extends('layouts.frontbody')

@section('title')
    Lieferzonas
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')
    <section class="main-dash-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-7 col-sm-3 ">
                    <div class="media">
                        <div class="pull-left">
                            <a href="javascript:getLocation()" class="map-marker"><i class="fa fa-map-marker"></i></a>
                        </div>
                        <div class="media-body">
                            {!! Form::open(['url'=>'search','class'=>'form-horizontal','method'=>'post','id'=>'zip_form']) !!}
                            <div class="media mt0">
                                <div class="pull-right">
                                    {!! Form::button('Weiter', ['class'=>'btn btn-search btn-border4','onclick'=>'submit_zipcode()']) !!}
                                </div>
                                <div class="media-body">
                                    {!! Form::text('zipcode',Session::get('zipcode'),['class'=>'btn-border3 form-control', 'placeholder'=>'Postleitzahl', 'id'=>'zipcode']) !!}
                                </div>

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div style="display:none" class="alert alert-danger text-center" id="zip_error"></div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <ul class="list-unstyled list2 mt25">
                @if(count($restCategories)>0)
                    <?php $count = 0; ?>
                    @foreach($restCategories as $cate)        <!-- show category by restaurant-->
                        @if($count<8)
                            <li><a href="#{{$cate->name}}">{{ ucfirst($cate->name) }}</a></li>
                        @else
                            <li class="view_all_categories" style="display:none"><a
                                        href="#{{$cate->name}}">{{ ucfirst($cate->name) }}</a></li>
                        @endif
                        <?php $count++; ?>
                        @endforeach
                        @if(count($restCategories)>8)
                            <li class="none"><a href="javascript:void(0)" onclick="$('.view_all_categories').toggle();">View
                                    all</a></li>
                        @endif
                    @else
                        <li><a href="javascript:void(0)">No Record Found</a></li>
                    @endif
                </ul>
                <div class="clearfix"></div>
                <br>
                <!--<div class="gray_bg panel-body">-->
                <!--                <p>Dear Customers,<br>
                                  Please note that we only deliver when full contact details (ie, telephone number and
                                  full address) are specified. We ask for your understanding.</p>
                                <p>We also accept vouchers from Sodexo and Magistra!</p>
                                <p>You agree ordering via this portal this also to pay and to accept.
                                    If this did not happen and we will suffer as a restaurant harm by it brought to the police report.</p>-->
                <!--</div>-->
            <?php
            $extrasData = App\superadmin\RestExtraMap::getExtrasByResturant($restId);

            ?>
            <!--Category Description-->
                @foreach($restCategories as $cate)
                    <div class="img_title" id="{{$cate->name}}">
                        <figure>
                            @if (file_exists('public/uploads/superadmin/category/'.$cate->image))
                                {!! Html::image('public/uploads/superadmin/category/'.$cate->image, '', array('class' => 'img-responsive')) !!}
                            @else
                                {!! Html::image('resources/assets/front/img/premium.png','',['class'=>'img-responsive']) !!}
                            @endif

                        </figure>
                        {{--<h2>Premium</h2>--}}
                    </div>
                    <!--Category Description-->

                    <!--Menu Description-->
                    @foreach(App\superadmin\RestMenu::getMenuByCategory($cate->id) as $manuData) <!-- get menu by category-->
                    <?php

                    $subMenusData = App\superadmin\RestSubMenu::getSubMenuByMenu($manuData['id']);  ///get submenu by menu
                    $submenuCount = count($subMenusData);
                    $menuPriceType = App\superadmin\RestMenu::getMenuPrice($manuData['id']);      //get price type by menu
                    $menuPriceTypeCount = count($menuPriceType);
                    $menuAlerticContents = App\superadmin\RestMenu::getMenuAlergicData($manuData['id']);   //get alergic content by menu
                    $alergicData = '0';

                    foreach ($menuAlerticContents as $menuAlerticContent) {

                        $alergicData .= ',' . $menuAlerticContent->allergic_id;
                    }
                    if ($alergicData == '0') {
                        $alergicData = '';
                    } else {
                        $alergicData = str_replace('0,', '', $alergicData);
                    }

                    $menuSpicesContents = App\superadmin\RestMenu::getMenuSpicesData($manuData['id']);   //get Spice content by menu


                    ?>
                    <h2 class="bestseller_title"><strong>{{$manuData['name']}}</strong>
                        <div class="clearfix"></div>
                        <small>{{$manuData['info']}}</small>
                    </h2>
                    <!--Menu Description-->

                    <!--Sub Menus-->
                    <?php if ($submenuCount > 0){ ?>

                    @foreach($subMenusData as $subMenuData)

                        <?php

                        $deliveryPrice = \App\superadmin\RestSubMenu::getSubMenuDeliveryPrice($subMenuData['id']);
                        $pickupPrice = \App\superadmin\RestSubMenu::getSubMenuPickupPrice($subMenuData['id']);

                        $subMenuPriceType = App\superadmin\RestSubMenu::getSubMenuCurrentPrice($subMenuData['id']);

                        $subMenuPriceTypeCount = count($subMenuPriceType);

                        $subMenuAlerticContents = App\superadmin\RestSubMenu::getSubMenuAlergicData($subMenuData['id']);   //get alergic content by submenu

                        $alergicData = '0';
                        foreach ($subMenuAlerticContents as $subMenuAlerticContent) {

                            $alergicData .= ',' . $subMenuAlerticContent->allergic_id;
                        }
                        if ($alergicData == '0') {
                            $alergicData = '';
                        } else {
                            $alergicData = str_replace('0,', '', $alergicData);
                        }

                        $subMenuSpicesContents = App\superadmin\RestSubMenu::getSubMenuSpicesData($subMenuData['id']);   //get Spice content by submenu

                        ?>
                        @if($subMenuPriceTypeCount>0)
                            <div class="bestseller-lst">
                                <div class="bestseller" data-toggle="modal" data-target=".myModal-OPN">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="media">
                                                <figure class="pull-left media-middle">
                                                    @if ($subMenuData['image'] && file_exists('uploads/superadmin/submenu/'.$subMenuData['image']))
                                                        {!! Html::image('uploads/superadmin/submenu/'.$subMenuData['image'], '', array('class' => 'thumb')) !!}
                                                    @endif
                                                </figure>
                                                <aside class="media-body">
                                                    <p><b>{{$subMenuData['name']}}</b><sup> {{$alergicData}} </sup><br>
                                                        {{$subMenuData['info']}}
                                                    </p>
                                                </aside>
                                                <div class="bestseller_right" style="margin:0">
                                                    Spices
                                                    @foreach($subMenuSpicesContents as $subMenuSpicesContent)
                                                        @if (file_exists('public/superadmin/uploads/spices/'.$subMenuSpicesContent->getSpiceData->image))
                                                            {!! Html::image('public/superadmin/uploads/spices/'.$subMenuSpicesContent->getSpiceData->image, '', array('class' => 'thumb' ,'height'=>'30px','width'=>'30px')) !!}
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            @if($pickupPrice && \Illuminate\Support\Facades\Session::get("is_pickup"))
                                                <div class="price_box"
                                                     @if (\App\superadmin\SubMenuExtraMap::hasSubMenuAnyExtras($subMenuData["id"]))
                                                     data-toggle="modal"
                                                     data-target="#sub-menu-extra-modal-{{$subMenuData["id"]}}"
                                                     @else
                                                     onclick="addItemToCart('{{$subMenuData["id"]}}')"
                                                     @endif
                                                >
                                                    <span class="glyphicon glyphicon-euro"></span> {{$pickupPrice->price}}
                                                </div>
                                            @elseif($deliveryPrice && !\Illuminate\Support\Facades\Session::get("is_pickup"))
                                                <div class="price_box"
                                                     @if (\App\superadmin\SubMenuExtraMap::hasSubMenuAnyExtras($subMenuData["id"]))
                                                     data-toggle="modal"
                                                     data-target="#sub-menu-extra-modal-{{$subMenuData["id"]}}"
                                                     @else
                                                     onclick="addItemToCart('{{$subMenuData["id"]}}')"
                                                     @endif
                                                >
                                                    <span class="glyphicon glyphicon-euro"></span> {{$deliveryPrice->price}}
                                                </div>
                                            @else
                                                @if (\Illuminate\Support\Facades\Session::get("is_pickup"))
                                                    <div class="price_box"><span>Nur Lieferung</span></div>
                                                @else
                                                    <div class="price_box"><span>Nur Abholung</span></div>
                                            @endif
                                        @endif

                                        </div>
                                    </div>
                                </div>

                            </div>
                            @include("superadmin.restaurants.submenu.extra_modal", [
                                "sub_menu_id" => $subMenuData["id"]
                            ])
                        @endif
                    @endforeach
                    <?php } else { ?>     <!-- if menu dont have submenu  -->

                    @if($menuPriceTypeCount>0)                 <!-- check menu have price or not -->
                    <div class="bestseller-lst">
                        <div class="bestseller" data-toggle="modal" data-target=".myModal-OPN">
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="media">
                                        <figure class="pull-left media-middle">
                                        @if (file_exists('public/uploads/superadmin/menu/'.$manuData['image']))
                                            {!! Html::image('public/uploads/superadmin/menu/'.$manuData['image'], '', array('class' => 'thumb' ,'height'=>'50px','width'=>'50px')) !!}
                                        @else
                                            {!! Html::image('resources/assets/front/img/default-food.png', '', array('class' => 'thumb' ,'height'=>'50px','width'=>'50px')) !!}
                                        @endif
                                        <!--{!! Html::image('public/uploads/superadmin/menu/'.$manuData['image'], '', array('class' => 'thumb' ,'height'=>'50px','width'=>'50px')) !!}-->
                                            {{--{!! Html::image('resources/assets/front/img/burger.png','') !!}--}}
                                        </figure>
                                        <aside class="media-body">
                                            <p>{{$manuData['name']}}<sup> {{$alergicData}} </sup><br>
                                                {{$manuData['info']}}
                                            </p>
                                        </aside>
                                        <div class="bestseller_right">
                                            Spices
                                        @foreach($menuSpicesContents as $menuSpicesContent)
                                            @if (file_exists('public/uploads/superadmin/spices/'.$menuSpicesContent->getSpiceData->image))
                                                {!! Html::image('public/uploads/superadmin/spices/'.$menuSpicesContent->getSpiceData->image, '', array('class' => 'thumb' ,'height'=>'30px','width'=>'30px')) !!}
                                            @endif
                                            <!--{!! Html::image('public/uploads/superadmin/spices/'.$menuSpicesContent->getSpiceData->image, '', array('class' => 'thumb' ,'height'=>'30px','width'=>'30px')) !!}-->
                                                {{--<span>{{$subMenuSpicesContent->getSpiceData->name}}</span>--}} {{--<img src="img/icon5.png" alt=""> <img src="img/icon6.png" alt="">--}}
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    @if($menuPriceType[0]->price>0)
                                        <div class="price_box"
                                             onclick="setCart('menu','{{$manuData['id']}}','{{$restNames[0]->id}}','add')">{{$menuPriceType[0]->price}}
                                            € <a href="javascript:void(0)"><i class="fa fa-plus"></i></a></div>
                                    @else
                                        <div class="price_box"><span>Only For Takeaway</span></div>
                                    @endif
                                </div>

                                @if(count($extrasData)>0)
                                    <div class="col-sm-12">
                                        <small class="add_extra btn-danger btn-xs">Add Extra</small>

                                        <div class="seller_sub_main row" style="display: none">
                                            @foreach($extrasData as $extraData)
                                                <aside class="seller_sub">
                                                    <div class="col-sm-9">
                                                        <small>{{$extraData->name}} </small>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="price_box"
                                                             onclick="setCart('extras','{{$extraData->id}}','{{$restNames[0]->id}}','add')"> {{$extraData->price}}
                                                            € <a href="#"><i class="fa fa-plus"></i></a></div>
                                                    </div>
                                                </aside>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>
                    @endif
                    <?php } ?>
                    @endforeach
                <!--Sub Menus-->
                @endforeach

                <br>
                <h2 class="bestseller_title"><strong style="text-decoration:underline;">ALLERGENE
                        INFORMATIONEN </strong>
                </h2>
                <div class="row links">
                    <div class="col-sm-12">
                        <ul class="list-unstyled">
                            @foreach($allergicContents as $allergicContent)

                                <li><a href="#">{{$allergicContent->name}} {{$allergicContent->id}}</a></li>


                            @endforeach
                            <div class="clearfix"></div>
                        </ul>
                    </div>

                    <div class="col-sm-12"><a href="#">Eine detaillierte Liste aller Zusatzstoffe findest du hier</a>
                        <p>Druckfehler, Irrtümer und Änderungen vorbehalten.</p>
                    </div>
                </div>
            </div>

            <!--//function for get rating of restaurants-->
        <?php
        $totalrating = \App\front\UserOrderReview::totalrating($restId);
        if ($totalrating != '') {
            $count = $totalrating / 10;
            $rating = \App\front\UserOrderReview::rating($restId);
            $quality_rating = '0';
            $service_rating = '0';
            $starrating = 0;
            foreach ($rating as $ratings) {
                $quality_rating = (float)$ratings->quality_rating + (float)$quality_rating;
                $service_rating = (float)$ratings->service_rating + (float)$service_rating;
                $total_rating = $quality_rating + $service_rating;
            }
            $starrating = $total_rating / ($count * 2);
        }
        ?>
        <!--//function for get rating of restaurants-->

            <div class="col-sm-4  col-md-4 col-lg-4">
                <div class="content-box leftsidebar">
                    <div class="tabbing-side menu4">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#menus" data-toggle="tab">Menu </a></li>
                            <li><a href="#actions" data-toggle="tab">Actions</a></li>
                            <li><a href="#info" data-toggle="tab">Info </a></li>
                            <li><a href="#reviews" data-toggle="tab">Reviews </a></li>
                        </ul>
                        <div class="tab-content">

                            @include('includes/cartsidebar')

                            <div class="tab-pane" id="actions">
                                <div class="action_menu">
                                    <h4>{{ $AllrestDetail->f_name }} {{ $AllrestDetail->l_name }} <span>200<br>
                      <i class="fa fa-heart"></i></span></h4>
                                    <div class="media">
                                        <figure class="pull-left">
                                        <!--{!! Html::image('resources/assets/front/img/logo_de.png','',['class'=>'img-responsive']) !!}-->

                                            @if (file_exists('public/uploads/superadmin/restaurants/'.$AllrestDetail->logo))
                                                {!! Html::image('public/uploads/superadmin/restaurants/'.$AllrestDetail->logo, '', array('class' => 'img-responsive')) !!}
                                            @else
                                                {!! Html::image('resources/assets/front/img/logo_de.png','',['class'=>'img-responsive']) !!}
                                            @endif
                                        </figure>
                                        <aside class="media-body">
                                            @if(count($dayTime))
                                                <span class="green">
                                    <i class="fa fa-clock-o"></i>
                                                    {{ $dayTime->open_time }} to {{ $dayTime->close_time }}
                                </span><br>
                                            @else
                                                <span class="green">
                                    <i class="fa fa-clock-o"></i> 
                                    Closed
                                </span><br>
                                            @endif
                                            Delivery Costs: ab 0,00 € <br>
                                            Delivery date: 9,00 € <br>
                                            <small>see info:</small>
                                        </aside>
                                        <p>
                                            <?php
                                            if($totalrating != '')
                                            {
                                            $review = floor($starrating);
                                            for ($i = 1; $i <= $review; $i++) {
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                            if (ceil($starrating) > $review) {
                                                echo '<i class="fa fa-star-half-o"></i>';
                                            }
                                            if (5 > ceil($starrating)) {
                                                for ($i = ceil($starrating); $i < 5; $i++) {
                                                    echo '<i class="fa fa-star-o text-gray"></i>';
                                                }
                                            } ?>
                                            <small>({{ $totalrating/10 }})</small>
                                            <?php } else {
                                                echo 'No Reviews Yet';
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>

                            <!--                    @if(count($restComplaint))
                                @foreach($restComplaint as $complaints)
                                    <div class="p_a">
                                        <h3> {{ ucfirst($complaints->topic)  }} </h3>
                                {{ ($complaints->description)  }}
                                            </div>
                                        @endforeach
                            @else
                                <div class="p_a">
                                    <h3>No Complaint </h3>
                                </div>
                            @endif-->

                    <!--code for special offer-->
                                    <?php $special_offerlist = \App\superadmin\RestMenu::Specialofferlist(); //print_r($special_offerlist); die?>
                                    @if($special_offerlist)
                                    @foreach($special_offerlist as $special_offerlists)
                                    <div class="p_a">
                                        <h3>{{ $special_offerlists['name'] }}</h3>
                                        {{ $special_offerlists['day'] }} {{ $special_offerlists['time_from'] }}
                                        <?php if ($special_offerlists['time_from'] != '' && $special_offerlists['time_to'] != '') {
                                            echo '-';
                                        } ?>
                                        {{ $special_offerlists['time_to'] }}
                                        &euro; <s
                                                class="text-muted">{{ $special_offerlists['original_price'] }}</s> {{ $special_offerlists['discount_price'] }}
                                    </div>
                                @endforeach
                            @endif
                            <!--<div class="p_a">
                        <h3>Pizza-action </h3>
                        Montag - Freitag: 11 - 15 Uhr bei Selbstabholung u. Zustellung Jede Pizza oder Pasta (außer Nr. 16,41,44, 57, 45a)
                        &euro; 6,50 
                    </div>
                    <div class="p_a">
                        <h3>10% Discount </h3>
                        Montag - Freitag: 11 - 15 Uhr bei Selbstabholung u. Zustellung Jede Pizza oder Pasta (außer Nr. 16,41,44, 57, 45a)
                        &euro; 6,50 
                    </div>
                    <div class="p_a">
                        <h3>4+1 action </h3>
                        Montag - Freitag: 11 - 15 Uhr bei Selbstabholung u. Zustellung Jede Pizza oder Pasta (außer Nr. 16,41,44, 57, 45a)
                        &euro; 6,50 
                    </div>
                    <div class="p_a">
                        <h3>10% Rabatt </h3>
                        Montag - Freitag: 11 - 15 Uhr bei Selbstabholung u. Zustellung Jede Pizza oder Pasta (außer Nr. 16,41,44, 57, 45a)
                        &euro; 6,50 
                    </div>
                    <!--code for special offer-->

                            </div>

                            <div class="tab-pane" id="info">
                                <div class="action_menu">
                                    <h4>{{ $AllrestDetail->f_name }} {{ $AllrestDetail->l_name }} <span>200<br>
                      <i class="fa fa-heart"></i></span></h4>
                                    <div class="media">
                                        <figure class="pull-left">

                                        @if (file_exists('public/uploads/superadmin/restaurants/'.$AllrestDetail->logo))
                                            {!! Html::image('public/uploads/superadmin/restaurants/'.$AllrestDetail->logo, '', array('class' => 'img-responsive')) !!}
                                        @else
                                            {!! Html::image('resources/assets/front/img/logo_de.png','',['class'=>'img-responsive']) !!}
                                        @endif

                                        <!--{!! Html::image('resources/assets/front/img/logo_de.png','',['class'=>'img-responsive']) !!}-->
                                        </figure>

                                        <aside class="media-body">
                                            @if(count($dayTime))
                                                <span class="green">
                                    <i class="fa fa-clock-o"></i>
                                                    {{ $dayTime->open_time }} to {{ $dayTime->close_time }}
                                </span><br>
                                            @else
                                                <span class="green">
                                    <i class="fa fa-clock-o"></i> 
                                    Closed
                                </span><br>
                                            @endif
                                            Delivery Costs: ab 0,00 € <br>
                                            Delivery date: 9,00 € <br>
                                            <small>see info:</small>
                                        </aside>

                                        <p>
                                            <?php
                                            if($totalrating != '')
                                            {
                                            $review = floor($starrating);
                                            for ($i = 1; $i <= $review; $i++) {
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                            if (ceil($starrating) > $review) {
                                                echo '<i class="fa fa-star-half-o"></i>';
                                            }
                                            if (5 > ceil($starrating)) {
                                                for ($i = ceil($starrating); $i < 5; $i++) {
                                                    echo '<i class="fa fa-star-o text-gray"></i>';
                                                }
                                            } ?>
                                            <small>({{ $totalrating/10 }})</small>
                                            <?php } else {
                                                echo 'No Reviews Yet';
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>

                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d448182.50738077075!2d77.0932634!3d28.646965499999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1459238319855"
                                        width="100%" height="300" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>

                                <h3>Address</h3>
                                <p>{{ ucfirst($AllrestDetail->add1)}}</p>
                                <h3>Opening times:</h3>
                                <table>
                                    @if(count($restDayTiming))
                                        @foreach($restDayTiming as $info)
                                            <tr>
                                                <td>{{ ucfirst($info->day)  }}:</td>
                                                <td>{{ (($info->open_time) ? $info->open_time : 'Closed') }}
                                                    - {{ (($info->close_time) ? $info->close_time : 'Closed') }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="2" align="center">
                                                No Record Exist
                                            </td>
                                        </tr>
                                    @endif
                                </table>

                                <h3>The provider's website:</h3>
                                <p>megaburgerpizzeriamaspiro.at</p>
                                <h4 class="mt25"><strong>This delivery service delivers in the following areas:</strong>
                                </h4>
                                <table width="100%">
                                    @if(count($RestZipcodeDeliveryMap))
                                        @foreach($RestZipcodeDeliveryMap as $Maps)
                                            <tr>
                                                <td>{{ ucfirst($Maps->zipcode)  }} Wien</td>

                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="2" align="center">
                                                No Record Exist
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                                <br>
                                <div class="row">
                                    <div class="col-xs-4"><strong>Delivery Times</strong> <br>
                                        11:00 - 03:00
                                    </div>
                                    <div class="col-xs-4"><strong>Delivery Costs</strong> <br>
                                        KOSTENLOS
                                    </div>
                                    <div class="col-xs-4"><strong>Minimum Value</strong> <br>
                                        € 14,90
                                    </div>
                                </div>
                                <h3 class="mt25">Imprint:</h3>

                                <div class="row">
                                    <!--<div class="col-sm-12">Provider</div>-->
                                    <div class="col-sm-3">Company:</div>
                                    <div class="col-sm-9"> {{ $AllrestDetail->f_name }} {{ $AllrestDetail->l_name }}</div>
                                    <div class="col-sm-3">Owner:</div>
                                    <div class="col-sm-9">
                                        @if(count($ownerDetails))
                                            {{ $ownerDetails->firstname }} {{ $ownerDetails->lastname }}
                                        @else
                                            {{'None'}}
                                        @endif
                                    </div>
                                    <div class="col-sm-3">Address:</div>
                                    <div class="col-sm-9"> {{ ucfirst($AllrestDetail->add1) }} </div>
                                    <div class="col-sm-3">St. No.:</div>
                                    <div class="col-sm-9"> {{ (($AllrestDetail->add2) ? ucfirst($AllrestDetail->add2) : 'None') }} </div>
                                    <?php $getrestemail = App\superadmin\RestMenu::getrestemail($AllrestDetail->id); ?>
                                    <div class="col-sm-3">E-Mail:</div>
                                    <div class="col-sm-9">{{ $getrestemail['0']['email'] }}</div>
                                </div>
                                <h3 class="mt25">Payment options:</h3>
                                <p>Bar, PayPal, Sofortüberweisung, Kreditkarte, Bitcoins</p>
                                {!! Html::image('resources/assets/front/img/card.png','',['class'=>'img-responsive']) !!}
                            </div>

                            <div class="tab-pane" id="reviews">
                                <div class="action_menu">
                                    <h4>{{ $AllrestDetail->f_name }} {{ $AllrestDetail->l_name }} <span>200<br>
                            <i class="fa fa-heart"></i></span>
                                    </h4>
                                    <div class="media">
                                        <figure class="pull-left">
                                        <!--                                {!! Html::image('resources/assets/front/img/logo_de.png','',['class'=>'img-responsive']) !!}-->
                                            @if (file_exists('public/uploads/superadmin/restaurants/'.$AllrestDetail->logo))
                                                {!! Html::image('public/uploads/superadmin/restaurants/'.$AllrestDetail->logo, '', array('class' => 'img-responsive')) !!}
                                            @else
                                                {!! Html::image('resources/assets/front/img/logo_de.png','',['class'=>'img-responsive']) !!}
                                            @endif
                                        </figure>

                                        <aside class="media-body">
                                            @if(count($dayTime))
                                                <span class="green">
                                        <i class="fa fa-clock-o"></i>
                                                    {{ $dayTime->open_time }} to {{ $dayTime->close_time }}
                                    </span><br>
                                            @else
                                                <span class="green">
                                        <i class="fa fa-clock-o"></i> 
                                        Closed
                                    </span><br>
                                            @endif
                                            Delivery Costs: ab 0,00 € <br>
                                            Delivery date: 9,00 € <br>
                                            <small>see info:</small>
                                        </aside>
                                        <p>
                                            <?php
                                            if($totalrating != '')
                                            {
                                            $review = floor($starrating);
                                            for ($i = 1; $i <= $review; $i++) {
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                            if (ceil($starrating) > $review) {
                                                echo '<i class="fa fa-star-half-o"></i>';
                                            }
                                            if (5 > ceil($starrating)) {
                                                for ($i = ceil($starrating); $i < 5; $i++) {
                                                    echo '<i class="fa fa-star-o text-gray"></i>';
                                                }
                                            } ?>
                                            <small>({{ $totalrating/10 }})</small>
                                            <?php } else {
                                                echo 'No Reviews Yet';
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>

                                <?php
                                $totalPercentage = 0;
                                if ($totalrating != '') {
                                    $count = $totalrating / 10;
                                    $rating = \App\front\UserOrderReview::rating($restId);
                                    $quality_rating = '0';
                                    $service_rating = '0';
                                    $starrating = 0;
                                    foreach ($rating as $ratings) {
                                        $quality_rating = (float)$ratings->quality_rating + (float)$quality_rating;
                                        $service_rating = (float)$ratings->service_rating + (float)$service_rating;
                                        $total_rating = $quality_rating + $service_rating;
                                    }
                                    $totalPercentage = round(($total_rating * 100) / $totalrating);
                                }

                                $RestReviews = \App\front\UserOrderReview::getLatestReviews($restId);
                                if(count($RestReviews)){
                                if($totalPercentage > 50){
                                ?>
                                <div class="alert alert-success">
                                    <div class="media">
                                        <div class="media-left" style="font-size:25px; white-space: nowrap;"><i
                                                    class="fa fa-thumbs-up"></i> {{ $totalPercentage }}%
                                        </div>
                                        <div class="media-body text-center"> Positive Feedback.</div>
                                    </div>
                                </div>
                                <?php } else { ?>
                                <div class="alert alert-danger">
                                    <div class="media">
                                        <div class="media-left" style="font-size:25px; white-space: nowrap;"><i
                                                    class="fa fa-thumbs-down text-danger fa-flip-horizontal"></i> {{ $totalPercentage }}
                                            %
                                        </div>
                                        <div class="media-body text-center"> Negative Feedback.</div>
                                    </div>
                                </div>
                                <?php }
                                }?>

                                @if(count($RestReviews))
                                    @foreach($RestReviews as $RestReview)
                                        {{--*/ $thmbClass='' /*--}}
                                        {{--*/ $reviewPoints=0 /*--}}
                                        {{--*/ $reviewPoints=($RestReview->quality_rating + $RestReview->service_rating)/2  /*--}}
                                        @if($reviewPoints>2)
                                            {{--*/ $thmbClass = 'fa-thumbs-up text-success' /*--}}
                                        @else
                                            {{--*/ $thmbClass = 'fa fa-thumbs-down text-danger fa-flip-horizontal' /*--}}
                                        @endif

                                        <div class="media review_box">
                                            <div class="media-left text-center">
                                                <?php
                                                $FrontUserDetails = \App\front\UserOrderReview::userdetail($RestReview->user_id);
                                                ?>
                                                @if(count($FrontUserDetails))
                                                    @if (file_exists('public/front/uploads/users/'.$FrontUserDetails->profile_pic))
                                                        <img src="{{asset('public/front/uploads/users/'.$FrontUserDetails->profile_pic) }}"
                                                             class="img-circle" height="65" width="65">
                                                    @else
                                                        <img src="{{asset('public/front/uploads/users/face.png') }}"
                                                             class="img-circle" height="65" width="65">
                                                    @endif
                                                @endif

                                                @if(count($FrontUserDetails))
                                                    {{ ucfirst($FrontUserDetails->fname) }}
                                                @endif
                                            </div>

                                            <aside class="media-body">
                                                <h4><i class="fa {{ $thmbClass }}"></i>
                                                    <?php if (strtotime($RestReview->created_at) > 0) {
                                                        echo date("d M Y", strtotime($RestReview->created_at));
                                                    }?>
                                                    <span class="pull-right action_menu" style="margin-top:-10px;">
                                        <?php
                                                        $review = floor($reviewPoints);
                                                        for ($i = 1; $i <= $review; $i++) {
                                                            echo '<i class="fa fa-star"></i>';
                                                        }
                                                        if (ceil($reviewPoints) > $review) {
                                                            echo '<i class="fa fa-star-half-o"></i>';
                                                        }
                                                        if (5 > ceil($reviewPoints)) {
                                                            for ($i = ceil($reviewPoints); $i < 5; $i++) {
                                                                echo '<i class="fa fa-star-o text-gray"></i>';
                                                            }
                                                        }
                                                        ?>
                                        </span>
                                                    <!--                                        <span class="pull-right star-5">
                                                                                                <i class="fa fa-star active"></i>
                                                                                                <i class="fa fa-star"></i>
                                                                                                <i class="fa fa-star"></i>
                                                                                                <i class="fa fa-star"></i>
                                                                                                <i class="fa fa-star"></i>
                                                                                            </span> -->
                                                </h4>
                                                {{ $RestReview->comment }}
                                                <p class="text-right"><i
                                                            class="fa fa-exclamation-circle text-muted"></i>
                                                </p>
                                            </aside>
                                        </div>
                                @endforeach
                            @endif

                            <!--                  <div class="media review_box">
                    <div class="pull-left text-center">
                        {!! Html::image('resources/assets/front/img/imgres.jpg','',['class'=>'img-circle','width'=>'65']) !!}
                                    Julius
                                </div>
                                <aside class="media-body">
                                  <h4><i class="fa fa-thumbs-up text-success"></i> 13.12.16 <span class="pull-right star-5"> <i class="fa fa-star active"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span> </h4>
                                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                  <p class="text-right"><i class="fa fa-exclamation-circle text-muted"></i></p>
                                </aside>
                              </div>-->
                            <!--                  <div class="media review_box">
                    <div class="pull-left text-center">
                        {!! Html::image('resources/assets/front/img/imgres.jpg','',['class'=>'img-circle','width'=>'65']) !!}
                                    Julius</div>
                                <aside class="media-body">
                                  <h4><i class="fa fa-thumbs-down text-danger fa-flip-horizontal"></i> 13.12.16 <span class="pull-right star-5"> <i class="fa fa-star active"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span> </h4>
                                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                  <p class="text-right"><i class="fa fa-exclamation-circle text-muted"></i></p>
                                </aside>
                              </div>-->
                            <!--                  <div class="media review_box">
                    <div class="pull-left text-center">
                        {!! Html::image('resources/assets/front/img/imgres.jpg','',['class'=>'img-circle','width'=>'65']) !!}
                                    Julius</div>
                                <aside class="media-body">
                                  <h4><i class="fa fa-thumbs-up text-success"></i> 13.12.16 <span class="pull-right star-5"> <i class="fa fa-star active"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span> </h4>
                                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                  <p class="text-right"><i class="fa fa-exclamation-circle text-muted"></i></p>
                                </aside>
                              </div>-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <!--    <div class="modal fade myModal-OPN" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-alt"></i> Share</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <a title="Facebook" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x"></i>
                            </span>
                        </a> 
                        <a title="Twitter" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x"></i>
                            </span>
                        </a> 
                        <a title="Google+" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-google-plus fa-stack-1x"></i>
                            </span>
                        </a> 
                        <a title="Linkedin" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x"></i>
                            </span>
                        </a> 
                        <a title="Reddit" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-reddit fa-stack-1x"></i>
                            </span>
                        </a> 
                        <a title="WordPress" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-wordpress fa-stack-1x"></i>
                            </span>
                        </a> <a title="Digg" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-digg fa-stack-1x"></i>
                            </span>
                        </a>
                        <a title="Stumbleupon" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-stumbleupon fa-stack-1x"></i>
                            </span>
                        </a>
                        <a title="E-mail" href="">
                            <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-envelope fa-stack-1x"></i>
                            </span>
                        </a>  
                        <a title="Print" href="">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-print fa-stack-1x"></i>
                            </span>
                        </a>
                    </p>
                    <h2><i class="fa fa-envelope"></i> Newsletter</h2>
                    <p>Subscribe to our weekly Newsletter and stay tuned.</p>

                    <form action="" method="post">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" class="form-control" placeholder="your@email.com">
                        </div>
                        <br />
                        <button type="submit" value="sub" name="sub" class="btn btn-primary"><i class="fa fa-share"></i> Subscribe Now!</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>-->
    <!-- Modal -->

@endsection


@section('script')

    <script type="text/javascript">
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 100) {
                $("#menu").addClass("nav-fixed");
            } else {
                $("#menu").removeClass("nav-fixed");
            }
        });
    </script>
    <script type="text/javascript">
        $('.dropdown-menu').click(function (event) {
            event.stopPropagation();
        });
        $('.dropdown-menu .bottom h4').click(function (e) {
            $('.inf, .loggin').slideToggle(350);
            $('.dropdown-menu .bottom').toggleClass('act-bottom');
        });
        $('body').click(function (e) {
            $('.inf').hide();
            $('.loggin').show();
        });
    </script>
    <script type="text/javascript">
        $(function () {
            var msie6 = $.browser == 'msie' && $.browser.version < 7;
            if (!msie6 && $('.leftsidebar').offset() != null) {
                var top = $('.leftsidebar').offset().top - parseFloat($('.leftsidebar').css('margin-top').replace(/auto/, 0));
                var height = $('.leftsidebar').height();
                var winHeight = $(window).height();
                var winHeight_s = parseInt(winHeight) + parseInt(parseInt(winHeight) / 2);
                var footerTop = $('#footer').offset().top - parseFloat($('#footer').css('margin-top').replace(/auto/, 0));
                var gap = 7;
                $(window).scroll(function (event) {
                    // what the y position of the scroll is
                    var y = $(this).scrollTop();
                    // whether that's below the form
                    if (y + winHeight_s >= top + height + gap && y + winHeight <= footerTop) {
                        // if so, ad the fixed class
                        $('.leftsidebar').addClass('leftsidebarfixed').css('top', winHeight_s - height - gap + 'px');
                    }
                    else if (y + winHeight > footerTop) {
                        // if so, ad the fixed class
                        $('.leftsidebar').addClass('leftsidebarfixed').css('top', footerTop - height - y - gap + 'px');
                    }
                    else {
                        // otherwise remove it
                        $('.leftsidebar').removeClass('leftsidebarfixed').css('top', '0px');
                    }
                });
            }
        });

    </script>



@endsection

{!! Html::script('resources/assets/front/js/jquery-2.2.0.min.js') !!}
<script type="text/javascript">

    $(document).ready(function () {
        $("#place-order").remove();
    });

    $(document).ready(function () {
        $('.add_extra').click(function () {
            $(this).next('.seller_sub_main').slideToggle();
        });
    });

    $(function () {
        updateCart();
    });

</script>

{{--Nikhil's Code------------------------------------------------------------------------------------------------------}}
<script type="text/javascript">
    function submit_zipcode(i) {
        if (typeof(i) === 'undefined') i = 0;
        $('#zip_error').hide();
        var zip = $('#zipcode').val();
        //alert(zip);
        $.ajax({
            url: '{{url('checkzip')}}',
            type: "post",
            data: {zipcode: zip, "_token": "{{ csrf_token() }}"},
            success: function (data) {
                if (data != "")
                    $('#zip_form').submit();
                else {
                    if (i == 1)
                        $('#zip_error').html('No Restaurant found for your current location.!!');
                    else
                        $('#zip_error').html('No Restaurant Found.!!');

                    $('#zip_error').show();
                }
            }
        });
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
        else {
            alert('Error in fetching location. Please try again after some time.');
        }
    }

    function clearCart() {
        $.ajax({
            url: "{{url('front/clear_cart')}}",
            type: "post",
            dataType: "json",
            data: {
                "_token": "{{csrf_token()}}",
                "restaurant_id": "{{$AllrestDetail->id}}"
            },
            success: function (response) {
                if (response.status === "failure") return console.error(response.reason);
                $("#FrontShoppingCart").html(response.html);
            }
        });
    }

    function updateCart() {
        $.ajax({
            url: "{{url('front/get_cart')}}",
            type: "get",
            dataType: "html",
            data: {
                "_token": "{{csrf_token()}}",
                "restaurant_id": "{{$AllrestDetail->id}}",
                "editable": true
            },
            success: function (response) {
                $("#FrontShoppingCart").html(response);
            }
        });
    }

    function incrementCartItem(index) {
        console.log("Incrementing " + index);
        $.ajax({
            url: "{{url("front/increment_cart_item")}}",
            type: "post",
            dataType: "json",
            data: {
                restaurant_id: "{{$AllrestDetail->id}}",
                index: index,
                _token: "{{csrf_token()}}"
            },
            success: function (response) {
                if (response.status != "success") return console.error(response.reason);
                $("#FrontShoppingCart").html(response.html);
            }
        });
    }

    function decrementCartItem(index) {
        console.log("Decrementing " + index);
        $.ajax({
            url: "{{url("front/decrement_cart_item")}}",
            type: "post",
            dataType: "json",
            data: {
                restaurant_id: "{{$AllrestDetail->id}}",
                index: index,
                _token: "{{csrf_token()}}"
            },
            success: function (response) {
                if (response.status != "success") return console.error(response.reason);
                $("#FrontShoppingCart").html(response.html);
            }
        });
    }

    function addItemToCart(subMenuId) {
        var modal = $("#sub-menu-extra-modal-" + subMenuId);
        var extras = {};
        var extrasExist = false;
        modal.find(".extra-single-choice").each(function (index) {
            var selection = $(this);
            extras[selection.data("extra")] = selection.val();
            extrasExist = true;
        });
        modal.find(".extra-multiple-choice").each(function (index) {
            var checkbox = $(this);
            if (!(checkbox.data("extra") in extras)) {
                extras[checkbox.data("extra")] = [];
            }
            if (checkbox.prop("checked")) {
                extras[checkbox.data("extra")].push(checkbox.val());
            }
            extrasExist = true;
        });
        if (!extrasExist) extras = [];
        $.ajax({
            url: "{{url('front/add_item_to_cart')}}",
            type: "post",
            dataType: "json",
            data: {
                "_token": "{{csrf_token()}}",
                "restaurant_id": "{{$AllrestDetail->id}}",
                "item_id": subMenuId,
                "extras": extras
            },
            success: function (response) {
                console.dir(response);
                if (response.status == "success") {
                    $("#FrontShoppingCart").html(response.html);
                }
            }
        })
    }

    function showPosition(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;

        $.ajax({
            type: "POST",
            url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true',
            //url   : 'http://maps.googleapis.com/maps/api/geocode/json?latlng=48.2079336,16.3305928&sensor=true',
            accepts: 'application/json',
            dataType: "json",
            data: {},
            success: function (data) {
                var len = data.results[0]['address_components'].length;
                var zipcode = data.results[0]['address_components'][len - 1]['long_name'];

                $('#zipcode').val(zipcode);
                submit_zipcode(1);
            }
        });
    }
</script>
