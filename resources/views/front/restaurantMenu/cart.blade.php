<?php
/**
 * Created by PhpStorm.
 * User: Aras
 * Date: 14.11.2016
 * Time: 19:17
 */
?>

<div class="cart" style="max-height:500px;">
    <h3>Warenkorb</h3>
    <table class="table">
        <tbody>
        @foreach ($cart as $key => $cart_element)
            <?php
            $sub_menu = \App\superadmin\RestSubMenu::find($cart_element->item_id);
            if (\Illuminate\Support\Facades\Session::get("is_pickup")) {
                $price = \App\Http\Controllers\front\FrontCartController::getCartElementPrice($cart_element, '2');
            } else {
                $price = \App\Http\Controllers\front\FrontCartController::getCartElementPrice($cart_element, '1');
            }
            ?>
            <tr>
                <td>
                    @if ($editable)
                    <div class="btn-group-vertical btn-group-xs">
                        <button type="button" class="btn btn-sm btn-success" onclick="incrementCartItem({{$key}})"><b>+</b></button>
                        <button type="button" class="btn btn-sm btn-danger" onclick="decrementCartItem({{$key}})"><b>-</b></button>
                    </div>
                    @endif
                    <b>{{$cart_element->count}}x {{$sub_menu->name}}</b>
                    <br/>
                    @if ($cart_element->extras)
                        +
                        @foreach ($cart_element->extras as $extra)
                            <?php
                            $extra_element_array = [];
                            if (!is_array($extra)) {
                                $extra_obj = \App\superadmin\ExtraChoiceElement::find($extra);
                                $extra_element_array[] = $extra_obj;
                            } else {
                                foreach ($extra as $extra_element) {
                                    $extra_obj = \App\superadmin\ExtraChoiceElement::find($extra_element);
                                    $extra_element_array[] = $extra_obj;
                                }
                            }
                            ?>
                            @foreach ($extra_element_array as $extra_element)
                                [{{$extra_element->name}}]
                            @endforeach
                        @endforeach
                    @endif
                </td>
                <td style="text-align: right; vertical-align:middle;">
                    @if ($price)
                    <b>&euro; {{number_format($price * $cart_element->count, 2)}}</b>
                    @else
                        Ungültig
                    @endif
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @if ($editable && $cart)
    <div class="btn-group pull-right">
        <button type="button" class="btn btn-sm btn-danger" onclick="clearCart()">Warenkorb löschen</button>
        <button type="button" class="btn btn-sm btn-success" onclick="window.location.href='/checkout/{{$restaurant_id}}'">Zur Kassa</button>
    </div>
    @elseif (!$editable)
    <div class="btn-group pull-right">
        <button type="button" class="btn btn-sm btn-primary" onclick="window.location.href='/restaurant/{{$restaurant_id}}/search'">Warenkorb bearbeiten</button>
    </div>
    @endif
</div>