@extends('layouts.frontbody')

@section('title')
    Lieferzonas
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

<section class="main-dash-section">
  <div class="container">
    <div class="row">
        <div class="col-sm-12 color-white">
            <label class="radio-inline">
                <input type="radio" name="female" id="delivery" checked>
                <span> Delivery</span></label>
            <label class="radio-inline">
                <input type="radio" name="female" id="pickup">
                <span>Pickup</span></label>
        </div>
    </div>
  </div>
</section>

@if(Session::has('errmessage'))
    <li style="color:red;"> {{ Session::get('errmessage') }}</li>

    {{ Session::put('errmessage','') }}
@endif
    {{--Error Display--}}
@if($errors->any())
    <ul class="alert">
        @foreach($errors->all() as $error)
            <li style="color:red;"> <b>{{ $error }}</b></li>
        @endforeach
    </ul>
@endif
{{--    Error Display ends--}}
<div class="container margin-lg-top">
    <div class="row">
      <div class="col-sm-8">
          <h3>&nbsp;*  Required fields</h3>
          <div class="prof-form">
          <h5>Your Basic Information</h5>
              {!! Form::open(array('url'=>'registercart','id'=>'cart-register-form')) !!}
              <div class="require_star">
                <span>*</span>
                  {!! Form :: text('fisrt_name','',['placeholder'=>'First Name','class'=>'form-control','id'=>'first-name'])  !!}
              </div>
              <div class="require_star">
                <span>*</span>
                  {!! Form :: text('last_name','',['placeholder'=>'Last Name','class'=>'form-control','id'=>'last-name'])  !!}

              </div>
              <div class="require_star">
                <span>*</span>
                  {!! Form :: text('email','',['placeholder'=>'Email','class'=>'form-control','id'=>'user-email',
                  'onchange'=>'checkExistValOneCond("user-email","front_users","email","email-exist-error","This email is already exist","")'])  !!}
                  <span id="email-exist-error" style="color:red;"></span>
              </div>
              <div class="require_star clr_in">
                <span>*</span>
				  
				  
                  {!! Form :: password('password','',['placeholder'=>'Password','class'=>'form-control','id'=>'password'])  !!}
				  
				  
				  
				  <style>
				  	.clr_in input{ background: #ddd; color: #222; height: 45px; border: 1px solid #ccc; border-radius: 4px; width: 100%; box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;}
				  </style>
              </div>
			  
              <h5>Your Address Information</h5>
              <div class="require_star">
                  <span>*</span>
                  {!! Form :: text('phone','',['placeholder'=>'Phone Number','class'=>'form-control','id'=>'phone'])  !!}

              </div>

              <div class="require_star" style=" width: 95%;">
                <span>*</span>
                  {!! Form :: textarea('address','',['placeholder'=>'Address','class'=>'form-control','rows'=>4,'id'=>'address1'])  !!}

              </div>
              <div class="require_star">
                  <span>*</span>
                  {!! Form :: text('landmark','',['placeholder'=>'Landmark','class'=>'form-control','id'=>'address2'])  !!}

              </div>
          


              <div class="require_star">
                <span>*</span>
                  {!! Form :: text('zipcode','',['placeholder'=>'Zip Code','class'=>'form-control','id'=>'zipcode'])  !!}

              </div>



              <div class="require_aria">
                  <div class="form-group">
                    <textarea placeholder=" Further information" rows="4" id="" class="form-control"></textarea>
                  </div>

              </div>
              <div class="clearfix"></div>

              {!! Form :: hidden('restId',$restId,['id'=>'restro-mobile-1-count'])  !!}
              <div class="text-right form-group">
              <label class="checkbox"><input type="checkbox" name="3"> <span>Ich akzeptiere die Allgemeinen Geschattsbedingungen von Mjam. </span></label>
              <button class="btn btn-green btn-lg">Zahlungspflichtig bestellen</button>
              </div>
              {!! Form::close() !!}
          </div>

      </div>

      <div class="col-sm-4  col-md-4 col-lg-4">

          <div class="tabbing-side">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#Shopping" data-toggle="tab">Shopping </a></li>
              <li><a href="#Help" data-toggle="tab">Help</a></li>
            </ul>
            @include('includes/cartsidebar')
             </div>
      </div>
    </div>
  </div>


@endsection

{!! Html::script('resources/assets/front/js/jquery-2.2.0.min.js') !!}
<script type="text/javascript">
    $(document).ready(function(){
        $("#ckeckout").remove();
        $("#place-order").remove();
    });

</script>






