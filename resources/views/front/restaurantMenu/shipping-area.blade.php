@extends('layouts.frontbody')

@section('title')
    Lieferzonas
@endsection

@section('headerclass')
    inner-header
@endsection

@section('body')

    <section class="main-dash-section">
        <div class="container">
        </div>
    </section>

    @if(Session::has('errmessage'))
        <li style="color:red;"> {{ Session::get('errmessage') }}</li>
        {{ Session::put('errmessage','') }}
        @if($errors->any())
            <ul class="alert">
                @foreach($errors->all() as $error)
                    <li style="color:red;"><b>{{ $error }}</b></li>
                @endforeach
            </ul>
        @endif
    @endif

    <div class="container margin-lg-top">
        <div class="row">

            <div class="col-sm-8">
                <h3 class="green">Details</h3>
                {!! Form::open(array('url'=>'checkout','id'=>'')) !!}

                <div class="form-group row">
                    <label class="col-sm-3">Bestellungstyp</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label class="radio-inline">
                                <input type="radio" name="order_type" value="1"
                                       onclick="$('#lie-addresse-div').attr('style', '')">
                                <span> Zustellung</span></label>
                        </div>
                        <div class="form-group">
                            <label class="radio-inline">
                                <input type="radio" name="order_type" value="2"
                                       onclick="$('#lie-addresse-div').attr('style', 'display: none;')">
                                <span> Abholung</span></label>
                        </div>
                    </div>
                </div>

                <div id="lie-addresse-div" style="display: none;">

                    <div class="form-group row">
                        <label class="col-sm-3">Adresse</label>
                    </div>
                    @if (\Illuminate\Support\Facades\Auth::check("front"))
                        <div class="row">
                            @foreach($addressDetails as $addressDetail)
                                <div class="col-sm-6">
                                    <div class="well add_box">
                                        <div class="row">
                                            <?php //$udetail = \App\front\FrontUserDetail::userdetail(); ?>
                                            <div class="col-md-9">{{$addressDetail->booking_person_name}}</div>
                                            <div class="col-md-3">
                                                <aside><span><a href="javascript:void(0)"
                                                                onclick="editAddressModal('{{$addressDetail->id}}')"><i
                                                                    class="fa fa-pencil"></i></a></span><span><a
                                                                href="javascript:void(0)"
                                                                onclick="deleteAddressModal('{{$addressDetail->id}}')"><i
                                                                    class="fa fa-trash"></i></a></span></aside>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {{$addressDetail->address.' '.$addressDetail->landmark}}

                                        </div>
                                        <div class="form-group">
                                            Phone: {{$addressDetail->mobile}}
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"><a href="javascript:void(0)"
                                                                     onclick="setAddress('{{$addressDetail->id}}')"
                                                                     class="btn btn-success add_add">Select Address</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3"><a href="#" class="btn btn-success brn-rg" data-target="#Add_address"
                                                     data-toggle="modal">Add Address</a></div>
                        </div>
                        @else
                        <div class="form-group row">
                            <div class="col-md-3">Name :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="add-name" name="add_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Pin code :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="add-pincode"
                                                         name="add_pincode"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Address :</div>
                            <div class="col-md-9"><textarea class="form-control" id="add-address"
                                                            name="add_address"></textarea></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Landmark</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="add-landmark"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Phone :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="add-mobile"
                                                         name="add_mobile"></div>
                        </div>
                        <input type="hidden" id="add-user-id"
                               value="<?php if (Auth::check('front')) echo Auth::user('front')->id ?>">
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success" id="add-form"
                                        onclick="setAddressModal('add')">Save
                                </button>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="form-group row">
                    <label class="col-sm-3">Lieferzeit</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label class="radio-inline">
                                <input type="radio" checked name="order_date_time" onClick="$('.preoder').hide();"
                                       value="1">
                                <span> Sofort</span></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12 col-lg-4 radio-inline" onClick="$('.preoder').show();">
                                <input type="radio" name="order_date_time"
                                       value="2"><span> Vorbestellen</span></label>
                            <div class="preoder" style="display:none;">
                                <div class="form-group col-sm-6 col-lg-4">

                                    <select class="form-control selectpicker" id="pre-order-day"
                                            name="delivery_date">
                                        <option value="<?php echo date('Y-m-d'); ?>"
                                                selected><?php echo date('d-m-Y'); ?></option>
                                        <?php
                                        for ($i = 0; $i < 6; $i++) { ?>
                                        <option value="<?php echo date('Y-m-d', strtotime('tomorrow + ' . $i . ' day')) ?>"><?php echo date('d-m-Y', strtotime('tomorrow + ' . $i . ' day')) ?></option>

                                        <?php } ?>

                                    </select>
                                </div>

                                <div class="form-group col-sm-6 col-lg-4">

                                    <select class="form-control" data-style="btn-transp-4" id="today"
                                            name="delivery_time">
                                        <?php
                                        $currentDateTime = date('d-m-Y H:i:s');
                                        $startTime = date('h:i A', strtotime($currentDateTime));
                                        /* if(count($closeTimeForToday)>0){
                                           $endTime=$closeTimeForToday[0]->close_time;
                                         }
                                         else{*/
                                        $endTime = '11:30PM';
                                        // }

                                        $hour_diff = round(abs(strtotime($startTime) - strtotime($endTime)) / 3600);

                                        //$hour_diff = 5;

                                        $t = time();
                                        $t = round($t / (15 * 60)) * (15 * 60);
                                        $t = $t + (45 * 60);
                                        for ($i = 0; $i < ($hour_diff - 1) * 4; $i++) {
                                        $t = $t + (15 * 60); ?>
                                        <option value="<?php echo(date("h:i A", $t)); ?>"><?php echo(date("h:i A", $t)); ?></option>

                                        <?php } ?>

                                    </select>


                                    <select class="form-control" data-style="btn-transp-4" id="otherday"
                                            name="delivery_time" style="display: none;">


                                    </select>
                                    <input type="hidden" value="<?php echo $restId ?>" id="restId">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>


                <br>
                <div class="row form-group xs-center">
                    <figure class="col-sm-4 text-center">{!! Html::image('resources/assets/front/img/service100.png','',['class'=>'img-responsive']) !!} </figure>
                    <div class="col-sm-8">
                        <h4>NEU: BEI ONLINEZAHLUNG <br>
                            <strong>GELD ZURÜCK SERVICE!</strong></h4>
                        <br>
                        <p>MEHR SICHERHEIT BEIM BESTELLEN <strong>NUR BEI ONLINEZAHLUNG</strong> MIT <br>SOFORTÜBERWEISUNG,
                            PAYPAL, <br>PAYBOX/HANDY ODER KREDITKARTE. </p>
                        <div class="text-right">
                            <a href="#" class="btn btn-green btn-lg">Mehr erfahren!<i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
                <label class="checkbox mt25">
                    <input type="checkbox" name="subscription">
                    <span> Ich möchte laufend über Aktionen, Angebote und neue Restaurants informiert werden. </span>
                </label>
                <hr>
                <?php
                if (Auth::check("front")) {
                    $cashbackPoints = App\front\UserCashbackPoint::availablepoint(Auth::user('front')->id);
                    $bonusPoints = App\front\UserBonusPoint::availablepoint(Auth::user('front')->id);
                } else {
                    $cashbackPoints = 0;
                    $bonusPoints = 0;
                }
                ?>
                <div class="row">
                    <div class="col-md-6">
                        @if (\Illuminate\Support\Facades\Auth::check("front"))
                            <h4 class="green">You currently have {{$bonusPoints}} bonus points, {{$cashbackPoints}}
                                CASHBACK
                                points. Do you einkisen your points? </h4>
                            <div class="bonus-point">
                                <label class="checkbox">
                                    <input type="checkbox" name="bonus_point" id="bonus-point">
                                    <span>Yes, I want to redeem my reward points.</span>
                                </label>
                                <h4 class="green">Mir will be deducted up to {{$bonusPoints/$bonusPointValue}} of
                                    the
                                    order.</h4></div>
                            <div class="cashback-point">
                                <label class="checkbox ">
                                    <input type="checkbox" name="cashback_point" id="cashback-point">
                                    <span>Yes, I want my einkisen CASH BACK.</span>
                                </label>
                                <h4 class="green"> Mir will be deducted up to {{$cashbackPoints/$cashbackvalue}} of
                                    the
                                    order. </h4></div>
                        @else
                            <h4>Loggen Sie sich ein, um Gebrauch von Ihren Bonus- und Cashbackpunkten zu machen</h4>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="well _well"><br>
                            <div class="coupon-main-div">
                                <label class="radio-inline pull-left coupon-radio"><input type="radio"
                                                                                          name="offer_type"
                                                                                          id="coupon-radio"
                                                                                          value="have_coupon"><span></span></label>
                                <div class="pull-right"><a href="#"><i class="fa fa-question-circle fa-lg"></i></a>
                                </div>
                                <div class="Coupon-Code-main">
                                    <div class="input-group Coupon-Code">
                                        <span id="coupon-error" style="color:red;"></span>
                                        <input type="text" class="form-control input-lg" placeholder="Coupon Code"
                                               id="coupon-code" name="coupon_code">
                                        <span class="input-group-addon"><input class="btn" type="button"
                                                                               value="Redeem"
                                                                               id="coupon-button"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <label class="radio-inline pull-left"><input type="radio" checked name="offer_type"
                                                                         id="no-discount"
                                                                         value="no_discount"><span></span></label>
                            <div class="Coupon-Code-main">
                                <strong>Without discount order </strong><br>
                                No savings
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <div class="text-center"><a href="#">Schlieben</a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <h4 class="red">Wie möchten Sie bezahlen?</h4>
                <ul class="pay-type-list list-inline">
                    <li>
                        <figure>{!! Html::image('resources/assets/front/img/visa-icon.png','',['class'=>'img-responsive']) !!}</figure>
                        Visa
                        <span>{!! Html::image('resources/assets/front/img/service100.png','',['class'=>'img-responsive']) !!} </span>
                    </li>
                    <li>
                        <figure>{!! Html::image('resources/assets/front/img/paypal-icon.png','',['class'=>'img-responsive']) !!}</figure>
                        Visa
                        <span>{!! Html::image('resources/assets/front/img/service100.png','',['class'=>'img-responsive']) !!} </span>
                    </li>
                    <li>
                        <figure>{!! Html::image('resources/assets/front/img/master-icon.png','',['class'=>'img-responsive']) !!}</figure>
                        Visa
                        <span>{!! Html::image('resources/assets/front/img/service100.png','',['class'=>'img-responsive']) !!} </span>
                    </li>
                    <li>
                        <figure>{!! Html::image('resources/assets/front/img/eps-icon.png','',['class'=>'img-responsive']) !!}</figure>
                        Visa
                        <span>{!! Html::image('resources/assets/front/img/service100.png','',['class'=>'img-responsive']) !!} </span>
                    </li>
                    <li>
                        <figure>{!! Html::image('resources/assets/front/img/sofort-icon.png','',['class'=>'img-responsive']) !!}</figure>
                        Visa
                    </li>
                </ul>
                <div class="well">
                    <h4>You pay (with):</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <select class="form-control selectpicker" name="payment_method">
                                <option value="1">Barzahlung</option>
                                <option value="2">Onlinezahlung</option>
                            </select>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="restaurant_id" value="{{$restId}}" />

                {!! Form :: hidden('address','',['id'=>'address-id']) !!}

                {!! Form :: submit('Place order',['id'=>'place-order','class'=>'btn btn-success btn-lg']) !!}
                <span class="error" id="address-err" style="display:none;">Zuerst Adresse auswählen</span>
                <span class="error" id="ordertype-err" style="display:none;">Zuerst Bestellungstyp auswählen</span>

                {!! Form :: close() !!}


            </div>
            <div class="col-sm-4  col-md-4 col-lg-4">

                <div class="tabbing-side">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#Shopping" data-toggle="tab">Shopping </a></li>
                        <li><a href="#Help" data-toggle="tab">Help</a></li>
                    </ul>
                    @include('includes/cartsidebar')
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="Edit_address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md robotoregular">
            <form id="edit-address-form">
                <div class="modal-content">
                    <div class="modal-header" style="background:#81c02f; color:#fff;">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                                    class="sr-only">Close</span></button>
                        <h4 class="modal-title"> Edit the shipping address </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-md-3">Name :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="edit-name"
                                                         name="edit_name"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Pin code :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="edit-pincode"
                                                         name="edit_pincode"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Address :</div>
                            <div class="col-md-9"><textarea class="form-control" id="edit-address"
                                                            name="edit_address"></textarea></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Landmark</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="edit-landmark"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Phone :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="edit-mobile"
                                                         name="edit_phone"></div>
                        </div>
                        <input type="hidden" id="edit-address-id" value="">
                        <input type="hidden" id="edit-user-id"
                               value="<?php if (Auth::check('front')) echo Auth::user('front')->id ?>">
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9"><input type="button" name="" id="add-form" class="btn btn-success"
                                                         value="Save" onclick="setAddressModal('edit')"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="Add_address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md robotoregular">
            <div class="modal-content">
                <div class="modal-header" style="background:#81c02f; color:#fff;">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                                class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Adresse hinzufügen </h4>
                </div>
                <form id="add-address-form">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-md-3">Name :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="add-name" name="add_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Adresse :</div>
                            <div class="col-md-9"><textarea class="form-control" id="add-address"
                                                            name="add_address"></textarea></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">PLZ :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="add-pincode"
                                                         name="add_pincode"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Bemerkung :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="add-landmark"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">Telefon :</div>
                            <div class="col-md-9"><input type="text" class="form-control" id="add-mobile"
                                                         name="add_mobile"></div>
                        </div>
                        <input type="hidden" id="add-user-id"
                               value="<?php if (Auth::check('front')) echo Auth::user('front')->id ?>">
                        <div class="form-group row">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <button type="button" class="btn btn-success" id="add-form"
                                        onclick="setAddressModal('add')">Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

{!! Html::script('resources/assets/front/js/jquery-2.2.0.min.js') !!}
<script type="text/javascript">

    $("#place-order").click(function (evt) {
        var address = $("#address-id").val();
        var orderType = $("#order-type").val();
        console.dir(orderType);
        evt.stopPropagation();
        return false;
        if (orderType) {
            $("#ordertype-err").attr("style", "");
            evt.stopPropagation();
            return false;
        }
        else if (orderType == '1' && (address == '' || address == null)) {
            $("#address-err").attr("style", "display:block");
            evt.stopPropagation();
            return false;
        }
        else {
            $("#address-err").attr("style", "display:none");
        }
    });

    $(document).ready(function () {
        $("#ckeckout").remove();
    });

    $(document).ready(function () {
        $('.add_add').click(function () {
            $('.add_add').parent('div').parent('div').parent('div').removeClass('bg-success');
            $(this).parent('div').parent('div').parent('div').addClass('bg-success');
        });

    });

    $(function () {
        updateCart();
        updateAddresses();
    });

    function updateCart() {
        $.ajax({
            url: "{{url('front/get_cart')}}",
            type: "get",
            dataType: "html",
            data: {
                "_token": "{{csrf_token()}}",
                "restaurant_id": "{{$restId}}",
                "editable": false
            },
            success: function (response) {
                $("#FrontShoppingCart").html(response);
            }
        });
    }

    function updateAddresses() {
        $.ajax({
            url: "{{url('front/addresses')}}",
            type: "get",
            dataType: "json",
            data: {
                "_token": "{{csrf_token()}}"
            },
            success: function (response) {
                console.log(response);
            }
        });
    }

</script>

<style>
    .add_box.bg-success {
        background-color: #dff0d8;
    }
</style>




