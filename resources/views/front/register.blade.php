@extends("front_new.layout")

@section("title")
Lieferzonas - Registrieren
@endsection

@section("head")
<style>

    .lie-contents {
        margin-top: 80px;
        width: 640px;
        min-width: 640px;
    }

    .lie-register-element {
        background-color: white;
        -webkit-box-shadow: 0px 0px 15px 6px rgba(0,0,0,0.1);
        -moz-box-shadow: 0px 0px 15px 6px rgba(0,0,0,0.1);
        box-shadow: 0px 0px 15px 6px rgba(0,0,0,0.1);
        margin-bottom: 40px;
    }

    .lie-register-element-header {
        font-size: 20px;
        border-bottom: 1px solid lightgrey;
        text-align: center;
        padding: 8px;
    }

    .lie-register-element-close {
        cursor: pointer;
    }

    .lie-register-element-body {
        text-align: left;
        padding: 20px;
    }

    .lie-register-page-button {
        background-color: #da4f4a;
        color: white;
        text-align: center;
        border: none;
        padding: 15px 20px;
    }

    .lie-register-page-input {
        width: 100%;
        height: 45px;
        margin-bottom: 10px;
        padding-left: 8px;
    }

    a,
    a:link,
    a:visited,
    a:focus,
    a:hover {
        color: #e16b69;
    }

</style>
@endsection

@section("contents")
<div class="lie-contents">

    <div class="lie-register-element" id="lie-app-element" {!! lie_tooltip("Bald verfügbar") !!}>
        <div class="lie-register-element-header">
            Download App
            <span class="pull-right lie-register-element-close" onclick="$('#lie-app-element').hide()"><i class="fa fa-times"></i></span>
        </div>
        <div class="lie-register-element-body">
            <table width="100%">
                <tbody>
                <tr>
                    <td style="text-align: right">
                        <button type="button" class="lie-register-page-button" disabled>
                            <i class="fa fa-apple"></i> &nbsp; IPHONE APP
                        </button>
                    </td>
                    <td style="text-align: center">
                        <button type="button" class="lie-register-page-button" disabled>
                            <i class="fa fa-android"></i> &nbsp; ANDROID APP
                        </button>
                    </td>
                    <td style="text-align: left">
                        <button type="button" class="lie-register-page-button" disabled>
                            <i class="fa fa-windows"></i> &nbsp; WINDOWS APP
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
            Oder geben Sie Ihre Handynummer ein und wir werden Ihnen einen Download-Link senden.<br>
            <br>
            <table width="100%">
                <tbody>
                <tr>
                    <td width="67%"><input type="text" class="lie-register-page-input" placeholder="Handynummer" style="margin-top: 10px; height: 50px;"></td>
                    <td width="3%"></td>
                    <td width="30%"><button type="button" class="lie-register-page-button" disabled>LINK ERHALTEN</button></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Register User -->
    <div class="lie-register-element" id="lie-register-user-element">
        <div class="lie-register-element-header">
            Registriere dich bei <b>Lieferzonas.at</b>
            <span class="pull-right lie-register-element-close" onclick="$('#lie-register-user-element').hide()"><i class="fa fa-times"></i></span>
        </div>
        <div class="lie-register-element-body">
            <table width="100%">
                <tbody>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Vorname"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Nachname"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Telefonnummer"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="text" class="lie-register-page-input" placeholder="E-Mail Adresse"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="password" class="lie-register-page-input" placeholder="Passwort" id="lie-user-password-input"></td>
                    <td width="25%"></td>
                </tr>
                </tbody>
            </table>

            <div style="width: 100%; text-align: center;">
                <a href="javascript:void(0)" id="lie-show-password-link">Passwort anzeigen</a>
                <a href="javascript:void(0)" id="lie-hide-password-link" style="display: none;">Passwort verbergen</a>
            </div>

            <div>
                <table>
                    <tbody>
                    <tr>
                        <td>{!! lie_general_checkbox("accept_terms_and_agreements", "", false, "lie-accept-terms") !!}</td>
                        <td>Ich habe die AGB und die Datenschutzbestimmungen von Lieferzonas.at gelesen und akzeptiert.</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div id="lie-register-user-separator">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td><hr></td>
                        <td width="1%"> &nbsp; oder &nbsp; </td>
                        <td><hr></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <table width="100%">
                <tbody>
                <tr>
                    <td width="32%"><button type="button" class="lie-register-page-button" style="background-color: #3b5999; width: 100%;"><i class="fa fa-facebook"></i></button></td>
                    <td width="32%"><button type="button" class="lie-register-page-button" style="background-color: #55acef; width: 100%;"><i class="fa fa-twitter"></i></button></td>
                    <td width="32%"><button type="button" class="lie-register-page-button" style="background-color: #db4838; width: 100%;"><i class="fa fa-google-plus"></i></button></td>
                </tr>
                </tbody>
            </table>

            <br>

            <table width="100%">
                <tbody>
                <tr>
                    <td width="32%"></td>
                    <td width="32%">
                        <button type="button" class="lie-register-page-button" id="lie-user-submit-button" style="width: 100%;">
                            REGISTRIEREN
                        </button>
                    </td>
                    <td width="32%"></td>
                </tr>
                </tbody>
            </table>

            <br>

            <div style="text-align: center">
                Du hast schon einen Account? Jetzt <a href="/frontuser/login">Einloggen</a>
            </div>

        </div>
    </div>

    <!-- Register Restaurant -->
    <div class="lie-register-element" id="lie-register-restaurant-element">
        <div class="lie-register-element-header">
            Registriere dich bei <b>Lieferzonas.at</b>
            <span class="pull-right lie-register-element-close" onclick="$('#lie-register-restaurant-element').hide()"><i class="fa fa-times"></i></span>
        </div>
        <div class="lie-register-element-body">
            <table width="100%">
                <tbody>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Name des Zustelldienst"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Strasse"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Hausnummer"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="text" class="lie-register-page-input" placeholder="Postleitzahl / Ort"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="password" class="lie-register-page-input" placeholder="E-Mail Adresse"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="password" class="lie-register-page-input" placeholder="Vorname"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="password" class="lie-register-page-input" placeholder="Nachname"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="password" class="lie-register-page-input" placeholder="Handynummer"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="password" class="lie-register-page-input" placeholder="Telefon"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%"><input type="password" class="lie-register-page-input" placeholder="Küchenart"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%">{!! lie_general_checkbox("not_a_robot", "Ich bin kein Roboter") !!}</td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="50%" height="55"><textarea placeholder="Info" style="width: 100%" rows="5"></textarea></td>
                    <td width="25%"></td>
                </tr>
                </tbody>
            </table>

            <br>
            <br>

            <table width="100%">
                <tbody>
                <tr>
                    <td width="32%"></td>
                    <td width="32%">
                        <button type="button" class="lie-register-page-button" id="lie-user-submit-button" style="width: 100%;">
                            REGISTRIEREN
                        </button>
                    </td>
                    <td width="32%"></td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>

</div>
@endsection

@section("scripts")
<script>

</script>
@endsection