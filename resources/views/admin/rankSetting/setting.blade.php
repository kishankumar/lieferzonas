@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')

    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                <h3 class="text-success"> Rank Setting </h3>
                <hr>
                <?php 
                    $class='<i class="fa fa-caret-down text-muted"></i>';
                    $class1='<i class="fa fa-caret-up text-muted"></i>';
                    $class2='<i class="fa fa-sort text-muted"></i>';
                ?>
            </div>
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                {!! Form::open(array('route'=>'rzone.rank.setting.index','id'=>'searchingForm','class'=>'form-horizontal','method'=>'GET')) !!}
                    <div class="col-md-12 form-group">
                        <label class="col-sm-2 control-label">Select Week</label>
                        <div class="col-sm-4">
                            <select class="form-control" id="week" name="week" onChange="hideErrorMsg('weekErr')">
                                <option value="">Select</option>
                                @foreach($weeks as $key=>$value)
                                    @if($key==$week_id)
                                        <option value="{{$key}}" selected >{{$value}}</option>
                                    @else
                                        <option value="{{$key}}" >{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span id="weekErr" class="text-danger"></span>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-success" type="button" name="getRank" onclick="validate_rest()">Get Ranks</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                {!! Form::close() !!}
                
                @if($week_id == 1 || $week_id == 2 || $admin_count > 0)
                    <table class="table table-new table-c">
                            <thead class="bg-danger">   
                            <tr>
                                <td>Zip Codes\Rank</td>
                                    <?php
                                        for($i=1; $i<=8; $i++){
                                            $priceVal='';
                                            if(count($sel_prices)){
                                                foreach($sel_prices as $priceInfo){
                                                    if($priceInfo->rank==$i){
                                                        $priceVal = $priceInfo->price;
                                                    }
                                                }
                                            }?>
                                    <td><?=$i?>(<?=(($priceVal)?'&euro; '.$priceVal:'0');?>)
                                        <input type="hidden" id="price<?=$i;?>" value="<?=$priceVal;?>" name="rank1<?=$i?>">
                                    </td>
                                    <?php } ?>
                                    <td>No Selection</td>
                                    <td>Total Price</td>
                                </tr>
                        </thead>
                        <tbody>
                                <?php
                                if(count($restZipCodeMaps)){
                                    foreach($restZipCodeMaps as $info){ ?>
                                        <tr>
                                            <td>{{ ($info->name) }}</td>
                                            <?php
                                            $Zchecked = 0;
                                            for($i=1; $i<=8; $i++){
                                                $isZExist=0;
                                                if(count($sel_ranks)){
                                                    foreach($sel_ranks as $rankInfo){
                                                        if($rankInfo->zipcode_id==$info->id && $rankInfo->rank==$i && $rankInfo->rest_id==$curr_rest_id){
                                                            $Zchecked = 1;
                                                            break;
                                                        }else if($rankInfo->zipcode_id==$info->id && $rankInfo->rank==$i && $rankInfo->rest_id != $curr_rest_id){
                                                            $Zchecked = 0;
                                                            $isZExist=1;
                                                            break;
                                                        }else{
                                                            $Zchecked = 0;
                                                        }
                                                    }
                                                }
                                                if($isZExist){
                                                ?>
                                                    <td>
                                                        <label>-</label>
                                                    </td>
                                                <?php } else {
                                                    if($Zchecked){ ?>
                                                        <td><label>Selected<span></span></label></td>
                                                    <?php }else{ ?>
                                                        <td><label>No<span></span></label></td>
                                                    <?php } 
                                                }
                                            }?>
                                            
                                            <?php
                                            $counter=0;$rankPriceVal=0;$NoChecked = 1;
                                            for($i=1; $i<=8; $i++){
                                                if($counter)
                                                    break;
                                                if(count($sel_ranks)){
                                                    foreach($sel_ranks as $rankInfo){
                                                        if($rankInfo->zipcode_id==$info->id && $rankInfo->rank==$i && $rankInfo->rest_id==$curr_rest_id){
                                                            $NoChecked = 0;
                                                            $rankPriceVal = $rankInfo->price;
                                                            $counter=1;
                                                            break;
                                                        }else{
                                                            $rankPriceVal=0;
                                                            $NoChecked = 1;
                                                        }
                                                    }
                                                }
                                            }
                                            if($NoChecked){ ?>
                                                <td><label>No<span></span></label></td>
                                            <?php }else{ ?>
                                                <td><label>Yes<span></span></label></td>
                                            <?php } ?>
                                            <td>&euro; <label><?=$rankPriceVal?></label></td>
                                        </tr>
                                <?php }
                                } else { ?>
                                        <tr>
                                            <td colspan="11" style="text-align: center">No Zip Code Exist</td>
                                        </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                @endif
                
                @if($week_id == 3 && $admin_count<1)
                        {!! Form::open(array('route'=>'rzone.rank.setting.store','id'=>'settingForm','class'=>'form-horizontal')) !!}
                    <div>
                        <input type="hidden" id="price0" value="0" name="rank0">
                        <input type="hidden" id="week_id" value="<?=$week_id?>" name="week_id">
                        <table class="table table-new table-c">
                            <thead class="bg-danger">
                                <tr>
                                    <td>Zip Codes\Rank</td>
                                    <?php
                                        for($i=1; $i<=8; $i++){
                                            $priceVal='';
                                            if(count($sel_prices)){
                                                foreach($sel_prices as $priceInfo){
                                                    if($priceInfo->rank==$i){
                                                        $priceVal = $priceInfo->price;
                                                    }
                                                }
                                            }?>
                                    <td><?=$i?>(<?=(($priceVal)?'&euro; '.$priceVal:'0');?>)
                                        <input type="hidden" id="price<?=$i;?>" value="<?=$priceVal;?>" name="rank1<?=$i?>">
                                    </td>
                                    <?php } ?>
                                    <td>No Selection</td>
                                    <td>Total Price</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(count($restZipCodeMaps)){
                                    foreach($restZipCodeMaps as $info){ ?>
                                        <tr>
                                            <td>{{ ($info->name) }}</td>
                                            <?php
                                            $checked = 'checked';
                                            for($i=1; $i<=8; $i++){
                                                $priceVal=0;
                                                if(count($sel_prices)){
                                                    foreach($sel_prices as $priceInfo){
                                                        if($priceInfo->rank==$i){
                                                            $priceVal = $priceInfo->price;
                                                        }
                                                    }
                                                }
                                                $isExist=0;
                                                if(count($sel_ranks)){
                                                    foreach($sel_ranks as $rankInfo){
                                                        if($rankInfo->zipcode_id==$info->id && $rankInfo->rank==$i && $rankInfo->rest_id==$curr_rest_id){
                                                            $checked = 'checked';
                                                            break;
                                                        }else if($rankInfo->zipcode_id==$info->id && $rankInfo->rank==$i && $rankInfo->rest_id != $curr_rest_id){
                                                            $checked = '';
                                                            $isExist=1;
                                                            break;
                                                        }else{
                                                            $checked = '';
                                                        }
                                                    }
                                                }
                                                if($isExist){
                                                ?>
                                                    <td>
                                                        <label>-</label>
                                                    </td>
                                                <?php } else { ?>
                                                    <td>
                                                        <label>
                                                            <input type="radio" <?=$checked;?> id="price-<?=$info->id;?>-<?=$i;?>" class="rankPrice" value="<?=$info->id;?>,<?=$i;?>" name="rank[<?=$info->id?>]">
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                <?php }
                                            } ?>

                                            <?php 
                                            $counter=0;$rankPriceVal=0;$NoChecked = 'checked';
                                                for($i=1; $i<=8; $i++){
                                                    if($counter)
                                                        break;
                                                    if(count($sel_ranks)){
                                                        foreach($sel_ranks as $rankInfo){
                                                            if($rankInfo->zipcode_id==$info->id && $rankInfo->rank==$i && $rankInfo->rest_id==$curr_rest_id){
                                                                $NoChecked = '';
                                                                $rankPriceVal = $rankInfo->price;
                                                                $counter=1;
                                                                break;
                                                            }else{
                                                                $rankPriceVal=0;
                                                                $NoChecked = 'checked';
                                                            }
                                                        }
                                                    }
                                                }
                                            ?>
                                            <td><label><input type="radio" <?=$NoChecked;?> id="price-<?=$info->id?>-0" value="<?=$info->id?>,0" class="rankPrice" name="rank[<?=$info->id?>]" ><span></span></label></td>
                                            <td>&euro; <label id="PriceVal<?=$info->id?>"><?=$rankPriceVal?></label></td>
                                        </tr>
                                <?php    }
                                    } else { ?>
                                        <tr>
                                            <td colspan="11" style="text-align: center">No Zip Code Exist</td>
                                        </tr>
                               <?php } ?>
                            </tbody>
                        </table>
                        <?php
                            if(count($restZipCodeMaps)){    ?>      
                                <div class="col-md-12 form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-success" type="submit" onclick="confirm('Once you set the rank price you cann\'t set again. Are you sure to set rank ?')">Update</button>
                                    </div>
                                </div>
                        <?php } ?>
                    </div>
                        {!! Form::close() !!}
                @endif
            
        </div>
    </div>
@endsection 
      
@section('script')
<script>
    function hideErrorMsg(id){
        $('#'+id).attr('style', 'display:none');
    }
    function validate_rest()
    {
        var week = $('#week').val();
        if(week=='')
        {
            $("#week").focus();
            $("#weekErr").html('Please select week');
            $("#weekErr").show();
            return false;
        }
        document.forms["searchingForm"].submit();
    }
    $(function(){
        $('.rankPrice').change(function(){
            var str=$(this).attr('id');
            var res = str.split("-");
            var zipId = res[1];
            var rankId = res[2];
            var priceVal = $('#price'+rankId).val();
            $('#PriceVal'+zipId).html(priceVal);
        });
    });
    
    $(window).on("scroll resize load", function () {
        document.getElementById('left_side').style.height = "";
        var lefttHeight = document.getElementById('left_side').clientHeight;
        var rightHeight = document.getElementById('right_side').clientHeight;
        if(lefttHeight < rightHeight){
            document.getElementById('left_side').style.height = rightHeight+25+"px";
        }
        else{
            document.getElementById('left_side').style.height = lefttHeight+25+"px";
        }
    });
</script>
@endsection
