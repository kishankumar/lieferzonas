<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin | Log in </title>
    {!! Html::style('resources/assets/admin/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/admin/css/font-awesome.css') !!}
    {!! Html::style('resources/assets/admin/css/style.css') !!}
</head>
<body>

</body>
    <div class="login_section">
        <div class="login flipInY animated">
			
                <section>
                    <figure class="logo_icon bounceInDown animated">
                        {!!Html::image('resources/assets/admin/img/logo-icon.png','','')!!}
                    </figure>
                    <figure class="text-center">
					{!!Html::image('resources/assets/admin/img/login-logo.png','','')!!}
                    </figure>
                    
                    {!! Form::open(['url'=>'authadmin/login','method'=>'post','id'=>'login_form']) !!}
                    <!--<form action="{{ action("Auth\AdminAuthController@getLogin") }}" method="POST">-->
                    <!--{!! csrf_field() !!}-->
                        <hr>
                        
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div id="loginID">
							{!! Form::text('email','',['class'=>'form-control','placeholder'=>'Email']) !!}
							{!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
							<div class="text-center">
								{!! Form::submit('Login',['id'=>'login','class'=>'btn btn-success']) !!}
							
								<h4><a href="javascript:void(0)" id="myModal_forger">Forgot your password ?</a><br></h4>
                        	</div>
						</div>
						<div id="forgotID" style="display:none">
							
							<h4 class="modal-title">Hast Du Dein Passwort vergessen?</h4>
								{!! Form::open(['url'=>'front/forgotpass','method'=>'post','id'=>'forgot_pwd','name'=>'forgot_pwd']) !!}
								
									
									{!! Form::text('semail','',["id"=>'femail',"class"=>"form-control modal-input","placeholder"=>"Email-Adresse","onkeypress"=>"hideErrorMsg('emailerr')","onkeyup"=>"hideErrorMsg('err')"]) !!}
									  <span id="emailerr" class="text-danger"></span>
									
									{!! Form::text('smobile','',["id"=>'fmobile',"class"=>"form-control modal-input","placeholder"=>"Mobile No.","onkeypress"=>"hideErrorMsg('mobileerr')","onkeyup"=>"hideErrorMsg('err')"]) !!}
									  
									{!! Form::text('scustomerid','',["id"=>'fcustomerid',"class"=>"form-control modal-input","placeholder"=>"Customer Id","onkeypress"=>"hideErrorMsg('customeriderr')","onkeyup"=>"hideErrorMsg('err')"]) !!}
									  <span id="customeriderr" class="text-danger"></span>
									  <span id="err" class="text-danger"></span>
									{!!Form::button('Neues Passwort anfordern',["id"=>'forgotpass',"class"=>"btn btn-success btn-block brn-rg", "style"=>"text-transform:none; width:auto;","onclick"=>"forgot_pass()"]) !!}

									

							   {!! Form::close() !!}
							
							<div class="text-center">
								<h4><a href="javascript:void(0)" id="myModal_back">Back to login</a><br></h4>
                        	</div>
						</div>
						
                        <!--</form>-->
                    {!! Form::close() !!}
                </section>
            </div>
            <div class="copy">Copyright &copy; 2016 Lieferzonas.at</div>
    </div>


	
	
<style>
    html, body, .login_section{height: 100%;}	
</style>	

{!! Html::script('resources/assets/admin/js/jQuery-2.2.0.min.js') !!}
{!! Html::script('resources/assets/admin/js/jquery.min.js') !!}
{!! Html::script('resources/assets/admin/js/bootstrap.js') !!}
{!! Html::script('resources/assets/admin/js/jquery.validate.js') !!}
{!! Html::script('resources/assets/admin/js/additional-methods.js') !!}
</html>

<script>

 function forgot_pass()
		{
			var email = $('#femail').val();
			var mobile = $('#fmobile').val();
			var customerid = $('#fcustomerid').val();
			if(email!='')
			{
				if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))) 
				{
					$("#femail").focus();
					$("#emailerr").html('Invalid email id. Please enter valid email');
					$("#emailerr").show();
					return false;
				}
			}
			
			if(isNaN(mobile)){
					
					$("#mobile").focus();
					$("#mobileerr").html('Please enter numeric value for mobile');
				    $("#mobileerr").show();
					return false;
			}
			if(isNaN(customerid)){
					
					$("#customerid").focus();
					$("#customeriderr").html('Please enter numeric value for customer id');
				    $("#customeriderr").show();
					return false;
			}
			if(email=='' && mobile=='' && customerid=='')
			{
				    $("#err").html('Please enter atleast one field');
				    $("#err").show();
					return false;
			}

			  $.ajax({
                url: '{{url('sendmail')}}',
                type: "post",
                dataType:"json",
                data: {'email':email,'mobile':mobile,'customerid':customerid, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                    if(res.success==1)
                    {
                        alert('A link is sent to your registered email successfully!');
                    }
                    else
                    {
                        alert('Credential is not correct!');
                    }
                }
            });
			
			 //document.forms["forgot_pwd"].submit();
		}
    $(document).ready(function(){
        $("#login_form").validate({
            rules: {
                email: {
                    required: true,
                    email:true
                },
                password: {
                    required : true,
                    minlength:3,
                },
            },
            messages: {
                email:{
                    required: "Please enter email",
                    email: "Please enter valid email",
                },
                password: {
                    required: "Please enter password",
                    minlength: "Password must be atleast 3 letters",
                },
            }
        });
    });
	/*$(".form-control modal-input").keyup(function(){
		alert('hshs');
     $('#err').attr('style', 'display:none');
    });*/
	
	function hideErrorMsg(id){
             //alert(id)
			  $('#'+id).attr('style', 'display:none');
		  
			
			 }
</script>
<script>
	
	var forgotID = document.getElementById('forgotID');
	var loginID = document.getElementById('loginID');
	var myModal_forger = document.getElementById('myModal_forger');
	
	myModal_forger.onclick = function(){
	  	loginID.style="display:none"
		forgotID.style="display:block"
	}
	
	myModal_back.onclick = function(){
	  	loginID.style="display:block"
		forgotID.style="display:none"
	}
	
		
	  
	  
</script>


<style>
	.alert{padding: 5px;}
	.alert ul{padding-left: 20px;}
</style>