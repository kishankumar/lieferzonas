@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')
<div class="col-sm-9">
  <div id="right_side">
	<div class="panel-body">
		<ul class="nav nav-tabs">
		  <li class="active"><a aria-expanded="false" href="#today" data-toggle="tab">Today</a></li>
		  <li class=""><a aria-expanded="true" href="#weekly" data-toggle="tab">Weekly</a></li>
		  <li class=""><a href="#monthly" data-toggle="tab">Monthly</a> </li>
		</ul>
	</div>
	<div id="myTabContent" class="tab-content">
		
	  <div class="tab-pane fade active in" id="today">
		  <?php 
			$auth_id = Auth::User('admin')->id;
			$today_grand_totalsale = \App\front\OrderPayment::totalsale_today($auth_id); 
			$today_grand_total = 0;
			foreach($today_grand_totalsale as $today_grand_totalsales)
			{
			  $today_grand_total = $today_grand_total + (int)$today_grand_totalsales->grand_total;
			}
			?>
			  <table class="table table-new table-c">
			  <thead class="bg-danger"><tr><th>Total No. Of Order</th> <th>Total Sales Amount</th></tr></thead>
			  <tbody>
			  @if(count($today_grand_totalsale))
			  <tr><td>{{ count($today_grand_totalsale) }}</td><td>{{ $today_grand_total }} €</td></tr>
			  @else
			  <tr><td colspan="8" align="center"> No Record Exist</td> </tr>
			  @endif
			  </tbody>
			  </table>
	   </div>
	  <div class="tab-pane fade " id="weekly">
		  <?php 
			
			$week_grand_totalsale = \App\front\OrderPayment::totalsale_weekly($auth_id); 
			$week_grand_total = 0;
			foreach($week_grand_totalsale as $week_grand_totalsales)
			{
			  $week_grand_total = $week_grand_total + (int)$week_grand_totalsales->grand_total;
			}
		 	?>
	      <table class="table table-new table-c">
			  <thead class="bg-danger"><tr><th>Total No. Of Order</th> <th>Total Sales Amount</th></tr></thead>
			  <tbody>
			  @if(count($week_grand_totalsale))
			  <tr><td>{{ count($week_grand_totalsale) }}</td><td>{{$week_grand_total}} €</td></tr>
			  @else
			  <tr><td colspan="8" align="center"> No Record Exist</td> </tr>
			  @endif
			  </tbody>
		  </table>
	  </div>
	  
	  <div class="tab-pane fade" id="monthly">
	  <?php 
		
		$month_grand_totalsale = \App\front\OrderPayment::totalsale_monthly($auth_id); 
		$month_grand_total = 0;
		foreach($month_grand_totalsale as $month_grand_totalsales)
		{
		  $month_grand_total = $month_grand_total + (int)$month_grand_totalsales->grand_total;
		}
		?>
	  <table class="table table-new table-c">
	  <thead class="bg-danger"><tr><th>Total No. Of Order</th> <th>Total Sales Amount</th></tr></thead>
	  <tbody>
	  @if(count($month_grand_totalsale))
	  <tr><td>{{ count($month_grand_totalsale) }} </td><td>{{ $month_grand_total }} €</td></tr>
	  @else
	  <tr><td colspan="8" align="center"> No Record Exist</td> </tr>
	  @endif
	  </tbody>
	  </table>
	  </div>
	 
	</div> 
  </div>	
</div>

@endsection

@section('script')

@endsection
