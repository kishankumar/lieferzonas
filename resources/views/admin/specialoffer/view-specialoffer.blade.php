@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
   
		<div class="btn-group pull-right">
			 <a  href="{{ url('rzone/specialoffer/create') }}" class="btn btn-success DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
				 <span>Add</span>
			 </a>
			 <a  href="javascript:window.print()" class="btn btn-warning DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
				 <span>Print</span>
			 </a>
			 <a  href="#" class="btn btn-danger DTTT_button_print" onclick="download_offers()" title="View print view" tabindex="0" aria-controls="simpledatatable" >
				 <span >Download</span>
			 </a>
		</div>
      @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
               <table id="example1" class="table table-bordered table-new table-c">
                <thead class="bg-danger">
                    <tr>
                        <th>Sr.</th>
						<th>Special offer name</th>
                        <th>Restaurant</th>
                        <th>Discount type</th>
                        <th>Valid from</th>
                        <th>Valid to</th>
                        <th>Discount Type Value</th>
                        <th>Original Price</th>
                        <th>Discount price</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>View</th>
                        
                </thead>
                <tbody>
                    
                @if(count($specialoffers))
                    @foreach($specialoffers as $specialoffer)
                <tr>
                    <td>
                        <div class="icheck">
                            <label class="checkbox">
                                {!! Form::checkbox('offer_id',$specialoffer['id'], null, ['class' => 'offers']) !!}
                                <span class="button-checkbox"></span>
                            </label>
                        </div>
                    </td>
					<td>{{ $specialoffer['name']}}</td>
                    <td>{{ $specialoffer['f_name'].' '.$specialoffer['l_name'] }}</td>
                    <td>@if( $specialoffer['discount_type_id']=='1'){{ 'Fixed'  }} @endif @if( $specialoffer['discount_type_id']=='2'){{ 'Percentage'  }}  @endif  @if( $specialoffer['discount_type_id']=='3') {{ 'One free on purchase'  }}  @endif </td>
                     <td>{{  date("d M Y", strtotime($specialoffer['valid_from']))  }}</td>
                    <td>{{  date("d M Y", strtotime($specialoffer['valid_to'])) }}</td>
                    
                    <td>{{ $specialoffer['discount_type_value'] }}</td>
                    <td>{{ $specialoffer['original_price'] }}</td>
                    <td>{{ $specialoffer['discount_price'] }}</td>
                   
                    <td> @if($specialoffer['status']==1) {{ 'Active' }}@endif  @if($specialoffer['status']==0) {{ 'Pending' }} 
                    @endif</td>
                    <td class="text-center">
                        @if($specialoffer['status']==0) <a class="btn btn-danger" href="{{url('rzone/specialoffer/'.$specialoffer['id'].'/edit')}}"> Edit @endif
                        
                        </a>
                    </td>

                    <td>
                         <a class="btn btn-success" href="{{url('rzone/specialoffer/details/'.$specialoffer['id'])}}"> View </a>
                    </td>
                
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
                 {!! $specialoffers->render() !!} 
  
</div>
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
</div>

	
@endsection

@section('script')
  <script>
/*added by Rajlakshmi(01-06-16)*/					

	function download_offers() 
	{
		var doc = new jsPDF();
		var specialElementHandlers = {
		'#editor': function (element, renderer) {
			return true;
		}
       };
	     var len = $(".offers:checked").length;
        if(len==0){
            alert('Please select atleast one offer');
            return false;
        }
        var base_host = window.location.origin;  //give http://localhost
        //var base_url=base_host+'/delete';
        //var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
        var r = confirm("Are you sure to download offers?");
        if (r == true) {
            var selectedOffers = new Array();
            $('input:checkbox[name="offer_id"]:checked').each(function(){
                selectedOffers.push($(this).val());
            });
            $.ajax({
                url: 'specialoffer/download',
                type: "post",
                dataType:"json",
                data: {'selectedOffers':selectedOffers, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                     
                    
                      $("#download").html(res.data1);
					 
                        var doc = new jsPDF('p', 'pt');
						var elem = document.getElementById("table1");
						var res = doc.autoTableHtmlToJson(elem);
						doc.autoTable(res.columns, res.data);

				doc.save('specialoffer_list.pdf');
                    
                }
            });
        } else {
            return false;
        }
		
	}
   
         /*added by Rajlakshmi(01-06-16)*/
    </script>
@endsection
