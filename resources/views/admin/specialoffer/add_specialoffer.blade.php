@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
    <div class="panel-body">
    
                @if(Session::has('message'))
                <div style="color:red;">
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
				</div>
                @endif
                
                @if($errors->any())
                    @foreach( $errors->all()  as $error)
                     <li style="color:red;"> <b>{{ $error }}</b></li>
                    @endforeach
                @endif
                
{!! Form::open(array('route' => 'rzone.specialoffer.store','id'=>'specialoffer_form','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->
    <div class="modal-body" style="padding: 5px;">
        
        <div class="panel-body">
            <div class="errors" id="errors" style="color: red"></div>
            <div class="row">
               
				
                <div class="col-md-10">
                    <div class="form-group">
                    {!! Form::label('Name') !!}<span class="red">*</span>
                    {!! Form::text('name','',["id"=>'name',"class"=>"form-control","onkeypress"=>"hideErrorMsg('nameerr')"]) !!}
                     <span id="nameerr" class="text-danger"></span>
                    </div>

                </div>
               
            </div>
            <div class="row">
                
				<div class="col-md-10">
                    <div class="form-group">
                    {!! Form::label('Discount type') !!}
                    <?php
                     foreach($dis_types as $dis_type) { ?>
                    
						<label class="radiobox">
						<input type="radio" name='discount_type' value="{{ $dis_type['id']}}" 
						id="discount_type" <?php if($dis_type['id']==1) { echo 'checked'; }?>
						onclick="show('<?= $dis_type['name']?>')"> 
						<span>{{ $dis_type['name']}}</span></label>
					
                    <?php
                              } ?> 
					{!! Form::text('discount','',["id"=>'discount',"class"=>"form-control","placeholder"=>"Fixed","onkeypress"=>"hideErrorMsg('discounterr')"] )!!}
                    {!! Form::hidden('rest_id',$rest_id['0']['rest_detail_id'],["id"=>'discount',"class"=>"form-control"])!!}
                    <span id="discounterr" class="text-danger"></span>
                    </div>
                      
                </div>
				
            
               
            </div>
			<div class="clearfix"></div>
			<div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                   
                        <label class="radiobox"><input type="radio" name='type' value="1" class='cat' checked> <span>Category</span></label>
						<label class="radiobox"><input type="radio" name='type' value="2" class='menu'> <span>Menu</span></label>
					    <label class="radiobox"><input type="radio" name='type' value="3" class='submenu'> <span>Submenu</span></label>
                    </div>
				<div class="form-group col-sm-3" id="cat">
                    <select class="form-control" name="category" id="category" 
					onchange="getDatatbl(this.value,'rest_categories','rest_menus','menus','id','name','id','rest_category_id','caterr')"  >
                    <option value=''>Select</option>    
						      <?php
                                foreach($categories as $category) { ?>

                                    <option value="<?=$category['id'] ?>" name="<?=$category['name'] ?>"><?=$category['name'] ?></option>
                              <?php
                              } ?>
                    </select>
                    <span id="caterr" class="text-danger"></span>
                </div>
                <div class="form-group col-sm-3" id="menu" style='display:none;'>
                    <select class="form-control" name="menu" id="menus" onchange="getDatatbl(this.value,'rest_menus','rest_sub_menus','submenus','id','name','id','menu_id','menuerr')" >
                      
						     
                    </select>
                    <span id="menuerr" class="text-danger"></span>
                </div>
				<div class="form-group col-sm-3" id="submenu" style='display:none;'>
                    <select class="form-control" name="submenu" id="submenus" onchange="hideErrorMsg(submenuerr);">
                     
						      
                             
                    </select>
                  <span id="submenuerr" class="text-danger"></span>
                    
                </div>

                <div class="form-group col-sm-3">
                    <a class="btn  btn-primary"style="border:none;" onclick='addcatmenu();'>Add</a>
                </div>

                </div>
				
            
               
            </div>
			<table class="table table-new text-center table-c " id='table'>
								<thead>
									<tr class="bg-danger">
										<th width="21%">Category</th>
										<th width="16%">Menu</th>
										<th width="21%">Submenus</th>
										<th width="16%"></th>
										
									</tr>
								</thead>
								<tbody>
								
								</tbody>
			</table>
			  <span id="listerr" class="text-danger"></span>
			<div class="row">
               
				
                <div class="col-md-10">
                    <div class="form-group">
                    {!! Form::label('Order Service') !!}<span class="red">*</span>
                    <?php
                     foreach($order_services as $order_service) { 
                         
                         ?>
						<label class="radiobox">
						<input type="checkbox" name='order_service[]'  value="{{ $order_service['id'] }}" 
						<?php if($order_service['id']=='1') { echo 'checked'; } ?> onclick="hideErrorMsg('order_serviceerr');" > 
						<span>{{ $order_service['name']}}</span></label>
					
                    <?php
                    } ?>  
                    <span id="order_serviceerr" class="text-danger"></span>
                      
                    </div>

                </div>
               
            </div>
			
			<div class="row">
            
                        
                <div class="form-group col-sm-5">
                      <label class="radiobox"><input type="radio" name="validity" value="T" id ="validity" class="val_table" > <span>Validity table</span></label>
                </div>
                <div class="form-group col-sm-5">
                            <label class="radiobox"><input type="radio" name="validity" value="C" id ="validity" class="val_date" checked > <span>Date validity</span></label>
                </div>
                
                <div class="clearfix"></div>
                <div class="form-group col-sm-10" id="select_s" style="display:none;">
                    <select class="form-control" name="validity_table" id="validity_table" onchange="hideErrorMsg('validity_tableerr')">
                    <option value=''>Select</option>    
						      <?php
                                foreach($time_validations as $time_validation) { ?>

                                    <option value="<?=$time_validation['id'] ?>" ><?=$time_validation['name'] ?></option>
                              <?php
                              } ?>
                    </select>
                    <span id="validity_tableerr" class="text-danger"></span>
                </div>
                <div class="form-group col-sm-12" id="date_s" style="display:block;">
                    <div class="row">
                        <div class="col-sm-6">
                          <div class="input-group date " id="datetimepicker8">
                            <input type="text" class="form-control" name="valid_from" id="valid_from" placeholder="Valid from" onClick="hideErrorMsg('valid_fromerr')">
                            <span class="input-group-addon"> 
                            <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                            <span id="valid_fromerr" class="text-danger"></span>
                    </div>
                    <div class="col-sm-6">
                          <div class="input-group date" id="datetimepicker7">
                            <input type="text" class="form-control" name="valid_to" id="valid_to" placeholder="Valid to"
                            ononClick="hideErrorMsg('valid_toerr')">
                            <span class="input-group-addon"> 
                            <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                            <span id="valid_toerr" class="text-danger"></span>
                    </div>
                    </div>
                </div>          
                        
            
				
            </div>
			<?php
			foreach($days as $day) { ?>
		
			<div class="row">
			<div class="col-sm-3">
			    <label class="checkbox checkbox-inline check_click">
				
				 
						<input class="days" name="days[]" type="checkbox" value="<?=$day['id'] ?>">
						<span><?=$day['day'] ?></span>
					
					
                </label>
			</div>
			<div id='time<?=$day['id'] ?>'>
				<div class="col-sm-3">
				    
					<div class="form-group">
						<label for="Start Time">Start Time</label>
						<select id="from<?=$day['id'] ?>_start_time" class="form-control" name="start_time<?=$day['id'] ?>[]">
						<option value="" selected="selected">Select</option>
						<option value="00:00 AM">00:00 AM</option>
						<option value="00:30 AM">00:30 AM</option>
						<option value="01:00 AM">01:00 AM</option>
						<option value="01:30 AM">01:30 AM</option>
						<option value="02:00 AM">02:00 AM</option>
						<option value="02:30 AM">02:30 AM</option>
						<option value="03:00 AM">03:00 AM</option>
						<option value="03:30 AM">03:30 AM</option>
						<option value="04:00 AM">04:00 AM</option>
						<option value="04:30 AM">04:30 AM</option>
						<option value="05:00 AM">05:00 AM</option>
						<option value="05:30 AM">05:30 AM</option>
						<option value="06:00 AM">06:00 AM</option>
						<option value="06:30 AM">06:30 AM</option>
						<option value="07:00 AM">07:00 AM</option>
						<option value="07:30 AM">07:30 AM</option>
						<option value="08:00 AM">08:00 AM</option>
						<option value="08:30 AM">08:30 AM</option>
						<option value="09:00 AM">09:00 AM</option>
						<option value="09:30 AM">09:30 AM</option>
						<option value="10:00 AM">10:00 AM</option>
						<option value="10:30 AM">10:30 AM</option>
						<option value="11:00 AM">11:00 AM</option>
						<option value="11:30 AM">11:30 AM</option>
						<option value="12:00 PM">12:00 PM</option>
						<option value="12:30 PM">12:30 PM</option>
						<option value="01:00 PM">01:00 PM</option>
						<option value="01:30 PM">01:30 PM</option>
						<option value="02:00 PM">02:00 PM</option>
						<option value="02:30 PM">02:30 PM</option>
						<option value="03:00 PM">03:00 PM</option>
						<option value="03:30 PM">03:30 PM</option>
						<option value="04:00 PM">04:00 PM</option>
						<option value="04:30 PM">04:30 PM</option>
						<option value="05:00 PM">05:00 PM</option>
						<option value="05:30 PM">05:30 PM</option>
						<option value="06:00 PM">06:00 PM</option>
						<option value="06:30 PM">06:30 PM</option>
						<option value="07:00 PM">07:00 PM</option>
						<option value="07:30 PM">07:30 PM</option>
						<option value="08:00 PM">08:00 PM</option>
						<option value="08:30 PM">08:30 PM</option>
						<option value="09:00 PM">09:00 PM</option>
						<option value="09:30 PM">09:30 PM</option>
						<option value="10:00 PM">10:00 PM</option>
						<option value="10:30 PM">10:30 PM</option>
						<option value="11:00 PM">11:00 PM</option>
						<option value="11:30 PM">11:30 PM</option>
						</select>
						<span id="time_fromerr<?=$day['id'] ?>" class="text-danger"></span>
					</div>
				</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label for="End Time">End Time</label>
							<select id="to<?=$day['id'] ?>_end_time" class="form-control" name="end_time<?=$day['id']?>[] ">
							<option value="" selected="selected">Select</option>
							<option value="00:30 AM" >00:30 AM</option>
							<option value="01:00 AM">01:00 AM</option>
							<option value="01:30 AM">01:30 AM</option>
							<option value="02:00 AM">02:00 AM</option>
							<option value="02:30 AM">02:30 AM</option>
							<option value="03:00 AM">03:00 AM</option>
							<option value="03:30 AM">03:30 AM</option>
							<option value="04:00 AM">04:00 AM</option>
							<option value="04:30 AM">04:30 AM</option>
							<option value="05:00 AM">05:00 AM</option>
							<option value="05:30 AM">05:30 AM</option>
							<option value="06:00 AM">06:00 AM</option>
							<option value="06:30 AM">06:30 AM</option>
							<option value="07:00 AM">07:00 AM</option>
							<option value="07:30 AM">07:30 AM</option>
							<option value="08:00 AM">08:00 AM</option>
							<option value="08:30 AM">08:30 AM</option>
							<option value="09:00 AM">09:00 AM</option>
							<option value="09:30 AM">09:30 AM</option>
							<option value="10:00 AM">10:00 AM</option>
							<option value="10:30 AM">10:30 AM</option>
							<option value="11:00 AM">11:00 AM</option>
							<option value="11:30 AM">11:30 AM</option>
							<option value="12:00 PM">12:00 PM</option>
							<option value="12:30 PM">12:30 PM</option>
							<option value="01:00 PM">01:00 PM</option>
							<option value="01:30 PM">01:30 PM</option>
							<option value="02:00 PM">02:00 PM</option>
							<option value="02:30 PM">02:30 PM</option>
							<option value="03:00 PM">03:00 PM</option>
							<option value="03:30 PM">03:30 PM</option>
							<option value="04:00 PM">04:00 PM</option>
							<option value="04:30 PM">04:30 PM</option>
							<option value="05:00 PM">05:00 PM</option>
							<option value="05:30 PM">05:30 PM</option>
							<option value="06:00 PM">06:00 PM</option>
							<option value="06:30 PM">06:30 PM</option>
							<option value="07:00 PM">07:00 PM</option>
							<option value="07:30 PM">07:30 PM</option>
							<option value="08:00 PM">08:00 PM</option>
							<option value="08:30 PM">08:30 PM</option>
							<option value="09:00 PM">09:00 PM</option>
							<option value="09:30 PM">09:30 PM</option>
							<option value="10:00 PM">10:00 PM</option>
							<option value="10:30 PM">10:30 PM</option>
							<option value="11:00 PM">11:00 PM</option>
							<option value="11:30 PM">11:30 PM</option>
							<option value="11:59 PM">11:59 PM</option>
							</select>
						    <span id="time_toerr<?=$day['id'] ?>" class="text-danger"></span>
						</div>
					</div>
			</div>	
					<div class="col-sm-3">
					<a href='#' onclick="showtime(<?=$day['id'] ?>)">Add more</a>
					</div>
			</div>
			
			<div class="row">
			<div  id='add<?=$day['id'] ?>'>
			
			
			
			
			
			</div>
			
			</div>
			
			
			
			
			
			 <?php
				  } ?>
			<div class="row">
               
				
                <div class="col-md-10">
                    <div class="form-group">
                    {!! Form::label('Original price') !!}<span class="red">*</span>
                    {!! Form::text('original_price','',["id"=>'original_price',"class"=>"form-control","onkeypress"=>"hideErrorMsg('original_priceerr')"]) !!}
                     <span id="original_priceerr" class="text-danger"></span>
                    </div>

                </div>
               
            </div>
			<div class="row">
               
				
                <div class="col-md-10">
                    <div class="form-group">
                    {!! Form::label('Discount price') !!}<span class="red">*</span>
                    {!! Form::text('discount_price','',["id"=>'discount_price',"class"=>"form-control","onkeypress"=>"hideErrorMsg('discount_priceerr')"]) !!}
                     <span id="discount_priceerr" class="text-danger"></span>
                    </div>

                </div>
               
            </div>
               
            
            <div class="form-group margin-top">
                  {!!Form::button('Add',["id"=>'add',"class"=>"btn  btn-primary","onclick"=>"validate_offer()"]) !!}
                <hr>
				<p><span class="red">*</span> - Required Fields.</p>
            </div>
        </div>
    </div>
            {!! Form::close() !!}
              
           
    </div>
</div>

@endsection

 @section('script')
 <script>
 $(document).ready(function() {
        $('.cat, .menu , .submenu').click(function(){
            if($('.cat').is(":checked")==true){
                 $('#cat').css({'display':'block'});
				 $('#menu').css({'display':'none'});
				 $('#submenu').css({'display':'none'});
            }
            if($('.menu').is(":checked")==true){
                 
				 $("#cat option[value='']").attr('selected','selected');
				 $('#cat').css({'display':'block'});
				 $('#menu').css({'display':'block'});
				 $('#submenu').css({'display':'none'});
            }
			if($('.submenu').is(":checked")==true){
                 $('#cat').css({'display':'block'});
				 $('#menu').css({'display':'block'});
				 $('#submenu').css({'display':'block'});
            }
    });
	
	
    });  
	function showtime(id)
	{
		
		 $('#time'+id).clone().appendTo('#add'+id).addClass('col-md-offset-3');
		
	}
	
	
	function show(name)
	{
		
		$('#discount').attr('placeholder', name);
		
	}
	function getDatatbl(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn,err,selectedOption=0) {
    
	$.ajax({

		url: '{{url('getdatatbl')}}',
        type: "get",
        data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,
        'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn,'selectedOption':selectedOption},
        
	    success: function (data) {
		   
			$('#'+appendId).html(data);
			
		},
	    error: function (jqXHR, textStatus, errorThrown) {
	      
	    }
	});
	 //alert(err);
	 $('#'+err).attr('style', 'display:none');
	
}


function addcatmenu()
{
  $("#listerr").hide();
  var type =  $('input[name=type]:checked').val()
 
  if(type=='1')
  {
     var cat_id = $('#category').val();
     if(cat_id=='')
     {
     	            $("#cat").focus();
                    $("#caterr").html('Please select category');
                    $("#caterr").show();
                    return false;
     }
     var cat = $('#category').find('option:selected').attr("name");
     var cat_id = $('#category').val();
     cat = '<input type="hidden" name="catid1[]" value="'+cat_id+'">'+cat;
     var menu = 'All';
     var submenu = 'All';
     var min = 100;
     var max = 999;
     var uniqueid = Math.floor(Math.random() * (max - min + 1)) + min;
  }
   if(type=='2')
  {
  	 var cat_id = $('#category').val();
  	  if(cat_id=='')
     {
     	            $("#cat").focus();
                    $("#caterr").html('Please select category');
                    $("#caterr").show();
                    return false;
     }
  	 var menu_id = $('#menus').val();
  	 if(menu_id=='')
     {
     	            $("#menus").focus();
                    $("#menuerr").html('Please select menu');
                    $("#menuerr").show();
                    return false;
     }
  	
  	 var cat = $('#category').find('option:selected').attr("name");
  	 var cat_id = $('#category').val();
     cat = '<input type="hidden" name="catid2[]" value="'+cat_id+'">'+cat;
     var menu = $('#menus').find('option:selected').attr("name");
     var menu_id = $('#menus').val();
     menu = '<input type="hidden" name="menuid2[]" value="'+menu_id+'">'+menu;
     var submenu = 'All';
     var min = 100;
     var max = 999;
     var uniqueid = Math.floor(Math.random() * (max - min + 1)) + min;
  }
   if(type=='3')
  {
  	 var cat_id = $('#category').val();
  	 var menu_id = $('#menus').val();
  	 var submenu_id = $('#submenus').val();
  	 if(cat_id=='')
     {
     	            $("#cat").focus();
                    $("#caterr").html('Please select category');
                    $("#caterr").show();
                    return false;
     }
     if(menu_id=='')
     {
     	            $("#menus").focus();
                    $("#menuerr").html('Please select menu');
                    $("#menuerr").show();
                    return false;
     }
     if(submenu_id=='')
     {
     	            $("#submenus").focus();
                    $("#submenuerr").html('Please select submenu');
                    $("#submenuerr").show();
                    return false;
     }
  	 var cat = $('#category').find('option:selected').attr("name");
  	 var cat_id = $('#category').val();
     cat = '<input type="hidden" name="catid3[]" value="'+cat_id+'">'+cat;
     var menu = $('#menus').find('option:selected').attr("name");
     var menu_id = $('#menus').val();
     menu = '<input type="hidden" name="menuid3[]" value="'+menu_id+'">'+menu;
     var submenu =  $('#submenus').find('option:selected').attr("name");
     var submenu_id = $('#submenus').val();
     submenu = '<input type="hidden" name="submenuid3[]" value="'+submenu_id+'">'+submenu;
     var min = 100;
     var max = 999;
     var uniqueid = Math.floor(Math.random() * (max - min + 1)) + min;
  }

     var  data ="<tr id="+ uniqueid + "><td width='55'>" + cat + "</td><td width='55'>" + menu + "</td><td width='55'>" + submenu + 
     "</td><td><a href='#' onclick='removedata("+uniqueid+")'>delete</a></td></tr>";
     //alert(data);
     
     $('#table').append(data);
}
function removedata(uniqueid)
{
	 //alert(uniqueid);
	 $('#table tr#'+uniqueid).remove();
	// $('#table tbody tr #'id).remove();
}


function validate_offer()
     {
       
        /*var starttime_mon = $(".col-sm-3 #from1-1").val();
		var endtime_mon = $(".col-sm-3 #to1-1").val();
		if(starttime_mon!='')
		{
			if(endtime_mon=='')
			{
				$(".col-sm-3 #to1-1").focus();
			     return false;
			}
				
		}
		if(endtime_mon!='')
		{
			if(starttime_mon=='')
			{
				$(".col-sm-3 #from1-1").focus();
			     return false;
			}
				
		}*/
		var name = $("#name").val();
        
                 if(name=="")
                 {

                    $("#name").focus();
                    $("#nameerr").html('Please enter name');
                    $("#nameerr").show();
                    return false;
                 }
		var discount_type = $("#discount_type").val();
        var discount = $("#discount").val();
        
                 if(discount=="")
                 {

                    $("#discount").focus();
                    $("#discounterr").html('Please select discount type value');
                    $("#discounterr").show();
                    return false;
                 }
				 if(isNaN(discount)){
					
					$("#discount").focus();
					$("#discounterr").html('Please enter numeric value!');
				    $("#discounterr").show();
					return false;
				 }
		var listing = document.getElementById("table").rows.length;
		if(listing<2)
		{
			
            $("#listerr").html('Please add atleast one combination of category,menu,submenu');
            $("#listerr").show();
            return false;
		}
        
        var order_service_length = $('[name="order_service[]"]:checked').length;
		if(order_service_length<1)
		{
			$("#order_service").focus();
            $("#order_serviceerr").html('Please check atleast one order service');
            $("#order_serviceerr").show();
            return false;
		}

        var validity = $("#validity:checked").val();
        
        var validity_table = $("#validity_table").val();
        var valid_from = $("#valid_from").val();
        var valid_to = $("#valid_to").val();
                 if(validity=='T')
                 {
                    if(validity_table=='')
                    {
                      $("#validity_table").focus();
                      $("#validity_tableerr").html('Please select one validity title');
                      $("#validity_tableerr").show();
                      return false;
                    }
                   
                 }
                  if(validity=='C')
                 {
                    if(valid_from=='')
                    {
                      $("#valid_from").focus();
                      $("#valid_fromerr").html('Please enter valid from');
                      $("#valid_fromerr").show();
                    return false;
                    }
                    if(valid_to=='')
                    {
                      $("#valid_to").focus();
                      $("#valid_toerr").html('Please enter valid to');
                      $("#valid_toerr").show();
                    return false;
                    }
                   
                 }
        var original_price = $("#original_price").val();
        
                 if(original_price=="")
                 {

                    $("#original_price").focus();
                    $("#original_priceerr").html('Please enter original price');
                    $("#original_priceerr").show();
                    return false;
                 }
				 if(isNaN(original_price)){
					
					$("#original_price").focus();
					$("#original_priceerr").html('Please enter numeric value!');
				    $("#original_priceerr").show();
					return false;
				 }
				 if(discount_type==1 &&  parseInt(original_price)<parseInt(discount))
				 {
					 $("#original_price").focus();
					 $("#original_priceerr").html('Please enter original price more than discount fixed value!');
				     $("#original_priceerr").show();
					 return false;
				 }
        var discount_price = $("#discount_price").val();
        
                 if(discount_price=="")
                 {

                    $("#discount_price").focus();
                    $("#discount_priceerr").html('Please enter discount price');
                    $("#discount_priceerr").show();
                    return false;
                 }

                if(isNaN(discount_price)){
					
					$("#discount_price").focus();
					$("#discount_priceerr").html('Please enter numeric value!');
				    $("#discount_priceerr").show();
					return false;
				 }
                  if( parseInt(original_price) < parseInt(discount_price))
				{
					$("#discount_price").focus();
					$("#discount_priceerr").html('Please enter discount price less than original price!');
				    $("#discount_priceerr").show();
					return false;
				}
				if(discount_type==1 &&  parseInt(discount_price)<parseInt(discount))
				 {
					 $("#discount_price").focus();
					 $("#discount_priceerr").html('Please enter discount price more than discount fixed value!');
				     $("#discount_priceerr").show();
					 return false;
				 }

                  document.forms["specialoffer_form"].submit();
     }

     function hideErrorMsg(id)
     {

    
      $('#'+id).attr('style', 'display:none');
  
    
     }

</script>

 @endsection
