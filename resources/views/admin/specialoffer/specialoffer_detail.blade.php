@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
   <div class="panel-body">
     @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
					<ul class="offer_box list-inline">
						<li><strong>Special Offer Name :</strong></li>
						<li>
							{{ $specialoffer['0']['name'] }} 
						</li>
					</ul> 
                    <ul class="offer_box list-inline">
						<li><strong>Discount Type :</strong></li>
						<li>
						{{ $specialoffer['0']['discount_type_name'] }}: {{ $specialoffer['0']['discount_type_value'] }}
						</li>
	   				</ul>
                    
                    <ul class="offer_box list-inline">
                     <li><strong>Category :</strong> </li>
					 <li>
						
                    	<div class="table-responsive">
                            <table class="table table-new text-center table-c">
                                <?php if(count($listcat)>0 || count($listmenu)>0 || count($listsubmenu)>0 ) { ?>
                                <thead>
                                    <tr class="bg-danger">
                                        <th width="21%">Category</th>
                                        <th width="16%">Menu</th>
                                        <th width="21%">Submenu</th>
                                       
                                    </tr>
                                </thead>
							    <?php } ?>
                                <tbody>
                                @if(count($listcat))
                                @foreach($listcat as $listcats)
                                <?php $catrand = rand(10,100); ?>
                                <tr id='<?=$catrand?>'>
                                <td width="55"><?=$listcats['name']?><input name="catid1[]" value="<?=$listcats['id']?>" type="hidden"></td>
                                <td>All</td>
                                <td>All</td>
                              
                                </tr>
                                @endforeach
                                @endif
                                @if(count($listmenu))
                                @foreach($listmenu as $listmenus)
                                <?php $menurand = rand(10,100); ?>
                                <tr id='<?=$menurand?>'>
                                <td width="55"><?=$listmenus['catname']?><input name="catid2[]" value="<?=$listmenus['category_id']?>" type="hidden"></td>
                                <td width="55"><?=$listmenus['menuname']?><input name="menuid2[]" value="<?=$listmenus['menu_id']?>" type="hidden"></td>
                                <td>All</td>
                               
                                </tr>
                                @endforeach
                                @endif
                                @if(count($listsubmenu)>0)
                                @foreach($listsubmenu as $listsubmenus)
                                <?php $submenurand = rand(10,100); ?>
                                <tr id='<?=$submenurand?>'>
                                <td width="55"><?=$listsubmenus['catname']?><input name="catid3[]" value="<?=$listsubmenus['id']?>" type="hidden"></td>
                                 <td width="55"><?=$listsubmenus['menuname']?><input name="menuid3[]" value="<?=$listsubmenus['menu_id']?>" type="hidden"></td>
                                 <td width="55"><?=$listsubmenus['submenuname']?><input name="submenuid3[]" value="<?=$listsubmenus['sub_menu_id']?>" type="hidden"></td>
                                
                                </tr>
                                @endforeach
                                @endif
                             </tbody>               
                            </table>
                        </div>

						
					  </li>
				    </ul>	
                    <ul class="offer_box list-inline">
					<li><strong>Valid From : </strong></li>
						<li>{{  date("d M Y", strtotime($specialoffer['0']['valid_from']))  }}</li>
				    </ul>
	   				<ul class="offer_box list-inline">
						<li><strong>Valid To : </strong></li>
						<li>{{  date("d M Y", strtotime($specialoffer['0']['valid_to']))  }}</li>
				    </ul>
	   				<ul class="offer_box list-inline">
						<li><strong>Order Service :</strong></li>
						<li>
						<ul style="padding-left:18px;">@foreach($orderservice as $orderservices) <li>{{ $orderservices['order_service_name'] }} </li> @endforeach</ul>                    
						<li>
                    </ul>
	   				<ul class="offer_box list-inline">	
                	<h4 class="text-success" style="margin:0;"> Special Offer starts </h4>
				    </ul>

					<?php 
					 for($i=0; $i < count($selected_daytimeall); $i++) 
					  {  
				     $day_id = $selected_daytimeall[$i];  
				       $dayname = \App\superadmin\Rest_day::dayname($day_id);?>
							<ul class="offer_box list-inline">	
								    <li><lable><?=$dayname['0']['day']?></lable></li>
									<li>
									<?php
									  	 $daytime = \App\superadmin\Rest_day::daytime($day_id,$specialoffer['0']['id']);
										
										 if(count($daytime)>0)

										 {
										 foreach($daytime as $daytimes)
										 {

											 if($daytimes['time_from']!='' && $daytimes['time_to']!='')
											 {
										?>  
										   
											

											<div class="col-sm-4"><label>Start Time :</label><?=$daytimes['time_from']?></div>
											<div class="col-sm-4"><label>End Time :</label><?=$daytimes['time_to']?></div>
											<div class="clearfix"></div>

									   <?php }  } } ?>	
								    </li>
							</ul>	
					  <?php } ?>			
				    
                    
	   				<ul class="offer_box list-inline">	
					    <li><strong>Original Price: </strong></li>
						<li>{{ $specialoffer['0']['original_price'] }}</li>
				    </ul>
	   				<ul class="offer_box list-inline">	
						<li><strong>Discount Price: </strong></li>
						<li>{{ $specialoffer['0']['discount_price'] }}</li>
				    </ul>
                    <div class="clearfix"></div>
                </div>
</div>
</div>
@endsection

@section('script')
@endsection
