<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    {!! Html::style('resources/assets/admin/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/admin/css/font-awesome.css') !!}
    {!! Html::style('resources/assets/admin/css/style.css') !!}
    {!! Html::style('resources/assets/admin/css/nanoscroller.css') !!}
    {!! Html::style('resources/assets/css/date/bootstrap-datetimepicker.css') !!}
</head>

<body>
    
<header class="new_header">
    <div class="container">
        <?php 
            $username = \App\superadmin\RestOwner::adminuserdetail(); 
            $restDetail = \App\superadmin\RestOwner::getRestaurantDetail();
            $adminSetting = \App\superadmin\RestOwner::getAdminSettingsData();
            $timeSetting = \App\superadmin\RestOwner::getTimeSettingsData();
        ?>
        <div class="row">
            <div class="col-sm-3">
                <section class="top-bar">
                    <span style="cursor: pointer;" class="pull-left light-shad-red" onclick="document.location.href='{{url('rzone/restaurant/orders')}}'">Back</span>
                </section>
            </div>
            <div class="col-sm-9">
                <section class="top-bar">
                    <span>
                        <a href="javascript:void(0)">{{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }}</a>
                        &nbsp; &nbsp; &nbsp; &nbsp; 
                        <a href="javascript:void(0)">www.backshop.at</a>
                    </span>
                    <span>
                        <a href="javascript:void(0)"><i class="fa fa-fw fa-bell"></i>  
                        <span class="badge badge-up">0</span></a>
                    </span>
                    <span>Welcome {{ ucfirst($username['0']['firstname']).' '.ucfirst($username['0']['lastname']) }}</span>
                    <span>
                        <i class="fa fa-shopping-bag"></i> Sales, April: 5.353 &euro;
                    </span>
                    <span>
                        <a href="{{url('authadmin/logout')}}"><i class="fa fa-sign-out"></i> log out</a>
                    </span>
                </section>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <a href="{{url('rzone/dashboard')}}" class="logo">
                    {!!Html::image('resources/assets/admin/img/logo3.png','Logo')!!}
                </a>
            </div>
            <div class="col-sm-5 text-center">
                <h2>Updated <strong class="black"> &nbsp; <span class="settime"></span> &nbsp; </strong> 
                    <a href="javascript:void(0)" onclick="settime();"><i class="fa fa-refresh text-danger"></i></a></h2>
            </div>
            <div class="col-sm-4">
                @if(count($adminSetting))
                    @if($adminSetting->is_open)
                        @if($timeSetting == 'open')
                            <span class="light-shad pull-right">
                                {{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }} is open today 
                                <i class="fa fa-circle text-success"></i>
                            </span><br>
                        @else
                            <span class="light-shad pull-right">
                                {{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }} is closed today 
                                <i class="fa fa-circle text-danger"></i>
                            </span><br>
                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>
</header>
    
    <?php
        $FrontUserAddressDetails = \App\front\UserOrder::FrontUserAddress($OrderInfo->front_user_id); 
        $order_time = \App\front\UserOrder::get_confirmed_time($OrderInfo->order_id); 
        $order_status = \App\front\UserOrder::get_order_status($OrderInfo->order_id); 
    ?>
    
    <div class="container">
        <div class="row">
            <div class="text-center">
                <div class="col-sm-3">
                    <aside class="numberbox">
                        <h4><strong>&nbsp;</strong></h4>	
                        <h4>Receive:
                            @if((strtotime($OrderInfo->created_at)>0) || ($OrderInfo->created_at !='0000-00-00 00:00:00'))
                                {{ date('d M Y h:i:s A',strtotime($OrderInfo->created_at)) }}
                            @else
                                {{ 'None' }}
                            @endif
                        </h4>
                        <aside class="num_inner sel_1">
                            <figure class="icon"><i class="fa fa-check"></i></figure>
                            <h4>Hello</h4>
                            <h3 class="black">Status</h3>
                            
                        @if($OrderInfo->front_order_status_id==4)
                            <p>This order has been cancelled by customer.</p>
                        @else
                            @if($order_status != '')
                                <p>{{ $order_status }}</p>
                            @else
                                @if($OrderInfo->time_type==1)
                                    <ul class="list-inline">
                                        <li>15</li>
                                        <li>30</li>
                                        <li>45</li>
                                        <li>60</li>
                                        <li>75</li>
                                        <li>90</li>
                                        <li><select>
                                                <option>120</option>
                                                <option>150</option>
                                                <option>180</option>
                                            </select>
                                        </li>
                                    </ul>
                                @else
                                    <p class="LaterMessage">
                                        @if(($OrderInfo->delivery_time!='0000-00-00 00:00:00') || strtotime($OrderInfo->delivery_time)>0)
                                            {{ 'User has set to deliever this order later till '. date('d M Y h:i:s A',strtotime($OrderInfo->delivery_time)) }}
                                        @else
                                            {{ 'User has set to deliever this order later.' }}
                                        @endif
                                    </p>
                                @endif
                                <a class="btn btn-success confirmed HideButton" href="javascript:void(0)">To confirm</a>
                                <a class="btn btn-success rejected HideButton" href="javascript:void(0)">Rejected</a>
                            @endif
                        @endif    
                        </aside>
                    </aside>
                </div>
                
                <?php
                    $order_confirmed = \App\front\UserOrder::get_order_confirmedOrNot($OrderInfo->order_id); 
                    if($order_confirmed == 1){
                        $confirmClass='';
                    }else{
                        $confirmClass='disabled';
                    }
                    $order_cooked = \App\front\UserOrder::get_order_cookedOrNot($OrderInfo->order_id); 
                    if($order_cooked != 0){
                        $confirmCooked='';
                    }else{
                        $confirmCooked='disabled';
                    }
                    $order_cooked_time = \App\front\UserOrder::get_order_cookedTime($OrderInfo->order_id); 
                ?>
                
                <div class="col-sm-3">
                    <aside class="numberbox">
                        <h4><strong>Status:</strong></h4>
                        <h4 id="confirmedId">Confirmed: {{ $order_time }}</h4>
                        <aside class="num_inner {{$confirmClass}} sel_2">
                            <figure class="icon"><i class="fa fa-check"></i></figure>
                            <h4>Hello</h4>
                            <h3 class="black">Cooking Status</h3>
                            <ul class="list-inline">
                            </ul>
                            @if($order_cooked != 0)
                                <p class="LaterMessage1">
                                    {{ $order_cooked_time }}
                                </p>
                            @else
                                <a class="btn btn-success HideButton1" onclick="ReadyForcookDeliver('cooked')" href="javascript:void(0)">To confirm</a>
                            @endif
                        </aside>
                    </aside>
                </div>
                
                <?php
                    $order_delivered = \App\front\UserOrder::get_order_deliveredOrNot($OrderInfo->order_id); 
                    $order_delivered_time = \App\front\UserOrder::get_order_deliveredTime($OrderInfo->order_id); 
                ?>
                <div class="col-sm-3">
                    <aside class="numberbox">
                        <h4><strong>&nbsp;</strong></h4>
                        <h4 id="deadlineId">Deadline: 
                            @if((strtotime($OrderInfo->delivery_time)>0) || ($OrderInfo->delivery_time !='0000-00-00 00:00:00'))
                                {{ date('d M Y h:i:s A',strtotime($OrderInfo->delivery_time)) }}
                            @else
                                {{ 'Not Confirmed' }}
                            @endif
                        </h4>
                        <aside class="num_inner {{$confirmCooked}} sel_3">
                            <figure class="icon"><i class="fa fa-check"></i></figure>
                            <h4>Hello</h4>
                            <h3 class="black">Delivery Status</h3>
                            <ul class="list-inline">
                            
                            </ul>
                            @if($order_delivered != 0)
                                <p class="LaterMessage1">
                                    {{ $order_delivered_time }}
                                </p>
                            @else
                                <a class="btn btn-success HideButton2" onclick="ReadyForcookDeliver('deliver')" href="javascript:void(0)">To confirm</a>
                            @endif
                        </aside>
                    </aside>
                </div>
                
                <?php
                $orderStartDelivery = \App\front\UserOrder::get_order_DeliveryStartOrNot($OrderInfo->order_id); 
                if($orderStartDelivery != 0){
                    $confirmForDelivery='';
                }else{
                    $confirmForDelivery='disabled';
                }
                $orderFinishDeliveredTime = \App\front\UserOrder::getFinishOrderDeliveredTime($OrderInfo->order_id); 
                $orderFinishDeliveredTimeMsg = \App\front\UserOrder::getFinishOrderDeliveredTimeMsg($OrderInfo->order_id); 
                ?>
                <div class="col-sm-3">
                    <aside class="numberbox">
                        <h4><strong>&nbsp;</strong></h4>
                        <h4 id="deliveredId">Delivered : 
                            @if((strtotime($orderFinishDeliveredTime)>0) || ($orderFinishDeliveredTime !='0000-00-00 00:00:00'))
                                {{ date('d M Y h:i:s A',strtotime($orderFinishDeliveredTime)) }}
                            @else
                                {{ 'Not Confirmed' }}
                            @endif
                        </h4>
                        <aside class="num_inner {{$confirmForDelivery}} sel_4">
                            <figure class="icon"><i class="fa fa-check"></i></figure>
                            <h4>&nbsp;</h4>
                            <h3 class="black">Delivered</h3>
                            <ul class="list-inline">
                                
                            </ul>
                            @if($orderFinishDeliveredTime != 0)
                                <p class="LaterMessage1">
                                    {{ $orderFinishDeliveredTimeMsg }}
                                </p>
                            @else
                                <a class="btn btn-success HideButton3" onclick="ReadyForcookDeliver('finishDelivered')" href="javascript:void(0)">To confirm</a>
                            @endif
                        </aside>
                    </aside>
                </div>
                
            </div>
        </div>
	
        <div class="row">
            <div>
                <div class="col-md-12">
                    <div class="TableDiv">
                        <div class="col-md-5 bg-success">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Delivery</h4>
                                </div>
                                <div class="col-md-6">
                                    <h4>Order No.</h4>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-7 bg-success">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="list-inline t-list">
                                        <li><i></i> Order</li>
                                        <li>Price</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <?php
                            $FrontUserFullAddress = \App\front\UserOrder::FrontUserFullAddress($OrderInfo->front_user_id); 
                            
                        ?>
                        
                        <div class="col-md-5 bor-lft">
                            <div class="row">
                                <div class="col-md-6 bor-lft">
                                    <ul class="list-unstyled">
                                        <li>Name : {{ $FrontUserFullAddress->booking_person_name }}</li>
                                        <li>Mobile : {{ $FrontUserFullAddress->mobile }}</li>
                                        <li>Address : {{ $FrontUserFullAddress->address }}</li>
                                        <li>Landmark : {{ $FrontUserFullAddress->landmark }}</li>
                                        <li>City : {{ $FrontUserFullAddress->city_name }}</li>
                                        <li>State : {{ $FrontUserFullAddress->state_name }}</li>
                                        <li>Country : {{ $FrontUserFullAddress->country_name }}</li>
                                        <li>Zip Code : {{ $FrontUserFullAddress->zipcode }}</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="list-unstyled">
                                        <li>{{ $OrderInfo->order_id }}</li>
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="col-md-12 bg-success"><h4>Column 3</h4></div>
                                        <div class="clearfix"></div>
                                        <h4>Styles for displaying content with some. </h4>
                                        <br>
                                        <h4>displaying content  </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="list-inline t-list">
                                        @if(count($ItemInfo))
                                            @foreach($ItemInfo as $info)
                                                <li><i>{{ $info->item_qty }}</i>{{ ucfirst($info->item_name) }}</li>
                                                <li> &euro; {{ number_format($info->item_price,2) }}</li>
<!--                                                <li><i></i> <strong>Extra desire </strong></li>
                                                <li></li>
                                                <li><i>+</i> Lorem Ipsum is simply </li>
                                                <li>1.00 â‚¬</li>
                                                <li><i>+</i> Lorem Ipsum is simply </li>
                                                <li>1.00 â‚¬</li>-->
                                                <li>&nbsp;</li>
                                                <li>&nbsp;</li>
                                            @endforeach
                                        @endif
                                         
                                        <li>Total Amount</li>
                                        <li>&euro; {{ number_format($OrderInfo->grand_total,2) }}</li>
                                         
<!--                                        <li><i>1</i> Empfangen Empfangen 3</li>
                                        <li>0.50 â‚¬</li>
                                        <li><i>1</i> Empfangen Empfangen 3</li>
                                        <li>0.50 â‚¬</li>
                                        <li><i></i> <strong>Lorem Ipsum is simply </strong></li>
                                        <li></li>
                                        <li><i>+</i> Lorem Ipsum is simply </li>
                                        <li>1.00 â‚¬</li>
                                        <li><i>+</i> Lorem Ipsum is simply </li>
                                        <li>1.00 â‚¬</li>
                                        <li>&nbsp;</li>
                                        <li>&nbsp;</li>
                                        <li><i>1</i> Empfangen Empfangen 3</li>
                                        <li>0.50 â‚¬</li>
                                        <li><i></i> <strong>Lorem Ipsum is simply </strong></li>
                                        <li></li>
                                        <li><i>+</i> Lorem Ipsum is simply </li>
                                        <li>1.00 â‚¬</li>
                                        <li><i>+</i> Lorem Ipsum is simply </li>
                                        <li>1.00 â‚¬</li>-->
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <strong>Copyright Â© 2016 â€” Lieferzonas</strong>.at <br>Delivery Network
                </div>
            </div>
        </div>
    </footer>
    
    {!! Html::script('resources/assets/admin/js/jQuery-2.2.0.min.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.min.js') !!}
    {!! Html::script('resources/assets/admin/js/bootstrap.js') !!}
    <!--Added by Rajlakshmi-->
    {!! Html::script('resources/assets/js/date/moment-with-locales.js') !!}
    {!! Html::script('resources/assets/js/date/bootstrap-datetimepicker.js') !!}
    <!--Added by Rajlakshmi-->
    <script>
        var SITE_URL_PRE_FIX = '{{ Config::get('constants.SITE_URL_PRE_FIX', '/lieferzonas') }}';
    </script>
    {!! Html::script('resources/assets/admin/js/script.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.validate.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.nanoscroller.js') !!}
    <!--start Script add by Rajlakshmi(28-05-16)-->
    {!! Html::script('resources/assets/js/jspdf.min.js') !!}
    {!! Html::script('resources/assets/js/html2canvas.js') !!}
    {!! Html::script('resources/assets/js/jspdf.plugin.autotable.src.js') !!}
    <!--end Script add by Rajlakshmi(28-05-16-->
    
@yield('script')
    <script>
        $(function(){
            $('.nano').nanoScroller({
                preventPageScrolling: true
            });
        });	
    </script>
    <script>
//        $(window).on("scroll resize load", function () {
//            document.getElementById('left_side').style.height = "";
//            var lefttHeight = document.getElementById('left_side').clientHeight;
//            var rightHeight = document.getElementById('right_side').clientHeight;
//            if(lefttHeight < rightHeight){
//                document.getElementById('left_side').style.height = rightHeight+25+"px";
//            }
//            else{
//                document.getElementById('left_side').style.height = lefttHeight+25+"px";
//            }
//        });
    </script>
    <script type="text/javascript">
        function change_restaurant_status(status){
            $.ajax({
                url: '{{url('rzone/restaurant/status/change')}}',
                type: "post",
                dataType:"json",
                data: { 'is_open':status, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success == 1){
                        window.location.reload();
                    }else{
                        alert('Status not changed');
                    }
                }
            });
        }
        
        function ReadyForcookDeliver(status){
            if(status == 'cooked'){
                if(!confirm("Are You Sure to start cooking."))
                {
                    return false;
                }
            }else if(status == 'deliver'){
                if(!confirm("Are You Ready to deliver this order ?"))
                {
                    return false;
                }
            }else if(status == 'finishDelivered'){
                if(!confirm("Are You Ready this order has been delivered ?"))
                {
                    return false;
                }
            }else{
                return false;
            }
            var orderId = '<?=$OrderInfo->order_id;?>';
            $.ajax({
                url: '{{ url('rzone/restaurant/order/cookdelivery') }}',
                type: "post",
                dataType:"json",
                data: {'status':status,'orderId':orderId,"_token":"{{ csrf_token() }}"},
                success: function (res) {
                    if(res.success == 1){
                        if(status == 'cooked'){
                            $('.sel_3').removeClass('disabled');
                            $('.HideButton1').remove();
                            $('.sel_2 ul li').remove();
                            $('.sel_2 ul').append("<p>"+res.message+"</p>");
                        }else if(status == 'deliver'){
                            $('.sel_4').removeClass('disabled');
                            $('.HideButton2').remove();
                            $('.sel_3 ul li').remove();
                            $('.sel_3 ul').append("<p>"+res.message+"</p>");
                        }else if(status == 'finishDelivered'){
                            $('.HideButton3').remove();
                            $('.sel_4 ul li').remove();
                            $('.sel_4 ul').append("<p>"+res.message+"</p>");
                            $('#deliveredId').text('Delivered Time: '+res.orderDelivedtime);
                        }
                    }else{
                        alert('some error is comming!');
                    }
                }
            });
        }
        
        $(document).ready(function(){
            $.fn.nset = function(){
                var $this = this
                $($this).find('li').click(function(){
                    $($this).find('li').removeClass('active');
                    $(this).addClass('active');
                });
                $($this).find('.confirmed').click(function(){
                    if(!confirm("Are You Sure to Confirm this order."))
                    {
                        return false;
                    }
                    var time_type = '<?=$OrderInfo->time_type;?>';
                    if(time_type == 1){
                        if($($this).find('li:last-child').hasClass('active')){
                            var active_text = $($this).find('li.active select').val();
                            //$($this).addClass('disabled');
                            //$('.sel_2').removeClass('disabled');
                        }
                        else if($($this).find('li').hasClass('active')){
                            var active_text = $($this).find('li.active').html();
                            //$($this).addClass('disabled');
                            //$('.sel_2').removeClass('disabled');
                        }
                        else{
                            alert('Please select time');
                            return false;
                        }
                    }else{
                        active_text = 'none';
                    }
                    var orderId = '<?=$OrderInfo->order_id;?>';
                    $.ajax({
                        url: '{{ url('rzone/restaurant/order/confirmed') }}',
                        type: "post",
                        dataType:"json",
                        data: {'time_type':time_type,'given_time':active_text,'orderId':orderId,"_token":"{{ csrf_token() }}"},
                        success: function (res) {
                            if(res.success == 1){
                                //$($this).addClass('disabled');
                                $('.sel_2').removeClass('disabled');
                                $('.HideButton').remove();
                                $('#confirmedId').text('Confirmed: '+res.confirmed_time);
                                if(time_type == 1){
                                    $('#deadlineId').text('Deadline: '+res.deadline_time);
                                    $('.sel_1 ul li').remove();
                                    $('.sel_1 ul').append("<p>"+res.message+"</p>");
                                }else{
                                    $('.LaterMessage').text(res.message);
                                }
                            }else{
                                alert('order not confirmed');
                            }
                        }
                    });
                });
            }
            $('.sel_1').nset();
            
            $('.rejected').click(function(){
                if(!confirm("Are You Sure to reject this order."))
                {
                    return false;
                }
                var time_type = '<?=$OrderInfo->time_type;?>';
                var orderId = '<?=$OrderInfo->order_id;?>';
                $.ajax({
                    url: '{{ url('rzone/restaurant/order/rejected') }}',
                    type: "post",
                    dataType:"json",
                    data: {'time_type':time_type,'orderId':orderId,"_token":"{{ csrf_token() }}"},
                    success: function (res) {
                        if(res.success == 1){
                            //$('.sel_2').removeClass('disabled');
                            $('.HideButton').remove();
                            if(time_type == 1){
                                $('.sel_1 ul li').remove();
                                $('.sel_1 ul').append("<p>"+res.message+"</p>");
                            }else{
                                $('.LaterMessage').text(res.message);
                            }
                        }else{
                            alert('order not rejected');
                        }
                    }
                });
            });
        });
        
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        function settime(){
            $('.settime').empty();
            var dt = new Date();
            var time = addZero(dt.getHours()) + ":" + addZero(dt.getMinutes());
            $('.settime').append(time);
        }
        settime();
    </script>
</body>
</html>