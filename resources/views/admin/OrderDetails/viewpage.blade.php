@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')

    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                <h3 class="box-title"><i class="fa fa-tags"></i> Order Detail
                    <span class="pull-right">
                        <a class="btn btn-success" href="{{url('rzone/restaurant/orders')}}">Back</a>
                    </span>
                    <span class="pull-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#order-track" data-original-title><span>Track Order</span></a>
                    </span>
                </h3>    
                <hr>
                @if(count($OrderInfo))
                    @foreach($OrderInfo as $info)
                        <div class="box-body">
                            <div class="row form-horizontal">
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Order Id :</label></div>
                                    <div class="col-md-7">{{$info->order_id}}</div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Order Status  :</label></div>
                                    <div class="col-md-7"> {{$info->status_name}}	</div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>User Name :</label></div>
                                    <div class="col-md-7">{{ ucfirst($info->fname)  }} {{ ucfirst($info->lname)  }}</div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Email Id :</label></div>
                                    <div class="col-md-7">{{$info->email}}
                                    </div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Phone  :</label></div>
                                    <div class="col-md-7">{{$info->mobile}}	</div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Created  :</label></div>
                                    <div class="col-md-7">{{date('d M Y',strtotime($info->created_at))}} </div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Address  :</label></div>
                                    <div class="col-md-7"> {{$info->address}}, {{$info->landmark}}, {{$info->country_name}} {{$info->state_name}} {{$info->city_name}} {{$info->zipcode}}		</div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Order Amount  :</label></div>
                                    <div class="col-md-7">&euro; {{$info->grand_total}}	</div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Order :</label></div>
                                    <div class="col-md-7">
                                        <?php if($info->time_type==1){
                                           echo 'Immediate Order'; 
                                        }else{
                                            echo 'Pre Order';
                                        }?>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Order Time :</label></div>
                                    <div class="col-md-7">
                                        <?php if($info->time_type==2){
                                            if(($info->user_time !='0000-00-00 00:00:00') || (strtotime($info->user_time)>0)){
                                                echo date('d M Y h:i:s A',strtotime($info->user_time));
                                            }else{
                                                echo 'NO Time';
                                            }
                                        }else{
                                            if(($info->delivery_time !='0000-00-00 00:00:00') || (strtotime($info->delivery_time)>0)){
                                                echo date('d M Y h:i:s A',strtotime($info->delivery_time));
                                            }else{
                                                echo 'NO Time';
                                            }
                                        }?>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Payment Method :</label></div>
                                    <div class="col-md-7">
                                        <?php if($info->payment_method==1){
                                            echo 'Cash on Delivery'; 
                                        }else{
                                            echo 'Online';
                                        }?>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Order Type :</label></div>
                                    <div class="col-md-7">
                                        <?php if($info->order_type==1){
                                            echo 'Delivery'; 
                                        }else{
                                            echo 'Takeaway';
                                        }?>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 form-group">
                                    <div class="col-md-5"><label>Order Paid :</label></div>
                                    <div class="col-md-7">
                                        <?php if(count($TransactionInfo)>0){
                                            echo 'Yes'; 
                                        }else{
                                            echo 'No';
                                        }?>
                                    </div>
                                </div>
                                
                                <?php  $grand_total =$info->grand_total ;?>
                            </div>
                        </div>
                    @endforeach
                @endif
                
                <div class="box-header">
                    <h3 class="box-title text-success"> Item Detail :</h3>
                    <hr>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-inline t-list">
                                @if(count($ItemInfo))
                                    @foreach($ItemInfo as $info)
                                        <li><i>{{ $info->item_qty }} </i>{{ ucfirst($info->item_name) }}</li>
                                        <li> &euro; {{ number_format($info->item_price,2) }}</li>
<!--                                        <li><i></i> <strong class="text-success">Extra desire </strong></li>
                                        <li></li>
                                        <li><i>+</i> Lorem Ipsum is simply </li>
                                        <li>1.00 €</li>
                                        <li><i>+</i> Lorem Ipsum is simply </li>
                                        <li>1.00 €</li>-->
                                        <li>&nbsp;</li>
                                        <li>&nbsp;</li>
                                    @endforeach
                                @endif
                                <li><strong>Total Amount</strong></li>
                                <li><strong>&euro; {{ number_format($grand_total,2) }}</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="order-track" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-primary theme-border">
                <div class="panel-heading theme-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="panel-title" id="contactLabel">Track Order</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped">
                   <thead>
                        <tr>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                        <tbody>
                             @if(count($order_status))
                                  @foreach($order_status as $status)
                                       <tr>
                                           <td>{{ $status->status_name }}</td>
                                           <td>{{ date("d M Y H:i:s", strtotime($status->created_at)) }}</td>
                                       </tr>
                                  @endforeach
                             @endif
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
@endsection 

     
@section('script')
<script>
    
</script>
@endsection
