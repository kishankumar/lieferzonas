<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    {!! Html::style('resources/assets/admin/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/admin/css/font-awesome.css') !!}
    {!! Html::style('resources/assets/admin/css/style.css') !!}
    {!! Html::style('resources/assets/admin/css/nanoscroller.css') !!}
    {!! Html::style('resources/assets/css/date/bootstrap-datetimepicker.css') !!}
</head>

<body>
    
<header class="new_header">
    <div class="container">
        <?php 
            $username = \App\superadmin\RestOwner::adminuserdetail(); 
            $restDetail = \App\superadmin\RestOwner::getRestaurantDetail();
            $adminSetting = \App\superadmin\RestOwner::getAdminSettingsData();
            $timeSetting = \App\superadmin\RestOwner::getTimeSettingsData();
        ?>
        <div class="row">
            <div class="col-sm-3">
                <section class="top-bar">
                    <span style="cursor: pointer;" class="pull-left light-shad-red" onclick="document.location.href='{{url('rzone/restaurant/orders')}}'">Back</span>
                </section>
            </div>
            <div class="col-sm-9">
                <section class="top-bar">
                    <span>
                        <a href="javascript:void(0)">{{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }}</a>
                        &nbsp; &nbsp; &nbsp; &nbsp; 
                        <a href="javascript:void(0)">www.backshop.at</a>
                    </span>
                    <span>
                        <a href="javascript:void(0)"><i class="fa fa-fw fa-bell"></i>  
                        <span class="badge badge-up">0</span></a>
                    </span>
                    <span>Welcome {{ ucfirst($username['0']['firstname']).' '.ucfirst($username['0']['lastname']) }}</span>
                    <span>
                        <i class="fa fa-shopping-bag"></i> Sales, April: 5.353 &euro;
                    </span>
                    <span>
                        <a href="{{url('authadmin/logout')}}"><i class="fa fa-sign-out"></i> log out</a>
                    </span>
                </section>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <a href="{{url('rzone/dashboard')}}" class="logo">
                    {!!Html::image('resources/assets/admin/img/logo3.png','Logo')!!}
                </a>
            </div>
            
            <div class="col-sm-3">
                {!! Form::open(array('url'=>"rzone/restaurant/order/list",'class'=>'','method'=>'GET')) !!}
                    <div class="input-group" style="margin-top:10px;">
                        {!! Form::text('search',$search,["id"=>'search',"class"=>"form-control","placeholder"=>"Search by order number"]) !!}
                        <span class="input-group-btn">
                            {!! Form::submit('Search',['style'=>'border:none;padding:5px 10px;','id'=>'submit','class'=>'btn btn-success']) !!}
                        </span>
                    </div>
                {!! Form::close() !!}
            </div>
            
            <div class="col-sm-3 text-center">
                <h3 style="margin-top:16px;">Updated <strong class="black"> &nbsp; <span class="settime"></span> &nbsp; </strong> 
                <a href="javascript:void(0)" onclick="settime();"><i class="fa fa-refresh text-danger"></i></a></h3>
            </div>
            <div class="col-sm-3">
                @if(count($adminSetting))
                    @if($adminSetting->is_open)
                        @if($timeSetting == 'open')
                            <span class="light-shad pull-right">
                                {{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }} is open today 
                                <i class="fa fa-circle text-success"></i>
                            </span><br>
                        @else
                            <span class="light-shad pull-right">
                                {{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }} is closed today 
                                <i class="fa fa-circle text-danger"></i>
                            </span><br>
                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>
</header>
    
    <div class="container-fluid">
        <div class="row">
            <ul class="fullBGtable list-unstyled black">
    		<li>
                    <div class="container">
                        <table class="table">
                            <tr>
                                <td width="11%">Order</td>
                                <td width="11%">Deadline</td>
                                <td width="17%">Created</td>
                                <td width="17%">Confirmed</td>
                                <td width="32%">Address</td>
                                <td width="12%">Amount</td>
                            </tr>
                        </table>
                    </div>
                </li>
                
                @if(count($OrderInfo))
                    @foreach($OrderInfo as $info)
                        @if($info->front_order_status_id == 1)
                            {{--*/ $thmbClass = 'noti_info' /*--}}
                        @else
                            {{--*/ $thmbClass = '' /*--}}
                        @endif
                        <?php
                            $FrontUserAddressDetails = \App\front\UserOrder::FrontUserAddress($info->front_user_id); 
                            $order_time = \App\front\UserOrder::get_order_confirmed_time($info->order_id); 
                        ?>
                        
                        <li class="{{ $thmbClass }}">
                            <a href="{{url('rzone/restaurant/order/'.$info->order_id.'/status')}}">
                                <div class="container">
                                    <table class="table">
                                        <tr>
                                            <td width="11%">{{ ($info->order_id)  }}</td>
                                            <td width="11%">{{ (($info->rest_given_time) ? ($info->rest_given_time).' Minutes' : 'Not Accepted') }}</td>
                                            <td width="17%">{{ date('d M Y h:i:s A',strtotime($info->created_at)) }}</td>
                                            <td width="17%">{{ $order_time }}</td>
                                            <td width="32%">{{ $FrontUserAddressDetails }}</td>
                                            <td width="12%">&euro; {{ number_format($info->grand_total,2) }} </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </li>
                    @endforeach
                @else 
                    <li>
                        <div class="container">
                            <table class="table">
                                <tr>
                                    <td colspan="7" align="center">No Order Exist</td>
                                </tr>
                            </table>
                        </div>
                    </li>
                @endif
            </ul>
            {!! $OrderInfo->appends(Request::except('page'))->render() !!}
        </div>
    </div>
    
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <strong>Copyright © 2016 — Lieferzonas</strong>.at <br>Delivery Network
                </div>
            </div>
        </div>
    </footer>
    
    {!! Html::script('resources/assets/admin/js/jQuery-2.2.0.min.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.min.js') !!}
    {!! Html::script('resources/assets/admin/js/bootstrap.js') !!}
    <!--Added by Rajlakshmi-->
    {!! Html::script('resources/assets/js/date/moment-with-locales.js') !!}
    {!! Html::script('resources/assets/js/date/bootstrap-datetimepicker.js') !!}
    <!--Added by Rajlakshmi-->
    <script>
        var SITE_URL_PRE_FIX = '{{ Config::get('constants.SITE_URL_PRE_FIX', '/lieferzonas') }}';
    </script>
    {!! Html::script('resources/assets/admin/js/script.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.validate.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.nanoscroller.js') !!}
    <!--start Script add by Rajlakshmi(28-05-16)-->
    {!! Html::script('resources/assets/js/jspdf.min.js') !!}
    {!! Html::script('resources/assets/js/html2canvas.js') !!}
    {!! Html::script('resources/assets/js/jspdf.plugin.autotable.src.js') !!}
    <!--end Script add by Rajlakshmi(28-05-16-->
    
@yield('script')
    <script>
        $(function(){
            $('.nano').nanoScroller({
                preventPageScrolling: true
            });
        });	
    </script>
    <script>
        $(window).on("scroll resize load", function () {
            document.getElementById('left_side').style.height = "";
            var lefttHeight = document.getElementById('left_side').clientHeight;
            var rightHeight = document.getElementById('right_side').clientHeight;
            if(lefttHeight < rightHeight){
                document.getElementById('left_side').style.height = rightHeight+25+"px";
            }
            else{
                document.getElementById('left_side').style.height = lefttHeight+25+"px";
            }
        });
    </script>
    <script type="text/javascript">
        function change_restaurant_status(status){
            $.ajax({
                url: '{{url('rzone/restaurant/status/change')}}',
                type: "post",
                dataType:"json",
                data: { 'is_open':status, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success == 1){
                        window.location.reload();
                    }else{
                        alert('Status not changed');
                    }
                }
            });
        }
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        function settime(){
            $('.settime').empty();
            var dt = new Date();
            var time = addZero(dt.getHours()) + ":" + addZero(dt.getMinutes());
            $('.settime').append(time);
        }
        settime();
    </script>
</body>
</html>