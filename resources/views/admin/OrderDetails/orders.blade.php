@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')

    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                <h3 class="text-success"> Restaurant Orders </h3>
                    <hr>
                    @if(count($OrderInfoCount))
                        <div class="form-group">
                            <a class="btn btn-outline" href="{{url('rzone/restaurant/order/list')}}">Click here to Check Order Status</a>
                        </div>
                    @endif
                
                {!! Form::open(array('url'=>"rzone/restaurant/orders",'class'=>'form-horizontal','method'=>'GET')) !!}
                    <div class="form-group">
                        <div class="col-md-4">
                            {!! Form::text('search',$search,["id"=>'search','style'=>'width:100%',"class"=>"form-control","placeholder"=>"Search"]) !!}
                        </div>
                        <div class="col-md-4">
                            <div class="input-group date" id="datetimepicker6">
                                {!! Form::text('from',$from_date,["id"=>'from',"class"=>"form-control","placeholder"=>"Order time from"]) !!}
                                <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span> 
                           </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group date" id="datetimepicker7">
                                {!! Form::text('to',$to_date,["id"=>'to',"class"=>"form-control","placeholder"=>"Order time to"]) !!}
                                <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span> 
                            </div>
                        </div>
                    </div>
                    <div>
                        {!! Form::submit('Search',['id'=>'submit','class'=>'btn btn-outline']) !!}
                        <a class="btn btn-outline" href="{{url('rzone/restaurant/orders')}}">Reset Search</a>
                    </div>
                {!! Form::close() !!}
                
<!--                <form class="row">
                    <div class="col-md-4">
                        <div class="media">
                            <div class="pull-right">
                                <button class="btn btn-outline">Search</button>
                            </div>
                            <div class="media-body">
                                <select class="form-control"><option>Lorem Ipsum is simply</option></select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="media">
                            <div class="pull-right">
                                <button class="btn btn-outline">Search</button>
                            </div>
                            <div class="media-body">
                                <select class="form-control"><option>Lorem Ipsum is simply</option></select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="#"><i class="fa fa-file-pdf-o fa-2x text-danger"></i></a>  &nbsp; 
                        <a href="#"><i class="fa fa-print fa-2x"></i></a> 
                    </div>
                </form>-->
                
                <?php 
                    $class='<i class="fa fa-caret-down"></i>';
                    $class1='<i class="fa fa-caret-up"></i>';
                    $class2='<i class="fa fa-sort"></i>';
                ?>
                </div>
                <div class="table-responsive mt25">
                    <table class="table table-new table-c">
                        <thead class="bg-danger sortby">
                            <tr>
                                <th>
                                    <a href="{{url('rzone/restaurant/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='created' ? 'created-desc' :'created';?>">
                                    Date
                                    <?php if($type=='created'){ echo $class; } elseif($type=='created-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('rzone/restaurant/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='order' ? 'order-desc' :'order';?>">
                                    Order ID
                                    <?php if($type=='order'){ echo $class; } elseif($type=='order-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>
                                    <a href="{{url('rzone/restaurant/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='name' ? 'name-desc' :'name';?>">
                                    Name
                                    <?php if($type=='name'){ echo $class; } elseif($type=='name-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
<!--                                <th>
                                    <a href="{{url('rzone/restaurant/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='email' ? 'email-desc' :'email';?>">
                                    Email
                                    <?php if($type=='email'){ echo $class; } elseif($type=='email-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>-->
                                <th>
                                    <a href="{{url('rzone/restaurant/orders')}}?search=<?=(($search!='') ? $search :'')?>&type=<?=$type=='number' ? 'number-desc' :'number';?>">
                                    Number
                                    <?php if($type=='number'){ echo $class; } elseif($type=='number-desc'){ echo $class1; } else { echo $class2; } ?>
                                    </a>
                                </th>
                                <th>Status</th>
                                <th>Payment</th>
                                <th>Type</th>
                                <th>Price</th>
                                <!--<th>Detail</th>-->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($OrderInfo))
                                {{--*/ $totalAmount=0 /*--}}
                                @foreach($OrderInfo as $info)
                                    <tr>
                                        <td>{{ date("d M Y", strtotime($info->created_at))  }}</td>
                                        <td>
                                            <a style="color:#333;font-weight: bold;" href="{{url('rzone/restaurant/orders/'.$info->order_id)}}">
                                                {{ ($info->order_id)  }}
                                            </a>
                                        </td>
                                        <td>{{ ($info->fname)  }}</td>
                                        <!--<td>{{ ($info->email)  }}</td>-->
                                        <td>{{ ($info->mobile)  }}</td>
                                        <td>{{$info->status_name}}</td>
                                        <td><?php if($info->payment_method==1) { echo 'COD'; } else { echo 'Online'; } ?></td>
                                        <td>
                                            <?php if($info->order_type==1){
                                                echo 'Delivery'; 
                                            }else{
                                                echo 'Takeaway';
                                            }?>
                                        </td>
                                        
                                        <td> &euro; {{ number_format($info->grand_total,2) }} </td>
<!--                                        <td>
                                            <a class="btn btn-success" style=" border-width: 1px; font-size: 15px; font-weight: normal;" href="{{url('rzone/restaurant/orders/'.$info->order_id)}}">
                                                Detail
                                            </a>
                                        </td>-->
										<?php if($info->front_order_status_id!=1 && $info->front_order_status_id!=3 && $info->front_order_status_id!=4) { ?>
                                        <td>
                                            <a href="{{url('rzone/restaurant/orders/printbill/'.$info->order_id)}}" style=" border-width: 1px; font-size: 15px; font-weight: normal;" class="btn btn-success">
                                               Print Bill
                                            </a>
                                        </td>
										<?php } else { ?>
										<td>
											Not Available
										</td>
										<?php } ?>
                                    </tr>
                                    <?php $totalAmount = $totalAmount + (float) $info->grand_total; ?>
                                @endforeach
                                <tr class="text-right">
                                    
                                    <td colspan="9"><strong>Total:</strong> <strong>&euro; {{ number_format($totalAmount,2) }}</strong></td>
                                   
                                </tr>
                            @else 
                                <tr>
                                    <td colspan="9" align="center">
                                        No Order Exist
                                    </td>
                                </tr>
                             @endif
                        </tbody>
                    </table>
                    {!! $OrderInfo->appends(Request::except('page'))->render() !!}
                </div>
        </div>
    </div>
@endsection 

    <div class="modal fade" id="show-orders-view" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-success theme-border">
                <div class="panel-heading theme-bg">
                    <span id="order-number"></span>
                    <button type="button" id="modalclose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="panel-body"  id="order-item-list">

                </div>
            </div>
        </div>
    </div>   
@section('script')
<script>
    
    function show_modal(modelId,orderNumber,orderList,orderId,ordertableId,restId) {
	$.ajax({
            url: '{{ url('rzone/shop/orders/getdata') }}',
            type: "post",
            data: {'orderId':orderId,'ordertableId':ordertableId,'restId':restId, "_token":"{{ csrf_token() }}"},
	    	success: function (data) {
                if(data=='' || data==null){
                    data='No items assigned';
                }
                else{
                    data=data;
                }
                document.getElementById(orderNumber).innerHTML = orderId;
                document.getElementById(orderList).innerHTML = data;
            }
	});
	$("#"+modelId).modal('show');
    }
    
    $(window).on("scroll resize load", function () {
        document.getElementById('left_side').style.height = "";
        var lefttHeight = document.getElementById('left_side').clientHeight;
        var rightHeight = document.getElementById('right_side').clientHeight;
        if(lefttHeight < rightHeight){
            document.getElementById('left_side').style.height = rightHeight+25+"px";
        }
        else{
            document.getElementById('left_side').style.height = lefttHeight+25+"px";
        }
    });
    
    $(function () {
        $('#datetimepicker6').datetimepicker({
            useCurrent: false,
            format: 'YYYY-MM-DD',
        });
        $('#datetimepicker7').datetimepicker({
            useCurrent: false,
            format: 'YYYY-MM-DD',
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
        
</script>
@endsection
