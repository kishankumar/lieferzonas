<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin | Log in </title>
    {!! Html::style('resources/assets/admin/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/admin/css/font-awesome.css') !!}
    {!! Html::style('resources/assets/admin/css/style.css') !!}
</head>
<body>

</body>
    <div class="login_section">
        <div class="login flipInY animated">
			
                <section>
                    <figure class="logo_icon bounceInDown animated">
                        {!!Html::image('resources/assets/admin/img/logo-icon.png','','')!!}
                    </figure>
                    <figure class="text-center">
					{!!Html::image('resources/assets/admin/img/login-logo.png','','')!!}
                    </figure>
                    
                    {!! Form::open(['url'=>'authadmin/login','method'=>'post','id'=>'login_form']) !!}
                    <!--<form action="{{ action("Auth\AdminAuthController@getLogin") }}" method="POST">-->
                    <!--{!! csrf_field() !!}-->
                        <hr>
                        
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li> {{$error}} </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        
                        {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Email']) !!}
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
                        <div class="text-center">
                            {!! Form::submit('Login',['id'=>'login','class'=>'btn btn-success']) !!}
						<h4><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal_forger">Forgot your password ?</a><br></h4>
						<!--<h4><a href="#">Contact service center</a></h4>-->
                        </div>
                        <!--</form>-->
                    {!! Form::close() !!}
                </section>
            </div>
            <div class="copy">Copyright &copy; 2016 Lieferzonas.at</div>
    </div>

 <div class="modal fade" id="myModal_forger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md robotoregular">
        <div class="modal-content">
            <div class="modal-header" style="background:#81c02f; color:#fff;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Hast Du Dein Passwort vergessen?</h4>
            </div>
            <div class="modal-body">
                <div class="row">
				{!! Form::open(['url'=>'front/forgotpass','method'=>'post','id'=>'forgot_pwd','name'=>'forgot_pwd']) !!}
                    
                        <div class="col-xs-12">
                            
                            <div class="form-group">
                               
						    {!! Form::text('email','',["id"=>'email',"class"=>"form-control modal-input","placeholder"=>"Email-Adresse","onkeypress"=>"hideErrorMsg('emailerr')"]) !!}
								  <span id="emailerr" class="text-danger"></span>
                            </div>
                            <!--<small class="help-block text-center"> <span class="text-danger">Dieses Feld darf nicht leer sein</span></small>
                            <div class="form-group"> {!! Html::image('resources/assets/front/img/caprcha.png') !!} </div>-->
                            <div class="form-group">
							{!!Form::button('Neues Passwort anfordern',["id"=>'forgotpass',"class"=>"btn btn-success btn-block brn-rg",
							"style"=>"text-transform:none;","onclick"=>"forgot_pass()"]) !!}
                              
                            </div>
                        </div>
                     {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
	
	
<style>
    html, body, .login_section{height: 100%;}	
</style>	

{!! Html::script('resources/assets/admin/js/jQuery-2.2.0.min.js') !!}
{!! Html::script('resources/assets/admin/js/jquery.min.js') !!}
{!! Html::script('resources/assets/admin/js/bootstrap.js') !!}
{!! Html::script('resources/assets/admin/js/jquery.validate.js') !!}
{!! Html::script('resources/assets/admin/js/additional-methods.js') !!}
</html>

<script>
 function forgot_pass()
		{
			if(document.forgot_pwd.email.value.trim() == '')
			{
				
				document.forgot_pwd.email.focus();
				$("#emailerr").html('Please enter your registered email');
                $("#emailerr").show();
				return false;
			}
			if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(document.forgot_pwd.email.value))) 
			{
				
				document.forgot_pwd.email.focus();
				$("#emailerr").html('Invalid email id. Please enter valid email');
                $("#emailerr").show();
				return false;
			}
			var email = document.forgot_pwd.email.value.trim();

			  $.ajax({
                url: '{{url('sendmail')}}',
                type: "post",
                dataType:"json",
                data: {'email':email, "_token":"{{ csrf_token() }}"},
                success: function(res){
                   
                    if(res.success==1)
                    {
                        alert('A link is sent to your registered email successfully!');
                    }
                    else
                    {
                        alert('This Email id is not registred!');
                    }
                }
            });
			
			 //document.forms["forgot_pwd"].submit();
		}
    $(document).ready(function(){
        $("#login_form").validate({
            rules: {
                email: {
                    required: true,
                    email:true
                },
                password: {
                    required : true,
                    minlength:3,
                },
            },
            messages: {
                email:{
                    required: "Please enter email",
                    email: "Please enter valid email",
                },
                password: {
                    required: "Please enter password",
                    minlength: "Password must be atleast 3 letters",
                },
            }
        });
    });
	
	function hideErrorMsg(id){
    
			  $('#'+id).attr('style', 'display:none');
		  
			
			 }
</script>
<style>
	.alert{padding: 5px;}
	.alert ul{padding-left: 20px;}
</style>