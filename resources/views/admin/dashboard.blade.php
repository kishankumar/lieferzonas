@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
    <div class="panel-body">
        <h3 class="text-success">Restaurant Info </h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
    </div>
    </div>
	 @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                <div id="right_side">
					<div class="panel-body">
						<h3 class="text-success">Notification</h3>
						<hr>
						<div class="notification_show">
						@if(count($notification))
                      @foreach($notification as $notifications)
				       <?php
						if($notifications['is_read']=='1')
						{
							$notinameclass = "noti_name".$notifications['id']." text-muted";
						}
						else{
							 $notinameclass = "noti_name".$notifications['id']."";
						}
						?>
							<aside>
								<h4><a class="<?=$notinameclass?>" href="javascript:void(0)" onclick="noti(<?=$notifications['id']?>);"><?=$notifications['title']?></a></h4>
								<p class="noti_text<?=$notifications['id']?>"><?=$notifications['comment']?></p>
							</aside>
							
							@endforeach
							@endif
						</div>
						<br>
						@if($noticount > 5)
						<a href="javascript:void(0);" class="btn btn-success See_More5">See More</a>
					    @endif
					</div> 
                </div>
</div>

@endsection

@section('script')
    <script>
        $(window).on("scroll resize load", function () {
            document.getElementById('left_side').style.height = "";
            var lefttHeight = document.getElementById('left_side').clientHeight;
            var rightHeight = document.getElementById('right_side').clientHeight;
            if(lefttHeight < rightHeight){
                document.getElementById('left_side').style.height = rightHeight+25+"px";
            }
            else{
                document.getElementById('left_side').style.height = lefttHeight+25+"px";
            }
        });
    </script>
    <script>
        function noti(num){
            $('.notification_show p').slideUp();
            if($('.noti_name'+num).hasClass('open')){
                $('.noti_text'+num).slideUp();	
                $('.noti_name'+num).removeClass('open');
            }
            else{
                $('.noti_text'+num).slideDown();
                $('.notification_show h4 a').removeClass('open');
                $('.noti_name'+num).addClass('open')
            }
            var base_host = window.location.href;
            base_host = base_host.substring(0, base_host.lastIndexOf("/"));
            var base_url = base_host+'/front/readstatus';
            $.ajax({
                url: '{{url('readstatus')}}',
                type: "post",
                dataType:"json",
                data: {'noti_id':num,"_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        $('.noti_name'+num).addClass('text-muted')
                    }
               }
            });
        }
	
        $(document).ready(function(){
            $('.See_More5').click(function(){
                var count = $(".notification_show aside a").length;
                $.ajax({
                    url: '{{url('showmore')}}',
                    type: "GET",
                    data: {'count':count},
                    success: function(res)
                    {
                        var received =jQuery.parseJSON(res);
                        $(".notification_show").append(received['data']);
                    }
                });
            });
        });
    </script>
@endsection
