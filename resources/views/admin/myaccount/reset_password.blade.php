<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin | Log in </title>
    {!! Html::style('resources/assets/admin/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/admin/css/font-awesome.css') !!}
    {!! Html::style('resources/assets/admin/css/style.css') !!}
</head>
<body>

</body>
 <div class="login_section">
    <div class="login flipInY animated">
    <section>	
	   <figure class="logo_icon bounceInDown animated">
                        {!!Html::image('resources/assets/admin/img/logo-icon.png','','')!!}
                    </figure>
                    <figure class="text-center">
					{!!Html::image('resources/assets/admin/img/login-logo.png','','')!!}
                    </figure>
                    
		@if(Session::has('message'))
						{{ Session::get('message') }}
						
						{{ Session::put('message','') }}
					@endif
					
					@if($errors->any())
						@foreach( $errors->all()  as $error)
						<li>{{ $error }}</li>
						@endforeach
			 @endif
		    {!! Form::open(array('route' => 'forgotpassword.store','id'=>'reset_pass','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}
		 
			<h3>Reset Password</h3>
			
			 <div class="require_star"> {!! Form::label('New Password') !!}<span class="red">*</span>
			 {!! Form::password('newpassword',["id"=>'newpassword',"class"=>"form-control","placeholder"=>"New Password","onkeypress"=>"hideErrorMsg('npasserr')"]) !!}
			 {!! Form::hidden('act_key',$act_key,["id"=>'act_key',"class"=>"form-control"]) !!}
			
			</div>
			 <span id="npasserr" class="text-danger"></span>
			<div class="require_star">{!! Form::label('Confirm Password') !!} <span class="red">*</span>
			 {!! Form::password('conpassword',["id"=>'conpassword',"class"=>"form-control","placeholder"=>"Confirm Password","onkeypress"=>"hideErrorMsg('cpasserr')"]) !!}
			  
			</div>
			 <span id="cpasserr" class="text-danger"></span>
		   
			<div class="form-group margin-top text-center">
			{!!Form::button('Reset Password',["id"=>'changepass',"class"=>"btn  btn-primary","onclick"=>"validate_pass()"]) !!}
<!--			<hr>-->
<!--			<p><span class="red">*</span> - Required Fields.</p>  -->
			</div>
		   {!! Form::close() !!}
    </section>
    </div>
	 <div class="copy">Copyright &copy; 2016 Lieferzonas.at</div>
</div>
	<style>
    html, body, .login_section{height: 100%;}	
    </style>	
	{!! Html::script('resources/assets/admin/js/jQuery-2.2.0.min.js') !!}
	{!! Html::script('resources/assets/admin/js/jquery.min.js') !!}
	{!! Html::script('resources/assets/admin/js/bootstrap.js') !!}
	{!! Html::script('resources/assets/admin/js/jquery.validate.js') !!}
	{!! Html::script('resources/assets/admin/js/additional-methods.js') !!}
</html>
<script>
function validate_pass()
			{
			    //alert('g');
				 var newpassword = $("#newpassword").val();
				 if(newpassword==""){
					
					$("#newpassword").focus();
					$("#npasserr").html('Please enter new password');
					$("#npasserr").show();
					return false;
				 } 
				 var newpasswordlength = $("#newpassword").val().length;
				 if(newpasswordlength<6)
				 {
					$("#newpassword").focus();
					$("#npasserr").html('Please enter atleast 6 characters for new password');
					$("#npasserr").show();
					return false;
				 }	
				 var conpassword = $("#conpassword").val();
				 if(conpassword==""){
					
					$("#conpassword").focus();
					$("#cpasserr").html('Please enter confirm password');
					$("#cpasserr").show();
					return false;
				 } 
				 var conpasswordlength = $("#conpassword").val().length;
				 if(conpasswordlength<6)
				 {
					$("#conpassword").focus();
					$("#cpasserr").html('Please enter atleast 6 characters for confirm password');
					$("#cpasserr").show();
					return false; 
				 }
				 if(conpassword!=newpassword){
					
					$("#conpassword").focus();
					$("#cpasserr").html('Please enter confirm password same as new password');
					$("#cpasserr").show();
					return false;
				 } 
				  document.forms["reset_pass"].submit();
     
			}
			function hideErrorMsg(id){
    
			  $('#'+id).attr('style', 'display:none');
		  
			
			 }
</script>
