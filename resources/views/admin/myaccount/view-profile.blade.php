@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
    <div class="panel-body">
		<h3 class="text-success">Profile</h3>
		<hr>
	
        @if(Session::has('message'))
			{{ Session::get('message') }}

			{{ Session::put('message','') }}
		@endif
		
		<table class="table table-new table-c">
			<tbody>
				<tr>
					<td>Restaurant Name:</td>
					<td>{{ ucfirst($info['0']['f_name']).' '.$info['0']['l_name']  }}</td>
				</tr>
				<tr>
					<td>Mobile:</td>
					<td>{{$info['0']['mobile'] }}</td>
				</tr>
				<tr>
					<td>Email:</td>
					<td>{{$info['0']['email'] }}</td>
				</tr>
				<tr>
					<td>Address:</td>
					<td>{{$info['0']['add1'].' '.$info['0']['add2'] }}</td>
				</tr>
				<tr>
					<td>City:</td>
					<td>{{$info['0']['city_name'] }}</td>
				</tr>
				<tr>
					<td>State:</td>
					<td>{{$info['0']['state_name'] }}</td>
				</tr>
				<tr>
					<td>Pincode:</td>
					<td>{{$info['0']['pincode'] }}</td>
				</tr>
				<tr>
					<td>Country:</td>
					<td>{{$info['0']['country_name'] }}</td>
				</tr>
			</tbody>
		</table>
				
		<div class="form-group row">
			<div class="col-md-3"><a class="btn btn-success" href="{{url('rzone/profile/'.$info['0']['id'].'/edit')}}">Edit</a></div>
			<label class="col-md-9">&nbsp;</label>
		</div>
		
		
                
    </div>
</div>
</div>
@endsection

@section('script')
@endsection
