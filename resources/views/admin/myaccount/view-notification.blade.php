@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
 @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                <div id="right_side">
					<div class="panel-body">
						<h3 class="text-success">Notification</h3>
						<hr>
						<div class="notification_show">
						@if(count($notification))
                      @foreach($notification as $notifications)
							<aside>
								<h4><a class="noti_name<?=$notifications['id']?>" href="javascript:void(0)" onclick="noti(<?=$notifications['id']?>);"><?=$notifications['title']?></a></h4>
								<p class="noti_text<?=$notifications['id']?>"><?=$notifications['comment']?></p>
							</aside>
							
							@endforeach
							@endif
						</div>
						<hr>
						<a href="javascript:void(0);" class="btn btn-success See_More5">See More</a>
					</div> 
                </div>
            </div>
@endsection

@section('script')
<script>
        $(window).on("scroll resize load", function () {
            document.getElementById('left_side').style.height = "";
            var lefttHeight = document.getElementById('left_side').clientHeight;
            var rightHeight = document.getElementById('right_side').clientHeight;
            if(lefttHeight < rightHeight){
                document.getElementById('left_side').style.height = rightHeight+25+"px";
            }
            else{
                document.getElementById('left_side').style.height = lefttHeight+25+"px";
            }
        });
    </script>
	<script>
		
			function noti(num){

				$('.notification_show p').slideUp();
				
				if($('.noti_name'+num).hasClass('open')){
					$('.noti_text'+num).slideUp();	
					$('.noti_name'+num).removeClass('open');
				}
				else{
					$('.noti_text'+num).slideDown();	
					$('.noti_name'+num).addClass('open')
				}
			}
		
		$(document).ready(function(){
			$('.See_More5').click(function(){
				
				
				  
                var count = $(".notification_show aside").length;
				//alert(count);
				$.ajax({
                url: '{{url('showmore')}}',
                type: "GET",
                data: {'count':count},
                success: function(res)
                {
                    var received =jQuery.parseJSON(res);
					
                    $(".notification_show").append(received['data']);
             
                    
                }
              });
				
			});
		});

	</script>
@endsection
