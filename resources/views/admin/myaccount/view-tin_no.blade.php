@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
    <div class="panel-body">
      @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
				<?php 
				if($info['0']['tin_number']=='')
				{
					$tin_no = 'No record found';
				}
				else
				{
					$tin_no = $info['0']['tin_number'];
				}
				?>
                <table class="table-condensed">
											<tr>
												<th>Tin Invoice Number:</th>
												<td>{{ $tin_no }}</td>
											</tr>
											<tr>
												<th>
												 <a class="btn btn-success" href="{{url('rzone/taxinvoicenumber/'.$info['0']['id'].'/edit')}}">Edit</a>
												</th>
											</tr>
		        </table>   
    </div>
</div>
</div>
@endsection

@section('script')
@endsection
