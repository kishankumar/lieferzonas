@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
    <div class="panel-body">
    
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    @foreach( $errors->all()  as $error)

                    <li> {{$error}} </li>
                    @endforeach
                @endif
                
{!! Form::model($info,['route'=>['rzone.profile.update',$info['0']['id']],'method'=>'patch',
'id' => 'edit_profile','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}
        <!--<form action="#" method="post" accept-charset="utf-8">-->
		
		
    
			
            <div class="errors" id="errors" style="color: red"></div>
            
            <div class="row">
                <div class="col-md-5 form-group">
					{!! Form::label('Name') !!}<span class="red">*</span>
					{!! Form::text('f_name',$info['0']['f_name'],["id"=>'f_name',"class"=>"form-control"]) !!}
                </div>
                {!! Form::hidden('rest_id',$info['0']['id'],['id'=>'rest_id']) !!}
				{!! Form::hidden('email_id',$info['0']['email_id'],['id'=>'email_id']) !!}
				{!! Form::hidden('mobile_id',$info['0']['mobile_id'],['id'=>'mobile_id']) !!}
               
				<div></div>
            </div>
			
			<div class="row">
                <div class="col-md-5 form-group">
                    {!! Form::label('Email') !!}<span class="red">*</span>
                    {!! Form::text('email',$info['0']['email'],["id"=>'email',"class"=>"form-control"]) !!}
                </div>
				<div class="col-md-5 form-group">
                    {!! Form::label('Mobile no') !!}<span class="red">*</span>
                    {!! Form::text('mobile_no',$info['0']['mobile'],["id"=>'mobile_no',"class"=>"form-control"]) !!}
                </div>
				<div></div>
            </div>
			
			 <div class="row">
               
				<div class="col-md-5 form-group">
					{!! Form::label('Country') !!}<span class="red">*</span>
					<select id="country" name="country" class="form-control js-example-basic-multiple"  onchange="getData(this.value,'countries','states','state','id','name','id','country_id')">
					  <option value="">Select Country</option>
						 @foreach($country as $country)
					  <option <?php if($country['id'] == $info['0']['country']) echo "Selected='selected'" ;?> 
								value="<?=$country['id'] ?>"><?=$country['name'] ?>
					  </option>
						@endforeach  
					</select>
				 </div>
				 <div></div>
				    <div class="col-md-5 form-group">
					{!! Form::label('State') !!}<span class="red">*</span>
					 <select id="state" name="state" class="form-control js-example-basic-multiple" onchange="getData(this.value,'states','cities','city','id','name','id','state_id')">
					  <option value="">Select State</option>
						 @foreach($state as $state)
								<option <?php if($state['id'] == $info['0']['state']) echo "Selected='selected'" ;?> 
								value="<?=$state['id'] ?>"><?=$state['name'] ?>
						</option>
							@endforeach
					</select>
                 </div>
               </div>
               <div class="row">
              
				 <div class="col-md-5 form-group">
						{!! Form::label('City') !!}<span class="red">*</span>
					   <select id="city" name="city" class="form-control js-example-basic-multiple" onchange="getData(this.value,'cities','zipcodes','zipcode','id','name','id','city_id')">

						<option value="">Select State</option>
							 @foreach($city as $city)
									<option <?php if($city['id'] == $info['0']['city']) echo "Selected='selected'" ;?> 
									value="<?=$city['id'] ?>"><?=$city['name'] ?>
							</option>
								@endforeach     
						</select>
				  </div>
                  <div></div> 
					  <div class="col-md-5 form-group">
						{!! Form::label('zipcode') !!}<span class="red">*</span>
						<select id="zipcode" name="zipcode" class="form-control js-example-basic-multiple" >
                               <option value="">Select Zipcode</option>
                                 @foreach($zipcode as $zipcode)
                                        <option <?php if($zipcode['id'] == $info['0']['pincode']) echo "Selected='selected'" ;?> 
                                        value="<?=$zipcode['id'] ?>"><?=$zipcode['name'] ?>
                                </option>
                                    @endforeach 
                                
                           </select>
					 </div>				  
                </div>
				<div class="row">
					 <div class="col-md-5 form-group">
						{!! Form::label('Address1') !!}<span class="red">*</span>
						{!! Form::text('address1',$info['0']['add1'],["id"=>'address1',"class"=>"form-control"]) !!}
					 </div>
					<div class="col-md-5 form-group">
						{!! Form::label('Address2') !!}
						{!! Form::text('address2',$info['0']['add2'],["id"=>'address2',"class"=>"form-control"]) !!}
					</div>
					<div></div>
				</div>
            
			<div class="form-group margin-top">
               {!!Form::submit('Save',["id"=>'submit',"class"=>"btn  btn-success"]) !!}
                <hr>
				<p><span class="red">*</span> - Required Fields.</p>
            </div>
        
   
		
		
		
            {!! Form::close() !!}
              
	</div>       
    </div>
</div>

@endsection

 @section('script')
 
     <script type="text/javascript"> 
     $(document).ready(function(){
         
        $.validator.addMethod("email", function(value, element)
	{
            return this.optional(element) || /^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
	}, "Please enter valid email address.");
        
     $("#edit_profile").validate({
		
      rules: {
        f_name: {
            required: true
               },
        l_name: {
            required: true
               },
        
       
         email: {
            required: true,
            email:true
               },
		 mobile_no: {
			 required:true,
			 digits:true,
			 minlength:10,
             maxlength:12
		 },
         
         zipcode: {
            required: true
               },
         country: {
            required: true
               },
         state: {
            required: true
               },			   
         city: {
            required: true
               },
         address1: {
            required: true
               }			   
         
         
        },
       messages: {
                    f_name: "Please enter firstname",
                    l_name: "Please enter lastname",
                    email:{
                        required: "Please enter email",
                        email: "Please enter valid email"
                    },
                    mobile_no:{
                        required: "Please enter mobile no",
                        digits: "Please enter valid mobile no",
						minlength: "Please enter valid phone number",
                        maxlength: "Please enter valid phone number"
                    },
                    zipcode: "Please enter zipcode",
					country: "Please enter country",
					state: "Please enter state",
					city: "Please enter city",
                    address1: "Please enter address",
                    
                    agree: "Please accept our policy"
                },
         
     });
});
    
     function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn) {
    
         $.ajax({

                url: '{{url('getdata')}}',
                type: "get",
                data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn},
                
             success: function (data) {
             
           $('#'+appendId).html(data);
           
          },
             error: function (jqXHR, textStatus, errorThrown) {
               
             }
         });
   
      }
    </script>

    @endsection
