@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
    <div class="panel-body">
    
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    @foreach( $errors->all()  as $error)

                    <li> {{$error}} </li>
                    @endforeach
                @endif
                
{!! Form::model($info,['route'=>['rzone.taxinvoicenumber.update',$info['0']['id']],'method'=>'patch',
'id' => 'edit_tin_no','class' => 'form','novalidate' => 'novalidate', 'files' => true])  !!}
            <!--<form action="#" method="post" accept-charset="utf-8">-->


            <div class="errors" id="errors" style="color: red"></div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    {!! Form::label('Tin Number') !!}<span class="red">*</span>
                    {!! Form::text('tin_no',$info['0']['tin_number'],["id"=>'tin_no',"class"=>"form-control"]) !!}
                  
                    </div>

                </div>
                {!! Form::hidden('rest_id','',['id'=>'rest_id']) !!}
               
            </div>
			
			
			
               
				
            
            <div class="form-group margin-top">
               {!!Form::submit('Save',["id"=>'submit',"class"=>"btn  btn-success"]) !!}
                <hr>
				<p><span class="red">*</span> - Required Fields.</p>
            </div>
   
            {!! Form::close() !!}
              
           
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript"> 
     $(document).ready(function(){
     $("#edit_tin_no").validate({
		
      rules: {
        tin_no: {
            required: true
               }
       
        },
       messages: {
                    tin_no: "Please enter tin number",
                    
                    agree: "Please accept our policy"
                },
         
     });
});
</script>
@endsection
