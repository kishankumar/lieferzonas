@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
        <div class="panel-body">
            <h3 class="text-success"><i class="fa fa-tags"></i> Sub Menu Detail
                <span class="pull-right">
                        <a class="btn btn-success" href="{{url('rzone/menu')}}">Back</a>
                </span>
            </h3>
            <hr>
            
        @if(count($viewDetails))
            @foreach($viewDetails as $info)
			
			<table class="table table-new table-c">
				<tbody>
                    <tr>
                        <td width="25%">Sub Menu Name :</td>
                        <td width="75%">{{ ucfirst($info->name)  }}</td>
                    </tr>
                    <tr>
                        <td>Restaurant Name :</td>
                        <td>
                            {{$info->getResturant->f_name}} {{$info->getResturant->l_name}}  
                        </td>
                    </tr>
                    <tr>
                        <td>Image :</td>
                        <td>
                            {!! Html::image('public/uploads/superadmin/submenu/'.$info->image, '', array('class'=>'img-responsive','style'=>'width: 150px;')) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Menu Name :</td>
                        <td>{{$info->getMenu->name}}</td>
                    </tr>
                    
                    <tr>
                        <td>Short Name :</td>
                        <td>{{$info->short_name}}</td>
                    </tr>
                    <tr>
                        <td>Sub Menu Code :</td>
                        <td>
                            {{$info->code}} 
                        </td>
                    </tr>
                    <tr>
                        <td>Status :</td>
                        <td>
                            @if($info->status == 1) 
                                <i class="fa fa-check text-success text-success"></i>
                            @else
                                <i class="fa fa-ban text-danger text-danger" ></i>
                            @endif
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Is Alergic :</td>
                        <td>@if($info->is_alergic){{'Yes'}}@else{{'No'}} @endif</td>
                    </tr>
                    <tr>
                        <td>Is Spicy :</td>
                        <td>
                            @if($info->is_spicy){{'Yes'}}@else{{'No'}} @endif 
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Delivery Price :</td>
                        <td>
                            &euro; {{ \App\superadmin\RestMenu::getSubMenuDeliveryTakePrice($info->id,1,$info->rest_detail_id) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Pickup Price :</td>
                        <td>
                            &euro; {{ \App\superadmin\RestMenu::getSubMenuDeliveryTakePrice($info->id,2,$info->rest_detail_id) }}  
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Description :</td>
                        <td>{{$info->description}}</td>
                    </tr>
                    <tr>
                        <td>Out of Stock</td>
                        <td>
                            @if($info->out_of_stock){{'Yes'}}@else{{'No'}} @endif 
                        </td>
                    </tr>
					</tbody>
				</table>
            @endforeach
        @endif
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
