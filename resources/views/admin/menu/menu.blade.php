@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection


@section('body')
    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                <h3 class="text-success" style="margin:0">Menu</h3>
            </div>
            
            @if(Session::has('message'))
                <div class="alert alert-success" style="padding: 7px 15px;">
                    {{ Session::get('message') }}
                    {{ Session::put('message','') }}
                </div>
            @endif
            
            <div class="table-responsive">
                <table class="table table-new table-c">
                    <thead>
                        <tr class="bg-danger">
                            <th width="14%" style="text-align:left !important;">Product name</th>
                            <th width="40%" style="text-align:left !important;">Product Description</th>
                            <th width="12%" style="text-align:right !important;">Delivery</th>
                            <th width="12%" style="text-align:right !important;">Pickup</th>
                            <th width="12%" style="text-align:right !important;">Out of Stock</th>
                            <th width="10%" style="text-align:right !important;">Views</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $display=''; $display1=''; 
                        ?>
                        @foreach($getRestroMenus as $Menu)
                            @if($Menu->out_of_stock == 1)
                                {{--*/ $display='line-through' /*--}}
                                {{--*/ $checked='checked' /*--}}
                            @else
                                {{--*/ $display='' /*--}}
                                {{--*/ $checked='' /*--}}
                            @endif
                            <tr>
                                <td><bdo style="text-decoration: <?=$display;?>">{{$Menu->name}}</bdo></td>
                                <td>
                                    <span class="MenuDes" style="display:none">
                                        {{$Menu->description}}
                                    </span>
                                    @if(strlen($Menu->description) > 30)
									<span class="MenuDes30">
                                        {{ substr($Menu->description,0,30) }}
									</span>
                                        <a href="javascript:void(0)" class="red Menuclick">Read More</a>
                                    @else
                                        {{$Menu->description}}
                                    @endif
                                </td>
                                <td style="text-align:right !important;">&euro; {{ \App\superadmin\RestMenu::getMenuDeliveryTakePrice($Menu->id,1,$restId) }} </td>
                                <td style="text-align:right !important;">&euro; {{ \App\superadmin\RestMenu::getMenuDeliveryTakePrice($Menu->id,2,$restId) }}</td>
                                <td style="text-align:center !important;">
                                    <input type="checkbox" name="menus" id="<?=$Menu->id;?>" value="menu" class="name_cut" <?=$checked;?>>
                                </td>
                                <td style="text-align:center !important;">
                                    <a href="{{url('rzone/menu/'.$Menu->id.'/')}}?type=menu">Detail</a>
                                </td>
                            </tr>
                        @endforeach
                        
                        @foreach($getRestroSubMenus as $SubMenu)
                            @if($SubMenu->out_of_stock == 1)
                                {{--*/ $display1='line-through' /*--}}
                                {{--*/ $checked1='checked' /*--}}
                            @else
                                {{--*/ $display1='' /*--}}
                                {{--*/ $checked1='' /*--}}
                            @endif
                            <tr>
                                <td><bdo style="text-decoration: <?=$display1;?>">{{$SubMenu->name}}</bdo></td>
                                <td>
                                    <span class="MenuDes" style="display:none">
                                        {{$SubMenu->description}}
                                    </span>
                                    @if(strlen($SubMenu->description) > 30)
									<span class="MenuDes30">
                                        {{ substr($SubMenu->description,0,30) }}
									</span>
                                        <a href="javascript:void(0)" class="red Menuclick">Read More</a>
                                    @else
                                        {{$SubMenu->description}}
                                    @endif
                                </td>
                                <td style="text-align:right !important;">&euro; {{ \App\superadmin\RestMenu::getSubMenuDeliveryTakePrice($SubMenu->id,1,$restId) }} </td>
                                <td style="text-align:right !important;">&euro; {{ \App\superadmin\RestMenu::getSubMenuDeliveryTakePrice($SubMenu->id,2,$restId) }}</td>
                                <td style="text-align:center !important;">
                                   <input type="checkbox" name="submenus" id="<?=$SubMenu->id;?>" value="submenu" class="name_cut" <?=$checked1;?>>
                                </td>
                                <td style="text-align:center !important;">
                                    <a href="{{url('rzone/menu/'.$SubMenu->id.'/')}}?type=submenu">Detail</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection                    
        
@section('script')
<script>
    $(document).ready(function(){
        $('.name_cut').click(function(){
            var menuId = $(this).attr('id');
            var menuType = $(this).val();
            if(this.checked != true){
                var outStock = 0;
            }
            else{
                var outStock = 1;
            }
            if(this.checked != true){
                $(this).parent().parent().find('bdo').css( "text-decoration", "" );	
            }
            else{
                $(this).parent().parent().find('bdo').css( "text-decoration", "line-through" );	
            }
            var base_host = window.location.origin;  //give http://localhost
            var base_url = base_host + SITE_URL_PRE_FIX + '/rzone';
            $.ajax({
                url: base_url+'/menu/checkstock',
                type: "POST",
                dataType:"json",
                data: {'menuId':menuId, 'menuType':menuType, 'outStock':outStock, "_token":"{{ csrf_token() }}"},
                success: function(res) {
                    if(res.success==1){
                        
                    }
                    else{
                        alert('some error comming');
                    }
                }
            });
        });
    });
    
    $(window).on("scroll resize load", function () {
        document.getElementById('left_side').style.height = "";
        var lefttHeight = document.getElementById('left_side').clientHeight;
        var rightHeight = document.getElementById('right_side').clientHeight;
        if(lefttHeight < rightHeight){
            document.getElementById('left_side').style.height = rightHeight+25+"px";
        }
        else{
            document.getElementById('left_side').style.height = lefttHeight+25+"px";
        }
    });
	

</script>

<script>
	$(document).ready(function(){
		$('.Menuclick').click(function(){
			$(this).hide();
			$(this).parent().parent().find('.MenuDes').show();
			$(this).parent().parent().find('.MenuDes30').hide();
		});
	});
</script>


@endsection
