@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
        <div class="panel-body">
            <h3 class="text-success"><i class="fa fa-tags"></i> Sub Menu Detail
                <span class="pull-right">
                        <a class="btn btn-success" href="{{url('rzone/menu')}}">Back</a>
                </span>
            </h3>
            <hr>
            
        @if(count($viewDetails))
            @foreach($viewDetails as $info)
                <div class="row form-horizontal">
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Sub Menu Name :</label></div>
                        <div class="col-md-6">{{ ucfirst($info->name)  }}</div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Restaurant Name :</label></div>
                        <div class="col-md-6">
                            {{$info->getResturant->f_name}} {{$info->getResturant->l_name}}  
                        </div>
                    </div>
                    
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Image :</label></div>
                        <div class="col-md-6">
                            {!! Html::image('public/uploads/superadmin/submenu/'.$info->image, '', array('class'=>'img-responsive')) !!}
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Menu Name :</label></div>
                        <div class="col-md-6">{{$info->getMenu->name}}</div>
                    </div>
                    
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Short Name :</label></div>
                        <div class="col-md-6">{{$info->short_name}}</div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Sub Menu Code :</label></div>
                        <div class="col-md-6">
                            {{$info->code}} 
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Status :</label></div>
                        <div class="col-md-6">
                            @if($info->status == 1) 
                                <i class="fa fa-check text-success text-success"></i>
                            @else
                                <i class="fa fa-ban text-danger text-danger" ></i>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Is Alergic :</label></div>
                        <div class="col-md-6">@if($info->is_alergic){{'Yes'}}@else{{'No'}} @endif</div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Is Spicy :</label></div>
                        <div class="col-md-6">
                            @if($info->is_spicy){{'Yes'}}@else{{'No'}} @endif 
                        </div>
                    </div>
                    
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Delivery Price :</label></div>
                        <div class="col-md-6">
                            &euro; {{ \App\superadmin\RestMenu::getSubMenuDeliveryTakePrice($info->id,1,$info->rest_detail_id) }}
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Pickup Price :</label></div>
                        <div class="col-md-6">
                            &euro; {{ \App\superadmin\RestMenu::getSubMenuDeliveryTakePrice($info->id,2,$info->rest_detail_id) }}  
                        </div>
                    </div>
                    
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Description :</label></div>
                        <div class="col-md-6">{{$info->description}}</div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="col-md-6"><label>Out of Stock</label></div>
                        <div class="col-md-6">
                            @if($info->out_of_stock){{'Yes'}}@else{{'No'}} @endif 
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
