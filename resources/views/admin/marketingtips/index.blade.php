@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')

<div class="col-sm-9">
    <div id="right_side">
        <div class="panel-body">
            <h3 class="text-success">Marketing Tips </h3>
        
    
    @if(Session::has('message'))
        {{ Session::get('message') }}
        {{ Session::put('message','') }}
    @endif
    
            <!--<h3 class="text-success">Notification</h3>-->
            <hr>
            <div class="notification_show">
                @if(count($tips))
                    @foreach($tips as $tip)
                        <aside>
					<h4><a class="noti_name<?=$tip['id']?>" href="javascript:void(0)" onclick="noti(<?=$tip['id']?>);"><?=$tip['topic']?></a></h4>
                            <p class="noti_text<?=$tip['id']?>"><?=$tip['description']?></p>
                        </aside>
                    @endforeach
                @else 
                    No Tips Exist
                @endif
            </div>
            {!! $tips->render() !!}
          
        </div> 
    </div>
</div>

@endsection

@section('script')
<script>
    $(window).on("scroll resize load", function () {
        document.getElementById('left_side').style.height = "";
        var lefttHeight = document.getElementById('left_side').clientHeight;
        var rightHeight = document.getElementById('right_side').clientHeight;
        if(lefttHeight < rightHeight){
            document.getElementById('left_side').style.height = rightHeight+25+"px";
        }
        else{
            document.getElementById('left_side').style.height = lefttHeight+25+"px";
        }
    });
</script>
<script>
    function noti(num){
        $('.notification_show p').slideUp();
        if($('.noti_name'+num).hasClass('open')){
            $('.noti_text'+num).slideUp();	
            $('.noti_name'+num).removeClass('open');
        }
        else{
            $('.noti_text'+num).slideDown();	
            $('.notification_show h4 a').removeClass('open');
            $('.noti_name'+num).addClass('open')
        }
    }
</script>
@endsection
