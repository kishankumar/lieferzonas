@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')
    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                <h3 class="text-success">Change Category Image
                </h3><hr>
                
                @if(Session::has('message'))
                    <div class="alert alert-success" style="padding: 7px 15px;">
                        {{ Session::get('message') }}
                        {{ Session::put('message','') }}
                    </div>
                @endif
                
                {{--    Error Display--}}
                    @if($errors->any())
                        <div class="alert alert-danger ">
                            <ul class="list-unstyled">
                                @foreach($errors->all() as $error)
                                    <li> {{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                {{--    Error Display ends--}}
                
                {!! Form::open(array('route' => 'rzone.category.image.change.store','id'=>'change_image','class' => 'form-horizontal','novalidate' => 'novalidate', 'files' => true)) !!}
                    <div class="form-group">
                         <div class="col-md-6">
                        {!! Form::label('Category') !!} 
                        <select class="form-control" name="category">
                            <option value="">Select</option>
                            @foreach($getRestCategories as $Categories)
                                <option value="{{$Categories->id}}" >{{ucfirst($Categories->name)}}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('Category Image') !!}
                            {!! Form::file('image','',array('id'=>'image','class'=>'form-control')) !!}
                        </div>
                    </div>
                    
                    <div class="form-group col-md-12">
                        {!! Form::submit('Change',["id"=>'submit',"class"=>"btn  btn-success"]) !!}
                        <hr>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    $(document).ready(function(){
        // Accept a value from a file input based on a required mimetype
        jQuery.validator.addMethod("accept", function(value, element, param) {
            var typeParam = typeof param === "string" ? param.replace(/\s/g, '').replace(/,/g, '|') : "image/*",
            optionalValue = this.optional(element),
            i, file;
            if (optionalValue) {
                    return optionalValue;
            }
            if ($(element).attr("type") === "file") {
                typeParam = typeParam.replace(/\*/g, ".*");
                if (element.files && element.files.length) {
                    for (i = 0; i < element.files.length; i++) {
                        file = element.files[i];
                        // Grab the mimetype from the loaded file, verify it matches
                        if (!file.type.match(new RegExp( ".?(" + typeParam + ")$", "i"))) {
                                return false;
                        }
                    }
                }
            }
            return true;
        }, "Please enter a value with a valid mimetype.");
        
        $("#change_image").validate({
            rules: {
                category: {
                    required : true,
                },
                image: {
                    required: true,
                    accept: "image/jpg,image/jpeg,image/png,image/gif",
                },
            },
            messages: {
                category:{
                    required: "Please select category",
                },
                image:{
                    required: "Please upload image",
                    accept: 'Not an image!'
                },
            },
        });
   });
</script>
@endsection
   
   


