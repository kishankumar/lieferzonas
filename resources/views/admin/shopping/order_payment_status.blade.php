@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')
    <div class="col-sm-9">
        <br>
        @if($payment_status =='AUTHORISED')
            <div class="well text-center">
                {!!Html::image('resources/assets/admin/img/tick.png','')!!}
                <h3 class="green"> Your payment has been Authorised. <br>
                    <small>Here are the details of this transaction for your reference.</small>
                </h3>
                <hr>
                <p><strong>Order Number :</strong> {{$order_id}}</p>
                <p><strong>Amount Paid :</strong> &euro; {{$grand_total}}</p>
                <p><strong>Reference Number :</strong> {{$pspReference}}</p>
                <br>
                <a class="btn btn-green2" href="{{url('rzone/shop/orders')}}"><strong>Home</strong></a>
            </div>
        @elseif($payment_status ==  'REFUSED')
            <div class="well text-center">
                {!!Html::image('resources/assets/admin/img/alert.png','')!!}
                <h3 class="green"> Your payment has been Refused. <br>
                    <small>Here are the details of this transaction for your reference.</small>
                </h3>
                <hr>
                <a class="btn btn-green2" href="{{url('rzone/shop/orders')}}"><strong>Home</strong></a>
            </div>
        @elseif($payment_status == 'CANCELLED')
            <div class="well text-center">
                {!!Html::image('resources/assets/admin/img/cancel.png','')!!}
                <h3 class="green"> Your payment has been Cancelled. <br>
                    <small>Here are the details of this transaction for your reference.</small>
                </h3>
                <hr>
                <a class="btn btn-green2" href="{{url('rzone/shop/orders')}}"><strong>Home</strong></a>
            </div>
        @elseif($payment_status == 'PENDING')
            <div class="well text-center">
                {!!Html::image('resources/assets/admin/img/pending.png','')!!}
                <h3 class="green"> Your payment is pending. <br>
                    <small>Here are the details of this transaction for your reference.</small>
                </h3>
                <hr>
                <a class="btn btn-green2" href="{{url('rzone/shop/orders')}}"><strong>Home</strong></a>
            </div>
        @elseif($payment_status == 'PENDING')
            <div class="well text-center">
                {!!Html::image('resources/assets/admin/img/pending.png','')!!}
                <h3 class="green"> Your payment has been Cancelled. <br>
                    <small>Here are the details of this transaction for your reference.</small>
                </h3>
                <hr>
                <a class="btn btn-green2" href="{{url('rzone/shop/orders')}}"><strong>Home</strong></a>
            </div>
        @else
            <div class="well text-center">
                {!!Html::image('resources/assets/admin/img/alert.png','')!!}
                <h3 class="green"> Error is coming in payment. <br>
                    <small>Here are the details of this transaction for your reference.</small>
                </h3>
                <hr>
                <a class="btn btn-green2" href="{{url('rzone/shop/orders')}}"><strong>Home</strong></a>
            </div>
        @endif
    </div>
@endsection                    
         
@section('script')
<script>
    $(window).on("scroll resize load", function () {
        document.getElementById('left_side').style.height = "";
        var lefttHeight = document.getElementById('left_side').clientHeight;
        var rightHeight = document.getElementById('right_side').clientHeight;
        if(lefttHeight < rightHeight){
            document.getElementById('left_side').style.height = rightHeight+25+"px";
        }
        else{
            document.getElementById('left_side').style.height = lefttHeight+25+"px";
        }
    });
</script>
@endsection
