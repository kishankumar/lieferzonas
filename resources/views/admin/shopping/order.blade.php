@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection


@section('body')

    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                <h3 class="text-success">Shop Orders</h3>
            </div>
            
            @if(Session::has('message'))
                {{ Session::get('message') }}

                {{ Session::put('message','') }}
            @endif
            
			<table class="table table-new text-center table-c">
				<thead>
					<tr class="bg-danger">
						<th width="20%">Restaurant Name</th>
						<th width="10%">Order</th>
						<th width="10%">Price</th>
						<th width="10%">Order Date</th>
						<th width="15%">Order Status</th>
						<th width="20%">Address</th>
						<th width="15%">Views</th>
						<!--<th width="15%"></th>-->
					</tr>
				</thead>
				<tbody>
					@if(count($OrderInfo))
                                            @foreach($OrderInfo as $info)
                                                <?php
                                                $shopOrderAddress='';
                                                $shopOrderAddress = \App\admin\ShopOrder::get_shop_order_address("$info->order_id");
                                                $shopOrderStatus = \App\admin\ShopOrder::get_shop_order_status($info->shop_order_status_id);
                                                ?>
						<tr>
                                                    <td>{{ ucfirst($info->restaurant_address->f_name).' '.ucfirst($info->restaurant_address->l_name)  }}</td>
                                                    <td>{{ ($info->order_id)  }}</td>
                                                    <td>&euro; {{ number_format($info->grand_total,2)  }} </td>
                                                    <td>{{ date('l', strtotime($info->created_at)) }} <br>{{ date('d/m/Y', strtotime($info->created_at)) }} </td>
                                                    <td>{{ $shopOrderStatus }}</td>
                                                    <td>{{ $shopOrderAddress }} </td>
                                                    <td>
                                                        <a class="text-danger" href="javascript:void(0)" onclick='show_modal("show-orders-view","order-number","order-item-list","<?=@$info->order_id?>",<?=@$info->id?>,<?=@$info->rest_detail_id;?>)'>View Order
                                                        </a>
                                                    </td>
						</tr>
                                            @endforeach
					@else 
                                            <tr>
                                                <td colspan="7" align="center">
                                                        No Order Exist
                                                </td>
                                            </tr>
					 @endif
				</tbody>
</table>
			{!! $OrderInfo->render() !!}
        	
		</div>
    </div>

        

@endsection                    
         <div class="modal fade" id="show-orders-view" tabindex="-1" role="dialog" aria-labelledby="contactLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="panel panel-success theme-border">
                    <div class="panel-heading theme-bg">
                        <span id="order-number"></span>
                        <button type="button" id="modalclose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="panel-body"  id="order-item-list">

                    </div>
                </div>
            </div>
        </div>   
@section('script')
<script>
    
    function show_modal(modelId,orderNumber,orderList,orderId,ordertableId,restId) {
	$.ajax({
            url: '{{ url('rzone/shop/orders/getdata') }}',
            type: "post",
            data: {'orderId':orderId,'ordertableId':ordertableId,'restId':restId, "_token":"{{ csrf_token() }}"},
	    	success: function (data) {
                if(data=='' || data==null){
                    data='No items assigned';
                }
                else{
                    data=data;
                }
                document.getElementById(orderNumber).innerHTML = orderId;
                document.getElementById(orderList).innerHTML = data;
            }
	});
	$("#"+modelId).modal('show');
    }
    
    $(window).on("scroll resize load", function () {
        document.getElementById('left_side').style.height = "";
        var lefttHeight = document.getElementById('left_side').clientHeight;
        var rightHeight = document.getElementById('right_side').clientHeight;
        if(lefttHeight < rightHeight){
            document.getElementById('left_side').style.height = rightHeight+25+"px";
        }
        else{
            document.getElementById('left_side').style.height = lefttHeight+25+"px";
        }
    });
</script>
@endsection
