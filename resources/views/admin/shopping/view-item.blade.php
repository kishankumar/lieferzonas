@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
        <div class="panel-body">
            <h3 class="text-success">{{ ucfirst($info->name) }}</h3>
            <hr>
	
        @if(Session::has('message'))
            {{ Session::get('message') }}
            {{ Session::put('message','') }}
        @endif
		
        <table class="table table-new table-c">
            <tbody>
                <tr>
                    <td>Item Name:</td>
                    <td>{{ ucfirst($info->name) }}</td>
                </tr>
                <tr>
                    <td>Item Image:</td>
                    <td>
                        @if ($info->item_image)
                            {!! Html::image(asset('public/uploads/superadmin/shop/items/'.$info->item_image), '', array('class' => 'img-responsive','style'=>'display: inline-block;
                                max-width: 150px;
                                vertical-align: middle;')) !!}
                        @else 
                            {{ 'No-Image' }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Item Number:</td>
                    <td>{{ ucfirst($info->item_number) }}</td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td>{{ ($info->description) }}</td>
                </tr>
                <tr>
                    <td>Current Quantity:</td>
                    <td>{{ ($info->quantity) }}</td>
                </tr>
                <tr>
                    <td>Price:</td>
                    <td> &euro; {{ number_format($info->front_price,2) }} </td>
                </tr>
                <tr>
                    <td>Item Category:</td>
                    <td> {{ ucfirst($info->shop_category->name)  }}</td>
                </tr>
                <tr>
                    <td>Item Metrics:</td>
                    <td>{{ ucfirst($info->metric_name->name)  }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection

@section('script')
@endsection
