@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
    <div class="action_menu shopping_logo">
        <h4>
            {!!Html::image('resources/assets/admin/img/cart.png','')!!}
            Shopping <span><a href="javascript:void(0)" onclick="empty_cart()"><i class="fa fa-trash"></i></a></span> 
        </h4>
    </div>
    
    {!! Form::open(['url'=>'rzone/payment/address','method'=>'post','id'=>'shopping-cart']) !!}
    <!--{!! Form::open(array('route'=>'rzone.shop.cart.store','id'=>'shopping-cart')) !!}-->
    <div class="shopping_bag">
        <div style="height:300px;">
            <div class="nano">
                <div class="nano-content">
                    <div class="check_table">
            <?php
            if(count($CartInfo)){
                $per_item_price=0;$total_price=0;
                foreach($CartInfo as $Cinfo){
                    $per_item_price=round((($item_price[$Cinfo->shop_item_id])*$Cinfo->quantity),2);
                    $total_price=$total_price+$per_item_price; ?>
                    <div class="item_list shop_items ItemNo<?=$Cinfo->shop_item_id;?>">
                        <div class="left">
                            <span>{!! Form :: text("items[$Cinfo->shop_item_id]",$Cinfo->quantity,["id"=>"itemId-$Cinfo->shop_item_id",'readonly'=>'readonly','class'=>'cart_items form-control t_num','onchange'=>"change_price_cart($Cinfo->shop_item_id,$Cinfo->quantity)"])  !!}</span>&nbsp;
                            {{ ucfirst($Cinfo->item_name->name)  }}
                        </div>
                        <aside>
                            <a href="javascript:void(0)" onclick="edit_item_in_cart(<?=$Cinfo->shop_item_id;?>)"><i class="fa fa-pencil"></i> </a>
                            <a href="javascript:void(0)" onclick="plus_item_in_cart_items(<?=$Cinfo->shop_item_id;?>)"><i class="fa fa-plus"></i>   </a>
                            <a href="javascript:void(0)" onclick="minus_item_in_cart_items(<?=$Cinfo->shop_item_id;?>)"><i class="fa fa-minus"></i>  </a>
                            <a href="javascript:void(0)" onclick="remove_item_from_cart(<?=$Cinfo->shop_item_id;?>)"><i class="fa fa-trash"></i>  </a>
                            <br>
                            <strong id="price-<?=$Cinfo->shop_item_id;?>"><?=number_format($per_item_price,2);?> €</strong>
                        </aside>
                        <div class="clearfix"></div>
                        <div class="error" id="span<?=$Cinfo->shop_item_id;?>"></div>
                    </div>
            <?php 
                }
            }
            else{ ?>
                <div class="item_list">
                    No item Exist in cart.
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    </div>
    </div>
        <?php
            if(count($CartInfo)){ ?>
                <!--commented this portion because this is part of front order-->
<!--            <div class="item_list">
                <div class="left">Order:  </div>
                <aside id="order-price">
                    <?//=number_format($total_price,2);?> &euro;
                </aside>
                <div class="left">Minimum value:  </div>
                <aside id="minimum-price">
                    <?//=number_format($total_price,2);?> &euro;
                </aside>
            </div>-->
<!--            <div class="item_list">
                <div class="left">For minimum orders missing:  </div>
                <aside  id="min-order-price">
                    <?//=number_format($total_price,2);?> &euro;
                </aside>
                <div class="left">Free Shipping: </div>
                <aside id="free-shipping-price">
                    <?//=number_format($total_price,2);?> &euro;
                </aside>
            </div>-->
            <!--commented this portion because this is part of front order-->
            <div class="action_menu">
                <h4><strong>Total Amount : <bdo class="pull-right" id="total-cart-price"><?=number_format($total_price,2);?> &euro;</bdo></strong></h4>
            </div>
            <div class="item_list" style="border:none;">
                <a href="javascript:void(0)" onclick="cart_check_out()" class="btn btn-success btn-block">Proceed to Checkout</a>
                <!--<a href="javascript:void(0)" onclick="document.getElementById('adyen-encrypted-form').submit()" class="btn btn-success btn-block">To Checkout</a>-->
            </div>
            <div class="clearfix"></div>
        <?php } ?>
   
    </div>
    {!! Form :: close() !!}
    
@endsection

     
@section('body')
    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                <h3 class="text-success">Shop Items</h3>
<!--                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
            </div>
                
            <table class="table table-new text-center table-c">
                <thead>
                    <tr class="bg-danger">
                        <th width="9%">Item No.</th>
                        <th width="22%">Item Name</th>
                        <th width="23%">Description</th>
                        <th width="10%">Quantity</th>
                        <th width="10%">Pieces</th>
                        <th width="12%">Price</th>
                        <th width="11%">Special Price</th>
                        <th width="14%">Total Price</th>
                    </tr>
                </thead>
                <tbody style="text-align:left;">
                    
                @if(count($ItemInfo))
                    @foreach($ItemInfo as $info)
                        <tr>
                            <td>{{ ucfirst($info->item_number) }}</td>
                            <td>
                                <a href="{{url('rzone/shop/cart/'.$info->id)}}" class="text-danger">
                                    @if ($info->item_image)
                                        {!! Html::image(asset('public/uploads/superadmin/shop/items/'.$info->item_image), '', array('class' => 'img-responsive','style'=>'display: inline-block;
                                            max-width: 50px;
                                            vertical-align: middle;')) !!}
                                    @endif
                                    {{ ucfirst($info->name) }}
                                </a>
                            </td>
                            <td>{{ (strlen($info->description) > 20) ? substr($info->description,0,20).'...' : $info->description  }}</td>
                            <td>{{ ($info->quantity) }}</td>
                            <td>
                                {!! Form::text('quantity',1,["id"=>"quantity$info->id","style"=>'text-align:center','onchange'=>"change_price($info->id)","class"=>"form-control t_num"]) !!}
                                <!--<br><a href="javascript:void(0)" style="color:black"><i class="fa fa-refresh fa-2x"></i></a>-->
                            </td>
                            <td colspan="3" style="padding:0;">
                                <table class="table  text-left" style="margin:0;">
                                    <tr>
                                        <td width="33.3333%">{{ number_format($info->front_price,2) }} €</td>
                                        <td width="33.3333%"><a class="text-danger" href="#">0,00 €</a></td>
                                        <td width="33.3333%" id="total_price<?=$info->id?>">{{ number_format($info->front_price,2) }} €</td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0;" colspan="2">
                                            <a href="#" class="btn btn-danger">Note</a>
                                            <a href="javascript:void(0)" onclick="add_to_cart({{$info->id}})" class="AddCart btn btn-success">Add to Cart</a>
                                            <!--<a href="javascript:void(0)" id="AddCart<?//=$info->id?>" onclick="this.onclick = function(){};add_to_cart({{$info->id}});" class="btn btn-success">Add to Cart</a>-->
                                        </td>
                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection                    
            
@section('script')

<script>
$prices = '<?php echo json_encode($item_price);?>';
$prices = JSON.parse($prices);
$curr_quantity = '<?php echo json_encode($item_quantity);?>';
$curr_quantity = JSON.parse($curr_quantity);
function get_item_price(item_id){
    currPrice=0;
    $.each($prices,function( key, value ) {
        if(key==item_id)
            currPrice=value;
    });
    return currPrice;
}
function get_item_quantity(item_id){
    currQuan=0;
    $.each($curr_quantity,function( key, value ) {
        if(key==item_id){
            currQuan=value;
        }
    });
    return currQuan;
}
</script>

<script>
    
    function cart_check_out(){
        var count=0;
        $('.cart_items').each(function(){
            var str=$(this).attr('id');
            var res = str.split("-");
            var id = res[1];
            var val = $('#itemId-'+id).val();
            //var quanV=/^[0-9]+([.][0-9]{1,2})?$/;
            var quanV=/^[0-9]+$/;
            var validQuan=quanV.test(val);
            currQuan=get_item_quantity(id);
            if(val<1 || isNaN(val) || validQuan==false)
            {
                $('#span'+id).html('Enter quantity in digits.');
                $('#itemId-'+id).prop('readonly', false);
                count=1;
                return false;
            }else if(val>currQuan){
                $('#itemId-'+id).val(OldQuan);
                $('#span'+id).html('Quantity must be less than current quantity.');
                count=1;
                return false;
            }else{
                $('#span'+id).html('');
                //$('#shopping-cart').submit();
            }
        });
        
//        $(".cart_items").each(function() {
//            var quanV=/^[0-9]+([,.][0-9]{1,2})?$/;
//            var validQuan=quanV.test($(this).val());
//            if($(this).val()==''){
//                $(this).next().html("Please enter quantity");
//                count=1;
//            }
//            else if(validQuan==false)
//            {
//                $(this).next().html("Please enter valid quantity");
//                count=1;
//            }
//            else
//            {
//                $(this).next().html("");
//            }
//        });
        if(count==1){
            return false;
        }else{
            $('#shopping-cart').submit();
        }
    }
    
    function cart_calculation(ItemId,Quan=0,action=''){
        if(isNaN(ItemId))
            return false;
        $.ajax({
            url: '{{ url('rzone/shop/cart/updateCartItems') }}',  //working
            type: "post",
            dataType:"json",
            data: {'ItemId':ItemId,'quantity':Quan,'action':action, "_token":"{{ csrf_token() }}"},
            success: function(res){
                if(res.success==1)
                {
                    if(action=='edit' || action=='plus' || action=='minus'  || action=='addCart'){
                        $('.shopping_bag').html(res.html);
//                        new_amt = (parseFloat(res.PerItemAmount)).toFixed(2);
//                        totalAmount = (parseFloat(res.totalAmount)).toFixed(2);
//                        $('#price-'+ItemId).html(new_amt+' €');
//                        $('#order-price').html(totalAmount+' €');
//                        $('#minimum-price').html(totalAmount+' €');
//                        $('#min-order-price').html(totalAmount+' €');
//                        $('#free-shipping-price').html(totalAmount+' €');
//                        $('#total-cart-price').html(totalAmount+' €');
                    }else if(action=='Remove'){
//                        totalAmount = (parseFloat(res.totalAmount)).toFixed(2);
//                        $('#order-price').html(totalAmount+' €');
//                        $('#minimum-price').html(totalAmount+' €');
//                        $('#min-order-price').html(totalAmount+' €');
//                        $('#free-shipping-price').html(totalAmount+' €');
//                        $('#total-cart-price').html(totalAmount+' €');
                        $('.shopping_bag').html(res.html);
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }
        });
        
    }
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    function change_price_cart(id,OldQuan)
    {
        if(isNaN(id))
            return false;
        var val = $('#itemId-'+id).val();
        //var quanV=/^[0-9]+([.][0-9]{1,2})?$/;
        var quanV=/^[0-9]+$/;
        var validQuan=quanV.test(val);
        currQuan=get_item_quantity(id);
        if(val<1 || isNaN(val) || validQuan==false)
        {
            $('#span'+id).html('Enter quantity in digits.');
            $('#itemId-'+id).prop('readonly', false);
            return false;
        }else if(val>currQuan){
            $('#itemId-'+id).val(OldQuan);
            $('#span'+id).html('Quantity must be less than current quantity.');
            return false;
        }else{
            $('#span'+id).html('');
            $('#itemId-'+id).prop('readonly', true);
            cart_calculation(id,val,'edit');
        }
    }
    function edit_item_in_cart(id){
        $('#itemId-'+id).prop('readonly', false);
    }
    function plus_item_in_cart_items(id){
        var OldQuan = $('#itemId-'+id).val();
        var NewQuan=parseInt(OldQuan)+1;
        currQuan=get_item_quantity(id);
        if(NewQuan>currQuan){
            $('#span'+id).html('Quantity must be less than current quantity.');
            return false;
        }
        else{
            $('#span'+id).html('');
            $('#itemId-'+id).val(NewQuan);
            cart_calculation(id,NewQuan,'plus');
        }
    }
    function minus_item_in_cart_items(id){
        var OldQuan = $('#itemId-'+id).val();
        var NewQuan=parseInt(OldQuan)-1;
        if(NewQuan<1){
            $('#span'+id).html('Quantity must be gretaer than 0.');
            return false;
        }
        else{
            $('#span'+id).html('');
            $('#itemId-'+id).val(NewQuan);
            cart_calculation(id,NewQuan,'minus');
        }
    }
    function remove_item_from_cart(id){
        if (confirm("Are you sure to remove item from cart?")) {
            $('.ItemNo'+id).remove();
            cart_calculation(id,0,'Remove');
        }else{
            return false;
        }
    }
    function add_to_cart(id){
        if(isNaN(id))
            return false;
        var quantity = $('#quantity'+id).val();
        
        var quanV=/^[0-9]+$/;
        var validQuan=quanV.test(quantity);
        if(quantity<1 || isNaN(quantity) || validQuan==false)
        {
            $('#quantity'+id).val('1');
            alert('Enter quantity in digits.');
            return false;
        }
        var CurrItemId = id;
        var exist=0;
        $('.cart_items').each(function(){
            var str=$(this).attr('id');
            var res = str.split("-");
            var itemId = res[1];
            if(CurrItemId==itemId){
                exist=1;
            }
        });
        if(exist){
            alert('Item already exist in cart.');
            return false;
        }else{
            //$('#AddCart'+id).prop('disabled',false);
            $.ajax({
                url: '{{ url('rzone/shop/cart/getItemPrice') }}',  //working
                type: "post",
                dataType:"json",
                data: {'itemId':id,'quantity':quantity, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
//                        new_amt = (parseFloat(res.totalAmount)* parseFloat(quantity)).toFixed(2);
//                        var clone = '<div class="item_list shop_items ItemNo'+id+'">';
//                        clone+=     '<div class="left">';
//                        clone+=     '<span><input type="text" value="'+quantity+'" name="items[]" onchange="change_price_cart(\''+id+'\',\''+quantity+'\')" class="cart_items form-control t_num" readonly="readonly" id="itemId-'+id+'"></span>';
//                        clone+=     ''+res.ItemName+'</div><aside>';
//                        clone+=     '<a onclick="edit_item_in_cart('+id+')" href="javascript:void(0)"><i class="fa fa-pencil"></i> </a>';
//                        clone+=     '<a onclick="plus_item_in_cart_items('+id+')" href="javascript:void(0)"><i class="fa fa-plus"></i>   </a>';
//                        clone+=     '<a onclick="minus_item_in_cart_items('+id+')" href="javascript:void(0)"><i class="fa fa-minus"></i>  </a>';
//                        clone+=     '<a onclick="remove_item_from_cart('+id+')" href="javascript:void(0)"><i class="fa fa-trash"></i>  </a>';
//                        clone+=     '<br>';
//                        clone+=     '<strong id="price-'+id+'">'+new_amt+' €</strong>';
//                        clone+=     '</aside>';
//                        clone+=     '<span id="span'+id+'" style="color:red"></span>';
//                        clone+=     '</div>';
//                        $('.check_table').append(clone);
                        cart_calculation(id,quantity,'addCart');
                    }else{
                        alert(res.message);
                        return false;
                    }
                }
            });
		left_right_side();
        }
    }
    
    function empty_cart(){
		
        var len = $('.shop_items').length;
        if(len>0){
            var selectedItems = new Array();
            cartItems='';
            $('.cart_items').each(function(){
                var str=$(this).attr('id');
                var res = str.split("-");
                var itemId = res[1];
                cartItems+=itemId+',';
                selectedItems.push(itemId);
            });
            cartItems = cartItems.slice(0, -1);
            if (confirm("Are you sure to empty cart?")) {
                $.ajax({
                    url: '{{ url('rzone/shop/cart/removeCartItems') }}',  //working
                    type: "post",
                    dataType:"json",
                    data: {'cartItems':cartItems,'selectedItems':selectedItems, "_token":"{{ csrf_token() }}"},
                    success: function(res){
                        if(res.success==1)
                        {
                            $('.shopping_bag').html('');
                            $('.shopping_bag').html('<div class="check_table"><div class="item_list">No item Exist in cart.</div></div>');
                            alert('Cart is empty now');
                        }else{
                            alert('Cart is not empty');
                        }
                    }
                });
            }
		left_right_side();
        }else{
            alert('No item added in cart.');
            return false;
        }
    }
    
    function change_price(id)
    {
        if(isNaN(id))
            return false;
        var val = $('#quantity'+id).val();
        //var quanV=/^[0-9]+([.][0-9]{1,2})?$/;
        var quanV=/^[0-9]+$/;
        var validQuan=quanV.test(val);
        if(val<1 || isNaN(val) || validQuan==false)
        {
            $('#quantity'+id).val('1');
            alert('Quantity must be greater than or equal to 1');
            return false;
        }
        else
        {
            $.ajax({
                url: '{{ url('rzone/shop/cart/getItemPrice') }}',  //working
                type: "post",
                dataType:"json",
                data: {'itemId':id,'quantity':val, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
                    {
                        new_amt = (parseFloat(res.totalAmount)* parseFloat(val)).toFixed(2);
                        $('#total_price'+id).html(new_amt+' €');
                    }else{
                        $('#quantity'+id).val('1');
                        alert('Quantity must be less than current quantity');
                    }
                }
            });
        }
    }
    
   function left_right_side() {
	   setTimeout(function() {
        document.getElementById('left_side').style.height = "";
        var lefttHeight = document.getElementById('left_side').clientHeight;
        var rightHeight = document.getElementById('right_side').clientHeight;
        if(lefttHeight < rightHeight){
            document.getElementById('left_side').style.height = rightHeight+25+"px";
        }
        else{
            document.getElementById('left_side').style.height = lefttHeight+25+"px";
        }
		   }, 800);
    }
</script>
@endsection


<style>
    .shopping_bag input  {
        border:none;
        
    }
    .shopping_bag .t_num {
        text-align: center;
        background: none;
        border:none;
        width:28px;
    }
</style>