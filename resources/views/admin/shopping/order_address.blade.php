@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    {{ Session::put('message','') }}
                @endif
                
                @if($errors->any())
                    <ul>
                        @foreach( $errors->all()  as $error)
                            <li style="color: red"> {{$error}} </li>
                        @endforeach
                    </ul>
                @endif
                
                {!! Form::open(array('route'=>'rzone.shop.cart.store','method'=>'post','id'=>'edit_profile')) !!}
                    <div class="errors" id="errors" style="color: red"></div>
                    <div class="checkbox icheck">
                        <label>{!! Form::checkbox('use_old_address','1',null,['id'=>'use_old_address','class'=>'use_old_address','onclick'=>'showValue()']) !!} 
                            <span>Use Restaurant address as order address.</span>
                        </label>
                    </div>
                    <div class="new_Address" style="display: block">
                        <div class="row">
                            <div class="col-md-5 form-group">
                                {!! Form::label('First name') !!}
                                {!! Form::text('f_name','',["id"=>'f_name',"class"=>"form-control"]) !!}
                            </div>
                            <div class="col-md-5 form-group">
                                {!! Form::label('Last name') !!}
                                {!! Form::text('l_name','',["id"=>'l_name',"class"=>"form-control"]) !!}
                            </div>
                        </div>
			
			<div class="row">
                            <div class="col-md-5 form-group">
                                {!! Form::label('Mobile no') !!}
                                {!! Form::text('mobile_no','',["id"=>'mobile_no',"class"=>"form-control"]) !!}
                            </div>
                            <div class="col-md-5 form-group">
                                {!! Form::label('Country') !!}
                                <select id="country" name="country" class="form-control"  onchange="getData(this.value,'countries','states','state','id','name','id','country_id')">
                                    <option value="">Select Country</option>
                                    @foreach($country as $country)
                                        <option value="<?=$country['id'] ?>"><?=$country['name'] ?></option>
                                    @endforeach  
                                </select>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-5 form-group">
                                {!! Form::label('State') !!}
                                <select id="state" name="state" class="form-control" onchange="getData(this.value,'states','cities','city','id','name','id','state_id')">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                            <div class="col-md-5 form-group">
                                {!! Form::label('City') !!}
                                <select id="city" name="city" class="form-control" >
                                    <option value="">Select State</option>
                                </select>
                             </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 form-group">
                                {!! Form::label('Address1') !!}
                                {!! Form::text('add1','',["id"=>'address1',"class"=>"form-control"]) !!}
                            </div>
                           <div class="col-md-5 form-group">
                                {!! Form::label('Address2') !!}
                                {!! Form::text('add2','',["id"=>'address2',"class"=>"form-control"]) !!}
                           </div>
                        </div>
                    </div>
                    
                    <div class="form-group margin-top">
                        {!!Form::submit('Add',["id"=>'submit',"class"=>"btn  btn-success"]) !!}
                        <hr>
                    </div>
                    {!! Form::close() !!}
                </div>       
            </div>
        </div>
@endsection

@section('script')
 
    <script type="text/javascript">
        $(document).ready(function(){
            $("#edit_profile").validate({
                rules: {
                    f_name: {
                        required: true
                    },
                    l_name: {
                        required: true
                    },
                    mobile_no: {
                        required:true,
                        digits:true,
                        minlength:10,
                        maxlength:12
                    },
                    country: {
                       required: true
                    },
                    state: {
                        required: true
                    },			   
                    city: {
                        required: true
                    },
                    address1: {
                        required: true
                    }			   
                },
                messages: {
                    f_name: "Please enter firstname",
                    l_name: "Please enter lastname",
                    mobile_no:{
                        required: "Please enter mobile no",
                        digits: "Please enter valid mobile no",
                        minlength: "Please enter valid phone number",
                        maxlength: "Please enter valid phone number"
                    },
                    country: "Please enter country",
                    state: "Please enter state",
                    city: "Please enter city",
                    address1: "Please enter address",
                },
            });
        });
        
        function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn) {
            $.ajax({
                url: '{{url('getdata')}}',
                type: "get",
                data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn},
                success: function (data) {
                    $('#'+appendId).html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
        
        function showValue() {
            var checked=$('.use_old_address').is(':checked');
            if(checked==true){
                $(".new_Address").attr("style","display:none;");
            }
            else{
                $(".new_Address").attr("style","display:block;");
            }
        }
    </script>
@endsection
