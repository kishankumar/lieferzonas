@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
    <div class="panel-body">
	<h3 class="text-success"> bewertungen 
	<span class="pull-right">
               
                    
					 <a  href="javascript:window.print()" class="btn btn-warning DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#" class="btn btn-default DTTT_button_print" onclick="download_offers()" title="View print view" tabindex="0" aria-controls="simpledatatable" >
                         <span >Download</span>
                     </a>-->
                     
	</span>
	</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
					
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
					
			
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
              	<div class="table-responsive mt25">
						<table class="table table-hover table-striped table-new">
                @if(count($order_rate))
                            @foreach($order_rate as $order_rates)
								<tr>
									<td>
										<table class="table">
											<tr>
												<td width="10%">Date</td>
												<td width="20%">{{ $order_rates->created_at }}</td>
												<td width="70%" rowspan="4">
													<p class="text-right">@if($order_rates->is_reported==0)<a href="#" onclick="report(<?=$order_rates->id?>);" class="btn btn-outline" >Melden</a>@else Already Reported @endif</p>
													{{ $order_rates->comment }}
												</td>
											</tr>
											<tr>
												<td>Name</td>
												<td>{{ $order_rates->booking_person_name }}</td>
											</tr>
											<tr>
												<td>Quality</td>
												<td>
												<?php for($i=1; $i<=floor($order_rates->quality_rating); $i++)
												  {
													  
													  echo '<i class="fa fa-star text-warning"></i>';
												  }
												   
											   
												  if(ceil($order_rates->quality_rating) > floor($order_rates->quality_rating) )
												  {
													 
													 echo '<i class="fa fa-star-half-o text-warning"></i>';
												   
												  }
												  
												  if(5 > ceil($order_rates->quality_rating) )
												  {
												   
													for($i = ceil($order_rates->quality_rating); $i<5; $i++)
													{
														
														echo '<i class="fa fa-star"></i>';
													}
													
												  }
												?>
												</td>
											</tr>
											<tr>
												<td>Service</td>
												<td>
												<?php for($i=1; $i<=floor($order_rates->service_rating); $i++)
												  {
													  
													  echo '<i class="fa fa-star text-warning"></i>';
												  }
												   
											   
												  if(ceil($order_rates->service_rating) > floor($order_rates->service_rating) )
												  {
													 
													 echo '<i class="fa fa-star-half-o text-warning"></i>';
												   
												  }
												  
												  if(5 > ceil($order_rates->service_rating) )
												  {
												   
													for($i = ceil($order_rates->service_rating); $i<5; $i++)
													{
														
														echo '<i class="fa fa-star"></i>';
													}
													
												  }
												?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							@endforeach
							@else 
								<tr>
									<td colspan="8" align="center">
										No Record Exist
									</td>
								</tr>
							 @endif
						
              </table>
               {!! $order_rate->render() !!}   
  
            </div>
	</div>
</div>
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
</div>
<div></div>

@if(count($order_rate))
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Comments</h4>
      </div>
      <div class="modal-body" id="comment">
       
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<style>
.table{margin: 0;}	
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border: none; padding: 5px;}
</style>
@endif
@endsection

@section('script')
 <script>
 function report(review_id)
		 {
			 var review_id = review_id;
			 //alert(review_id);
			 var base_host = window.location.href;
			 base_host = base_host.substring(0, base_host.lastIndexOf("/"));
			 var base_url1 = base_host+'/order_rating/report';
			 $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'review_id':review_id,"_token":"{{ csrf_token() }}"},
                success: function(res){
                   if(res.success==1)
                    {
                        location.reload(); 
                    }
                    
                }
            });

		 }
		 function getcomment(id)
		 {
			 var review_id = id;
			 var base_host = window.location.href;
			 base_host = base_host.substring(0, base_host.lastIndexOf("/"));
			 var base_url1 = base_host+'/order_rating/getcomment';
			 $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'review_id':review_id,"_token":"{{ csrf_token() }}"},
                success: function(res){
                   if(res.success==1)
                    {
                         var comment = res.comment;
						 //alert(comment);
						 $('#comment').html(comment);
						 $('#myModal').modal('show');
                    }
                    
                }
            });
		 }
 </script>
@endsection
