@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
   
			<div class="text-right">
               
                    
					 <a  href="javascript:window.print()" class="btn btn-warning DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
                         <span>Print</span>
                     </a>
                     <!--<a  href="#" class="btn btn-default DTTT_button_print" onclick="download_offers()" title="View print view" tabindex="0" aria-controls="simpledatatable" >
                         <span >Download</span>
                     </a>-->
                     
			</div>
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
               <table id="example1" class="table table-bordered table-new table-c">
                <thead class="bg-danger">
                    <tr>
                    <th>Order No.</th>
					<th>Order Date</th>
					<th>Customer Name</th>
					<th>Quality Rating</th>
					<th>Service Rating</th>
					<th>Comment</th>
					<th>Action</th>
                    </tr>   
                </thead>
                <tbody>
                    
                 @if(count($order_rate))
                    @foreach($order_rate as $order_rates)
              
                    <tr>
                    <td>{{ $order_rates->order_id }}</td>
					<td>{{ $order_rates->created_at }}</td>
					<td>{{ $order_rates->booking_person_name }}</td>
					<td>{{ $order_rates->quality_rating }}</td>
					<td>{{ $order_rates->service_rating }} </td>
					<td><a href="#" class="red" onclick="getcomment(<?=$order_rates->id?>)" data-toggle="modal" >View</a> </td>
					<td>@if($order_rates->is_reported==0)<a href="#" onclick="report(<?=$order_rates->id?>);" >Report</a>@else Already Reported @endif</td>
                    </tr> 
                
               
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
               {!! $order_rate->render() !!}   
  
</div>
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
</div>
@if(count($order_rate))
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Comments</h4>
      </div>
      <div class="modal-body" id="comment">
       
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
@endif
@endsection

@section('script')
 <script>
 function report(review_id)
		 {
			 var review_id = review_id;
			 //alert(review_id);
			 var base_host = window.location.href;
			 base_host = base_host.substring(0, base_host.lastIndexOf("/"));
			 var base_url1 = base_host+'/order_rating/report';
			 $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'review_id':review_id,"_token":"{{ csrf_token() }}"},
                success: function(res){
                   if(res.success==1)
                    {
                        location.reload(); 
                    }
                    
                }
            });

		 }
		 function getcomment(id)
		 {
			 var review_id = id;
			 var base_host = window.location.href;
			 base_host = base_host.substring(0, base_host.lastIndexOf("/"));
			 var base_url1 = base_host+'/order_rating/getcomment';
			 $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'review_id':review_id,"_token":"{{ csrf_token() }}"},
                success: function(res){
                   if(res.success==1)
                    {
                         var comment = res.comment;
						 //alert(comment);
						 $('#comment').html(comment);
						 $('#myModal').modal('show');
                    }
                    
                }
            });
		 }
 </script>
@endsection
