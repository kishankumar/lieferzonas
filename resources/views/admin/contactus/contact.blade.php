@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')

@section('body')
    <div class="col-sm-9">
        <div id="right_side">
            <div class="panel-body">
                
            @if(Session::has('message'))
                <div class="alert alert-success" style="padding: 7px 15px;">
                    {{ Session::get('message') }}
                    {{ Session::put('message','') }}
                </div>
            @endif
            
            {{--    Error Display--}}
                @if($errors->any())
                    <div class="alert alert-danger ">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $error)
                                <li> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            {{--    Error Display ends--}}
            
            <h3 class="text-success">Customer Service</h3>
            
            {!! Form::open(array('route' => 'rzone.contactus.store','id'=>'contact_form','class' => 'form','novalidate' => 'novalidate', 'files' => true)) !!}
                
                <div class="form-group">
                    {!! Form::label('Topic') !!}<span class="red">*</span>  <span id="nameErr" style="color:red"></span>
                    {!! Form::text('topic','',["id"=>'topic',"class"=>"form-control",'onblur' => 'trimmer(name)']) !!}
                </div>
                
                <div class="form-group">
                    {!! Form::label('Subject') !!}<span class="red">*</span> <span id="subjectErr" style="color:red"></span>
                    {!! Form::text('subject','',["id"=>'subject',"class"=>"form-control",'onblur' => 'trimmer(subject)']) !!}
                </div>
                
                <div class="form-group">
                    {!! Form::label('Email') !!}<span class="red">*</span> <span id="emailErr" style="color:red"></span>
                    {!! Form::text('email','',["id"=>'email',"class"=>"form-control",'onblur' => 'trimmer(email)']) !!}
                </div>
                
                <div class="form-group">
                    {!! Form::label('Message') !!}<span class="red">*</span> <span id="messageErr" style="color:red"></span>
                    {!! Form :: textarea('message','',['style'=>'height:200px;','id'=>'message','onblur' => 'trimmer(message)','placeholder'=>'Enter message ','class'=>'form-control'])  !!}
                </div>
                 
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('Phone') !!}<span class="red">*</span>
                            {!! Form::text('phone','',["id"=>'phone',"class"=>"form-control",'onblur' => 'trimmer(phone)']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group filebox">
                            {!! Form::label('Image') !!}<span class="red">*</span>
                            {!! Form::file('image','',array('id'=>'image','class'=>'form-control')) !!}
                        </div>
                    </div>
                    <div></div>
                </div>
		
                <div class="form-group margin-top">
                    {!! Form::submit('Add',["id"=>'submit',"class"=>"btn  btn-success"]) !!}
                    <hr>
                    <p><span class="red">*</span> - Required Fields.</p>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
<style>
	.filebox{overflow: hidden;}
</style>

@section('script')

<script type="text/javascript">
    function trimmer(id){
        $('#'+id).val($('#'+id).val().trim());
    }
    $(document).ready(function(){
        // Accept a value from a file input based on a required mimetype
        jQuery.validator.addMethod("accept", function(value, element, param) {
            var typeParam = typeof param === "string" ? param.replace(/\s/g, '').replace(/,/g, '|') : "image/*",
            optionalValue = this.optional(element),
            i, file;
            if (optionalValue) {
                return optionalValue;
            }
            if ($(element).attr("type") === "file") {
                typeParam = typeParam.replace(/\*/g, ".*");
                if (element.files && element.files.length) {
                    for (i = 0; i < element.files.length; i++) {
                        file = element.files[i];
                        // Grab the mimetype from the loaded file, verify it matches
                        if (!file.type.match(new RegExp( ".?(" + typeParam + ")$", "i"))) {
                                return false;
                        }
                    }
                }
            }
            return true;
        }, "Please enter a value with a valid mimetype.");
        
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-zA-ZäÄöÖüÜß«» -]+$/i.test(value);
        }, "Name must contain only letters.");
        
        $.validator.addMethod("email", function(value, element)
	{
            return this.optional(element) || /^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
	}, "Please enter valid email address.");
        
        $("#contact_form").validate({
            rules: {
                topic: {
                    required : true,
                    minlength:3,
                    maxlength:100,
                    loginRegex:true
                },
                subject: {
                    required : true,
                    minlength:3,
                    maxlength:100,
                    loginRegex:true
                },
                email: {
                    required: true,
                    email:true
                },
                message: {
                    required: true,
                    minlength:6
                },
                phone: {
                    required:true,
                    number:true,
                    minlength:10,
                    maxlength:12
                },
                image: {
                    required: true,
                    accept: "image/jpg,image/jpeg,image/png,image/gif",
                },
            },
            messages: {
                topic:{
                    required: "Please enter topic",
                    loginRegex: "Topic name must contain only letters.",
                },
                subject:{
                    required: "Please enter subject",
                    loginRegex: "Subject name must contain only letters.",
                },
                email:{
                    required: "Please enter email",
                    email: "Please enter valid email"
                },
                message:{
                    required: "Please enter message",
                },
                phone: {
                    required: "Please enter phone number",
                    number: "Please enter valid phone number",
                    minlength: "Please enter valid phone number",
                    maxlength: "Please enter valid phone number"
                },
                image:{
                    required: "Please upload image",
                    accept: 'Not an image!'
                },
            },
        });
   });

</script>
@endsection
   
   


