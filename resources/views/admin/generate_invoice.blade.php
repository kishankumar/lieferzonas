@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
   
			<div class="btn-group">
               
			</div>
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
            <div>
			    
			    <?php
				// set start and end year range
				$currentyear = date("Y");
				$yearArray = range(2000, $currentyear);
				?>
				<!-- displaying the dropdown list -->
				<div class="col-sm-3 form-group">
				<select class="form-control" name="year" id="year" onclick="hideErrorMsg('yearerr');" onchange="getmonth()">
					<option value="">Select Year</option>
					<?php
					foreach ($yearArray as $year) {
						// if you want to select a particular year
						$selected = ($year == 0) ? 'selected' : '';
						echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
					}
					?>
				</select>
				<span id="yearerr" class="text-danger"></span> 
				</div>
				<?php
				// set start and end month range
				$currentmonth = date("m");
				$monthArray = range(1, ($currentmonth-1));
				$allmonthArray = range(1, (12));

				?>
				<!-- displaying the dropdown list -->
				<div class="col-sm-3 form-group currentmonth"  id="selmonths" style="display:None;">
				<select class="form-control" name="month" id="cmonth" class="currentmonth" onclick="hideErrorMsg('cmontherr');">
					<option value="">Select Month</option>
					<?php
					foreach ($monthArray as $month) 
					{
						$monthName = date("F", mktime(0, 0, 0, $month, 10));
						// if you want to select a particular year
						$selected = ($month == 0) ? 'selected' : '';
						echo '<option '.$selected.' value="'.$month.'">'.$monthName.'</option>';
					}
					?>
				</select>
					 <span id="cmontherr" class="text-danger"></span> 
				</div>
				<div class="col-sm-3 form-group allmonth" id="allmonths" >
				<select class="form-control" name="month" id="amonth" class="allmonth" onclick="hideErrorMsg('amontherr');">
					<option value="">Select Month</option>
					<?php
					foreach ($allmonthArray as $month) 
					{
						$monthName = date("F", mktime(0, 0, 0, $month, 10));
						// if you want to select a particular year
						$selected = ($month == 0) ? 'selected' : '';
						echo '<option '.$selected.' value="'.$month.'">'.$monthName.'</option>';
					}
					?>
				</select>
			    <span id="amontherr" class="text-danger"></span> 
				</div>
			    
				{!!Form::button('Generate Invoice',["id"=>'invoice',"class"=>"btn  btn-primary","onclick"=>"genearte_invoice()"]) !!}
			
			</div>  			
  
    </div>
</div>
<!--
<style>
	.form-control{width: 25%; float: left; margin-right: 10px;}
</style>
-->
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<div id="content" style="display:none;" >
	
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
	
@endsection

@section('script')
 <script>
   function genearte_invoice()
   {
	 
	 var year = $('#year').val();
			if(year=='')
			{
				$('#year').focus();
				$("#yearerr").html('Please select year');
                $("#yearerr").show();
				return false;
			}
     if ($("#allmonths").is(":visible") == true)
	{
		
		var month = $('#amonth').val();
			if(month=='')
			{
				$('#amonth').focus();
				$("#amontherr").html('Please select month');
                $("#amontherr").show();
				return false;
			}
	}
	if ($("#selmonths").is(":visible") == true)
	{
		
		var month = $('#cmonth').val();
			if(month=='')
			{
				$('#cmonth').focus();
				$("#cmontherr").html('Please select month');
                $("#cmontherr").show();
				return false;
			}
	}
	
	    if(month<10)
		{
			var month = '0'+ month;
		}
		var doc = new jsPDF();
			var specialElementHandlers = {
			'#editor': function (element, renderer) {
				return true;
			}
        };
		$.ajax({
                url: '{{url('rzone/invoice/generatepdf')}}',
                type: "post",
                dataType:"json",
                data: {'month':month,'year':year,"_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success==1)
					   {
							$("#download").html(res.data1);
							$("#content").html(res.data); 
							var doc = new jsPDF('p', 'pt');
							var elem = document.getElementById("table1");
							var res = doc.autoTableHtmlToJson(elem);
							doc.autoTable(res.columns, res.data);
							doc.fromHTML($('#content').html(), 10, 200, {
							'width': 250,
							'elementHandlers': specialElementHandlers
							});
							doc.save("invoice.pdf");
					   }
				   else
					   {
						   alert('Data not found')
					   }
                }
            });
   }
   
    function hideErrorMsg(id){
    
      $('#'+id).attr('style', 'display:none');
  
    
     }
	  function getmonth()
	 {
		var TodayDate = new Date();
		var currentmonth = TodayDate.getMonth();
		var currentyear = TodayDate.getFullYear();
		var selectedyear = $('#year').val();
		if( currentyear==selectedyear)
		{
			$(".currentmonth").show(); 
			$(".allmonth").hide();
		}
		else
		{
			$(".allmonth").show(); 
			$(".currentmonth").hide(); 
		}
	 }
 </script>
@endsection
