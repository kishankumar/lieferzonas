@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
   
               
				<div class="text-right">
				 <a  href="javascript:window.print()" class="btn btn-warning DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
					 <span>Print</span>
				 </a>
				</div>
                     <!--<a  href="#" class="btn btn-default DTTT_button_print" onclick="download_offers()" title="View print view" tabindex="0" aria-controls="simpledatatable" >
                         <span >Download</span>
                     </a>-->
                     
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
               <table id="example1" class="table table-new table-c">
                <thead class="bg-danger">
                    <tr>
                    <th>User Name</th>
					<th>User Email</th>
					<th>User Phone No</th>
					<th>Stamp Count</th>
                    </tr>   
                </thead>
                <tbody>
                    
                @if(count($stamplist))
                    @foreach($stamplist as $stamplists)
                <tr>
                    
                    <td>{{ $stamplists->fname.' '.$stamplists->lname }}</td>
					<td>{{ $stamplists->uemail}}</td>
					<td>{{ $stamplists->umobile}}</td>
					<td>{{ $stamplists->stamp_count. '/'. $stampcount['0']['stamp_count'] }}</td>
                   
                
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
                 {!! $stamplist->render() !!} 
  
</div>
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
	
</div>

@endsection

@section('script')
 
@endsection
