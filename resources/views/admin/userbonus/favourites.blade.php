@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
   
                     <!--<a  href="#" class="btn btn-default DTTT_button_print" onclick="download_offers()" title="View print view" tabindex="0" aria-controls="simpledatatable" >
                         <span >Download</span>
                     </a>-->
				<div class="media">
					<div class="pull-right">
						<a href="javascript:window.print()" class="btn btn-warning DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
							<span>Print</span>
						</a>
					</div>
					<div class="media-body">
						<div style="margin:10px 0 0 15px" class="DTTT_button_print">Total: {{count($favlist)}} </div> 
					</div>
				</div>
				
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
               <table id="example1" class="table table-new table-c">
                <thead class="bg-danger">
                    <tr>
                    <th>Customer Name</th>
					</tr>   
                </thead>
                <tbody>
                    
                @if(count($favlist))
                    @foreach($favlist as $favlists)
                <tr>
                    
                    <td>{{ $favlists->fname.' '.$favlists->lname }}</td>
					
                
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
                 {!! $favlist->render() !!} 
  
</div>
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
</div>

	
@endsection

@section('script')
 
@endsection
