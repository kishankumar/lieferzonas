@extends('layouts.adminbody')

@section('title')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
   
               <div class="text-right">
				 <a  href="javascript:window.print()" class="btn btn-warning DTTT_button_print" id="ToolTables_simpledatatable_4" title="View print view" tabindex="0" aria-controls="simpledatatable">
					 <span>Print</span>
				 </a>
				</div>
				 <!--<a  href="#" class="btn btn-default DTTT_button_print" onclick="download_offers()" title="View print view" tabindex="0" aria-controls="simpledatatable" >
					 <span >Download</span>
				 </a>-->
                     
                @if(Session::has('message'))
                    {{ Session::get('message') }}
                    
                    {{ Session::put('message','') }}
                @endif
               <table id="example1" class="table table-new table-c">
                <thead class="bg-danger">
                    <tr>
                    <th>User Name</th>
					<th>Order Id</th>
					<th>Bonus Value</th>
					<th>Date</th>
                    </tr>   
                </thead>
                <tbody>
                 @if(count($newcustbonus))
                    @foreach($newcustbonus as $custbonus)
                <tr>
                    <td>{{ $custbonus->fname.' '.$custbonus->lname }}</td>
					<td>{{ $custbonus->order_id }}</td>
					<td>{{ $custbonus->bonus_value}}</td>
					<td>{{ $custbonus->created_at }}</td>
                
                </tr>
                    @endforeach
                @else 
                    <tr>
                        <td colspan="8" align="center">
                            No Record Exist
                        </td>
                    </tr>
                 @endif
                </tbody>
                
              </table>
              {!! $newcustbonus->render() !!}   
  
</div>
    <!--added by Rajlakshmi(31-05-16)-->
	<div id="download" style="display:none;" >
	</div>
	<!--added by Rajlakshmi(31-05-16)-->
</div>

	
@endsection

@section('script')
 <script>
 function report(review_id)
		 {
			 var review_id = review_id;
			 //alert(review_id);
			 var base_host = window.location.href;
			 base_host = base_host.substring(0, base_host.lastIndexOf("/"));
			 var base_url1 = base_host+'/order_rating/report';
			 $.ajax({
                url: base_url1,
                type: "post",
                dataType:"json",
                data: {'review_id':review_id,"_token":"{{ csrf_token() }}"},
                success: function(res){
                   if(res.success==1)
                    {
                        location.reload(); 
                    }
                    
                }
            });

		 }
 </script>
@endsection
