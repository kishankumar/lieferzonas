<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    {!! Html::style('resources/assets/admin/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/admin/css/font-awesome.css') !!}
    {!! Html::style('resources/assets/admin/css/select2.css') !!}
    {!! Html::style('resources/assets/admin/css/style.css') !!}
    {!! Html::style('resources/assets/admin/css/nanoscroller.css') !!}
    {!! Html::style('resources/assets/css/date/bootstrap-datetimepicker.css') !!}
</head>

<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 text-center">
                <a href="{{url('rzone/dashboard')}}" class="logo">
                    {!!Html::image('resources/assets/admin/img/logo.png','Logo')!!}
                </a>
            </div>
            
            <?php 
                $username = \App\superadmin\RestOwner::adminuserdetail(); 
                $restDetail = \App\superadmin\RestOwner::getRestaurantDetail();
                $adminSetting = \App\superadmin\RestOwner::getAdminSettingsData();
                $timeSetting = \App\superadmin\RestOwner::getTimeSettingsData();
            ?>
            
            <div class="col-sm-9">
                <section class="top-bar">
                    <span>
                        <a href="javascript:void(0)">{{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }}</a>
                        &nbsp; &nbsp; &nbsp; &nbsp; 
                        <a href="javascript:void(0)">www.backshop.at</a>
                    </span>
                    <?php $notReadNotiCount = \App\superadmin\RestNotification::restnotification(); ?>
                    <span>
                        <a href="{{url('rzone/dashboard')}}"><i class="fa fa-fw fa-bell"></i>  
                            <span class="badge badge-up"><?=@$notReadNotiCount?></span>
                        </a>
                    </span>
                    
					<span>Welcome {{ ucfirst($username['0']['firstname']).' '.ucfirst($username['0']['lastname']) }}</span>
					<?php 
					$auth_id = Auth::User('admin')->id;
					$grand_totalsale = \App\front\OrderPayment::totalsale_monthly($auth_id); 
					$month_grand_total = 0;
					foreach($grand_totalsale as $grand_totalsales)
					{
					  $month_grand_total = $month_grand_total + (int)$grand_totalsales->grand_total;
					}
					$month = date("M",strtotime(date("Y-m-d")));
					?>
                    <span>
                        <i class="fa fa-shopping-bag"></i> Umsatz, {{ $month }}: {{ $month_grand_total }} €

                    </span>
                    <span>
                        <a href="{{url('authadmin/logout')}}"><i class="fa fa-sign-out"></i> log out</a>
                        <!--<a href="{{action('Auth\AdminAuthController@getLogout')}}"><i class="fa fa-sign-out"></i> log out</a>-->
                    </span>
                </section>
                
                <div class="text-right">
                    @if(count($adminSetting))
                        @if($adminSetting->is_open)
                            @if($timeSetting == 'open')
                                <span class="light-shad">
                                    {{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }} is open today 
                                    <i class="fa fa-circle text-success"></i>
                                </span><br>
                            @else
                                <span class="light-shad">
                                    {{ ucfirst($restDetail->f_name).' '. ucfirst($restDetail->l_name) }} is closed today 
                                    <i class="fa fa-circle text-danger"></i>
                                </span><br>
                            @endif
                            <span class="light-shad"><a href="javascript:void(0)" onclick="change_restaurant_status('close')">Restaurant IMMEDIATELY Close</a> 
                                <i class="fa fa-times text-danger"></i></span>
                        @else
                            <span class="light-shad"><a href="javascript:void(0)" onclick="change_restaurant_status('open')">Restaurant IMMEDIATELY Open</a>
                                <i class="fa fa-times text-success"></i></span>
                        @endif
                    @else
                        <span class="light-shad"><a href="javascript:void(0)" onclick="change_restaurant_status('open')">Restaurant IMMEDIATELY Open</a>
                            <i class="fa fa-times text-success"></i></span>
                    @endif
                </div>
                <div class="clearfix"></div>
				 <?php $not_read_noti_count = \App\superadmin\RestNotification::restnotification(); ?>
                <nav class="navbar navbar-inverse">
                    <ul class="nav navbar-nav ">
                        <li><span class="badge badge-up" style="left: auto; position: absolute; right: 0; top: 2px; z-index: 9;"><a style="color:#fff;" href="{{url('rzone/dashboard')}}"><?=$not_read_noti_count?></a></span>
							<a href="{{route('rzone.profile.index')}}">My account</a> 
						</li>
                        <!--<li><a href="javascript:void(0)">My Team </a></li>-->
                        <li><a href="{{route('rzone.shop.orders.index')}}">Shop Orders </a></li>
                        <li><a href="{{route('rzone.menu.index')}}">Menu </a></li>
                        <li><a href="javascript:void(0)">Actions </a></li>
                        <li><a href="javascript:void(0)">Finances </a></li>
                        <li><a href="{{url('rzone/marketingtips')}}">Marketing Tips </a></li>
                        <li><a href="{{route('rzone.shop.cart.index')}}">Shop </a></li>
                        <li><a href="{{route('rzone.contactus.create')}}">Service Center </a></li>
						<li><a href="{{route('rzone.total_sales.index')}}">Total Sales </a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>   
    
    <?php 
        $allow_admin = 0;
        if(count($adminSetting)){
            $allow_admin = $adminSetting->allow_admin;
        }
    ?>
    
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="leftbar" id="left_side">
                    <div class="bg-warning"><h3>Lieferzonas.at SHOP</h3></div>
                    <ul>
                        <!--Added by Rajlakshmi-->

                        <li class="bg-danger pd">Navigation</li>
                        <li class="MenuDropdown"><a href="javascript:void(0)"><strong>My account</strong> <b class="caret"></b></a>
							<ul>
                        		<li><a href="{{route('rzone.profile.index')}}">View Profile</a></li>
							</ul>
						</li>

                        <li><a href="{{route('rzone.taxinvoicenumber.index')}}">Tax Invoice Number</a></li>
                        <li><a href="{{route('rzone.specialoffer.index')}}">Special Offer</a></li>
                        <!--Added by Rajlakshmi-->
                        @if($allow_admin)
                            <li><a href="{{route('rzone.category.image.change.index')}}">Change Category Image</a></li>
                        @endif
                        <!--<li><a href="javascript:void(0)">Info</a></li>-->
                        
                        <li><a href="{{route('rzone.restaurant.orders.index')}}">Order Status</a></li>
                        <li><a href="{{route('rzone.rank.setting.index')}}">Rank Setting</a></li>
                        
                        <!--<li><a href="javascript:void(0)">Organic Pasta</a></li>-->
                        <!--<li><a href="javascript:void(0)">Sticker</a></li>-->
                        <!--<li><a href="javascript:void(0)">Others</a></li>-->
						<!--Added by Rajlakshmi(01-07-16)-->
						<li><a href="{{route('rzone.userbonus.favourites.index')}}">Favourites</a></li>
						<li><a href="{{route('rzone.userbonus.stamp.index')}}">Stamp</a></li>
						<li><a href="{{route('rzone.userbonus.new_customer_bonus.index')}}">New customer bonus</a></li>
						<li><a href="{{route('rzone.userorder.order_rating.index')}}">User Order Rating</a></li>
						<li><a href="{{route('rzone.total_sales.index')}}">Total Sales </a></li>
						<li><a href="{{route('rzone.generate_invoice.index')}}">Generate Invoice</a></li>
						<!--Added by Rajlakshmi(01-07-16)-->
                    </ul>
                    @yield('shopcart')
                </div>
            </div>
            
            @yield('body')

        </div>
    </div>
    
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <strong>Copyright © 2016 — Lieferzonas</strong>.at <br>Delivery Network
                </div>
            </div>
        </div>
    </footer>
    
    {!! Html::script('resources/assets/admin/js/jQuery-2.2.0.min.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.min.js') !!}
    {!! Html::script('resources/assets/admin/js/bootstrap.js') !!}
    <!--Added by Rajlakshmi-->
    {!! Html::script('resources/assets/js/date/moment-with-locales.js') !!}
    {!! Html::script('resources/assets/js/date/bootstrap-datetimepicker.js') !!}
    <!--Added by Rajlakshmi-->
    <script>
        var SITE_URL_PRE_FIX = '{{ Config::get('constants.SITE_URL_PRE_FIX', '/lieferzonas') }}';
    </script>
    {!! Html::script('resources/assets/admin/js/script.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.validate.js') !!}
    {!! Html::script('resources/assets/admin/js/jquery.nanoscroller.js') !!}
    <!--start Script add by Rajlakshmi(28-05-16)-->
    {!! Html::script('resources/assets/js/jspdf.min.js') !!}
    {!! Html::script('resources/assets/js/html2canvas.js') !!}
    {!! Html::script('resources/assets/js/jspdf.plugin.autotable.src.js') !!}
    {!! Html::script('resources/assets/admin/js/select2.js') !!}
    
    <!--end Script add by Rajlakshmi(28-05-16-->
    
    @yield('script')
	<script type="text/javascript">
		$(".js-example-basic-multiple").select2();
	</script>	
    <script>
        $(function(){
            $('.nano').nanoScroller({
                preventPageScrolling: true
            });
        });	
    </script>
    <script>
        $(window).on("scroll resize load", function () {
            document.getElementById('left_side').style.height = "";
            var lefttHeight = document.getElementById('left_side').clientHeight;
            var rightHeight = document.getElementById('right_side').clientHeight;
            if(lefttHeight < rightHeight){
                document.getElementById('left_side').style.height = rightHeight+25+"px";
            }
            else{
                document.getElementById('left_side').style.height = lefttHeight+25+"px";
            }
        });
	
        $(document).ready(function() {
            $('.val_table, .val_date').click(function(){
            if($('.val_table').is(":checked")==true){
                $('#select_s').css({'display':'block'});
                $('#date_s').css({'display':'none'});
            }
            else if($('.val_date').is(":checked")==true){
                $('#date_s').css({'display':'block'});
                $('#select_s').css({'display':'none'});
            }
        });
    });
    
    $(function() {
        var date = new Date();
        var currentMonth = date.getMonth();
        var currentDate = date.getDate();
        var currentYear = date.getFullYear();
		
        $('#datetimepicker8').datetimepicker({
            format: 'YYYY/MM/DD',
            useCurrent: false,
            minDate: new Date(currentYear, currentMonth, currentDate)
        });
        $('#datetimepicker7').datetimepicker({
            format: 'YYYY/MM/DD',
            useCurrent: false,
            minDate: new Date(currentYear, currentMonth, currentDate)
        });
        $("#datetimepicker8").on("dp.change", function(e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function(e) {
            $('#datetimepicker8').data("DateTimePicker").maxDate(e.date);
        });
    });
    <!--Added by Rajlakshmi-->
    </script>
	
    <script type="text/javascript">
        $.fn.MenuDropdown = function() {
            $(this).click(function(){
                if($(this).find('ul').hasClass('active')){
                    $(this).find('ul').parent('li').removeClass('active')
                    $(this).find('ul').removeClass('active').slideUp();
                }
                else{
                    $(this).find('ul').parent('li').addClass('active')
                    $(this).find('ul').addClass('active').slideDown();
                }
            });
            return this;
        }
        
        $( ".MenuDropdown" ).MenuDropdown();
        
        $(document).ready(function(){
            $("#left_side a").each(function() {
                if (this.href == window.location.href) {
                    $(this).addClass("active").parent('li').parent('ul').css('display','block').parent('li').addClass('active');
                }
            });
            $('.nav.navbar-nav a').each(function(){
                if (this.href == window.location.href) {
                    $(this).addClass('active');	
                }
            });
        });
        
        function change_restaurant_status(status){
            $.ajax({
                url: '{{url('rzone/restaurant/status/change')}}',
                type: "post",
                dataType:"json",
                data: { 'is_open':status, "_token":"{{ csrf_token() }}"},
                success: function(res){
                    if(res.success == 1){
                        window.location.reload();
                    }else{
                        alert('Status not changed');
                    }
                }
            });
        }
    </script>
	<script type="text/javascript">		
		$.fn.MenuDropdown = function() {
			$(this).find('ul').css("display","none");
			$(this).click(function(){
				if($(this).find('ul').hasClass('selected')){
					$(this).css("background","none");
					$(this).find('ul').removeClass('selected').slideUp().css( "border-bottom", "none");
				}
				else{
					$(this).css("background","#C6EC95");
					$(this).find('ul').addClass('selected').slideDown().css( "border-top", "1px solid #fff");
				}
			});
	
			
			return this;
		}
		
		$( ".MenuDropdown" ).MenuDropdown();
	</script>
</body>
</html>