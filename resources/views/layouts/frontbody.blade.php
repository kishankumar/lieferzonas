<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

{!! Html::style('resources/assets/front/css/font-awesome.css') !!}
{!! Html::style('resources/assets/front/css/animate.css') !!}
{!! Html::style('resources/assets/front/css/bootstrap.css') !!}
{!! Html::style('resources/assets/front/css/bootstrap-select.min.css') !!}
{!! Html::style('resources/assets/front/css/style.css') !!}
{!! Html::style('resources/assets/front/css/responsive.css') !!}
{!! Html::style('assets/css/jquery-ui.min.css') !!}
<!-- Added By Rajlakshmi(14-06-16) -->

    <!-- Added By Rajlakshmi(14-06-16) -->
    <!-- Added By Rajlakshmi(28-06-16) -->
{!! Html::style('resources/assets/front/css/star-rating.css') !!}
{!! Html::style('resources/assets/front/css/select2.css') !!}
{!! Html::style('resources/assets/front/css/bootstrap-slider.css') !!}
<!-- {!! Html::style('resources/assets/front/css/lockfixed.css') !!} -->
    <!-- Scroll -->
{!! Html::style('resources/assets/front/css/nanoscroller.css') !!}
{!! Html::style('resources/assets/css/date/bootstrap-datetimepicker.css') !!}

{!! Html::style('http://fonts.googleapis.com/css?family=Open+Sans') !!}

<!-- Added By Rajlakshmi(28-06-16) -->
    {{--<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">--}}
</head>
<body>

@yield('homeheader')

<header class="home-header @yield('headerclass') ">
    <nav class="navbar" id="menu">
        <div class="container">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#main-navbar-container1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}">

                    {!! Html::image('resources/assets/front/img/logo.png') !!}
                </a>
            </div>
            <div id="main-navbar-container1" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right main-menu">
                    <li>
                        <a class="border-none"
                           href="#"> {!! Html::image('resources/assets/front/img/flag.png','flag',['width'=>'25']) !!}
                            Deutsch</a>
                    </li>

                    <?php if(!\Auth::check('front')){ ?>
                    <li><a href="#" data-toggle="modal" data-target="#myModal">Register</a></li>
                    <li class="dropdown"><a href="#" id="login_panel" class="dropdown-toggle" data-toggle="dropdown">
                            Log in</a>

                        <div id="login-dp" class="dropdown-menu">
                            <div class="loggin">
                                <h2>Login with Lieferzonas.at account:</h2>
                                <div class="log">
                                    <div class="panel-body">
                                        <!--<form class="form" method="post" action="login" id="login-nav">-->
                                        {!! Form::open(['url'=>'frontuser/login','method'=>'post','id'=>'login_form']) !!}

                                        @if($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach($errors->all() as $error)
                                                        <li> {{$error}} </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <?php
                                        if((Session::has('socialLogin')) || $errors->any()){
                                        $socialLogin = Session::get('socialLogin');
                                        if($socialLogin && $socialLogin != ''){ ?>
                                        <h2>Please login once if you want to link your social account to your member
                                            account.</h2>
                                        <?php
                                        }
                                        }
                                        ?>

                                        <div class="form-group">
                                            <!--<input type="email" class="form-control" placeholder="Email address" required>-->
                                            {!! Form::text('email','',['id'=>'email','class'=>'form-control','placeholder'=>'Email']) !!}
                                        </div>
                                        <div class="form-group">
                                            <!--<input type="password" class="form-control" placeholder="Password" required>-->
                                            {!! Form::password('password',['id'=>'password','class'=>'form-control','placeholder'=>'Password']) !!}

                                        </div>
                                        <label class="help-block" for="rememberpwd">
                                            <input type="checkbox" name="remember" id="rememberpwd"> Remember me on this
                                            computer
                                        </label>
                                        <div class="row">
                                            <div class="col-sm-7" style="margin-top:5px;">
                                                <a href="javascript:void(0)" data-toggle="modal"
                                                   data-target="#myModal_forger">Forgot Password ?</a><br>
                                                If don't have account ?<a id="register_user"
                                                                          onclick="show_register_users()"
                                                                          href="javascript:void(0)"> Register</a>
                                            </div>
                                            <div class="col-sm-5">
                                                <!--<button type="submit" id="login" class="btn btn-primary btn-block button">Einloggenn</button>-->
                                                {!! Form::submit('Login',['id'=>'login','onclick' => 'login_front_user()','class'=>'btn btn-primary btn-block button']) !!}
                                            </div>
                                        </div>

                                        <h4 class="text-center green"><strong> Or</strong></h4>
                                        {!! Form::close() !!}
                                    </div>
                                    <div><a href="{{url('auth/facebook')}}" class="btn btn-fb"><i
                                                    class="fa fa-facebook"></i> Facebook</a>
                                        <a href="{{url('auth/twitter')}}" class="btn btn-tw"><i
                                                    class="fa fa-twitter"></i> Twitter</a>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="bottom text-center">
                                <h4>Info <i class="fa fa-chevron-down"></i></h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="inf">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            </div>
                        </div>
                    </li>
                    <?php } ?>

                    <?php
                    if(\Auth::check('front')){ ?>
                    <li>
                        <select class="selectpicker" data-style="btn-transp" style="display: none;">
                            <option>Margaret Street 110 Vienna (Margareten)</option>
                            <option>Margaret Street 110 Vienna (Margareten)</option>
                            <option>Margaret Street 110 Vienna (Margareten)</option>
                        </select>
                    </li>


                    <?php $udetail = \App\front\FrontUserDetail::userdetail();

                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle"
                           data-toggle="dropdown"> {{ ucfirst($udetail['0']['fname']).' '.ucfirst($udetail['0']['lname']) }}
                            <b class="caret"></b></a>
                        <div id="login-dp" class="dropdown-menu">
                            <div class="loggin">
                                <h2 class="prof-hed">


                                    @if($udetail['0']['profile_pic'])
                                        <a href="{{ url('front/profile/'.$udetail['0']['id'].'/edit') }}"><img
                                                    src="{{asset('public/front/uploads/users/'.$udetail['0']['profile_pic']) }}"></a>
                                    @else
                                        <a href="{{ url('front/profile/'.$udetail['0']['id'].'/edit') }}"><img
                                                    src="{{asset('public/front/uploads/users/face.png') }}"></a>

                                    @endif


                                    <span>{{ ucfirst($udetail['0']['fname']).' '.ucfirst($udetail['0']['lname']) }}</span>

                                </h2>

                                <ul class="tops-links">
                                    <li><a href="{{ url('front/profile/'.$udetail['0']['id'].'/edit') }}" class="">Meine
                                            Daten</a></li>
                                    <!-- Added by Rajlakshmi(24-06-16) -->
                                    <li><a href="{{route('front.address.index')}}" class="">Meine Addressen</a></li>
                                    <li><a class="" href="{{route('front.changepass.index')}}">Passwort ändern</a></li>
                                    <!-- Added by Rajlakshmi(24-06-16) -->
                                    <li><a href="{{route('front.bonuspoint.index')}}" class="">Treuepunkte</a></li>
                                    <li><a class="" href="{{route('front.bonus_point_history.index')}}">Bonus Point
                                            History</a></li>
                                    <li><a href="{{route('front.cashbackpoint.index')}}" class="">Cashback-Punkte</a>
                                    </li>
                                    <li><a class="" href="{{route('front.cashback_point_history.index')}}">Cashback
                                            Point History</a></li>
                                    <li><a href="{{route('front.stamp.index')}}" class="">Stempelkarten</a></li>
                                    <li><a href="{{route('front.favourite.index')}}" class="">Favoriten</a></li>
                                    <li><a href="{{route('front.order.index')}}" class="">Bestellungen</a></li>
                                    <li><a href="{{route('front.orderrate.index')}}" class="">Orders Rate</a></li>
                                    <li><a href="{{route('front.complaint.index')}}" class="">Beschwerden</a></li>
                                    <li><a href="{{url('frontuser/logout')}}" class="">Ausloggen</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="bottom text-center">
                                <h4>Info <i class="fa fa-chevron-down"></i></h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="inf">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <!-- End .cart-dropdown -->
            </div>
        </div>
    </nav>

    @yield('homeheadersearch')


</header>


@yield('body')


<footer>
    <section class="icon-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7">
                    <div class="icon-content-foot text-center">
                        <h3> Folge Lieferzonas auf</h3>
                        <ul class="social-foot wow animated flipInX" data-wow-delay="0.2s"
                            style="visibility: visible; animation-delay: 0.2s; animation-name: flipInX;">
                            <?php $soc = \App\superadmin\SocialNetwork::social() ?>
                            <li><a href="{{ $soc->twitter }}"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{ $soc->facebook }}"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{ $soc->youtube }}"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="{{ $soc->google }}"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="{{ $soc->linkedin }}"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="{{ $soc->vk }}"><i class="fa fa-vk "></i></a></li>
                            <li><a href="{{ $soc->instagram }}"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="{{ $soc->pinterest }}"><i class="fa fa-pinterest-p "></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 col-md-5 text-right"> {!! Html::image('resources/assets/front/img/img.png','',['class'=>'img-responsive']) !!} </div>
            </div>
        </div>
    </section>
    <section class="red-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="content-red">
                        <h3>Brauchst Du Hilfe? <span> <a href="#" data-toggle="modal" data-target="#myModal-add-res">Restaurant hinzufügen</a></span>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-3">
                    <div class="foot-box">
                        <ul class="footlink">
                            <li><a href="#">Jobs </a></li>
                            <li><a href="#">Affiliate Programm </a></li>
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="#">Prese</a></li>
                            <li><a href="#">AGB</a></li>
                            <li><a href="#">Datenschutz</a></li>
                            <li><a href="#">Impressum</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="foot-box">
                        <h3>Partner</h3>
                        <ul class="footlink">
                            <li><a href="#">HamHam.at</a></li>
                            <li><a href="#">Zustellkaiser.at</a></li>
                            <li><a href="#">Zustellservice.net</a></li>
                            <li><a href="#">Wirliefern.at</a></li>
                            <li><a href="#">Pizzaessen.eu</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="foot-box"> {!! Html::image('resources/assets/front/img/Middle-Phone-Kopie.png','mobile',['class'=>'img-responsive']) !!} </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="foot-logo">
                        <a class="navbar-brand" href="{{url('/')}}">
                            {!! Html::image('resources/assets/front/img/footlogo.png','foot logo') !!}
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="foot-logo">
                        <p> &copy; 2016 <a href="#">Lieferzonas.at</a> All rights reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>
<!-- Added by Rajlakshmi(21-06-16)-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg2 robotoregular">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        style="color:#81c02f; opacity:1; font-size: 34px;"><span aria-hidden="true">×</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title">Register at Lieferzonas.at</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="r_msg" class="alert text-center" style="display: none"></div>
                    {{--<form method="POST" action="/login/">--}}
                    {!! Form::open(['id'=>'r_form']) !!}
                    <div class="col-xs-12 col-sm-6">
                        <div class="modal-left">
                            <small class="help-block">Bitte geben Sie Ihre Daten ein..</small>
                            <div class="form-group">
                                {!! Form::text('r_fname','',['id'=>'r_fname','class'=>'form-control modal-input required','placeholder'=>'Vorname']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::text('r_lname','',['id'=>'r_lname','class'=>'form-control modal-input required','placeholder'=>'Nachname']) !!}
                            </div>
                            <div class="form-group">
                                <select name="gender" id="gender" class="form-control modal-input required">
                                    <option value=''>Geschlecht</option>
                                    <option value='m'>Männlich</option>
                                    <option value='f'>Weiblich</option>
                                    <option value='o'>Sonstiges</option>
                                </select>
                            </div>
                            <div class="form-group">
                                {!! Form::text('r_email','',['id'=>'r_email','class'=>'form-control modal-input required email','placeholder'=>'E-Mail Adresse']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::password('r_password',['id'=>'r_password','class'=>'form-control modal-input required','placeholder'=>'Passwort']) !!}
                            </div>
                            <div class="help-block">
                                <a href="javascript:void(0)" id="show_password" onclick="showHide()"
                                   class="green" style="font-size:16px;">Passwort anzeigen</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="modal-right oder">
                            <!--<input type="checkbox" class="pull-left" id="141">-->
                            <!--<p style="margin-left:22px; font-size:13px;">I have the <a href="#" class="green">Conditions</a> and the <a href="#" class="green">Privacy Policy</a> read by Lieferzonas.at and accept it. </p>-->
                            <p><a href="{{url('auth/facebook')}}" class="btn btn-info btn-block bfcolor"><i
                                            class="fa fa-facebook"></i> Register now with Facebook</a></p>
                            <p><a href="{{url('auth/twitter')}}" class="btn btn-info btn-block trcolor"><i
                                            class="fa fa-twitter"></i> Register now with Twitter</a></p>
                            <p><a href="{{url('auth/google')}}" class="btn btn-info btn-block gpcolor"><i
                                            class="fa fa-google-plus"></i> Register now with Google+</a></p>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-7">
                        <input type="checkbox" class="pull-left" name="conditions_policy" id="142">
                        <p style="margin-left:22px; font-size:15px;">I have the <a href="javascript:void(0)"
                                                                                   class="green">Conditions</a> and the
                            <a href="javascript:void(0)" class="green">Privacy Policy</a> read by Lieferzonas.at and
                            accept it. </p>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="modal-left" style="font-size:14px;">
                            <div class="form-group">
                                {{--<button type="submit" class="btn btn-success btn-block brn-rg">Register</button>--}}
                                {!! Form::button('Register',['onclick'=>'register_user()','class'=>'btn btn-success btn-block brn-rg']) !!}
                            </div>
                            You already have an account? Now <a href="javascript:void(0)" onclick="show_login_panel()"
                                                                class="green">log in</a></div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Added by Rajlakshmi(21-06-16)-->

<div class="modal fade" id="myModal_forger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md robotoregular">
        <div class="modal-content">
            <div class="modal-header" style="background:#81c02f; color:#fff;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title">Hast Du Dein Passwort vergessen?</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::open(['url'=>'front/forgotpass','method'=>'post','id'=>'forgot_pwd','name'=>'forgot_pwd']) !!}
                    <div class="errors" id="ferrors" style="color: red"></div>
                    <div class="col-xs-12">
                        <div class="form-group"> Wenn Du Dein Passwort vergessen hast, gib bitte
                            <br> Deine Emailadresse ein, wir senden Dir dann eine
                            <br> E-Mail mit einem Link zur Erstellung eines
                            <br> neuen Passworts.
                            <br> Gib bitte die E-Mail-Adresse an,
                            <br> mit der Dein Konto registriert ist.
                            <br>
                        </div>
                        <div class="form-group">

                            {!! Form::text('email','',["id"=>'email',"class"=>"form-control modal-input","placeholder"=>"Email-Adresse","onkeypress"=>"hideErrorMsg('emailerr')"]) !!}
                            <span id="emailerr" class="text-danger"></span>
                        </div>
                        <small class="help-block text-center"><span
                                    class="text-danger">Dieses Feld darf nicht leer sein</span></small>
                    <!--<div class="form-group"> {!! Html::image('resources/assets/front/img/caprcha.png') !!} </div>-->
                        <div class="col-xs-12">
                            <label class="col-md-4 control-label">Captcha</label>
                            <div class="form-group captcha">
                                <div class="g-recaptcha" id="g-recaptcha-pass"
                                     onclick="hideErrorMsg('captcherr');"></div>

                                <style>
                                    .captcha {
                                        height: 110px;
                                    }
                                </style>
                            </div>
                            <span id="captcherr" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            {!!Form::button('Neues Passwort anfordern',["id"=>'forgotpass',"class"=>"btn btn-success btn-block brn-rg",
                            "style"=>"text-transform:none;","onclick"=>"forgot_pass()"]) !!}

                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal-add-res" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog robotoregular">
        <div class="modal-content">
            <div class="modal-header" style="background:#81c02f; color:#fff; padding: 5px 15px;">
                <button type="button" class="close" data-dismiss="modal"
                        style="color:#81c02f; opacity:1; font-size: 34px;"><span aria-hidden="true">×</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title">Restaurant Sign in Lieferzonas.at</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form method="POST" action="#" id="suggest_rest">
                        <div class="errors" id="errors" style="color: red"></div>
                        <div class="col-xs-12" id="s_msg" class="alert text-center"></div>
                        <div class="col-xs-12">
                            <small class="help-block">Gib bitte Deine Zugangsdaten ein..</small>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('delivery_service','',["id"=>'delivery_service',"class"=>"form-control modal-input","placeholder"=>"*Name des Zustelldienstes","onkeypress"=>"hideErrorMsg('delivery_serviceerr')"]) !!}
                                <span id="delivery_serviceerr" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('fname','',["id"=>'fname',"class"=>"form-control modal-input","placeholder"=>"*Vorname","onkeypress"=>"hideErrorMsg('fnameerr')"]) !!}
                                <span id="fnameerr" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('street','',["id"=>'street',"class"=>"form-control modal-input","placeholder"=>"*Straße" ,"onkeypress"=>"hideErrorMsg('streeterr')"]) !!}
                                <span id="streeterr" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('lname','',["id"=>'lname',"class"=>"form-control modal-input","placeholder"=>"*Nachname"]) !!}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('houseno','',["id"=>'houseno',"class"=>"form-control modal-input","placeholder"=>"*Hausnummer" ,"onkeypress"=>"hideErrorMsg('housenoerr')"]) !!}
                                <span id="housenoerr" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('mobile','',["id"=>'mobile',"class"=>"form-control modal-input","placeholder"=>"Handynummer" ,"onkeypress"=>"hideErrorMsg('mobileerr')"]) !!}
                                <span id="mobileerr" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('zipcode','',["id"=>'pincode',"class"=>"form-control modal-input","placeholder"=>"*Postleitzahl / Ort" ,"onkeypress"=>"hideErrorMsg('zipcodeerr')"]) !!}
                                <span id="zipcodeerr" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('phone','',["id"=>'phone',"class"=>"form-control modal-input","placeholder"=>"*Telefon" ]) !!}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">

                                {!! Form::text('email','',["id"=>'emailid',"class"=>"form-control modal-input","placeholder"=>"*Dein Email-Adresse" ,"onkeypress"=>"hideErrorMsg('emailiderr')"]) !!}
                                <span id="emailiderr" class="text-danger"></span>
                            </div>
                        </div>
                        <?php $kitchens = \App\superadmin\Kitchen::kitchenlist(); ?>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <select multiple="multiple" class="form-control modal-input js-example-basic-multiple"
                                        name="kitchen[]" id="kitchen">

                                    <?php
                                    foreach($kitchens as $kitchen) { ?>
                                    <option value="<?=$kitchen['id'] ?>"><?=$kitchen['name'] ?></option>
                                    <?php
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <h3>Info</h3>
                                <textarea class="form-control modal-input" rows="4" name="info" id="info"></textarea>
                            </div>
                        </div>
                    <!--<div class="col-xs-6">
                                <div class="form-group"> {!! Html::image('resources/assets/front/img/caprcha.png') !!} </div>
                            </div>-->
                        <div class="col-xs-12">
                            <label class="col-md-4 control-label">Captcha</label>
                            <div class="form-group captcha">
                                <div class="g-recaptcha" id="g-recaptcha-sugg"></div>

                                <style>
                                    .captcha {
                                        height: 110px;
                                    }
                                </style>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="text-center" style="font-size:14px;">
                                <div class="form-group">
                                    {!!Form::button('Registrieren',["id"=>'add',"class"=>"btn  btn-success ","onclick"=>"validate_suggestion()"]) !!}

                                </div>

                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <input type="hidden" id="token-value" value="{{ csrf_token() }}">

            {!! Html::script('resources/assets/front/js/jquery-2.2.0.min.js') !!}
            {!! Html::script('resources/assets/front/js/bootstrap.min.js') !!}
            {!! Html::script('resources/assets/front/js/bootstrap-select.min.js') !!}
            {!! Html::script('assets/js/jquery.viewportchecker.min.js') !!}
            {!! Html::script('resources/assets/front/js/jquery.validate.min.js') !!}
            {!! Html::script('resources/assets/front/js/additional-methods.js') !!}
            <script>
                var SITE_URL_PRE_FIX = '{{ Config::get('constants.SITE_URL_PRE_FIX', '/lieferzonas') }}';
            </script>
            {!! Html::script('resources/assets/js/script.js') !!}
        <!-- Added By Rajlakshmi(14-06-16) -->
            {!! Html::script('resources/assets/js/date/moment-with-locales.js') !!}
            {!! Html::script('resources/assets/js/date/bootstrap-datetimepicker.js') !!}
            {!! Html::script('resources/assets/js/jquery.slimscroll.min.js') !!}
        <!-- Added By Rajlakshmi(14-06-16) -->
            <!-- Added By Rajlakshmi(28-06-16) -->
            {!! Html::script('resources/assets/front/js/star-rating.js') !!}
            {!! Html::script('resources/assets/front/js/bootstrap-slider.js') !!}
            {!! Html::script('resources/assets/front/js/jquery_002.js') !!}
        <!-- Scroll -->
            {!! Html::script('resources/assets/front/js/bootstrap-slider.js') !!}
            {!! Html::script('resources/assets/front/js/select2.js') !!}
            <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit&hl=en"></script>
            <script type="text/javascript">
                var CaptchaCallback = function () {
                    $('.g-recaptcha').each(function () {
                        grecaptcha.render(this, {'sitekey': '6LfNzScTAAAAAL-XDJ2LHKkeACONkT3uTVVo-dCe'});
                    })
                };
            </script>
            <script type="text/javascript">
                $(".js-example-basic-multiple").select2();
            </script>
            <script>
                $(function () {
                    $('.nano').nanoScroller({
                        preventPageScrolling: true
                    });
                    $("#main").find('.description').load("readme.html", function () {
                        $(".nano").nanoScroller();
                        $("#main").find("img").load(function () {
                            $(".nano").nanoScroller();
                        });
                    });
                });
            </script>

            <!-- Added By Rajlakshmi(28-06-16) -->
            {{--<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script>
            <script src="js/bootstrap.min.js" type="text/javascript"></script>
            <script src="js/bootstrap-select.min.js" type="text/javascript"></script>
            <script src="js/viewportchecker.js"></script>--}}


            @yield('script')

        <!--@if($errors->any())
            <script type="text/javascript">
                $('#login_panel').trigger('click');
            </script>
        @endif
                -->
            <?php
            if((Session::has('socialLogin')) || $errors->any()){
            $socialLogin = Session::get('socialLogin');
            if(($socialLogin && $socialLogin != '') || ($errors->any())){ ?>
            <script type="text/javascript">
                $('#login_panel').trigger('click');
            </script>
            <?php
            }
            }
            ?>

            <script type="text/javascript">

                function show_register_users() {
                    $('#login_panel').trigger('click');
                    $('#myModal').modal('show');
                }
                function show_login_panel() {
                    $('#myModal').modal('hide');
                    setTimeout(function () {
                        $('#login_panel').trigger('click');
                    }, 500);
                }

                function showHide() {
                    var showText = "Passwort anzeigen";
                    var hideText = "Passwort verbergen";
                    var text = $('#show_password').text();
                    if (text == showText) {
                        $('#r_password').attr('type', 'text');
                        $('#show_password').text(hideText);
                    } else {
                        $('#r_password').attr('type', 'password');
                        $('#show_password').text(showText);
                    }
                }
                $('.dropdown-menu .bottom').click(function (event) {
                    event.stopPropagation();
                });
                $('.dropdown-menu .bottom h4').click(function (e) {
                    $('.inf, .loggin').slideToggle(350);
                    $('.dropdown-menu .bottom').toggleClass('act-bottom');
                });
            </script>
            <script>
                //new WOW().init();
            </script>
            <script type="text/javascript">
                $(document).ready(function () {
                    var winHeight = $(window).height();
                    var winWidth = $(window).width();
                    if (winWidth >= 992) {
                        $('.full-head').css("height", winHeight - 15);
                    }
                });
            </script>

            <script type="text/javascript">
                $(window).scroll(function () {
                    var scroll = $(window).scrollTop();
                    var scroll = $(window).scrollTop();
                    if (scroll >= 100) {
                        $("#menu").addClass("nav-fixed");
                    } else {
                        $("#menu").removeClass("nav-fixed");
                    }
                });
            </script>
            <script>
                $(document).ready(function () {
                    $('.head-bar > .container').hide();
                    $('.btn-grn-top').click(function () {
                        if ($('.head-bar > .container').is(':visible')) {
                            $('.head-bar > .container').slideUp();
                        } else {
                            $('.head-bar > .container').slideDown();
                        }
                    });
                });
            </script>

            <script type="text/javascript">

                $(document).ready(function () {
                    $("#address_form").validate({
                        rules: {
                            mobile: {
                                required: true,
                                digits: true,
                                minlength: 7,
                                maxlength: 12
                            },

                            address: {
                                required: true
                            },
                            country: {
                                required: true
                            },

                            state: {
                                required: true
                            },
                            city: {
                                required: true
                            },

                            zipcode: {
                                required: true

                            }

                        },
                        messages: {
                            mobile: {
                                required: "Please enter mobile number",
                                digits: "Please enter valid mobile number",
                                minlength: "Please enter valid mobile number",
                                maxlength: "Please enter valid mobile number"
                            },

                            address: "Please enter address",
                            country: "Please enter country",
                            state: "Please enter state",
                            city: "Please enter city",


                            zipcode: {
                                required: "Please enter zipcode"

                            },
                            agree: "Please accept our policy"
                        },
//            errorPlacement: function (error, element) {
//                if (element.a	ttr("type") == "select") {
//                    error.insertAfter($('.select2-container'));
//                } else {
//                    error.insertAfter(element);
//                    //error.insertBefore(element);
//                }
//            }

                    });
                });


                $(document).ready(function () {
                    $("#profile").validate({
                        rules: {
                            firstname: {
                                required: true
                            },
                            lastname: {
                                required: true
                            },
                            nickname: {
                                required: true
                            },
                            gender: {
                                required: true
                            },
                            dob: {
                                required: true
                            },
                            mobile: {
                                required: true,
                                digits: true,
                                minlength: 7,
                                maxlength: 12
                            }
                        },
                        messages: {
                            firstname: "Please enter firstname",
                            lastname: "Please enter lastname",
                            nickname: "Please enter nick name",
                            gender: "Please select gender",
                            dob: "Please enter date of birth",
                            mobile: {
                                required: "Please enter mobile number",
                                digit: "Please enter valid mobile number",
                                minlength: "Please enter valid mobile number",
                                maxlength: "Please enter valid mobile number"
                            },


                            agree: "Please accept our policy"
                        },

                    });
                });

                function validateimage(fieldObj) {

                    var FileName = fieldObj.value;
                    var FileExt = FileName.substr(FileName.lastIndexOf('.') + 1);
                    var FileSize = fieldObj.files[0].size;
                    var FileSizeMB = (FileSize / 5485760).toFixed(2);

                    if ((FileExt != "jpg" && FileExt != "jpge" && FileExt != "png" && FileExt != "tif" && FileExt != "bmp")) {
                        var error = "File type : " + FileExt + "\n\n";
                        error += "Please make sure profile pic is in jpg,png,tif,bmp format.\n\n";

                        alert(error);
                        return false;
                    }
                    return true;
                }
                $(document).ready(function () {
                    $('#login_panel').click(function () {
                        $('.error').html('');
                        //document.getElementById("login_form").reset();
                    });

                    $("#login_form").validate({
                        rules: {
                            email: {
                                required: true,
                                email: true
                            },
                            password: {
                                required: true,
                                minlength: 3,
                            },
                        },
                        messages: {
                            email: {
                                required: "Please enter email",
                                email: "Please enter valid email",
                            },
                            password: {
                                required: "Please enter password",
                                minlength: "Password must be atleast 3 letters",
                            },
                        }
                    });
                });

                //    function login_front_user(){
                //        var email = $('#email').val();
                //        var password = $('#password').val();
                //        if($('#login_form').valid())
                //        {
                //            $.ajax({
                //                url: '{{url('frontuser/login')}}',
                //                type: "post",
                //                data: { 'email':email, 'password':password, "_token":"{{ csrf_token() }}"},
                //                success: function(res){
                //                    if(res == 1)
                //                    {
                //                        $('#r_msg').show();
                //                    }
                //                    else
                //                    {
                //                        $('#r_msg').show();
                //                    }
                //                }
                //            });
                //        }
                //    }

                function register_user() {
                    $('#r_msg').hide();

                    $('#r_form').validate({
                        rules: {
                            r_fname: {
                                minlength: 2,
                                lettersonly: true
                            },
                            r_lname: {
                                minlength: 2,
                                lettersonly: true
                            },
                            gender: {
                                required: true
                            },
                            r_password: {
                                minlength: 6
                            },
                            conditions_policy: {
                                required: true,
                                //oneletteronenumbermust : true
                            }
                        },
                        messages: {
                            conditions_policy: {
                                required: "&nbsp;Please accept Conditions & Privacy Policy.",
                            },
                        }
                    });

                    var fname = $('#r_fname').val();
                    var lname = $('#r_lname').val();
                    var email = $('#r_email').val();
                    var password = $('#r_password').val();
                    var gender = $('#gender').val();

                    if ($('#r_form').valid()) {
                        var base_host = window.location.origin;  //give http://localhost
                        var base_url = base_host + SITE_URL_PRE_FIX + '/vikasbatra/';
                        var base_url1 = base_url + 'register';

                        $.ajax({
                            //url: base_url1,
                            url: '{{url('register')}}',
                            type: "post",
                            dataType: "json",
                            data: {
                                'fname': fname,
                                'lname': lname,
                                'gender': gender,
                                'email': email,
                                'password': password,
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function (res) {
                                if (res.status == 1) {
                                    $('#r_form').hide();
                                    $('#r_msg').html('You have successfully registered with us. Please check your e-mail and follow the instructions to activate your account.');
                                    $('#r_msg').removeClass('alert-danger');
                                    $('#r_msg').addClass('alert-success');
                                    $('#r_msg').show();
                                }
                                else if (res.status == 2) {
                                    $('#r_msg').html('This email is already registered with us. Please sign in using this email or register using a diffrent email.');
                                    $('#r_msg').addClass('alert-danger');
                                    $('#r_msg').show();
                                }
                                else if (res.status == 3) {
                                    window.location.href = res.redirecturl;
                                }
                                else {
                                    //alert('Error occured.!! Please try after some time.');
                                    $('#r_msg').html('Unknown Error occured.!! Please try again after some time.');
                                    $('#r_msg').addClass('alert-danger');
                                    $('#r_msg').show();
                                }
                            }
                        });
                    }
                }


                <!-- Added By Rajlakshmi(14-06-16) -->
                $(function () {
                    var date = new Date();
                    var currentMonth = date.getMonth();
                    var currentDate = date.getDate();
                    var currentdate = (currentDate - 30);
                    var currentyear = date.getFullYear();
                    var currentYear = (currentyear - 18);
                    $('#datetimepicker5').datetimepicker({
                        format: 'YYYY/MM/DD',
                        useCurrent: false,
                        maxDate: new Date(currentYear, currentMonth, currentDate)
                    });

                });


                function forgot_pass() {
                    if (document.forgot_pwd.email.value.trim() == '') {

                        document.forgot_pwd.email.focus();
                        $("#emailerr").html('Please enter your registered email');
                        $("#emailerr").show();
                        return false;
                    }
                    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(document.forgot_pwd.email.value))) {

                        document.forgot_pwd.email.focus();
                        $("#emailerr").html('Invalid email id. Please enter valid email');
                        $("#emailerr").show();
                        return false;
                    }
                    var email = document.forgot_pwd.email.value.trim();
                    var recaptcha = $('#g-recaptcha-pass').find('.g-recaptcha-response').val();
                    /*if(recaptcha=='')
                     {
                     $("#captcherr").html('Please enter captcha');
                     $("#captcherr").show();
                     return false;
                     }*/
                    $.ajax({
                        url: '{{url('front/fpasssendmail')}}',
                        type: "post",
                        dataType: "json",
                        data: {'email': email, 'recaptcha': recaptcha, "_token": "{{ csrf_token() }}"},
                        success: function (res) {

                            if (res.success == 1) {
                                $('#ferrors').html('');
                                alert('A link is sent to your registered email successfully!');
                            }
                            else if (res.success == 0) {
                                var errorsHtml = '';
                                $.each(res.errors, function (key, value) {
                                    errorsHtml += value + '<br>';
                                });

                                $('#ferrors').html(errorsHtml);
                            }
                            else {
                                alert('This Email id is not registred!');
                            }
                        }
                    });

                    //document.forms["forgot_pwd"].submit();
                }
                function hideErrorMsg(id) {

                    $('#' + id).attr('style', 'display:none');


                }
                <!-- Added By Rajlakshmi(14-06-16) -->
                function validate_suggestion() {
                    var recaptcha = $('#g-recaptcha-sugg').find('.g-recaptcha-response').val();
                    //alert(recaptcha);
                    var delivery_service = $('#delivery_service').val();

                    if (delivery_service == '') {
                        $('#delivery_service').focus();
                        $("#delivery_serviceerr").html('Please enter delivery service');
                        $("#delivery_serviceerr").show();
                        return false;
                    }
                    var fname = $('#fname').val();
                    if (fname == '') {
                        $('#fname').focus();
                        $("#fnameerr").html('Please enter first name');
                        $("#fnameerr").show();
                        return false;
                    }
                    var street = $('#street').val();
                    if (street == '') {
                        $('#street').focus();
                        $("#streeterr").html('Please enter street');
                        $("#streeterr").show();
                        return false;
                    }
                    var lname = $('#lname').val();

                    var houseno = $('#houseno').val();
                    if (houseno == '') {
                        $('#houseno').focus();
                        $("#housenoerr").html('Please enter house no');
                        $("#housenoerr").show();
                        return false;
                    }
                    var mobile = $('#mobile').val();
                    if (mobile == '') {
                        $('#mobile').focus();
                        $("#mobileerr").html('Please enter mobile no');
                        $("#mobileerr").show();
                        return false;
                    }
                    if (isNaN(mobile)) {
                        $("#mobile").focus();
                        $("#mobileerr").html('Please enter digit for mobile');
                        $("#mobileerr").show();
                        return false;
                    }
                    if (mobile.length > 11) {
                        $("#mobile").focus();
                        $("#mobileerr").html('Please enter less than or equal to 11 digits for mobile');
                        $("#mobileerr").show();
                        return false;
                    }
                    if (mobile.length < 7) {
                        $("#mobile").focus();
                        $("#mobileerr").html('Please enter more than or equal to 7 digits for mobile');
                        $("#mobileerr").show();
                        return false;
                    }
                    var pincode = $('#pincode').val();

                    if (pincode == '') {
                        $('#pincode').focus();
                        $("#zipcodeerr").html('Please enter zipcode');
                        $("#zipcodeerr").show();
                        return false;
                    }
                    /*if(isNaN(pincode)){
                     $('#pincode').focus();
                     $("#zipcodeerr").html('Please enter digit for zipcode');
                     $("#zipcodeerr").show();
                     return false;
                     }
                     if(pincode.length>6){
                     $("#pincode").focus();
                     $("#zipcodeerr").html('Please enter only 6 digits for zipcode');
                     $("#zipcodeerr").show();
                     return false;
                     }
                     if(pincode.length<6){
                     $("#pincode").focus();
                     $("#zipcodeerr").html('Please enter atleast 6 digits for zipcode');
                     $("#zipcodeerr").show();
                     return false;
                     }*/
                    var phone = $('#phone').val();
                    var email = $('#emailid').val();
                    //alert(email);
                    if (email == '') {
                        $('#emailid').focus();
                        $("#emailiderr").html('Please enter email');
                        $("#emailiderr").show();
                        return false;
                    }
                    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))) {
                        $('#emailid').focus();
                        $("#emailiderr").html('Please enter valid email');
                        $("#emailiderr").show();
                        return false;
                    }

                    var kitchenselected = new Array();
                    $("#kitchen :selected").each(function () {
                        kitchenselected.push($(this).val());
                    });
                    var kitchenlength = kitchenselected.length;
                    //alert(kitchenlength);
                    if (kitchenlength < 1) {
                        $('#kitchen').focus();
                        $("#kitchenerr").html('Please select atleast one kitchen');
                        $("#kitchenerr").show();
                        return false;
                    }
                    //var base_host = window.location.href;
                    //base_host = base_host.substring(0, base_host.lastIndexOf("/"));
                    //var base_url1 = base_host+'front/suggestrest';

                    //alert(kitchenselected);
                    var info = $('#info').val();
                    $.ajax({
                        url: '{{url('front/suggestrest')}}',
                        type: "post",
                        dataType: "json",
                        data: {
                            'delivery_service': delivery_service,
                            'fname': fname,
                            'street': street,
                            'lname': lname,
                            'houseno': houseno,
                            'mobile': mobile
                            ,
                            'zipcode': pincode,
                            'phone': phone,
                            'email': email,
                            'kitchen': kitchenselected,
                            'info': info,
                            'recaptcha': recaptcha,
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function (res) {
                            if (res.success == 1) {
                                //alert('you successfully suggest a restuarant!');
                                $('#s_msg').html('You have successfully suggested a restaurant.');
                                $('#s_msg').removeClass('alert-danger');
                                $('#s_msg').addClass('alert-success');
                                $('#s_msg').show();
                                $('#errors').html('');

                                document.getElementById("suggest_rest").reset();
                                //$('#myModal-add-res').modal("hide");
                            }
                            if (res.success == 0) {       //alert('hi');
                                var errorsHtml = '';
                                $.each(res.errors, function (key, value) {
                                    errorsHtml += value + '<br>';
                                });
                                //alert(errorsHtml);
                                $('#errors').html(errorsHtml);
                            }
                        }
                    });
                }


                function qrating_hover() {

                    var qrating = $('.qrating #rating').val();
                    var drating = $('.drating #rating').val();

                    var aaaa = "Not Rated";

                    var dddd = "Not Rated";
                    if (qrating != dddd) {
                        $(".qrating #qratingerr").hide();

                        if (drating != aaaa) {
                            $(".drating #dratingerr").hide();
                            return false;
                        }

                        return false;
                    }


                }

                function add_review() {

                    var user_id = $('#user_id').val();
                    var rest_id = $('#rest_id').val();
                    var order_id = $('#order_id').val();
                    var qrating = $('.qrating #rating').val();
                    var drating = $('.drating #rating').val();
                    var comment = $('#comment').val();
                    if (qrating == 'Not Rated') {

                        $("#qratingerr").html('Please enter quality rating');
                        $("#qratingerr").show();
                        //alert('Plesse enter quality rating');
                        return false;
                    }
                    if (drating == 'Not Rated') {
                        $("#dratingerr").html('Please enter delivery rating');
                        $("#dratingerr").show();
                        //alert('Plesse enter delivery rating');
                        return false;
                    }
                    var base_host = window.location.href;
                    base_host = base_host.substring(0, base_host.lastIndexOf("/"));
                    var base_url1 = base_host + '/orderrate/add';
                    $.ajax({
                        url: base_url1,
                        type: "post",
                        dataType: "json",
                        data: {
                            'user_id': user_id,
                            'rest_id': rest_id,
                            'order_id': order_id,
                            'qrating': qrating,
                            'drating': drating,
                            'comment': comment,
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function (res) {
                            if (res.success == 1) {
                                location.reload();
                            }

                        }
                    });

                }
                function hideErrorMsg(id) {

                    $('#' + id).attr('style', 'display:none');


                }
                function open_modal(user_id, rest_id, order_id) {
                    $('#user_id').val(user_id);
                    $('#rest_id').val(rest_id);
                    $('#order_id').val(order_id);
                    $('#myModal_rating').modal('show');
                }


                function pass_validation() {
                    var oldpassword = $("#oldpassword").val().trim();
                    if (oldpassword == "") {
                        $("#oldpassword").focus();
                        $("#opasserr").html('Please enter old password');
                        $("#opasserr").show();
                        return false;
                    }
                    var newpassword = $("#newpassword").val().trim();
                    if (newpassword == "") {

                        $("#newpassword").focus();
                        $("#npasserr").html('Please enter new password');
                        $("#npasserr").show();
                        return false;
                    }
                    var newpasswordlength = $("#newpassword").val().length;
                    if (newpasswordlength < 6) {
                        $("#newpassword").focus();
                        $("#npasserr").html('New password contains more than 6 characters');
                        $("#npasserr").show();
                        return false;
                    }
                    var conpassword = $("#conpassword").val().trim();
                    if (conpassword == "") {

                        $("#conpassword").focus();
                        $("#cpasserr").html('Please enter confirm password');
                        $("#cpasserr").show();
                        return false;
                    }
                    var conpasswordlength = $("#conpassword").val().length;
                    if (conpasswordlength < 6) {
                        $("#conpassword").focus();
                        $("#cpasserr").html('Confirm password contains more than 6 characters');
                        $("#cpasserr").show();
                        return false;
                    }
                    if (conpassword != newpassword) {

                        $("#conpassword").focus();
                        $("#cpasserr").html('Please enter confirm password same as new password');
                        $("#cpasserr").show();
                        return false;
                    }
                    document.forms["change_pass"].submit();

                }


                function validate_pass() {

                    var newpassword = $("#newpassword").val();
                    if (newpassword == "") {

                        $("#newpassword").focus();
                        $("#npasserr").html('Please enter new password');
                        $("#npasserr").show();
                        return false;
                    }
                    var newpasswordlength = $("#newpassword").val().length;
                    if (newpasswordlength < 6) {
                        $("#newpassword").focus();
                        $("#npasserr").html('New password contains more than 6 characters');
                        $("#npasserr").show();
                        return false;
                    }
                    var conpassword = $("#conpassword").val();
                    if (conpassword == "") {

                        $("#conpassword").focus();
                        $("#cpasserr").html('Please enter confirm password');
                        $("#cpasserr").show();
                        return false;
                    }
                    var conpasswordlength = $("#conpassword").val().length;
                    if (conpasswordlength < 6) {
                        $("#conpassword").focus();
                        $("#cpasserr").html('Confirm password contains more than 6 characters');
                        $("#cpasserr").show();
                        return false;
                    }
                    if (conpassword != newpassword) {

                        $("#conpassword").focus();
                        $("#cpasserr").html('Please enter confirm password same as new password');
                        $("#cpasserr").show();
                        return false;
                    }
                    document.forms["reset_pass"].submit();

                }
                function cancel_order(id) {
                    var x = confirm('Do you want to cancle this order!');
                    if (x == 1) {
                        var order_id = id;
                        var base_host = window.location.href;
                        base_host = base_host.substring(0, base_host.lastIndexOf("/"));
                        var base_url1 = base_host + '/order/cancel';
                        $.ajax({
                            url: base_url1,
                            type: "post",
                            dataType: "json",
                            data: {'order_id': order_id, "_token": "{{ csrf_token() }}"},
                            success: function (res) {
                                if (res.success == 1) {
                                    location.reload();
                                }

                            }
                        });
                    }
                }

                function getdatas(val, tableName, mapTable, appendId, joinColumn, selectColumn, whereColumn, secondJoinColumn, selectedOption=0) {

                    $.ajax({

                        url: '{{url('getdatas')}}',
                        type: "get",
                        data: {
                            'value': val,
                            'table': tableName,
                            'maptable': mapTable,
                            'joincolumn': joinColumn,
                            'selectcolumn': selectColumn,
                            'wherecolumn': whereColumn,
                            'secondjoincolumn': secondJoinColumn,
                            'selectedOption': selectedOption
                        },

                        success: function (data) {

                            $('#' + appendId).html(data);

                        },
                        error: function (jqXHR, textStatus, errorThrown) {

                        }
                    });

                }
            </script>

            <script type="text/javascript">
                if (window.location.hash == '#_=_') {
                    history.replaceState
                            ? history.replaceState(null, null, window.location.href.split('#')[0])
                            : window.location.hash = '';
                }
            </script>

            <script type="text/javascript">
                $(function () {
                    var current = location.pathname;
                    $('#login-dp a').each(function () {
                        var $this = $(this);
                        if ($this.attr('href').indexOf(current) !== -1) {
                            $this.addClass('active');
                        }
                    })
                })
            </script>

</body>
</html>
