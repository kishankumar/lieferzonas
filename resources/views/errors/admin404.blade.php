@extends('layouts.adminbody')

@section('title')
@endsection

@section('shopcart')
@endsection

@section('body')
<div class="col-sm-9">
    <div id="right_side">
        <div class="row">
            <div class="col-sm-12">
                <section class="arror404">
                    <aside> 
                        {!! Html::image('resources/assets/img/404.png','') !!}
                        <h1>Page Not Found</h1>
                        <p>The page you've requested could not be found on the server. Please contact your webmaster, or use the back button in your browser to navigate back to your most recent active page.</p>
                        <!--<a class="btn btn-green btn-lg" href="#">Back to Home <i class="icon icon-home"></i></a>-->
                    </aside>
                </section>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
@endsection

<style>
    .arror404{ margin: 50px auto 0; max-width: 800px; padding-bottom: 15px; text-align: center; padding:10px;}
    .arror404 h1{color:#C42127; font-size:30px; margin:5px 0 20px 0}
    .arror404 aside{padding:15px;}
    .arror404 aside img{max-width:100%;}
</style>



