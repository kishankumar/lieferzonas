<div class="col-sm-4  col-md-4 col-lg-4">
    <div class="content-box">
        <div class="tabbing-side">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#dashboard" data-toggle="tab">Dashboard </a></li>
                <li><a href="#meine-konto" data-toggle="tab">Meine Konto</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="dashboard">
                    <div class="collem-side">
                        <p class="text-center"><a href="{{ url('front/address/create') }}" class="redborderbtn btn">Neue-Adressen Hinzufügen</a></p>
                        <h2>Zustell-Adressen</h2>
						<?php 
						 $uaddress = \App\front\FrontUserAddress::useraddress();
						 ?>
                        <ul class="list-unstyled list">
						@if(count($uaddress))
						@foreach($uaddress as $uaddresses)
                            <li><a href="#">
                                    <div class="media">
                                        <figure class="pull-left"><img src="img/face.png" alt=""> </figure>
                                        <aside class="media-body"> 
										<span>{{ $uaddresses['address'].' '.$uaddresses['landmark'].' '.$uaddresses['city_name'].' '.$uaddresses['state_name'].' '.$uaddresses['country_name'].' '.$uaddresses['zipcode'] }}  </span> 
										<small>Mobile:{{ $uaddresses['mobile'] }}</small> </aside>
                                        <i class="icon-rght fa fa-angle-right"></i> </div>
                            </a> </li>
						 @endforeach
						 @endif
                            <!--<li><a href="#">
                                    <div class="media">
                                        <figure class="pull-left"><img src="img/face.png" alt=""> </figure>
                                        <aside class="media-body"> <span>Margaretenstrasse 102 </span> <small>10117 Berlin (Mitte) </small> </aside>
                                        <i class="icon-rght fa fa-angle-right"></i> </div>
                                </a> </li>
                            <li><a href="#">
                                    <div class="media">
                                        <figure class="pull-left"><img src="img/face.png" alt=""> </figure>
                                        <aside class="media-body"> <span>Margaretenstrasse 102 </span> <small>10117 Berlin (Mitte) </small> </aside>
                                        <i class="icon-rght fa fa-angle-right"></i> </div>
                                </a> </li>
                            <li><a href="#">
                                    <div class="media">
                                        <figure class="pull-left"><img src="img/face.png" alt=""> </figure>
                                        <aside class="media-body"> <span>Margaretenstrasse 102 </span> <small>10117 Berlin (Mitte) </small> </aside>
                                        <i class="icon-rght fa fa-angle-right"></i> </div>
                                </a> </li>-->
                        </ul>
                        <h2>Bestellungen</h2>
                        <div class="order-list">
						 <?php 
						 $uorder = \App\front\UserOrder::userorder();
						    
						 ?>
						 
                            
								<?php $i=1;?>
								@if(count($uorder))
								@foreach($uorder as $uorders)
							    <?php
								if($uorders['logo']!='')
								{
									$logo = $uorders['logo'];
								}
								else{
									$logo = 'logo_de.png';
								}
								 $order_id = $uorders['order_id']; 
						         $uorderitem = \App\front\OrderItem::userorderitem($order_id);  //print_r($uorderitem);  ?> 
								 
                                 <div class="peoplebox2 media mt0">
                                  <div class="pull-left"><img alt="" src="{{asset('public/uploads/superadmin/restaurants/'.$logo) }}" height="70"></div>
                                  <aside class="media-body app-sp">
                                    <div class="row">
									<input type="hidden" value="<?=$uorders['delivery_time']?>" name="del_time" id="delivery_time<?= $i?>">
                                        <div class="col-xs-7">
										    <a href="#" class="text-grey">{{ $uorders['f_name'] }}</a>
                                            <p><span class="text-success">#{{ $uorders['order_id'] }}</span>{{ date("d M Y H:i:s", strtotime($uorders['created_at'])) }}</p>
											
                                            <p>{{ $uorders['address'].' '.$uorders['landmark'].' '.$uorders['zipcode'].' '.$uorders['city_name'].' '.$uorders['state_name'].' '.$uorders['country_name']  }} </p>
											
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="pricebox-1">
											
                                              <aside>{{ '€ '. $uorders['total_amount'] }}</aside>
                                               <span class="countdown" id="countdown<?=$i?>">0:0:0</span>
                                                <div id="callme<?=$i?>" style="display:none;"><nav> 
												<a class="btn btn-xs redborderbtn" href="tel:<?php echo $uorders['mobile']; ?>">Anrufen</a> </nav></div>
                                                <a class="btn-xs" href="{{url('front/order/detail/'.$uorders['id'])}}">Details</a> </div>
                                        </div>
                                    </div>
									<div class="clearfix"></div>
                                     </aside>
                            		</div>
									<?php $i++; ?>
									 @endforeach
									 @endif
                                       
                               
                           <!-- <div class="peoplebox2 media">
                                <div class="pull-left"><img alt="" src="img/list-logo.png" height="70"></div>
                                <aside class="media-body app-sp">
                                    <div class="row">
                                        <div class="col-xs-7"> <a href="#" class="text-grey">Amigo Pizzeria</a>
                                            <p><span class="text-success">#W105060DUD2</span> 20-01-16 um 19:05</p>
                                            <p>Lieferung nach
                                                Steinstrasse 172
                                                1584 Manheim</p>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="pricebox-1">
                                                <aside>€ 15,56</aside>
                                                <span>00:00:00:</span>
                                                <nav> <a class="btn btn-xs redborderbtn" href="#">Anrufen</a> </nav>
                                                <a class="btn-xs" href="#">Details</a> </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                            <div class="peoplebox2 media">
                                <div class="pull-left"><img alt="" src="img/list-logo.png" height="70"></div>
                                <aside class="media-body app-sp">
                                    <div class="row">
                                        <div class="col-xs-7"> <a href="#" class="text-grey">Amigo Pizzeria</a>
                                            <p><span class="text-success">#W105060DUD2</span> 20-01-16 um 19:05</p>
                                            <p>Lieferung nach
                                                Steinstrasse 172
                                                1584 Manheim</p>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="pricebox-1">
                                                <aside>€ 15,56</aside>
                                                <span>00:00:00:</span>
                                                <nav> <a class="btn btn-xs redborderbtn" href="#">Anrufen</a> </nav>
                                                <a class="btn-xs" href="#">Details</a> </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                            <div class="peoplebox2 media">
                                <div class="pull-left"><img alt="" src="img/list-logo.png" height="70"></div>
                                <aside class="media-body app-sp">
                                    <div class="row">
                                        <div class="col-xs-7"> <a href="#" class="text-grey">Amigo Pizzeria</a>
                                            <p><span class="text-success">#W105060DUD2</span> 20-01-16 um 19:05</p>
                                            <p>Lieferung nach
                                                Steinstrasse 172
                                                1584 Manheim</p>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="pricebox-1">
                                                <aside>€ 15,56</aside>
                                                <span>00:00:00:</span>
                                                <nav> <a class="btn btn-xs redborderbtn" href="#">Anrufen</a> </nav>
                                                <a class="btn-xs" href="#">Details</a> </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>-->
                        </div>
                    </div>
                </div>
				<?php $udetail = \App\front\FrontUserDetail::userdetail(); 

                        ?>
                <div class="tab-pane fade" id="meine-konto">
                    <div class="collem-side">
                        <ul class="tabbing-side-links">
                            <li><a class="" href="{{ url('front/profile/'.$udetail['0']['id'].'/edit') }}">@if($udetail['0']['profile_pic'])
                                       <img src="{{asset('public/front/uploads/users/'.$udetail['0']['profile_pic']) }}"></img>
                                    @else
                                        <img src="{{asset('resources/assets/front/img/face.png') }}"></img>

                                    @endif <span> {{ ucfirst($udetail['0']['fname']).' '.ucfirst($udetail['0']['lname']) }}</span></a></li>
                            <li><a class="" href="{{ url('front/profile/'.$udetail['0']['id'].'/edit') }}">Meine Daten</a></li>
							<li><a class="" href="{{route('front.address.index')}}">Meine Addressen</a></li>
							<li><a class="" href="{{route('front.changepass.index')}}">Passwort ändern</a></li>
                            <li><a class="" href="{{route('front.bonuspoint.index')}}" >Bonus Punkte</a></li>
							<li><a class="" href="{{route('front.bonus_point_history.index')}}" >Bonus Point History</a></li>
                            <li><a class="" href="{{route('front.cashbackpoint.index')}}">Cashback Punkte</a></li>
							<li><a class="" href="{{route('front.cashback_point_history.index')}}" >Cashback Point History</a></li>
                            <li><a class="" href="{{route('front.stamp.index')}}">Stempelkarten</a></li>
                            <li><a class="" href="{{route('front.favourite.index')}}">Favoriten</a></li>
                            <li><a class="" href="{{route('front.order.index')}}">Bestellungen</a></li>
                            <li><a class="" href="{{route('front.orderrate.index')}}">Orders Rate</a></li>
                            <li><a href="{{route('front.complaint.index')}}" class="">Beschwerden</a></li>
                            <li><a class="" href="{{url('frontuser/logout')}}">Ausloggen</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 @section('script')
 <script>
    

    function showRemaining() {
        
		var length = $(".countdown").length;
		//alert(length);
       
		for(i=1 ; i<=length ; i++)
		{
			
			var now = new Date();
			var del_time = 'delivery_time'+i;
			var delivery_time = $('#'+del_time).val();
			var end = new Date(delivery_time);
			var _second = 1000;
			var _minute = _second * 60;
			var _hour = _minute * 60;
			var _day = _hour * 24;
			var timer;
			var cnt = 'countdown'+i;
			var distance = end - now;
			var callbtn = "callme"+i;
		
		    if (distance < 0) 
			{
                $('#'+callbtn).show();
				clearInterval(timer);
				
			}
			else
			{
				var days = Math.floor(distance / _day);
				var hours = Math.floor((distance % _day) / _hour);
				var minutes = Math.floor((distance % _hour) / _minute);
				var seconds = Math.floor((distance % _minute) / _second);

				// document.getElementById('countdown').innerHTML = days + ':';
				document.getElementById(cnt).innerHTML = hours + ':';
				document.getElementById(cnt).innerHTML += minutes + ':';
				document.getElementById(cnt).innerHTML += seconds ;
			}
			
			
		}
       
    }

    timer = setInterval(showRemaining, 1000);
	function favourite(rest_id,user_id)
	{
		
		var base_host = window.location.href;
		base_host = base_host.substring(0, base_host.lastIndexOf("/"));
        var base_url = base_host+'/front/addfavourite';
		$.ajax({
					url: '{{url('front/addfavourite')}}',
					type: "post",
					dataType:"json",
					data: {'rest_id':rest_id,'user_id':user_id ,"_token":"{{ csrf_token() }}"},
					success: function(res){
					   
						if(res.success==1)
						{
							location.reload(); 
						}
					   
					}
				});
	}
</script>
 @endsection