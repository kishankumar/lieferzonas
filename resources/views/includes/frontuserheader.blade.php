<section class="main-dash-section">
    <div class="container">
	<?php 
	$totalbonuspoint = \App\front\UserBonusPoint::bonuspoint(); 
    $totalcashbackpoint = \App\front\UserCashbackPoint::cashbakpoint(); 
    ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="bonus-point">
                    <h3>Treuepunkte: <span>{{ $totalbonuspoint }}</span></h3>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="bonus-point">
                    <h3>CashBack-Punkte: <span>{{ $totalcashbackpoint }}</span></h3>
                </div>
            </div>
        </div>
    </div>
</section>