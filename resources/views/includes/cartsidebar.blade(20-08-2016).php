    <div class="tab-pane active" id="menus">
        <div class="action_menu">
            <h4>Quitzdorf Fruits & Vegetables - L <span>200<br>
                <i class="fa fa-heart"></i></span> 
            </h4>
            <div class="media">
                <figure class="pull-left">
                    {!! Html::image('resources/assets/front/img/logo_de.png','',['class'=>'img-responsive']) !!}
                </figure>
                <aside class="media-body"> 
                    <?php $totalDayTiminng = \App\front\UserOrderReview::getDayTime($restId);?>
                    @if(count($totalDayTiminng))
                        <span class="green">
                            <i class="fa fa-clock-o"></i> 
                            {{ $totalDayTiminng->open_time }} to {{ $totalDayTiminng->close_time }}
                        </span><br>
                    @else
                        <span class="green">
                            <i class="fa fa-clock-o"></i> 
                            Closed
                        </span><br>
                    @endif
                    Delivery Costs: ab 0,00 € <br>
                    Delivery date: 9,00 € <br>
                    <small>see info: </small> 
                </aside>
                <p>
                    <?php
                    $totalrating = \App\front\UserOrderReview::totalrating($restId);
                    if($totalrating != '')
                    {
                        $count = $totalrating/10;
                        $rating = \App\front\UserOrderReview::rating($restId);
                        $quality_rating = '0';
                        $service_rating = '0';
                        $starrating =0;
                        foreach($rating as $ratings)
                        {
                            $quality_rating  = (float)$ratings->quality_rating + (float)$quality_rating;
                            $service_rating  = (float)$ratings->service_rating+(float)$service_rating;
                            $total_rating = $quality_rating+$service_rating;
                        }
                        $starrating = $total_rating/($count*2);
                    }
                    if($totalrating != '')
                    {
                        $review=floor($starrating);
                        for($i=1; $i<=$review; $i++)
                        {
                            echo '<i class="fa fa-star"></i>';
                        }
                        if(ceil($starrating) > $review )
                        {
                            echo '<i class="fa fa-star-half-o"></i>';
                        }
                        if(5 > ceil($starrating) )
                        {
                            for($i = ceil($starrating); $i<5; $i++)
                            {
                                echo '<i class="fa fa-star-o text-gray"></i>';
                            }
                        } ?>
                        <small>({{ $totalrating/10 }})</small>
                    <?php } else{
                        echo 'No Reviews Yet';
                    }
                    ?> 
                </p>
            </div>
        </div>
        
    {!! Html::image('resources/assets/front/img/card.png','',['class'=>'img-responsive']) !!}
        
    {{--{!! Form::open(array('url'=>'checkout','id'=>'')) !!}
    {!! Form :: hidden('address','',['id'=>'address-id']) !!}
    {!! Form :: submit('Place order',['id'=>'place-order','class'=>'btn btn-success btn-block']) !!}
    <span class="error" id="address-err" style="display:none;">Firstly select an address </span>
    {!! Form :: close() !!}--}}
        
    <?php 
        if (count(Session::get('cart'))>0) {
            if($restId==Session::get('cart')[0]['restid']){
                $displayCart = "block";
            }else{
                $displayCart = "none";
            }
        }else{
            $displayCart = "none";
        }
    ?>
    <div id="FrontShoppingCart" style="display:<?=$displayCart?>">
        <div class="SideBarFix">        
            <div class="action_menu">
                <h4>
                    {!! Html::image('resources/assets/front/img/cart.png','') !!}
                    Shopping
                    <!--<span><a href="#"><i class="fa fa-trash"></i></a></span>--> 
                </h4>
            </div>
            <div class="nano_height">
                <div class="nano">
                    <div class="nano-content">
                        <div id="cart-parent-id">
                            <?php $sum=0; ?>
                            <?php 
                            if (count(Session::get('cart'))>0) {
                                //Session::put('cart',array());
                                if($restId==Session::get('cart')[0]['restid']){
                            ?>
                                    @foreach(Session::get('cart') as $cartData)
                                        <?php $sum=$sum+$cartData['price'];  ?>

                                        <div class="item_list">
                                            <div class="left"><span id="cart-qty"><?php echo $cartData['qty'] ?></span>
                                                <?php echo $cartData['itemname'] ?>
                                            </div>
                                            <aside>  
                                                <a href="javascript:void(0)" onclick="setCart('{{$cartData['itemType']}}','{{$cartData['itemId']}}','{{$cartData['restid']}}','add')">
                                                    <i class="fa fa-plus"></i> 
                                                </a> 
                                                <a href="javascript:void(0)" onclick="setCart('{{$cartData['itemType']}}','{{$cartData['itemId']}}','{{$cartData['restid']}}','substract')">
                                                    <i class="fa fa-minus"></i> 
                                                </a> 
                                                <a href="javascript:void(0)" onclick="setCart('{{$cartData['itemType']}}','{{$cartData['itemId']}}','{{$cartData['restid']}}','remove')">
                                                    <i class="fa fa-trash"></i> 
                                                </a> <br>
                                                <strong id="cart-item-price"><?php echo $cartData['price'] ?> €</strong> 
                                            </aside>
                                        </div>
                                    @endforeach
                                <?php }
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="cart-total-amount" value="<?php echo $sum; ?>">
            <input type="hidden" id="temp-totalcartval" value="<?php echo $sum; ?>">

                      {{--<div class="item_list">
                        <div class="left">Order: </div>
                        <aside> 7,73 € </aside>
                        <div class="left">Minimum value: </div>
                        <aside> 7,73 € </aside>
                      </div>
                      <div class="item_list">
                        <div class="left">For minimum orders missing: </div>
                        <aside> 7,73 € </aside>
                        <div class="left">Free Shipping: </div>
                        <aside> 7,73 € </aside>
                      </div>--}}
                      <div class="clearfix"></div>

            <h4><strong>Sum <span class="pull-right" id="cart-total-sum"><?php echo $sum ?> €</span></strong></h4>
            <a href="{{ url('restaurant/'.$restId.'/review-order') }}"  class="btn btn-success btn-block" id="ckeckout">Proceed To Checkout</a>
        </div>
    </div>
</div>

@section('script')

{!! Html::script('resources/assets/front/js/lockfixed.js') !!}
<script>
	(function($) {
		var foot_height = $('footer').height();
		$.lockfixed(".SideBarFix", {
			offset: {
				top: 10,
				bottom: foot_height
			}
		});
		
//		var Shopping_wt = $('#Shopping').width();
		$('.tabbing-side a').click(function(){
			$.lockfixed(".SideBarFix", {
				offset: {
					top: 10,
					bottom: foot_height
				}
			});
			$('.SideBarFix').css('width',Shopping_wt);
		});
		
	})(jQuery);
</script>

    <script type="text/javascript">
		function itemfun() {			
			var item_count = $('.item_list').length;
			var nano_height =  item_count*$('.item_list').outerHeight()+5
			if(item_count<5){
				$('.nano_height').css('height',nano_height);
			}
		}
		itemfun();
	</script>

    <script type="text/javascript">

        $("#place-order").click(function () {

            var address=$("#address-id").val();

            if(address=='' || address==null){

                $("#address-err").attr("style","display:block");
                return false;

            }
            else{

                $("#address-err").attr("style","display:none");
            }

        });


        function setCart(itemType,itemId,restId,eventType) {

            var token=$("#token-value").val();

            $.ajax({
                url: base_url+'/cart/addcart',
                type: "POST",
                dataType:"json",
                data: {'itemId':itemId, 'itemType':itemType,'restId':restId,'eventType':eventType ,'_token':token},
                success: function (data) {
                    console.log(data);
                    $("#cart-parent-id").html('');  //
                    var totalSum=0;
                    $.each(data,function(i,item)
                    {
                        totalSum=parseInt(totalSum)+parseInt(item.price);
                        $('#cart-parent-id').append('<div class="item_list"><div class="left"><span id="cart-qty">'+item.qty+'</span>'+item.itemname+' </div><aside> <a href="javascript:void(0)" onclick="setCart(\''+item.itemType+'\',\''+item.itemId+'\',\''+item.restid+'\',{{"'add'"}})"><i class="fa fa-plus"></i> </a><a href="javascript:void(0)" onclick="setCart(\''+item.itemType+'\',\''+item.itemId+'\',\''+item.restid+'\',{{"'substract'"}})"><i class="fa fa-minus"></i> </a> <a href="javascript:void(0)" onclick="setCart(\''+item.itemType+'\',\''+item.itemId+'\',\''+item.restid+'\',{{"'remove'"}})"><i class="fa fa-trash"></i> </a> <br><strong id="cart-item-price">'+item.price+' €</strong> </aside> </div>');
                    });
                    $("#cart-total-sum").html(totalSum+' €');
                    $("#cart-total-amount").val(totalSum);
                    $("#temp-totalcartval").val(totalSum);
                    if(totalSum > 0){
                        $('#FrontShoppingCart').show();
                    }else{
                        $('#FrontShoppingCart').hide();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
			setTimeout(function(){
				itemfun();
			}, 400);
        }

        function editAddressModal(id) {

            var token=$("#token-value").val();

            $.ajax({
                url: base_url+'/front/ajax/getAddress',
                type: "POST",
                dataType:"json",
                data: {'addressId':id,'_token':token},

                success: function (data) {
                    console.log(data);

                    $("#edit-name").val(data[0].booking_person_name);
                    $("#edit-pincode").val(data[0].zipcode);
                    $("#edit-address").val(data[0].address);
                    $("#edit-landmark").val(data[0].landmark);
                    $("#edit-mobile").val(data[0].mobile);
                    $("#edit-address-id").val(data[0].id);

                    $("#Edit_address").modal('show');
                    //$("#cart-total-sum").html(totalSum+' €');

                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });


        }

        function setAddressModal(eventType) {

            var data={};

            var validateEdit = $("#edit-address-form").valid();
            var validateAdd = $("#add-address-form").valid();

            if (validateEdit==false) {
                return false;
            }

            else if(validateAdd==false) {
                return false;
            }
            else{



                if (eventType == 'edit') {


                    data['type'] = eventType;
                    data['id'] = $("#edit-address-id").val();
                    data['userId'] = $("#edit-user-id").val();
                    data['personName'] = $("#edit-name").val();
                    data['zipcode'] = $("#edit-pincode").val();
                    data['address'] = $("#edit-address").val();
                    data['landmark'] = $("#edit-landmark").val();
                    data['mobile'] = $("#edit-mobile").val();

                }
                else {


                    data['type'] = eventType;
                    data['userId'] = $("#add-user-id").val();
                    data['personName'] = $("#add-name").val();
                    data['zipcode'] = $("#add-pincode").val();
                    data['address'] = $("#add-address").val();
                    data['landmark'] = $("#add-landmark").val();
                    data['mobile'] = $("#add-mobile").val();
                }


                var token = $("#token-value").val();

                $.ajax({
                    url: base_url + '/front/ajax/setAddress',
                    type: "POST",
                    dataType: "json",
                    data: {'data': data, '_token': token},

                    success: function (data) {
                        location.reload();
                        return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
            }


        }

        function setAddress(id) {

            $("#address-id").val(id);

        }

        function deleteAddressModal(addressId) {

            var token = $("#token-value").val();

            if (confirm("Are you sure you want to delete!") == true) {
                $.ajax({
                    url: base_url + '/front/ajax/deleteAddress',
                    type: "POST",
                    dataType: "json",
                    data: {'addressId': addressId, '_token': token},

                    success: function (data) {
                        location.reload();
                        return false;

                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
            }
        }

        $(document).ready(function(){
            $("#add-address-form").validate({
                rules: {

                    add_name: {
                        required: true
                    },

                    add_pincode: {
                        required: true,
                        digits:true,
                        minlength:4,
                        maxlength:7,

                    },
                    add_address: {
                        required: true

                    },
                    add_mobile: {
                        required: true,
                        digits:true

                    }

                },
                messages: {
                    add_name: "Please enter name",
                    add_pincode:{
                        required: "Please enter pincode",
                        digits: "Please enter pincode in numbers",
                        minlength: "Please enter valid pincode",
                        maxlength: "Please enter valid pincode",
                    },
                    //add_pincode: "Please enter pincode",
                    add_address: "Please enter address",
                    add_mobile: "Please enter valid phone"


                },

            });

            $("#edit-address-form").validate({
                rules: {

                    edit_name: {
                        required: true
                    },

                    edit_pincode: {
                        required: true,
                        digits:true,
                        minlength:4,
                        maxlength:7,

                    },
                    edit_address: {
                        required: true

                    },
                    edit_phone: {
                        required: true,
                        digits:true

                    }

                },
                messages: {
                    edit_name: "Please enter name",
                    edit_pincode:{
                        required: "Please enter pincode",
                        digits: "Please enter pincode in numbers",
                        minlength: "Please enter valid pincode",
                        maxlength: "Please enter valid pincode",
                    },
                    edit_address: "Please enter address",
                    edit_phone: "Please enter valid phone"


                },

            });


            $("#cart-register-form").validate({
                rules: {

                    fisrt_name: {
                        required: true
                    },

                    email: {
                        required: true,
                        email: true

                    },
                    password: {
                        required: true,
                        minlength:6

                    },
                    phone: {
                        required: true,
                        digits:true

                    },
                    address: {
                        required: true,
                        minlength:15

                    },
                    zipcode: {
                        required: true,
                        digits:true

                    }

                },
                messages: {
                    fisrt_name: "Please enter name",
                    email: "Please enter valid email",
                    password: "Please enter password",
                    phone: "Please enter valid phone",
                    address: "Please enter valid address",
                    zipcode: "Please enter valid zipcode"


                },

            });

            $("#pre-order-day").change(function () {

                var token = $("#token-value").val();
                var restId=$("#restId").val();
                var currentTime = new Date();
                var month = currentTime.getMonth() + 1;
                if(month<10){
                    month='0'+month;
                }
                else{
                    month = month;
                }
                var day = currentTime.getDate();
                if(day<10){
                    day= '0'+day;
                }
                else{
                    day=day;
                }
                var year = currentTime.getFullYear();
                var todayDate=day+'-'+month+'-'+year;

                if(todayDate.trim()==$(this).val()){
                    $("#today").attr('style','display:block');
                    $("#otherday").attr('style','display:none');
                }
                else{

                    $.ajax({
                        url: base_url+'/front/ajax/setTimePreOrder',
                        type: "POST",
                        dataType:"json",
                        data: {'date':$(this).val(),'rest':restId ,'_token':token},

                        success: function (data) {

                            $("#otherday").html(data.options);
                            $("#today").attr('style','display:none');
                            $("#otherday").attr('style','display:block');
                            //alert(data);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {

                        }
                    });



                }
                //alert($(this).val());
                //alert(todayDate);

            });
        });

    </script>


<script>
//    (function($) {
//        var foot_height = $('footer').height();
//        $.lockfixed(".SideBarFix", {
//            offset: {
//                top: 10,
//                bottom: foot_height
//            }
//        });
//        
//        var Shopping_wt = $('#Shopping').width();
//        $('.tabbing-side a').click(function(){
//            $.lockfixed(".SideBarFix", {
//                offset: {
//                    top: 10,
//                    bottom: foot_height
//                }
//            });
//            $('.SideBarFix').css('width',Shopping_wt);
//        });
//        
//    })(jQuery);

    $(document).ready(function() {
        $("input[type=checkbox]").click(function(){
            //alert($(this).attr("id"));


            var totalCartAmount= $("#cart-total-amount").val();


            if($(this).is(":checked")) {
                var id=$(this).attr("id");
                if(id=='bonus-point'){

                    $(".cashback-point").attr('style','display:none;');
                    $("#coupon-code").attr( "disabled", true );
                    $("#coupon-radio").attr( "disabled", true );

                    var actualBonusPoint=bonusPoint/bonusPointValue;
                    var newTotal=totalCartAmount-actualBonusPoint;
                    $("#cart-total-sum").html(newTotal + ' ' + '€');
                    $("#cart-total-amount").val(newTotal);

                }
                else if(id=='cashback-point'){
                    $(".bonus-point").attr('style','display:none;');
                    $("#coupon-code").attr( "disabled", true );
                    $("#coupon-radio").attr( "disabled", true );

                    var actualCashbackPoint=cashbackPoint/cashbackValue;
                    var newTotal=totalCartAmount-actualCashbackPoint;
                    $("#cart-total-sum").html(newTotal + ' ' + '€');
                    $("#cart-total-amount").val(newTotal);


                }
                else{
                    $(".cashback-point").attr('style','display:block;');
                    $(".bonus-point").attr('style','display:block;');
                    $("#coupon-radio").removeAttr('disabled');
                    $("#coupon-code").attr('disabled',false);

                    var orgTotalcartamt=$("#temp-totalcartval").val();
                    $("#cart-total-sum").html(orgTotalcartamt + ' ' + '€');
                    $("#cart-total-amount").val(orgTotalcartamt);

                }
            } else {
                $(".cashback-point").attr('style','display:block;');
                $(".bonus-point").attr('style','display:block;');
                $("#coupon-radio").removeAttr('disabled');
                $("#coupon-code").attr('disabled',false);

                var orgTotalcartamt=$("#temp-totalcartval").val();
                $("#cart-total-sum").html(orgTotalcartamt + ' ' + '€');
                $("#cart-total-amount").val(orgTotalcartamt);

            }

        });

        $("#coupon-button").click(function () {

            var couponCode = $("#coupon-code").val();
            if (couponCode == '' || couponCode == null) {
                $("#coupon-error").html('Enter valid coupon code');
            }
            else{
            var token = $("#token-value").val();
            var restId = $("#restId").val();
            var cartTotalAmount = $("#cart-total-amount").val();

            $.ajax({
                url: base_url + '/front/ajax/checkCouponCode',
                type: "POST",
                dataType: "json",
                data: {'couponCode': couponCode, 'rest': restId, '_token': token},

                success: function (data) {

                    var error = data['error'];
                    if (error) {
                        $("#coupon-error").html(error);
                    }
                    else {

                        $("#cart-total-sum").html(data['newTotal'] + ' ' + '€');
                        $("#cart-total-amount").val(data['newTotal']);
                        $("#coupon-error").html('');
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        }
        });

        $("#coupon-code").focus(function(){

            $(".cashback-point").attr('style','display:none;');
            $(".bonus-point").attr('style','display:none;');
            $("#coupon-radio").prop("checked", true); 
            $("#no-discount").attr('checked',false);
           // $("#coupon-radio + span").addClass('active');
            //$("#no-discount + span").removeClass('active');
        });

        $("#coupon-code").blur(function(){

            $(".cashback-point").attr('style','display:block;');
            $(".bonus-point").attr('style','display:block;');
            $("#coupon-radio").removeAttr('checked',true);
            $("#no-discount").attr('checked',true);
           // $("#coupon-radio + span").removeClass('active');
           // $("#no-discount + span").addClass('active');
        });
    });
</script>



@endsection