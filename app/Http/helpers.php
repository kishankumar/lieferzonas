<?php

/* Adressen */



function lie_format_address($street, $house, $staircase, $floor, $apartment) {
    $numbers = array();
    if ($house) array_push($numbers, $house);
    if ($staircase) array_push($numbers, $staircase);
    if ($floor) array_push($numbers, $floor);
    if ($apartment) array_push($numbers, $apartment);
    $number_string = "";
    for ($i = 0; $i < count($numbers); $i++) {
        if ($i > 0) {
            $number_string .= "/";
        }
        $number_string .= $numbers[$i];
    }
    return $street . " " . $number_string;
}

/* -------- */

/* Währung */

function currency_format($decimal)
{
    return "€ " . number_format($decimal, 2, ",", ".");
}

/* ------- */

/* Filter Kontroll-Elemente */

function lie_checkbox($value, $label, $checked = false)
{
    return
        "<label class='lie-checkbox-label'>" .
        "<input" . ($checked ? "checked" : "") . " type='checkbox' value='$value' class='lie-checkbox-input'>" .
        "<span class='lie-checkbox-icon'></span>" .
        " $label" .
        "</label>";
}

/* ------------------------ */

/* Bewertung */

function lie_rating_stars($value)
{
    $full_stars = floor($value);
    $output = "";
    for ($i = 0; $i < $full_stars; $i++) {
        $output .= "<i class='fa fa-star'></i>";
    }
    if ($full_stars < 5) {
        $fraction = $value - $full_stars;
        if ($fraction <= 0.25) {
            $output .= "<i class='fa fa-star-o'></i>";
        } else if ($fraction >= 0.75) {
            $output .= "<i class='fa fa-star'></i>";
        } else {
            $output .= "<i class='fa fa-star-half-o'></i>";
        }
        for ($i = 5; $i > $full_stars + 1; $i--) {
            $output .= "<i class='fa fa-star-o'></i>";
        }
    }
    return $output;
}

function lie_rating_text($value)
{
    $stars = round($value);
    return ["Mangelhaft", "Ausreichend", "Befriedigend", "Gut", "Sehr Gut"][max($stars - 1, 0)];
}

function lie_rating_format($value)
{
    return number_format($value, 1, ",", ".");
}

function lie_rating_bundle($value) {
    return lie_rating_stars($value) . " " . lie_rating_format($value);
}

function lie_rating_input($id, $variable) {
    $output = "<div id='$id'>";

    $output .= "<i class='fa fa-star-o lie-rating-input' onclick='$variable = setRating($id, 1)'></i>";
    $output .= "<i class='fa fa-star-o lie-rating-input' onclick='$variable = setRating($id, 2)'></i>";
    $output .= "<i class='fa fa-star-o lie-rating-input' onclick='$variable = setRating($id, 3)'></i>";
    $output .= "<i class='fa fa-star-o lie-rating-input' onclick='$variable = setRating($id, 4)'></i>";
    $output .= "<i class='fa fa-star-o lie-rating-input' onclick='$variable = setRating($id, 5)'></i>";

    $output .= "</div>";
}

/* --------- */

/* Stempelkarten */

function lie_stamp_bullets($stamps, $max)
{
    $output = "<nobr>";
    for ($i = 0; $i < $stamps; $i++) {
        $output .= "<i class='fa fa-circle'></i> ";
    }
    for ($i = $max; $i > $stamps; $i--) {
        $output .= "<i class='fa fa-circle-o'></i> ";
    }
    $output .= "</nobr>";
    return $output;
}

function lie_stamp_text($stamps, $max)
{
    return "<nobr>" . $stamps . " / " . $max . " Stempel gesammelt</nobr>";
}

function lie_full_stamps_text($full_stamps)
{
    return "<nobr>" . $full_stamps . " volle Stempelkarten</nobr>";
}

/* ------------- */

/* Zustellgebühren */

function lie_minimum_order_text($value)
{
    return "<nobr>Mindestbestellwert: " . currency_format($value) . "&nbsp;&nbsp;&nbsp;</nobr>";
}

function lie_below_minimum_order_text($value)
{
    return "<nobr>darunter " . currency_format($value) . " Zustellgebühr</nobr>";
}

function lie_above_minimum_order_text($value)
{
    if (!$value) return "";
    return "<nobr>Zustellgebühr: " . currency_format($value) . "</nobr>";
}

/* --------------- */

/* Zustellzeit */

function lie_average_delivery_time_text($value)
{
    if (!$value) return "";
    else if ($value < 30) {
        return "<nobr>Zustellzeit: unter 30 Minuten</nobr>";
    } else if ($value < 60) {
        return "<nobr>Zustellzeit: 30-60 Minuten</nobr>";
    } else {
        return "<nobr>Zustellzeit: über 60 Minuten</nobr>";
    }
}

/* ----------- */

/* Favoriten */

function lie_favorite_count_text($restaurant_id, $count)
{
    return "+<span class='lie-rest-favorite-count' data-rest='$restaurant_id'>$count</span>";
}

function lie_favorite_button($restaurant_id)
{
    $csrfToken = '"' . csrf_token() . '"';
    return "<i class='fa fa-heart' style='cursor: pointer' onclick='onClickToggleFavoriteButton($csrfToken,$restaurant_id)'></i>";
}

function lie_favorite_bundle($restaurant_id, $rest_info)
{
    return "<span data-rest='$restaurant_id' class='lie-rest-favorite-wrap" . ($rest_info["is_user_favorite"] ? " active" : "") . "'>" .
    lie_favorite_count_text($restaurant_id, $rest_info["rest_favorites"])
    . " " .
    lie_favorite_button($restaurant_id) .
    "</span>";
}

/* --------- */

/* Öffnungszeiten */

function lie_open_hours_text($array, $closedToday)
{
    $text = "<i class='fa fa-clock-o'></i> &nbsp; <b>";
    if ($closedToday) {
        $text .= "Heute geschlossen</b>";
        return $text;
    }
    for ($i = 0; $i < count($array); $i++) {
        if ($i > 0) $text .= ", ";
        $text .= $array[$i];
    }
    $text .= "</b>";
    return $text;
}

function lie_open_text($isOpen, $closedToday)
{
    if (!$closedToday) {
        if ($isOpen) {
            return "Geöffnet";
        } else {
            return "Geschlossen";
        }
    }
}

function lie_colored_open_text($isOpen) {
    if ($isOpen) {
        return "<span class='lie-open-color'>Geöffnet</span>";
    } else {
        return "<span class='lie-closed-color'>Geschlossen</span>";
    }
}

/* -------------- */

/* Checkboxes and Radio Buttons */

function lie_general_checkbox($name, $label, $checked = false, $id = null)
{
    $checked_string = $checked ? "checked" : "";
    $id_string = $id ? "id='$id'" : "";
    return "<div class=\"lie-checkbox\">
                    <label>
                        <input type=\"checkbox\" name=\"$name\" $checked_string $id_string>
                        <span class=\"cr\"><i class=\"cr-icon glyphicon glyphicon-ok\"></i></span>
                        $label
                    </label>
                </div>";
}

function lie_general_radio($name, $value, $label, $checked = false)
{
    $check_string = $checked ? "checked" : "";
    return "<div class=\"lie-radio\">
              <label>
                <input type=\"radio\" name=\"$name\" value=\"$value\" $check_string>
                <span class=\"cr\"><i class=\"cr-icon fa fa-circle\"></i></span>
                $label
              </label>
            </div>";
}

/* ---------------------------- */

/* Abholung/Zustellung */

function lie_pickup_switch($is_pickup_initial) {
    $checked = $is_pickup_initial ? "" : "checked";
    return "<input type=\"checkbox\" class=\"lie-pickup-toggle\"
           $checked
           data-toggle=\"toggle\"
           data-on=\"Zustellung\" data-off=\"Abholung\"
           data-onstyle=\"success\" data-offstyle=\"success\"
           data-height=\"20\">";
}

/* ------------------- */

/* PLZ Eingabe Formular */

function lie_zipcode_entry_form($size) {
    $csrf_token = csrf_token();
    $action = URL::to('/').'/search';
    return "<form action='$action' method='post' id='lie-zip-form'>
            <input type='hidden' value='$csrf_token' name='_token'>
            <nobr>
                <input name='zipcode' class='lie-zipcode-input-field' type='text' placeholder='Postleitzahl eingeben'
                /><button type='button' class='btn' id='lie-geolocation-button'><i id='lie-map-marker' class='fa fa-2x fa-crosshairs'></i></button
                ><button type='button' class='btn' id='lie-continue-button' data-csrf='$csrf_token'>WEITER</button>
            </nobr></form>";
}

/* -------------------- */

/* Tooltips */

function lie_tooltip($content) {
    return "data-toggle='tooltip' title='$content'";
}

/* -------- */

/* Checkout Helpers */



/* ---------------- */

/* Menu Helpers */

/*
6	Gluten
7	Krebstiere
8	Eier
9	Fisch
10	Erdnuesse
11	Sojabohnen
12	Milch
13	Schalenfruechte
14	Sellerie
15	Senf
16	Sesam
17	Schwefeldioxid u Sulfite
18	Lupinen
19	Weichtiere
*/

function lie_allergic_symbol($id, $size) {

    $baseUrl = URL::to('/');

    if (!in_array($id, [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19])) {
        return "";
    }
    $image_map = [
        6 => "gluten",
        7 => "shellfish",
        8 => "eggs",
        9 => "fish",
        10 => "peanuts",
        11 => "soy",
        12 => "milk",
        13 => "nuts",
        14 => "sellery",
        15 => "mustard",
        16 => "sesame",
        17 => "sulfid",
        18 => "lupin",
        19 => "molluscs"
    ];
    $image_name = $image_map[$id];
    $tooltip_map = [
        6 => "Gluten",
        7 => "Schalentiere",
        8 => "Eier",
        9 => "Fisch",
        10 => "Erdnüsse",
        11 => "Sojabohnen",
        12 => "Milch",
        13 => "Schalenfrüchte",
        14 => "Sellerie",
        15 => "Senf",
        16 => "Sesam",
        17 => "Schwefeldioxid",
        18 => "Lupine",
        19 => "Weichtiere"
    ];
    return "<img src = $baseUrl/public/front/allergene/$image_name.png height='$size' width='$size'" . lie_tooltip('Allergen: ' . $tooltip_map[$id]) . ">";
}

/* ------------ */

?>