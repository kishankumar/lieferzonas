<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use DB;
use Auth;

class FrontRedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //if ($this->auth->check()) {
        if (\Auth::check('front')) {
            DB::table('front_user_login_logs')->insert(
                ['front_user_id' =>Auth::User('front')->id,'type'=>'L','ip_address'=>$_SERVER['REMOTE_ADDR'],
                    'time'=> date("Y-m-d H:i:s"),'created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s"),
                    'created_by'=>Auth::User('front')->id,'updated_by'=> Auth::User('front')->id,'status'=>'1'
                ]
            );
//            echo 'login';exit();
            return redirect('/front/dashboard'); //add url by vikas
        }
        return $next($request);
    }
}
