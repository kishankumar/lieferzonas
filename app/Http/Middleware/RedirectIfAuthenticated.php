<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use DB;
use Auth;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            //dd(Auth::User('user'));exit();die;
            DB::table('superadmin_login_logs')->insert(
                ['user_id' =>Auth::User()->id,'type'=>'L','ip_address'=>$_SERVER['REMOTE_ADDR'],
                    'time'=> date("Y-m-d H:i:s"),'created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s"),
                    'created_by'=>Auth::User('user')->id,'updated_by'=> Auth::User('user')->id,'status'=>'1'
                ]
            );
            return redirect('/root/dashboard'); //add url by vikas
        }
        return $next($request);
    }
}
