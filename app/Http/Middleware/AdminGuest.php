<?php
 
namespace App\Http\Middleware;
 
use Closure;
use DB;
//use Illuminate\Contracts\Auth\Guard;
 
class AdminGuest
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    //protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
//    public function __construct(Guard $auth)
//    {
//        $this->auth = $auth->with('admin');
//    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check('admin')) {
            return redirect('/admin/dashboard');
        }
 
        return $next($request);
    }
}