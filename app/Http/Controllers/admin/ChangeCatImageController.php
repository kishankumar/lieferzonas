<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Validator;
use Auth;
use Input;
use App\superadmin\RestCategory;

class ChangeCatImageController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restro_detail_id = Auth::User('admin')->rest_detail_id;
        $data['getRestCategories'] = RestCategory::where('status','=',1)->where('rest_detail_id',$restro_detail_id)->select('id','name')->orderBy('name', 'asc')->get();
        return view('admin/restaurants/changeCategoryImage/changecategory')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //dd($request);
        $message=array(
            "category.required"=>"Please select Category",
            "image.required"=>"Image is required",
        );
        $this->validate($request,[
            'category'=>'required',
            'image'=>'required',
            //'image'=>'required|image|mimes:jpg, jpeg, gif, png',
        ],$message);
        
        $destinationpath=public_path()."/uploads/superadmin/category";
        $pic=Input::file('image');
        $extension=  $pic->getClientOriginalExtension(); // getting image extension
        $filename=time().rand(1111,9999).'.'.$extension; // renameing image
        $pic->move($destinationpath,$filename);
        
        $type = RestCategory::find($request->category);
        $type->image = $filename;
        $type->updated_by = Auth::User('admin')->id;
        $type->updated_at = date("Y-m-d H:i:s");
        $type->save();
        
        Session::flash('message', 'Image changed Successfully!'); 
        return redirect(route('rzone.category.image.change.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
