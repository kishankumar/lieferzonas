<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Input;
use App\superadmin\ShopItem;
use App\admin\ShopRestCartTemp;
use App\admin\ShopOrder;
use App\admin\ShopRestCartMap;
use Validator;
use Auth;

class ShopOrderController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['OrderInfo'] = ShopOrder:: where('status','=','1')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->orderBy('created_at','desc')->paginate(10);
        return view('admin.shopping.order')->with($data); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getdata(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $orderId = $data['orderId'];
            $ordertableId = $data['ordertableId'];
            $restId = $data['restId'];
        }else{
            $json['success']=0;
            $json['html']='';
            echo json_encode($json);
            return;
        }
        $table='';
        if($request) {
            //$orderId=$request->orderId;
            
            $OrderItems= DB::table('shop_rest_cart_maps')
                ->leftjoin('shop_items', 'shop_rest_cart_maps.shop_item_id', '=', 'shop_items.id')
                ->join('metrics', 'shop_items.metric_id', '=', 'metrics.id')
                ->join('rest_details', 'shop_rest_cart_maps.rest_detail_id', '=', 'rest_details.id')
                ->where('shop_rest_cart_maps.rest_detail_id','=',$restId)
                ->where('shop_rest_cart_maps.order_id','=',$orderId)
                ->orderBy('shop_rest_cart_maps.created_at','desc')
                ->select('shop_rest_cart_maps.*',
                        'shop_items.name as ItemName','shop_items.item_number as ItemNo','shop_items.front_price as PerUnitFrontPrice',
                        'metrics.name as MetricName','metrics.code as MetricCode',
                        'rest_details.f_name','rest_details.l_name','rest_details.add1','rest_details.add2','rest_details.city','rest_details.state','rest_details.country','rest_details.pincode')
                ->get();
            
            $table .= '<table id="example1" class="table table-new text-center table-c" style="margin-bottom:0">';
            $table .='<thead class="bg-danger">';
            $table .='<tr>';
            $table .='<th>Restaurant Name</th><th>Order Id</th><th>Item</th>';
            $table .='<th>Quantity</th><th>Metrics</th><th>Amount</th>';
            $table .='</tr></thead>';
            $table .='<tbody>';
            //echo '<pre>';print_r($OrderItems);die;
            if(count($OrderItems)>0){
                foreach($OrderItems as $info){
                    $table .='<tr>';
                    $table .='<td>'.ucfirst($info->f_name).' '.ucfirst($info->l_name).'</td>';
                    $table .='<td>'.($info->order_id).'</td>';
                    $table .='<td>'.ucfirst($info->ItemName).'</td>';
                    $table .='<td>'.number_format($info->quantity,2).'</td>';
                    $table .='<td>'.ucfirst($info->MetricCode).'</td>';
                    $table .='<td>&euro; '.  number_format($info->grand_total,2).'</td>';
                    $table .='</tr>';
                }
            }else{
                echo '';return;
            }
            $table .='</tbody>';
            $table .='</table>';
            echo $table;
        }else{
            echo $table;
        }
    }
    
}
