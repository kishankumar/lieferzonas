<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use validator;
use App\superadmin\RestNotification;
use App\superadmin\RestNotificationMap;


class ShowNotificationController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	public function showmore(Request $request)
	{
		
		$count=$request->count;
		$count=($count+1);
        $noti = DB::table('rest_notifications')
		->select('id','title','comment','is_read')
		->where('status','=','1')->where('send_type','=','1')->where('is_type','=','1');
		

           $notification = DB::table('rest_notifications')
		   ->leftjoin('rest_notification_maps', 'rest_notification_maps.notification_id', '=', 'rest_notifications.id')
           ->select('rest_notifications.id','rest_notifications.title','rest_notifications.comment','rest_notifications.is_read')
		   ->where('rest_notifications.status','=','1')->where('rest_notifications.send_type','=','2')
		   ->where('rest_notifications.is_type','=','1')
            ->union($noti)->limit(5)->offset($count)
            ->get();
			 for ($i = 0, $c = count($notification); $i < $c; ++$i) {
            $notification[$i] = (array) $notification[$i];
            
           } 
			$data = '';
			foreach($notification as $notifications):
			if($notifications['is_read']=='1')
			{
				$noti_nameclass =  "noti_name".$notifications['id']." text-muted";
			}
			else
			{
				$noti_nameclass =  "noti_name".$notifications['id']."";
			}
			$data.=   '<aside>
					<h4><a class="'.$noti_nameclass.'" href="javascript:void(0)" onclick="noti('.$notifications['id'].');">'.$notifications['title'].'</a></h4>
					<p class="noti_text'.$notifications['id'].'">'.$notifications['comment'].'</p>
					</aside>';
			endforeach;  

			$sendata['data']=$data;
			print_r(json_encode($sendata));
	}
    public function index()
    {
		
		$noti = DB::table('rest_notifications')
		->select('id','title','comment')
		->where('status','=','1')->where('send_type','=','1')->where('is_type','=','1');
		

        $notification = DB::table('rest_notifications')
		 ->leftjoin('rest_notification_maps', 'rest_notification_maps.notification_id', '=', 'rest_notifications.id')
           ->select('rest_notifications.id','rest_notifications.title','rest_notifications.comment')
		   ->where('rest_notifications.status','=','1')->where('rest_notifications.send_type','=','2')
		   ->where('rest_notifications.is_type','=','1')
            ->union($noti)->limit(5)->offset(0)
            ->get();
        
             
        for ($i = 0, $c = count($notification); $i < $c; ++$i) {
            $notification[$i] = (array) $notification[$i];
            
        } 
		//print_r($notification);  die;
         return view('admin.dashboard')->with('notification',$notification);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
