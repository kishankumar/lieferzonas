<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use validator;
use App\superadmin\RestDetail;
use App\superadmin\RestMobileMap;
use App\superadmin\RestEmailMap;
use App\superadmin\RestOwner;
class ProfileController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$res_owner_id = Auth::User('admin')->id;
		 $ownerinfo = DB::table('rest_owners')->select('rest_detail_id')->where('id','=',$res_owner_id)->get();
		 for ($i = 0, $c = count($ownerinfo); $i < $c; ++$i) {
             $ownerinfo[$i] = (array) $ownerinfo[$i];
             
             }
		 //print_r($ownerinfo); die;
         $info = DB::table('rest_details')
            ->leftjoin('rest_email_maps', 'rest_email_maps.rest_detail_id', '=', 'rest_details.id')
			->leftjoin('countries', 'countries.id', '=', 'rest_details.country')
			->leftjoin('states', 'states.id', '=', 'rest_details.state')
			->leftjoin('cities', 'cities.id', '=', 'rest_details.city')
			->leftjoin('zipcodes', 'zipcodes.id', '=', 'rest_details.pincode')
			->leftjoin('rest_mobile_maps', 'rest_mobile_maps.rest_detail_id', '=', 'rest_details.id')
            ->where('rest_email_maps.is_primary','=', '1')
			->where('rest_mobile_maps.is_primary','=', '1')
			->where('rest_details.id','=',$ownerinfo['0']['rest_detail_id'] )
            ->select('rest_details.id','rest_details.f_name','rest_details.l_name','rest_details.add1','rest_details.add2',
			'rest_details.city','rest_details.state','rest_details.country','zipcodes.name as pincode',
			'rest_email_maps.email','rest_mobile_maps.mobile','countries.name as country_name',
			'states.name as state_name','cities.name as city_name')
            ->paginate(10);
            for ($i = 0, $c = count($info); $i < $c; ++$i) {
             $info[$i] = (array) $info[$i];
             
             }
			
			 //print_r($info); die;
			
              return view('admin.myaccount.view-profile', ['info' => $info]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
		$info = DB::table('rest_details')
            ->leftjoin('rest_email_maps', 'rest_email_maps.rest_detail_id', '=', 'rest_details.id')
			->leftjoin('countries', 'countries.id', '=', 'rest_details.city')
			->leftjoin('states', 'states.id', '=', 'rest_details.state')
			->leftjoin('cities', 'cities.id', '=', 'rest_details.city')
			->leftjoin('rest_mobile_maps', 'rest_mobile_maps.rest_detail_id', '=', 'rest_details.id')
            ->where('rest_email_maps.is_primary','=', '1')
			->where('rest_mobile_maps.is_primary','=', '1')
			->where('rest_details.id','=',$id )
            ->select('rest_details.id','rest_details.f_name','rest_details.l_name','rest_details.add1','rest_details.add2',
			'rest_details.city','rest_details.state','rest_details.country','rest_details.pincode',
			'rest_email_maps.email','rest_email_maps.id as email_id','rest_mobile_maps.mobile','rest_mobile_maps.id as mobile_id','countries.name as country_name',
			'states.name as state_name','cities.name as city_name')->get();
			
			for ($i = 0, $c = count($info); $i < $c; ++$i) {
             $info[$i] = (array) $info[$i];
             
             }
        $country = DB::table('countries')->select('id', 'name')->orderBy('name', 'asc')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($country); $i < $c; ++$i) {
            $country[$i] = (array) $country[$i];
            
        } 

         $state = DB::table('states')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($state); $i < $c; ++$i) {
            $state[$i] = (array) $state[$i];
            
        } 
         $city = DB::table('cities')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($city); $i < $c; ++$i) {
            $city[$i] = (array) $city[$i];
            
        } 
		$zipcode = DB::table('zipcodes')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($zipcode); $i < $c; ++$i) {
            $zipcode[$i] = (array) $zipcode[$i];
            
        } 
		$auth_id = Auth::User('admin')->id;
		$username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
		for ($i = 0, $c = count($username); $i < $c; ++$i) {
            $username[$i] = (array) $username[$i];
            
        }
		 return view('admin.myaccount.edit-profile' , ['info' => $info])->with('country',$country)
		->with('state',$state)->with('city',$city)->with('zipcode',$zipcode)->with('username',$username);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->set_validation($id,$request);
        $auth_id = Auth::User('admin')->id; 
        $restdetail=RestDetail::find($id);

        $restdetail->f_name=$request->f_name;

        $restdetail->l_name=$request->l_name;
        $restdetail->pincode=$request->zipcode;
        $restdetail->country=$request->country;
		$restdetail->state=$request->state;
        $restdetail->city=$request->city;
        $restdetail->add1=$request->address1;
		$restdetail->add2=$request->address2;
		$restdetail->updated_by=$auth_id;
        $restdetail->updated_at=date("Y-m-d H:i:s");
        $restdetail->save();
		
		$email_map=RestEmailMap::find($request->email_id);
		$email_map->email = $request->email;
		$email_map->updated_by=$auth_id;
        $email_map->updated_at=date("Y-m-d H:i:s");
        $email_map->save();
		
		$mobile_map=RestMobileMap::find($request->mobile_id);
		$mobile_map->mobile = $request->mobile_no;
		$mobile_map->updated_by=$auth_id;
        $mobile_map->updated_at=date("Y-m-d H:i:s");
        $mobile_map->save();
		Session::flash('message','Updated Successfully');
		return redirect(route('rzone.profile.index'));
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function getData(Request $request)
    {
        if($request) {
            $table=$request->table;
            $mappingTable=$request->maptable;
            $cond=$request->value;
            $joinColumn=$request->joincolumn;
            $selectColumn=$request->selectcolumn;
            $whereColumn=$request->wherecolumn;
            $secondjoin=$request->secondjoincolumn;
            $selectedOption=$request->selectedOption;
            
            $values= DB::table($table)
                ->join($mappingTable, $table.'.'.$joinColumn, '=', $mappingTable.'.'.$secondjoin)
                ->select($mappingTable.'.'.'id',$mappingTable.'.'.$selectColumn)
                ->where($table.'.'.$whereColumn,$cond)
                ->get();
            
            $option='<option value="">Select</option>';
            foreach ($values as $value ) {
                $option .='<option value="'.$value->id.'" '.(($selectedOption==$value->id)?'selected':'').'>'.$value->$selectColumn.'</option>';
            }
            echo $option;
        }
    }
	 public function set_validation($id=null,$request)
     {
        $message=array(
            "f_name.required"=>"First Name is required",
            "f_name.min"=>"The First name must be at least 2 characters.",
           
			
            "email.required"=>"Email is required",
            "email.email"=>"Enter valid email",
			"mobile_no.required"=>"Mobile is required",
			"country.required"=>"Country is required",
			"state.required"=>"Adress is required",
			"city.required"=>"City is required",
            "address1.required"=>"Adress is required"
           
         );

        $this->validate($request,[
            'f_name'=> 'required|min:2|unique:rest_details,f_name,'.$id.',id',
            'email'=> 'required|Regex:/^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i',
			'mobile_no'=> 'required',
			'country'=> 'required',
			'state'=> 'required',
			'city'=> 'required',
            'address1'=> 'required'
        ],$message);
     }
	
}
