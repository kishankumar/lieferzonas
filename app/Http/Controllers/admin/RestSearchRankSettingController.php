<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Input;
use Validator;
use Auth;
use App\superadmin\RestSearchRankSetting;
use App\superadmin\Zipcode;
use App\admin\RestSearchRank;

class RestSearchRankSettingController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['restZipCodeMaps']= DB::table('rest_zipcode_delivery_maps')
            ->join('zipcodes', 'rest_zipcode_delivery_maps.zipcode_id', '=', 'zipcodes.id')
            ->select('zipcodes.name','zipcodes.id')
            ->where('rest_zipcode_delivery_maps.rest_id',Auth::User('admin')->rest_detail_id)
            ->get();
        $data['sel_prices'] = RestSearchRankSetting::where('status','1')->orderBy('id', 'asc')->get();
        $data['curr_rest_id'] = Auth::User('admin')->rest_detail_id;
        
        //previous week
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last monday midnight",$previous_week);
        $end_week = strtotime("next sunday",$start_week);
        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);
        $data['weeks']['1'] =  date('j F Y',strtotime($start_week)).' to '.date('j F Y',strtotime($end_week));
        $data['weekdate']['1']['startdate'] = $start_week;$data['weekdate']['1']['enddate'] = $end_week;
        
        //current week
        $d = strtotime("today");
        $start_week = strtotime("last monday midnight",$d);
        $end_week = strtotime("next sunday",$d);
        $start_week = date("Y-m-d",$start_week); 
        $end_week = date("Y-m-d",$end_week);  
        $data['weeks']['2'] =  date('j F Y',strtotime($start_week)).' to '.date('j F Y',strtotime($end_week));
        $data['weekdate']['2']['startdate'] = $start_week;$data['weekdate']['2']['enddate'] = $end_week;
        
        //next week
        $d = strtotime("+1 week +1 day");
        $start_week = strtotime("last monday midnight",$d);
        $end_week = strtotime("next sunday",$d);
        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);
        $data['weeks']['3'] =  date('j F Y',strtotime($start_week)).' to '.date('j F Y',strtotime($end_week));
        $data['weekdate']['3']['startdate'] = $start_week;$data['weekdate']['3']['enddate'] = $end_week;
        
        //$data['sel_ranks'] = RestSearchRank::where('status','1')->whereIn('rank', array(1,2,3,4,5,6,7,8))->get();
        $data['sel_ranks']=array();
        $week_id=$request->week;
        $data['week_id']=$request->week;
        if($week_id == 1 || $week_id == 2 || $week_id == 3){
            foreach ($data['weekdate'] as $key=>$value ) {
                if($week_id == $key){
                    $startdate = $value['startdate'];
                    $enddate = $value['enddate'];
                    $data['sel_ranks']= RestSearchRank:: where('status','=','1')
                        ->where(DB::raw('DATE_FORMAT(week_start_date,"%Y-%m-%d")'),'>=',$startdate)
                        ->where(DB::raw('DATE_FORMAT(week_end_date,"%Y-%m-%d")'),'<=',$enddate)
                        ->whereIn('rank', array(1,2,3,4,5,6,7,8))->get();
                }
            }
        }else{
            //Session::flash('message', 'Please select week again.'); 
            $data['week_id']='';
        }
        
        if($week_id == 3){
            $data['admin_count'] = RestSearchRank:: where('status','=','1')
                        ->where(DB::raw('DATE_FORMAT(week_start_date,"%Y-%m-%d")'),'>=',$startdate)
                        ->where(DB::raw('DATE_FORMAT(week_end_date,"%Y-%m-%d")'),'<=',$enddate)
                        ->whereIn('rank', array(1,2,3,4,5,6,7,8))
                        ->where('rest_id',Auth::User('admin')->rest_detail_id)->count();
        }else{
            $data['admin_count'] = 0;
        }
        //echo '<pre>';print_R($data['sel_ranks']);die;
        return view('admin.rankSetting.setting')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $rankInfo=$request->rank;
        $week_id=$request->week_id;
        //$count= RestSearchRank:: where('zipcode_id',$zipcodeId)->where('rest_id',Auth::User('admin')->rest_detail_id)->count();
        if($week_id == 3){
            if(count($rankInfo)){
                $created_at=date("Y-m-d H:i:s");$updated_at=date("Y-m-d H:i:s");
                $created_by=Auth::User('admin')->id;$updated_by=Auth::User('admin')->id;
                
                //next week
                $d = strtotime("+1 week +1 day");
                $start_week = strtotime("last monday midnight",$d);
                $end_week = strtotime("next sunday",$d);
                $start_week = date("Y-m-d",$start_week);
                $end_week = date("Y-m-d",$end_week);
                
                foreach ($rankInfo as $zipId=>$rankDetail) {
                    $rankValue = explode(",",$rankDetail);
                    $zipcodeId = $rankValue[0];
                    $rankId = $rankValue[1];
                    if($rankId==0 || $zipcodeId==0){
                        continue;
                    }
                    $type= new RestSearchRank;
                    $type->created_at=$created_at;
                    $type->created_by=$created_by;
                    
                    $PriceInfo = RestSearchRankSetting::where('status','1')->where('rank',$rankId)->first();
                    $price=0;
                    if($PriceInfo){
                        $price=$PriceInfo->price;
                    }
                    $type->week_start_date=$start_week;
                    $type->week_end_date=$end_week;
                    $type->zipcode_id=$zipcodeId;
                    $type->rest_id=Auth::User('admin')->rest_detail_id;
                    $type->rank=$rankId;
                    $type->price=$PriceInfo->price;
                    $type->updated_by=Auth::User('admin')->id;
                    $type->updated_at=date("Y-m-d H:i:s");
                    $type->status=1;
                    $type->save();
                }
            }
            Session::flash('message', 'Rank price updated successfully.');
        }else{
            Session::flash('message', 'Rank price not updated successfully.');
        }
        return redirect(route('rzone.rank.setting.index'));
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
