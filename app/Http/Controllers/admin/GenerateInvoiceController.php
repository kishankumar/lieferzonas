<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use DB;
use Input;
use Auth;
use App\admin\SuperadminServiceTaxInvoice;
class GenerateInvoiceController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin/generate_invoice');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	 public function invoice(Request $request)
     {
		$month = Input::get('month');
		$monthName = date("F", mktime(0, 0, 0, $month, 10));
		$year = Input::get('year');
		$auth_id = Auth::user('admin')->id;
		$Restowner= DB ::table('rest_owners')
		->where('id','=',$auth_id)
		->select('rest_detail_id')->get();
		for ($i = 0, $c = count($Restowner); $i < $c; ++$i) {
            $Restowner[$i] = (array) $Restowner[$i];
        }
		$rest_id = $Restowner['0']['rest_detail_id'];
		
		$rest_detail = DB ::table('rest_details')->where('status',1)->where('id',$rest_id)->select('f_name','l_name')->get();
		for ($i = 0, $c = count($rest_detail); $i < $c; ++$i) 
		{
				$rest_detail[$i] = (array) $rest_detail[$i];
		}
		$data1='';
		$data = '';
		
		$invoice =  DB :: table('super_admin_service_tax_invoices')
		->leftjoin('rest_details', 'rest_details.id', '=', 'super_admin_service_tax_invoices.rest_id')
		->where('super_admin_service_tax_invoices.rest_id',$rest_id)->where('super_admin_service_tax_invoices.status','!=',2)
		->where('super_admin_service_tax_invoices.month',$month)->where('super_admin_service_tax_invoices.year',$year)
		->select('super_admin_service_tax_invoices.*','rest_details.f_name','rest_details.l_name')->get();
	        for ($i = 0, $c = count($invoice); $i < $c; ++$i) {
				$invoice[$i] = (array) $invoice[$i];
			}
		 			  
		if(count($invoice)>0)
		{
			$rest_name = $invoice['0']['f_name'].' '.$invoice['0']['l_name'];
			$monthName = date("F", mktime(0, 0, 0, $invoice['0']['month'], 10));
			$servicetax = ((int)$invoice['0']['total_sale']*(int)$invoice['0']['service_tax'])/100;
			$data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
					 <tr><th>Restaurant</th><td>'.$rest_name.'</td></tr>
					 <tr><th>Invoice Id</th><td>'.$invoice['0']['invoice_id'].'</td></tr>
					 <tr><th>Invoice For</th><td>'.$monthName.' '.$invoice['0']['year'].'</td></tr>
					 <tr><th>Total Sale</th><td>'.$invoice['0']['total_sale'].'</td></tr>
					 <tr><th>Online Paid</th><td>'.$invoice['0']['online_sale'].'</td></tr>
					 <tr><th>Service Tax('.$invoice['0']['service_tax'].'%)</th><td>'.$servicetax.'</td></tr>
					 <tr><th>Shop Order</th><td>'.$invoice['0']['shop_orders'].'</td></tr>
					 <tr><th>Search Rank</th><td>'.$invoice['0']['search_rank'].'</td></tr>				 
					 <tr><th>Final Amount</th><td>'.$invoice['0']['final_amount'].'</td></tr><h1>bb</h1>';  
			$data1.='</table>';
			
			if($invoice['0']['final_amount']<0)
			{
				$data.= $rest_name.' will pay '.abs($invoice['0']['final_amount']).' to Superadmin!';
			}
			elseif($invoice['0']['final_amount']>0)
			{
				$data.= 'Superadmin will pay '.abs($invoice['0']['final_amount']).' to '.$rest_name.'!';
			}
			else{
				$data.= 'No dues!';
			}
			
			    $sendata['data1']=$data1;
				$sendata['data']=$data;
				$sendata['success']=1;
				echo json_encode($sendata);
				return;
		}
		else
		{
			
			$total_sale = DB :: table('order_payments')->where('status','!=','2')->where('rest_detail_id',$rest_id)->where('status','!=',2)
			->whereRaw("DATE_FORMAT(lie_order_payments.created_at, '%Y-%m') = ?", array(date(''.$year.'-'.$month.'')))
			->get();
			for ($i = 0, $c = count($total_sale); $i < $c; ++$i) {
				$total_sale[$i] = (array) $total_sale[$i];
			}
			$totalsale = 0;	
			if(count($total_sale))
			{
				foreach($total_sale as $total_sales)
				{
					$totalsale = $totalsale + (int)$total_sales['total_amount'];
				}
			}
			$online_sale = DB :: table('order_payments')->where('status','!=','2')->where('rest_detail_id',$rest_id)->where('payment_method','=','2')
			->whereRaw("DATE_FORMAT(lie_order_payments.created_at, '%Y-%m') = ?", array(date(''.$year.'-'.$month.'')))
			->get();
			for ($i = 0, $c = count($online_sale); $i < $c; ++$i) {
				$online_sale[$i] = (array) $online_sale[$i];
			}
			$onlinesale = 0;
			if(count($online_sale))
			{		
				foreach($online_sale as $online_sales)
				{
					$onlinesale = $onlinesale + (int)$online_sales['total_amount'];
				}
			}
			$service_tax = DB :: table('super_admin_settings')->where('status','!=','2')->select('service_tax')->get();
			for ($i = 0, $c = count($service_tax); $i < $c; ++$i) {
				$service_tax[$i] = (array) $service_tax[$i];
			}
			$taxpercent = $service_tax['0']['service_tax'];
			$servicetax = ((int)$totalsale*(int)$taxpercent)/100;
			
			$shop = DB :: table('shop_orders')->where('rest_detail_id',$rest_id)->where('status','!=','2')->select('grand_total')
			->whereRaw("DATE_FORMAT(lie_shop_orders.created_at, '%Y-%m') = ?", array(date(''.$year.'-'.$month.'')))->get();
			for ($i = 0, $c = count($shop); $i < $c; ++$i) {
				$shop[$i] = (array) $shop[$i];
			}
			$shop_sale = 0;	
			if(count($shop))
			{
				foreach($shop as $shops)
				{
					$shop_sale = $shop_sale + (int)$shops['grand_total'];
				}
			}
			
			$rank = DB :: table('rest_search_ranks')
			->leftjoin('rest_search_rank_settings','rest_search_ranks.rank', '=', 'rest_search_rank_settings.rank')
			->where('rest_search_ranks.rest_id',$rest_id)->where('rest_search_ranks.status','!=','2')->select('rest_search_rank_settings.price')
			->whereRaw("DATE_FORMAT(lie_rest_search_ranks.created_at, '%Y-%m') = ?", array(date(''.$year.'-'.$month.'')))->get();
			for ($i = 0, $c = count($rank); $i < $c; ++$i) {
				$rank[$i] = (array) $rank[$i];
			}
			
			$rank_sale = 0;	
			if(count($rank))
			{
			   foreach($rank as $ranks)
				{
					$rank_sale = $rank_sale + (int)$ranks['price'];
				}
			}
			$balance = ($onlinesale)-($servicetax+$shop_sale+$rank_sale);
			if($totalsale!=0 || $onlinesale!=0 || $shop_sale!=0 || $rank_sale!=0 )
			{
				/*insert data*/
				$auth_id = Auth::User()->id;
				$strid = @date("y-m-d H:i:s");
				list($idate, $time) = explode(" ", $strid);
				list($years, $months, $day) = explode("-", $idate);
				list($hour, $min, $sec) = explode(":", $time);

				$yearCal = 20;
				$newYear = $years + $yearCal;
				$monthCal = 26;
				$newmonth = $months + $monthCal;
				$hourCal = 28;
				$newHour = $hour + $hourCal;

				$int = rand(0, 10);
				$a_z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				$rand_letter = $a_z[$int];

				$InNO = $newYear . $newmonth . $day . $newHour;
				$InCH = $rand_letter;

				$InvoiceId = "Inv" . $InNO . "" . $InCH ;
				
				$insert_invoice = new SuperadminServiceTaxInvoice;
				$insert_invoice->invoice_id=$InvoiceId;
				$insert_invoice->rest_id=$rest_id;
				$insert_invoice->total_sale=$totalsale;
				$insert_invoice->online_sale=$onlinesale;
				$insert_invoice->service_tax=$taxpercent;
				$insert_invoice->shop_orders=$shop_sale;
				$insert_invoice->search_rank=$rank_sale;
				$insert_invoice->final_amount=$balance;
				$insert_invoice->month=$month;
				$insert_invoice->year=$year;
				$insert_invoice->status=1;
				$insert_invoice->created_by=$auth_id;
				$insert_invoice->created_at=date("Y-m-d H:i:s");
				$insert_invoice->save();
				/*insert data*/
				$res_name = $rest_detail['0']['f_name'].' '.$rest_detail['0']['l_name'];
				$data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
						 <tr><th>Restaurant</th><td>'.$res_name.'</td></tr>
					     <tr><th>Invoice Id</th><td>'.$InvoiceId.'</td></tr>
					     <tr><th>Invoice For</th><td>'.$monthName.' '.$year.'</td></tr>
						 <tr><th>Total Sale</th><td>'.$totalsale.'</td></tr>
						 <tr><th>Online Paid</th><td>'.$onlinesale.'</td></tr>
						 <tr><th>Service Tax('.$taxpercent.'%)</th><td>'.$servicetax.'</td></tr>
						 <tr><th>Shop Order</th><td>'.$shop_sale.'</td></tr>
						 <tr><th>Search Rank</th><td>'.$rank_sale.'</td></tr>				 
						 <tr><th>Final Amount</th><td>'.$balance.'</td></tr><h1>bb</h1>';  
				$data1.='</table>';
				
				if($balance<0)
				{
					$data.= $res_name.' will pay '.abs($balance).' to Superadmin!';
				}
				elseif($balance>0)
				{
					$data.= 'Superadmin will pay '.abs($balance).' to '.$res_name.'!';
				}
				else{
					$data.= 'No dues!';
				}
				$sendata['data1']=$data1;
				$sendata['data']=$data;
				$sendata['success']=1;
				echo json_encode($sendata);
				return;
			
			}
			else
			{
				$sendata['success']=0;
				echo json_encode($sendata);
				return;
			}	
		}
		
        
     }
}
