<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\DefaultController;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Validator;
use App\superadmin\RestOwner;
use Mail;

class ForgotPasswordController extends DefaultController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $adminuser = RestOwner::where('activation_key',$request->act_key)->get();
        $adminuser =$adminuser->toArray();
		//print_r($frontuser); die;
		$count = count($adminuser); 
		
		if($count>0)
		{
			$activation_key = '';
			$id = $adminuser['0']['id'];
			$auser=RestOwner::find($id);
			$auser->password = bcrypt($request->conpassword);
            $auser->activation_key = $activation_key;			
			$auser->save();
			Session::flash('message','Password reset successfully.');
            return redirect("resetpasssuccess");
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function sendforgotpassmail()
	{
	  
	  
	   /* mail */
		
		$uemail = Input::get('email');
	    $umobile = Input::get('mobile');
		$ucustomerid = Input::get('customerid');
		if($uemail!='')
		{
			$adminuser = RestOwner::where('email',$uemail)->get();
            $adminuser =$adminuser->toArray();
		}
		if($uemail=='' && $umobile!='')
		{
			$adminuser = RestOwner::where('contact_no',$umobile)->get();
            $adminuser =$adminuser->toArray();
		}
		if($uemail=='' && $umobile=='' && $ucustomerid!='')
		{
			$adminuser = RestOwner::where('id',$ucustomerid)->get();
            $adminuser =$adminuser->toArray();
		}
		
		$count = count($adminuser); 
		$activation_key = base64_encode($uemail);
		
		$random_var = rand(1, 100);
	
		$act_key = $activation_key.$random_var;
		if($count>0)
		{
			$email = $adminuser['0']['email'];
			$id = $adminuser['0']['id'];
			$auser=RestOwner::find($id);
			$auser->activation_key = $act_key; 
			$auser->save();
			$from_email = 'vikas.alivenetsolutions@gmail.com';
			$from_name = 'Lieferzonas Superadmin';
			$base_host = $_SERVER['SERVER_NAME'];
			$link = $base_host.'/lieferzonas/resetpass/'.$act_key;
			//$link = "http://localhost/lieferzonas/front/resetpassword/".$act_key."";
			$data = array(
				
				'uemail' => $email,
				'link' => $link
			);
			
			$pageuser = 'admin.emails.forgotpass.touser';
			$subject = 'Forgot Password';
			$this->sendusermail($pageuser,$data,$subject,$from_email,$from_name,$email);
			
			
			 /* mail */
			//Session::flash('message', 'Successfully!'); 
			$json['success']=1;
			echo json_encode($json);
			return;
		}
		else{
			$json['success']=0;
			echo json_encode($json);
			return;
			
		}
		
		
	}
	
	public function sendusermail($pageuser,$data,$subject,$from_email,$from_name,$uemail)
	{
		Mail::send($pageuser, $data, function($messa) use ($subject,$from_email,$from_name,$uemail)  {
            $messa->from($from_email, $from_name);
            $messa->to($uemail)->subject($subject);
           
        });
	}
	public function resetpass($act_key=0)
    {
		$adminuser = RestOwner::where('activation_key',$act_key)->get();
        $adminuser =$adminuser->toArray();
		//print_r($frontuser); die;
		$count = count($adminuser); 
		if($count>0)
		{
			return view('admin/myaccount/reset_password')->with('act_key',$act_key);
		}
		else
		{
			 return redirect("invalidpassurl");
		}
		
	}
	public function Invalidurl()
	{
		return view('admin/myaccount/password_reseted');
	}
    public function successresetmsg()
	{
		return view('admin/myaccount/success_resetpassmsg');
	}
}
