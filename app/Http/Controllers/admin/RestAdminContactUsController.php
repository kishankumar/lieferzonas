<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Input;
use App\superadmin\EmailTemplate;
use App\admin\RestAdminContactUs;
use Validator;
use Auth;
use Mail;
//use Illuminate\Support\Facades\Mail;

class RestAdminContactUsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/contactus/contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $message=array(
            "topic.required"=>"Topic is required",
            "topic.min"=>"Topic must be at least 3 characters.",
            "subject.required"=>"Subject is required",
            "subject.min"=>"Subject must be at least 3 characters.",
            "email.required"=>"Email is required",
            "email.email"=>"Enter valid email",
            "message.required"=>"Message is required",
            "message.min"=>"Message must be at least 6 characters.",
            'phone.required'=>'Phone is required',
            'phone.min'=>'Phone must be at least 10 numbers.',
            'phone.max'=>'Phone must be maximum 12 numbers.',
            "image.required"=>"Image is required",
        );
        
        $this->validate($request,[
            'topic'=>'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            'subject'=>'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            'email'=>'required|Regex:/^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i',
            'message'=>'required|min:6',
            'phone'=>'required|min:10|max:12|Regex: /^[0-9]{1,45}$/',
            'image'=>'required',
            //'image'=>'required|image|mimes:jpg, jpeg, gif, png',
        ],$message);
        
        $destinationpath=public_path()."\admin\uploads\customercontact";
        $pic=Input::file('image');
        $extension=  $pic->getClientOriginalExtension(); // getting image extension
        $filename=time().rand(1111,9999).'.'.$extension; // renameing image
        $pic->move($destinationpath,$filename);
        
        $CustomerContact= new RestAdminContactUs;
        $CustomerContact->topic=$request->topic;
        $CustomerContact->subject=$request->subject;
        $CustomerContact->message=$request->message;
        $CustomerContact->email=$request->email;
        $CustomerContact->phone=$request->phone;
        $CustomerContact->image=$filename;
        $CustomerContact->created_by=Auth::User('admin')->id;
        $CustomerContact->updated_by=Auth::User('admin')->id;
        $CustomerContact->created_at=date("Y-m-d H:i:s");
        $CustomerContact->updated_at=date("Y-m-d H:i:s");
        $CustomerContact->status=1;
        $CustomerContact->save();
        
        $pathToFile=$_SERVER['DOCUMENT_ROOT'].'/lieferzonas/public/admin/uploads/customercontact/'.$filename;
        $topic = $request->topic;
        $subject = $request->subject;
        $email = $request->email;
        $messagess = $request->message;
        $phone = $request->phone;
        $from_email = 'vikas.alivenetsolutions@gmail.com';
        $from_name = 'Customer Service';
        $to_admin = 'rajlakshmi017@gmail.com';
        $data = array(
            'topic' => $topic,
            'subject' => $subject,
            'email' => $email,
            'description' => $messagess,
            'phone' => $phone,
            'pathToFile' => $pathToFile,
            'from_email' => $from_email,
            'from_name' => $from_name,
            'to_admin' => $to_admin
        );
        $page = 'admin.emails.welcome';
        //$this->sendmail($page,$data,$pathToFile,$subject,$from_email,$from_name,$to_admin);
        //$this->sendmail($page,$data,$pathToFile,$subject,$from_email,$from_name,$email);
        
        Session::flash('message', 'Service Added Successfully!');
        return redirect(route('rzone.contactus.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function sendmail($page,$data,$pathToFile,$subject,$from_email,$from_name,$to_admin)
    {
        Mail::send($page, $data, function($messa) use ($pathToFile,$subject,$from_email,$from_name,$to_admin)  {
            $messa->from($from_email, $from_name);
            $messa->to($to_admin)->subject($subject);
            $messa->attach($pathToFile);
        });
    }
}
