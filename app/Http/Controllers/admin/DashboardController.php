<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Auth;
//use Illuminate\Support\Facades\Input;
use Session;
use DB;
use validator;
use Input;
use App\superadmin\RestNotification;
use App\superadmin\RestNotificationMap;
use App\superadmin\RestDetail;
use App\superadmin\RestDayTimeMap;
use App\superadmin\RestSetting;

class DashboardController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		$res_owner_id = Auth::User('admin')->id;
		$ownerinfo = DB::table('rest_owners')->select('rest_detail_id')->where('id','=',$res_owner_id)->get();
		for ($i = 0, $c = count($ownerinfo); $i < $c; ++$i) {
             $ownerinfo[$i] = (array) $ownerinfo[$i];
             
             }
		$rest_id = $ownerinfo['0']['rest_detail_id'];
        $noti = DB::table('rest_notifications')

        ->select('id','title','comment','is_read')
        ->where('status','=','1')->where('send_type','=','1')->where('is_type','=','1');

        $notification = DB::table('rest_notifications')

         ->leftjoin('rest_notification_maps', 'rest_notification_maps.notification_id', '=', 'rest_notifications.id')
           ->select('rest_notifications.id','rest_notifications.title','rest_notifications.comment','rest_notifications.is_read')
           ->where('rest_notifications.status','=','1')->where('rest_notifications.send_type','=','2')
           ->where('rest_notifications.is_type','=','1')
           ->where('rest_notification_maps.rest_detail_id','=',$rest_id)
		    ->where('rest_notification_maps.status','!=',2)
            ->union($noti)->limit(5)->offset(0)
            ->get();
        //echo'<pre>';print_r($notification);echo'</pre>';
        $noti_count = DB::table('rest_notifications')

         ->leftjoin('rest_notification_maps', 'rest_notification_maps.notification_id', '=', 'rest_notifications.id')
           ->select('rest_notifications.id','rest_notifications.title','rest_notifications.comment','rest_notifications.is_read')
           ->where('rest_notifications.status','=','1')->where('rest_notifications.send_type','=','2')
           ->where('rest_notifications.is_type','=','1')
           ->where('rest_notification_maps.rest_detail_id','=',$rest_id)
		   ->where('rest_notification_maps.status','!=',2)
            ->union($noti)
            ->get();     
        $noticount = count($noti_count); 
        for ($i = 0, $c = count($notification); $i < $c; ++$i) {
            $notification[$i] = (array) $notification[$i];
        }
        
        return view('admin.dashboard')->with('notification',$notification)->with('noticount',$noticount);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function openStatusChange(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $is_open = $data['is_open'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        if($is_open == 'close'){
            $is_open = 0;
        }else{
            $is_open = 1;
        }
        $restro_detail_id = Auth::User('admin')->rest_detail_id;
        $type = RestSetting::where('rest_detail_id',$restro_detail_id)->where('status',1)->first();
        $type->is_open=$is_open;
        $type->updated_by=Auth::User('admin')->id;
        $type->updated_at=date("Y-m-d H:i:s");
        $type->save();
        $json['success']=1;
        echo json_encode($json);
        return;
    }

	public function readstatus()
	{
		$id = Input::get('noti_id');
		$restnotification=RestNotification::find($id);
		$restnotification->is_read=1;
		$restnotification->save();
		$json['success']=1;
        echo json_encode($json);
        return ;
	}

}
