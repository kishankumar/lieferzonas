<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Input;
use App\superadmin\RestDetail;
use App\superadmin\RestMobileMap;
use App\superadmin\ShopItem;
use App\superadmin\ShopInventory;
use App\admin\ShopRestCartTemp;
use App\admin\ShopOrder;
use App\admin\ShopRestCartMap;
use App\admin\ShopTransaction;
use Validator;
use Auth;

class ShopCartController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Auth::User('admin'));exit();
        $data['ItemInfo']= ShopItem:: where('status', '=','1')->orderBy('name', 'desc')->get();
        foreach($data['ItemInfo'] as $price){
            $data['item_price'][$price->id] = $price->front_price;
        }
        foreach($data['ItemInfo'] as $quan){
            $data['item_quantity'][$quan->id] = $quan->quantity;
        }
        //echo '<pre>';print_r($data['item_quantity']);die;
        $data['CartInfo']= ShopRestCartTemp:: where('status', '=','1')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->orderBy('created_at', 'desc')->get();
        return view('admin.shopping.shopcart')->with($data);
    }
    
    function getItemPrice(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $itemId = $data['itemId'];
            $quantity = $data['quantity'];
        }else{
            $json['success']=0;
            $json['html']='';
            echo json_encode($json);
            return;
        }
        $shop_detail = DB::table('shop_items')->where('id',$itemId)->get();
        foreach($shop_detail as $info){
            $front_price = $info->front_price;
            $current_quantity = $info->quantity;
            $ItemName = $info->name;
        }
        if($quantity>$current_quantity){
            $json['success']=0;
            $json['totalAmount']=$front_price;
            $json['ItemName']=  ucfirst($ItemName);
            $json['message']='Quantity should be less than current quantity.';
            echo json_encode($json);
            return;
        }else{
            $json['success']=1;
            $json['message']='valid quantity';
            //$json['totalAmount']=round(($front_price*$quantity),2);
            $json['totalAmount']=$front_price;
            $json['ItemName']=ucfirst($ItemName);
            echo json_encode($json);
            return;
        }
    }
    
    public function removeCartItems(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $restId = Auth::User('admin')->rest_detail_id;
            $cartItems = $data['cartItems'];
        }else{
            $json['success']=0;
            $json['cart_status']='Cart is empty';
            echo json_encode($json);
            return;
        }
        $cartItems= explode(',', $cartItems);
        if(count($cartItems)>0){
            foreach($cartItems as $item):
                $deleted= DB::table('shop_rest_cart_temps')->where('rest_detail_id',$restId)->where('shop_item_id',$item)->delete();
            endforeach;
        }
        if($deleted){
            $json['success']=1;
            $json['cart_status']='Cart is empty';
            echo json_encode($json);
            return;
        }else{
            $json['success']=0;
            $json['cart_status']='Cart is not empty';
            echo json_encode($json);
            return;
        }
    }
    
    public function updateCartItems(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $restId = Auth::User('admin')->rest_detail_id;
            $ItemId = $data['ItemId'];
            $quantity = $data['quantity'];
            $action = $data['action'];
        }else{
            $json['success']=0;
            $json['cart_status']='Cart is empty';
            echo json_encode($json);
            return;
        }
        if($action=='edit' || $action=='plus' || $action=='minus'  || $action=='addCart'){
            $count = DB:: table('shop_rest_cart_temps')->where('rest_detail_id',$restId)->where('shop_item_id',$ItemId)->count();
            if($count>0){
                $content = ShopRestCartTemp::where('rest_detail_id','=',$restId)->where('shop_item_id',$ItemId)->first();
            }else{
                $content=new ShopRestCartTemp;
                $content->created_at=date("Y-m-d H:i:s");
                $content->created_by=Auth::User('admin')->id;
            }
            $content->rest_detail_id=$restId;
            $content->shop_item_id=$ItemId;
            $content->quantity=$quantity;
            $content->updated_by=Auth::User('admin')->id;
            $content->updated_at=date("Y-m-d H:i:s");
            $content->status=1;
            $content->save();
            
            $shop_detail = DB::table('shop_items')->where('id',$ItemId)->get();
            $PerItemAmount=0;
            $json['PerItemAmount'] = $PerItemAmount;
            foreach($shop_detail as $info){
                $front_price = $info->front_price;
                $PerItemAmount = round(($front_price*$quantity),2);
                $json['PerItemAmount'] = $PerItemAmount;
            }
        }else if($action=='Remove'){
            $PerItemAmount=0;
            $json['PerItemAmount'] = $PerItemAmount;
            $deleted= DB::table('shop_rest_cart_temps')->where('rest_detail_id',$restId)->where('shop_item_id',$ItemId)->delete();
        }else{
            $json['success']=0;
            $json['cart_status']='Cart is empty';
            echo json_encode($json);
            return;
        }
        
        $data['ItemInfo']= ShopItem:: where('status', '=','1')->orderBy('name', 'desc')->get();
        $item_price=array();$item_quantity=array();
        foreach($data['ItemInfo'] as $price){
            $item_price[$price->id] = $price->front_price;
        }
        foreach($data['ItemInfo'] as $quan){
            $item_quantity[$quan->id] = $quan->quantity;
        }
        $json['totalAmount']=0;$totalAmount=0;
        $CartInfo = ShopRestCartTemp:: where('status', '=','1')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->orderBy('created_at', 'desc')->get();
        foreach($CartInfo as $CartItems){
            $totalAmount+= round((($item_price[$CartItems->shop_item_id])*$CartItems->quantity),2);
        }
        $html='';
        $html .= '<div class="check_table">';
            if(count($CartInfo)){
                $per_item_price=0;$total_price=0;
                foreach($CartInfo as $Cinfo){
                    $per_item_price=round((($item_price[$Cinfo->shop_item_id])*$Cinfo->quantity),2);
                    $total_price=$total_price+$per_item_price;
                    $html .='<div class="item_list shop_items ItemNo'.$Cinfo->shop_item_id.'">';
                    $html .='<div class="left">';
                    $html .='<span>';
                    $html .='<input id="itemId-'.$Cinfo->shop_item_id.'" class="cart_items form-control t_num" type="text" value="'.$Cinfo->quantity.'" name="items['.$Cinfo->shop_item_id.']" onchange="change_price_cart('.$Cinfo->shop_item_id.','.$Cinfo->quantity.')" readonly="readonly">';
                    $html .='</span>';
                    //<span>{!! Form :: text('items[]',$Cinfo->quantity,["id"=>"itemId-$Cinfo->shop_item_id",'readonly'=>'readonly','class'=>'cart_items form-control t_num','onchange'=>"change_price_cart($Cinfo->shop_item_id,$Cinfo->quantity)"])  !!}</span>
                    $html .= ucfirst($Cinfo->item_name->name);
                    $html .='</div>';
                    $html .='<aside>';
                    $html .='<a href="javascript:void(0)" onclick="edit_item_in_cart(\''.$Cinfo->shop_item_id.'\')"><i class="fa fa-pencil"></i> </a>';
                    $html .='<a href="javascript:void(0)" onclick="plus_item_in_cart_items(\''.$Cinfo->shop_item_id.'\')"><i class="fa fa-plus"></i> </a>';
                    $html .='<a href="javascript:void(0)" onclick="minus_item_in_cart_items(\''.$Cinfo->shop_item_id.'\')"><i class="fa fa-minus"></i></a>';
                    $html .='<a href="javascript:void(0)" onclick="remove_item_from_cart(\''.$Cinfo->shop_item_id.'\')"><i class="fa fa-trash"></i></a>';
                    $html .='<br>';
                    $html .='<strong id="price-'.$Cinfo->shop_item_id.'">'.number_format($per_item_price,2).' €</strong>';
                    $html .='</aside>';
                    $html .='<span style="color:red" id="span'.$Cinfo->shop_item_id.'"></span>';
                    $html .='</div>';
                }
            }
            else{
                $html .='<div class="item_list">No item Exist in cart.</div>';
            }
        $html .='</div>';
        
        if(count($CartInfo)){
            //$html .='<div class="item_list"><div class="left">Order:  </div><aside id="order-price">';
            //$html .=number_format($total_price,2).' &euro;';
            //$html .='</aside>';
            //$html .='<div class="left">Minimum value:  </div>';
            //$html .='<aside id="minimum-price">';
            //$html .=number_format($total_price,2).' &euro;';
            //$html .='</aside></div>';
            //$html .='<div class="item_list">';
            //$html .='<div class="left">For minimum orders missing:  </div>';
            //$html .='<aside  id="min-order-price">';
            //$html .=number_format($total_price,2).' &euro;'; 
            //$html .='</aside>';
            //$html .='<div class="left">Free Shipping: </div>';
            //$html .='<aside id="free-shipping-price">';
            //$html .=number_format($total_price,2).' &euro;';
            //$html .='</aside></div>';
            $html .='<div class="action_menu">';
            $html .='<h4><strong>Total Amount : <bdo class="pull-right" id="total-cart-price">'.number_format($total_price,2).'&euro;</bdo></strong></h4>';
            $html .='</div>';
            $html .='<div class="item_list" style="border:none;">';
            $html .='<a href="javascript:void(0)" onclick="cart_check_out()" class="btn btn-success btn-block">Proceed to Checkout</a>';
            $html .='</div>';
            $html .='<div class="clearfix"></div>';
        }
        $json['html'] = $html;
        //echo $json['html']=View::make('admin.shopping.shopcart',array('item_price'=>$item_price,'item_quantity'=>$item_quantity,'CartInfo'=>$CartInfo));
        $json['totalAmount']=$totalAmount;
        $json['success']=1;
        $json['cart_status']='Cart is updated';
        echo json_encode($json);
        return;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        $message=array(
//            "f_name.required"=>"First Name is required",
//            "f_name.min"=>"First name must be at least 2 characters.",
//            "f_name.alpha"=>"First name must be alphabet",
//            "l_name.required"=>"Last Name is required",
//            "l_name.min"=>"Last name must be at least 2 characters.",
//            "l_name.alpha"=>"Last name must be alphabet",
//            "mobile_no.required"=>"Mobile is required",
//            "country.required"=>"Country is required",
//            "state.required"=>"Adress is required",
//            "city.required"=>"City is required",
//            "address1.required"=>"Adress is required"
//        );
//        $this->validate($request,[
//            'f_name'=> 'required|min:2|alpha',
//            'l_name'=> 'required|min:2|alpha',
//            'mobile_no'=> 'required',
//            'country'=> 'required',
//            'state'=> 'required',
//            'city'=> 'required',
//            'address1'=> 'required'
//        ],$message);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function showAddress(Request $request){
        $country = DB::table('countries')->select('id', 'name')->where('status','=','1')->get();
        for ($i = 0, $c = count($country); $i < $c; ++$i) {
            $country[$i] = (array) $country[$i];
        }
        $data['country'] =$country;
        return view('admin.shopping.order_address')->with($data);
    }
    public function store(Request $request)
    {
        $use_old_address=0;
        if($request->use_old_address){
            $use_old_address = $request->use_old_address;
        }
        if($use_old_address){
            $mobileNo='';
            $mobileMap = RestMobileMap::where('rest_detail_id',Auth::User('admin')->rest_detail_id)->where('is_primary','1')->where('status',1)->first();
            if(count($mobileMap)){
                $mobileNo = $mobileMap->mobile;
            }
            $restDetail = RestDetail:: where('id',Auth::User('admin')->rest_detail_id)->first();
            $object = new ShopTransaction;
            $object->rest_id=Auth::User('admin')->rest_detail_id;
            $object->f_name=  $restDetail->f_name;
            $object->l_name=  $restDetail->l_name;
            $object->mobile_no=  $mobileNo;
            $object->add1=  $restDetail->add1;
            $object->add2=  $restDetail->add2;
            $object->city_id=  $restDetail->city;
            $object->state_id=  $restDetail->state;
            $object->country_id=  $restDetail->country;
            $object->created_by=Auth::User('admin')->id;
            $object->updated_by=Auth::User('admin')->id;
            $object->created_at=date("Y-m-d H:i:s");
            $object->updated_at=date("Y-m-d H:i:s");
            $object->status=1;
            $object->save();
            $maxid = $object->id;
        }else{
            $object = new ShopTransaction;
            $object->rest_id=Auth::User('admin')->rest_detail_id;
            $object->f_name=  $request->f_name;
            $object->l_name=  $request->l_name;
            $object->mobile_no=  $request->mobile_no;
            $object->add1=  $request->add1;
            $object->add2=  $request->add2;
            $object->city_id=  $request->city;
            $object->state_id=  $request->state;
            $object->country_id=  $request->country;
            $object->created_by=Auth::User('admin')->id;
            $object->updated_by=Auth::User('admin')->id;
            $object->created_at=date("Y-m-d H:i:s");
            $object->updated_at=date("Y-m-d H:i:s");
            $object->status=1;
            $object->save();
            $maxid = $object->id;
        }
        
        $data['ItemInfo']= ShopItem:: where('status', '=','1')->orderBy('name', 'desc')->get();
        $item_price=array();$item_quantity=array();
        foreach($data['ItemInfo'] as $price){
            $item_price[$price->id] = $price->front_price;
        }
        foreach($data['ItemInfo'] as $quan){
            $item_quantity[$quan->id] = $quan->quantity;
        }
        $totalAmount=0;$total_price=0;
        $CartInfo = ShopRestCartTemp:: where('status', '=','1')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->orderBy('created_at', 'desc')->get();
        if(count($CartInfo)){
            $per_item_price=0;$total_price=0;$discount=0;$grand_total=0;
            foreach($CartInfo as $Cinfo){
                $per_item_price=round((($item_price[$Cinfo->shop_item_id])*$Cinfo->quantity),2);
                $total_price=$total_price+$per_item_price;
            }
        }
        $data['paymentAmount'] = $total_price*100;
        $data['last_inserted_id'] = $maxid;
        if($total_price)
            return view('admin.shopping.adyenform')->with($data);
        else
            return redirect(route('rzone.shop.orders.index'));
    }
    
    public function payment(Request $request){
        //echo '<pre>';print_r($_REQUEST);die;
        $authResult = $request->authResult;
        $paymentMethod = $request->paymentMethod;
        $pspReference = $request->pspReference;
        $shopperLocale = $request->shopperLocale;
        $skinCode = $request->skinCode;
        $merchantReference = $request->merchantReference;
        $referenceArray = explode("-",$merchantReference);
        $ShopTransactionId = $referenceArray[1];
        
        if($authResult == 'AUTHORISED'){
            $data['ItemInfo']= ShopItem:: where('status', '=','1')->orderBy('name', 'desc')->get();
            $item_price=array();$item_quantity=array();
            foreach($data['ItemInfo'] as $price){
                $item_price[$price->id] = $price->front_price;
            }
            foreach($data['ItemInfo'] as $quan){
                $item_quantity[$quan->id] = $quan->quantity;
            }
            //echo '<pre>';print_r($item_price);die;
            $json['totalAmount']=0;$totalAmount=0;
            $CartInfo = ShopRestCartTemp:: where('status', '=','1')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->orderBy('created_at', 'desc')->get();
            if(count($CartInfo)){
                $per_item_price=0;$total_price=0;$discount=0;$grand_total=0;
                foreach($CartInfo as $Cinfo){
                    $per_item_price=($item_price[$Cinfo->shop_item_id])*$Cinfo->quantity;
                    $total_price=$total_price+$per_item_price;
                }
                $grand_total=$total_price;
                
                $maxid = DB :: table('shop_orders')->max('id');
                if($maxid == 0 || $maxid<1){
                    $maxid=0;
                }
                $order_id  = 'Order-'.($maxid + 1);
                $orderTotal = number_format($grand_total,2);
                
                $object = new ShopOrder;
                $object->order_id=$order_id;
                $object->rest_detail_id=Auth::User('admin')->rest_detail_id;
                $object->total_price=  number_format($total_price,2, '.', '');
                $object->discount=number_format($discount,2, '.', '');
                $object->grand_total=number_format($grand_total,2, '.', '');
                $object->created_by=Auth::User('admin')->id;
                $object->updated_by=Auth::User('admin')->id;
                $object->created_at=date("Y-m-d H:i:s");
                $object->updated_at=date("Y-m-d H:i:s");
                $object->status=1;
                $object->save();
                
                $content = ShopTransaction::where('id',$ShopTransactionId)->where('rest_id',Auth::User('admin')->rest_detail_id)->first();
                $content->shop_order_id=$order_id;
                $content->transaction_id=$pspReference;
                $content->updated_by=Auth::User('admin')->id;
                $content->updated_at=date("Y-m-d H:i:s");
                $content->transaction_date=date("Y-m-d H:i:s");
                $content->save();
                
                foreach($CartInfo as $Cinfos){
                    $per_item_price=0;$total_price=0;$discount=0;$grand_total=0;
                    $per_item_price=($item_price[$Cinfos->shop_item_id])*($Cinfos->quantity);
                    $total_price=$total_price+$per_item_price;
                    $grand_total=$total_price;
                    
                    $object1 = new ShopRestCartMap;
                    $object1->order_id=$order_id;
                    $object1->rest_detail_id=Auth::User('admin')->rest_detail_id;
                    $object1->shop_item_id=$Cinfos->shop_item_id;
                    $object1->quantity=$Cinfos->quantity;
                    $object1->price=  number_format($total_price,2, '.', '');
                    $object1->discount=number_format($discount,2, '.', '');
                    $object1->grand_total=number_format($grand_total,2, '.', '');
                    $object1->created_by=Auth::User('admin')->id;
                    $object1->updated_by=Auth::User('admin')->id;
                    $object1->created_at=date("Y-m-d H:i:s");
                    $object1->updated_at=date("Y-m-d H:i:s");
                    $object1->status=1;
                    $object1->save();
                    
                    $shop_detail = DB::table('shop_items')->where('id',$Cinfos->shop_item_id)->get();
                    foreach($shop_detail as $info){
                        $quantity = $info->quantity;
                        $price = $info->front_price;
                        $category_id = $info->shop_category_id;
                        $MtricId = $info->metric_id;    
                    }
                    $content = ShopItem::where('id',$Cinfos->shop_item_id)->first();
                    $content->quantity=$quantity-($Cinfos->quantity);
                    $content->save();
                    
                    $TotalPrice = ($price)*($Cinfos->quantity);
                    
                    $object2 = new ShopInventory;
                    $object2->shop_category_id=$category_id;
                    $object2->shop_item_id=$Cinfos->shop_item_id;
                    $object2->metric_id=$MtricId;
                    $object2->quantity=number_format($Cinfos->quantity,2, '.', '');
                    $object2->price=number_format($TotalPrice,2, '.', '');
                    $object2->action='M';
                    $object2->created_by=Auth::User('admin')->id;
                    $object2->updated_by=Auth::User('admin')->id;
                    $object2->created_at=date("Y-m-d H:i:s");
                    $object2->updated_at=date("Y-m-d H:i:s");
                    $object2->status=1;
                    $object2->save();
                    $deleted= DB::table('shop_rest_cart_temps')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->where('shop_item_id',$Cinfos->shop_item_id)->delete();
                }
                $data['payment_status'] =  'AUTHORISED';
                $data['order_id'] =  $order_id;
                $data['grand_total'] =  $orderTotal;
                $data['pspReference'] =  $pspReference;
            }else{
                $data['payment_status'] =  'ERROR';
            }
            return view('admin.shopping.order_payment_status')->with($data);
        }else if($authResult == 'REFUSED'){
            $data['payment_status'] =  'REFUSED';
            $payment_status =  'REFUSED';
            return view('admin.shopping.order_payment_status')->with($data);
        }else if($authResult == 'CANCELLED'){
            return redirect(route('rzone.shop.cart.index'));
            //$data['payment_status'] =  'CANCELLED';
            //return view('admin.shopping.order_payment_status')->with($data);
        }else if($authResult == 'PENDING'){
            $data['payment_status'] =  'PENDING';
            return view('admin.shopping.order_payment_status')->with($data);
        }else if($authResult == 'ERROR'){
            $data['payment_status'] =  'ERROR';
            return view('admin.shopping.order_payment_status')->with($data);
        }else{
            return redirect(route('rzone.shop.orders.index'));
        }
    }
    
    public function store1(Request $request)
    {
        //dd($request);die;
        $data['ItemInfo']= ShopItem:: where('status', '=','1')->orderBy('name', 'desc')->get();
        $item_price=array();$item_quantity=array();
        foreach($data['ItemInfo'] as $price){
            $item_price[$price->id] = $price->front_price;
        }
        foreach($data['ItemInfo'] as $quan){
            $item_quantity[$quan->id] = $quan->quantity;
        }
        $json['totalAmount']=0;$totalAmount=0;
        $CartInfo = ShopRestCartTemp:: where('status', '=','1')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->orderBy('created_at', 'desc')->get();
        //echo '<pre>';print_r($CartInfo);die;
        if(count($CartInfo)){
            $per_item_price=0;$total_price=0;$discount=0;$grand_total=0;
            foreach($CartInfo as $Cinfo){
                $per_item_price=round((($item_price[$Cinfo->shop_item_id])*$Cinfo->quantity),2);
                $total_price=$total_price+$per_item_price;
            }
            $total_price= round($total_price,2);
            $grand_total=$total_price;
            
            $maxid=DB :: table('shop_orders')->max('id');
            $order_id  = 'Order-'.($maxid + 1);
            
            $object = new ShopOrder;
            $object->order_id=$order_id;
            $object->rest_detail_id=Auth::User('admin')->rest_detail_id;
            $object->total_price=  number_format($total_price,2);
            $object->discount=number_format($discount,2);
            $object->grand_total=number_format($grand_total,2);
            $object->created_by=Auth::User('admin')->id;
            $object->updated_by=Auth::User('admin')->id;
            $object->created_at=date("Y-m-d H:i:s");
            $object->updated_at=date("Y-m-d H:i:s");
            $object->status=1;
            $object->save();
            
            foreach($CartInfo as $Cinfos){
                $per_item_price=0;$total_price=0;$discount=0;$grand_total=0;
                $per_item_price=round((($item_price[$Cinfos->shop_item_id])*$Cinfos->quantity),2);
                $total_price=$total_price+$per_item_price;
                $total_price= round($total_price,2);
                $grand_total=$total_price;
                
                $object1 = new ShopRestCartMap;
                $object1->order_id=$order_id;
                $object1->rest_detail_id=Auth::User('admin')->rest_detail_id;
                $object1->shop_item_id=$Cinfos->shop_item_id;
                $object1->quantity=$Cinfos->quantity;
                $object1->price=  number_format($total_price,2);
                $object1->discount=number_format($discount,2);
                $object1->grand_total=number_format($grand_total,2);
                $object1->created_by=Auth::User('admin')->id;
                $object1->updated_by=Auth::User('admin')->id;
                $object1->created_at=date("Y-m-d H:i:s");
                $object1->updated_at=date("Y-m-d H:i:s");
                $object1->status=1;
                $object1->save();
                
                $shop_detail = DB::table('shop_items')->where('id',$Cinfos->shop_item_id)->get();
                foreach($shop_detail as $info){
                    $quantity = $info->quantity;
                    $price = $info->front_price;
                    $category_id = $info->shop_category_id;
                }
                $content = ShopItem::where('id',$Cinfos->shop_item_id)->first();
                $content->quantity=$quantity-round($Cinfos->quantity,2);
                $content->save();
                
                $TotalPrice = $price*$Cinfos->quantity;
                $object2 = new ShopInventory;
                $object2->shop_category_id=$category_id;
                $object2->shop_item_id=$Cinfos->shop_item_id;
                $object2->quantity=round($Cinfos->quantity,2);
                $object2->price=round($TotalPrice,2);
                $object2->action='M';
                $object2->created_by=Auth::User('admin')->id;
                $object2->updated_by=Auth::User('admin')->id;
                $object2->created_at=date("Y-m-d H:i:s");
                $object2->updated_at=date("Y-m-d H:i:s");
                $object2->status=1;
                $object2->save();
                
                $deleted= DB::table('shop_rest_cart_temps')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->where('shop_item_id',$Cinfos->shop_item_id)->delete();
            }
        }
        Session::flash('message', 'Order Created Successfully!'); 
        return redirect(route('rzone.shop.orders.index'));
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id){
            $count = ShopItem:: where('status', '=','1')->where('id',$id)->count();
            if(!$count){
                Session::flash('message', 'Please select item again.'); 
                return redirect(route('rzone.shop.cart.index'));
            }
            if( ! preg_match('/^\d+$/', $id) ){
                Session::flash('message', 'Please select item again.'); 
                return redirect(route('rzone.shop.cart.index'));
            }
        }
        $data['info'] = ShopItem:: where('id',$id)->where('status','!=', '2')->first();
        return view('admin.shopping.view-item')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
