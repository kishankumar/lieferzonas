<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use validator;
use App\superadmin\OrderService;
use App\superadmin\DiscountType;
use App\admin\SpecialOffer;
use App\admin\SpecialOfferCatMap;
use App\admin\SpecialOfferDayMap;
use App\admin\SpecialOfferDayTimeMap;
use App\admin\SpecialOfferMenuMap;
use App\admin\SpecialOfferSubmenuMap;
use App\admin\SpecialOfferOrderService;

class SpecialOfferController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
       $res_owner_id = Auth::User('admin')->id;
		$rest_id = DB::table('rest_owners')->select('rest_detail_id')->where('id','=',$res_owner_id)->get();
		 for ($i = 0, $c = count($rest_id); $i < $c; ++$i) {
            $rest_id[$i] = (array) $rest_id[$i];
             
        }
      $specialoffer = DB::table('special_offers')
            ->leftjoin('rest_details', 'special_offers.rest_id', '=', 'rest_details.id')
            ->where('special_offers.status','!=', '2')
            ->where('special_offers.rest_id','=', $rest_id)
            ->select('special_offers.*', 'rest_details.f_name','rest_details.l_name')
         
            ->paginate(10);
             for ($i = 0, $c = count($specialoffer); $i < $c; ++$i) {
             $specialoffer[$i] = (array) $specialoffer[$i];
             
             }
    
       return view('admin.specialoffer.view-specialoffer')->with('specialoffers',$specialoffer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $time_val = DB::table('time_validations')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($time_val); $i < $c; ++$i) {
            $time_val[$i] = (array) $time_val[$i];
            
        } 
		$dis_type = DB::table('discount_types')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($dis_type); $i < $c; ++$i) {
            $dis_type[$i] = (array) $dis_type[$i];
            
        } 
		$order_service = DB::table('order_services')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($order_service); $i < $c; ++$i) {
            $order_service[$i] = (array) $order_service[$i];
            
        } 
		
		$res_owner_id = Auth::User('admin')->id;
		$rest_id = DB::table('rest_owners')->select('rest_detail_id')->where('id','=',$res_owner_id)->get();
		 for ($i = 0, $c = count($rest_id); $i < $c; ++$i) {
            $rest_id[$i] = (array) $rest_id[$i];
             
        }
		$categories = DB::table('rest_categories')->select('id', 'name')->orderBy('name', 'asc')->where('status','=','1')->where('rest_detail_id','=',$rest_id)->get();
        
             
        for ($i = 0, $c = count($categories); $i < $c; ++$i) {
            $categories[$i] = (array) $categories[$i];
            
        } 
		$day = DB::table('rest_days')->select('id', 'day')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($day); $i < $c; ++$i) {
            $day[$i] = (array) $day[$i];
            
        } 
        
			
		$auth_id = Auth::User('admin')->id;
		$username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
		for ($i = 0, $c = count($username); $i < $c; ++$i) {
            $username[$i] = (array) $username[$i];
            
        }
		return view('admin.specialoffer.add_specialoffer')->with('dis_types',$dis_type)->with('order_services',$order_service)
		->with('time_validations',$time_val)->with('categories',$categories)->with('days',$day)->with('rest_id',$rest_id)->with('username',$username);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
		    $this->set_validation('',$request);
            $auth_id = Auth::User('admin')->id;
	    
		
			$specialoffer = new SpecialOffer;
			$specialoffer->name=$request->name;
			$specialoffer->rest_id=$request->rest_id;
			$specialoffer->discount_type_id=$request->discount_type;
			$specialoffer->discount_type_value=$request->discount;
			$specialoffer->original_price=$request->original_price;
		    $specialoffer->discount_price=$request->discount_price;
		    $specialoffer->type=$request->type;
		    $specialoffer->validity_type=$request->validity;
				if($request->validity=='T')
				{
				  $validity_table =$request->validity_table;
				   
				   $valid_from = date("Y-m-d H:i:s");
				   $time_validation = DB::table('time_validations')->select('year','month','week' ,'day')->where('status','=','1')->where('id','=',$validity_table)->get();
				
					 
				   for ($i = 0, $c = count($time_validation); $i < $c; ++$i) {
					$time_validation[$i] = (array) $time_validation[$i];
					
				   } 
				   $year = $time_validation['0']['year'];
				   $month = $time_validation['0']['month'];
				   $week = $time_validation['0']['week'];
				   $day = $time_validation['0']['day'];
				   $totaltime = ($year*365) + ($month*30) + ($week*7) + ($day);
				   $valid_to = date('Y-m-d', strtotime("+".$totaltime."days"));
				   $specialoffer->validity_table=$validity_table;
				   $specialoffer->valid_from=$valid_from;
				   $specialoffer->valid_to=$valid_to;
				}
				 
			  else
				{
					 $specialoffer->valid_from=$request->valid_from;
					 $specialoffer->valid_to=$request->valid_to;
				}
				
				$specialoffer->created_by=$auth_id;
				$specialoffer->created_at=date("Y-m-d H:i:s");
				$specialoffer->status='0';
				$specialoffer->save();

		$id =$specialoffer->id;
			
		$orderservices = array();
        $orderservices = $request->order_service;

		if(count($orderservices)>0)
		{
			foreach($orderservices as $orderservice)
		    {
			   $specialofferorder = new SpecialOfferOrderService;
			   $specialofferorder->special_offer_id=$id;
			   $specialofferorder->order_service_id= $orderservice;
			  
			   $specialofferorder->created_by=$auth_id;
			   $specialofferorder->created_at=date("Y-m-d H:i:s");
			   $specialofferorder->status='1';
			   $specialofferorder->save();
		    }
		}
		

		   $category1 = array();
		   $category1= $request->catid1;
		  if(count($category1)>0)
		  {
			  foreach($category1 as $categories1)
		     {
		   	   $specialoffercat = new SpecialOfferCatMap;
			   $specialoffercat->special_offer_id=$id;
			   $specialoffercat->category_id=$categories1; 

			   $specialoffercat->created_by=$auth_id;
			   $specialoffercat->created_at=date("Y-m-d H:i:s");
			   $specialoffercat->status='1';
			   $specialoffercat->save();
		     }
		  }
		  
		   
	       $category2 = array();
		   $category2= $request->catid2;
		   $menu2 = array();
		   $menu2= $request->menuid2;
		   if(count($category2)>0 && count($menu2))
		   {
				for($i=0; $i< count($category2);$i++)
			   {
				//echo $menu2[$i];
			   $specialoffermenu = new SpecialOfferMenuMap;
			   $specialoffermenu ->special_offer_id=$id;
			   $specialoffermenu ->category_id=$category2[$i]; 
			   $specialoffermenu ->menu_id=$menu2[$i]; 
			   $specialoffermenu->created_by=$auth_id;
			   $specialoffermenu->created_at=date("Y-m-d H:i:s");
			   $specialoffermenu->status='1';
			   $specialoffermenu ->save();
			  }
		   }
		   $category3 = array();
		   $category3= $request->catid3;
		   $menu3 = array();
		   $menu3= $request->menuid3;
		   $submenu3 = array();
		   $submenu3= $request->submenuid3;
		   // print_r($category3);
		   if(count($category3)>0 && count($menu3) && count($submenu3))
		   {
				for($i=0; $i<count($category3);$i++)
			   {
			   $specialoffersubmenu = new SpecialOfferSubmenuMap;
			   $specialoffersubmenu->special_offer_id=$id;
			   $specialoffersubmenu->category_id=$category3[$i]; 
			   $specialoffersubmenu->menu_id=$menu3[$i]; 
			   $specialoffersubmenu->sub_menu_id=$submenu3[$i]; 
			   $specialoffersubmenu->created_by=$auth_id;
			   $specialoffersubmenu->created_at=date("Y-m-d H:i:s");
			   $specialoffersubmenu->status='1';
			   $specialoffersubmenu ->save();
			  }
		   }
	  
	    $day = array();
        $day = Input::get('days');
	    
	    for($i=0; $i<count($day); $i++ )
         {
			 
			 $start_time = array();
			 $start_time = Input::get('start_time'.$day[$i]);
			 //print_r($start_time);
			 $end_time = array();
			 $end_time = Input::get( 'end_time'.$day[$i]);
			 //print_r($end_time);
			 //die;
			 if( $start_time['0']=='' ||  $end_time['0']=='')
			 {
				 $specialofferdaymap = new SpecialOfferDayMap;
				 $specialofferdaymap->special_offer_id=$id;
				 $specialofferdaymap->day_id=$day[$i];
				 $specialofferdaymap->updated_by=$auth_id;
		         $specialofferdaymap->updated_at=date("Y-m-d H:i:s");
				 $specialofferdaymap->status='1';
				 $specialofferdaymap->save();
			 }
			 else
			 {
			 	
				 for($j=0; $j<count($start_time); $j++ )
                 {
                 	 $specialofferdaytimemap = new SpecialOfferDayTimeMap;
				     $specialofferdaytimemap->special_offer_id=$id;
				     $specialofferdaytimemap->day_id=$day[$i];
                 	
					 $specialofferdaytimemap->time_from=$start_time[$j]; 
					 $specialofferdaytimemap->time_to=$end_time[$j]; 
					 $specialofferdaytimemap->created_by=$auth_id;
					 $specialofferdaytimemap->created_at=date("Y-m-d H:i:s");
					 $specialofferdaytimemap->status='1';
					 $specialofferdaytimemap->save();
					
				 }
			}	  
			
		 }
		return redirect(route('rzone.specialoffer.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$specialoffer = DB::table('special_offers')->select('*')->where('id','=',$id)->get();
        
             
        for ($i = 0, $c = count($specialoffer); $i < $c; ++$i) {
            $specialoffer [$i] = (array) $specialoffer [$i];
            
        }  

		$time_val = DB::table('time_validations')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($time_val); $i < $c; ++$i) {
            $time_val[$i] = (array) $time_val[$i];
            
        } 
		$dis_type = DB::table('discount_types')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($dis_type); $i < $c; ++$i) {
            $dis_type[$i] = (array) $dis_type[$i];
            
        } 
		$order_service = DB::table('order_services')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($order_service); $i < $c; ++$i) {
            $order_service[$i] = (array) $order_service[$i];
            
        } 
		$res_owner_id = Auth::User('admin')->id;
		$rest_id = DB::table('rest_owners')->select('rest_detail_id')->where('id','=',$res_owner_id)->get();
		 for ($i = 0, $c = count($rest_id); $i < $c; ++$i) {
            $rest_id[$i] = (array) $rest_id[$i];
             
        }
		$categories = DB::table('rest_categories')->select('id', 'name')->orderBy('name', 'asc')->where('status','=','1')->where('rest_detail_id','=',$rest_id)->get();
        
             
        for ($i = 0, $c = count($categories); $i < $c; ++$i) 
        {
            $categories[$i] = (array) $categories[$i];

            
        } 
        //print_r($categories); 

        $menus = DB::table('rest_menus')->select('id', 'name')->where('rest_category_id',1)->where('status','=','1')->get();
        
             
            for ($i = 0, $c = count($menus); $i < $c; ++$i) {
            $menus[$i] = (array) $menus[$i];
            
        } 
		$day = DB::table('rest_days')->select('id', 'day')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($day); $i < $c; ++$i) {
            $day[$i] = (array) $day[$i];
            
        } 
        
        
        $selected_cat=SpecialOfferCatMap::where('special_offer_id','=',$id)->where('status','!=','2')->lists('category_id');		
        $selected_cat= $selected_cat->toArray();
		
        $submenus = DB::table('rest_sub_menus')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($submenus); $i < $c; ++$i) {
            $submenus[$i] = (array) $submenus[$i];
            
        }


            $listcat = DB::table('special_offer_cat_maps')
		   ->leftjoin('rest_categories', 'special_offer_cat_maps.category_id', '=', 'rest_categories.id')
           ->select('rest_categories.name','rest_categories.id')
           ->where('special_offer_cat_maps.status','=','1')
           ->where('special_offer_cat_maps.special_offer_id','=',$id)->get();

           for ($i = 0, $c = count($listcat); $i < $c; ++$i) {
            $listcat[$i] = (array) $listcat[$i];
            
            } 



            $listmenu = DB::table('special_offer_menu_maps')
		   ->leftjoin('rest_menus', 'special_offer_menu_maps.menu_id', '=', 'rest_menus.id')
		  ->leftjoin('rest_categories', 'special_offer_menu_maps.category_id', '=', 'rest_categories.id')
           ->select('special_offer_menu_maps.*','rest_menus.name as menuname','rest_categories.name as catname')
           ->where('special_offer_menu_maps.status','=','1')
           ->where('special_offer_menu_maps.special_offer_id','=',$id)->get();

           for ($i = 0, $c = count($listmenu); $i < $c; ++$i) {
            $listmenu[$i] = (array) $listmenu[$i];
            
            } 
            

            $listsubmenu = DB::table('special_offer_submenu_maps')
		   ->leftjoin('rest_menus', 'special_offer_submenu_maps.menu_id', '=', 'rest_menus.id')
		  ->leftjoin('rest_categories', 'special_offer_submenu_maps.category_id', '=', 'rest_categories.id')
		   ->leftjoin('rest_sub_menus', 'special_offer_submenu_maps.sub_menu_id', '=', 'rest_sub_menus.id')
           ->select('special_offer_submenu_maps.*','rest_menus.name as menuname','rest_categories.name as catname','rest_sub_menus.name as submenuname')
           ->where('special_offer_submenu_maps.status','=','1')
            ->where('special_offer_submenu_maps.special_offer_id','=',$id)->get();


           for ($i = 0, $c = count($listsubmenu); $i < $c; ++$i) {
           $listsubmenu[$i] = (array) $listsubmenu[$i];
            
            } 


        $selected_menu=SpecialOfferMenuMap::where('special_offer_id','=',$id)->where('status','!=','2')->select('category_id','menu_id')->get();		
        $selected_menu= $selected_menu->toArray();
     

        $selected_submenu=SpecialOfferSubmenuMap::where('special_offer_id','=',$id)->where('status','!=','2')->select('category_id','menu_id','sub_menu_id')->get();		
        $selected_submenu= $selected_submenu->toArray();
       
        $selected_order=SpecialOfferOrderService::where('special_offer_id','=',$id)->where('status','!=','2')->lists('order_service_id');		
        $selected_order= $selected_order->toArray();

       
        $selected_day=SpecialOfferDayMap::where('special_offer_id','=',$id)->where('status','!=','2')->lists('day_id');		
        $selected_day= $selected_day->toArray();
        $selected_daytime=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->lists('day_id');		
        $selected_daytime= $selected_daytime->toArray();
		
        $selected_daytimeall = array();
        $selected_daytimeall = array_keys(array_flip(array_unique(array_merge($selected_day,$selected_daytime))));
		
		for($i=0;$i<count($selected_daytimeall)-1;$i++)
		{
			if($selected_daytimeall[$i] > $selected_daytimeall[$i+1])
			{
				$temp = $selected_daytimeall[$i];
				$selected_daytimeall[$i] = $selected_daytimeall[$i+1];
				$selected_daytimeall[$i+1] = $temp;
			}
		}
		
         $fromtime = array();$totime = array();$toTime=1;
         $fromtime['']="Select";$totime['']="Select";
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            $fromtime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            $fromtime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            if($toTime>1){
                $totime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            }$toTime=2;
            $totime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        $totime['11:59 PM'] = '11:59 PM';
        $auth_id = Auth::User('admin')->id;
		$username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
		for ($i = 0, $c = count($username); $i < $c; ++$i) {
            $username[$i] = (array) $username[$i];
            
        }
         return view('admin/specialoffer/edit_specialoffer')->with('dis_types',$dis_type)->with('order_services',$order_service)
		->with('time_validations',$time_val)->with('categories',$categories)->with('days',$day)->with('rest_id',$rest_id)
		->with('specialoffer',$specialoffer)->with('selected_order',$selected_order)
		->with('selected_daytimeall',$selected_daytimeall)->with('selected_cat',$selected_cat)->with('selected_menu' ,$selected_menu)->with('selected_submenu',$selected_submenu)->with('menus' ,$menus)->with('submenus',$submenus)
		->with('fromtime',$fromtime)->with('totime',$totime)->with('listcat',$listcat)->with('listmenu',$listmenu)->with('listsubmenu',$listsubmenu)->with('username',$username);
		 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		    $this->set_validation('',$request);
            $auth_id = Auth::User('admin')->id;
	    
		
			$specialoffer = SpecialOffer::find($id);
            $specialoffer->name=$request->name;
			$specialoffer->discount_type_id=$request->discount_type;
			$specialoffer->discount_type_value=$request->discount;
			$specialoffer->original_price=$request->original_price;
		    $specialoffer->discount_price=$request->discount_price;
		    $specialoffer->type=$request->type;
		    $specialoffer->validity_type=$request->validity;
				if($request->validity=='T')
				{
				  $validity_table =$request->validity_table;
				   
				   $valid_from = date("Y-m-d H:i:s");
				   $time_validation = DB::table('time_validations')->select('year','month','week' ,'day')->where('status','=','1')->where('id','=',$validity_table)->get();
				
					 
				   for ($i = 0, $c = count($time_validation); $i < $c; ++$i) {
					$time_validation[$i] = (array) $time_validation[$i];
					
				   } 
				   $year = $time_validation['0']['year'];
				   $month = $time_validation['0']['month'];
				   $week = $time_validation['0']['week'];
				   $day = $time_validation['0']['day'];
				   $totaltime = ($year*365) + ($month*30) + ($week*7) + ($day);
				   $valid_to = date('Y-m-d', strtotime("+".$totaltime."days"));
				   $specialoffer->validity_table=$validity_table;
				   $specialoffer->valid_from=$valid_from;
				   $specialoffer->valid_to=$valid_to;
				}
				 
			  else
				{
					 $specialoffer->validity_table='0';
					 $specialoffer->valid_from=$request->valid_from;
					 $specialoffer->valid_to=$request->valid_to;
				}
				
				$specialoffer->updated_by=$auth_id;
				$specialoffer->updated_at=date("Y-m-d H:i:s");
				
				$specialoffer->save();
				$orderservices = array();
                $orderservices = $request->order_service;

		$countorderservice= SpecialOfferOrderService::where('special_offer_id', '=', $id)->where('status','=','1')->get();
        $countorderservice = count($countorderservice->toArray());
        if($countorderservice!= null)
        {
            $affected_data = DB::table('special_offer_order_services')
            ->where('special_offer_id', '=', $id)
            ->where('status', '=', '1')
            ->update(array('status' => '2'));
        }
        $restlist = array();
        $restlist = Input::get('restlist');

		foreach($orderservices as $orderservice)
		{
		   $specialofferorder = new SpecialOfferOrderService;
		   $specialofferorder->special_offer_id=$id;
		   $specialofferorder->order_service_id= $orderservice;
		  
		   $specialofferorder->updated_by=$auth_id;
		   $specialofferorder->updated_at=date("Y-m-d H:i:s");
		   $specialofferorder->status='1';
		   $specialofferorder->save();
		}


		    $countcat= SpecialOfferCatMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countcat = count($countcat->toArray());
	        if($countcat!= null)
	        {
	             $affected_data = DB::table('special_offer_cat_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }


		   $category1 = array();
		   $category1= $request->catid1;
		   if(count($category1)>0)
		   {

		   	foreach($category1 as $categories1)
			  {
			   	   $specialoffercat = new SpecialOfferCatMap;
				   $specialoffercat->special_offer_id=$id;
				   $specialoffercat->category_id=$categories1; 

				   $specialoffercat->created_by=$auth_id;
				   $specialoffercat->created_at=date("Y-m-d H:i:s");
				   $specialoffercat->status='1';
				   $specialoffercat->save();
			   }
		   }
			  



		    $countmenu= SpecialOfferMenuMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countmenu = count($countmenu->toArray());
	        if($countmenu!= null)
	        {
	             $affected_data = DB::table('special_offer_menu_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }

	       $category2 = array();
		   $category2= $request->catid2;
		   $menu2 = array();
		   $menu2= $request->menuid2;
		   if(count($category2)>0 && count($menu2)>0)
		   {
			    for($i=0; $i< count($category2);$i++)
			   {
			   	//echo $menu2[$i];
			   $specialoffermenu = new SpecialOfferMenuMap;
			   $specialoffermenu ->special_offer_id=$id;
			   $specialoffermenu ->category_id=$category2[$i]; 
			   $specialoffermenu ->menu_id=$menu2[$i]; 
			   $specialoffermenu->created_by=$auth_id;
			   $specialoffermenu->created_at=date("Y-m-d H:i:s");
			   $specialoffermenu->status='1';
			   $specialoffermenu ->save();
			  }
           }
            $countsubmenu= SpecialOfferSubmenuMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countsubmenu = count($countsubmenu->toArray());
	        if($countsubmenu!= null)
	        {
	             $affected_data = DB::table('special_offer_submenu_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }

		   $category3 = array();
		   $category3= $request->catid3;
		   $menu3 = array();
		   $menu3= $request->menuid3;
		   $submenu3 = array();
		   $submenu3= $request->submenuid3;
		 

            if(count($category3)>0 && count($menu3)>0 && count($submenu3)>0)
		    {
		        for($i=0; $i<count($category3);$i++)
			   {
			   $specialoffersubmenu = new SpecialOfferSubmenuMap;
			   $specialoffersubmenu->special_offer_id=$id;
			   $specialoffersubmenu->category_id=$category3[$i]; 
			   $specialoffersubmenu->menu_id=$menu3[$i]; 
			   $specialoffersubmenu->sub_menu_id=$submenu3[$i]; 
			   $specialoffersubmenu->created_by=$auth_id;
			   $specialoffersubmenu->created_at=date("Y-m-d H:i:s");
			   $specialoffersubmenu->status='1';
			   $specialoffersubmenu ->save();
			  }
		
            }
	      
            $countdaytime= SpecialOfferDayTimeMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countdaytime = count($countdaytime->toArray());
	        //echo $countdaytime; die;
	        if($countdaytime!= null)
	        {
	             $affected_data = DB::table('special_offer_day_time_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }
	        $countday= SpecialOfferDayMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countday = count($countday->toArray());
	        //echo $countdaytime; die;
	        if($countday!= null)
	        {
	             $affected_data = DB::table('special_offer_day_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }
	        $day = array();
            $day = Input::get('days');
          //print_r($day);
          //echo count($day);
	    
	    for($i=0; $i<count($day); $i++ )
         {


         	
	        
			 $start_times = array();
			 $start_times = Input::get('start_time'.$day[$i]);
			 //echo count($start_times);
			 //print_r($start_times);
			 $end_times= array();
			 $end_times = Input::get( 'end_time'.$day[$i]);
			 if( $start_times['0']=='' &&  $end_times['0']=='')
			 {
				 $specialofferdaymap = new SpecialOfferDayMap;
				 $specialofferdaymap->special_offer_id=$id;
				 $specialofferdaymap->day_id=$day[$i];
				 $specialofferdaymap->updated_by=$auth_id;
		         $specialofferdaymap->updated_at=date("Y-m-d H:i:s");
				 $specialofferdaymap->status='1';
				 $specialofferdaymap->save();
			 }
			 else
			 {
			
			 	
				 for($j=0; $j<count($start_times); $j++ )
                 {
                 	 
                 	 $specialofferdaytimemap = new SpecialOfferDayTimeMap;
				     $specialofferdaytimemap->special_offer_id=$id;
				     $specialofferdaytimemap->day_id=$day[$i];
                 	
					 $specialofferdaytimemap->time_from=$start_times[$j]; 
					 $specialofferdaytimemap->time_to=$end_times[$j]; 
					 $specialofferdaytimemap->created_by=$auth_id;
					 $specialofferdaytimemap->created_at=date("Y-m-d H:i:s");
					 $specialofferdaytimemap->status='1';
					 $specialofferdaytimemap->save();
					
				 }
			}	  
			
		 }
		 return redirect(route('rzone.specialoffer.index'));
	   
		 //return redirect(route('rzone.specialoffer.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	 public function getDatatbl(Request $request)
    {
        if($request) {
            $table=$request->table;
            $mappingTable=$request->maptable;
            $cond=$request->value;
            $joinColumn=$request->joincolumn;
            $selectColumn=$request->selectcolumn;
            $whereColumn=$request->wherecolumn;
            $secondjoin=$request->secondjoincolumn;
            $selectedOption=$request->selectedOption;
            
            $values= DB::table($table)
                ->join($mappingTable, $table.'.'.$joinColumn, '=', $mappingTable.'.'.$secondjoin)
                ->select($mappingTable.'.'.'id',$mappingTable.'.'.$selectColumn)
                ->where($table.'.'.$whereColumn,$cond)
                ->get();
            
            $option='<option value="">Select</option>';
            foreach ($values as $value ) {
                $option .='<option  name="'.$value->$selectColumn.'"  value="'.$value->id.'" '.(($selectedOption==$value->id)?'selected':'').'>'.$value->$selectColumn.'</option>';
            }
            echo $option;
        }
    }

    public function getDetails($offerId=0)
    {

            $specialoffer = DB::table('special_offers')
            ->leftjoin('discount_types', 'special_offers.discount_type_id', '=', 'discount_types.id')
           ->where('special_offers.id','=',$offerId)
           ->select('special_offers.*','discount_types.name as discount_type_name')->get();
              for ($i = 0, $c = count($specialoffer); $i < $c; ++$i) {
             $specialoffer[$i] = (array) $specialoffer[$i];
             
             }
             
            $orderservice = DB::table('special_offer_order_services')
            ->leftjoin('order_services', 'special_offer_order_services.order_service_id', '=', 'order_services.id')
            ->where('special_offer_order_services.special_offer_id','=',$offerId)
            ->where('special_offer_order_services.status','!=',2)
            ->select('special_offer_order_services.order_service_id','order_services.name as order_service_name')->get();

              for ($i = 0, $c = count($orderservice); $i < $c; ++$i) {
             $orderservice[$i] = (array) $orderservice[$i];
             
             }


              $listcat = DB::table('special_offer_cat_maps')
		   ->leftjoin('rest_categories', 'special_offer_cat_maps.category_id', '=', 'rest_categories.id')
           ->select('rest_categories.name','rest_categories.id')
           ->where('special_offer_cat_maps.status','=','1')
           ->where('special_offer_cat_maps.special_offer_id','=',$offerId)->get();

           for ($i = 0, $c = count($listcat); $i < $c; ++$i) {
            $listcat[$i] = (array) $listcat[$i];
            
            } 



            $listmenu = DB::table('special_offer_menu_maps')
		   ->leftjoin('rest_menus', 'special_offer_menu_maps.menu_id', '=', 'rest_menus.id')
		  ->leftjoin('rest_categories', 'special_offer_menu_maps.category_id', '=', 'rest_categories.id')
           ->select('special_offer_menu_maps.*','rest_menus.name as menuname','rest_categories.name as catname')
           ->where('special_offer_menu_maps.status','=','1')
           ->where('special_offer_menu_maps.special_offer_id','=',$offerId)->get();

           for ($i = 0, $c = count($listmenu); $i < $c; ++$i) {
            $listmenu[$i] = (array) $listmenu[$i];
            
            } 
            
              //print_r($listmenu); die;

            $listsubmenu = DB::table('special_offer_submenu_maps')
		   ->leftjoin('rest_menus', 'special_offer_submenu_maps.menu_id', '=', 'rest_menus.id')
		  ->leftjoin('rest_categories', 'special_offer_submenu_maps.category_id', '=', 'rest_categories.id')
		   ->leftjoin('rest_sub_menus', 'special_offer_submenu_maps.sub_menu_id', '=', 'rest_sub_menus.id')
           ->select('special_offer_submenu_maps.*','rest_menus.name as menuname','rest_categories.name as catname','rest_sub_menus.name as submenuname')
           ->where('special_offer_submenu_maps.status','=','1')
            ->where('special_offer_submenu_maps.special_offer_id','=',$offerId)->get();


           for ($i = 0, $c = count($listsubmenu); $i < $c; ++$i) {
           $listsubmenu[$i] = (array) $listsubmenu[$i];
            
            } 
		$day = DB::table('rest_days')->select('id', 'day')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($day); $i < $c; ++$i) {
            $day[$i] = (array) $day[$i];
            
        } 
		
		$selected_day=SpecialOfferDayMap::where('special_offer_id','=',$offerId)->where('status','!=','2')->lists('day_id');     
        $selected_day= $selected_day->toArray();
        $selected_daytime=SpecialOfferDayTimeMap::where('special_offer_id','=',$offerId)->where('status','!=','2')->lists('day_id');     
        $selected_daytime= $selected_daytime->toArray();

        $selected_daytimeall = array();
        $selected_daytimeall = array_keys(array_flip(array_unique(array_merge($selected_day,$selected_daytime))));
		//print_r($selected_daytimeall); die;
		for($i=0;$i<count($selected_daytimeall)-1;$i++)
		{
			if($selected_daytimeall[$i] > $selected_daytimeall[$i+1])
			{
				$temp = $selected_daytimeall[$i];
				$selected_daytimeall[$i] = $selected_daytimeall[$i+1];
				$selected_daytimeall[$i+1] = $temp;
			}
		}

        
		
		$auth_id = Auth::User('admin')->id;
		$username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
		for ($i = 0, $c = count($username); $i < $c; ++$i) {
            $username[$i] = (array) $username[$i];
            
        }
        return view('admin.specialoffer.specialoffer_detail')->with('specialoffer',$specialoffer)->with('orderservice',$orderservice)
		->with('listcat',$listcat)->with('listmenu',$listmenu)->with('listsubmenu',$listsubmenu)
		->with('selected_daytimeall',$selected_daytimeall)->with('days',$day)->with('username',$username);
	}
	
	public function download(Request $request)
    {
		
        if($request->ajax()){
            $data = Input::all();
            $selectedOffers = $data['selectedOffers'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Restaurantt</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Dicount type</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Valid from</th>
						<th style="border:solid 1px #81C02F; padding:10px">Valid to</th>
						<th style="border:solid 1px #81C02F; padding:10px">Dicount type value</th>
						<th style="border:solid 1px #81C02F; padding:10px">Original price</th>
						<th style="border:solid 1px #81C02F; padding:10px">Discount price</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th></tr></thead>';
        foreach($selectedOffers as $key => $value)
        {
            $data = DB::table('special_offers')
            ->leftjoin('rest_details', 'special_offers.rest_id', '=', 'rest_details.id')
            ->where('special_offers.id','=', $value)
           
            ->select('special_offers.*', 'rest_details.f_name','rest_details.l_name')
         
            ->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}
                if($data['discount_type_id']=='1')
				{
					$discount_type='Fixed';
				}
				else if($data['discount_type_id']=='2')
				{
					$discount_type='Percentage';
				}
				else
				{
					$discount_type='One free on purchase';
				}
				$valid_from = date("d M Y", strtotime($data['valid_from']));
				$valid_to = date("d M Y", strtotime($data['valid_to']));
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['f_name'].' '.$data['l_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$discount_type.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_from.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_to.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['discount_type_value'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['original_price'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['discount_price'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	
	public function set_validation($id=null,$request)
     {
        $message=array(
            "discount.required"=>"Coupon code is required",
            "original_price.required"=>"Original price is required",
			"discount_price.required"=>"Discount price is required",
            );

        $this->validate($request,[
        'discount' => 'required',
        'original_price' => 'required',
		'discount_price' => 'required',
        ],$message);
     }
}
