<?php

namespace App\Http\Controllers\admin;
date_default_timezone_set('Asia/Calcutta'); 
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\OrderItem;
use App\front\UserOrder;
use App\front\UserOrderReview;
use App\front\UserRestFavourite;
class OrderRateController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$auth_id = Auth::user('admin')->id;
		$Restowner= DB ::table('rest_owners')
		->where('id','=',$auth_id)
		->select('rest_detail_id')->get();
		for ($i = 0, $c = count($Restowner); $i < $c; ++$i) {
            $Restowner[$i] = (array) $Restowner[$i];
        }
		$rest_id = $Restowner['0']['rest_detail_id']; 
		
		$order_rate= DB ::table('user_order_reviews')
		->leftjoin('user_orders','user_order_reviews.order_id','=','user_orders.order_id')
		->leftjoin('rest_details','user_orders.rest_detail_id', '=','rest_details.id')
		->leftjoin('front_user_addresses','user_orders.front_user_address_id', '=','front_user_addresses.id')
		->where('user_order_reviews.status','!=',2)
		->where('user_order_reviews.rest_id','=',$rest_id)
		->orderby('user_orders.created_at','desc')
		->select('user_order_reviews.comment','user_order_reviews.id','user_order_reviews.is_reported','user_order_reviews.comment','user_order_reviews.quality_rating','user_order_reviews.service_rating','user_orders.order_id','user_orders.created_at','rest_details.image','rest_details.f_name','rest_details.l_name','front_user_addresses.booking_person_name')
		->paginate(10);
        return view('admin/userorder/order_rate')->with('order_rate',$order_rate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function report()
	{
	 $review_id = Input::get('review_id'); 
	 $report = UserOrderReview::find($review_id);
	 $report->is_reported = '1';
     $report->save();
	 $json['success']=1;
        echo json_encode($json);
        return ;
	}
	public function getcomment()
	{
	 $review_id = Input::get('review_id'); 
	 $order_rate= DB ::table('user_order_reviews')
	 ->where('id','=',$review_id)
	 ->select('comment')->get();
	 for ($i = 0, $c = count($order_rate); $i < $c; ++$i) {
            $order_rate[$i] = (array) $order_rate[$i];
        }
	    $comment =  $order_rate['0']['comment'];
		$json['comment']=$comment;
		
        $json['success']=1;
        echo json_encode($json);
        return ;
	}
}
