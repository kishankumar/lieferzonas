<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\UserRestFavourite;

class UserFavouriteController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$auth_id = Auth::user('admin')->id;
		$Restowner= DB ::table('rest_owners')
		->where('id','=',$auth_id)
		->select('rest_detail_id')->get();
		for ($i = 0, $c = count($Restowner); $i < $c; ++$i) {
            $Restowner[$i] = (array) $Restowner[$i];
        }
		$rest_id = $Restowner['0']['rest_detail_id']; 
		
		
		$favlist= DB ::table('user_rest_favourites')
		->leftjoin('rest_details', 'user_rest_favourites.rest_id', '=', 'rest_details.id')
		->leftjoin('front_user_details', 'user_rest_favourites.user_id', '=', 'front_user_details.front_user_id')
		->where('rest_details.id','=',$rest_id)
		->select('front_user_details.fname','front_user_details.lname','user_rest_favourites.*')
		->paginate(10);
		
        return view('admin/userbonus/favourites')->with('favlist',$favlist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
