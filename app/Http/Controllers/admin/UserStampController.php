<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\UserStampRestCurrent;

class UserStampController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$auth_id = Auth::user('admin')->id;
		$Restowner= DB ::table('rest_owners')
		->where('id','=',$auth_id)
		->select('rest_detail_id')->get();
		for ($i = 0, $c = count($Restowner); $i < $c; ++$i) {
            $Restowner[$i] = (array) $Restowner[$i];
        }
		$rest_id = $Restowner['0']['rest_detail_id']; 
		
		$x = date("d-m-Y");
        $day_id = date('N', strtotime($x)); 
		$stamplist= DB ::table('user_stamp_rest_currents')
		->leftjoin('rest_details', 'user_stamp_rest_currents.rest_detail_id', '=', 'rest_details.id')
		->leftjoin('front_user_details', 'user_stamp_rest_currents.user_id', '=', 'front_user_details.front_user_id')
		->where('rest_details.id','=',$rest_id)
		->select('front_user_details.fname','front_user_details.lname','front_user_details.email as uemail','front_user_details.mobile as umobile',
		'user_stamp_rest_currents.rest_detail_id','user_stamp_rest_currents.stamp_count')
		->paginate(10);
		//print_r($stamplist); die;
		$stampcount= DB ::table('rest_stamps')->select('stamp_count')->get();
		for ($i = 0, $c = count($stampcount); $i < $c; ++$i) {
            $stampcount[$i] = (array) $stampcount[$i];
        }
        return view('admin/userbonus/stamp_list')->with('stamplist',$stamplist)->with('stampcount',$stampcount);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
