<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Input;
use Validator;
use Auth;
use App\superadmin\RestDetail;
use App\front\OrderItem;
use App\front\UserOrder;

use App\front\OrderPayment;

use App\admin\FrontOrderStatus;
use App\admin\FrontOrderStatusMap;
use App\front\FrontUserDetail;

class RestaurantOrderController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='created-desc';
        }
        
        $toDate = date('Y-m-d');
        $fromDate = date("Y-m-d");
        $data['to_date']='';$data['from_date']='';
        
        if($request->from){
            $fromDate = date("Y-m-d", strtotime($request->from));
            $data['from_date']=date("Y-m-d", strtotime($request->from));
        }
        if($request->to){
            $toDate = date("Y-m-d", strtotime($request->to));
            $data['to_date']=date("Y-m-d", strtotime($request->to));
        }
        
        $q = DB::table('user_orders');
        $q->leftjoin('front_user_details', 'user_orders.front_user_id', '=', 'front_user_details.front_user_id');
        $q->leftjoin('front_users', 'front_users.id', '=', 'user_orders.front_user_id');
        $q->leftjoin('front_order_statuses', 'front_order_statuses.id', '=', 'user_orders.front_order_status_id');
        $q->leftjoin('order_payments','order_payments.order_id', '=', 'user_orders.order_id');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('user_orders.order_id', 'LIKE', "%$search%");
                $q2->orWhere('front_user_details.fname', 'LIKE', "%$search%");
                $q2->orWhere('front_user_details.lname', 'LIKE', "%$search%");
                $q2->orWhere('front_user_details.email', 'LIKE', "%$search%");
                $q2->orWhere('front_user_details.mobile', 'LIKE', "%$search%");
                //$q2->orWhere('front_users.email', 'LIKE', "%$search%");
                //$q2->orWhere(DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%$search%");
            });
        }
        $q->where('user_orders.status','=', '1');
        $q->where(DB::raw('DATE_FORMAT(lie_user_orders.created_at,"%Y-%m-%d")'),'>=',$fromDate);
        $q->where(DB::raw('DATE_FORMAT(lie_user_orders.created_at,"%Y-%m-%d")'),'<=',$toDate);
        
        $q->where('user_orders.rest_detail_id',Auth::User('admin')->rest_detail_id);
        if($type=='order'){
            $q->orderBy('user_orders.order_id', 'asc');
        }elseif($type=='order-desc'){
            $q->orderBy('user_orders.order_id', 'desc');
        }elseif($type=='created-desc'){
            $q->orderBy('user_orders.created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('user_orders.created_at', 'asc');
        }else if($type=='name'){
            $q->orderBy('front_user_details.fname', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('front_user_details.fname', 'desc');
        }elseif($type=='number-desc'){
            $q->orderBy('front_user_details.mobile', 'desc');
        }elseif($type=='number'){
            $q->orderBy('front_user_details.mobile', 'asc');
        }else{
            $q->orderBy('user_orders.created_at', 'desc');
        }
        $q->select('user_orders.*','front_user_details.fname','front_user_details.lname','front_user_details.mobile','front_user_details.dob',
                'front_users.email','front_order_statuses.status_name','order_payments.payment_method','order_payments.order_type');
        $data['OrderInfo']=$q->paginate(15);
        $data['OrderInfoCount'] = UserOrder:: where('status','=','1')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->orderBy('created_at','desc')->paginate(10);
        $data['type']=$type;
        $data['search']=$search;
        return view('admin.OrderDetails.orders')->with($data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id){
            $count= UserOrder:: where('status','=','1')->where('order_id','=',$id)->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->count();
            if($count < 1){
                Session::flash('message', 'Please select order again.'); 
                return redirect('rzone/restaurant/orders');
            }

        }
        
        $q = DB::table('user_orders');
        $q->leftjoin('front_user_details', 'user_orders.front_user_id', '=', 'front_user_details.front_user_id');
        $q->leftjoin('front_users', 'front_users.id', '=', 'user_orders.front_user_id');
        $q->leftjoin('rest_details', 'rest_details.id', '=', 'user_orders.rest_detail_id');
        $q->leftjoin('front_user_addresses',  'user_orders.front_user_address_id', '=','front_user_addresses.id');
        $q->leftjoin('countries',  'front_user_addresses.country_id', '=','countries.id');
        $q->leftjoin('states',  'front_user_addresses.state_id', '=','states.id');
        $q->leftjoin('cities',  'front_user_addresses.city_id', '=','cities.id');
        $q->leftjoin('front_order_statuses', 'front_order_statuses.id', '=', 'user_orders.front_order_status_id');
        $q->leftjoin('order_payments', 'order_payments.order_id', '=','user_orders.order_id');
        
        $q->where('user_orders.status','=', '1');
        $q->where('user_orders.rest_detail_id',Auth::User('admin')->rest_detail_id);
        $q->where('user_orders.order_id',$id);
        $q->select('user_orders.*','front_user_details.fname','front_user_details.lname','front_user_details.mobile','front_user_details.dob',
                'front_users.email','rest_details.f_name','rest_details.l_name','front_user_addresses.booking_person_name','front_user_addresses.zipcode',
                'front_user_addresses.address','front_user_addresses.landmark','countries.name as country_name',
                'states.name as state_name','cities.name as city_name','front_order_statuses.status_name',
                'order_payments.payment_method','order_payments.order_type');
        $data['OrderInfo']=$q->get();
        $data['ItemInfo'] = OrderItem:: where('status','=','1')->where('order_id','=',$id)->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->get();
        
        $data['order_status'] = DB::table('front_order_status_maps')
                ->leftjoin('front_order_statuses', 'front_order_statuses.id', '=', 'front_order_status_maps.status_id')
                ->where('front_order_status_maps.order_id','=',$id)
                ->select('front_order_statuses.status_name','front_order_statuses.id','front_order_status_maps.created_at')
                ->get();
        $data['TransactionInfo'] = DB::table('front_order_transactions')->where('order_id','=',$id)->get();
        //echo '<pre>';print_r($data['ItemInfo']);die;
        return view('admin.OrderDetails.viewpage')->with($data); 
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function orderList(Request $request){
        $search='';
        if($request->search){
            $search=$request->search;
        }
        $data['search']=$search;
        $q = DB::table('user_orders');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('user_orders.order_id', 'LIKE', "%$search%");
                //$q2->orWhere('front_user_details.mobile', 'LIKE', "%$search%");
            });
        }
        $q->where('user_orders.status','=', '1');
        $q->where('user_orders.rest_detail_id',Auth::User('admin')->rest_detail_id);
        $q->orderBy('user_orders.created_at', 'desc');
        $data['OrderInfo']=$q->paginate(20);
        //$data['OrderInfo'] = UserOrder:: where('status','=','1')->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->orderBy('created_at','desc')->paginate(20);
        return view('admin.OrderDetails.orders_status_list')->with($data); 
    }
    
    public function orderStatus(Request $request, $orderId){
        if($orderId){
            $count= UserOrder:: where('status','=','1')->where('order_id','=',$orderId)->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->count();
            if($count < 1){
                Session::flash('message', 'Please select order again.'); 
                return redirect('rzone/restaurant/order/list');
            }
        }
        //echo Auth::User('admin')->rest_detail_id;die;
        $data['ItemInfo'] = OrderItem:: where('status','=','1')->where('order_id','=',$orderId)->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->get();
        $data['OrderInfo'] = UserOrder:: where('status','=','1')->where('order_id','=',$orderId)->where('rest_detail_id',Auth::User('admin')->rest_detail_id)->first();
        //echo '<pre>';print_r($data['ItemInfo']);die;
        return view('admin.OrderDetails.order_status')->with($data); 
    }
    
    public function startCookingDelivery(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $orderId = $data['orderId'];
            $status = $data['status'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        if($status == 'cooked'){
            $front_order_status_id = 5;
        }else if($status == 'deliver'){
            $front_order_status_id = 6;
        }else if($status == 'finishDelivered'){
            $front_order_status_id = 7;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $restId = Auth::User('admin')->rest_detail_id;
        $count = FrontOrderStatusMap:: where('order_id',$orderId)->where('status_id',$front_order_status_id)->count();
        if($count>0){
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        
        $type = UserOrder::where('rest_detail_id','=',$restId)->where('order_id',$orderId)->first();
        $type->front_order_status_id=$front_order_status_id;
        $type->updated_by=Auth::User('admin')->id;
        $type->updated_at=date("Y-m-d H:i:s");
        $type->save();
        
        $content = new FrontOrderStatusMap;
        $content->created_at=date("Y-m-d H:i:s");
        $content->created_by=Auth::User('admin')->id;
        $content->order_id=$orderId;
        $content->status_id=$front_order_status_id;
        $content->updated_by=Auth::User('admin')->id;
        $content->updated_at=date("Y-m-d H:i:s");
        $content->status=1;
        $content->save();
        
        if($status == 'cooked'){
            $json['message']="Cooking has been started on ".date("d M Y h:i:s A");
        }else if($status == 'deliver'){
            $json['message']="Delivering has been started on ".date("d M Y h:i:s A");
        }else if($status == 'finishDelivered'){
            $json['message']="Order has been delivered at ".date("d M Y h:i:s A");
            $json['orderDelivedtime']=date("d M Y h:i:s A");
        }
        $json['success']=1;
        echo json_encode($json);
        return;
    }
    
    public function orderRejected(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $orderId = $data['orderId'];
            $time_type = $data['time_type'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $restId = Auth::User('admin')->rest_detail_id;
        $count = FrontOrderStatusMap:: where('order_id',$orderId)->where('status_id',3)->count();
        if($count>0){
            $json['success']=0;
            echo json_encode($json);
            return;
        }else{
            $type = UserOrder::where('rest_detail_id','=',$restId)->where('order_id',$orderId)->first();
            $type->front_order_status_id=3;
            $type->updated_by=Auth::User('admin')->id;
            $type->updated_at=date("Y-m-d H:i:s");
            $type->save();
            
            $content = new FrontOrderStatusMap;
            $content->created_at=date("Y-m-d H:i:s");
            $content->created_by=Auth::User('admin')->id;
            $content->order_id=$orderId;
            $content->status_id=3;
            $content->updated_by=Auth::User('admin')->id;
            $content->updated_at=date("Y-m-d H:i:s");
            $content->status=1;
            $content->save();
            
            $json['message']="This order has been rejected on ".date("d M Y h:i:s A");
            $json['success']=1;
            echo json_encode($json);
            return;
        }
    }
    
    public function orderConfirmed(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $orderId = $data['orderId'];
            $orderTime = $data['given_time'];
            $time_type = $data['time_type'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $restId = Auth::User('admin')->rest_detail_id;
        $count = FrontOrderStatusMap:: where('order_id',$orderId)->where('status_id',2)->count();
        if($count>0){
            $json['success']=0;
            echo json_encode($json);
            return;
        }else{
            if($time_type == 1){
                $delivery_time = date("Y-m-d h:i:s", strtotime("+$orderTime minutes"));
                $type = UserOrder::where('rest_detail_id','=',$restId)->where('order_id',$orderId)->first();
                $type->rest_given_time=$orderTime;
                $type->front_order_status_id=2;
                $type->delivery_time=$delivery_time;
                $type->updated_by=Auth::User('admin')->id;
                $type->updated_at=date("Y-m-d H:i:s");
                $type->save();
            }else{
                
                $type = UserOrder::where('rest_detail_id','=',$restId)->where('order_id',$orderId)->first();
				$delivery_time = $type->user_time;
                $type->front_order_status_id=2;
				$type->delivery_time=$delivery_time;
                $type->updated_by=Auth::User('admin')->id;
                $type->updated_at=date("Y-m-d H:i:s");
                $type->save();
            }
            $content = new FrontOrderStatusMap;
            $content->created_at=date("Y-m-d H:i:s");
            $content->created_by=Auth::User('admin')->id;
            $content->order_id=$orderId;
            $content->status_id=2;
            $content->updated_by=Auth::User('admin')->id;
            $content->updated_at=date("Y-m-d H:i:s");
            $content->status=1;
            $content->save();
            $json['message']="This order has been confirmed on ".date("d M Y h:i:s A");
            $json['confirmed_time']=date("d M Y h:i:s A");
            if($time_type == 1){
                $json['deadline_time']=date("d M Y h:i:s A", strtotime("+$orderTime minutes"));
            }
            $json['success']=1;
            echo json_encode($json);
            return;
        }
    }

	
	public function printBill($orderId=0)
    {
	    $order_id = $orderId; 
		$order=DB::table('user_orders')->where('order_id',$order_id)->where('status','!=','2')->select('rest_detail_id','front_user_address_id')->get();
		for ($i = 0, $c = count($order); $i < $c; ++$i) {
            $order[$i] = (array) $order[$i];
        }
		
		$rest_id = $order['0']['rest_detail_id'];
        $front_user_address_id = $order['0']['front_user_address_id'];		
        $rest_detail = DB ::table('rest_details')
		->leftjoin('countries','rest_details.country', '=','countries.id')
		->leftjoin('states','rest_details.state', '=','states.id')
		->leftjoin('cities','rest_details.city', '=','cities.id')
		->where('rest_details.id',$rest_id)
		->select('rest_details.f_name','rest_details.l_name','rest_details.add1','rest_details.add2',
		'countries.name as country_name','states.name as state_name','cities.name as city_name','rest_details.pincode')->get();
		
		for ($i = 0, $c = count($rest_detail); $i < $c; ++$i) {
            $rest_detail[$i] = (array) $rest_detail[$i];
        }
		
		$customer_detail = DB ::table('front_user_addresses')
		->leftjoin('countries','front_user_addresses.country_id', '=','countries.id')
		->leftjoin('states','front_user_addresses.state_id', '=','states.id')
		->leftjoin('cities','front_user_addresses.city_id', '=','cities.id')
		->where('front_user_addresses.id',$front_user_address_id)
		->select('front_user_addresses.booking_person_name','front_user_addresses.mobile','front_user_addresses.address','front_user_addresses.landmark',
		'countries.name as country_name','states.name as state_name','cities.name as city_name','front_user_addresses.zipcode')->get();
		for ($i = 0, $c = count($customer_detail); $i < $c; ++$i) {
            $customer_detail[$i] = (array) $customer_detail[$i];
        }
		$item=OrderItem::where('order_id',$order_id)->where('status','!=','2')->select('*')->get();	
		
        $order_payment=DB ::table('order_payments')
		->leftjoin('front_user_details','front_user_details.front_user_id', '=','order_payments.front_user_id')
		->leftjoin('order_services','order_services.id', '=','order_payments.order_type')
		->where('order_payments.order_id',$order_id)->where('order_payments.status','!=','2')
		->select('order_payments.*','front_user_details.fname','front_user_details.lname','order_services.name as order_type_name')->get();
		for ($i = 0, $c = count($order_payment); $i < $c; ++$i) {
            $order_payment[$i] = (array) $order_payment[$i];
        }
	    return view('admin/OrderDetails/printbill')->with('customer_detail',$customer_detail)
		->with('rest_detail',$rest_detail)->with('item',$item)->with('order_payment',$order_payment);
	}

}
