<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use validator;
use App\superadmin\RestDetail;

class TinInvoiceController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $res_owner_id = Auth::User('admin')->id;
		 $ownerinfo = DB::table('rest_owners')->select('rest_detail_id')->where('id','=',$res_owner_id)->get();
		 for ($i = 0, $c = count($ownerinfo); $i < $c; ++$i) {
             $ownerinfo[$i] = (array) $ownerinfo[$i];
             
             }
		 $info = DB::table('rest_details')
            ->where('id','=',$ownerinfo['0']['rest_detail_id'] )
            ->select('id','tin_number')
			->get();
            for ($i = 0, $c = count($info); $i < $c; ++$i) {
             $info[$i] = (array) $info[$i];
             
             }
	   
			 return view('admin.myaccount.view-tin_no', ['info' => $info]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $info = DB::table('rest_details')
            ->where('id','=',$id )
            ->select('id','tin_number')
			->get();
            for ($i = 0, $c = count($info); $i < $c; ++$i) {
             $info[$i] = (array) $info[$i];
             
             }
			$auth_id = Auth::User('admin')->id;
			$username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
			for ($i = 0, $c = count($username); $i < $c; ++$i) {
				$username[$i] = (array) $username[$i];
				
			}
			 return view('admin.myaccount.edit-tin_no', ['info' => $info])->with('username',$username);
			
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->set_validation($id,$request);
        $auth_id = Auth::User('admin')->id; 
        $restdetail=RestDetail::find($id);

        $restdetail->tin_number=$request->tin_no;
        $restdetail->updated_by=$auth_id;
        $restdetail->updated_at=date("Y-m-d H:i:s");
        $restdetail->save();
		
		Session::flash('message','Updated Successfully');
        return redirect(route('rzone.taxinvoicenumber.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function set_validation($id=null,$request)
     {
        $message=array(
            
            "tin_no.required"=>"tin no is required"
           
         );

        $this->validate($request,[
            
            'tin_no'=> 'required'
        ],$message);
     }
}
