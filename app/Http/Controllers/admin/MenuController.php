<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Session;
use DB;
use Input;
use Validator;
use Auth;

use App\superadmin\RestDetail;
use App\superadmin\RestMenu;
use App\superadmin\RestCategory;
use App\superadmin\RestKitchenMap;
use App\superadmin\Spice;

use App\superadmin\MenuPriceMap;
use App\superadmin\RestMenuAllergicMap;
use App\superadmin\RestMenuSpiceMap;

use App\superadmin\RestSubMenu;
use App\superadmin\SubMenuPriceMap;
use App\superadmin\RestSubmenuAllergicMap;
use App\superadmin\RestSubmenuSpiceMap;

class MenuController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $restro_detail_id = Auth::User('admin')->rest_detail_id;
        $data['restro_names'] = RestDetail::where('status','!=',2)->select('id','f_name','l_name')->get();
        $data['getRestroMenus'] = RestMenu::where('status','=',1)->where('have_submenu',0)->where('rest_detail_id',$restro_detail_id)->paginate(20);
        $data['getRestroSubMenus'] = RestSubMenu::where('status','=',1)->where('rest_detail_id',$restro_detail_id)->paginate(20);
        $data['restId'] = $restro_detail_id;
        return view('admin.menu.menu')->with($data);
    }
    
    public function updateStock(Request $request){
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $menuType = $_REQUEST['menuType'];
        $menuId = $_REQUEST['menuId'];
        $outStock = $_REQUEST['outStock'];
        $restro_detail_id = Auth::User('admin')->rest_detail_id;
        if($menuType == 'menu'){
            $type = RestMenu::where('rest_detail_id',$restro_detail_id)->where('id',$menuId)->first();
        }else if($menuType == 'submenu'){
            $type = RestSubMenu::where('rest_detail_id',$restro_detail_id)->where('id',$menuId)->first();
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $type->out_of_stock=$outStock;
        $type->updated_by=Auth::User('admin')->id;
        $type->updated_at=date("Y-m-d H:i:s");
        if($type->save()){
            $json['success']=1;
            echo json_encode($json);
            return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $type = $request->query('type');
        $restId = Auth::User('admin')->rest_detail_id;
        if($id){
            $count=0;
            if( ! preg_match('/^\d+$/', $id) ){
                Session::flash('message', 'Please select menu again.'); 
                return redirect(route('rzone.menu.index'));
            }
            if($type == 'menu'){
                $count= RestMenu:: where('status','=','1')->where('id','=',$id)->where('rest_detail_id',$restId)->count();
            }else if($type == 'submenu'){
                $count= RestSubMenu:: where('status','=','1')->where('id','=',$id)->where('rest_detail_id',$restId)->count();
            }else{
                Session::flash('message', 'Please select menu again.'); 
                return redirect('rzone/menu');
            }
            if($count < 1){
                Session::flash('message', 'Please select menu again.'); 
                return redirect('rzone/menu');
            }
        }
        
        if($type == 'menu'){
            $data['viewDetails'] = RestMenu:: where('status','=','1')->where('id','=',$id)->where('rest_detail_id',$restId)->get();
            return view('admin.menu.view-menu')->with($data);
        }else if($type == 'submenu'){
            $data['viewDetails'] = RestSubMenu:: where('status','=','1')->where('id','=',$id)->where('rest_detail_id',$restId)->get();
            return view('admin.menu.view-submenu')->with($data);
        }else{
            Session::flash('message', 'Please select menu again.'); 
            return redirect('rzone/menu');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
