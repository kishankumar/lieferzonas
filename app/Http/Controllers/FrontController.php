<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\front\FrontUserSocialMap;
use Session;
use Auth;

abstract class FrontController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->user = "front";
        $this->middleware('front');

        if (Session::has('socialLogin')) {
            $socialLogin = Session::get('socialLogin');
            $socialType = Session::get('socialType');
            $social_id = Session::get('social_id');
            if ($socialLogin && $socialLogin != '') {
                $userId = Auth::user('front')->id;
                $type = new FrontUserSocialMap;
                $type->front_user_id = $userId;
                $type->social_type = "$socialType";
                $type->social_id = $social_id;
                $type->created_by = $userId;
                $type->updated_by = $userId;
                $type->created_at = date("Y-m-d H:i:s");
                $type->updated_at = date("Y-m-d H:i:s");
                $type->status = 1;
                $type->save();
            }
            Session::put('socialLogin', '');
            Session::put('socialType', '');
            Session::put('social_id', '');
        }
    }
}
