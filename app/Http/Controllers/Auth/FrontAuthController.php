<?php

namespace App\Http\Controllers\Auth;

use App\front\FrontUser;
use App\superadmin\EmailTemplate;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers; //commented by vikas
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers; //add by vikas
use Illuminate\Support\Facades\Auth;
use DB;
use Session;

use Input;
use App\User;
use App\Front;
use App\front\FrontUserSocialMap;
use App\front\FrontUserDetail;
use App\front\FrontUserAddress;
use App\front\FrontUserEmailVerification;
use Socialite;
use Illuminate\Http\Request;
use App\Http\Requests;

class FrontAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/';
    //protected $loginPath = 'front/login';
    protected $loginPath = '/';
//    protected $redirectAfterLogout = "front/login"; //add by vikas
    protected $redirectAfterLogout = "/";

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = "front"; //add by vikas
        $this->middleware('front.guest', ['except' => 'getLogout']);
    }


    //Start Code for Sociallite login

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function redirectToProviderTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */

    public function handleProviderCallbackTwitter()
    {
        try {
            $user = \Socialite::driver('twitter')->user();
        } catch (Exception $e) {
            return redirect('auth/twitter');
        }
        $authUser = $this->findOrCreateUser($user, 'T');
        if ($authUser == 'socialLogin') {
            $filename = time() . rand(111, 999) . '.jpg';
            $destinationpath = public_path() . "/uploads/front/users/$filename";
            copy($user->avatar, $destinationpath);

            Session::put('social_id', $user->id);
            Session::put('socialLogin', 1);
            Session::put('socialType', 'T');
            Session::put('avatar', $filename);
            return redirect('/');
        }
        Auth::login('front', $authUser, true);
        return redirect('/front/dashboard/');
    }

    public function handleProviderCallbackGoogle()
    {
        try {
            $user = \Socialite::driver('google')->user();
        } catch (Exception $e) {
            return redirect('auth/google');
        }
        $authUser = $this->findOrCreateUser($user, 'G');
        if ($authUser == 'socialLogin') {
            $filename = time() . rand(111, 999) . '.jpg';
            $destinationpath = public_path() . "/uploads/front/users/$filename";
            copy($user->avatar, $destinationpath);

            Session::put('social_id', $user->id);
            Session::put('socialLogin', 1);
            Session::put('socialType', 'G');
            Session::put('avatar', $filename);
            return redirect('/');
        }
        Auth::login('front', $authUser, true);
        return redirect('/front/dashboard/');
    }

    public function handleProviderCallback()
    {
        try {
            $user = \Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return redirect('auth/facebook');
        }
        $authUser = $this->findOrCreateUser($user, 'F');
        if ($authUser == 'socialLogin') {
            $filename = time() . rand(111, 999) . '.jpg';
            $destinationpath = public_path() . "/uploads/front/users/$filename";
            copy($user->avatar, $destinationpath);

            Session::put('social_id', $user->id);
            Session::put('socialLogin', 1);
            Session::put('socialType', 'F');
            Session::put('avatar', $filename);
            return redirect('/');
        }
        Auth::login('front', $authUser, true);
        return redirect('/front/dashboard/');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */

    private function findOrCreateUser($facebookUser, $socialType = 'F')
    {
        $authUser = FrontUserSocialMap::where('social_id', $facebookUser->id)->where('social_type', '=', "$socialType")->where('status', '=', '1')->first();
        if ($authUser) {
            $userId = $authUser->front_user_id;
            $frontUserInfo = Front::where('id', $userId)->first();
            return $frontUserInfo;
        }
        $existUser = Front::where('email', $facebookUser->email)->first();
        if ($existUser) {
            $userId = $existUser->id;
            $type = new FrontUserSocialMap;
            $type->front_user_id = $userId;
            $type->social_type = "$socialType";
            $type->social_id = $facebookUser->id;
            $type->created_by = $userId;
            $type->updated_by = $userId;
            $type->created_at = date("Y-m-d H:i:s");
            $type->updated_at = date("Y-m-d H:i:s");
            $type->status = 1;
            $type->save();

            $frontUserInfo = Front::where('id', $userId)->first();
            return $frontUserInfo;
        }

        if ($facebookUser) {
            return 'socialLogin';
        }

//        $filename=time().rand(111,999).'.jpg';
//        $destinationpath=public_path()."/uploads/front/users/$filename";
//        copy($facebookUser->avatar, $destinationpath);

//        return Front::create([
//            'email' => $facebookUser->email,
//            'password' => $password,
//            'social_login_id' => $facebookUser->id,
//            'social_type'=>'F'
//        ]);
    }

    function createAuthUsers($userId = 0)
    {
        if (Session::has('socialLogin')) {
            $socialLogin = Session::get('socialLogin');
            $socialType = Session::get('socialType');
            $social_id = Session::get('social_id');
            if ($socialLogin && $socialLogin != '') {
                if ($userId) {
                    $type = new FrontUserSocialMap;
                    $type->front_user_id = $userId;
                    $type->social_type = "$socialType";
                    $type->social_id = $social_id;
                    $type->created_by = $userId;
                    $type->updated_by = $userId;
                    $type->created_at = date("Y-m-d H:i:s");
                    $type->updated_at = date("Y-m-d H:i:s");
                    $type->status = 1;
                    $type->save();
                }
                Session::put('socialLogin', '');
                Session::put('socialType', '');
                Session::put('social_id', '');
            }
            return 1;
        } else {
            return 0;
        }
    }

    function postRegister(Request $request)
    {
        $check = Front::where('email', $request->email)->first();
        if ($check) {
            Session::flash("errmessage", "Mit dieser E-Mail ist bereits ein Benutzer registriert!");
            return redirect(url("frontuser/register"));
        }

        DB::beginTransaction();

        try {

            $front = new Front;
            $front->email = $request->email;
            $front->password = bcrypt($request->password);
            $front->save();

            $detail = new FrontUserDetail;
            $destination_path = public_path() . "/uploads/front/users";
            $image = $request->image;
            if ($image) {
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $filename = time() . rand(111, 999) . '.' . $extension; // renameing image
                $image->move($destination_path, $filename);
                $detail->profile_pic = $filename;
            }
            $detail->front_user_id = $front->id;
            $detail->nickname = $request->nickname;
            $detail->email = $request->email;
            $detail->fname = $request->fname;
            $detail->lname = $request->lname;
            $detail->gender = $request->gender;
            $detail->dob = $request->dob;
            $detail->mobile = $request->mobile;
            $detail->registered_via = '1';
            $detail->save();

            $verification_code = Crypt::encrypt($request->email);
            $pending_verification = new FrontUserEmailVerification;
            $pending_verification->front_user_id = $front->id;
            $pending_verification->verification_code = $verification_code;
            $pending_verification->email_id = $request->email;
            $pending_verification->save();

            $template_row = EmailTemplate::where("status", 1)->where("title", "Email Verification")->first();
            if ($template_row) {
                $subject = "Lieferzonas - Email Verification";
                $verification_link = url("/verify?code=" . $verification_code);
                $text = $template_row->text;
                $text = str_replace("%FIRST_NAME%", $request->fname, $text);
                $text = str_replace("%LAST_NAME%", $request->lname, $text);
                $text = str_replace("%USERNAME%", $request->nickname, $text);
                $text = str_replace("%VERIFICATION_LINK%", $verification_link, $text);
                Mail::raw($text, function ($message) use (&$request, &$subject) {
                    $message->to($request->email);
                    $message->subject($subject);
                });
            }
            DB::commit();
            Session::flash("message", "Erfolgreich registriert!");
            return redirect(url("/"));
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash("errmessage", "Es gab einen Fehler bei der Registrierung!");
            return redirect(url("/"));
        }

    }

    function registeruser(Request $request)
    {
        if ($request->ajax()) {
            $data = Input::all();
        } else {
            $json['status'] = 4;
            echo json_encode($json);
            return;
        }
        $check = Front::where('email', $request->email)->first();
        if (count($check)) {
            $json['status'] = 2;
            echo json_encode($json);
            return;
        } else {
            $reg = new Front;
            //$reg->firstname = $request->fname;
            //$reg->lastname = $request->lname;
            $reg->email = $request->email;
            $reg->password = bcrypt($request->password);
            $reg->save();

            $regdetail = new FrontUserDetail;
            $regdetail->front_user_id = $reg->id;
            $regdetail->email = $request->email;
            $regdetail->fname = $request->fname;
            $regdetail->lname = $request->lname;
            $regdetail->gender = $request->gender;
            if (Session::has('avatar')) {
                $avatar = Session::get('avatar');
                if ($avatar && $avatar != '') {
                    $regdetail->profile_pic = $avatar;
                    Session::put('avatar', '');
                }
            }
            $regdetail->registered_via = '1';
            if ($regdetail->save()) {
                $email = $request->email;
                $ver_code = preg_replace('/[^A-Za-z0-9\-]/', '', base64_encode($email));
                $ver_code = $ver_code . time();
                $ver_link = url("register/verifyemail/" . $ver_code);

                $save_link = new FrontUserEmailVerification;
                $save_link->front_user_id = $reg->id;
                $save_link->verification_code = $ver_code;
                $save_link->email_id = $email;

                if ($save_link->save()) {
                    if (Session::has('socialLogin')) {
                        $authUser = $this->createAuthUsers($reg->id);
                        if ($authUser) {
                            $frontUserInfo = Front::where('id', $reg->id)->first();
                            Auth::login('front', $frontUserInfo, true);
                            $ver_link = url("front/dashboard");
                            $json['status'] = 3;
                            $json['redirecturl'] = $ver_link;
                            echo json_encode($json);
                            return;
                        } else {
                            $json['status'] = 4;
                            echo json_encode($json);
                            return;
                        }
                    } else {
                        $subject = "Email verification mail from Lieferzonas.";
                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                        $headers .= 'From: donotreply@lieferzonas.at' . "\r\n";
                        $msg = "Hello, <br> You are now successfully registered with lieferzonas.";
                        if (mail($email, $subject, $msg, $headers)) {
                            $json['status'] = 1;
                            echo json_encode($json);
                            return;
                        } else {
                            $json['status'] = 4;
                            echo json_encode($json);
                            return;
                        }
                    }
                } else {
                    $json['status'] = 4;
                    echo json_encode($json);
                    return;
                }
            } else {
                $json['status'] = 4;
                echo json_encode($json);
                return;
            }
        }
    }

    function registerCartUser(Request $request)
    {

        $message = array(
            "fisrt_name.required" => "First Name is required",
            "email.required" => "Email is required.",
            "email.email" => "Enter valid email",
            "email.value_exists" => "Email is already exist",
            "password.required" => "Password is required",
            "phone.required" => "mobile is required",
            "address.required" => "Adress is required",

            "zipcode.required" => "Zipcode is required",

        );

        $this->validate($request, [
            'fisrt_name' => 'required|min:2|alpha',
            'email' => 'required|email|value_exists',
            'password' => 'required',
            'phone' => 'required',
            'address' => 'required',

            'zipcode' => 'required'

        ], $message);


        $reg = new Front;

        $reg->email = $request->email;
        $reg->password = bcrypt($request->password);
        $reg->save();

        $regdetail = new FrontUserDetail;
        $regdetail->front_user_id = $reg->id;
        $regdetail->email = $request->email;
        $regdetail->fname = $request->fisrt_name;
        $regdetail->lname = $request->last_name;
        $regdetail->mobile = $request->phone;
        $regdetail->registered_via = '1';
        $regdetail->save();


        $regaddress = new FrontUserAddress();
        $regaddress->front_user_id = $reg->id;
        $regaddress->booking_person_name = $request->fisrt_name . ' ' . $request->last_name;
        $regaddress->mobile = $request->phone;
        $regaddress->address = $request->address;
        $regaddress->landmark = $request->landmark;
        $regaddress->zipcode = $request->zipcode;


        if ($regaddress->save()) {
            $email = $request->email;
            $ver_code = preg_replace('/[^A-Za-z0-9\-]/', '', base64_encode($email));
            $ver_code = $ver_code . time();
            $ver_link = url("register/verifyemail/" . $ver_code);

            $save_link = new FrontUserEmailVerification;
            $save_link->front_user_id = $reg->id;
            $save_link->verification_code = $ver_code;
            $save_link->email_id = $email;

            if ($save_link->save()) {
                if (Session::has('socialLogin')) {
                    $authUser = $this->createAuthUsers($reg->id);
                    if ($authUser) {
                        $frontUserInfo = Front::where('id', $reg->id)->first();
                        Auth::login('front', $frontUserInfo, true);
                        $ver_link = url("front/dashboard");
                        $json['status'] = 3;
                        $json['redirecturl'] = $ver_link;
                        echo json_encode($json);
                        return;
                    } else {
                        $json['status'] = 4;
                        echo json_encode($json);
                        return;
                    }
                } else {
                    $subject = "Email verification mail from Lieferzonas.";
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                    $headers .= 'From: donotreply@lieferzonas.at' . "\r\n";
                    $msg = "Please click on the below link to verify your email. <br> <a href='" . $ver_link . "'>Verify Email</a>";
                    mail($email, $subject, $msg, $headers);
                    $frontUserInfo = Front::where('id', $reg->id)->first();
                    if ($frontUserInfo) {
                        Auth::login('front', $frontUserInfo, true);
                        return redirect(url('restaurant/' . $request->restId . '/shipping-area'));

                    } else {
                        Session::flash('errmessage', 'Oops something went wrong!');
                        return redirect(url('restaurant/' . $request->restId . '/review-order'));
                    }
                }
            } else {
                Session::flash('errmessage', 'Oops something went wrong!');
                return redirect(url('restaurant/' . $request->restId . '/review-order'));
            }
        } else {
            Session::flash('errmessage', 'Oops something went wrong!');
            return redirect(url('restaurant/' . $request->restId . '/review-order'));
        }

    }

    //End Code for Sociallite login


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function getLogout()
    {
        if (\Auth::check('front')) {
            DB::table('front_user_login_logs')->insert(
                ['front_user_id' => Auth::User('front')->id, 'type' => 'O', 'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'time' => date("Y-m-d H:i:s"), 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"),
                    'created_by' => Auth::User('front')->id, 'updated_by' => Auth::User('front')->id, 'status' => '1'
                ]
            );
        }
        Auth::logout($this->user());
//        return redirect('front/login');
        return redirect('/');
    }

    public function newRegisterAjax(Request $request)
    {
        if (Auth::check("front")) return response()->json([
            "success" => 0,
            "reason" => "Du bist bereits eingeloggt. Bitte logge dich zuerst aus um einen neuen Account anzulegen!"
        ]);
        try {
            FrontUser::registerByInput(
                $request->input("email"),
                $request->input("first-name"),
                $request->input("last-name"),
                $request->input("password"),
                $request->input("phone"),
                $request->input("dob")
            );
            return response()->json([
                "success" => 1
            ]);
        }
        catch(\Exception $e) {
            return response()->json([
                "success" => 0,
                "reason" => $e->getMessage()
            ]);
        }
    }

}
