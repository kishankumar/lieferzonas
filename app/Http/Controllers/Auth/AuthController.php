<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers; //commented by vikas
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers; //add by vikas
use Auth;
use DB;
use Session;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    
   // protected $redirectPath = 'root/dashboard';
    protected $loginPath = 'superadmin/login';
    protected $redirectAfterLogout = "superadmin/login"; //add by vikas

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = "user"; //add by vikas
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    protected function getLogout()
    {
        if (\Auth::check('user')) {
            DB::table('superadmin_login_logs')->insert(
                ['user_id' =>Auth::User()->id,'type'=>'O','ip_address'=>$_SERVER['REMOTE_ADDR'],
                    'time'=> date("Y-m-d H:i:s"),'created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s"),
                    'created_by'=>Auth::User()->id,'updated_by'=> Auth::User()->id,'status'=>'1'
                ]
            );
        }
        Auth::logout($this->user());
        //Session::flush();
        return redirect('superadmin/login');
    }
}
