<?php

namespace App\Http\Controllers\front;

use App\front\FrontUserDetail;
use App\Http\Controllers\FrontController;
use App\library\Smsapi;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class SendInviteController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendSms(Request $request) {
        $user = FrontUserDetail::userdetail();
        $number = $request->input("number");
        $smsObj = new Smsapi();
        $message='Hallo! ' . $user->fname . ' ' . $user->lname . ' hat dich dazu eingeladen Essen auf Lieferzonas.at zu bestellen!';
        $sms = $smsObj->sendsms_api($number,$message);
        Session::flash("message", "Erfolgreich gesendet!");
        return redirect("/");
    }

    public function sendEmail(Request $request) {
        $address = $request->input("address");
        $user = FrontUserDetail::userdetail();
        Mail::raw('Hallo! ' . $user->fname . ' ' . $user->lname . ' hat dich dazu eingeladen Essen auf Lieferzonas.at zu bestellen!', function($message) use ($address) {
            $message->to($address, $name = null);
            $message->subject("Einladung zu Lieferzonas.at");
        });
        Session::flash("message", "Erfolgreich gesendet!");
        return redirect("/");
    }
}
