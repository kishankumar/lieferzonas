<?php

namespace App\Http\Controllers\front;

use App\front\FrontOrderStatusMap;
use App\front\FrontUser;
use App\front\FrontUserAddress;
use App\front\FrontUserDetail;
use App\front\FrontUserSubscription;
use App\front\OrderDiscount;
use App\front\OrderItem;
use App\front\OrderItemExtra;
use App\front\OrderPayment;
use App\front\UserBonusPoint;
use App\front\UserCashbackPoint;
use App\front\UserOrder;
use App\Http\Controllers\DefaultController;
use App\superadmin\ExtraChoice;
use App\superadmin\ExtraChoiceElement;
use App\superadmin\RestDeliveryCost;
use App\superadmin\RestDetail;
use App\superadmin\RestSubMenu;
use App\superadmin\SubMenuPriceMap;
use App\superadmin\SuperAdminSetting;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class NewCartController extends DefaultController
{

    protected static $cart_array_key = "cart_array_key";

    protected function getRestaurantCart($restaurant_id) {
        if (!Session::has(self::$cart_array_key)) {
            Session::put(self::$cart_array_key, array());
        }
        $cart_array = Session::get(self::$cart_array_key);
        if (!array_key_exists($restaurant_id, $cart_array)) {
            $cart_array[$restaurant_id] = array();
        }
        $cart = $cart_array[$restaurant_id];
        return $cart;
    }

    protected function setRestaurantCart($restaurant_id, $new_cart)
    {
        if (!Session::has(self::$cart_array_key)) {
            Session::put(self::$cart_array_key, array());
        }
        $cart_array = Session::get(self::$cart_array_key);
        $cart_array[$restaurant_id] = $new_cart;
        Session::put(self::$cart_array_key, $cart_array);
    }

    protected function hasCartElementErrors($submenu, $extras) {
        if (!RestSubMenu::find($submenu)) return "Gericht existiert nicht";
        $cart_element["extras"] = $extras;
        if (is_array($extras)) {
            foreach($extras as $extra_choice => $extra_choice_selection) {
                $extra_choice_row = ExtraChoice::find($extra_choice);
                if (!$extra_choice_row) return "Ungültiges Extra ausgewählt";
                if (is_array($extra_choice_selection)) {
                    if ($extra_choice_row->type != "multiple-choice") return "Ungültige Extra-Kardinalität";
                    $extra_choice_element_rows = ExtraChoiceElement::find($extra_choice_selection);
                    foreach ($extra_choice_element_rows as $extra_choice_element_row) {
                        if ($extra_choice_element_row->extra_choice_id != $extra_choice) return "Ungültiges Extra-Element ausgewählt";
                    }
                }
                else {
                    if ($extra_choice_row->type != "single-choice") return "Ungültige Extra-Kardinalität";
                    $extra_choice_element_row = ExtraChoiceElement::find($extra_choice_selection);
                    if ($extra_choice_element_row->extra_choice_id != $extra_choice) return "Ungültiges Extra-Element ausgewählt";
                }
            }
        }
    }

    protected function clearCart($restaurant_id) {
        $this->setRestaurantCart($restaurant_id, array());
    }

    protected function addToItemCount($restaurant_id, $index, $amount) {
        $cart = $this->getRestaurantCart($restaurant_id);
        $cart[$index]["amount"] += $amount;
        if ($cart[$index]["amount"] <= 0) {
            unset($cart[$index]);
            $cart = array_values($cart);
        }
        $this->setRestaurantCart($restaurant_id, $cart);
    }

    protected function setItemCount($restaurant_id, $index, $amount) {
        $cart = $this->getRestaurantCart($restaurant_id);
        $cart[$index]["amount"] = $amount;
        if ($cart[$index]["amount"] <= 0) {
            unset($cart[$index]);
            $cart = array_values($cart);
        }
        $this->setRestaurantCart($restaurant_id, $cart);
    }

    protected function removeItem($restaurant_id, $index) {
        $cart = $this->getRestaurantCart($restaurant_id);
        unset($cart[$index]);
        $cart = array_values($cart);
        $this->setRestaurantCart($restaurant_id, $cart);
    }

    protected function addItem($restaurant_id, $submenu, $extras) {
        $cart = $this->getRestaurantCart($restaurant_id);
        foreach ($cart as $key => $cart_element) {
            if ($cart_element["submenu"] != $submenu) {
                continue;
            }
            if ($extras != $cart_element["extras"]) {
                continue;
            }
            $cart[$key]["amount"] += 1;
            $this->setRestaurantCart($restaurant_id, $cart);
            return;
        }
        $error = $this->hasCartElementErrors($submenu, $extras);
        if ($error) {
            return error;
        }
        $cart_element = array();
        $cart_element["amount"] = 1;
        $cart_element["submenu"] = $submenu;
        if ($extras) {
            $cart_element["extras"] = $extras;
        }
        else {
            $cart_element["extras"] = array();
        }
        array_push($cart, $cart_element);
        $this->setRestaurantCart($restaurant_id, $cart);
    }

    protected function getCartElementSinglePrice($cart_element) {
        $sum = 0;
        $submenu_price_row = RestSubMenu::getSubMenuCurrentPrice($cart_element["submenu"]);
        if (!$submenu_price_row) {
            return false;
        }
        $sum += $submenu_price_row->price;
        foreach($cart_element["extras"] as $extra) {
            if (is_array($extra)) {
                foreach($extra as $element) {
                    $extra_choice_element = ExtraChoiceElement::find($element);
                    $sum += $extra_choice_element->price;
                }
            }
            else {
                $extra_choice_element = ExtraChoiceElement::find($extra);
                $sum += $extra_choice_element->price;
            }
        }
        return $sum;
    }

    protected function getCartElementPrice($cart_element) {
        $single = $this->getCartElementSinglePrice($cart_element);
        if (!$single) {
            return false;
        }
        $sum = $single * $cart_element["amount"];
        return $sum;
    }

    protected function getCartPriceSum($cart) {
        $sum = 0;
        foreach ($cart as $key => $cart_element) {
            $element_price = $this->getCartElementPrice($cart_element);
            if (!$element_price) return false;
            $sum += $element_price;
        }
        return $sum;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cart = $this->getRestaurantCart($id);
        foreach($cart as $key => $cart_element) {
            $submenu_row = RestSubMenu::find($cart_element["submenu"]);
            $submenu_price = RestSubMenu::getSubMenuCurrentPrice($submenu_row->id);
            $cart_element["submenu_display"] = [
                "name" => $submenu_row->name,
                "price" => ($submenu_price ? $submenu_price->price : null)
            ];
            $cart_element["extras_display"] = array();
            foreach ($cart_element["extras"] as $extra_choice => $extra_choice_selection) {
                if (is_array($extra_choice_selection)) {
                    foreach($extra_choice_selection as $element) {
                        $extra_choice_element_row = ExtraChoiceElement::find($element);
                        $extra_display = [
                            "name" => $extra_choice_element_row->name,
                            "price" => $extra_choice_element_row->price
                        ];
                        array_push($cart_element["extras_display"], $extra_display);
                    }
                }
                else {
                    $extra_choice_element_row = ExtraChoiceElement::find($extra_choice_selection);
                    $extra_display = [
                        "name" => $extra_choice_element_row->name,
                        "price" => $extra_choice_element_row->price
                    ];
                    array_push($cart_element["extras_display"], $extra_display);
                }
            }
            $cart_element["display_single_price"] = $this->getCartElementSinglePrice($cart_element);
            $cart_element["display_total_price"] = $this->getCartElementPrice($cart_element);
            $cart[$key] = $cart_element;
        }
        $data = array();
        $data["items"] = $cart;
        $cart_sum = $this->getCartPriceSum($cart);
        if ($cart_sum) {
            $data["cart_sum"] = $cart_sum;
        }
        $delivery_costs = RestDeliveryCost::getRestaurantDeliveryCostRow($id);
        if ($cart_sum < $delivery_costs->minimum_order_amount) {
            $data["minimum_order_amount"] = $delivery_costs->minimum_order_amount;
            if ($delivery_costs->below_order_delivery_cost) {
                $data["below_order_delivery_cost"] = $delivery_costs->below_order_delivery_cost;
                $data["cart_total"] = $cart_sum + $delivery_costs->below_order_delivery_cost;
            }
            else {
                $data["missing_to_minimum"] = $delivery_costs->minimum_order_amount - $cart_sum;
                if ($delivery_costs->above_order_delivery_cost > 0) {
                    $data["delivery_cost"] = $delivery_costs->above_order_delivery_cost;
                    $data["cart_total"] = $delivery_costs->minimum_order_amount + $delivery_costs->above_order_delivery_cost;
                }
                else {
                    $data["cart_total"] = $delivery_costs->minimum_order_amount;
                }
            }
        }
        else {
            $data["delivery_cost"] = $delivery_costs->above_order_delivery_cost;
            $data["cart_total"] = $cart_sum + $delivery_costs->above_order_delivery_cost;
        }
        return json_encode($data);
    }

    public function getJson($id) {
        return response()->json([
            "status" => "success",
            "cart_json" => $this->show($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $operation = $request->input("operation");
        if ($operation == "clear") {
            $error = $this->clearCart($id);
            if ($error) {
                return response()->json([
                    "status" => "failure",
                    "reason" => $error
                ]);
            }
        }
        else if($operation == "add_count") {
            $error = $this->addToItemCount($id, $request->input("item_index"), $request->input("amount"));
            if ($error) {
                return response()->json([
                    "status" => "failure",
                    "reason" => $error
                ]);
            }
        }
        else if($operation == "set_count") {
            $error = $this->setItemCount($id, $request->input("item_index"), $request->input("amount"));
            if ($error) {
                return response()->json([
                    "status" => "failure",
                    "reason" => $error
                ]);
            }
        }
        else if($operation == "remove") {
            $error = $this->removeItem($id, $request->input("item_index"));
            if ($error) {
                return response()->json([
                    "status" => "failure",
                    "reason" => $error
                ]);
            }
        }
        else if($operation == "add") {
            $error = $this->addItem($id, $request->input("submenu"), $request->input("extras"));
            if ($error) {
                return response()->json([
                    "status" => "failure",
                    "reason" => $error
                ]);
            }
        }
        else {
            return response()->json([
                "status" => "failure",
                "reason" => "Ungültige Operation!"
            ]);
        }
        return response()->json([
            "status" => "success",
            "cart_json" => $this->show($id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkout($id) {
        $cart = $this->getRestaurantCart($id);
        if (count($cart) <= 0) {
            Session::flash("errmessage", "Füge deinem Warenkorb erst etwas zu!");
            return redirect(url("restaurant/$id/details"));
        }
        if (Auth::check("front")) {
            return view("front_new.restaurants.checkout_user")->with("restaurant_id", $id);
        }
        else {
            return view("front_new.restaurants.checkout")->with("restaurant_id", $id);
        }
    }

    protected static function parseBoolean($value) {
        if ($value == "true") return true;
        else return false;
    }

    public function place_order(Request $request) {

        $restaurant_id = intval($request->input("restaurant_id"));

        $order_type = $request->input("order_type");

        if ($order_type != '1' && $order_type != '2') {
            Session::flash("errmessage", "Bitte Zustellung oder Abholung auswählen!");
            return redirect(url("checkout/" . $restaurant_id));
        }

        $cart = $this->getRestaurantCart($restaurant_id);

        if (count($cart) == 0) {
            Session::flash("errmessage", "Der Warenkorb enthält keine Gerichte!");
            return redirect(url("checkout/" . $restaurant_id));
        }

        $totalCartAmount = 0;
        foreach ($cart as $cart_element) {
            $cart_element_price = self::getCartElementPrice($cart_element, $order_type);
            if (!$cart_element_price) {
                Session::flash("errmessage", "Eines der Gerichte kann nicht zugestellt/abgeholt werden!");
                return redirect(url("checkout/" . $restaurant_id));
            }
            $totalCartAmount = bcadd($totalCartAmount, bcmul($cart_element_price, $cart_element["amount"], 2), 2);
        }

        $user_id = null;
        $address_id = null;

        if (!Auth::check('front')) {
            $return = FrontUser::registerGuest($request->email, $request);
            $user_id = $return["user_id"];
            $address_id = $return["address_id"];
        }
        else {
            $user_id = Auth::user('front')->id;
            $address_id = $request->input("address");
        }


        $address = FrontUserAddress::find($address_id);

        if (!$address and $order_type == '1') {
            Session::flash("errmessage", "Bitte wählen Sie eine gültige Adresse!");
            return redirect(url("checkout/" . $restaurant_id));
        }

        /* Time */

        $orderDateTime = $request->order_date_time;

        if ($orderDateTime == 2) {
            $deliveryDate = $request->delivery_date;
            $deliveryTime = date("H:i", strtotime($request->delivery_time));
            $userDateTime = $deliveryDate . ' ' . $deliveryTime . ':00';
        } else {
            $userDateTime = '';
        }

        /* Discounts */

        $bonus = self::parseBoolean($request->bonus_point);
        $cashback = self::parseBoolean($request->cashback_point);
        $discount = $request->offer_type;

        $couponCode = $request->coupon_code;
        $userId = $user_id;
        $currDate = date('Y-m-d H:i:s');
        $paymentMethod = $request->payment_method;
        $discountPriceType = 'C';

        $discount_count = 0;
        if ($bonus) $discount_count++;
        if ($cashback) $discount_count++;
        if ($discount == 'have_coupon') $discount_count++;
        if ($discount_count > 1) {
            Session::flash('errmessage', 'Es kann nur eine Vergünstigung gleichzeitig verwendet werden');
            return redirect(url("checkout/" . $restaurant_id));
        }

        $cashbackPoints = UserCashbackPoint::availablepoint($userId);
        $bonusPoints = UserBonusPoint::availablepoint($userId);

        $userPointsDetail = SuperAdminSetting::where('status', 1)->select('cashback_point_value', 'bonus_point_value')->get();

        if (count($userPointsDetail) > 0) {
            $cashbackvalue = $userPointsDetail[0]->cashback_point_value;
            $bonusPointValue = $userPointsDetail[0]->bonus_point_value;
        } else {
            $cashbackvalue = 100;
            $bonusPointValue = 100;
        }

        if ($bonus) {
            $discountAmt = $bonusPoints / $bonusPointValue;
            $totalAmt = $totalCartAmount - $discountAmt;
            $discountType = 'bonusPoint';
            $discountVal = '';
            $couponCode = '';
        } elseif ($cashback) {
            $discountAmt = $cashbackPoints / $cashbackvalue;
            $totalAmt = $totalCartAmount - $discountAmt;
            $discountType = 'cashbackPoint';
            $discountVal = '';
            $couponCode = '';
        } elseif ($discount == 'have_coupon' or $couponCode != '') {
            $TotalOrder = OrderPayment::where('front_user_id', $userId)->count();
            $procodeDetails = Promocode:: where('status', 1)->where('valid_to', '>', $currDate)->where('coupon_code', $couponCode)->get();
            $count = count($procodeDetails);
            if ($count < 1) {
                Session::flash('errmessage', 'Ungültiger Coupon');
                return redirect(url('checkout/' . $restaurant_id));
            }
            $discountType = 'couponCode';
            $discountPriceType = $procodeDetails[0]->payment_mode;
            $discountVal = $procodeDetails[0]->payment_amount;
            $couponCode = $couponCode;
            $promoCodeId = $procodeDetails[0]->id;
            $promoMinAmount = $procodeDetails[0]->min_amount;
            $paymentMode = $procodeDetails[0]->payment_mode;
            $paymentAmount = $procodeDetails[0]->payment_amount;

            if ($paymentMode == 'P') {
                $discountAmt = $totalCartAmount * $paymentAmount / 100;
                $totalAmt = $totalCartAmount - $totalCartAmount * $paymentAmount / 100;
            } else {
                $discountAmt = $paymentAmount;
                $totalAmt = $totalCartAmount - $paymentAmount;
            }

            $isNewCustomer = $procodeDetails[0]->is_new_customer;
            if ($isNewCustomer == 1 and $TotalOrder > 0) {
                Session::flash('errmessage', 'Dieser Coupon ist nur für Neu-Kunden!');
                return redirect(url("checkout/" . $restaurant_id));

            } else {
                if ($totalCartAmount < $promoMinAmount) {
                    Session::flash('errmessage', 'Dieser Coupon ist nur für Bestellungen über € ' . $promoMinAmount . ' gültig!');
                    return redirect(url("checkout/" . $restaurant_id));
                }
                $isUserSpecific = $procodeDetails[0]->is_user_specific;
                $isResturantSpecific = $procodeDetails[0]->is_rest_specific;
                if ($isResturantSpecific == 1) {
                    $promoRestDetails = PromoRestMap:: where('promo_id', $promoCodeId)->select('rest_id')->get();
                    $existRest = 0;
                    foreach ($promoRestDetails as $promoRestDetail) {
                        if ($promoRestDetail->rest_id == $restaurant_id) {
                            $existRest = 1;
                            break;
                        }
                    }
                    if ($existRest == 0) {
                        Session::flash('errmessage', 'Dieser Coupon ist für dieses Restaurant nicht gültig!');
                        return redirect(url("checkout/" . $restaurant_id));
                    }
                }
                if ($isUserSpecific == 1) {
                    $promoUserDetails = PromoUserMap::where('promo_id', $promoCodeId)->select('user_id')->get();
                    $existUser = 0;
                    foreach ($promoUserDetails as $promoUserDetail) {
                        if ($promoUserDetail->user_id == $userId) {
                            $existUser = 1;
                            break;
                        }
                    }
                    if ($existUser == 0) {
                        Session::flash('errmessage', 'Ungültiger Coupon');
                        return redirect(url("checkout/" . $restaurant_id));
                    }
                }
            }
        } else {
            $discountType = 'noDiscount';
            $totalAmt = $totalCartAmount;
            $discountAmt = '';
            $couponCode = '';
        }

        $strid = @date("y-m-d H:i:s");
        list($idate, $time) = explode(" ", $strid);
        list($year, $month, $day) = explode("-", $idate);
        list($hour, $min, $sec) = explode(":", $time);

        $yearCal = 20;
        $newYear = $year + $yearCal;
        $monthCal = 26;
        $newmonth = $month + $monthCal;
        $hourCal = 28;
        $newHour = $hour + $hourCal;

        $int = rand(0, 25);
        $a_z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $rand_letter = $a_z[$int];

        $tkordNO = $newYear . $newmonth . $day . $newHour . $min . $sec;
        $tkordRCH = $rand_letter;

        $orderId = "" . $tkordNO . "" . $tkordRCH . "R" . $restaurant_id;

        $userSubscription = $request->subscription;
        // $totalAmt = 0;
        DB::beginTransaction();

        foreach($cart as $cart_element) {
            $orderItem = new OrderItem;
            $orderItem->order_id = $orderId;
            $orderItem->rest_detail_id = $restaurant_id;
            $orderItem->front_user_id = $user_id;
            $orderItem->item_id = $cart_element["submenu"];
            $orderItem->item_name = RestSubMenu::find($cart_element["submenu"])->name;
            $orderItem->item_qty = $cart_element["amount"];
            $orderItem->item_price = bcmul(self::getCartElementPrice($cart_element, $order_type), $cart_element["amount"], 2);
            $orderItem->item_type = "submenu";
            $orderItem->save();
            if (!$cart_element["extras"]) continue;
            foreach ($cart_element["extras"] as $extra_choice => $extra_choice_element) {
                if (is_array($extra_choice_element)) {
                    foreach($extra_choice_element as $single_extra_choice_element) {
                        $extra = new OrderItemExtra;
                        $extra->order_item_id = $orderItem->id;
                        $extra->extra_choice_id = $extra_choice;
                        $extra->extra_choice_element_id = $single_extra_choice_element;
                        $extra->save();
                    }
                }
                else {
                    $extra = new OrderItemExtra;
                    $extra->order_item_id = $orderItem->id;
                    $extra->extra_choice_id = $extra_choice;
                    $extra->extra_choice_element_id = $extra_choice_element;
                    $extra->save();
                }
            }
        }


        $userOrder = new UserOrder;

        $userOrder->order_id = $orderId;
        $userOrder->front_user_id = $user_id;
        $userOrder->front_user_address_id = $address_id;
        $userOrder->rest_detail_id = $restaurant_id;
        $userOrder->total_amount = $totalAmt;
        $userOrder->grand_total = $totalAmt;
        $userOrder->user_time = $userDateTime;
        $userOrder->time_type = $orderDateTime;
        $userOrder->created_by = $user_id;
        $userOrder->updated_by = $user_id;
        $userOrder->order_type = $order_type;

        $userOrder->save();

        $userOrderId = $userOrder['id'];


        $orderStatus = new FrontOrderStatusMap();
        $orderStatus->order_id = $orderId;
        $orderStatus->status_id = 1;
        $orderStatus->created_by = $user_id;
        $orderStatus->updated_by = $user_id;
        $orderStatus->save();

        $orderPayment = new OrderPayment();
        $orderPayment->front_user_id = $user_id;
        $orderPayment->order_id = $orderId;
        $orderPayment->rest_detail_id = $restaurant_id;
        $orderPayment->total_amount = $totalAmt;
        $orderPayment->grand_total = $totalAmt;
        $orderPayment->payment_method = $paymentMethod;
        $orderPayment->order_type = $order_type;
        $orderPayment->discount_amount = $discountAmt;
        $orderPayment->save();

        $orderPaymentId = $orderPayment['id'];

        if ($discountType != 'noDiscount') {
            $orderDiscount = new OrderDiscount();
            $orderDiscount->front_user_id = $user_id;
            $orderDiscount->payment_id = $orderPaymentId;
            $orderDiscount->order_id = $orderId;
            $orderDiscount->rest_detail_id = $restaurant_id;
            $orderDiscount->discount_type = $discountType;
            $orderDiscount->coupon_code = $couponCode;
            $orderDiscount->discount_price_type = $discountPriceType;
            $orderDiscount->discount_val = $discountVal;
            $orderDiscount->discount_amount = $discountAmt;
            $orderDiscount->save();
        }

        if ($userSubscription) {
            $frontUserDetail = FrontUserDetail::where('front_user_id', $user_id)->where('status', 1)->select('fname', 'lname', 'email')->get();
            if (count($frontUserDetail) > 0) {
                $userEmail = $frontUserDetail[0]->email;
                $userName = $frontUserDetail[0]->fname . '' . $frontUserDetail[0]->lname;
            } else {
                $userEmail = '';
            }

            $getSubscriptionDetail = FrontUserSubscription::where('front_user_id', $user_id)->where('email', $userEmail)->select('id', 'status')->get();
            if (count($getSubscriptionDetail) > 0) {
                $status = $getSubscriptionDetail[0]->status;

                if ($status == 0) {
                    $updateQuery = DB::table('front_user_subscriptions')
                        ->where('front_user_id', $user_id)
                        ->where('email', $userEmail)
                        ->update(['status' => 1]);
                }

            } else {
                $userSubscription = new FrontUserSubscription();
                $userSubscription->front_user_id = $user_id;
                $userSubscription->name = $userName;
                $userSubscription->email = $userEmail;
                $userSubscription->status = 1;
                $userSubscription->created_by = $user_id;
                $userSubscription->updated_by = $user_id;
                $userSubscription->save();
            }
        }

        if ($orderPaymentId) {
            DB::commit();
            if ($paymentMethod == 1) {
                $this->clearCart($restaurant_id);
                return redirect(url('confirm-order/' . $userOrderId));
            } else {
                $data['paymentAmount'] = $totalAmt * 100;
                $data['OrderId'] = $orderId;
                $this->clearCart($restaurant_id);
                return view('front.restaurantMenu.front-order-form')->with($data);
            }


        } else {
            DB::rollBack();
            Session::flash('errmessage', 'Die Bestellung konnte nicht abgeschlossen werden');
            return redirect(url("checkout/" . $restaurant_id));
        }

    }

}
