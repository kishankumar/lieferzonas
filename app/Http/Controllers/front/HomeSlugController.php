<?php

namespace App\Http\Controllers\front;

use App\superadmin\FrontService;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\DefaultController;
use Session;
use DB;
use Input;
use Validator;
use Auth;
use App\superadmin\AdditionalHomePageSlug;
use App\superadmin\FrontCityMap;
use App\superadmin\FrontCountryMap;
use App\superadmin\FrontStateMap;

class HomeSlugController extends DefaultController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    function getSlugDetails(Request $request){
        $slug = $request->slug;
        $count= AdditionalHomePageSlug:: where('slug','=',$slug)->where('status','=','1')->count();
        if($count < 1){
            return redirect('/');
        }
        $data['slugDetails'] = AdditionalHomePageSlug ::where('slug','=',$slug)->where('status','=','1')->first();
        //echo '<pre>';print_r($data['slugDetails']);die;
        return view('front/PageSlugData/index')->with($data);
    }

    public function showServiceSlug($id) {
        $service = FrontService::find($id);
        return view('front_new.slugs.service_slug')->with("service", $service);
    }

}
