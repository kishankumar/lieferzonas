<?php

namespace App\Http\Controllers\front;
date_default_timezone_set('Asia/Calcutta'); 
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\OrderItem;
use App\front\UserOrder;
use App\front\OrderPayment;
use App\front\FrontOrderStatusMap;
class OrderController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$auth_id = Auth::user('front')->id;
		$userorder= DB ::table('user_orders')
		->leftjoin('rest_details',  'user_orders.rest_detail_id', '=','rest_details.id')
		->leftjoin('front_user_addresses',  'user_orders.front_user_address_id', '=','front_user_addresses.id')
		->leftjoin('countries','front_user_addresses.country_id', '=','countries.id')
		->leftjoin('states','front_user_addresses.state_id', '=','states.id')
		->leftjoin('cities','front_user_addresses.city_id', '=','cities.id')
		->orderby('user_orders.created_at','desc')
		->where('user_orders.front_user_id',$auth_id)
		->where('user_orders.status',1)
		->select('rest_details.logo','rest_details.image','rest_details.f_name','rest_details.l_name','front_user_addresses.booking_person_name','front_user_addresses.mobile','front_user_addresses.zipcode','front_user_addresses.address','front_user_addresses.landmark','user_orders.*','countries.name as country_name','states.name as state_name','cities.name as city_name')
		->paginate(5);
		
		// return view('front/useraccount/order')->with('userorder',$userorder);
        return view('front_new.user_profile.orders')->with('userorder',$userorder);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function trackorder($orderId=0)
    {
		$order_id = $orderId;
        $userInfo = UserOrder::where('order_id',$orderId)->select('front_user_id')->get();
        if (count($userInfo)>0) {
            $userId=$userInfo[0]->front_user_id;
        }
        else{
           $userId=''; 
        }
        if ($userId==Auth::user('front')->id) {
           
        
		$order_status = DB::table('front_order_statuses')
		
		->where('status','=',1)
		->select('status_name','id')
		->get();
		
		$track=FrontOrderStatusMap::where('order_id',$order_id)->where('status','=',1)->lists('status_id');		
        $track = $track->toArray();
		//print_r($track); die;
		
		return view('front/useraccount/track_order')->with('order_track',$track)->with('order_id',$order_id)->with('order_status',$order_status);
        }
        else{
            return redirect('front/dashboard');
        }
	}
	
	public function printBill($orderId=0)
    {
	    $order_id = $orderId;
		$order=DB::table('user_orders')->where('order_id',$order_id)->where('status','!=','2')->select('rest_detail_id','front_user_address_id')->get();
		for ($i = 0, $c = count($order); $i < $c; ++$i) {
            $order[$i] = (array) $order[$i];
        }
		//print_r($order); die;
		$rest_id = $order['0']['rest_detail_id'];
        $front_user_address_id = $order['0']['front_user_address_id'];		
        $rest_detail = DB ::table('rest_details')
		->leftjoin('countries','rest_details.country', '=','countries.id')
		->leftjoin('states','rest_details.state', '=','states.id')
		->leftjoin('cities','rest_details.city', '=','cities.id')
		->where('rest_details.id',$rest_id)
		->select('rest_details.f_name','rest_details.l_name','rest_details.add1','rest_details.add2',
		'countries.name as country_name','states.name as state_name','cities.name as city_name','rest_details.pincode')->get();
		//print_r($rest_detail); die;
		for ($i = 0, $c = count($rest_detail); $i < $c; ++$i) {
            $rest_detail[$i] = (array) $rest_detail[$i];
        }
		
		$customer_detail = DB ::table('front_user_addresses')
		->leftjoin('countries','front_user_addresses.country_id', '=','countries.id')
		->leftjoin('states','front_user_addresses.state_id', '=','states.id')
		->leftjoin('cities','front_user_addresses.city_id', '=','cities.id')
		->where('front_user_addresses.id',$front_user_address_id)
		->select('front_user_addresses.booking_person_name','front_user_addresses.mobile','front_user_addresses.address','front_user_addresses.landmark',
		'countries.name as country_name','states.name as state_name','cities.name as city_name','front_user_addresses.zipcode')->get();
		for ($i = 0, $c = count($customer_detail); $i < $c; ++$i) {
            $customer_detail[$i] = (array) $customer_detail[$i];
        }
		$item=OrderItem::where('order_id',$order_id)->where('status','!=','2')->select('*')->get();	
		
        $order_payment=DB ::table('order_payments')
		->leftjoin('front_user_details','front_user_details.front_user_id', '=','order_payments.front_user_id')
		->leftjoin('order_services','order_services.id', '=','order_payments.order_type')
		->where('order_payments.order_id',$order_id)->where('order_payments.status','!=','2')
		->select('order_payments.*','front_user_details.fname','front_user_details.lname','order_services.name as order_type_name')->get();
		for ($i = 0, $c = count($order_payment); $i < $c; ++$i) {
            $order_payment[$i] = (array) $order_payment[$i];
        }
	    return view('front/useraccount/printbill')->with('customer_detail',$customer_detail)
		->with('rest_detail',$rest_detail)->with('item',$item)->with('order_payment',$order_payment);
	}
	
	function order_cancel()
	{
		$auth_id = Auth::user('front')->id;
		$order_id = Input::get('order_id');
		$orderstatusmap = new FrontOrderStatusMap;
		$orderstatusmap->order_id =$order_id;
		$orderstatusmap->status_id =4;
		$orderstatusmap->created_by =$auth_id;
		$orderstatusmap->created_at =date("Y-m-d H:i:s");
		$orderstatusmap->status =1;
		$orderstatusmap->save();
		
		$userorder = Userorder:: where('order_id',$order_id)->first();
		$userorder->front_order_status_id=4;
		$userorder->save();
		$json['success']=1;
        echo json_encode($json);
        return ;
	}
}
