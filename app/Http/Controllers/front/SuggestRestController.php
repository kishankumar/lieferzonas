<?php

namespace App\Http\Controllers\front;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\DefaultController;
use App\superadmin\FrontCityMap;
use App\superadmin\FrontCountryMap;
use App\superadmin\FrontKitchenMap;
use App\superadmin\FrontStateMap;
use App\front\FrontUserDetail;
use App\front\FrontUserAddress;
use App\front\RestSuggestion;
use App\front\SuggestionKitchenMap;
use Session;
use DB;
use Auth;
use Validator;
class SuggestRestController extends DefaultController
{
    public function index()
    {
	   
	 
    }
    
    public function create()
    {
		
    }

    public function store(Request $request)
    {
		
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
		
    }

    public function destroy($id)
    {
      //return redirect(route('front.address.index'));
    }
	public function delete($id)
    {
	 
    }
	public function getData(Request $request)
    {
        
    }
	public function add_suggestion(Request $request)
	{
		if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
		
		//print_r($data); die;
		$messages = array(
		    "delivery_service.required" =>"Please enter delivery service",
			"fname.required" =>"Please enter first name",
			"street.required" => "Please enter street",
			"houseno.required" => "Please enter house no.",
			"mobile.required" => "Please enter mobile",
			"zipcode.required" =>"Please enter zipcode",
			"email.required" =>"Please enter delivery email",
            "recaptcha.required"=>"Please enter captcha"
        );
		$userData = array(
           
		   'delivery_service' => $data['delivery_service'],
		   'email' => $data['email'],
		   'fname' => $data['fname'],
		   'lname' => $data['lname'],
		   'houseno' => $data['houseno'],
		   'mobile' => $data['mobile'],
		   'phone' => $data['phone'],
		   'street' => $data['street'],
		   'zipcode' => $data['zipcode'],
		   'info' => $data['info'],
		   'recaptcha'=>$data['recaptcha']
		   
        );
        
		$rules = array(
		    "delivery_service" => 'required',
			"fname" => 'required',
			"street" => 'required',
			"houseno" => 'required',
			"mobile" => 'required|numeric',
			"zipcode" => 'required',
			"email" => 'required|Email',
            "recaptcha"=>'required'  
			
        );
       $validation  = Validator::make($userData,$rules,$messages); 
	   //print_r($validation);
        //$validation  =  $this->set_validation('',$request); 
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
		
		else{
			$delivery_service = $request->delivery_service;
			$email = $request->email;
			$fname = $request->fname;
			$houseno =$request->houseno;
			$info = $request->info;
			$lname = $request->lname;
			$mobile = $request->mobile;
			$phone = $request->phone;
			$street = $request->street;
			$zipcode = $request->zipcode;
			
			
			$restsuggestion = new RestSuggestion;
			$restsuggestion->delivery_service=$delivery_service;
			$restsuggestion->email=$email;
			$restsuggestion->fname=$fname;
	        $restsuggestion->houseno=$houseno;
			$restsuggestion->zipcode=$zipcode;
			$restsuggestion->street=$street;
			$restsuggestion->lname=$lname;
			$restsuggestion->mobile=$mobile;
			$restsuggestion->phone_no=$phone;
			$restsuggestion->information=$info;
			$restsuggestion->status=1;
			$restsuggestion->created_at=date("Y-m-d H:i:s");
			$restsuggestion->save();
			$id =$restsuggestion->id;
			$selkitchen = Input::get('kitchen');
		
			if(count($selkitchen)>0)
			{
				foreach($selkitchen as $key => $value)
				{
				 $suggestionkitchenmap = new SuggestionKitchenMap;
				 $suggestionkitchenmap->rest_suggestion_id=$id;
				 $suggestionkitchenmap->kitchen_id = $value;
				 $suggestionkitchenmap->status=1;
				 $suggestionkitchenmap->created_at=date("Y-m-d H:i:s");
				 $suggestionkitchenmap->save();
				}
			}
			
			
			$json['success']=1;
			echo json_encode($json);
			return ;
		}
		
	}
	/////Add validation////////
     public function set_validation($id=null,$request)
     {
        $message=array(
            
            "mobile.required"=>"Mobile no is required",
            "address1.required"=>"Address is required",
            "city.required"=>"City is required",
            "state.required"=>"State is required",
            "country.required"=>"Country is required",
            "zipcode.required"=>"Zipcode is required",
          );

        $this->validate($request,[
        'mobile' => 'required|numeric',
        'address1' => 'required',
        'city' => 'required',
        'state' => 'required',
        'country' => 'required',
        'zipcode' => 'required',
        ],$message);
     }
}
