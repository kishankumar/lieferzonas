<?php

namespace App\Http\Controllers\front;

use App\front\FrontOrderStatusMap;
use App\front\FrontOrderTransaction;
use App\front\OrderDiscount;
use App\front\OrderItemExtra;
use App\front\UserCashbackPoint;
use App\superadmin\ExtraChoice;
use App\superadmin\ExtraChoiceElement;
use App\superadmin\RestSubMenu;
use App\superadmin\SubMenuPriceMap;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\DefaultController;
use App\front\FrontUserDetail;
use App\front\FrontUserSubscription;
use App\front\FrontUserAddress;
use App\front\UserBonusPoint;
use App\superadmin\SuperAdminSetting;
use App\front\OrderPayment;
use App\superadmin\Promocode;
use App\superadmin\PromoRestMap;
use App\superadmin\PromoUserMap;

use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Session;
use DB;
use Validator;
use Auth;

use App\front\OrderItem;
use App\front\UserOrder;

class FrontCartController extends DefaultController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Hakan Aras: New Cart

    protected static function areExtrasDifferent($extras1, $extras2)
    {
        return $extras1 != $extras2;
    }

    protected function getRestaurantCart(&$request, $restaurant_id)
    {
        $session = $request->session();

        $cart_array_key = "new_carts";

        if (!$session->has($cart_array_key)) {
            $session->put($cart_array_key, array());
        }

        $cart_array = $session->get($cart_array_key);

        if (!array_key_exists($restaurant_id, $cart_array)) {
            $cart_array[$restaurant_id] = array();
        }

        $cart = $cart_array[$restaurant_id];

        return $cart;
    }

    protected function setRestaurantCart(&$request, $restaurant_id, $new_cart)
    {
        $session = $request->session();

        $cart_array_key = "new_carts";

        if (!$session->has($cart_array_key)) {
            $session->put($cart_array_key, array());
        }

        $cart_array = $session->get($cart_array_key);

        $cart_array[$restaurant_id] = $new_cart;

        $session->put($cart_array_key, $cart_array);
    }

    public function getCartHtml($cart, $restaurant_id, $editable = true)
    {
        return view("front.restaurantMenu.cart", [
            "cart" => $cart,
            "restaurant_id" => $restaurant_id,
            "editable" => $editable
        ])->render();
    }

    public function addItemToCart(Request $request)
    {
        $restaurant_id = $request->input("restaurant_id");
        $item_id = $request->input("item_id");
        $extras = $request->input("extras");

        $cart = $this->getRestaurantCart($request, $restaurant_id);

        // Check if exact item exists in the cart
        foreach ($cart as $key => $cart_element) {
            if ($cart_element->item_id != $item_id) {
                continue;
            }
            if (FrontCartController::areExtrasDifferent($extras, $cart_element->extras)) {
                continue;
            }
            $cart[$key]->count = $cart_element->count + 1;

            $this->setRestaurantCart($request, $restaurant_id, $cart);

            return response()->json([
                "status" => "success",
                "html" => $this->getCartHtml($cart, $restaurant_id)
            ]);
        }

        unset($cart_element);

        $cart_element = new \stdClass();
        $cart_element->item_id = $item_id;
        $cart_element->count = 1;
        $cart_element->extras = $extras;

        $cart[] = $cart_element;

        $this->setRestaurantCart($request, $restaurant_id, $cart);

        return response()->json([
            "status" => "success",
            "html" => $this->getCartHtml($cart, $restaurant_id)
        ]);
    }

    public function clearCart(Request $request)
    {
        $restaurant_id = $request->input("restaurant_id");
        $this->setRestaurantCart($request, $restaurant_id, []);
        return response()->json([
            "status" => "success",
            "html" => $this->getCartHtml(array(), $restaurant_id)
        ]);
    }

    public function getCart(Request $request)
    {
        $restaurant_id = $request->input("restaurant_id");
        $editable = $request->input("editable") == "false" ? false : true;
        $cart = $this->getRestaurantCart($request, $restaurant_id);
        return $this->getCartHtml($cart, $restaurant_id, $editable);
    }

    public function incrementCartItem(Request $request)
    {
        $restaurant_id = $request->input("restaurant_id");
        $cart_element_index = intval($request->input("index"));
        $cart = $this->getRestaurantCart($request, $restaurant_id);
        $cart[$cart_element_index]->count += 1;
        $this->setRestaurantCart($request, $restaurant_id, $cart);
        return response()->json([
            "status" => "success",
            "html" => $this->getCartHtml($cart, $restaurant_id)
        ]);
    }

    public function decrementCartItem(Request $request)
    {
        $restaurant_id = $request->input("restaurant_id");
        $cart_element_index = intval($request->input("index"));
        $cart = $this->getRestaurantCart($request, $restaurant_id);
        $cart[$cart_element_index]->count -= 1;
        if ($cart[$cart_element_index]->count == 0) {
            unset($cart[$cart_element_index]);
            $cart = array_values($cart);
        }
        $this->setRestaurantCart($request, $restaurant_id, $cart);
        return response()->json([
            "status" => "success",
            "html" => $this->getCartHtml($cart, $restaurant_id)
        ]);
    }

    public function newShippingArea(Request $request, $restaurant_id)
    {
        $cart = $this->getRestaurantCart($request, $restaurant_id);
        if ($cart == array()) {
            return redirect(url("/restaurant/" . $restaurant_id . "/search"));
        }
        $data['restId'] = $restaurant_id;
        $t = date('d-m-Y');
        $dayId = date("N", strtotime($t));

        $data['closeTimeForToday'] = DB::table('rest_day_time_maps')->select('id', 'open_time', 'close_time')
            ->where('rest_detail_id', $restaurant_id)
            ->where('day_id', $dayId)
            ->get();


        $userPointsDetail = SuperAdminSetting::where('status', 1)->select('cashback_point_value', 'bonus_point_value')->get();

        if (count($userPointsDetail) > 0) {
            $data['cashbackvalue'] = $userPointsDetail[0]->cashback_point_value;
            $data['bonusPointValue'] = $userPointsDetail[0]->bonus_point_value;
        } else {
            $data['cashbackvalue'] = 100;
            $data['bonusPointValue'] = 100;
        }


        if (Auth::check("front")) {
            $data['addressDetails'] = FrontUserAddress::where('front_user_id', Auth::user('front')->id)->where('status', '!=', '2')->get();
        } else {
            $data['addressDetails'] = array();
        }

        return view('front/restaurantMenu/shipping-area')->with($data);
    }

    // Hakan Aras: New Cart Ende

    public function addItem(Request $request)
    {

        $itemType = $request->itemType;
        $itemId = $request->itemId;
        $restId = $request->restId;
        $eventType = $request->eventType;

        if ($itemType == 'extras') {
            $data['itemDeatls'] = DB:: table('menu_extras')
                ->select('id', 'name', 'price')
                ->where('id', $itemId)
                ->get();

            $price = $data['itemDeatls'][0]->price;
            $itemName = $data['itemDeatls'][0]->name;

        } else {

            if ($itemType == 'menu') {

                $itemTable = 'rest_menus';
                $priceMapTable = 'menu_price_maps';
                $mapColumn = 'menu_id';
            } else {

                $itemTable = 'rest_sub_menus';
                $priceMapTable = 'sub_menu_price_maps';
                $mapColumn = 'sub_menu_id';
            }

            $data['itemDeatls'] = DB:: table($itemTable)
                ->join($priceMapTable, $priceMapTable . '.' . $mapColumn, '=', $itemTable . '.id')
                ->select($itemTable . '.id', $itemTable . '.name', $priceMapTable . '.price')
                ->where($itemTable . '.id', $itemId)
                ->where($priceMapTable . '.price_type_id', 1)
                ->get();

            $price = $data['itemDeatls'][0]->price;
            $itemName = $data['itemDeatls'][0]->name;
        }

        $i = 0;
        //Session::put('cart',array());

        if (count(Session::get('cart')) < 1) {

            Session::put('cart', array());
        } elseif (Session::get('cart')[0]['restid'] != $restId) {

            Session::put('cart', array());

        }
        $i = 0;
        $arr = array();
        $arr = Session::get('cart');
        $check = 0;
        $decheck = 0;
        if (count($arr) == 0) {


            $arr[$i]['price'] = $price;
            $arr[$i]['itemname'] = $itemName;
            $arr[$i]['restid'] = $restId;
            $arr[$i]['qty'] = 1;
            $arr[$i]['itemId'] = $itemId;
            $arr[$i]['itemType'] = $itemType;

            $i++;
            Session::put('arrayindex', $i);
            Session::put('cart', $arr);

        } else {
            $i = Session::get('arrayindex');
            for ($k = 0; $k < $i; $k++) {
                if (!$arr[$k]) {
                    continue;
                }
                if ($arr[$k]['itemId'] == $itemId && $arr[$k]['itemType'] == $itemType) {

                    if ($eventType == 'add') {
                        $arr[$k]['qty'] = $arr[$k]['qty'] + 1;

                        $arr[$k]['price'] = $arr[$k]['price'] + $price;
                    } elseif ($eventType == 'substract') {
                        $arr[$k]['qty'] = $arr[$k]['qty'] - 1;
                        $arr[$k]['price'] = $arr[$k]['price'] - $price;

                        if ($arr[$k]['qty'] == 0) {
                            $arrLength = count($arr);

                            for ($x = $k; $x < $arrLength - 1; $x++) {

                                $arr[$x] = $arr[$x + 1];

                            }
                            unset($arr[$arrLength - 1]);
                            Session::put('arrayindex', $i - 1);
                            $i--;
                        }
                    } else {

                        $arrLength = count($arr);

                        for ($x = $k; $x < $arrLength - 1; $x++) {

                            $arr[$x] = $arr[$x + 1];

                        }
                        unset($arr[$arrLength - 1]);
                        Session::put('arrayindex', $i - 1);
                        $i--;

                    }
                    $check++;
                    Session::put('cart', $arr);

                }
            }


            if ($check == 0) {

                $i = Session::get('arrayindex');
                $arr[$i]['price'] = $price;
                $arr[$i]['itemname'] = $itemName;
                $arr[$i]['restid'] = $restId;
                $arr[$i]['qty'] = 1;
                $arr[$i]['itemId'] = $itemId;
                $arr[$i]['itemType'] = $itemType;

                $i++;
                Session::put('arrayindex', $i);
                Session::put('cart', $arr);

            }

        }


        echo json_encode(Session::get('cart'));


    }

    public function reviewOrder(Request $request, $restroId = 0)
    {

        if (!Auth::check('front')) {
            $data['restId'] = $restroId;

            return view('front/restaurantMenu/review-order')->with($data);
        } else {

            return redirect(url('restaurant/' . $request->restId . '/shipping-area'));

        }
    }

    public function shippingArea(Request $request, $restroId = 0)
    {

        if (count(Session::get('cart')) > 0) {

            if (!Auth::check('front')) {

                return redirect(url('restaurant/' . $restroId . '/review-order'));
            } else {

                $data['restId'] = $restroId;
                $t = date('d-m-Y');
                $dayId = date("N", strtotime($t));

                $data['closeTimeForToday'] = DB::table('rest_day_time_maps')->select('id', 'open_time', 'close_time')
                    ->where('rest_detail_id', $restroId)
                    ->where('day_id', $dayId)
                    ->get();


                $userPointsDetail = SuperAdminSetting::where('status', 1)->select('cashback_point_value', 'bonus_point_value')->get();

                if (count($userPointsDetail) > 0) {
                    $data['cashbackvalue'] = $userPointsDetail[0]->cashback_point_value;
                    $data['bonusPointValue'] = $userPointsDetail[0]->bonus_point_value;
                } else {
                    $data['cashbackvalue'] = 100;
                    $data['bonusPointValue'] = 100;
                }


                $data['addressDetails'] = FrontUserAddress::where('front_user_id', Auth::user('front')->id)->where('status', '!=', '2')->get();

                return view('front/restaurantMenu/shipping-area')->with($data);
            }
        } else {

            if (!Auth::check('front')) {

                return redirect(url());
            } else {

                return redirect(url('restaurant/' . $restroId . '/search'));
            }
        }

    }

    public function getExistValOneCond(Request $request)
    {
        if ($request) {

            $firstVal = $request->firstValue;

            $table = $request->table;
            $firstCond = $request->firstCond;

            $id = $request->id;

            $values = DB::table($table)->select('id')
                ->where($firstCond, $firstVal)
                ->where('status', '!=', 2)
                ->where('id', '!=', $id)
                ->get();

            $count = count($values);
            if ($count > 0) {
                echo 'error';
            } else {
                echo 'success';
            }
        }
    }

    public static function getCartElementPrice($cart_element, $order_type)
    {
        $price = 0.0;
        if ($order_type == '1') { // Zustellung
            $price_row = RestSubMenu::getSubMenuDeliveryPrice($cart_element->item_id);
            if ($price_row) {
                $price = bcadd($price, $price_row->price, 2);
            } else return false;
        } else { // Abholung
            $price_row = RestSubMenu::getSubMenuPickupPrice($cart_element->item_id);
            if ($price_row) {
                $price = bcadd($price, $price_row->price, 2);
            } else return false;
        }
        if (!$cart_element->extras) return $price;
        foreach ($cart_element->extras as $extra) {
            if (is_array($extra)) {
                foreach ($extra as $extra_choice) {
                    $price = bcadd($price, ExtraChoiceElement::find($extra_choice)->price, 2);
                }
            } else {
                $price = bcadd($price, ExtraChoiceElement::find($extra)->price, 2);
            }
        }
        return $price;
    }

    protected function getBonusPointDiscount(Request $request)
    {

    }

    protected function getCashbackPointDiscount(Request $request)
    {

    }

    protected function getCouponDiscount(Request $request)
    {

    }

    public function place_order(Request $request)
    {
        if (!Auth::check('front')) {
            return redirect(url('/'));
        }
        $restaurant_id = $request->input("restaurant_id");
        $order_type = $request->input("order_type");

        if ($order_type != '1' && $order_type != '2') {
            Session::flash('errmessage', 'Bitte Zustellung oder Abholung auswählen!');
            return redirect(url("checkout/" . $restaurant_id));
        }

        $cart = $this->getRestaurantCart($request, $restaurant_id);
        if (count($cart) == 0) {
            Session::flash('errmessage', 'Der Warenkorb enthält keine Gerichte!');
            return redirect(url('/checkout/' . $restaurant_id));
        }

        $totalCartAmount = 0;
        foreach ($cart as $cart_element) {
            $cart_element_price = self::getCartElementPrice($cart_element, $order_type);
            if (!$cart_element_price) {
                Session::flash('errmessage', 'Eines der Gerichte kann nicht zugestellt/abgeholt werden!');
                return redirect(url("checkout/" . $restaurant_id));
            }

            $totalCartAmount = bcadd($totalCartAmount, bcmul($cart_element_price, $cart_element->count, 2), 2);
        }

        $address_id = $request->address;

        $address = FrontUserAddress::find($address_id);

        if (!$address and $order_type == '1') {
            Session::flash('errmessage', 'Bitte wählen Sie eine gültige Adresse!');
            return redirect(url("checkout/" . $restaurant_id));
        }

        $bonus = $request->bonus_point;
        $cashback = $request->cashback_point;
        $discount = $request->offer_type;

        $couponCode = $request->coupon_code;
        $userId = Auth::user('front')->id;
        $currDate = date('Y-m-d H:i:s');
        $paymentMethod = $request->payment_method;
        $discountPriceType = 'C';

        $orderDateTime = $request->order_date_time;

        if ($orderDateTime == 2) {
            $deliveryDate = $request->delivery_date;
            $deliveryTime = date("H:i", strtotime($request->delivery_time));
            $userDateTime = $deliveryDate . ' ' . $deliveryTime . ':00';
        } else {
            $userDateTime = '';
        }

        $discount_count = 0;
        if ($bonus == 'on') $discount_count++;
        if ($cashback == 'on') $discount_count++;
        if ($discount == 'have_coupon') $discount_count++;
        if ($discount_count > 1) {
            Session::flash('errmessage', 'Es kann nur eine Vergünstigung gleichzeitig verwendet werden');
            return redirect(url("checkout/" . $restaurant_id));
        }

        $cashbackPoints = UserCashbackPoint::availablepoint($userId);
        $bonusPoints = UserBonusPoint::availablepoint($userId);

        $userPointsDetail = SuperAdminSetting::where('status', 1)->select('cashback_point_value', 'bonus_point_value')->get();

        if (count($userPointsDetail) > 0) {
            $cashbackvalue = $userPointsDetail[0]->cashback_point_value;
            $bonusPointValue = $userPointsDetail[0]->bonus_point_value;
        } else {
            $cashbackvalue = 100;
            $bonusPointValue = 100;
        }

        if ($bonus == 'on') {
            $discountAmt = $bonusPoints / $bonusPointValue;
            $totalAmt = $totalCartAmount - $discountAmt;
            $discountType = 'bonusPoint';
            $discountVal = '';
            $couponCode = '';
        } elseif ($cashback == 'on') {
            $discountAmt = $cashbackPoints / $cashbackvalue;
            $totalAmt = $totalCartAmount - $discountAmt;
            $discountType = 'cashbackPoint';
            $discountVal = '';
            $couponCode = '';
        } elseif ($discount == 'have_coupon' or $couponCode != '') {
            $TotalOrder = OrderPayment::where('front_user_id', $userId)->count();
            $procodeDetails = Promocode:: where('status', 1)->where('valid_to', '>', $currDate)->where('coupon_code', $couponCode)->get();
            $count = count($procodeDetails);
            if ($count < 1) {
                Session::flash('errmessage', 'Ungültiger Coupon');
                return redirect(url('checkout/' . $restaurant_id));
            }
            $discountType = 'couponCode';
            $discountPriceType = $procodeDetails[0]->payment_mode;
            $discountVal = $procodeDetails[0]->payment_amount;
            $couponCode = $couponCode;
            $promoCodeId = $procodeDetails[0]->id;
            $promoMinAmount = $procodeDetails[0]->min_amount;
            $paymentMode = $procodeDetails[0]->payment_mode;
            $paymentAmount = $procodeDetails[0]->payment_amount;

            if ($paymentMode == 'P') {
                $discountAmt = $totalCartAmount * $paymentAmount / 100;
                $totalAmt = $totalCartAmount - $totalCartAmount * $paymentAmount / 100;
            } else {
                $discountAmt = $paymentAmount;
                $totalAmt = $totalCartAmount - $paymentAmount;
            }

            $isNewCustomer = $procodeDetails[0]->is_new_customer;
            if ($isNewCustomer == 1 and $TotalOrder > 0) {
                Session::flash('errmessage', 'Dieser Coupon ist nur für Neu-Kunden!');
                return redirect(url("checkout/" . $restaurant_id));

            } else {
                if ($totalCartAmount < $promoMinAmount) {
                    Session::flash('errmessage', 'Dieser Coupon ist nur für Bestellungen über € ' . $promoMinAmount . ' gültig!');
                    return redirect(url("checkout/" . $restaurant_id));
                }
                $isUserSpecific = $procodeDetails[0]->is_user_specific;
                $isResturantSpecific = $procodeDetails[0]->is_rest_specific;
                if ($isResturantSpecific == 1) {
                    $promoRestDetails = PromoRestMap:: where('promo_id', $promoCodeId)->select('rest_id')->get();
                    $existRest = 0;
                    foreach ($promoRestDetails as $promoRestDetail) {
                        if ($promoRestDetail->rest_id == $restaurant_id) {
                            $existRest = 1;
                            break;
                        }
                    }
                    if ($existRest == 0) {
                        Session::flash('errmessage', 'Dieser Coupon ist für dieses Restaurant nicht gültig!');
                        return redirect(url("checkout/" . $restaurant_id));
                    }
                }
                if ($isUserSpecific == 1) {
                    $promoUserDetails = PromoUserMap::where('promo_id', $promoCodeId)->select('user_id')->get();
                    $existUser = 0;
                    foreach ($promoUserDetails as $promoUserDetail) {
                        if ($promoUserDetail->user_id == $userId) {
                            $existUser = 1;
                            break;
                        }
                    }
                    if ($existUser == 0) {
                        Session::flash('errmessage', 'Ungültiger Coupon');
                        return redirect(url("checkout/" . $restaurant_id));
                    }
                }
            }
        } else {
            $discountType = 'noDiscount';
            $totalAmt = $totalCartAmount;
            $discountAmt = '';
            $couponCode = '';
        }

        $strid = @date("y-m-d H:i:s");
        list($idate, $time) = explode(" ", $strid);
        list($year, $month, $day) = explode("-", $idate);
        list($hour, $min, $sec) = explode(":", $time);

        $yearCal = 20;
        $newYear = $year + $yearCal;
        $monthCal = 26;
        $newmonth = $month + $monthCal;
        $hourCal = 28;
        $newHour = $hour + $hourCal;

        $int = rand(0, 25);
        $a_z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $rand_letter = $a_z[$int];

        $tkordNO = $newYear . $newmonth . $day . $newHour . $min . $sec;
        $tkordRCH = $rand_letter;

        $orderId = "" . $tkordNO . "" . $tkordRCH . "R" . $restaurant_id;

        $userSubscription = $request->subscription;
        // $totalAmt = 0;
        DB::beginTransaction();

        foreach($cart as $cart_element) {
            $orderItem = new OrderItem;
            $orderItem->order_id = $orderId;
            $orderItem->rest_detail_id = $restaurant_id;
            $orderItem->front_user_id = Auth::user('front')->id;
            $orderItem->item_id = $cart_element->item_id;
            $orderItem->item_name = RestSubMenu::find($cart_element->item_id)->name;
            $orderItem->item_qty = $cart_element->count;
            $orderItem->item_price = bcmul(self::getCartElementPrice($cart_element, $order_type), $cart_element->count, 2);
            $orderItem->item_type = "submenu";
            $orderItem->save();
            if (!$cart_element->extras) continue;
            foreach ($cart_element->extras as $extra_choice => $extra_choice_element) {
                if (is_array($extra_choice_element)) {
                    foreach($extra_choice_element as $single_extra_choice_element) {
                        $extra = new OrderItemExtra;
                        $extra->order_item_id = $orderItem->id;
                        $extra->extra_choice_id = $extra_choice;
                        $extra->extra_choice_element_id = $single_extra_choice_element;
                        $extra->save();
                    }
                }
                else {
                    $extra = new OrderItemExtra;
                    $extra->order_item_id = $orderItem->id;
                    $extra->extra_choice_id = $extra_choice;
                    $extra->extra_choice_element_id = $extra_choice_element;
                    $extra->save();
                }
            }
        }


        $userOrder = new UserOrder();

        $userOrder->order_id = $orderId;
        $userOrder->front_user_id = Auth::user('front')->id;
        $userOrder->front_user_address_id = $address_id;
        $userOrder->rest_detail_id = $restaurant_id;
        $userOrder->total_amount = $totalAmt;
        $userOrder->grand_total = $totalAmt;
        $userOrder->user_time = $userDateTime;
        $userOrder->time_type = $orderDateTime;
        $userOrder->created_by = Auth::user('front')->id;
        $userOrder->updated_by = Auth::user('front')->id;
        $userOrder->order_type = $order_type;

        $userOrder->save();

        $userOrderId = $userOrder['id'];


        $orderStatus = new FrontOrderStatusMap();
        $orderStatus->order_id = $orderId;
        $orderStatus->status_id = 1;
        $orderStatus->created_by = Auth::user('front')->id;
        $orderStatus->updated_by = Auth::user('front')->id;
        $orderStatus->save();

        $orderPayment = new OrderPayment();
        $orderPayment->front_user_id = Auth::user('front')->id;
        $orderPayment->order_id = $orderId;
        $orderPayment->rest_detail_id = $restaurant_id;
        $orderPayment->total_amount = $totalAmt;
        $orderPayment->grand_total = $totalAmt;
        $orderPayment->payment_method = $paymentMethod;
        $orderPayment->order_type = $order_type;
        $orderPayment->discount_amount = $discountAmt;
        $orderPayment->save();

        $orderPaymentId = $orderPayment['id'];

        if ($discountType != 'noDiscount') {
            $orderDiscount = new OrderDiscount();
            $orderDiscount->front_user_id = Auth::user('front')->id;
            $orderDiscount->payment_id = $orderPaymentId;
            $orderDiscount->order_id = $orderId;
            $orderDiscount->rest_detail_id = $restaurant_id;
            $orderDiscount->discount_type = $discountType;
            $orderDiscount->coupon_code = $couponCode;
            $orderDiscount->discount_price_type = $discountPriceType;
            $orderDiscount->discount_val = $discountVal;
            $orderDiscount->discount_amount = $discountAmt;
            $orderDiscount->save();
        }

        if ($userSubscription == 'on') {
            $frontUserDetail = FrontUserDetail::where('front_user_id', Auth::user('front')->id)->where('status', 1)->select('fname', 'lname', 'email')->get();
            if (count($frontUserDetail) > 0) {
                $userEmail = $frontUserDetail[0]->email;
                $userName = $frontUserDetail[0]->fname . '' . $frontUserDetail[0]->lname;
            } else {
                $userEmail = '';
            }

            $getSubscriptionDetail = FrontUserSubscription::where('front_user_id', Auth::user('front')->id)->where('email', $userEmail)->select('id', 'status')->get();
            if (count($getSubscriptionDetail) > 0) {
                $status = $getSubscriptionDetail[0]->status;

                if ($status == 0) {
                    $updateQuery = DB::table('front_user_subscriptions')
                        ->where('front_user_id', Auth::user('front')->id)
                        ->where('email', $userEmail)
                        ->update(['status' => 1]);
                }

            } else {
                $userSubscription = new FrontUserSubscription();
                $userSubscription->front_user_id = Auth::user('front')->id;
                $userSubscription->name = $userName;
                $userSubscription->email = $userEmail;
                $userSubscription->status = 1;
                $userSubscription->created_by = Auth::user('front')->id;
                $userSubscription->updated_by = Auth::user('front')->id;
                $userSubscription->save();
            }
        }

        if ($orderPaymentId) {
            DB::commit();
            if ($paymentMethod == 1) {
                $this->clearCart($request);
                return redirect(url('confirm-order/' . $userOrderId));
            } else {
                $data['paymentAmount'] = $totalAmt * 100;
                $data['OrderId'] = $orderId;
                $this->clearCart($request);
                return view('front.restaurantMenu.front-order-form')->with($data);
            }


        } else {
            DB::rollBack();
            Session::flash('errmessage', 'Die Bestellung konnte nicht abgeschlossen werden');
            return redirect(url("checkout/" . $restaurant_id));
        }

    }

    public function cartCheckout(Request $request)
    {
        if (!Auth::check('front')) {
            return redirect(url('/'));
        }
        if (count(Session::get('cart')) > 0) {

            $totalCartAmount = 0;
            foreach (Session::get('cart') as $cartData) {
                $totalCartAmount = $totalCartAmount + $cartData['price'];
            }

            $restId = Session::get('cart')[0]['restid'];

            $addressId = $request->address;

            $bonus = $request->bonus_point;
            $cashback = $request->cashback_point;
            $discount = $request->offer_type;
            $couponCode = $request->coupon_code;
            $userId = Auth::check('front');
            $currDate = date('Y-m-d H:i:s');
            $paymentMethod = $request->payment_method;
            $discountPriceType = 'C';

            $orderDateTime = $request->order_date_time;

            if ($orderDateTime == 2) {
                $deliveryDate = $request->delivery_date;
                $deliveryTime = date("H:i", strtotime($request->delivery_time));
                $userDateTime = $deliveryDate . ' ' . $deliveryTime . ':00';
            } else {
                $userDateTime = '';
            }

            if ($bonus == 'on' and $cashback == 'on') {
                Session::flash('errmessage', 'You can not choose more than one discount');
                return redirect(url('restaurant/' . $restId . '/shipping-area'));
            } elseif ($bonus == 'on' and ($discount == 'have_coupon' or $couponCode != '')) {
                Session::flash('errmessage', 'You can not choose more than one discount');
                return redirect(url('restaurant/' . $restId . '/shipping-area'));

            } elseif ($cashback == 'on' and ($discount == 'have_coupon' or $couponCode != '')) {
                Session::flash('errmessage', 'You can not choose more than one discount');
                return redirect(url('restaurant/' . $restId . '/shipping-area'));
            } elseif ($addressId == '' or $addressId == 0) {

                Session::flash('errmessage', 'Firstly select the address');
                return redirect(url('restaurant/' . $restId . '/shipping-area'));
            } else {

                $cashbackPoints = \App\front\UserCashbackPoint::availablepoint(Auth::user('front')->id);

                $bonusPoints = \App\front\UserBonusPoint::availablepoint(Auth::user('front')->id);

                $userPointsDetail = SuperAdminSetting::where('status', 1)->select('cashback_point_value', 'bonus_point_value')->get();

                if (count($userPointsDetail) > 0) {
                    $cashbackvalue = $userPointsDetail[0]->cashback_point_value;
                    $bonusPointValue = $userPointsDetail[0]->bonus_point_value;
                } else {
                    $cashbackvalue = 100;
                    $bonusPointValue = 100;
                }

                if ($bonus == 'on') {
                    $discountAmt = $bonusPoints / $bonusPointValue;
                    $totalAmt = $totalCartAmount - $discountAmt;
                    $discountType = 'bonusPoint';
                    $discountVal = '';
                    $couponCode = '';
                } elseif ($cashback == 'on') {
                    $discountAmt = $cashbackPoints / $cashbackvalue;
                    $totalAmt = $totalCartAmount - $discountAmt;
                    $discountType = 'cashbackPoint';
                    $discountVal = '';
                    $couponCode = '';
                } elseif ($discount == 'have_coupon' or $couponCode != '') {

                    $TotalOrder = OrderPayment::where('front_user_id', $userId)->count();

                    $procodeDetails = Promocode:: where('status', 1)->where('valid_to', '>', $currDate)->where('coupon_code', $couponCode)->get();

                    $count = count($procodeDetails);

                    if ($count > 0) {

                        $discountType = 'couponCode';
                        $discountPriceType = $procodeDetails[0]->payment_mode;
                        $discountVal = $procodeDetails[0]->payment_amount;
                        $couponCode = $couponCode;


                        $promoCodeId = $procodeDetails[0]->id;
                        $promoMinAmount = $procodeDetails[0]->min_amount;
                        $paymentMode = $procodeDetails[0]->payment_mode;
                        $paymentAmount = $procodeDetails[0]->payment_amount;

                        if ($paymentMode == 'P') {
                            $discountAmt = $totalCartAmount * $paymentAmount / 100;
                            $totalAmt = $totalCartAmount - $totalCartAmount * $paymentAmount / 100;
                        } else {
                            $discountAmt = $paymentAmount;
                            $totalAmt = $totalCartAmount - $paymentAmount;
                        }

                        $isNewCustomer = $procodeDetails[0]->is_new_customer;
                        if ($isNewCustomer == 1 and $TotalOrder > 0) {
                            Session::flash('errmessage', 'This coupon is only for new customer');
                            return redirect(url('restaurant/' . $restId . '/shipping-area'));

                        } else {
                            $isUserSpecific = $procodeDetails[0]->is_user_specific;
                            $isResturantSpecific = $procodeDetails[0]->is_rest_specific;

                            if ($isResturantSpecific == 1 and $isUserSpecific == 0) {

                                $promoRestDetails = PromoRestMap:: where('promo_id', $promoCodeId)->select('rest_id')->get();

                                $existRest = '';
                                foreach ($promoRestDetails as $promoRestDetail) {

                                    if ($promoRestDetail->rest_id == $restId) {

                                        $existRest = 1;
                                        break;
                                    } else {
                                        $existRest = 0;
                                    }

                                }
                                if ($existRest == 0) {

                                    Session::flash('errmessage', 'This coupon is not valid for this resturant');
                                    return redirect(url('restaurant/' . $restId . '/shipping-area'));

                                } else {

                                    if ($totalCartAmount < $promoMinAmount) {

                                        Session::flash('errmessage', 'Your order amount must be greater than ' . $promoMinAmount);
                                        return redirect(url('restaurant/' . $restId . '/shipping-area'));
                                    } else {

                                        $totalAmt = $totalAmt;
                                    }
                                }

                            } elseif ($isResturantSpecific == 0 and $isUserSpecific == 1) {

                                $promoUserDetails = PromoUserMap::where('promo_id', $promoCodeId)->select('user_id')->get();
                                $existUser = '';

                                foreach ($promoUserDetails as $promoUserDetail) {

                                    if ($promoUserDetail->user_id == $userId) {
                                        $existUser = 1;
                                        break;
                                    } else {
                                        $existUser = 0;
                                    }
                                }

                                if ($existUser == 0) {

                                    Session::flash('errmessage', 'Invalid coupon code');
                                    return redirect(url('restaurant/' . $restId . '/shipping-area'));
                                } else {
                                    if ($totalCartAmount < $promoMinAmount) {

                                        Session::flash('errmessage', 'Your order amount must be greater than ' . $promoMinAmount);
                                        return redirect(url('restaurant/' . $restId . '/shipping-area'));
                                    } else {
                                        $totalAmt = $totalAmt;
                                    }
                                }

                            } elseif ($isResturantSpecific == 1 and $isUserSpecific == 1) {


                                $promoRestDetails = PromoRestMap:: where('promo_id', $promoCodeId)->select('rest_id')->get();

                                $existRest = '';
                                foreach ($promoRestDetails as $promoRestDetail) {

                                    if ($promoRestDetail->rest_id == $restId) {

                                        $existRest = 1;
                                        break;
                                    } else {
                                        $existRest = 0;
                                    }

                                }

                                $promoUserDetails = PromoUserMap::where('promo_id', $promoCodeId)->select('user_id')->get();
                                $existUser = '';

                                foreach ($promoUserDetails as $promoUserDetail) {

                                    if ($promoUserDetail->user_id == $userId) {
                                        $existUser = 1;
                                        break;
                                    } else {
                                        $existUser = 0;
                                    }
                                }

                                if ($existUser == 1 and $existRest == 1) {
                                    if ($totalCartAmount < $promoMinAmount) {
                                        Session::flash('errmessage', 'Your order amount must be greater than ' . $promoMinAmount);
                                        return redirect(url('restaurant/' . $restId . '/shipping-area'));
                                    } else {
                                        $totalAmt = $totalAmt;
                                    }
                                } else {

                                    Session::flash('errmessage', 'Invalid coupon code');
                                    return redirect(url('restaurant/' . $restId . '/shipping-area'));
                                }

                            } else {
                                if ($totalCartAmount < $promoMinAmount) {
                                    Session::flash('errmessage', 'Your order amount must be greater than ' . $promoMinAmount);
                                    return redirect(url('restaurant/' . $restId . '/shipping-area'));
                                } else {
                                    $totalAmt = $totalAmt;
                                }
                            }
                        }

                    } else {
                        Session::flash('errmessage', 'Invalid coupon code');
                        return redirect(url('restaurant/' . $restId . '/shipping-area'));
                    }

                } else {
                    $discountType = 'noDiscount';
                    $totalAmt = $totalCartAmount;
                    $discountAmt = '';
                    $couponCode = '';
                }

                $strid = @date("y-m-d H:i:s");
                list($idate, $time) = explode(" ", $strid);
                list($year, $month, $day) = explode("-", $idate);
                list($hour, $min, $sec) = explode(":", $time);

                $yearCal = 20;
                $newYear = $year + $yearCal;
                $monthCal = 26;
                $newmonth = $month + $monthCal;
                $hourCal = 28;
                $newHour = $hour + $hourCal;

                $int = rand(0, 25);
                $a_z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $rand_letter = $a_z[$int];

                $tkordNO = $newYear . $newmonth . $day . $newHour . $min . $sec;
                $tkordRCH = $rand_letter;

                $orderId = "" . $tkordNO . "" . $tkordRCH . "R" . $restId;

                $userSubscription = $request->subscription;
                // $totalAmt = 0;
                DB::beginTransaction();

                foreach (Session::get('cart') as $cartData) {

                    //$totalAmt = $totalAmt + $cartData['price'];

                    $orderItem = new OrderItem;

                    $orderItem->order_id = $orderId;
                    $orderItem->rest_detail_id = $cartData['restid'];
                    $orderItem->front_user_id = Auth::user('front')->id;
                    $orderItem->item_id = $cartData['itemId'];
                    $orderItem->item_name = $cartData['itemname'];
                    $orderItem->item_qty = $cartData['qty'];
                    $orderItem->item_price = $cartData['price'];
                    $orderItem->item_type = $cartData['itemType'];
                    $orderItem->save();
                }


                $userOrder = new UserOrder();

                $userOrder->order_id = $orderId;
                $userOrder->front_user_id = Auth::user('front')->id;
                $userOrder->front_user_address_id = $addressId;
                $userOrder->rest_detail_id = $restId;
                $userOrder->total_amount = $totalAmt;
                $userOrder->grand_total = $totalAmt;
                $userOrder->user_time = $userDateTime;
                $userOrder->time_type = $orderDateTime;
                $userOrder->created_by = Auth::user('front')->id;
                $userOrder->updated_by = Auth::user('front')->id;
                $userOrder->order_type = 1;

                $userOrder->save();

                $userOrderId = $userOrder['id'];


                $orderStatus = new FrontOrderStatusMap();

                $orderStatus->order_id = $orderId;
                $orderStatus->status_id = 1;
                $orderStatus->created_by = Auth::user('front')->id;
                $orderStatus->updated_by = Auth::user('front')->id;

                $orderStatus->save();

                $orderPayment = new OrderPayment();

                $orderPayment->front_user_id = Auth::user('front')->id;
                $orderPayment->order_id = $orderId;
                $orderPayment->rest_detail_id = $restId;
                $orderPayment->total_amount = $totalAmt;
                $orderPayment->grand_total = $totalAmt;
                $orderPayment->payment_method = $paymentMethod;
                $orderPayment->order_type = 1;
                $orderPayment->discount_amount = $discountAmt;

                $orderPayment->save();

                $orderPaymentId = $orderPayment['id'];

                if ($discountType != 'noDiscount') {
                    $orderDiscount = new OrderDiscount();

                    $orderDiscount->front_user_id = Auth::user('front')->id;
                    $orderDiscount->payment_id = $orderPaymentId;
                    $orderDiscount->order_id = $orderId;
                    $orderDiscount->rest_detail_id = $restId;
                    $orderDiscount->discount_type = $discountType;
                    $orderDiscount->coupon_code = $couponCode;
                    $orderDiscount->discount_price_type = $discountPriceType;
                    $orderDiscount->discount_val = $discountVal;
                    $orderDiscount->discount_amount = $discountAmt;

                    $orderDiscount->save();
                }

                if ($userSubscription == 'on') {
                    $frontUserDetail = FrontUserDetail::where('front_user_id', Auth::user('front')->id)->where('status', 1)->select('fname', 'lname', 'email')->get();
                    if (count($frontUserDetail) > 0) {
                        $userEmail = $frontUserDetail[0]->email;
                        $userName = $frontUserDetail[0]->fname . '' . $frontUserDetail[0]->lname;
                    } else {
                        $userEmail = '';
                    }

                    $getSubscriptionDetail = FrontUserSubscription::where('front_user_id', Auth::user('front')->id)->where('email', $userEmail)->select('id', 'status')->get();
                    if (count($getSubscriptionDetail) > 0) {
                        $status = $getSubscriptionDetail[0]->status;

                        if ($status == 0) {
                            $updateQuery = DB::table('front_user_subscriptions')
                                ->where('front_user_id', Auth::user('front')->id)
                                ->where('email', $userEmail)
                                ->update(['status' => 1]);
                        }

                    } else {
                        $userSubscription = new FrontUserSubscription();

                        $userSubscription->front_user_id = Auth::user('front')->id;
                        $userSubscription->name = $userName;
                        $userSubscription->email = $userEmail;
                        $userSubscription->status = 1;
                        $userSubscription->created_by = Auth::user('front')->id;
                        $userSubscription->updated_by = Auth::user('front')->id;

                        $userSubscription->save();
                    }

                }

                if ($orderPaymentId) {
                    DB::commit();
                    if ($paymentMethod == 1) {
                        Session::put('cart', array());
                        return redirect(url('confirm-order/' . $userOrderId));
                    } else {
                        $data['paymentAmount'] = $totalAmt * 100;
                        $data['OrderId'] = $orderId;
                        return view('front.restaurantMenu.front-order-form')->with($data);

                        //return redirect(url('confirm-order/'.$userOrderId));
                    }


                } else {
                    DB::rollBack();
                    Session::flash('errmessage', 'Something went wrong');
                    return redirect(url('restaurant/' . $restId . '/shipping-area'));
                }
            }
        } else {
            return redirect(url('front/dashboard'));
        }

    }

    public function payment(Request $request)
    {

        $merchant = explode('-', $request->merchantReference);
        $orderId = $merchant[1];
        $restId = substr($orderId, -1);
        $transactionId = $request->pspReference;

        if ($request->authResult == 'AUTHORISED') {
            Session::put('cart', array());

            $userOrderDetail = UserOrder::where('order_id', $orderId)->select('id')->get();
            $userOrderId = $userOrderDetail[0]->id;

            $frontOrderTransaction = new FrontOrderTransaction();

            $frontOrderTransaction->order_id = $orderId;
            $frontOrderTransaction->front_user_id = Auth::user('front')->id;
            $frontOrderTransaction->transaction_id = $transactionId;
            $frontOrderTransaction->transaction_date = date('Y-m-d H:i:s');
            $frontOrderTransaction->created_by = Auth::user('front')->id;
            $frontOrderTransaction->updated_by = Auth::user('front')->id;

            $frontOrderTransaction->save();

            return redirect(url('confirm-order/' . $userOrderId));

        } else {

            $deleted1 = DB::table('user_orders')->where('order_id', $orderId)->delete();

            $deleted2 = DB::table('order_payments')->where('order_id', $orderId)->delete();

            $deleted3 = DB::table('order_items')->where('order_id', $orderId)->delete();

            $deleted4 = DB::table('order_discounts')->where('order_id', $orderId)->delete();

            $deleted5 = DB::table('front_order_status_maps')->where('order_id', $orderId)->delete();

            Session::flash('errmessage', 'Your Transaction has been Failed');
            return redirect(url('restaurant/' . $restId . '/shipping-area'));

        }
    }

    public function orderConfirmation($id)
    {
        $userOrder = UserOrder::where('id', $id)->first();
        return view('front/confirmOrder/index')->with('orderDetails', $userOrder);
    }

    public function check_order_status(Request $request)
    {
        $userOrderId = $request->userOrderId;
        $userOrder = UserOrder::where('id', $userOrderId)->first();
        echo $userOrder['front_order_status_id'];
    }

    public function live_route($id)
    {
        $userOrder = UserOrder::where('id', $id)->first();
        return view('front/confirmOrder/live_route')->with('orderDetails', $userOrder);
    }
}
