<?php

namespace App\Http\Controllers\front;
date_default_timezone_set('Asia/Calcutta'); 
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\OrderItem;
use App\front\UserOrder;
use App\front\UserOrderReview;
use App\front\UserRestFavourite;
use App\front\UserBonusPoint;
use App\front\UserBonusPointLog;

class OrderRateController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$auth_id = Auth::user('front')->id;
		$userorder= DB ::table('user_orders')
		->leftjoin('rest_details',  'user_orders.rest_detail_id', '=','rest_details.id')
		->leftjoin('front_order_status_maps','user_orders.order_id', '=','front_order_status_maps.order_id')
		->leftjoin('front_user_addresses',  'user_orders.front_user_address_id', '=','front_user_addresses.id')
		->leftjoin('countries',  'front_user_addresses.country_id', '=','countries.id')
		->leftjoin('states',  'front_user_addresses.state_id', '=','states.id')
		->leftjoin('cities',  'front_user_addresses.city_id', '=','cities.id')
		->where('front_order_status_maps.status_id',7)
		->where('user_orders.front_user_id',$auth_id)
		->where('user_orders.status',1)
		->orderby('user_orders.created_at','desc')
		->select('rest_details.logo','rest_details.image','rest_details.f_name','rest_details.l_name','front_user_addresses.booking_person_name','front_user_addresses.mobile','front_user_addresses.zipcode','front_user_addresses.address','front_user_addresses.landmark','user_orders.*','countries.name as country_name','states.name as state_name','cities.name as city_name')
		->paginate(10);
        // return view('front/useraccount/order_rate')->with('userorder',$userorder);
        return view("front_new.user_profile.reviews");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function add_review()
	{
	 $user_id = Auth::user("front")->id;
	 $rest_id = Input::get('rest_id');
	 $order_id = Input::get('order_id');
	 $qrating = Input::get('qrating');
	 $drating = Input::get('drating');
	 $comment = Input::get('comment');
	 $orderrating = new UserOrderReview;
	 $orderrating->rest_id=$rest_id;
	 $orderrating->user_id=$user_id;
	 $orderrating->order_id=$order_id;
	 $orderrating->comment=$comment;
	 $orderrating->quality_rating = $qrating;
	 $orderrating->service_rating =	$drating;
	 $orderrating->status=1;
	 $orderrating->created_at=date("Y-m-d H:i:s");
	 $orderrating->save();
	 
	 $adduserbonus = new UserBonusPoint;
	 $adduserbonus->user_id = $user_id;
	 $adduserbonus->awarded_points = 5;
	 $adduserbonus->used_points = 0;
	 $adduserbonus->status = 1;
	 $adduserbonus->status = 1;
	 $adduserbonus->created_at=date("Y-m-d H:i:s");
	 $adduserbonus->save();
	 
	 $adduserbonuslog = new UserBonusPointLog;
	 $adduserbonuslog->user_id = $user_id;
	 $adduserbonuslog->activity_id = 1;
	 $adduserbonuslog->order_id = $order_id;
	 $adduserbonuslog->creditordebit = 1;
	 $adduserbonuslog->amount=5;
	 $adduserbonuslog->description="you reviewed an order";
	 $adduserbonuslog->status = 1;
	 $adduserbonuslog->created_at=date("Y-m-d H:i:s");
	 $adduserbonuslog->save();
	 
	 $json['success']=1;
        echo json_encode($json);
        return ;
	}
}
