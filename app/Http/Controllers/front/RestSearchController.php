<?php

namespace App\Http\Controllers\front;

use App\front\UserOrderReview;
use App\front\UserRestFavourite;
use App\Http\Controllers\DefaultController;
use App\superadmin\FaqQuestion;
use App\superadmin\Kitchen;
use App\superadmin\RestDeliveryCost;
use App\superadmin\RestKitchenMap;
use App\superadmin\RestZipcodeDeliveryMap;
use App\front\UserRecentRestVisit;
use App\superadmin\Zipcode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Session;
use DB;

class RestSearchController extends DefaultController
{
    public function index(Request $request)
    {
        
        $kit_arr = array();
        $restype_arr = array();

         if($request->has('zipcode'))
        {
            $zipcode = $request->zipcode;
            Session::put('zipcode',$zipcode);
        }
        else
        {
            $zipcode = Session::get('zipcode');
            if (!$zipcode) return redirect("/");
        }
        //print_r($zipcode); exit;
        //dd($request);
        $date = date("Y-m-d H:i:s");
        $q = DB::table('rest_zipcode_delivery_maps as dmap')->select('dmap.rest_id','rd.*','c.name as cname','s.name as sname','city.name as cityname','z.name as zname')
            ->join('rest_details as rd','dmap.rest_id','=','rd.id')
            ->join('countries as c','c.id','=','rd.country')
            ->join('states as s','s.id','=','rd.state')
            ->join('cities as city','city.id','=','rd.city')
            ->join('zipcodes as z','z.id','=','rd.pincode')
            ->join('rest_owners as ro','ro.rest_detail_id','=','rd.id')
            ->where('valid_to','>',$date)
            ->where('zipcode',$zipcode)
            ->where('rd.status', 1);

        if($request->has('kitchen_filter')||$request->has('restype_filter'))
        {
            $kitchen = $request->kitchen_filter;
            $restype = $request->restype_filter;
            $kit_arr = explode(',',$kitchen);
            $restype_arr = explode(',',$restype);

            if(in_array('new',$restype_arr))
                $q->join('rest_settings as rs','rs.rest_detail_id','=','dmap.rest_id')->where('rs.is_new','1');
            if(in_array('open',$restype_arr))
                $q->join('rest_day_time_maps as dtm','dtm.rest_detail_id','=','dmap.rest_id')->where('dtm.day_id',date('N'))->where('dtm.is_open','1');
        }

        $sort = $request->sort;
        if($sort == "alphabetical")
        {
            $rest = $q->orderBy('rd.f_name','asc')->get();
        }
        elseif($sort == "minimum")
        {
            $rest = $q->orderBy('rd.f_name','asc')->get();
        }
        else
        {
            $rest = $q->get();
        }

        $faq = FaqQuestion::where('faq_category_id','3')->where('status','1')->get();

        $all_kitchens = Kitchen::all();

        $restaurant_ids = array();
        foreach($rest as $restaurant) {
            array_push($restaurant_ids, $restaurant->id);
        }

        $rest_kitchens = RestKitchenMap::getRestaurantKitchenArray($restaurant_ids);

        $min_distance = 100;
        $max_distance = 100;

        $min_rating = 0;
        $max_rating = 5;

        $review_counts = UserOrderReview::rest_review_count_range($restaurant_ids);

        $min_rating_count = $review_counts[0];
        $max_rating_count = $review_counts[1];

        $delivery_cost_ranges = RestDeliveryCost::getDeliveryCostRanges($restaurant_ids);

        //dd($rest);
        // return view('front/search/index')->with([
        return view('front_new.restaurants.search')->with([
            'rest'=>$rest,
            'all_kitchens' => $all_kitchens,
            'rest_kitchen_maps' => $rest_kitchens,
            'zipcode'=>$zipcode,
            'kitchen_filter'=>$kit_arr,
            'restype_filter'=>$restype_arr,
            'sort'=>$sort,
            'faq'=>$faq,
            'min_rating' => $min_rating,
            'max_rating' => $max_rating,
            'min_rating_count' => $min_rating_count,
            'max_rating_count' => $max_rating_count,
            'delivery_cost_ranges' => $delivery_cost_ranges
        ]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }


    public function user_favourite(Request $request)
    {
        if($request->has('fav'))
        {
            $addfav = new UserRestFavourite;
            $addfav->user_id = $request->uid;
            $addfav->rest_id = $request->id;
            $addfav->save();
            echo $addfav;
        }
        else
        {
            $delfav = UserRestFavourite::where('user_id',$request->uid)->where('rest_id',$request->id)->delete();
            echo $delfav;
        }
    }

    public function checkzip_new(Request $request) {
        $zipcode = $request->zipcode;
        $zipcode_row = Zipcode::where("name", $zipcode)->where("status", 1)->get();
        if ($zipcode_row->count() < 1) {
            return response()->json([
                "status" => "failure",
                "reason" => "Konnte Postleitzahl nicht finden"
            ]);
        }
        return response()->json([
            "status" => "success"
        ]);
    }

    public function check_zip(Request $request)
    {
        $zipcode = $request->zipcode;
        $rest = DB::table('rest_zipcode_delivery_maps as dmap')->select('rest_id')->where('zipcode',$zipcode)->first();
        print_r($rest);
    }

    public function recent_visit(Request $request)
    {
        $rest_id = Input::get('rest_id');
        $user_id = Input::get('user_id');
        $userrestwisevisit=UserRecentRestVisit::where('user_id','=',$user_id)->where('rest_detail_id',$rest_id)->where('status','=',1)->get();
        $userrestwisevisitcount = count($userrestwisevisit->toArray()); 
        $userwisevisit=UserRecentRestVisit::where('user_id','=',$user_id)->where('status','=',1)->get();
        $userwisevisitcount = count($userwisevisit->toArray()); //die;
        
        if($userwisevisitcount>0)
        {
            $visit = $userwisevisit->toArray();
            $id = $visit[0]['id']; //die;
        }
        
        if($userrestwisevisitcount>0)
        {
            $deluservisit= UserRecentRestVisit::where('user_id',$user_id)->where('rest_detail_id',$rest_id)->delete();
            $recent_visit = new UserRecentRestVisit;
            $recent_visit->user_id=$user_id;
            $recent_visit->rest_detail_id=$rest_id;
            $recent_visit->status=1;
            $recent_visit->created_by=$user_id;
            $recent_visit->created_at=date("Y-m-d H:i:s");
            $recent_visit->save();
        }
        else if($userwisevisitcount=='8')
        {
            $deluservisit= UserRecentRestVisit::where('id',$id)->delete();
            $recent_visit = new UserRecentRestVisit;
            $recent_visit->user_id=$user_id;
            $recent_visit->rest_detail_id=$rest_id;
            $recent_visit->status=1;
            $recent_visit->created_by=$user_id;
            $recent_visit->created_at=date("Y-m-d H:i:s");
            $recent_visit->save();
        }
        else{
            
            $recent_visit = new UserRecentRestVisit;
            $recent_visit->user_id=$user_id;
            $recent_visit->rest_detail_id=$rest_id;
            $recent_visit->status=1;
            $recent_visit->created_by=$user_id;
            $recent_visit->created_at=date("Y-m-d H:i:s");
            $recent_visit->save();
        }
        
        $json['success']=1;
        echo json_encode($json);
        return;
    }

    public function set_pickup(Request $request) {
        $is_pickup = filter_var(Input::get("is_pickup"), FILTER_VALIDATE_BOOLEAN);
        Session::set("is_pickup", $is_pickup);
        $request->session()->set("is_pickup", $is_pickup);
        return response()->json([
            "status" => "success",
            "new_is_pickup" => $is_pickup
        ]);
    }

}
