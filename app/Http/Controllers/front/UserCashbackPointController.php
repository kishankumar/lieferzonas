<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\UserCashbackPoint;
class UserCashbackPointController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$auth_id = Auth::user('front')->id;
		$creditcpoint = UserCashbackPoint::where('user_id','=',$auth_id)
		->select('awarded_points')->get();
		
		$debitcpoint = UserCashbackPoint::where('user_id','=',$auth_id)
		->select('used_points')->get();
		//echo count($creditcpoint);
		
		$expcdate = date("Y-m-d H:i:s", strtotime(" -3 months"));
        
		$expirecpoint = UserCashbackPoint::where('user_id','=',$auth_id)->where('status','=',1)->where('created_at','<',$expcdate)
		->select('used_points','awarded_points')->get();
		
		$totalcpoint = UserCashbackPoint::where('user_id','=',$auth_id)->where('status','=',1)->where('created_at','>=',$expcdate)
		->select('used_points','awarded_points')->get();

        $cashbackhistory = DB::table('user_cashback_point_logs')
            ->leftjoin('set_cashback_points','user_cashback_point_logs.activity_id','=','set_cashback_points.id')
            ->where('user_cashback_point_logs.user_id','=',$auth_id)
            ->select('user_cashback_point_logs.*','set_cashback_points.cashback_title')
            ->paginate(10);

        //return view('front/useraccount/cashbackpoint')->with('creditcpoint',$creditcpoint)
        return view('front_new.user_profile.cashback')->with('creditcpoint',$creditcpoint)
		->with('debitcpoint',$debitcpoint)->with('expirecpoint',$expirecpoint)->with('totalcpoint',$totalcpoint)
            ->with('cashbackhistory',$cashbackhistory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
