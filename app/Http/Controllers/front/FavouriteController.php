<?php

namespace App\Http\Controllers\front;
date_default_timezone_set('Asia/Calcutta');
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\FrontController;
use App\superadmin\FrontCityMap;
use App\superadmin\FrontCountryMap;
use App\superadmin\FrontKitchenMap;
use App\superadmin\FrontStateMap;
use App\front\UserRestFavourite;
use App\superadmin\Rest_detail;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
class FavouriteController extends FrontController
{
    public function index()
    {
	    $auth_id = Auth::user('front')->id;
        $data['country'] = FrontCountryMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['state'] = FrontStateMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['city'] = FrontCityMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['kitchen'] = FrontKitchenMap::where('status','=','1')->orderBy('priority','asc')->get();
		$x = date("d-m-Y");
        $day_id = date('N', strtotime($x)); 
	    $restuarant = DB:: table('user_rest_favourites')
		->leftjoin('rest_details', 'user_rest_favourites.rest_id', '=', 'rest_details.id')
		->select('rest_details.id','rest_details.logo','rest_details.image','rest_details.f_name','rest_details.l_name',
		'user_rest_favourites.id as f_id')
		->where('rest_details.status','=','1')
		->where('user_rest_favourites.user_id','=',$auth_id)
		->paginate(10);
		
		for ($i = 0, $c = count($restuarant); $i < $c; ++$i) {
            $restuarant[$i] = (array) $restuarant[$i];
            
        } 
		

        // return view('front/useraccount/favourite')->with($data)->with('restuarant',$restuarant);
        return view('front_new.user_profile.favorites')->with($data)->with('restuarant',$restuarant);
    }
    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
       
    }

    public function update(Request $request, $id)
    {
		
    }

    public function destroy($id)
    {
        //
    }

    public function toggle_favourite(Request $request) {
        $rest_id = $request->input('rest_id');
        UserRestFavourite::toggle_favourite($rest_id);
        return response()->json([
            "status" => "success",
            "count" => UserRestFavourite::rest_favourite_count($rest_id),
            "isFavorite" => UserRestFavourite::is_rest_user_favourite($rest_id)
        ]);
    }

	function add_favourite()
	{
		
		$rest_id = Input::get('rest_id');
		$user_id = Input::get('user_id');
		$fav = db::table('user_rest_favourites')->where('rest_id',$rest_id)->where('user_id',$user_id)->select('id')->get();
		for ($i = 0, $c = count($fav); $i < $c; ++$i) {
            $fav[$i] = (array) $fav[$i];
            
        } 
		if(count($fav)=='0')
		{
			$restfavourite = new UserRestFavourite;
		    $restfavourite->user_id=$user_id;
			$restfavourite->rest_id=$rest_id;
			$restfavourite->status=1;
			$restfavourite->created_by=$user_id;
		    $restfavourite->created_at=date("Y-m-d H:i:s");
			$restfavourite->save();
		}
		else
		{
			$id = $fav['0']['id']; 
			DB::table('user_rest_favourites')->where('id', '=', $id)->delete();
			
		}
		$json['success']=1;
        echo json_encode($json);
        return;
	}
	 public function set_validation($id=null,$request)
     {
        $message=array(
            
            "firstname.required"=>"First name no is required",
            "lastname.required"=>"Last name is required",
            "nickname.required"=>"Nick name is required",
            "gender.required"=>"Gender is required",
            "dob.required"=>"Date of bith is required",
            "mobile.required"=>"Mobile is required",
          );

        $this->validate($request,[
        'firstname' => 'required',
        'lastname' => 'required',
        'nickname' => 'required',
        'gender' => 'required',
        'dob' => 'required',
        'mobile' => 'required|numeric',
        ],$message);
     }
}
