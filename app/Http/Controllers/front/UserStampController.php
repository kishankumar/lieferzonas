<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\UserStampRestCurrent;

class UserStampController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth_id = Auth::user('front')->id;
        $x = date("d-m-Y");
        $day_id = date('N', strtotime($x));
        $stamplist = UserStampRestCurrent::leftjoin('rest_details', 'user_stamp_rest_currents.rest_detail_id', '=', 'rest_details.id')
            ->where('user_stamp_rest_currents.user_id', $auth_id)
            ->select('user_stamp_rest_currents.rest_detail_id', 'user_stamp_rest_currents.stamp_count', 'rest_details.image', 'rest_details.id',
                'rest_details.f_name', 'rest_details.l_name', 'rest_details.logo')
            ->paginate(10);

        $stampcount = DB::table('rest_stamps')->select('stamp_count')->get();
        for ($i = 0, $c = count($stampcount); $i < $c; ++$i) {
            $stampcount[$i] = (array)$stampcount[$i];
        }
        // return view('front/useraccount/stamp')->with('stamplist',$stamplist)->with('stampcount',$stampcount);
        return view("front_new.user_profile.stamps");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
