<?php

namespace App\Http\Controllers\front;

use App\front\OrderPayment;
use App\superadmin\Promocode;
use App\superadmin\PromoRestMap;
use App\superadmin\PromoUserMap;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\DefaultController;
use Session;
use DB;
use Validator;
use Auth;

class FrontAjaxController extends DefaultController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getExistValOneCond(Request $request)
    {
        if ($request) {

            $firstVal = $request->firstValue;

            $table = $request->table;
            $firstCond = $request->firstCond;

            $id = $request->id;

            $values = DB::table($table)->select('id')
                ->where($firstCond, $firstVal)
                ->where('status', '!=', 2)
                ->where('id', '!=', $id)
                ->get();

            $count = count($values);
            if ($count > 0) {
                echo 'error';
            } else {
                echo 'success';
            }
        }
    }

    public function getAddress(Request $request)
    {
        $addressId = $request->addressId;
        $values = DB::table('front_user_addresses')->select('id', 'booking_person_name', 'address', 'landmark', 'zipcode', 'mobile')
            //->where('status','!=',2)
            ->where('id', $addressId)
            ->get();
        echo json_encode($values);

    }

    public function setAddress(Request $request)
    {
        $type = $request->data['type'];
        $zipcode = $request->data['zipcode'];

        $zipcodeDetails = DB::table('zipcodes')->where('status', '!=', '2')->where('name', $zipcode)->select('city_id')->get();
        if (count($zipcodeDetails) > 0) {
            $cityId = $zipcodeDetails[0]->city_id;
        } else {
            $cityId = 0;
        }


        if ($cityId > 0) {
            $cityDetails = DB::table('cities')->where('status', '!=', '2')->where('id', $cityId)->select('country_id', 'state_id')->get();
            if (count($cityDetails) > 0) {
                $stateId = $cityDetails[0]->state_id;
                $countryId = $cityDetails[0]->country_id;
            } else {
                $stateId = 0;
                $countryId = 0;
            }

        } else {
            $cityId = 0;
            $stateId = 0;
            $countryId = 0;
        }

        if ($type == 'edit') {
            $query = DB::table('front_user_addresses')
                ->where('id', $request->data['id'])
                ->where('front_user_id', $request->data['userId'])
                ->update(['booking_person_name' => $request->data['personName'], 'mobile' => $request->data['mobile'], 'address' => $request->data['address'],
                    'landmark' => $request->data['landmark'], 'zipcode' => $request->data['zipcode'],
                    'city_id' => $cityId, 'state_id' => $stateId, 'country_id' => $countryId, 'updated_at' => date('Y-m-d H:i:s')]);
        } else {
            $query = DB::table('front_user_addresses')->insert(
                ['front_user_id' => $request->data['userId'], 'booking_person_name' => $request->data['personName'], 'mobile' => $request->data['mobile'], 'address' => $request->data['address'],
                    'landmark' => $request->data['landmark'], 'zipcode' => $request->data['zipcode'],
                    'city_id' => $cityId, 'state_id' => $stateId, 'country_id' => $countryId,
                    'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            );
        }
        if ($query) {
            echo json_encode('success');
        } else {
            echo json_encode('error');
        }

    }

    public function deleteAddress(Request $request)
    {

        $addressId = $request->addressId;

        $query = DB::table('front_user_addresses')
            ->where('id', $addressId)
            ->update(['status' => 2]);

        if ($query) {
            echo json_encode('success');
        } else {
            echo json_encode('error');
        }

    }

    public function setTimePreOrder(Request $request)
    {

        $date = $request->date;
        $restroId = $request->rest;

        $dayId = date("N", strtotime($date));

        $closeTimeForToday = DB::table('rest_day_time_maps')->select('id', 'open_time', 'close_time')
            ->where('rest_detail_id', $restroId)
            ->where('day_id', $dayId)
            ->get();
        if (count($closeTimeForToday) > 0) {

            $startTime = $closeTimeForToday[0]->open_time;
            $endTime = $closeTimeForToday[0]->close_time;

        } else {

            $startTime = '01:00 AM';
            $endTime = '11:59 PM';
        }


        $hour_diff = round(abs(strtotime($startTime) - strtotime($endTime)) / 3600);
        if ($hour_diff > 23) {
            $hour_diff = 24;
        } else {
            $hour_diff = $hour_diff;
        }

        $t = strtotime($startTime);
        //$t=time();
        $data['options'] = '';
        for ($i = 0; $i < ($hour_diff - 1) * 4; $i++) {
            $t = $t + (15 * 60);
            $data['options'] .= '<option value="' . (date("h:i A", $t)) . '"> ' . (date("h:i A", $t)) . ' </option>';

        }
        echo json_encode($data);

    }

    public function checkCouponCode(Request $request)
    {

        $couponCode = $request->couponCode;
        $restId = $request->rest;
        $userId = Auth::check('front');
        $currDate = date('Y-m-d H:i:s');

        if (count(Session::get('cart')) > 0) {
            $totalCartAmount = 0;
            foreach (Session::get('cart') as $cartData) {
                $totalCartAmount = $totalCartAmount + $cartData['price'];
            }
        }

        $TotalOrder = OrderPayment::where('front_user_id', $userId)->count();

        $procodeDetails = Promocode:: where('status', 1)->where('valid_to', '>', $currDate)->where('coupon_code', $couponCode)->get();


        $count = count($procodeDetails);

        if ($count > 0) {

            $promoCodeId = $procodeDetails[0]->id;
            $promoMinAmount = $procodeDetails[0]->min_amount;
            $paymentMode = $procodeDetails[0]->payment_mode;
            $paymentAmount = $procodeDetails[0]->payment_amount;

            if ($paymentMode == 'P') {
                $newTotalAmount = $totalCartAmount - $totalCartAmount * $paymentAmount / 100;
            } else {
                $newTotalAmount = $totalCartAmount - $paymentAmount;
            }

            $isNewCustomer = $procodeDetails[0]->is_new_customer;
            if ($isNewCustomer == 1 and $TotalOrder > 0) {
                $data['error'] = 'This coupon is only for new customer';
            } else {
                $isUserSpecific = $procodeDetails[0]->is_user_specific;
                $isResturantSpecific = $procodeDetails[0]->is_rest_specific;

                if ($isResturantSpecific == 1 and $isUserSpecific == 0) {

                    $promoRestDetails = PromoRestMap:: where('promo_id', $promoCodeId)->select('rest_id')->get();

                    $existRest = '';
                    foreach ($promoRestDetails as $promoRestDetail) {

                        if ($promoRestDetail->rest_id == $restId) {

                            $existRest = 1;
                            break;
                        } else {
                            $existRest = 0;
                        }

                    }
                    if ($existRest == 0) {

                        $data['error'] = 'This coupon is not valid for this resturant';

                    } else {

                        if ($totalCartAmount < $promoMinAmount) {
                            $data['error'] = 'Your order amount must be greater than ' . $promoMinAmount;
                        } else {
                            $data['success'] = 'success';
                            $data['newTotal'] = $newTotalAmount;
                        }
                    }

                } elseif ($isResturantSpecific == 0 and $isUserSpecific == 1) {

                    $promoUserDetails = PromoUserMap::where('promo_id', $promoCodeId)->select('user_id')->get();
                    $existUser = '';

                    foreach ($promoUserDetails as $promoUserDetail) {

                        if ($promoUserDetail->user_id == $userId) {
                            $existUser = 1;
                            break;
                        } else {
                            $existUser = 0;
                        }
                    }

                    if ($existUser == 0) {
                        $data['error'] = 'Invalid coupon code';
                    } else {
                        if ($totalCartAmount < $promoMinAmount) {
                            $data['error'] = 'Your order amount must be greater than ' . $promoMinAmount;
                        } else {
                            $data['success'] = 'success';
                            $data['newTotal'] = $newTotalAmount;
                        }
                    }

                } elseif ($isResturantSpecific == 1 and $isUserSpecific == 1) {


                    $promoRestDetails = PromoRestMap:: where('promo_id', $promoCodeId)->select('rest_id')->get();

                    $existRest = '';
                    foreach ($promoRestDetails as $promoRestDetail) {

                        if ($promoRestDetail->rest_id == $restId) {

                            $existRest = 1;
                            break;
                        } else {
                            $existRest = 0;
                        }

                    }

                    $promoUserDetails = PromoUserMap::where('promo_id', $promoCodeId)->select('user_id')->get();
                    $existUser = '';

                    foreach ($promoUserDetails as $promoUserDetail) {

                        if ($promoUserDetail->user_id == $userId) {
                            $existUser = 1;
                            break;
                        } else {
                            $existUser = 0;
                        }
                    }

                    if ($existUser == 1 and $existRest == 1) {
                        if ($totalCartAmount < $promoMinAmount) {
                            $data['error'] = 'Your order amount must be greater than ' . $promoMinAmount;
                        } else {
                            $data['success'] = 'success';
                            $data['newTotal'] = $newTotalAmount;
                        }
                    } else {
                        $data['error'] = 'Invalid coupon code';
                    }

                } else {
                    if ($totalCartAmount < $promoMinAmount) {
                        $data['error'] = 'Your order amount must be greater than ' . $promoMinAmount;
                    } else {
                        $data['success'] = 'success';
                        $data['newTotal'] = $newTotalAmount;
                    }
                }
            }

        } else {
            $data['error'] = 'Invalid coupon code';
        }

        echo json_encode($data);

    }


}
