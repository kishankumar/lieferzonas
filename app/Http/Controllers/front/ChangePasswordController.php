<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Session;
use DB;
use Hash;
use Validator;
use Auth;
use App\front\FrontUser;
class ChangePasswordController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('front/useraccount/change_password');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$auth_id = Auth::user('front')->id;
		$user = DB::table('front_users')->where('id','=',$auth_id)->select('password')->get();
		for ($i = 0, $c = count($user); $i < $c; ++$i) 
		{
            $user[$i] = (array) $user[$i];
            
        }
		if (Hash::check($request->oldpassword, $user['0']['password'])) 
		{
            $newpass = $request->newpassword;
		    $conpass = bcrypt($request->conpassword);
			$changepass = FrontUser::find($auth_id);
			$changepass->password=$conpass;
			$changepass->save();
			Session::flash('message','Password changed successfully.');
	        return redirect('front/changepass');
        }
		else
		{
			Session::flash('message','Old password is not match!');
	        return redirect('front/changepass');
		}
			
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
}
