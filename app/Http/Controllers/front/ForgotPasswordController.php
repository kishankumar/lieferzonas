<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\DefaultController;
use Session;
use DB;
use Validator;
use Auth;
use App\front\FrontUser;
use Mail;

class ForgotPasswordController extends DefaultController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$frontuser = FrontUser::where('activation_key',$request->act_key)->get();
        $frontuser =$frontuser->toArray();
		//print_r($frontuser); die;
		$count = count($frontuser); 
		
		if($count>0)
		{
			$activation_key = '';
			$id = $frontuser['0']['id'];
			$fuser=FrontUser::find($id);
			$fuser->password = bcrypt($request->conpassword);
            $fuser->activation_key = $activation_key;			
			$fuser->save();
			Session::flash('message','Password reset successfully.');
            return redirect("front/resetpasswordsuccess");
		}
		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    function sendforgotpassmail(Request $request)
	{
		if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
	    $messages = array(
		    
			"email.required" =>"Please enter delivery email",
            "recaptcha.required"=>"Please enter captcha"
        );
		$userData = array(
           
		   
		   'email' => $data['email'],
		   'recaptcha'=>$data['recaptcha']
		   
        );
        
		$rules = array(
		    
			"email" => 'required|Email',
            "recaptcha"=>'required'  
			
        );
        $validation  = Validator::make($userData,$rules,$messages); 
	    if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
		else{
			/* mail */
			$uemail = Input::get('email');
	
			$frontuser = FrontUser::where('email',$uemail)->get();
			$frontuser =$frontuser->toArray();
			
			$count = count($frontuser); 
			$activation_key = base64_encode($uemail);
			
			$random_var = rand(1, 100);
		
			$act_key = $activation_key.$random_var;
			if($count>0)
			{
				$id = $frontuser['0']['id'];
				$fuser=FrontUser::find($id);
				$fuser->activation_key = $act_key; 
				$fuser->save();
				$from_email = 'vikas.alivenetsolutions@gmail.com';
				$from_name = 'Lieferzonas Superadmin';
				$base_host = $_SERVER['SERVER_NAME'];
				$link = $base_host.'/lieferzonas/front/resetpassword/'.$act_key;
				//$link = "http://localhost/lieferzonas/front/resetpassword/".$act_key."";
				$data = array(
					
					'uemail' => $uemail,
					'link' => $link
				);
				
				$pageuser = 'front.emails.forgotpass.touser';
				$subject = 'Forgot Password';
				$this->sendusermail($pageuser,$data,$subject,$from_email,$from_name,$uemail);
				
				
				 /* mail */
				//Session::flash('message', 'Successfully!'); 
				$json['success']=1;
				echo json_encode($json);
				return;
			}
			else{
				$json['success']=0;
				echo json_encode($json);
				return;
			}
		}
	   
		
		
		
		
	}
	public function sendusermail($pageuser,$data,$subject,$from_email,$from_name,$uemail)
	{
		Mail::send($pageuser, $data, function($messa) use ($subject,$from_email,$from_name,$uemail)  {
            $messa->from($from_email, $from_name);
            $messa->to($uemail)->subject($subject);
           
        });
	}
	public function resetpassword($act_key=0)
    {
		$frontuser = FrontUser::where('activation_key',$act_key)->get();
        $frontuser =$frontuser->toArray();
		//print_r($frontuser); die;
		$count = count($frontuser); 
		if($count>0)
		{
			return view('front/useraccount/reset_password')->with('act_key',$act_key);
		}
		else
		{
			 return redirect("front/invalidpasswordurl");
		}
		
	}
	public function Invalidurl()
	{
		return view('front/useraccount/password_reseted');
	}
    public function successresetmsg()
	{
		return view('front/useraccount/success_resetpassmsg');
	}
}
