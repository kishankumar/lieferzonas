<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Auth;
use App\superadmin\FrontCityMap;
use App\superadmin\FrontCountryMap;
use App\superadmin\FrontKitchenMap;
use App\superadmin\FrontStateMap;

class DashboardController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$user = Auth::user('front');
        //echo '<pre>';print_r($user);die;
        //echo "<a href='".action('Auth\FrontAuthController@getLogout')."'>Logout</a>";
        $data['country'] = FrontCountryMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['state'] = FrontStateMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['city'] = FrontCityMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['kitchen']=FrontKitchenMap::where('status','=','1')->orderBy('priority','asc')->get();
        // return view('front/home/index')->with($data);
        return view('front_new.user_profile.dashboard')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
