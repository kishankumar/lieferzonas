<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Session;
use DB;
use Input;
use Validator;
use Auth;
use App\superadmin\RestDetail;
use App\front\RestComplaint;
use App\superadmin\UserMaster;
use App\front\FrontUserDetail;

class RestComplaintController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status=(string)1;
        $front_user_id = Auth::User('front')->id;
        $data['user_details'] = FrontUserDetail::where('front_user_id', $front_user_id)->first();
        //$data['restro_names'] = DB::table('rest_details')->where('status',1)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();
        $data['restro_names'] = DB::table('user_orders')
            ->join('rest_details', 'rest_details.id', '=', 'user_orders.rest_detail_id')
            ->select('user_orders.front_user_id','rest_details.id', 'rest_details.f_name')
            ->where('rest_details.status','1')
            ->where('user_orders.front_order_status_id',$status)
            ->where('user_orders.front_user_id','=',$front_user_id)
            ->groupBy('user_orders.rest_detail_id')
            ->get();
        $data['order_detail'] = DB::table('user_orders')->where('status',1)->where('front_user_id',$front_user_id)->orderBy('created_at', 'desc')->select('id','order_id')->get();
        //echo '<pre>';print_r($data['user_details']);die;
        return view('front/complaint/complaint')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message=array(
            "restaurant.required"=>"Please select Restaurant",
            "orders.required"=>"Please select Order",
            "topic.required"=>"Please enter topic",
            "topic.min"=>"Topic must be at least 3 characters.",
            "description.required"=>"Please enter Description",
            "description.min"=>"Description must be at least 6 characters.",
            "name.required"=>"Please enter Name",
            "name.min"=>"Name must be at least 3 characters.",
            "email.required"=>"Please enter Email",
            "email.email"=>"Enter valid email",
            'phone.required'=>'Please enter Phone',
            'phone.min'=>'Phone must be at least 10 numbers.',
            'phone.max'=>'Phone must be maximum 12 numbers.',
        );
        $this->validate($request,[
            'restaurant'=>'required|integer',
            'orders'=>'required',
            'topic'=>'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            'description'=>'required|min:6',
            'name'=>'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            'email'=>'required|Regex:/^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i',
            'phone'=>'required|min:10|max:12|Regex: /^[0-9]{1,45}$/',
        ],$message);
        
        $complaint= new RestComplaint;
        $complaint->rest_id=$request->restaurant;
        $complaint->order_id=$request->orders;
        $complaint->topic=$request->topic;
        $complaint->description=$request->description;
        $complaint->name=$request->name;
        $complaint->email=$request->email;
        $complaint->phone=$request->phone;
        $complaint->created_by=Auth::User('front')->id;
        $complaint->updated_by=Auth::User('front')->id;
        $complaint->created_at=date("Y-m-d H:i:s");
        $complaint->updated_at=date("Y-m-d H:i:s");
        $complaint->status=1;
        $complaint->save();
        
        Session::flash('message', 'Complaint Added Successfully!');
        return redirect(route('front.complaint.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getdata(Request $request){
        if($request) {
            $restId = $request->restId;
            $front_user_id = Auth::User('front')->id;
            
            $values= DB ::table('user_orders')
		->leftjoin('front_order_status_maps','user_orders.order_id', '=','front_order_status_maps.order_id')
		->where('front_order_status_maps.status_id',7)
                ->where('user_orders.rest_detail_id',$restId)
		->where('user_orders.front_user_id',$front_user_id)
		->select('user_orders.order_ids')
		->get();
            
//            $values = DB::table('user_orders')
//                ->select('user_orders.order_id')
//                ->where('user_orders.rest_detail_id',$restId)
//                ->where('user_orders.front_user_id',$front_user_id)
//                ->get();
            $option='<option value="">Select Order</option>';
            foreach ($values as $value ) {
                $option .='<option value="'.$value->order_id.'">'.$value->order_id.'</option>';
            }
            echo $option;
        }else{
            $option='<option value="">Select Order</option>';
            echo $option;
        }
    }
}
