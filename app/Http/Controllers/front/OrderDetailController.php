<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\front\OrderItem;
use App\front\UserOrder;
class OrderDetailController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function order_detail($id)
	{
		$auth_id = Auth::user('front')->id;
        $userorderdetail = DB ::table('user_orders')
		->leftjoin('front_user_addresses',  'user_orders.front_user_address_id', '=','front_user_addresses.id')
		->leftjoin('countries',  'front_user_addresses.country_id', '=','countries.id')
		->leftjoin('states',  'front_user_addresses.state_id', '=','states.id')
		->leftjoin('cities',  'front_user_addresses.city_id', '=','cities.id')
		->where('user_orders.front_user_id',$auth_id)
		->where('user_orders.id',$id)
		->where('user_orders.status',1)
		->select('front_user_addresses.booking_person_name','front_user_addresses.mobile','front_user_addresses.zipcode','front_user_addresses.address','front_user_addresses.landmark','user_orders.*','countries.name as country_name','states.name as state_name','cities.name as city_name')
		
		->get();
		for ($i = 0, $c = count($userorderdetail); $i < $c; ++$i) {
            $userorderdetail[$i] = (array) $userorderdetail[$i];
        } 
        //return  $userorderdetail;
        return view('front.order_detail')->with('uorderdetail',$userorderdetail);
	}	
    
     
}
