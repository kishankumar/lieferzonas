<?php

namespace App\Http\Controllers\front;

use App\Front;
use App\front\FrontUserDetail;
use App\front\FrontUserEmailVerification;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function register(Request $request)
    {
        $check = Front::where('email',$request->email)->first();
        if(count($check))
        {
            echo 2;
        }
        else
        {
            $reg = new Front;
            $reg->firstname = $request->fname;
            $reg->lastname = $request->lname;
            $reg->email = $request->email;
            $reg->password = bcrypt($request->password);
            $reg->save();
            
            $regdetail = new FrontUserDetail;
            $regdetail->front_user_id = $reg->id;
            $regdetail->email = $request->email;
            $regdetail->fname = $request->fname;
            $regdetail->lname = $request->lname;
            $regdetail->registered_via = '1';
            if($regdetail->save())
            {
                /*$email = $request->email;
                $ver_code = preg_replace('/[^A-Za-z0-9\-]/', '',base64_encode($email));
                $ver_code = $ver_code.time();
                $ver_link = url("register/verifyemail/".$ver_code);
                //dd($ver_link);
                
                $save_link = new FrontUserEmailVerification;
                $save_link->front_user_id = $reg->id;
                $save_link->verification_code = $ver_code;
                $save_link->email_id = $email;
                
                if($save_link->save())
                {
                    $subject = "Email verification mail from Lieferzonas.";
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
                    $headers .= 'From: donotreply@lieferzonas.at' ."\r\n";
                    $msg = "Please click on the below link to verify your email. <br> <a href='".$ver_link."'>Verify Email</a>";
                    if(mail($email,$subject,$msg,$headers))
                        echo 1;
                }
                else
                {
                    echo 3;
                }*/
                echo 1;
            }
            else
            {
                echo 3;
            }
        }
    }
    
    /*public function verifyemail($code)
    {
        $success = FrontUserEmailVerification::where('verification_code',$code)->first();
        if(count($success))
        {
            if($success->is_verified == 1)
            {
                $success->is_verified = 1;
                $success->status = '1';
                $success->save();

                $success1 = FrontUserDetail::where('front_user_id',$success->front_user_id)->first();
                $success1->status = '1';
                $success1->save();

                $success2 = Front::find($success->front_user_id);
                $success2->status = 1;
                if($success2->save())
                    $message = "done";
                else
                    $message = "error";
            }
            else
            {
                $message = "already";
            }
        }
        else
        {
            $message = "notfound";
        }

        return view('front/message/index')->with('message',$message);
    }*/
}
