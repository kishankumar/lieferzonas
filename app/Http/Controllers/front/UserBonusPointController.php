<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\UserBonusPoint;
class UserBonusPointController extends FrontController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$auth_id = Auth::user('front')->id;
		$creditbpoint = UserBonusPoint::where('user_id','=',$auth_id)
		->select('awarded_points')->get();
		
		$debitbpoint = UserBonusPoint::where('user_id','=',$auth_id)
		->select('used_points')->get();
		
		$expcdate = date("Y-m-d H:i:s", strtotime(" -3 months"));
        
		$expirebpoint = UserBonusPoint::where('user_id','=',$auth_id)->where('status','=',1)->where('created_at','<',$expcdate)
		->select('used_points','awarded_points')->get();
		
		$totalbpoint = UserBonusPoint::where('user_id','=',$auth_id)->where('status','=',1)->where('created_at','>=',$expcdate)
		->select('used_points','awarded_points')->get();

        $bonushistory = DB::table('user_bonus_point_logs')
            ->leftjoin('set_bonus_points','user_bonus_point_logs.activity_id','=','set_bonus_points.id')
            ->where('user_bonus_point_logs.user_id','=',$auth_id)
            ->select('user_bonus_point_logs.*','set_bonus_points.activity_title')
            ->paginate(10);

        // return view('front/useraccount/bonuspoint')->with('creditbpoint',$creditbpoint)
        return view('front_new.user_profile.bonuspoint')->with('creditbpoint',$creditbpoint)
		->with('debitbpoint',$debitbpoint)->with('expirebpoint',$expirebpoint)->with('totalbpoint',$totalbpoint)->with('bonushistory',$bonushistory);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
