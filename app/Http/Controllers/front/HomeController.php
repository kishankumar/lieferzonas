<?php

namespace App\Http\Controllers\front;

use App\superadmin\Zipcode;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\DefaultController;
use App\superadmin\FrontCityMap;
use App\superadmin\FrontCountryMap;
use App\superadmin\FrontKitchenMap;
use App\superadmin\FrontStateMap;
use DB;

class HomeController extends DefaultController
{
    public function index()
    {
        $data['country'] = FrontCountryMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['state'] = FrontStateMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['city'] = FrontCityMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['kitchen'] = FrontKitchenMap::where('status','=','1')->orderBy('priority','asc')->get();

        $data['zipcode'] = Zipcode::where('status','=','1')->get();
		
		
        // return view('front/home/index')->with($data);
        return view('front_new.homepage')->with($data);
    }
    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function get_zipcodes(Request $request)
    {
        $zip = $request->term;
        $zipcode = DB::table('zipcodes')->select('name','description')->where('status','=','1')->where('name','like','%'.$zip.'%')->orWhere('description','like','%'.$zip.'%')->get();
        $i = 0 ;
        foreach ($zipcode as $item)
        {
            $data[$i]['label'] = $item->name;
            $data[$i]['value'] = $item->description;
            $i++;
        }

        echo json_encode($data);
    }
}
