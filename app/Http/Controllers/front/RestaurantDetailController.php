<?php

namespace App\Http\Controllers\front;

use App\front\UserRecentRestVisit;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\DefaultController;
use Session;
use DB;
use Input;
use App\superadmin\RestDetail;
use App\superadmin\RestCategory;
use App\superadmin\Alergic_content;
use App\front\RestComplaint;
use App\superadmin\RestDayTimeMap;
use App\superadmin\Rest_day;
use App\superadmin\RestOwner;
use App\superadmin\RestZipcodeDeliveryMap;
use App\front\UserOrderReview;
use App\front\FrontUserDetail;
use Validator;
use Auth;

class RestaurantDetailController extends DefaultController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    function getDetails(Request $request,$restroId=0){
        if($restroId){
            $count= RestDetail :: where('id','=',$restroId)->where('status', '1')->count();
            if($count < 1){
                Session::flash('message', 'Please select restaurant again.'); 
                //return redirect(route('root.zipcodes.setting.create'));
            }
            if( ! preg_match('/^\d+$/', $restroId) ){
                Session::flash('message', 'Please select restaurant again.'); 
                //return redirect(route('root.zipcodes.setting.create'));
            }
        }
        $data['restNames'] = RestDetail :: where('id',$restroId)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();
        $data['restCategories'] = RestCategory :: where('rest_detail_id',$restroId)
            ->where('status',1)
            ->select('id','name','description','image')
            ->orderBy('name', 'asc')->get();
        $data['allergicContents'] = Alergic_content :: where('status','1')->select('id','name')->get();
        $data['restId'] = $restroId;
        $data['restComplaint'] = RestComplaint:: where('rest_id','=',$restroId)->where('status', '=','1')->orderBy('created_at', 'desc')->paginate(5);
        $dayname = date('l');
        $days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
        $day_number = array_search($dayname, $days);
        $data['dayTime'] = RestDayTimeMap:: where('rest_detail_id','=',$restroId)->where('status','1')->where('day_id','=',$day_number)->first();
        $data['restDayTiming'] = DB::table('rest_details')
                ->join('rest_day_time_maps', 'rest_details.id', '=', 'rest_day_time_maps.rest_detail_id')
                ->join('rest_days', 'rest_days.id', '=', 'rest_day_time_maps.day_id')
                ->where('rest_details.status','!=', '2')
                ->where('rest_details.id','=', $restroId)
                ->orderBy('rest_days.id', 'asc')
                ->select('rest_details.*', 'rest_day_time_maps.day_id','rest_day_time_maps.open_time','rest_day_time_maps.close_time','rest_day_time_maps.is_open', 'rest_day_time_maps.is_24', 'rest_days.day')
                ->get();
        $data['AllrestDetail'] = RestDetail::where('id',$restroId)->where('status',1)->first();
        $data['ownerDetails'] = RestOwner::where('rest_detail_id',$restroId)->where('status',1)->first();
        $data['RestZipcodeDeliveryMap'] = RestZipcodeDeliveryMap::where('rest_id',$restroId)->where('status',1)->get();
        //$data['RestReviews'] = UserOrderReview::where('rest_id',$restroId)->where('status',1)->orderBy('created_at', 'desc')->paginate(5);

        
        //echo '<pre>';print_r($data);die;
        //echo '<pre>';print_r(Session::all());die;
        return view('front/restaurantMenu/menu')->with($data);
    }

    public function getRestaurantDetails($restaurant_id) {
        UserRecentRestVisit::registerVisit($restaurant_id);
        $rest_detail_row = RestDetail::where("status", 1)->where("id", $restaurant_id)->first();
        if (!$rest_detail_row) {
            return abort(404);
        }
        $rest_display_information = RestDetail::getRestaurantDisplayInformationArray($restaurant_id);
        return view("front_new.restaurants.menu")->with($rest_display_information)->with("rest_detail", $rest_detail_row);
    }
}
