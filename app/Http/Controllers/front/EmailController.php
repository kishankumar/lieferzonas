<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\DefaultController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class EmailController extends DefaultController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendEmail(Request $request) {
        $sender = $request->input("sender");
        $text = $request->input("text");
        if (!filter_var($sender, FILTER_VALIDATE_EMAIL)) {
            return response()->json([
                "status" => "failure",
                "reason" => "E-Mail Adresse ungültig!"
            ]);
        }
        Mail::raw($text, function ($message) use (&$request, &$sender) {
            $message->subject("E-Mail durch Homepage");
            $message->to("support@lieferzonas.at");
            $message->replyTo($sender);
        });
        return response()->json([
            "status" => "success"
        ]);
    }
}
