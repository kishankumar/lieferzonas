<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\FrontController;
use App\superadmin\FrontCityMap;
use App\superadmin\FrontCountryMap;
use App\superadmin\FrontKitchenMap;
use App\superadmin\FrontStateMap;
use App\front\FrontUserDetail;
use App\front\FrontUser;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
class ProfileController extends FrontController
{
    public function index()
    {
	
        $data['country'] = FrontCountryMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['state'] = FrontStateMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['city'] = FrontCityMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['kitchen'] = FrontKitchenMap::where('status','=','1')->orderBy('priority','asc')->get();
	    
        return view('front/useraccount/profile')->with($data);
    }
    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
		$auth_id = Auth::user('front')->id;
		
		if($auth_id!=$id)
		{
			return redirect()->action('front\DashboardController@index');
		}
        $profile = DB::table('front_user_details')
		 ->leftjoin('front_users', 'front_user_details.front_user_id', '=', 'front_users.id')
		 ->select('front_user_details.*')->where('front_users.id','=',$id)->get();
		for ($i = 0, $c = count($profile); $i < $c; ++$i) {
            $profile[$i] = (array) $profile[$i];
            
        }
		//print_r($profile); die;
		// return view('front/useraccount/profile')->with('profile',$profile);
        return view('front_new.user_profile.data')->with('profile',$profile);
    }

    public function update(Request $request, $id)
    {
		$this->set_validation($id,$request);
		$user_id  = $request->id;
        $profile=FrontUserDetail::find($user_id);
		$profile->fname = $request->firstname;
		$profile->lname = $request->lastname;
		$profile->nickname = $request->nickname;
		$profile->gender = $request->gender;
		$profile->mobile = $request->mobile;
		$profile->dob = $request->dob;
		$extension = '';
        $destinationpath=public_path()."/front/uploads/users";
		$pic=Input::file('pics');
		if($pic!='')
         {
             $extension=  $pic->getClientOriginalExtension(); // getting image extension
             $filename=time().rand(111,999).'.'.$extension; // renameing image
             $pic->move($destinationpath,$filename);
             $profile->profile_pic=$filename;
         } 
		$profile->save();
		Session::flash('message','Updated Successfully');
	    return redirect('front/profile/'.$user_id.'/edit');
    }

    public function destroy($id)
    {
        //
    }
	 public function set_validation($id=null,$request)
     {
        $message=array(
            
            "firstname.required"=>"Vorname muss ausgefüllt werden",
            "lastname.required"=>"Nachname muss ausgefüllt werden",
            "nickname.required"=>"Benutzername muss ausgefüllt werden",
            "gender.required"=>"Geschlecht muss ausgefüllt werden",
            "dob.required"=>"Geburtsdatum muss ausgefüllt werden",
            "mobile.required"=>"Mobilnummer muss ausgefüllt werden",
			"pics.mimes"=>"please choose profile pics of type jpg,png,tif,bmp",
          );

        $this->validate($request,[
        'firstname' => 'required',
        'lastname' => 'required',
        'nickname' => 'required',
        'gender' => 'required',
        'dob' => 'required',
        'mobile' => 'required|numeric',
		'pics' => 'mimes:jpeg,bmp,png,tif',
        ],$message);
     }
}
