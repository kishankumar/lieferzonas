<?php

namespace App\Http\Controllers\front;

use App\front\FrontUserAddress;
use App\Http\Controllers\DefaultController;
use App\superadmin\City;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use League\Flysystem\Exception;

class UserAddressController extends DefaultController
{

    protected function getSessionAddresses($request) {
        if (!$request->session()->has("addresses")) {
            $request->session()->put("addresses", array());
        }
        return $request->session()->get("addresses");
    }

    protected function setSessionAddresses($request, $addresses) {
        return $request->session()->put("addresses", $addresses);
    }

    public function index(Request $request)
    {
        if (Auth::check("front")) {
            $addresses = FrontUserAddress::where("status", 1)->where("front_user_id", Auth::user("front")->id)->get();
            return $addresses;
        }
        else {
            $addresses = $this->getSessionAddresses();
            return json_encode($addresses);
        }
    }

    public function create()
    {
        //
    }

    protected function storeInDatabase($request) {
        $name = $request->input("name");
        $street = $request->input("street");
        $house = $request->input("house");
        $staircase = $request->input("staircase");
        $floor = $request->input("floor");
        $apartment = $request->input("apartment");
        $address_string = lie_format_address($street, $house, $staircase, $floor, $apartment);

        $zipcode = $request->input("zipcode");
        $city_name = $request->input("city");
        $phone_number = $request->input("phone_number");
        $company = $request->input("company");
        $info = $request->input("info");

        $new_address = new FrontUserAddress;
        $new_address->front_user_id = Auth::user('front')->id;
        $new_address->booking_person_name = $name;
        $new_address->mobile = $phone_number;
        $new_address->address = $address_string;
        $new_address->street = $street;
        $new_address->house = $house;
        $new_address->staircase = $staircase;
        $new_address->floor = $floor;
        $new_address->apartment = $apartment;
        $new_address->landmark = $info;
        $new_address->company = $company;
        $new_address->is_type = "a";

        $city = City::getByZipcodeAndName($zipcode, $city_name);
        if (!$city) {
            return response()->json([
                "status" => "failure",
                "reason" => "Ungültige Stadt"
            ]);
        }
        $new_address->zipcode = $zipcode;
        $new_address->city_id = $city->city_id;
        $new_address->state_id = $city->state_id;
        $new_address->country_id = $city->country_id;

        $new_address->status = "1";

        $coordinates = City::getCoordinates($address_string, $zipcode, $city_name);
        $new_address->latitude = $coordinates["latitude"];
        $new_address->longitude = $coordinates["longitude"];

        $new_address->save();

        return response()->json([
            "status" => "success"
        ]);
    }

    protected function storeInSession($request) {
        $name = $request->input("name");
        $street = $request->input("street");
        $numbers = $request->input("numbers");
        $zipcode = $request->input("zipcode");
        $city_name = $request->input("city");
        $phone_number = $request->input("phone_number");
        $company = $request->input("company");
        $info = $request->input("info");
        throw new \Exception("Unimplemented");
    }

    public function store(Request $request)
    {
        if (Auth::check('front')) {
            return $this->storeInDatabase($request);
        }
        else {
            return $this->storeInSession($request);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    protected function updateInDatabase($request, $id) {
        $name = $request->input("name");
        $street = $request->input("street");
        $house = $request->input("house");
        $staircase = $request->input("staircase");
        $floor = $request->input("floor");
        $apartment = $request->input("apartment");
        $address_string = lie_format_address($street, $house, $staircase, $floor, $apartment);

        $zipcode = $request->input("zipcode");
        $city_name = $request->input("city");
        $phone_number = $request->input("phone_number");
        $company = $request->input("company");
        $info = $request->input("info");

        $new_address = FrontUserAddress::find($id);
        if (!$new_address) throw new \Exception("Ungültige Adresse!");
        $new_address->booking_person_name = $name;
        $new_address->mobile = $phone_number;
        $new_address->address = $address_string;
        $new_address->street = $street;
        $new_address->house = $house;
        $new_address->staircase = $staircase;
        $new_address->floor = $floor;
        $new_address->apartment = $apartment;
        $new_address->landmark = $info;
        $new_address->company = $company;
        $new_address->is_type = "a";

        $city = City::getByZipcodeAndName($zipcode, $city_name);
        if (!$city) {
            return response()->json([
                "status" => "failure",
                "reason" => "Ungültige Stadt"
            ]);
        }
        $new_address->zipcode = $zipcode;
        $new_address->city_id = $city->city_id;
        $new_address->state_id = $city->state_id;
        $new_address->country_id = $city->country_id;

        $new_address->status = "1";

        $coordinates = City::getCoordinates($address_string, $zipcode, $city_name);
        $new_address->latitude = $coordinates["latitude"];
        $new_address->longitude = $coordinates["longitude"];

        $new_address->save();

        return response()->json([
            "status" => "success"
        ]);
    }

    protected function updateInSession($request, $id) {

    }

    public function update(Request $request, $id)
    {
        if (Auth::check('front')) {
            return $this->updateInDatabase($request, $id);
        }
        else {
            return $this->updateInSession($request, $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check('front')) {
            $address = FrontUserAddress::find($id);
            if ($address->front_user_id == Auth::user('front')->id) {
                $address->status = 2;
                $address->save();
                return response()->json([
                    "status" => "success"
                ]);
            }
            else {
                return response()->json([
                    "status" => "failure",
                    "reason" => "Keine Berechtigung zu dieser Aktion!"
                ]);
            }
        }
        else {
            return response()->json([
                "status" => "failure",
                "reason" => "Keine Berechtigung zu dieser Aktion!"
            ]);
        }
    }

    public function addressesJson() {
        return FrontUserAddress::user_addresses_json();
    }
}
