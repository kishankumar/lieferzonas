<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\FrontController;
use App\superadmin\FrontCityMap;
use App\superadmin\FrontCountryMap;
use App\superadmin\FrontKitchenMap;
use App\superadmin\FrontStateMap;
use App\front\FrontUserDetail;
use App\front\FrontUserAddress;
use Session;
use DB;
use Auth;
use validator;
class AddressController extends FrontController
{
    public function index()
    {
	    $auth_id = Auth::user('front')->id;
        $data['country'] = FrontCountryMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['state'] = FrontStateMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['city'] = FrontCityMap::where('status','=','1')->orderBy('priority','asc')->get();
        $data['kitchen'] = FrontKitchenMap::where('status','=','1')->orderBy('priority','asc')->get();
	    //$address = FrontUserAddress::where('status','!=',2)->get();
		$address= DB::table('front_user_addresses')
       
		->leftjoin('countries', 'front_user_addresses.country_id', '=', 'countries.id')
		->leftjoin('cities', 'front_user_addresses.city_id', '=', 'cities.id')
		->leftjoin('states', 'front_user_addresses.state_id', '=', 'states.id')
		->leftjoin('zipcodes', 'front_user_addresses.zipcode', '=', 'zipcodes.id')
		->where('front_user_addresses.status','=',1)
		->where('front_user_addresses.front_user_id','=',$auth_id)
		->select('front_user_addresses.*','countries.name as country_name','states.name as state_name','cities.name as city_name','zipcodes.name as zipcode_name')->get();
		for ($i = 0, $c = count($address); $i < $c; ++$i) {
            $address[$i] = (array) $address[$i];
        }
		//print_r($address); die;
        // return view('front/useraccount/address')->with($data)->with('address',$address);
        return view("front_new.user_profile.addresses");
    }
    
    public function create()
    {
		$country = DB::table('countries')->select('id', 'name')->orderBy('name', 'asc')->where('status','=','1')->get();
		for ($i = 0, $c = count($country); $i < $c; ++$i) {
            $country[$i] = (array) $country[$i];
            
        }
        return view('front/useraccount/create_address')->with('country',$country);
    }

    public function store(Request $request)
    {
		$this->set_validation('',$request);
		$user_add=FrontUserAddress::where('status','=',1)->get();
        $user_addcount = count($user_add->toArray()); 
		if($user_addcount>0)
		{
		  $is_type='s';
		}
		else
		{
			$is_type='p';
		}
        $auth_id = Auth::user('front')->id;
		$address = new FrontUserAddress();
		$address->mobile=$request->mobile;
		$address->front_user_id = $auth_id;
		$address->address=$request->address;
		$address->landmark=$request->landmark;
		$address->zipcode=$request->zipcode;
		$address->city_id=$request->city;
		$address->state_id=$request->state;
		$address->country_id=$request->country;
		$address->created_by = $auth_id;
		$address->created_at=date("Y-m-d H:i:s");
		$address->status=1;
		$address->is_type=$is_type;
		$address->save();
		Session::flash('message','Added Successfully');
		return redirect(route('front.address.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
		$address = DB::table('front_user_addresses')->select('*')->where('id','=',$id)->get();
		for ($i = 0, $c = count($address); $i < $c; ++$i) {
            $address[$i] = (array) $address[$i];
            
        }
		//print_r($address); die;
	    $country = DB::table('countries')->select('id', 'name')->orderBy('name', 'asc')->where('status','=','1')->get();
		for ($i = 0, $c = count($country); $i < $c; ++$i) {
            $country[$i] = (array) $country[$i];
            
        }
		$state = DB::table('states')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($state); $i < $c; ++$i) {
            $state[$i] = (array) $state[$i];
            
        } 
        $city = DB::table('cities')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($city); $i < $c; ++$i) {
            $city[$i] = (array) $city[$i];
            
        } 
		$zipcode = DB::table('zipcodes')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($zipcode); $i < $c; ++$i) {
            $zipcode[$i] = (array) $zipcode[$i];
            
        } 
		
      return view('front/useraccount/edit_address')->with('address',$address)->with('country',$country)->with('state',$state)
	  ->with('city',$city)->with('zipcode',$zipcode);
	  
    }

    public function update(Request $request, $id)
    {
		$this->set_validation($id,$request);
	    $auth_id = Auth::user('front')->id;
        $address=FrontUserAddress::find($id);
		$address->mobile=$request->mobile;
		$address->front_user_id = $auth_id;
		$address->address=$request->address;
		$address->landmark=$request->landmark;
		$address->zipcode=$request->zipcode;
		$address->city_id=$request->city;
		$address->state_id=$request->state;
		$address->country_id=$request->country;
		$address->updated_by = $auth_id;
		$address->updated_at=date("Y-m-d H:i:s");
		$address->save();
		Session::flash('message','Updated Successfully');
		return redirect(route('front.address.index'));
    }

    public function destroy($id)
    {
      //return redirect(route('front.address.index'));
    }
	public function delete($id)
    {
	  $address = FrontUserAddress::find($id);
	  $address->status = '2';
      $address->save();
	  Session::flash('message','Deleted Successfully');
      return redirect(route('front.address.index'));
    }
	public function getDatas(Request $request)
    {
        if($request) {
            $table=$request->table;
            $mappingTable=$request->maptable;
            $cond=$request->value;
            $joinColumn=$request->joincolumn;
            $selectColumn=$request->selectcolumn;
            $whereColumn=$request->wherecolumn;
            $secondjoin=$request->secondjoincolumn;
            $selectedOption=$request->selectedOption;
            
            $values= DB::table($table)
                ->join($mappingTable, $table.'.'.$joinColumn, '=', $mappingTable.'.'.$secondjoin)
                ->select($mappingTable.'.'.'id',$mappingTable.'.'.$selectColumn)
                ->where($table.'.'.$whereColumn,$cond)
                ->get();
            
            $option='<option value="">Select</option>';
            
            foreach ($values as $value ) {
                $option .='<option value="'.$value->id.'" '.(($selectedOption==$value->id)?'selected':'').'>'.$value->$selectColumn.'</option>';
            }
            echo $option;
        }
    }
	
	/////Add validation////////
     public function set_validation($id=null,$request)
     {
        $message=array(
            
            "mobile.required"=>"Mobile no is required",
            "address.required"=>"Address is required",
            "city.required"=>"City is required",
            "state.required"=>"State is required",
            "country.required"=>"Country is required",
            "zipcode.required"=>"Zipcode is required",
          );

        $this->validate($request,[
        'mobile' => 'required|numeric',
        'address' => 'required',
        'city' => 'required',
        'state' => 'required',
        'country' => 'required',
        'zipcode' => 'required',
        ],$message);
     }
}
