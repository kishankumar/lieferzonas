<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\OrderService;
class OrderServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = DB::table('order_services')
            ->where('status','!=', '2')
            ->select('*')
            ->paginate(10);
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
                 $data[$i] = (array) $data[$i];
        }
       
        return view('superadmin.orders.orderservice.orderservice')
        ->with('orderservices',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	  public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedorderservices = $data['selectedorderservices'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedorderservices as $key => $value)
        {
            $orderservice = OrderService::find($value);
            $orderservice->status = '2';
            $orderservice->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
     public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedorderservice = $data['orderservice_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['orderservice']=OrderService::where('id',$selectedorderservice)->first();
        $orderservice = $data['orderservice']->toArray();
        if(count($orderservice)>0){
            $json['success']=1;
            $json['id']= $orderservice['id'];
            $json['name']=$orderservice['name'];
            $json['description']=$orderservice['description'];
            $json['status']=$orderservice['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

      public function add_orderservice(Request $request)
     {
        if($request->orderservice_id=='0')
        {
			$oservice=OrderService::where('name',$request->name)->where('status','!=',2)->get();
			$oservice = count($oservice->toArray()); 
			if($oservice>0)
			{
			  Session::flash('errmsg', 'This Name is already been taken!');

			  return redirect(route('root.orderservice.index'));
			}
			else{
				$this->set_validation('',$request); 
				$auth_id = Auth::User()->id;
				$orderService= new OrderService;
				$orderService->name=$request->name;
				$orderService->description=$request->description;
				$orderService->status=$request->status;
				$orderService->created_by=$auth_id;
				$orderService->created_at=date("Y-m-d H:i:s");
				$orderService->save();
				Session::flash('message', 'Added Successfully!');
				return redirect(route('root.orderservice.index'));
			}
        }
        else
        {  
	       $oservice=OrderService::where('name',$request->name)->where('id','!=',$request->orderservice_id)->where('status','!=',2)->get();
           $oservice = count($oservice->toArray()); 
			if($oservice>0)
			{
			  Session::flash('errmsg', 'This Name is already been taken!');
			  return redirect(route('root.orderservice.index'));
			}
			else{
				$this->set_validation($request->orderservice_id,$request); 
				$auth_id = Auth::User()->id;
				$orderService=OrderService::find($request->orderservice_id);
				$orderService->name=$request->name;
				$orderService->description=$request->description;
				$orderService->status=$request->status;
				$orderService->updated_by=$auth_id;
				$orderService->updated_at=date("Y-m-d H:i:s");
				$orderService->save();
				Session::flash('message', 'Updated Successfully!');
				return redirect(route('root.orderservice.index'));
			}
        }
        
        
     }
	 
	  /*added by Rajlakshmi(31-05-16)*/
	   public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedorderservices = $data['selectedorderservices'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
       $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>            <th style="border:solid 1px #81C02F; padding:10px">Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedorderservices as $key => $value)
        {
             $data = DB::table('order_services')
			
			->leftjoin('users', 'users.id', '=', 'order_services.created_by')
			->where('order_services.id','=',$value)
			->select('order_services.*','users.email')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				
				
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	 /*added by Rajlakshmi(31-05-16)*/
    
      public function set_validation($id=null,$request)
     {
        $message=array(
            "name.required"=>"Name is required",
            "description.required"=>"Description is required",
            );

        $this->validate($request,[
        'name' => 'required',
        'description' => 'required',
        ],$message);
     }
}
