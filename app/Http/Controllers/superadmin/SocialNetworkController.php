<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\SocialNetwork;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
class SocialNetworkController extends Controller
{
    public function index()
    {
        $social = SocialNetwork::first();
       
        return view('superadmin/social/links')->with('social',$social);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'facebook' => 'required',
            'twitter' => 'required',
            'youtube' => 'required',
            'google' => 'required',
            'linkedin' => 'required',
            'vk' => 'required',
            'instagram' => 'required',
            'pinterest' => 'required',
        ]);

        $social = SocialNetwork::find(1);
        $social->facebook = $request->facebook;
        $social->twitter = $request->twitter;
        $social->youtube = $request->youtube;
        $social->google = $request->google;
        $social->linkedin = $request->linkedin;
        $social->vk = $request->vk;
        $social->instagram = $request->instagram;
        $social->pinterest = $request->pinterest;
        $social->save();

        return redirect(route('root.sociallinks.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
