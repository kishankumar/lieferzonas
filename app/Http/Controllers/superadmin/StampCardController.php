<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\RestStamp;
use Validator;
use Auth;

class StampCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	
        $Info= RestStamp:: where('status','1')->orderBy('stamp_count', 'desc')->paginate(10);
        return view('superadmin.stamps.index')->with('stamp',$Info); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages=array(
            "stamp_count.numeric"=>"Please enter only digits",
            "stamp_count.Regex"=>"stamp count must be greater than 0",
            "value.numeric"=>"Please enter only digits",
            "value.Regex"=>"stamp value format not valid",
        );
        $userData = array(
            'stamp_count'=>$data['stamp_count'],
            'value'=>$data['value']
        );
        
        $stamp_id = Input::get('stamp_id');
        
        $rules = array(
            'stamp_count' => 'required|Regex:/^[0-9]{1,45}$/',
            'value' =>  'required|Regex:/^\d*(\.\d{1,2})?$/'
        );
        
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        $stamp_id=1;
        $type=RestStamp::find($stamp_id);
        $type->stamp_count=$request->stamp_count;
        $type->value=  number_format($request->value,2);
        $type->created_by=Auth::User('user')->id;
        $type->updated_by=Auth::User('user')->id;
        $type->created_at=date("Y-m-d H:i:s");
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=$request->status;
        $type->save();
        
        Session::flash('message', 'Stamp card updated Successfully!'); 
        $json['success']=1;
        $json['message']='Stamp card updated Successfully.';
        
        echo json_encode($json);
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedStamp = $data['selectedStamp'];
        
        $data['stamps']=RestStamp::find($selectedStamp);
        $stamps = $data['stamps']->toArray();
        if(count($stamps)>0){
            $json['success']=1;
            $json['id']=$stamps['id'];
            $json['stamp_count']=$stamps['stamp_count'];
            $json['value']=round($stamps['value'],2);
            $json['status']=$stamps['status'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	/*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedStamps = $data['selectedStamps'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Stamp Count</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Stamp Value</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Stamp Type</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th></tr></thead>';
        foreach($selectedStamps as $key => $value)
        {
            $data = DB::table('rest_stamps')
           

            ->where('id','=', $value)
           
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}
                if($data['type']=='p')
				{
					$type='Percentage';
				}
				else
				{
					$type='Cash';
				}				
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['stamp_count'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['value'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$type.'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
}
