<?php

namespace App\Http\Controllers\superadmin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\PromoUserMap;
use App\superadmin\PromoRestMap;
use App\superadmin\Promocode;
use App\superadmin\User;
use App\superadmin\Rest_detail;
class PromocodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='code';
        }
        
        $q = DB::table('promocodes')->where('status','!=', '2');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('coupon_code', 'LIKE', "%$search%");
                $q2->orWhere('payment_amount', 'LIKE', "%$search%");
                $q2->orWhere('min_amount', 'LIKE', "%$search%");
            });
        }
        
        if($type=='code'){
            $q->orderBy('coupon_code', 'asc');
        }elseif($type=='code-desc'){
            $q->orderBy('coupon_code', 'desc');
        }elseif($type=='from-desc'){
            $q->orderBy('valid_from', 'desc');
        }elseif($type=='from'){
            $q->orderBy('valid_from', 'asc');
        }elseif($type=='to-desc'){
            $q->orderBy('valid_to', 'desc');
        }elseif($type=='to'){
            $q->orderBy('valid_to', 'asc');
        }elseif($type=='pay-desc'){
            $q->orderBy('payment_amount', 'desc');
        }elseif($type=='pay'){
            $q->orderBy('payment_amount', 'asc');
        }elseif($type=='mini-desc'){
            $q->orderBy('min_amount', 'desc');
        }elseif($type=='mini'){
            $q->orderBy('min_amount', 'asc');
        }else{
            $q->orderBy('coupon_code', 'asc');
        }
        $data=$q->paginate(10);
        
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data[$i] = (array) $data[$i];
        }
        
        return view('superadmin.promocode.promocode')->with('search',$search)->with('type',$type)
            ->with('promoInfo',$data); 

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rest = DB::table('rest_details')->select('id', 'f_name','l_name')->orderBy('f_name', 'asc')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($rest); $i < $c; ++$i) {
            $rest[$i] = (array) $rest[$i];
            
        }  

        $user = DB::table('front_user_details')->select('front_user_id', 'fname')->orderBy('fname', 'asc')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($user); $i < $c; ++$i) {
            $user[$i] = (array) $user[$i];
            
        } 
       // print_r($user); die;		

        $time_val = DB::table('time_validations')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($time_val); $i < $c; ++$i) {
            $time_val[$i] = (array) $time_val[$i];
            
        }
        $order_service = DB::table('order_services')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($order_service); $i < $c; ++$i) {
            $order_service[$i] = (array) $order_service[$i];
            
        } 
        		
		 
        return view('superadmin.promocode.addpromocode')->with('resturants',$rest)->with('users',$user)
		->with('time_validations',$time_val)->with('order_services',$order_service);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $this->set_validation('',$request);	
	    $auth_id = Auth::User()->id;
		$promo_list=Promocode::where('coupon_code',$request->coupon_code)->where('status','!=',2)->get();
        $promocount = count($promo_list->toArray()); 
		if($promocount>0)
		{
		  Session::flash('errmessage', 'This Coupon code already exist!');
		  return redirect(route('root.promocode.create'));
		}
		else{
			$promocode = new Promocode;
		$orderservices = array();
        $orderservices = $request->order_service;
		if(count($orderservices)>1)
		{
			$order_type=3;
			$promocode->order_service_id=$order_type;
		}
		else
		{
			foreach($orderservices as $orderservice)
			{
			 $promocode->order_service_id=$orderservice;
			}
			
		}
		$paymentmethod = array();
        $paymentmethod= $request->payment_method;
		if(count($paymentmethod)>1)
		{
			$payment_method=2;
			$promocode->payment_method=$payment_method;
		}
		else
		{
			foreach($paymentmethod as $paymentmethods)
			{
			 $promocode->payment_method=$paymentmethods;
			}
			
		}
	    $promocode->is_new_customer=$request->is_new_customer;
        $promocode->validity_type=$request->validity;
		
        if($request->validity=='T')
        {
           $validity_table =$request->validity_table;
           
           $valid_from = date("Y-m-d H:i:s");
           $time_validation = DB::table('time_validations')->select('year','month','week' ,'day')->where('status','=','1')->where('id','=',$validity_table)->get();
        
             
           for ($i = 0, $c = count($time_validation); $i < $c; ++$i) {
            $time_validation[$i] = (array) $time_validation[$i];
            
           } 
           $year = $time_validation['0']['year'];
           $month = $time_validation['0']['month'];
           $week = $time_validation['0']['week'];
           $day = $time_validation['0']['day'];
           $totaltime = ($year*365) + ($month*30) + ($week*7) + ($day);
           $valid_to = date('Y-m-d', strtotime("+".$totaltime."days"));
           $promocode->validity_table=$validity_table;
           $promocode->valid_from=$valid_from;
           $promocode->valid_to=$valid_to;
        }
         
      else
        {
             $promocode->valid_from=$request->valid_from;
             $promocode->valid_to=$request->valid_to;
        }
	    $promocode->coupon_code=$request->coupon_code;
        $promocode->is_rest_specific=$request->is_rest_specific;
        $promocode->is_user_specific=$request->is_user_specific;
        $promocode->payment_mode=$request->payment_mode;
		$promocode->payment_amount=$request->payment_amount;
		$promocode->min_amount=$request->min_amount;
        $promocode->status=$request->status;
		$promocode->created_by=$auth_id;
        $promocode->created_at=date("Y-m-d H:i:s");
		$promocode->save();
        $id =$promocode->id;
		$restlist = array();
        $restlist = Input::get('restlist');
		//print_r($restlist); 
		for($i=0; $i<count($restlist); $i++ )
		{
		  $promo_restmap = new PromoRestMap;
		  $promo_restmap->promo_id=$id;
		  $promo_restmap->rest_id=$restlist[$i];
		  $promo_restmap->status=$request->status;
		  $promo_restmap->created_by=$auth_id;
          $promo_restmap->created_at=date("Y-m-d H:i:s");
		  $promo_restmap->save();
		  
		  
		}
		$userlist = array();
        $userlist = Input::get('userlist');
		//print_r($userlist); 
		for($i=0; $i<count($userlist); $i++ )
		{
		  $promo_usermap = new PromoUserMap;
          $promo_usermap->promo_id=$id;
		  $promo_usermap->user_id=$userlist[$i];
		  $promo_usermap->status=$request->status;
		  $promo_usermap->created_by=$auth_id;
          $promo_usermap->created_at=date("Y-m-d H:i:s");
		  $promo_usermap->save();
		  
		  
		}
		Session::flash('message', 'Promocode Added Successfully!');
        return redirect(route('root.promocode.index'));
		}
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
	   $rest = DB::table('rest_details')->select('id', 'f_name','l_name')->orderBy('f_name', 'asc')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($rest); $i < $c; ++$i) {
            $rest[$i] = (array) $rest[$i];
            
        }  

        $user = DB::table('front_user_details')->select('front_user_id', 'fname')->orderBy('fname', 'asc')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($user); $i < $c; ++$i) {
            $user[$i] = (array) $user[$i];
            
        }
        $order_service = DB::table('order_services')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($order_service); $i < $c; ++$i) {
            $order_service[$i] = (array) $order_service[$i];
            
        } 	

        
        $seluser=PromoUserMap::where('promo_id',$id)->where('status','!=','2')->lists('user_id');		
        $seluser = $seluser->toArray();
		//print_r($seluser); die;
        $selrest=PromoRestMap::where('promo_id',$id)->where('status','!=','2')->lists('rest_id');		
        $selrest = $selrest->toArray();
        $time_val = DB::table('time_validations')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($time_val); $i < $c; ++$i) {
            $time_val[$i] = (array) $time_val[$i];
            
        } 
       $promocode = Promocode::where('id',$id)->get();
	   $promocode =$promocode->toArray();
	    
		 
       return view('superadmin/promocode/editpromocode')->with('promocode',$promocode)->with('resturants',$rest)->with('users',$user)
	   ->with('time_validations',$time_val)->with('selected_user',$seluser)->with('selected_rest',$selrest)->with('order_services',$order_service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->set_validation($id,$request);
        $auth_id = Auth::User()->id; 
        $promocode=Promocode::find($id);
		$promocode->is_new_customer=$request->is_new_customer;
		$orderservices = array();
        $orderservices = $request->order_service;
		if(count($orderservices)>1)
		{
			$order_type=3;
			$promocode->order_service_id=$order_type;
		}
		else
		{
			foreach($orderservices as $orderservice)
			{
			 $promocode->order_service_id=$orderservice;
			}
			
		}
		$paymentmethod = array();
        $paymentmethod= $request->payment_method;
		if(count($paymentmethod)>1)
		{
			$payment_method=2;
			$promocode->payment_method=$payment_method;
		}
		else
		{
			foreach($paymentmethod as $paymentmethods)
			{
			 $promocode->payment_method=$paymentmethods;
			}
			
		}
        $promocode->validity_type=$request->validity;
        if($request->validity=='T')
        {
           $validity_table =$request->validity_table;
           
           $valid_from = date("Y-m-d H:i:s");
           $time_validation = DB::table('time_validations')->select('year','month','week' ,'day')->where('status','=','1')->where('id','=',$validity_table)->get();
        
             
           for ($i = 0, $c = count($time_validation); $i < $c; ++$i) {
            $time_validation[$i] = (array) $time_validation[$i];
            
           } 
           $year = $time_validation[0]['year'];
           $month = $time_validation[0]['month'];
           $week = $time_validation[0]['week'];
           $day = $time_validation[0]['day'];
           $totaltime = ($year*365) + ($month*30) + ($week*7) + ($day);
           $valid_to = date('Y-m-d', strtotime("+".$totaltime."days"));
           $promocode->validity_table=$validity_table;
           $promocode->valid_from=$valid_from;
           $promocode->valid_to=$valid_to;
        }
         
      else
        {
             $promocode->valid_from=$request->valid_from;
             $promocode->valid_to=$request->valid_to;
             $promocode->validity_table='0';
        }
        $promocode->coupon_code=$request->coupon_code;
        $promocode->is_rest_specific=$request->is_rest_specific;
        $promocode->is_user_specific=$request->is_user_specific;
        $promocode->payment_mode=$request->payment_mode;
        $promocode->payment_amount=$request->payment_amount;
        $promocode->min_amount=$request->min_amount;
        $promocode->status=$request->status;
        $promocode->updated_by=$auth_id;
        $promocode->updated_at=date("Y-m-d H:i:s");
        $promocode->save();
        if($request->is_rest_specific=='1')
		{
			$countrest = PromoRestMap::where('promo_id', '=', $id)->where('status','=','1')->get();
			$countrest = count($countrest->toArray());
			if($countrest!= null)
			{
				$affected_data = DB::table('promo_rest_maps')
				->where('promo_id', '=', $id)
				->where('status', '=', '1')
				->update(array('status' => '2'));
			}
			$restlist = array();
			$restlist = Input::get('restlist');
		   
			for($i=0; $i<count($restlist); $i++ )
			{
			  $promo_restmap = new PromoRestMap;
			  $promo_restmap->promo_id=$id;
			  $promo_restmap->rest_id=$restlist[$i];
			  $promo_restmap->status=1;
			  $promo_restmap->created_by=$auth_id;
			  $promo_restmap->created_at=date("Y-m-d H:i:s");
			  $promo_restmap->save();
			  
			  
			}
		}
		else
		{
			$countrest = PromoRestMap::where('promo_id', '=', $id)->where('status','=','1')->get();
			$countrest = count($countrest->toArray());
			if($countrest!= null)
			{
				$affected_data = DB::table('promo_rest_maps')
				->where('promo_id', '=', $id)
				->where('status', '=', '1')
				->update(array('status' => '2'));
			}
		}
			
        if($request->is_user_specific=='1')
		{
			$countuser = PromoUserMap::where('promo_id', '=', $id)->where('status','=','1')->get();
			$countuser = count($countuser->toArray());
			if($countuser!= null)
			{
				$affected_data = DB::table('promo_user_maps')
				->where('promo_id', '=', $id)
				->where('status', '=', '1')
				->update(array('status' => '2'));
			}
			$userlist = array();
			$userlist = Input::get('userlist');
			
			for($i=0; $i<count($userlist); $i++ )
			{
			  $promo_usermap = new PromoUserMap;
			  $promo_usermap->promo_id=$id;
			  $promo_usermap->user_id=$userlist[$i];
			  $promo_usermap->status=1;
			  $promo_usermap->created_by=$auth_id;
			  $promo_usermap->created_at=date("Y-m-d H:i:s");
			  $promo_usermap->save();
			  
			  
			}
        }
		else
		{
			$countuser = PromoUserMap::where('promo_id', '=', $id)->where('status','=','1')->get();
			$countuser = count($countuser->toArray());
			if($countuser!= null)
			{
				$affected_data = DB::table('promo_user_maps')
				->where('promo_id', '=', $id)
				->where('status', '=', '1')
				->update(array('status' => '2'));
			}
		}
		Session::flash('message', 'Promocode Added Successfully!');
        return redirect(route('root.promocode.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {

        if($request->ajax()){
            $data = Input::all();
           
            $selectedPromocode = $data['selectedPromocode'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedPromocode as $key => $value)
        {
            $promocode = Promocode::find($value);
            $promocode->status = '2';
            $promocode->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(01-06-16)*/
	public function download(Request $request)
    {

        if($request->ajax()){
            $data = Input::all();
           
            $selectedPromocode = $data['selectedPromocode'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Coupon code</th>
        <th style="border:solid 1px #81C02F; padding:10px">Valid from</th>
		<th style="border:solid 1px #81C02F; padding:10px">Valid to</th>
		<th style="border:solid 1px #81C02F; padding:10px">Is Restaurant specific</th>
		<th style="border:solid 1px #81C02F; padding:10px">Is User specific</th>
		<th style="border:solid 1px #81C02F; padding:10px">Discount Coupon Value</th>
		<th style="border:solid 1px #81C02F; padding:10px">Order minimum value</th>
        <th style="border:solid 1px #81C02F; padding:10px">Status</th></tr></thead>';
        foreach($selectedPromocode as $key => $value)
        {
            $data = DB::table('promocodes')
            

            ->where('id','=', $value)
           
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if($data['is_rest_specific']==1)
				{
					$rest_specific='Yes';
				}
				else
				{
					$rest_specific='No';
				}
				if($data['is_user_specific']==1)
				{
					$user_specific='Yes';
				}
				else
				{
					$user_specific='No';
				}
				if($data['payment_mode']=='P')
				{
					$paymentmode = '%';
				}
				else
				{
					$paymentmode = '$';
				}
				$valid_from = date("d M Y", strtotime($data['valid_from']));
				$valid_to = date("d M Y", strtotime($data['valid_to']));
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['coupon_code'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_from.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_to.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$rest_specific.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$user_specific.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['payment_amount'].' '.$paymentmode.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['min_amount'].'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	
	public function getpromocodeDetail($promocodeId=0)
    {
		$rest = DB::table('rest_details')->select('id', 'f_name','l_name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($rest); $i < $c; ++$i) {
            $rest[$i] = (array) $rest[$i];
            
        }  

        $user = DB::table('user_masters')->select('user_id', 'first_name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($user); $i < $c; ++$i) {
            $user[$i] = (array) $user[$i];
            
        }
        $order_service = DB::table('order_services')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($order_service); $i < $c; ++$i) {
            $order_service[$i] = (array) $order_service[$i];
            
        } 	

        
       $promocode = Promocode::where('id',$promocodeId)->get();
	   $promocode =$promocode->toArray();
	    
		 
       return view('superadmin/promocode/promocode_detail')->with('promocode',$promocode)->with('resturants',$rest)->with('users',$user)
	   ->with('order_services',$order_service);
	}
	
	
	public function showuser()
	{
		$promo_id = Input::get('promo_id');
		$showuser= DB ::table('promo_user_maps')
		->leftjoin('user_masters', 'user_masters.user_id', '=', 'promo_user_maps.user_id')
	    ->where('promo_user_maps.promo_id','=',$promo_id)
		->where('promo_user_maps.status','!=',2)
		->select('user_masters.first_name','user_masters.last_name')->get();
		
		$data= '';
		if(count($showuser)>0)
		{
			foreach($showuser as $showusers)
			{
				$data.= "<div class='col-md-4'>".$showusers->first_name.' '.$showusers->last_name."</div>";
			}
            $json['success']=1;
            $sendata['data']=$data;
			$sendata['success']=1;
            print_r(json_encode($sendata));
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		
	}
	
	
	public function showrest()
	{
		$promo_id = Input::get('promo_id');
		$showrest= DB ::table('promo_rest_maps')
		->leftjoin('rest_details', 'promo_rest_maps.rest_id', '=', 'rest_details.id')
	    ->where('promo_rest_maps.promo_id','=',$promo_id)
		->where('promo_rest_maps.status','!=',2)
		->select('rest_details.f_name')->get();
		
		$data= '';
		if(count($showrest)>0)
		{
			foreach($showrest as $showrests)
			{
				$data.= "<div class='col-md-4'>".$showrests->f_name."</div>";
			}
            $json['success']=1;
            $sendata['data']=$data;
			$sendata['success']=1;
            print_r(json_encode($sendata));
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		
	}
    /*added by Rajlakshmi(01-06-16)*/
     public function set_validation($id=null,$request)
     {
        $message=array(
            "coupon_code.required"=>"Coupon code is required",
            "min_amount.required"=>"Min amount is required",
            );

        $this->validate($request,[
        'coupon_code' => 'required',
        'min_amount' => 'required',
        ],$message);
     }
}
