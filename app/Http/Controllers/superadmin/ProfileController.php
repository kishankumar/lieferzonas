<?php

namespace App\Http\Controllers\superadmin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\User;
use App\superadmin\User_master;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		    $id = Auth::User()->id;
		   $info = DB::table('users')
            ->leftjoin('user_roles', 'users.user_role_id', '=', 'user_roles.id')
            ->leftjoin('user_masters', 'users.id', '=', 'user_masters.user_id')
            ->leftjoin('countries', 'user_masters.country', '=', 'countries.id')
			->leftjoin('states', 'user_masters.state', '=', 'states.id')
			->leftjoin('cities', 'user_masters.city', '=', 'cities.id')
			->leftjoin('zipcodes', 'user_masters.zipcode', '=', 'zipcodes.id')
            ->where('users.id','=', $id)
            
            ->select('users.*','user_masters.first_name','user_masters.last_name','user_masters.email', 
			'user_masters.contact','user_masters.add1','user_masters.add2','cities.name as city_name'
                ,'states.name as state_name','countries.name as country_name','zipcodes.name as zipcode')
				->get();
            for ($i = 0, $c = count($info); $i < $c; ++$i) {
             $info[$i] = (array) $info[$i];
             
             }
		   
			 //print_r($info); die;
			
              return view('superadmin.myaccount.profile', ['info' => $info]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
		$info = DB::table('users')
            ->leftjoin('user_roles', 'users.user_role_id', '=', 'user_roles.id')
            ->leftjoin('user_masters', 'users.id', '=', 'user_masters.user_id')
            ->leftjoin('countries', 'user_masters.country', '=', 'countries.id')
			->leftjoin('states', 'user_masters.state', '=', 'states.id')
			->leftjoin('cities', 'user_masters.city', '=', 'cities.id')
            ->where('users.id','=', $id)
            
            ->select('users.*','user_masters.id as mid','user_masters.first_name','user_masters.last_name','user_masters.email', 
			'user_masters.contact','user_masters.add1','user_masters.add2','user_masters.city','user_masters.state',
			'user_masters.country','cities.name as city_name','states.name as state_name',
			'countries.name as country_name','user_masters.zipcode')
				->get();
			
			for ($i = 0, $c = count($info); $i < $c; ++$i) {
             $info[$i] = (array) $info[$i];
             
             }
        $country = DB::table('countries')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($country); $i < $c; ++$i) {
            $country[$i] = (array) $country[$i];
            
        } 

         $state = DB::table('states')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($state); $i < $c; ++$i) {
            $state[$i] = (array) $state[$i];
            
        } 
         $city = DB::table('cities')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($city); $i < $c; ++$i) {
            $city[$i] = (array) $city[$i];
            
        } 
		
		$auth_id = Auth::User()->id;
		$username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
		for ($i = 0, $c = count($username); $i < $c; ++$i) {
            $username[$i] = (array) $username[$i];
            
        }
		
		 return view('superadmin.myaccount.edit-profile' , ['info' => $info])->with('country',$country)
		->with('state',$state)->with('city',$city)->with('username',$username);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->set_validation($id,$request);
        $auth_id = Auth::User()->id; 
		
		
        $userdetail=User_master::find($request->user_master_id);

        $userdetail->first_name=$request->f_name;

        $userdetail->last_name=$request->l_name;
        $userdetail->zipcode=$request->zipcode;
        $userdetail->country=$request->country;
		$userdetail->state=$request->state;
        $userdetail->city=$request->city;
        $userdetail->add1=$request->address1;
		$userdetail->add2=$request->address2;
		$userdetail->email=$request->email;
		$userdetail->contact=$request->mobile_no;
		$userdetail->updated_by=$auth_id;
        $userdetail->updated_at=date("Y-m-d H:i:s");
        $userdetail->save();
		
		
		Session::flash('message','Updated Successfully');
		return redirect(route('root.profile.index'));
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function getData(Request $request)
    {
        if($request) {
            $table=$request->table;
            $mappingTable=$request->maptable;
            $cond=$request->value;
            $joinColumn=$request->joincolumn;
            $selectColumn=$request->selectcolumn;
            $whereColumn=$request->wherecolumn;
            $secondjoin=$request->secondjoincolumn;
            $selectedOption=$request->selectedOption;
            
            $values= DB::table($table)
                ->join($mappingTable, $table.'.'.$joinColumn, '=', $mappingTable.'.'.$secondjoin)
                ->select($mappingTable.'.'.'id',$mappingTable.'.'.$selectColumn)
                ->where($table.'.'.$whereColumn,$cond)
                ->get();
            
            $option='<option value="">Select</option>';
            foreach ($values as $value ) {
                $option .='<option value="'.$value->id.'" '.(($selectedOption==$value->id)?'selected':'').'>'.$value->$selectColumn.'</option>';
            }
            echo $option;
        }
    }
	 public function set_validation($id=null,$request)
     {
        $message=array(
            "f_name.required"=>"First Name is required",
            "f_name.min"=>"The First name must be at least 2 characters.",
            "f_name.alpha"=>"The First name must be alphabet",
			
            "email.required"=>"Email is required",
            "email.email"=>"Enter valid email",
			"mobile_no.required"=>"Mobile is required",
			"country.required"=>"Country is required",
			"state.required"=>"Adress is required",
			"city.required"=>"City is required",
            "address1.required"=>"Adress is required"
           
         );

        $this->validate($request,[
            'f_name'=> 'required|min:2|alpha|unique:rest_details,f_name,'.$id.',id',
            'email'=> 'required|email',
			'mobile_no'=> 'required',
			'country'=> 'required',
			'state'=> 'required',
			'city'=> 'required',
            'address1'=> 'required'
        ],$message);
     }
	
}
