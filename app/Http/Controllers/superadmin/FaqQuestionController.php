<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\FaqCategory;
use App\superadmin\FaqQuestion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Session;
use Auth;
use DB;

class FaqQuestionController extends Controller
{
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='question';
        }
        
        $q = FaqQuestion::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('question', 'LIKE', "%$search%");
                $q2->orWhere('answer', 'LIKE', "%$search%");
            });
        }
        
        if($type=='question'){
            $q->orderBy('question', 'asc');
        }elseif($type=='question-desc'){
            $q->orderBy('question', 'desc');
        }elseif($type=='answer-desc'){
            $q->orderBy('answer', 'desc');
        }elseif($type=='answer'){
            $q->orderBy('answer', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('created_at', 'asc');
        }else{
            $q->orderBy('question', 'asc');
        }
        $data['faq'] = $q->paginate(10);
        
        //$data['faq'] = FaqQuestion::where('status','!=',2)->get();
        $data['cat'] = FaqCategory::where('status','=',1)->lists('name', 'id');
        $data['cat']->prepend('Select FAQ Category', '');
        //dd($country);

        
        return view('superadmin/faq/index')->with('data',$data)->with('search',$search)->with('type',$type);

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'question'=>'required|unique:faq_questions',
            'answer'=>'required|min:10',
            'category'=>'required'
        ]);

        $faq = new FaqQuestion;
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->faq_category_id = $request->category;
        $faq->status = $request->status;
        //$faq->created_by = session user id;
        $faq->save();
        Session::flash('message', 'Added Successfully!');
        return redirect(route('root.faq.index'));
    }
    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['faq'] = FaqQuestion::where('id',$id)->first();
        $data['cat'] = FaqCategory::where('status','=',1)->lists('name', 'id');
        //print_r($faq);exit();
		
        return view('superadmin/faq/edit')->with('data',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'question' => 'required|unique:faq_questions,question,'.$id.",id",
            'answer' => 'required|min:10'
        ]);
        $faq = FaqQuestion::find($id);
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->faq_category_id = $request->category;
        $faq->status = $request->status;
        $faq->save();
        Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.faq.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedFaq = $data['selectedfaq'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedFaq as $key => $value)
        {
            $faq = FaqQuestion::find($value);
            $faq->status = '2';
            $faq->save();
        }
        //Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }

    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $faq = FaqQuestion::find($id);
            $faq->status = $status;
            $faq->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedFaq = $data['selectedfaq'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Question</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Answer</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Category</th>
						<th style="border:solid 1px #81C02F; padding:10px">Created By</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created At</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedFaq as $key => $value)
        {
            $data = DB::table('faq_questions')
			->leftjoin('users', 'users.id', '=', 'faq_questions.created_by')
			->leftjoin('faq_categories', 'faq_categories.id', '=', 'faq_questions.faq_category_id')
			->where('faq_questions.id','=',$value)
			->select('faq_questions.*','users.email','faq_categories.name')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['question'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['answer'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['email'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
