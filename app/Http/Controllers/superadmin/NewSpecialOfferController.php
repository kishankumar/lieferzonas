<?php

namespace App\Http\Controllers\superadmin;

use App\admin\SpecialOffer;
use App\admin\SpecialOfferCatMap;
use App\admin\SpecialOfferDayMap;
use App\admin\SpecialOfferDayTimeMap;
use App\admin\SpecialOfferMenuMap;
use App\admin\SpecialOfferSubmenuMap;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NewSpecialOfferController extends Controller
{

    public function index()
    {
        return view("superadmin_new.special_offers.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function createForRestaurant($restaurant_id) {
        return view("superadmin_new.special_offers.create")->with("restaurant_id", $restaurant_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            $special_offer_row = new SpecialOffer;
            $special_offer_row->name = $request->input("name");
            $special_offer_row->rest_id = $request->input("restaurant");
            $special_offer_row->valid_from = $request->input("valid_from");
            $special_offer_row->valid_to = $request->input("valid_to");
            $special_offer_row->discount_type_id = $request->input("type");
            $special_offer_row->amount = $request->input("value");
            $special_offer_row->created_by = Auth::user()->id;
            $special_offer_row->updated_by = Auth::user()->id;
            $special_offer_row->status = 1;
            $special_offer_row->save();

            foreach($request->input("valid_weekdays") as $valid_weekday) {
                $special_offer_day_map = new SpecialOfferDayMap;
                $special_offer_day_map->special_offer_id = $special_offer_row->id;
                $special_offer_day_map->day_id = $valid_weekday;
                $special_offer_day_map->created_by = Auth::user()->id;
                $special_offer_day_map->updated_by = Auth::user()->id;
                $special_offer_day_map->status = 1;
                $special_offer_day_map->save();
                $special_offer_day_time_map = new SpecialOfferDayTimeMap;
                $special_offer_day_time_map->special_offer_id = $special_offer_row->id;
                $special_offer_day_time_map->day_id = $valid_weekday;
                $special_offer_day_time_map->created_by = Auth::user()->id;
                $special_offer_day_time_map->updated_by = Auth::user()->id;
                $special_offer_day_time_map->status = 1;
                $special_offer_day_time_map->time_from = $request->input("start_time");
                $special_offer_day_time_map->time_to = $request->input("end_time");
                $special_offer_day_time_map->save();
            }

            if (is_array($request->input("categories"))) {
                foreach ($request->input("categories") as $item) {
                    $item_map = new SpecialOfferCatMap;
                    $item_map->special_offer_id = $special_offer_row->id;
                    $item_map->category_id = $item;
                    $item_map->created_by = Auth::user()->id;
                    $item_map->updated_by = Auth::user()->id;
                    $item_map->status = 1;
                    $item_map->save();
                }
            }

            if (is_array($request->input("menus"))) {
                foreach($request->input("menus") as $item) {
                    $item_map = new SpecialOfferMenuMap;
                    $item_map->special_offer_id = $special_offer_row->id;
                    $item_map->menu_id = $item;
                    $item_map->created_by = Auth::user()->id;
                    $item_map->updated_by = Auth::user()->id;
                    $item_map->status = 1;
                    $item_map->save();
                }
            }

            if (is_array($request->input("submenus"))) {
                foreach ($request->input("submenus") as $item) {
                    $item_map = new SpecialOfferSubmenuMap;
                    $item_map->special_offer_id = $special_offer_row->id;
                    $item_map->sub_menu_id = $item;
                    $item_map->created_by = Auth::user()->id;
                    $item_map->updated_by = Auth::user()->id;
                    $item_map->status = 1;
                    $item_map->save();
                }
            }

            DB::commit();
            return response()->json([
                "status" => "success"
            ]);
        }
        catch(\Exception $e) {
            DB::rollback();
            return response()->json([
                "status" => "failure",
                "reason" => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
