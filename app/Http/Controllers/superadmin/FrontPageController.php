<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\FrontPage;
use App\superadmin\FrontPageMap;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Session;
use Auth;
use DB;

class FrontPageController extends Controller
{
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='title';
        }
        
        $q = FrontPage::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('title', 'LIKE', "%$search%");
                $q2->orWhere('slug', 'LIKE', "%$search%");
            });
        }
        if($type=='title'){
            $q->orderBy('title', 'asc');
        }elseif($type=='title-desc'){
            $q->orderBy('title', 'desc');
        }elseif($type=='slug-desc'){
            $q->orderBy('slug', 'desc');
        }elseif($type=='slug'){
            $q->orderBy('slug', 'asc');
        }elseif($type=='priority-desc'){
            $q->orderBy('priority', 'desc');
        }elseif($type=='priority'){
            $q->orderBy('priority', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('created_at', 'asc');
        }else{
            $q->orderBy('title', 'asc');
        }
        $page = $q->paginate(10);
        
        return view('superadmin/infopage/index')->with('pages',$page)->with('search',$search)->with('type',$type); 

    }
    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|unique:front_pages|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/',
            'slug'=>'required|unique:front_pages',
            'contents'=>'required|min:100',
            //'priority'=>'required'
        ]);
        
        $page = new FrontPage;
        $page->title = $request->title;
        $page->slug = $request->slug;
        //$page->created_by = session user id;
        $page->save();
        //dd($page->id);
        
        $content = new FrontPageMap;
        $content->page_id = $page->id;
        $content->content = $request->contents;
        $content->save();
        
        return redirect(route('root.infopage.index'));
    }
    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $page['page'] = FrontPage::where('id',$id)->first();
        $page['content'] = FrontPageMap::where('page_id',$page['page']->id)->first();
        //print_r($page);exit();
		
        return view('superadmin/infopage/edit')->with('pages',$page);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/|unique:front_pages,title,'.$id.",id",
            'slug' => 'required|unique:front_pages,slug,'.$id.",id",
            'contents' => 'required|min:100'
        ]);
        $page = FrontPage::find($id);
        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->status = $request->status;
        $page->save();

        $content = FrontPageMap::where('page_id','=',$id)->first();
        $content->content = $request->contents;
        $content->save();

        return redirect(route('root.infopage.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedPages= $data['selectedPages'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedPages as $key => $value)
        {
            $page = FrontPage::find($value);
            $page->status = '2';
            $page->save();
        }
        //Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }

    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $page = FrontPage::find($id);
            $page->status = $status;
            $page->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedPages= $data['selectedPages'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Title</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Slug</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Priority</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created By</th>
						<th style="border:solid 1px #81C02F; padding:10px">Created At</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedPages as $key => $value)
        {
             $data = DB::table('front_pages')
			->leftjoin('users', 'users.id', '=', 'front_pages.created_by')
			->where('front_pages.id','=',$value)
			->select('front_pages.*' ,'users.email')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['title'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['slug'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['priority'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['email'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
