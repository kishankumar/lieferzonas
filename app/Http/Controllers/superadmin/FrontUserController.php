<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use validator;
use App\front\FrontUser;
use App\front\FrontUserAddress;
use App\front\FrontUserDetail;
use Mail;
use File;
class FrontUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fuser = DB::table('front_users')
        ->leftjoin('front_user_details', 'front_users.id', '=', 'front_user_details.front_user_id')
        ->where('front_users.status','!=',2)
		->select('front_user_details.front_user_id','front_users.id','front_users.email as username','front_users.status','front_user_details.fname','front_user_details.lname','front_user_details.email','front_user_details.mobile')
		->paginate(10);
		return view('superadmin.frontusers.user')->with('fusersinfo',$fuser);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
        $data = DB::table('front_users')
        ->leftjoin('front_user_details', 'front_users.id', '=', 'front_user_details.front_user_id')
        ->where('front_users.id','=',$id )
		->select('front_users.id','front_users.email as username','front_users.status as userstatus','front_user_details.*')
		->get();
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data = (array) $data[$i];
        }
		$final = (object)$data;
		return view('superadmin/frontusers/edit_user')->with('fusers',$final);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->set_validation($id,$request);
		$auth_id = Auth::User()->id; 
		$extension = '';
        $destinationpath=public_path()."/front/uploads/users";
		$pic=Input::file('pics');
		$fuid = $request->fuid;
        //$fuserdetail= new FrontUserDetail;
        $fuserdetail = FrontUserDetail::where('front_user_id', $fuid)->first();
		$fuserdetail->fname=$request->fname;
		$fuserdetail->lname=$request->lname;
        $fuserdetail->nickname=$request->nickname;
        $fuserdetail->email=$request->email;
		$fuserdetail->mobile=$request->mobile;
		$fuserdetail->gender=$request->gender;
		if($pic!='')
         {
             $extension=  $pic->getClientOriginalExtension(); // getting image extension
             $filename=time().rand(111,999).'.'.$extension; // renameing image
             $pic->move($destinationpath,$filename);
             $fuserdetail->profile_pic=$filename;
         }  
		$fuserdetail->dob=$request->dob;
        $fuserdetail->updated_by=$auth_id;
        $fuserdetail->updated_at=date("Y-m-d H:i:s");
        $fuserdetail->save();
		$fuser = FrontUser::where('id', $fuid)->first();
		$fuser->status=$request->status;
		$fuser->save();
		Session::flash('message','Updated Successfully');
		
		return redirect(route('root.frontuser.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	 public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedUsers = $data['selectedUsers'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedUsers as $key => $value)
        {
            $fuser = FrontUser::find($value);
			$fuserpic = FrontUserDetail::where('front_user_id',$fuser->id)->get();
			
			$pathtofile = 'public/front/uploads/users/'.$fuserpic['0']['profile_pic'];
            $fuser->status = '2';
            $fuser->save();
			if($fuserpic['0']['profile_pic']!='')
			{
				File::Delete($pathtofile);
			}
			
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	public function FUserDetail($FUserId=0)
	{
		$data = DB::table('front_users')
        ->leftjoin('front_user_details', 'front_users.id', '=', 'front_user_details.front_user_id')
        ->where('front_users.id','=',$FUserId)
		->select('front_users.id','front_users.email as username','front_users.status as userstatus','front_user_details.*')
		->get();
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data = (array) $data[$i];
        }
		$final = (object)$data;
		return view('superadmin/frontusers/user_detail')->with('fusers',$final);
	}
	public function Showaddress($Id=0)
	{
		
		$data = DB::table('front_users')
        ->leftjoin('front_user_details', 'front_users.id', '=', 'front_user_details.front_user_id')
        ->where('front_users.id','=',$Id)
		->select('front_users.id')
		->get();
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $final = (array) $data[$i];
        }
		
		$addressDetails = DB::table('front_user_addresses')
        ->leftjoin('countries', 'front_user_addresses.country_id', '=', 'countries.id')
		->leftjoin('states', 'front_user_addresses.state_id', '=', 'states.id')
		->leftjoin('cities', 'front_user_addresses.city_id', '=', 'cities.id')
        ->where('front_user_addresses.front_user_id','=',$Id)
		 ->where('front_user_addresses.status','!=',2)
		->select('front_user_addresses.*','countries.name as country_name','states.name as state_name','cities.name as city_name')
		->get();
        for ($i = 0, $c = count($addressDetails); $i < $c; ++$i) {
            $addressDetails[$i] = (array) $addressDetails[$i];
        }
		$country = DB::table('countries')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($country); $i < $c; ++$i) {
            $country[$i] = (array) $country[$i];
            
        }
		$country1 = DB::table('countries')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($country1); $i < $c; ++$i) {
            $country1[$i] = (array) $country1[$i];
            
        }
		$state = DB::table('states')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($state); $i < $c; ++$i) {
            $state[$i] = (array) $state[$i];
            
        } 
        $city = DB::table('cities')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($city); $i < $c; ++$i) {
            $city[$i] = (array) $city[$i];
            
        } 
		//$addressDetails = (object)$addressDetails;
		//print_r($addressDetails); die;
		return view('superadmin/frontusers/show_address')->with('addressDetails',$addressDetails)->with('country1',$country1)
		->with('country',$country)->with('state',$state)->with('city',$city)->with('fusers',$final);
	}
	public function add_address()
	{
		$auth_id = Auth::User()->id; 
		$front_user_id = Input::get('front_user_id');
		$name = Input::get('name');
		$country = Input::get('country');
		$state= Input::get('state');
		$city = Input::get('city');
		$pincode= Input::get('pincode');
		$address = Input::get('address');
		$landmark= Input::get('landmark');
		$mobile = Input::get('mobile');
		$add_address = new FrontUserAddress;
		$add_address->front_user_id=$front_user_id;
		$add_address->booking_person_name=$name;
		$add_address->mobile=$mobile;
		$add_address->address=$address;
		$add_address->landmark=$landmark;
		$add_address->city_id=$city;
		$add_address->state_id=$state;
		$add_address->country_id=$country;
		$add_address->country_id=$country;
		$add_address->zipcode=$pincode;
		$add_address->created_by=$auth_id;
		$add_address->created_at=date("Y-m-d H:i:s");
		$add_address->status=1;
		$add_address->save();
		$json['success']=1;
        echo json_encode($json);
        return ;
	}
	public function edit_address()
	{
		$auth_id = Auth::User()->id;
        $id = Input::get('address_id');		
		$front_user_id = Input::get('front_user_id');
		$name = Input::get('name');
		$country = Input::get('country');
		$state= Input::get('state');
		$city = Input::get('city');
		$pincode= Input::get('pincode');
		$address = Input::get('address');
		$landmark= Input::get('landmark');
		$mobile = Input::get('mobile');
		$add_address = FrontUserAddress::find($id);
		$add_address->front_user_id=$front_user_id;
		$add_address->booking_person_name=$name;
		$add_address->mobile=$mobile;
		$add_address->address=$address;
		$add_address->landmark=$landmark;
		$add_address->city_id=$city;
		$add_address->state_id=$state;
		$add_address->country_id=$country;
		$add_address->country_id=$country;
		$add_address->zipcode=$pincode;
		$add_address->updated_by=$auth_id;
		$add_address->updated_at=date("Y-m-d H:i:s");
		$add_address->status=1;
		$add_address->save();
		$json['success']=1;
        echo json_encode($json);
        return ;
	}
	public function fetch_address()
	{
	 $address_id = Input::get('address_id');
	 $data['address']=FrontUserAddress::where('id',$address_id)->first();
        $address = $data['address']->toArray();
        if(count($address)>0){
            $json['success']=1;
            $json['id']= $address['id'];
            $json['name']=$address['booking_person_name'];
            $json['country']=$address['country_id'];
			$json['state']=$address['state_id'];
			$json['city']=$address['city_id'];
			$json['pincode']=$address['zipcode'];
            $json['address']=$address['address'];
            $json['landmark']=$address['landmark'];
            $json['mobile']=$address['mobile'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
	}
	public function delete_address()
	{
	  $address_id = Input::get('address_id');
	  $del_address = FrontUserAddress::find($address_id);
	  $del_address->status = '2';
      $del_address->save();
	  Session::flash('message', 'Deleted Successfully!'); 
	  $json['success']=1;
	  echo json_encode($json);
	  return;
	}
	 public function set_validation($id=null,$request)
     {
        $message=array(
            "fname.required"=>"First name is required",
            "fname.min"=>"The First name must be at least 3 characters.",
            "fname.alpha"=>"The First name must be alphabet",
            "email.required"=>"Email is required",
            "email.email"=>"Enter valid email",
            "mobile.required"=>"Phone no is required",
			"gender.required"=>"Date of birth is required",
            "dob.required"=>"Date of birth is required"
            );

        $this->validate($request,[
        'fname' => 'required|min:3',
        'email' => 'required|Regex:/^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i ',
        'mobile' => 'required|numeric',
        'gender' => 'required',
        'dob' => 'required'
        ],$message);
     }

}
