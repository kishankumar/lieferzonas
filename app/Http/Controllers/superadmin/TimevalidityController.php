<?php

namespace App\Http\Controllers\superadmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Session;
use DB;
use Auth;
use validator;
use App\superadmin\TimeValidation;

class TimevalidityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = DB::table('time_validations')->where('status','!=', '2');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $data=$q->paginate(10);
        
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
           $data[$i] = (array) $data[$i];
        }
         

        return view('superadmin.promocode.timevalidity')->with('search',$search)->with('type',$type)
            ->with('validityInfo',$data); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('superadmin.promocode.addtimevalidity');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->set_validation('',$request);

        $auth_id = Auth::User()->id;
		
		$validitycount = TimeValidation::where('year',$request->year)->where('month',$request->month)
		->where('week',$request->week)->where('day',$request->day)->get();
        
        $validitycount =count($validitycount);
		if($validitycount>0)
		{
			Session::flash('errorMessage', 'This licence period is already created!');
			return redirect(route('root.timevalidity.create'));
			
		}
		else{
			$timevalidation = new TimeValidation;
			$timevalidation->name=$request->name;
			$timevalidation->year=$request->year;
			$timevalidation->month=$request->month;
			$timevalidation->week=$request->week;
			$timevalidation->day=$request->day;
			$timevalidation->description=$request->desc;
			$timevalidation->status=$request->status;
			$timevalidation->created_by=$auth_id;
			$timevalidation->created_at=date("Y-m-d H:i:s");
			$timevalidation->save();
			Session::flash('message', 'Added Successfully!');
			return redirect(route('root.timevalidity.index'));
		}
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $timevalidity = TimeValidation::where('id',$id)->get();
        
         $timevalidity =$timevalidity->toArray();

         return view('superadmin/promocode/edittimevalidity')->with('timevalidity',$timevalidity);
        
             
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->set_validation($id,$request);

        $auth_id = Auth::User()->id; 
		$validitycount = TimeValidation::where('id','!=',$id)->where('year',$request->year)->where('month',$request->month)
		->where('week',$request->week)->where('day',$request->day)->get();
        
        $validitycount =count($validitycount);
		if($validitycount>0)
		{
			Session::flash('errorMessage', 'This licence period is already created!');
			//return redirect(route('root.timevalidity.edit'));
			return redirect(url('root/timevalidity/'.$id.'/edit'));
			
		}
		else{
			    $timevalidation =TimeValidation::find($id);
				$timevalidation->name=$request->name;
				$timevalidation->year=$request->year;
				$timevalidation->month=$request->month;
				$timevalidation->week=$request->week;
				$timevalidation->day=$request->day;
				$timevalidation->description=$request->desc;
				$timevalidation->status=$request->status;
				$timevalidation->updated_by=$auth_id;
				$timevalidation->updated_at=date("Y-m-d H:i:s");
				$timevalidation->save();
				Session::flash('message','Updated Successfully');
				return redirect(route('root.timevalidity.index'));
		}
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function delete(Request $request)
    {

        if($request->ajax()){
            $data = Input::all();
           
            $selectedValidity = $data['selectedValidity'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedValidity as $key => $value)
        {
            $validity = TimeValidation::find($value);
            $validity->status = '2';
            $validity->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }

    /*added by Rajlakshmi(01-06-16)*/
	public function download(Request $request)
    {

        if($request->ajax()){
            $data = Input::all();
           
            $selectedValidity = $data['selectedValidity'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
      $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Name</th>
        <th style="border:solid 1px #81C02F; padding:10px">Year</th>
		<th style="border:solid 1px #81C02F; padding:10px">Month</th>
		<th style="border:solid 1px #81C02F; padding:10px">Day</th>
		<th style="border:solid 1px #81C02F; padding:10px">Description</th>
		
        <th style="border:solid 1px #81C02F; padding:10px">Status</th></tr><tr></tr></thead>';
        foreach($selectedValidity as $key => $value)
        {
            $data = DB::table('time_validations')
            

            ->where('id','=', $value)
           
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				if((strlen($data['description']) > 50))
				{
					$desc = substr($data['description'],0,50).'...';
				}
				else
				{
					$desc = $data['description'];
				}
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['year'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['month'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['day'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
				
		}
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	
	/*added by Rajlakshmi(01-06-16)*/
    public function set_validation($id=null,$request)
     {
        $message=array(
            "name.required"=>"Name is required",
            "name.min"=>"The Name must be at least 3 characters.",
            "desc.required"=>"Description is required",
            );

        $this->validate($request,[
        'name' => 'required|min:3|unique:time_validations,name,'.$id.',id',
        'desc' => 'required',
        ],$message);
     }
       
}
