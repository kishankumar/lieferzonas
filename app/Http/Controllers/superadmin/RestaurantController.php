<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\RestDeliveryCost;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\Input;

use App\superadmin\RestDetail;
use App\superadmin\RestSetting;
use App\superadmin\RestEmailMap;
use App\superadmin\RestMobileMap;
use App\superadmin\RestLandlineMap;
use App\superadmin\RestPrivilegeLog;
use App\superadmin\PriceType;
use Illuminate\Support\Facades\Log;
use Session;
use DB;
use Auth;
use Validator;
use App\superadmin\Zipcode;

//use Input;
class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search = '';
        $type = '';
        if ($request->search) {
            $search = $request->search;
        }
        if ($request->type) {
            $type = $request->type;
        } else {
            $type = 'names';
        }


        $q = RestDetail::where('status', '!=', '2')->orderBy('id', 'desc');

        if (!empty($search)) {
            $q->where(function ($q2) use ($search) {
                $q2->where('f_name', 'LIKE', "%$search%");
                $q2->orWhere('l_name', 'LIKE', "%$search%");
                $q2->orWhere('add1', 'LIKE', "%$search%");
            });
        }

        if ($type == 'name') {
            $q->orderBy('f_name', 'asc');
        } else if ($type == 'name-desc') {
            $q->orderBy('f_name', 'desc');
        } else if ($type == 'addr-desc') {
            $q->orderBy('add1', 'desc');
        } else if ($type == 'addr') {
            $q->orderBy('add1', 'asc');
        } else {
            //$q->orderBy('f_name', 'asc');
            $q->orderBy('created_at', 'desc');
        }
        $data['restro_details'] = $q->paginate(10);

        $data['search'] = $search;
        $data['type'] = $type;
        return view('superadmin/restaurants/index')->with($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['email_types'] = DB::table('email_types')->where('status', 1)->get();

        $data['priceTypes'] = PriceType:: where('status', 1)->select('id', 'name')->get();
        $data['countries'] = DB::table('countries')->orderBy('name', 'asc')->select('id', 'name')->where('status', '=', '1')->get();
        $data['zipcodes'] = Zipcode::where('status', 1)->select('id', 'name')->get();
        return view('superadmin/restaurants/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //$this->set_validation('',$request);

        $getrestname = RestDetail::where('f_name', $request->f_name)->where('status', '!=', 2)->get();
        $restnamecount = count($getrestname->toArray());
        if ($restnamecount > 0) {
            Session::flash('message', 'This Restaurant name already exist!');
            return redirect(route('root.restaurant.create'));
        } else {
            $restaurants = new RestDetail;

            $destinationpath = public_path() . "/uploads/superadmin/restaurants";
            $pic = $request->file;

            if ($pic) {
                $extension = $pic->getClientOriginalExtension(); // getting image extension
                $filename = time() . rand(111, 999) . '.' . $extension; // renameing image
                $pic->move($destinationpath, $filename);
            } else {
                $filename = '';
            }

            /* print_r($request->restro_email[0]);

             exit();*/
            DB::beginTransaction();

            ////////////////Add Basic Information////////////////
            $restaurants->f_name = $request->f_name;
            //$restaurants->l_name=$request->l_name;
            $restaurants->add1 = $request->add1;
            $restaurants->add2 = $request->add2;
            $restaurants->city = $request->city;
            $restaurants->state = $request->state;
            $restaurants->country = $request->country;
            $restaurants->pincode = $request->zipcode;
            $restaurants->fax = $request->fax;
            $restaurants->total_email = $request->restro_email_count;
            $restaurants->total_mobile = $request->restro_mobile_count;
            $restaurants->total_landline = $request->restro_phone_count;
            $restaurants->logo = $filename;
            $restaurants->created_by = Auth::User()->id;
            $restaurants->updated_by = Auth::User()->id;
            $restaurants->save();

            $rest_detail_id = $restaurants['id'];
            ////////////Add restaurant settings/////////////////////

            $rest_delivery_costs = new RestDeliveryCost;
            $rest_delivery_costs->restaurant = $rest_detail_id;
            $rest_delivery_costs->minimum_order_amount = $request->restro_minimum_order;
            if ($request->restro_below_minimum_order) {
                $rest_delivery_costs->below_order_delivery_cost = $request->restro_below_minimum_order;
            }
            else {
                $rest_delivery_costs->below_order_delivery_cost = null;
            }
            $rest_delivery_costs->above_order_delivery_cost = $request->restro_delivery_cost;
            $rest_delivery_costs->save();

            $restSettings = new RestSetting;

            $restSettings->rest_detail_id = $rest_detail_id;
            $restSettings->is_open = '1';
            $restSettings->is_new = '0';
            $restSettings->header_color = '#ffffff';
            $restSettings->body_color = '#ffffff';
            $restSettings->privilege_id = '1';
            $restSettings->created_by = Auth::User()->id;
            $restSettings->updated_by = Auth::User()->id;

            $restSettings->save();

            ///////////////////Add Privilege to log table////////////////

            $privilegeLog = new RestPrivilegeLog;

            $privilegeLog->rest_detail_id = $rest_detail_id;
            $privilegeLog->privilege_id = '0';
            $privilegeLog->created_by = Auth::User()->id;
            $privilegeLog->updated_by = Auth::User()->id;

            $privilegeLog->save();

            ////////////////Add Multiple Emails////////////////

            $restro_email_count = $request->restro_email_count;
            $email = $request->restro_email;
            $email_type = $request->restro_email_type;
            $is_primary_email = $request->is_primary_email;
            if ($is_primary_email == '') {
                $is_primary_email = 1;
            } else {
                $is_primary_email = $is_primary_email;
            }

            for ($i = 0; $i < $restro_email_count; $i++) {

                if ($i == ($is_primary_email - 1)) {
                    $is_primary_val = 1;
                } else {
                    $is_primary_val = 0;
                }
                $getemail = RestEmailMap::where('email', $email[$i])->where('is_primary', '=', 1)->where('status', '!=', 2)->get();
                $emailcount = count($getemail->toArray());
                if ($emailcount > 0) {
                    Session::flash('message', 'Primary Email Id already exist!');
                    return redirect(route('root.restaurant.create'));
                } else {
                    DB::table('rest_email_maps')->insert(
                        ['rest_detail_id' => $rest_detail_id, 'email' => $email[$i], 'email_type' => $email_type[$i], 'is_primary' => $is_primary_val]
                    );
                }
            }

            ////////////////Add Multiple Mobiles////////////////

            $restro_mobile_count = $request->restro_mobile_count;
            $mobile = $request->restro_mobile;
            $mobile_designation = $request->restro_mobile_desig;
            $is_primary_mobile = $request->is_primary_mobile;
            if ($is_primary_mobile == '') {
                $is_primary_mobile = 1;
            } else {
                $is_primary_mobile = $is_primary_mobile;
            }

            for ($i = 0; $i < $restro_mobile_count; $i++) {

                if ($i == ($is_primary_mobile - 1)) {
                    $is_primary_val = 1;
                } else {
                    $is_primary_val = 0;
                }
                DB::table('rest_mobile_maps')->insert(
                    ['rest_detail_id' => $rest_detail_id, 'mobile' => $mobile[$i], 'name' => $mobile_designation[$i], 'is_primary' => $is_primary_val]
                );
            }

            ////////////////Add Multiple Phones Extensions(Landline)////////////////

            $restro_phone_count = $request->restro_phone_count;
            $phone = $request->restro_landline;
            $phone_extension = $request->restro_landlie_ext;
            $phone_designation = $request->restro_landlie_desig;

            for ($i = 0; $i < $restro_phone_count; $i++) {

                DB::table('rest_landline_maps')->insert(
                    ['rest_detail_id' => $rest_detail_id, 'landline' => $phone, 'extension' => $phone_extension[$i], 'name' => $phone_designation[$i]]
                );
            }


            /////////////////////Add price type////////////////////////

            $total_prictType_selected = count($request->price_types);
            $priceTypes = $request->price_types;

            for ($i = 0; $i < $total_prictType_selected; $i++) {


                DB::table('rest_price_maps')->insert(

                    ['rest_detail_id' => $rest_detail_id, 'price_type_id' => $priceTypes[$i], 'created_by' => Auth::User('user')->id, 'updated_by' => Auth::User('user')->id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                );
            }

            if ($i == $total_prictType_selected) {
                DB::commit();
            } else {
                DB::rollBack();
            }

            Session::flash('message', 'Restaurant Added Successfully!');
            if ($request->assignZip) {
                return redirect(url('root/zipcodes/setting/create?restaurant=' . $rest_detail_id));
            } else {
                return redirect(route('root.restaurant.index'));
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id) {
            $count = DB::table('rest_details')->where('id', '=', $id)->where('status', '!=', '2')->count();
            if (!$count) {
                Session::flash('message', 'Please select restaurant again.');
                return redirect(route('root.shop.orders.index'));
            }
            if (!preg_match('/^\d+$/', $id)) {
                Session::flash('message', 'Please select restaurant again.');
                return redirect(route('root.shop.orders.index'));
            }
        }
        $data['viewDetails'] = RestDetail:: where('id', $id)->where('status', '!=', '2')->get();
        return view('superadmin.shopping.viewpage')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['restDetails'] = RestDetail:: where('id', $id)->select('id', 'f_name', 'l_name', 'add1', 'add2', 'city', 'state', 'country', 'pincode', 'fax', 'logo', 'status')->first();

        $countryId = $data['restDetails']['country'];
        $stateId = $data['restDetails']['state'];


        $data['emails'] = RestEmailMap::where('rest_detail_id', $id)->where('status', 1)->select('id', 'email', 'email_type', 'is_primary')->get();

        $data['mobiles'] = RestMobileMap::where('rest_detail_id', $id)->where('status', 1)->select('id', 'mobile', 'name', 'is_primary')->get();

        $data['landLines'] = RestLandlineMap::where('rest_detail_id', $id)->where('status', 1)->select('id', 'landline', 'extension', 'name')->get();

        $data['email_types'] = DB::table('email_types')->where('status', 1)->get();

        $data['priceTypes'] = PriceType:: where('status', 1)->select('id', 'name')->get();

        $data['priceTypesMaps'] = DB:: table('rest_price_maps')->where('rest_detail_id', $id)->lists('price_type_id');

        $data['countries'] = DB::table('countries')->select('id', 'name')->where('status', '=', '1')->get();

        $data['states'] = DB::table('states')->select('id', 'name')->where('status', '=', '1')->where('country_id', $countryId)->get();

        $data['cities'] = DB::table('cities')->select('id', 'name')->where('status', '=', '1')->where('country_id', $countryId)->where('state_id', $stateId)->get();

        $data['zipcodes'] = Zipcode::where('status', 1)->select('id', 'name')->get();

        $data['delivery_costs'] = RestDeliveryCost::where('status', 1)->where('restaurant', $id)->first();

        return view('superadmin/restaurants/edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $getrestname = RestDetail::where('f_name', $request->f_name)->where('id', '!=', $id)->where('status', '!=', 2)->get();
        $restnamecount = count($getrestname->toArray());
        if ($restnamecount > 0) {
            Session::flash('message', 'This Restaurant name already exist!');
            return redirect(url('root/restaurant/' . $id . '/edit'));
        } else {

            $restaurants = RestDetail::find($id);

            $destinationpath = public_path() . "/uploads/superadmin/restaurants";
            $pic = $request->file;

            if ($pic) {
                $extension = $pic->getClientOriginalExtension(); // getting image extension
                $filename = time() . rand(111, 999) . '.' . $extension; // renameing image
                $pic->move($destinationpath, $filename);
                $restaurants->logo = $filename;
            }

            DB::beginTransaction();
            ////////////////Add Basic Information////////////////
            $restaurants->f_name = $request->f_name;
            //$restaurants->l_name=$request->l_name;
            $restaurants->add1 = $request->add1;
            $restaurants->add2 = $request->add2;
            $restaurants->city = $request->city;
            $restaurants->state = $request->state;
            $restaurants->country = $request->country;
            $restaurants->pincode = $request->zipcode;
            $restaurants->fax = $request->fax;
            $restaurants->total_email = $request->restro_email_count;
            $restaurants->total_mobile = $request->restro_mobile_count;
            $restaurants->total_landline = $request->restro_phone_count;
            $restaurants->status = $request->status;
            $restaurants->save();

            $rest_detail_id = $restaurants['id'];

            // Set delivery costs
            $rest_delivery_costs = RestDeliveryCost::where("status", 1)->where("restaurant", $rest_detail_id)->first();
            $rest_delivery_costs->minimum_order_amount = $request->restro_minimum_order;
            if ($request->restro_below_minimum_order) {
                $rest_delivery_costs->below_order_delivery_cost = $request->restro_below_minimum_order;
            }
            else {
                $rest_delivery_costs->below_order_delivery_cost = null;
            }
            $rest_delivery_costs->above_order_delivery_cost = $request->restro_delivery_cost;
            $rest_delivery_costs->save();

            ////////////////Add Multiple Emails////////////////

            $restro_email_count = $request->restro_email_count;
            $email = $request->restro_email;
            $email_type = $request->restro_email_type;
            $is_primary_email = $request->is_primary_email;

            if ($is_primary_email == '') {
                $is_primary_email = 1;
            } else {
                $is_primary_email = $is_primary_email;
            }


            $deleted = DB::table('rest_email_maps')->where('rest_detail_id', $id)->delete();

            for ($i = 0; $i < $restro_email_count; $i++) {

                if ($i == ($is_primary_email - 1)) {
                    $is_primary_val = 1;
                } else {
                    $is_primary_val = 0;
                }
                $getemail = RestEmailMap::where('email', $email[$i])->where('rest_detail_id', '!=', $id)->where('is_primary', '=', 1)->where('status', '!=', 2)->get();
                $emailcount = count($getemail->toArray());
                if ($emailcount > 0) {
                    Session::flash('message', 'Primary Email Id already exist!');
                    return redirect(url('root/restaurant/' . $id . '/edit'));
                } else {
                    DB::table('rest_email_maps')->insert(
                        ['rest_detail_id' => $rest_detail_id, 'email' => $email[$i], 'email_type' => $email_type[$i], 'is_primary' => $is_primary_val]
                    );
                }
            }

            /////////////////////Add price type////////////////////////

            $total_prictType_selected = count($request->price_types);
            $priceTypes = $request->price_types;

            $deleted = DB::table('rest_price_maps')->where('rest_detail_id', $id)->delete();

            for ($i = 0; $i < $total_prictType_selected; $i++) {


                DB::table('rest_price_maps')->insert(

                    ['rest_detail_id' => $rest_detail_id, 'price_type_id' => $priceTypes[$i], 'created_by' => Auth::User('user')->id, 'updated_by' => Auth::User('user')->id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                );
            }

            if ($i == $total_prictType_selected) {
                DB::commit();
            } else {
                DB::rollBack();
            }

            ////////////////Add Multiple Mobiles////////////////

            $restro_mobile_count = $request->restro_mobile_count;
            $mobile = $request->restro_mobile;
            $mobile_designation = $request->restro_mobile_desig;
            $is_primary_mobile = $request->is_primary_mobile;
            if ($is_primary_mobile == '') {
                $is_primary_mobile = 1;
            } else {
                $is_primary_mobile = $is_primary_mobile;
            }

            DB::beginTransaction();

            $deleted = DB::table('rest_mobile_maps')->where('rest_detail_id', $id)->delete();

            for ($i = 0; $i < $restro_mobile_count; $i++) {

                if ($i == ($is_primary_mobile - 1)) {
                    $is_primary_val = 1;
                } else {
                    $is_primary_val = 0;
                }
                DB::table('rest_mobile_maps')->insert(
                    ['rest_detail_id' => $rest_detail_id, 'mobile' => $mobile[$i], 'name' => $mobile_designation[$i], 'is_primary' => $is_primary_val]
                );
            }

            if ($deleted and ($i == $restro_mobile_count)) {
                DB::commit();
            } else {
                DB::rollBack();
            }

            ////////////////Add Multiple Phones Extensions(Landline)////////////////

            DB::beginTransaction();

            $deleted = DB::table('rest_landline_maps')->where('rest_detail_id', $id)->delete();

            $restro_phone_count = $request->restro_phone_count;
            $phone = $request->restro_landline;
            $phone_extension = $request->restro_landlie_ext;
            $phone_designation = $request->restro_landlie_desig;

            for ($i = 0; $i < $restro_phone_count; $i++) {

                DB::table('rest_landline_maps')->insert(
                    ['rest_detail_id' => $rest_detail_id, 'landline' => $phone, 'extension' => $phone_extension[$i], 'name' => $phone_designation[$i]]
                );
            }

            if ($deleted and ($i == $restro_phone_count)) {
                DB::commit();
            } else {
                DB::rollBack();
            }
            Session::flash('message', 'Restaurant Updated Successfully!');
            return redirect(route('root.restaurant.index'));

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*added by Rajlakshmi(31-05-16)*/
    public function download(Request $request)
    {
        //echo 'rrrrr';exit();
        if ($request->ajax()) {
            $data = Input::all();
            $selectedItems = $data['selectedItems'];
        } else {
            $json['success'] = 0;
            echo json_encode($json);
            return;
        }
        $data1 = '';
        $data1 .= '<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Restaurant</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Address</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Email Id</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Mobile</th><th>Status</th></tr></thead>';
        foreach ($selectedItems as $key => $value) {
            $data = DB::table('rest_details')
                ->leftjoin('rest_mobile_maps', 'rest_details.id', '=', 'rest_mobile_maps.rest_detail_id')
                ->leftjoin('rest_email_maps', 'rest_details.id', '=', 'rest_email_maps.rest_detail_id')
                ->where('rest_email_maps.is_primary', '=', 1)
                ->where('rest_mobile_maps.is_primary', '=', 1)
                ->where('rest_details.id', '=', $value)
                ->select('rest_details.f_name', 'rest_details.l_name', 'rest_details.add1', 'rest_details.add2',
                    'rest_email_maps.email', 'rest_mobile_maps.mobile'
                    , 'rest_details.status')->get();
            for ($i = 0, $c = count($data); $i < $c; ++$i) {
                $data = (array)$data[$i];
            }

            //print_r($data); die;
            if ($data['status'] == '1') {
                $status = 'Active';
            } else {
                $status = 'Deactive';
            }
            $data1 .= '<tr><td style="border:solid 1px #81C02F; padding:10px">' . $data['f_name'] . ' ' . $data['l_name'] . '</td>
				<td style="border:solid 1px #81C02F; padding:10px">' . $data['add1'] . ' ' . $data['add2'] . '</td>
				<td style="border:solid 1px #81C02F; padding:10px">' . $data['email'] . '</td>
				<td style="border:solid 1px #81C02F; padding:10px">' . $data['mobile'] . '</td>
				<td style="border:solid 1px #81C02F; padding:10px">' . $status . '</td>';
        }

        $data1 .= '</tr></table>';
        $sendata['data1'] = $data1;
        echo json_encode($sendata);
        return;
    }
    /*added by Rajlakshmi(31-05-16)*/
    /////Add validation////////
    public function set_validation($id = null, $request)
    {
        $message = array(
            "f_name.required" => "First Name is required",
            "f_name.min" => "The First name must be at least 2 characters.",
            "f_name.alpha" => "The First name must be alphabet",
            "add1.required" => "Adress is required",
            "city.required" => "City is required",
            "state.required" => "Adress is required",
            "country.required" => "Country is required",
            "file.required" => "Logo image is required",
            "file.mimes" => "Select valid image",


        );

        $this->validate($request, [
            'f_name' => 'required|min:2',
            'add1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'file' => 'required|mimes:jpeg,gif,png,jpg',


        ], $message);
    }
}
