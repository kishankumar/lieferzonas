<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use validator;
use App\front\FrontUserSubscription;
use Mail;
use File;
class FrontUserSubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $subscribeduser = FrontUserSubscription::select('*')->paginate(10);
	   //print_r($subscribeduser); die;
	   return view('superadmin/frontusersubscription/user')->with('subscribeduser',$subscribeduser);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('superadmin/frontusersubscription/createmail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$subscribeduser = FrontUserSubscription::select('email','name')->get();
		
		//print_r($subscribeduser); die;
		$count = count($subscribeduser); 
        $from_email = 'vikas.alivenetsolutions@gmail.com';
		$from_name = 'Lieferzonas Superadmin';	
        $subject = $request->subject;
        $msg = $request->message;
        
		if($count>0)
			{
				for ($i=0; $i  <$count ; $i=$i+10) { 

				$email=0;
				for ($j=0; $j < 10; $j++) { 

				if(($i+$j)<$count)
				{
				 $email .=','.$subscribeduser[$i+$j]->email;
				}
				
			   }
			   $uemail=str_replace("0,","",$email);
			   $uemail=explode(',',$uemail);
			   $data = array(
				
				'msg' => $msg,
				'uname' => 'User'
				 );
				$pageuser = 'superadmin.emails.usersubscription.touser';
				$this->sendusermail($pageuser,$data,$subject,$from_email,$from_name,$uemail);
				Session::flash('message', 'Mail sent!');
			
				return view('superadmin/frontusersubscription/createmail');
			   }
			}
			else
			{
				Session::flash('message', 'There is no subscribed User So, Mail not sent!');
			
				return view('superadmin/frontusersubscription/createmail');
			}
		
		/*foreach($subscribeduser as $subscribedusers)
		{
			$uemail = $subscribedusers->email;
			$uname = $subscribedusers->name;
			$data = array(
            
            'msg' => $msg,
            'uname' => 'User'
             );
			$pageuser = 'superadmin.emails.usersubscription.touser';
		    $this->sendusermail($pageuser,$data,$subject,$from_email,$from_name,$uemail);
		}*/
		
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function unsubscribe()
	{
	  $id = Input::get('subcribed_id');
	  DB::table('front_user_subscriptions')->where('id', '=', $id)->delete();
	  Session::flash('message', 'Unsubscribed Successfully!'); 
	  $json['success']=1;
	  echo json_encode($json);
	  return;
	}
	public function sendusermail($pageuser,$data,$subject,$from_email,$from_name,$uemail)
	{
		Mail::send($pageuser, $data, function($messa) use ($subject,$from_email,$from_name,$uemail)  {
            $messa->from($from_email, $from_name);
            $messa->to($uemail)->subject($subject);
           
        });
	} 

}
