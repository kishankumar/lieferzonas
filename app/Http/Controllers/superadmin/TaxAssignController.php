<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\RestTaxMap;
use App\superadmin\TaxSetting;
use App\superadmin\RestDetail;
use Validator;
use Auth;

class TaxAssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = RestDetail::where('status','=',1);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('f_name', 'LIKE', "%$search%");
                $q2->orWhere('l_name', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('f_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('f_name', 'desc');
        }else{
            $q->orderBy('f_name', 'asc');
        }
        $data['restro_names']=$q->paginate(10);
        $data['type']=$type;
        $data['search']=$search;
        $data['pages']=$data['restro_names']->toarray();
        return view('superadmin.AssignTax.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->restaurant){
            $count= DB::table('rest_details')->where('id','=',$request->restaurant)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.taxassign.create'));
            }
            if( ! preg_match('/^\d+$/', $request->restaurant) ){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.taxassign.create'));
            }
        }
        $data['restro_detail_id']='';
        $data['restro_detail_id']=$request->restaurant;
        $data['restro_names'] = DB::table('rest_details')->where('status',1)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();
        $data['tax_codes'] = DB::table('tax_settings')->where('status',1)->orderBy('id', 'asc')->select('id','tax_name','tax_type','description')->get();
        $data['sel_taxes'] = RestTaxMap::where('rest_id',$request->restaurant)->where('status','!=','2')->first();		
        //$data['tax_name_slug']=array();
        //foreach($data['tax_codes'] as $tax_codes){
            //$data['tax_name_slug'][$tax_codes->id] = ($tax_codes->tax_type);
        //}
        return view('superadmin/AssignTax/create')->with($data);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->restro_id){
            $count= DB::table('rest_details')->where('id','=',$request->restro_id)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select restaurant again.');
                return redirect(route('root.taxassign.create'));
            }
            if( ! preg_match('/^\d+$/', $request->restro_id) ){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.taxassign.create'));
            }
        }
        $messages=array(
            "taxes.required"=>'Please select tax',
            'tax_type.required'=>'Please select tax type',
            'tax_amount.required'=>'Please enter tax amount',
        );
        
        $messages=array("taxes.required"=>'Please select tax');
        $this->validate($request,[
            'taxes'=>'required|numeric',
            'tax_type'=>'required|alpha',
            'tax_amount'=>'required|numeric',
        ],$messages);
        
        $restroId=$request->restro_id;
        $taxes=$request->taxes;
        
        if($taxes){
            $created_at=date("Y-m-d H:i:s");$updated_at=date("Y-m-d H:i:s");
            $created_by=Auth::User('user')->id;$updated_by=Auth::User('user')->id;
            $count= RestTaxMap:: where('rest_id','=',$restroId)->count();
            if($count){
                $type=RestTaxMap:: where('rest_id','=',$restroId)->first();
            }else{
                $type= new RestTaxMap;
                $type->created_at=$created_at;
                $type->created_by=$created_by;
            }
            $type->rest_id=$restroId;
            $type->tax_setting_id=$taxes;
            $type->tax_type=$request->tax_type;
            $type->tax_amount=$request->tax_amount;
            $type->updated_by=Auth::User('user')->id;
            $type->updated_at=date("Y-m-d H:i:s");
            $type->status=1;
            $type->save();
        }
        
        Session::flash('message', 'Tax assign successfully.'); 
        return redirect(route('root.taxassign.index'));
    }
    
    public function fetchData(Request $request){
        $div='';
        if($request->ajax()){
            $rest_id=$request->rest_id;
            $rootTaxAssignMaps= DB::table('rest_tax_maps')
                ->join('tax_settings', 'rest_tax_maps.tax_setting_id', '=', 'tax_settings.id')
                ->select('tax_settings.tax_name')
                ->where('rest_tax_maps.rest_id',$rest_id)
                ->get();
            if(count($rootTaxAssignMaps)){
                foreach ($rootTaxAssignMaps as $rootTaxAssignMap ) {
                    $div .='<div class="col-md-4">'.$rootTaxAssignMap->tax_name.'</div>';
                }
            }
            echo $div;
        }else{
            echo $div;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
