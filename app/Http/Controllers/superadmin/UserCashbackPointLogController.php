<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\UserBonusPoint;
class UserCashbackPointLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		$fuserlist = DB::table('front_user_details')->orderBy('fname', 'asc')
		->select('front_user_id','fname','lname')->get();
	    return view('superadmin/userbonus/cashback_point_history')->with('fuserlist',$fuserlist);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function cashbackpointlog()
	{
		$user_id = Input::get('user_id'); 
		$cashbackhistory = DB::table('user_cashback_point_logs')
		->leftjoin('set_cashback_points','user_cashback_point_logs.activity_id','=','set_cashback_points.id')
		->where('user_cashback_point_logs.user_id','=',$user_id)
		->select('user_cashback_point_logs.*','set_cashback_points.cashback_title')
		->paginate(10);
		
		$data='<thead>
                    <tr>
					<th>S.No.</th>
                    <th>Event</th>
					<th>Order Id</th>
					<th>Transaction Type</th>
					<th>Amount</th>
					</tr>
                </thead><tbody>';
					
         $i=1;  
        if(count($cashbackhistory))
		{
			foreach($cashbackhistory as  $history )   
		    {
			  if($history->creditordebit==1)
			  {
				  $creditordebit = 'Credit';
			  }
			  else{
				  $creditordebit = 'Debit';
			  }
			 $data.= '<tr>
		          <td>'.$i.'</td>
				  <td>'.$history->cashback_title.'</td>
				  <td>'.$history->order_id.'</td>
				  <td>'.$creditordebit.'</td>
				  <td>'.$history->amount.'</td></tr>';
						
		        $i++;
		   }
           
           	   
		}
        else{
			
			$data.= '<tr><td colspan="8" align="center">
                            No Record Exist
                        </td></tr>';
                  
                
            
           
			
        }
		 $sendata['data']=$data;
		 print_r(json_encode($sendata));	
         $data.=  '</tbody>';
	}
}
