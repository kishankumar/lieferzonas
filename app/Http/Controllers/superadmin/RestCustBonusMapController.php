<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\RestCustBonusMap;
use App\superadmin\RestNewCustBonusLog;
use Validator;
use Auth;

class RestCustBonusMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->restaurant){
            $count= DB::table('rest_details')->where('id','=',$request->restaurant)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.assignbonus.create'));
            }
            if( ! preg_match('/^\d+$/', $request->restaurant) ){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.assignbonus.create'));
            }
        }
        $data['restro_detail_id']='';
        $data['restro_names'] = DB::table('rest_details')->where('status',1)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();
        $data['restro_detail_id']=$request->restaurant;
        $data['bonus_status']=0;$stamp_status=array();
        $bonus_status=DB:: table('rest_cust_bonus_maps')->where('rest_detail_id',$request->restaurant)->select('id','bonus_status')->get();
        if(count($bonus_status)>0){
            foreach($bonus_status as $info){
                $data['bonus_status'] = $info->bonus_status;
                break;
            }
        }
		
		
        return view('superadmin/BonusSetting/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->restro_id){
            $count= DB::table('rest_details')->where('id','=',$request->restro_id)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.assignbonus.create'));
            }
            if( ! preg_match('/^\d+$/', $request->restro_id) ){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.assignbonus.create'));
            }
        }
        $restroId=$request->restro_id;
        $bonus_status=$request->bonus_status;
        
        $count1 = DB:: table('rest_cust_bonus_maps')->where('rest_detail_id',$restroId)->count();
        if($count1>0){
            $content = RestCustBonusMap::where('rest_detail_id','=',$restroId)->first();
        }else{
            $content=new RestCustBonusMap;
            $content->created_at=date("Y-m-d H:i:s");
            $content->created_by=Auth::User('user')->id;
        }
        $content->rest_detail_id=$restroId;
        $content->bonus_status=$request->bonus_status;
        $content->updated_by=Auth::User('user')->id;
        $content->updated_at=date("Y-m-d H:i:s");
        $content->status=1;
        $content->save();
        
        $content1=new RestNewCustBonusLog;
        $content1->rest_detail_id=$restroId;
        $content1->is_new_cust_bonus=$request->bonus_status;
        $content1->type='S';
        $content1->created_at=date("Y-m-d H:i:s");
        $content1->updated_at=date("Y-m-d H:i:s");
        $content1->created_by=Auth::User('user')->id;
        $content1->updated_by=Auth::User('user')->id;
        $content1->status=1;
        $content1->save();
        
        Session::flash('message', 'Bonus assign successfully.'); 
        return redirect(route('root.assignbonus.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
