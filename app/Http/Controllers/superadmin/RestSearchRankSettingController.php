<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Validator;
use Input;
use App\superadmin\RestDetail;
use App\superadmin\RestSearchRankSetting;
use App\superadmin\Zipcode;
use App\superadmin\Rest_day;

class RestSearchRankSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        $data['search']='';
        $data['type']='';
        return view('superadmin.rankSetting.index')->with($data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sel_prices'] = RestSearchRankSetting::where('status','1')->orderBy('id', 'asc')->get();	
        return view('superadmin/rankSetting/setting')->with($data);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $rankInfo=$request->rank;
        if(count($rankInfo)){
            $created_at=date("Y-m-d H:i:s");$updated_at=date("Y-m-d H:i:s");
            $created_by=Auth::User('user')->id;$updated_by=Auth::User('user')->id;
            
            DB::beginTransaction();
            $counter=0;
            foreach ($rankInfo as $rankId=>$rankValue ) {
                $count= RestSearchRankSetting:: where('status','1')->where('rank',$rankId)->count();
                if($count){
                    $type=RestSearchRankSetting:: where('status','1')->where('rank',$rankId)->first();
                }else{
                    $type= new RestSearchRankSetting;
                    $type->created_at=$created_at;
                    $type->created_by=$created_by;
                }
                $type->rank=$rankId;
                $type->price=$rankValue;
                $type->updated_by=Auth::User('user')->id;
                $type->updated_at=date("Y-m-d H:i:s");
                $type->status=1;
                $type->save();
                $counter++;
            }
            if($counter==8){
                DB::commit();
                Session::flash('message', 'Rank price updated successfully.'); 
            }
            else{
                DB::rollBack();
                Session::flash('message', 'Rank price not updated.'); 
            }
        }else{
            Session::flash('message', 'Rank price not updated.'); 
        }
        return redirect(route('root.rank.setting.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show($id)
    {
        
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id){
            $count= DB::table('rest_search_rank_settings')->where('rank','=',$id)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select rank again.'); 
                return redirect(route('root.rank.setting.index'));
            }
            if( ! preg_match('/^\d+$/', $id) ){
                Session::flash('message', 'Please select rank again.'); 
                return redirect(route('root.rank.setting.index'));
            }
        }
        $data['sel_prices'] = RestSearchRankSetting::where('status','1')->orderBy('id', 'asc')->get();	
        //echo '<pre>';print_R($data['sel_prices']);die;
        return view('superadmin/rankSetting/setting')->with($data);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
