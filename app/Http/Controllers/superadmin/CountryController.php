<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\Country;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Session;
use Auth;
use DB;
class CountryController extends Controller
{
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = Country::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('created_at', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $country = $q->paginate(10);
        
        //$country = Country::where('status','!=',2)->get();
        return view('superadmin/country/index')->with('countries',$country)->with('search',$search)->with('type',$type);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/',
            //'description'=>'required|min:10',
            'myslug'=>'required'
        ]);
		$country_list=Country::where('name',$request->name)->where('status','!=',2)->get();
        $countrycount = count($country_list->toArray()); 
		if($countrycount>0)
		{
		  Session::flash('errmessage', 'This Country is already exist!');
		  return redirect(route('root.country.index'));
		}
		else{
        $country = new Country;
        $country->name = $request->name;
        $country->slug = $request->myslug;
        $country->description = $request->description;
        $country->status = $request->status;
        //$country->created_by = session user id;
        $country->save();
        Session::flash('message', 'Added Successfully!');
        return redirect(route('root.country.index'));
		
		}
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $country = Country::where('id',$id)->first();
        //print_r($country);exit();
        return view('superadmin/country/edit')->with('countries',$country);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/',
            //'description' => 'required'
        ]);
		$country_list=Country::where('id','!=',$id)->where('name',$request->name)->where('status','!=',2)->get();
        $countrycount = count($country_list->toArray()); 
		if($countrycount>0)
		{
		   Session::flash('errmessage', 'This Country is already exist!');
		   return redirect(url('root/country/'.$id.'/edit'));
		}
		else{
			$country = Country::find($id);
			$country->name = $request->name;
			$country->description = $request->description;
			$country->status = $request->status;
			$country->save();
			Session::flash('message', 'Updated Successfully!');
			return redirect(route('root.country.index'));
		}
        
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCountries= $data['selectedCountries'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedCountries as $key => $value)
        {
            $country = Country::find($value);
            $country->status = '2';
            $country->save();
        }
        //Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	 /*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCountries= $data['selectedCountries'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
       $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Country name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created at</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedCountries as $key => $value)
        {
             $data = DB::table('countries')
			->leftjoin('users', 'users.id', '=', 'countries.created_by')
			->where('countries.id','=',$value)
			->select('countries.*','users.email')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}	
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
			    <td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(31-05-16)*/
    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $country = Country::find($id);
            $country->status = $status;
            $country->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }

    public function generateSlug(Request $request)
    {
        $rawslug = $request->rawslug;
        $slug = Country::process_url($rawslug);
        echo '<div class="form-group"><label for="" class="col-md-3 col-sm-3 col-xs-12 control-label"> Slug : </label><div class="col-md-6 col-sm-6 col-xs-12"> <label class="col-md-3 col-sm-3 col-xs-12 form-control"><span id="myslug">'.$slug.'</span>&nbsp;&nbsp;</label> <button class="btn btn-default btn-xs" type="button" onclick="editSlug()"> Edit </button></div><input type="hidden" name="myslug" value="'.$slug.'" /></div>';
        exit();
    }
}
