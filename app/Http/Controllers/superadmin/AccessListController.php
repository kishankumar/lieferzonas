<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\AccessList;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Session;
use Auth;
use DB;

class AccessListController extends Controller
{
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='date-desc';
        }
        $q = AccessList::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('access_name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('access_name', 'asc');
        }else if($type=='name-desc'){
            $q->orderBy('access_name', 'desc');
        }else if($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }else if($type=='descrip'){
            $q->orderBy('description', 'asc');
        }else if($type=='date'){
            $q->orderBy('created_at', 'asc');
        }else if($type=='date-desc'){
            $q->orderBy('created_at', 'desc');
        }
        $access = $q->paginate(10);
        //$access = AccessList::where('status','!=',2)->get();
        return view('superadmin/accesslist/index')->with('accesslist',$access)->with('search',$search)->with('type',$type);

    }
    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'access_name'=>'required|unique:access_lists|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/',
            'description'=>'required|min:10'
        ]);

        $access = new AccessList;
        $access->access_name = $request->access_name;
        $access->description = $request->description;
        $access->status = $request->status;
        //$access->created_by = session user id;
        $access->save();

        return redirect(route('root.accesslist.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $access = AccessList::where('id',$id)->first();
        //print_r($role);exit();
		
        return view('superadmin/accesslist/edit')->with('access',$access);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'access_name' => 'required|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/|unique:access_lists,access_name,'.$id.",id",
            'description' => 'required|min:10'
        ]);
        $access = AccessList::find($id);
        $access->access_name = $request->access_name;
        $access->description = $request->description;
        $access->status = $request->status;
        $access->save();

        return redirect(route('root.accesslist.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedRoles = $data['selectedRoles'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedRoles as $key => $value)
        {
            $access = AccessList::find($value);
            $access->status = '2';
            $access->save();
        }
        //Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }

    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $access = AccessList::find($id);
            $access->status = $status;
            $access->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
	
		/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
       if($request->ajax()){
            $data = Input::all();
            $selectedRoles = $data['selectedRoles'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Access Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Access Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created By</th>
						<th style="border:solid 1px #81C02F; padding:10px">Created At</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedRoles as $key => $value)
        {
             $data = DB::table('access_lists')
			->leftjoin('users', 'users.id', '=', 'access_lists.created_by')
			->where('access_lists.id','=',$value)
			->select('access_lists.*','users.email')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['access_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['email'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
