<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\Root_categorie;
use Validator;
use Auth;
use App\superadmin\RestDetail;
use App\front\RestComplaint;
use App\superadmin\UserMaster;

class RestComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='created';
        }
        $q = RestComplaint::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('topic', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
                $q2->orWhere('name', 'LIKE', "%$search%");
                $q2->orWhere('email', 'LIKE', "%$search%");
                $q2->orWhere('phone', 'LIKE', "%$search%");
            });
        }
        if($type=='topic'){
            $q->orderBy('topic', 'asc');
        }elseif($type=='topic-desc'){
            $q->orderBy('topic', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='email-desc'){
            $q->orderBy('email', 'desc');
        }elseif($type=='email'){
            $q->orderBy('email', 'asc');
        }elseif($type=='phone-desc'){
            $q->orderBy('phone', 'desc');
        }elseif($type=='phone'){
            $q->orderBy('phone', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('created_at', 'asc');
        }else{
            $q->orderBy('created_at', 'desc');
        }
        $complaintInfo = $q->paginate(10);
        return view('superadmin.RestComplaint.complaint')->with('complaintInfo',$complaintInfo)->with('search',$search)->with('type',$type); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id){
            $count= DB::table('rest_complaints')->where('id','=',$id)->where('status','!=','2')->count();
            if(!$count){
                Session::flash('message', 'Please select topic again.'); 
                return redirect(route('root.rest.complaint.index'));
            }
            if( ! preg_match('/^\d+$/', $id) ){
                Session::flash('message', 'Please select topic again.'); 
                return redirect(route('root.rest.complaint.index'));
            }
        }
        //$content = RestComplaint::where('id','=',$id)->where('check_status','U')->first();
        $data['viewDetails'] = RestComplaint:: where('id',$id)->where('status','!=', '2')->get();
        
        $content = RestComplaint::find($id);
        $content->check_status = 'R';
        $content->save();
        return view('superadmin.RestComplaint.viewpage')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedPages = $data['selectedPages'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedPages as $key => $value)
        {
            $user = RestComplaint::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
}
