<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\RestSearchFilter;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Input;
class RestSearchFilterController extends Controller
{
    public function index()
    {
        $filter = RestSearchFilter::where('status','!=',2)->get();
        
		
		
        return view('superadmin/searchfilter/index')->with('filters',$filter);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $filter = RestSearchFilter::where('id',$id)->first();
        //print_r($filter);exit();
        return view('superadmin/searchfilter/edit')->with('filters',$filter);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/|unique:rest_search_filters,name,'.$id.",id",
            'low_value' => 'required|integer',
            'high_value' => 'required|integer'
        ]);
        $filter = RestSearchFilter::find($id);
        $filter->name = $request->name;
        $filter->low_value = $request->low_value;
        $filter->high_value = $request->high_value;
        $filter->status = $request->status;
        $filter->save();

        return redirect(route('root.searchfilter.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $filter = RestSearchFilter::find($id);
            $filter->status = $status;
            $filter->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
	
	/*added by Rajlakshmi(31-05-16)*/
     public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedSearchfilter = $data['selectedSearchfilter'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
            
    $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>            <th style="border:solid 1px #81C02F; padding:10px">Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Low value</th>
                        <th style="border:solid 1px #81C02F; padding:10px">High value</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created by</th>
						<th style="border:solid 1px #81C02F; padding:10px">Created at</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedSearchfilter as $key => $value)
        {
             $data = DB::table('rest_search_filters')
			
			->leftjoin('users', 'users.id', '=', 'rest_search_filters.created_by')
			->where('rest_search_filters.id','=',$value)
			->select('rest_search_filters.*','users.email')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['low_value'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['high_value'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['email'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	 /*added by Rajlakshmi(31-05-16)*/
}
