<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\City;
use App\superadmin\Country;
use App\superadmin\Zipcode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Session;
use Auth;
use DB;

class ZipcodeController extends Controller
{
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = Zipcode::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('created_at', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $data['zipcode'] = $q->paginate(10);
        
        //$data['zipcode'] = Zipcode::where('status','!=',2)->get();
        $data['city'] = City::where('status','=',1)->OrderBy('name','asc')->lists('name', 'id');
        $data['city']->prepend('Select City', '');
        
        return view('superadmin/zipcode/index')->with('data',$data)->with('search',$search)->with('type',$type);

    }
    
    public function create()
    {
        
    }
    
    public function store(Request $request)
    {
        $this->validate($request,[
            'zipcode'=>'required|unique:zipcodes,name|Regex:/^[a-zA-Z0-9äÄöÖüÜß«» -]+$/',
            //'description'=>'required|min:4',
            'city'=>'required'
        ]);

        $zipcode = new Zipcode;
        $zipcode->name = $request->zipcode;
        $zipcode->description = $request->description;
        $zipcode->city_id = $request->city;
        $zipcode->status = $request->status;
        //$zipcode->created_by = session user id;
        $zipcode->save();
        Session::flash('message', 'Added Successfully!');
        return redirect(route('root.zipcode.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['zipcode'] = Zipcode::where('id',$id)->first();
        $data['city'] = City::where('status','=',1)->OrderBy('name','asc')->lists('name', 'id');

        return view('superadmin/zipcode/edit')->with('data',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'zipcode' => 'required|Regex:/^[a-zA-Z0-9äÄöÖüÜß«» -]+$/|unique:zipcodes,name,'.$id.",id",
            //'description' => 'required|min:4'
        ]);
        $zipcode = Zipcode::find($id);
        $zipcode->name = $request->zipcode;
        $zipcode->description = $request->description;
        $zipcode->city_id = $request->city;
        $zipcode->status = $request->status;
        $zipcode->save();
        Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.zipcode.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedZipcodes = $data['selectedZipcodes'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedZipcodes as $key => $value)
        {
            $role = Zipcode::find($value);
            $role->status = '2';
            $role->save();
        }
        //Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }
     /*added by Rajlakshmi(31-05-16)*/
     public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedZipcodes = $data['selectedZipcodes'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
            
    $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
        <thead style="background:#81C02F; color:#fff;">
        <tr>            <th style="border:solid 1px #81C02F; padding:10px">Zipcode</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created at</th>
                        <th style="border:solid 1px #81C02F; padding:10px">City name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th>
                        </tr><tr></tr></thead>';
        foreach($selectedZipcodes as $key => $value)
        {
             $data = DB::table('zipcodes')
             ->leftjoin('cities', 'cities.id', '=', 'zipcodes.city_id')
            ->leftjoin('users', 'users.id', '=', 'zipcodes.created_by')
            ->where('zipcodes.id','=',$value)
            ->select('zipcodes.*','users.email','cities.name as cname')
            ->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
           } 
            
               //print_r($data); die; 
                
                if($data['0']['status']=='1')
                {
                    $status='Active';
                }
                else
                {
                    $status='Deactive';
                }
                if((strlen($data['0']['description']) > 50))
                {
                    $desc = substr($data['0']['description'],0,50).'...';
                }
                else
                {
                    $desc = $data['0']['description'];
                }               
                $cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
                
                $data1.= '<tr>
                <td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
                <td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
                <td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
                <td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['cname'].'</td>
                <td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
        
        $data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
     /*added by Rajlakshmi(31-05-16)*/
    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $role = Zipcode::find($id);
            $role->status = $status;
            $role->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
}
