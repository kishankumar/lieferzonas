<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\SuperAdminSetting;


class SuperAdminSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('super_admin_settings')
            ->where('status','!=', '2')
            ->select('*')
            ->paginate(10);
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
                 $data[$i] = (array) $data[$i];
        }
       
        return view('superadmin.settings.setting')
        ->with('setting',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->set_validation('',$request); 
        $auth_id = Auth::User()->id; 
        $setting=SuperAdminSetting::find($request->setting_id); 
        $setting->cashback_point_value=$request->cashback_point_value;
        $setting->bonus_point_value=$request->bonus_point_value;
        $setting->service_tax=$request->service_tax;
		$setting->status=$request->status;
        $setting->updated_by=$auth_id;
        $setting->updated_at=date("Y-m-d H:i:s");
        $setting->save();
		Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.setting.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	/*added by Rajlakshmi(01-06-16)*/
	
    public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedsetting = $data['setting_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['setting']=SuperAdminSetting::where('id',$selectedsetting)->first();
        $setting = $data['setting']->toArray();
        if(count($setting)>0){
            $json['success']=1;
            $json['id']= $setting['id'];
            $json['cashback_point_value']=$setting['cashback_point_value'];
            $json['bonus_point_value']=$setting['bonus_point_value'];
            $json['service_tax']=$setting['service_tax'];
            $json['status']=$setting['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
	
	

	
	
    public function set_validation($id=null,$request)
     {
        $message=array(
            "cashback_point_value.required"=>"Cashback point value is required",
			"bonus_point_value.required"=>"Bonus point value is required",
            "service_tax.required"=>"Service tax is required",
            );

        $this->validate($request,[
        'cashback_point_value' => 'required',
        'cashback_point_value' => 'required',
		'service_tax' => 'required',
        ],$message);
     }
}
