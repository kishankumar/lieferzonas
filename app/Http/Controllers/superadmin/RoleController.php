<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\superadmin\UserRole;
use Input;
use Auth;
use DB;
use Session;


class RoleController extends Controller
{
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = UserRole::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('user_roles.role_name', 'LIKE', "%$search%");
                $q2->orWhere('user_roles.description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('user_roles.role_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('user_roles.role_name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('user_roles.description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('user_roles.description', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('user_roles.created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('user_roles.created_at', 'asc');
        }else{
            $q->orderBy('user_roles.role_name', 'asc');
        }
        $role=$q->paginate(10);
        //$role = UserRole::where('status','!=',2)->get();
        
        return view('superadmin/roles/index')->with('roles',$role)->with('search',$search)->with('type',$type);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'role_name'=>'required|unique:user_roles|Regex:/^[a-zA-Z0-9äÄöÖüÜß«» -]*$/',
            'description'=>'required|min:10'
        ]);

        $role = new UserRole;
        $role->role_name = $request->role_name;
        $role->description = $request->description;
        $role->status = $request->status;
        //$role->created_by = session user id;
        $role->save();

        return redirect(route('root.roles.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $role = UserRole::where('id',$id)->first();

        return view('superadmin/roles/edit')->with('roles',$role);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'role_name' => 'required|Regex:/^[a-zA-Z0-9äÄöÖüÜß«» -]*$/|unique:user_roles,role_name,'.$id.",id",
            'description' => 'required|min:10'
        ]);
        $role = UserRole::find($id);
        $role->role_name = $request->role_name;
        $role->description = $request->description;
        $role->status = $request->status;
        $role->save();

        return redirect(route('root.roles.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedRoles = $data['selectedRoles'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedRoles as $key => $value)
        {
            $role = UserRole::find($value);
            $role->status = '2';
            $role->save();
        }
        //Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
	 public function download(Request $request)
    {
		//echo 'rrrrr';exit();
       if($request->ajax()){
            $data = Input::all();
            $selectedRoles = $data['selectedRoles'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		$data1='';
        $data1.='<table id="table1"><thead><tr><th>Role name</th>
                        <th>Description</th>
                        <th>Created by</th>
                        <th>Created at</th>
						<th>Status</th></tr></thead>';
        foreach($selectedRoles as $key => $value)
        {
            $data = DB::table('user_roles')
			->leftjoin('users', 'users.id', '=', 'user_roles.created_by')
            ->where('user_roles.id','=', $value)
           
            ->select('user_roles.*','users.email')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				$cdate = date('d M Y h:i:s',strtotime($data['created_at']));
				$data1.= '<tr><td>'.$data['role_name'].'</td><td>'.$data['description'].'</td>
				<td>'.$data['email'].'</td><td>'.$cdate.'</td><td>'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
    /*added by Rajlakshmi(31-05-16)*/
    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $role = UserRole::find($id);
            $role->status = $status;
            $role->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
}
