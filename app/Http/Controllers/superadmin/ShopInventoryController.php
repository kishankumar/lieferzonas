<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\ShopItem;
use App\superadmin\ShopInventory;
use Validator;use Auth;

class ShopInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $toDate = date('Y-m-d');
        $fromDate = date("Y-m-d", strtotime("-1 months"));
        
        $data['to_date']=date('m/d/Y');
        $data['from_date']=date("m/d/Y", strtotime("-1 months"));
        if($request->from){
            $fromDate = date("Y-m-d", strtotime($request->from));
            $data['from_date']=date("m/d/Y", strtotime($request->from));
        }
        if($request->to){
            $toDate = date("Y-m-d", strtotime($request->to));
            $data['to_date']=date("m/d/Y", strtotime($request->to));
        }
        
        //$data['InventoryInfo']= ShopInventory:: where('status', '!=','2')->orderBy('created_at', 'desc')->paginate(10);
        $data['InventoryInfo']= ShopInventory:: where('status','!=','2')
            ->where(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d")'),'>=',$fromDate)
            ->where(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d")'),'<=',$toDate)
            ->orderBy('created_at','desc')
            ->paginate(10);
        $data['pages']=$data['InventoryInfo']->toarray($data['InventoryInfo']);
        
		
        return view('superadmin.ShopManagement.inventory.index')->with($data); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category_names'] = DB::table('shop_categories')->where('status',1)->orderBy('name', 'asc')->select('id','name')->get();
		$auth_id = Auth::User()->id;
		$username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
		for ($i = 0, $c = count($username); $i < $c; ++$i) {
            $username[$i] = (array) $username[$i];
            
        }
        return view('superadmin/ShopManagement/inventory/create')->with($data)->with('username',$username);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages=array("category_id.required"=>'Please select category.');
        $this->validate($request,[
            'category_id'=>'required|integer',
        ],$messages);
        $quantity = $request->quantity;
        foreach($quantity as $key => $value)
        {
            $shop_detail=array();$price=0;
            $shop_detail = DB::table('shop_items')->where('id',$key)->get();
            foreach($shop_detail as $info){
                $price = $info->front_price;
                $quantity = $info->quantity;
                $metric_id = $info->metric_id;
                $item_category_id = $info->shop_category_id;
            }
            $totalPrice = $price*$value;
            $object = new ShopInventory;
            $object->shop_category_id=$item_category_id;
            $object->shop_item_id=$key;
            $object->metric_id=$metric_id;
            $object->quantity=  number_format($value,2, '.', '');
            $object->price=number_format($totalPrice,2, '.', '');
            $object->created_by=Auth::User('user')->id;
            $object->updated_by=Auth::User('user')->id;
            $object->created_at=date("Y-m-d H:i:s");
            $object->updated_at=date("Y-m-d H:i:s");
            $object->status=1;
            $object->save();
            
            $content = ShopItem::where('id',$key)->first();
            $content->quantity=number_format($quantity+$value,2);
            $content->save();
        }
        Session::flash('message', 'Inventory added Successfully!'); 
        return redirect(route('root.shop.inventory.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getCatItems(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCatId = $data['catId'];
            $selectedItems = $data['selectedItems'];
        }else{
            $json['success']=0;
            $json['html']='';
            echo json_encode($json);
            return;
        }
        $selectedItems= explode(',', $selectedItems);
        $item_list=ShopItem::where('status','1')->where('shop_category_id',$selectedCatId)->get();
        $item_list=$item_list->toArray();
        $datas='';
        if(count($item_list)>0){
            foreach($item_list as $item):
                if(in_array($item['id'],$selectedItems))
                {
                    $datas.='<div class="col-sm-3 col-lg-2"><label class="checkbox"><input type="checkbox" checked value='.$item['id'].' class="nitems" name="item['.$item['id'].']"><span>'.ucfirst($item['name']).'</span></label></div>';
                }
                else
                {
                    $datas.='<div class="col-sm-3 col-lg-2"><label class="checkbox"><input type="checkbox" value="'.$item['id'].'" class="nitems" name="item['.$item['id'].']"><span>'.ucfirst($item['name']).'</span></label></div>';
                }
            endforeach;
        }else{
            $datas='No Item exist in this category.';
        }
        $json['success']=0;
        $json['html']=$datas;
        echo json_encode($json);
        return;
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedItems = $data['selectedItems'];
        }else{
            $json['success']=0;
            $errors = array('name'=>'Please enter valid data.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        foreach($selectedItems as $key => $value)
        {
            $user = ShopInventory::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedItems = $data['selectedItems'];
        }else{
            $json['success']=0;
            $errors = array('name'=>'Please enter valid data.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Item Category</th>
        <th style="border:solid 1px #81C02F; padding:10px">Item Name</th>                
		<th style="border:solid 1px #81C02F; padding:10px">Item Quantity</th>
		<th style="border:solid 1px #81C02F; padding:10px">Price</th>
		<th style="border:solid 1px #81C02F; padding:10px">Created Date</th>
		<th style="border:solid 1px #81C02F; padding:10px">Item Status</th></tr></thead>';
        foreach($selectedItems as $key => $value)
        {
             $data = DB::table('shop_inventories')
            ->leftjoin('shop_categories', 'shop_inventories.shop_category_id', '=', 'shop_categories.id')
            ->leftjoin('shop_items' , 'shop_inventories.shop_item_id', '=', 'shop_items.id')
            ->where('shop_inventories.id','=', $value)
           
            ->select('shop_inventories.*','shop_categories.name as catname','shop_items.name as itemname')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['action']=='P')
				{
					$itemstatus='Items Added';
				}
				else
				{
					$itemstatus='Items Used';
				}
               	$cdate = date('j M Y',strtotime($data['created_at']));			
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['catname'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['itemname'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['quantity'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['price'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$itemstatus.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
}
