<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\State;
use App\superadmin\FrontStateMap;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
class FrontStateController extends Controller
{
    public function index()
    {
        $data['map'] = FrontStateMap::where('status','!=',2)->orderBy('priority','asc')->get();
        $data['state'] = State::where('status','=',1)->OrderBy('name','asc')->lists('name', 'id');
        $data['state']->prepend('Select State', '');
       
		
		
        return view('superadmin/homepagelist/state')->with('data',$data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        for($i=1;$i<=10;$i++)
        {
            $state = FrontStateMap::where('priority','=',$request['priority'.$i])->first();
            $state->state_id = $request['state'.$i];
            $state->save();
        }

        return redirect(route('root.statefrontlist.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
