<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\Page_list;
use Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = Page_list::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('page_name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('page_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('page_name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }else{
            $q->orderBy('page_name', 'asc');
        }
        $data['pagesinfo'] = $q->paginate(10);
        //$data['pagesinfo']= Page_list:: where('status', '!=','2')->paginate(10);
        //$data['pagesinfo']=$Page_list->toArray();

        return view('superadmin/pages/page')->with($data)->with('search',$search)->with('type',$type);

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		
        return view('superadmin/pages/add_page');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages=array("page_name.required"=>'Enter Page Name');
        $this->validate($request,[
            //'restaurant_id'=>'required',
            'page_name'=>'required|min:3|unique:page_lists|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            'description'=>'required|min:6',
            //'confirm_password'=>'required|same:password|min:6',
        ],$messages);
        
        $page_list= new Page_list;
        $page_list->page_name=$request->page_name;
        $page_list->description=$request->description;
        
        $page_list->created_by=Auth::User('user')->id;
        $page_list->updated_by=Auth::User('user')->id;
        
        $page_list->created_at=date("Y-m-d H:i:s");
        $page_list->updated_at=date("Y-m-d H:i:s");
        $page_list->status=$request->status;
        $page_list->save();
        
        Session::flash('message', 'Added Successfully!');
        return redirect(route('root.pages.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pages']=Page_list::find($id);
       //  echo '<pre>';print_r($data['pages']);die;
        
        return view('superadmin/pages/edit_page')->with($data); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages=array("page_name.required"=>'Enter Page Name');
        $this->validate($request,[
            //'restaurant_id'=>'required',
            'page_name'=>'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/|unique:page_lists,page_name,'.$id,
            'description'=>'required|min:6',
            //'confirm_password'=>'required|same:password|min:6',
        ],$messages);
        
        $page_list=Page_list::find($id);
        $page_list->page_name=$request->page_name;
        $page_list->description=$request->description;
        
        //$page_list->created_by=Auth::User('user')->id;
        $page_list->updated_by=Auth::User('user')->id;
        
        $page_list->updated_at=date("Y-m-d H:i:s");
        $page_list->status=$request->status;
        $page_list->save();
        
        Session::put('message','Updated Successfully');
        return redirect(route('root.pages.index'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedPages = $data['selectedPages'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedPages as $key => $value)
        {
            $user = Page_list::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(31-05-16)*/
	 public function download(Request $request)
    {
		//echo 'rrrrr';exit();
       if($request->ajax()){
            $data = Input::all();
            $selectedPages = $data['$selectedPages'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		$data1='';
        $data1.='<table id="table1" ><thead><tr><th>Page name</th>
                        <th>Description</th>
                        <th>Status</th></tr><tr></tr></thead>';
        foreach($selectedPages as $key => $value)
        {
            $data = DB::table('page_lists')
			->where('id','=', $value)
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if((strlen($data['description']) > 50))
				{
					$desc = substr($data['description'],0,50).'...';
				}
				else
				{
					$desc = $data['description'];
				}
				$data1.= '<tr><td>'.$data['page_name'].'</td><td>'.$data['description'].'</td>
				<td>'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
}
