<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\RestZipcodeDeliveryMap;
use App\superadmin\RestDetail;
use Validator;
use Auth;

class ZipCodeSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = RestDetail::where('status','=',1);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('f_name', 'LIKE', "%$search%");
                $q2->orWhere('l_name', 'LIKE', "%$search%");
            });
        }
        
        if($type=='name'){
            $q->orderBy('f_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('f_name', 'desc');
        }else{
            $q->orderBy('f_name', 'asc');
        }
        $data['restro_names']=$q->paginate(10);
        $data['type']=$type;
        $data['search']=$search;
        $data['pages']=$data['restro_names']->toarray($data['restro_names']);
        
        //$zipCodeInfo= RestZipcodeDeliveryMap:: where('status', '!=','2')->orderBy('id', 'desc')->paginate(10);
        //$data['restro_names'] = DB::table('rest_details')->where('status',1)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->paginate(10);
        return view('superadmin.zipcodesetting.index')->with($data);
    }
    
    public function getdata(Request $request){
        if($request) {
            $rest_id=$request->rest_id;
            $rootZipCodeMaps= DB::table('rest_zipcode_delivery_maps')
                ->join('zipcodes', 'rest_zipcode_delivery_maps.zipcode_id', '=', 'zipcodes.id')
                ->select('zipcodes.name')
                ->where('rest_zipcode_delivery_maps.rest_id',$rest_id)
                ->get();
            $div='';
            if(count($rootZipCodeMaps)){
                foreach ($rootZipCodeMaps as $rootZipCodeMap ) {
                    $div .='<div class="col-md-4">'.$rootZipCodeMap->name.'</div>';
                }
            }
            echo $div;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create(Request $request)
    {
        if($request->restaurant){
            $count= DB::table('rest_details')->where('id','=',$request->restaurant)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.zipcodes.setting.create'));
            }
            if( ! preg_match('/^\d+$/', $request->restaurant) ){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.zipcodes.setting.create'));
            }
        }
        $data['restro_detail_id']='';
        $data['restro_names'] = DB::table('rest_details')->where('status',1)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();
        $data['zip_codes'] = DB::table('zipcodes')->where('status',1)->orderBy('id', 'asc')->select('id','name','description')->get();
        $data['restro_detail_id']=$request->restaurant;
        $data['sel_zipcodes']=array();$sel_zipcodes=array();
        
        $sel_zipcodes=RestZipcodeDeliveryMap::where('rest_id',$request->restaurant)->where('status','!=','2')->lists('zipcode_id');		
        $sel_zipcodes = $sel_zipcodes->toArray();
        if(count($sel_zipcodes))
            $data['sel_zipcodes']=$sel_zipcodes;
        
        //echo '<pre>';print_R($data['sel_zipcodes']);die;
        return view('superadmin/zipcodesetting/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->restro_id){
            $count= DB::table('rest_details')->where('id','=',$request->restro_id)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.zipcodes.setting.create'));
            }
            if( ! preg_match('/^\d+$/', $request->restro_id) ){
                Session::flash('message', 'Please select restaurant again.'); 
                return redirect(route('root.zipcodes.setting.create'));
            }
        }
        
        $restroId=$request->restro_id;
        $zipcodes=$request->zipcodes;
        //echo '<pre>';print_r($zipcodes);die;
        
        foreach($zipcodes  as $zipcodeid){
            $created_at=date("Y-m-d H:i:s");$updated_at=date("Y-m-d H:i:s");
            $created_by=Auth::User('user')->id;$updated_by=Auth::User('user')->id;
            $count= RestZipcodeDeliveryMap:: where('rest_id','=',$restroId)->where('zipcode_id', '=',$zipcodeid)->count();
            if($count){
                $type=RestZipcodeDeliveryMap:: where('rest_id','=',$restroId)->where('zipcode_id', '=',$zipcodeid)->first();
            }else{
                $type= new RestZipcodeDeliveryMap;
                $type->created_at=$created_at;
                $type->created_by=$created_by;
            }
            $type->zipcode_id=$zipcodeid;
            $type->rest_id=$restroId;
            $OZipCode='';
            $zipCodeValue = DB::table('zipcodes')->where('id',$zipcodeid)->select('id','name','description')->get();
            if(count($zipCodeValue)){
                foreach ($zipCodeValue as $Value ) {
                    $OZipCode = $Value->name;
                }
            }
            $type->zipcode=$OZipCode;
            $type->updated_by=Auth::User('user')->id;
            $type->updated_at=date("Y-m-d H:i:s");
            $type->status=1;
            $type->save();
        }
        $deleted= DB::table('rest_zipcode_delivery_maps')->where('rest_id',$restroId)->whereNotIn('zipcode_id',$zipcodes)->delete();
        
        Session::flash('message', 'Zip Codes assign successfully.'); 
        return redirect(route('root.zipcodes.setting.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
