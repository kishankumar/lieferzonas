<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\Metric;
use App\superadmin\ShopMetricCategory;
use Validator;
use Auth;

class MetricsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = Metric::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('code', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='code-desc'){
            $q->orderBy('code', 'desc');
        }elseif($type=='code'){
            $q->orderBy('code', 'asc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $metricInfo=$q->paginate(10);
        
        return view('superadmin.ShopManagement.metrics.index')->with('metricInfo',$metricInfo)->with('search',$search)->with('type',$type); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$name = Input::get('name');//for get input field value
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages = array(
            'end_after' => "Valid from date should be before valid to date.",
        );
        $userData = array(
            'name'      => $data['name'],
            'description'     =>  $data['description'],
            'code'     =>  $data['code'],
        );
        
        $metrics_id = Input::get('metrics_id');
        if($metrics_id){
            $rules = array(
                'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'description'     =>  'required|min:6',
                'code'     =>  'required|min:1|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            );
        }else{
            $rules = array(
                'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'description'     =>  'required|min:6',
                'code'     =>  'required|min:1|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            );
        }
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($metrics_id){
            $count= Metric:: where('id','!=',$metrics_id)->where('name', '=',$request->name)->where('code', '=',$request->code)->where('status', '!=','2')->count();
        }else{
            $count= Metric:: where('name', '=',$request->name)->where('code', '=',$request->code)->where('status', '!=','2')->count();
        }
        if($count){
            $errors = array('name1'=>'Metric name has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($metrics_id){
            $object =Metric::find($metrics_id);
        }else{
            $object= new Metric;
            $object->created_at=date("Y-m-d H:i:s");
            $object->created_by=Auth::User('user')->id;
        }
        $object->name=$request->name;
        $object->code=$request->code;
        $object->description=$request->description;
        $object->updated_by=Auth::User('user')->id;
        $object->updated_at=date("Y-m-d H:i:s");
        $object->status=$request->status;
        $object->save();
        
        if($metrics_id){
            Session::flash('message', 'Metric updated Successfully!'); 
            $json['success']=1;
            $json['message']='Metric updated Successfully.';
        }else{
            Session::flash('message', 'Metric added Successfully!'); 
            $json['success']=1;
            $json['message']='Metric added Successfully.';
        }
        echo json_encode($json);
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedMetrics = $data['checkedMetrics'];
        
        $data['metrics']=Metric::find($selectedMetrics);
        $metrics = $data['metrics']->toArray();
        if(count($metrics)>0){
            $json['success']=1;
            $json['id']=$metrics['id'];
            $json['name']=$metrics['name'];
            $json['code']=$metrics['code'];
            $json['description']=$metrics['description'];
            $json['status']=$metrics['status'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedMetrics = $data['selectedMetrics'];
        }else{
            $json['success']=0;
            $errors = array('name'=>'Please enter valid data.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        foreach($selectedMetrics as $key => $value)
        {
            $count= ShopMetricCategory:: where('metric_id', $value)->where('status', '!=','2')->count();
            if($count){
                $errors = array('name'=>'Selected Metric already assigned to shop categories. it cann\'t be deleted.');
                $json['success']=0;
                $json['errors']=$errors;
                echo json_encode($json);
                return;
            }
        }
        
        foreach($selectedMetrics as $key => $value)
        {
            $user = Metric::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedMetrics = $data['selectedMetrics'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Metrics Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Metrics Code</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th></tr>
						<tr></tr></thead>';
        foreach($selectedMetrics as $key => $value)
        {
            $data = DB::table('metrics')
           

            ->where('id','=', $value)
           
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}
               	if((strlen($data['description']) > 50))
				{
					$desc = substr($data['description'],0,50).'...';
				}
				else
				{
					$desc = $data['description'];
				}			
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['code'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$desc.'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
}
