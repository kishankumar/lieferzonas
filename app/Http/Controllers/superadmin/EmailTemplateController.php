<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\EmailTemplate;
use Validator;
use Auth;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='title';
        }

        $q = EmailTemplate::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('title', 'LIKE', "%$search%");
                $q2->orWhere('text', 'LIKE', "%$search%");
            });
        }
        if($type=='title'){
            $q->orderBy('title', 'asc');
        }elseif($type=='title-desc'){
            $q->orderBy('title', 'desc');
        }elseif($type=='text-desc'){
            $q->orderBy('text', 'desc');
        }elseif($type=='text'){
            $q->orderBy('text', 'asc');
        }else{
            $q->orderBy('title', 'asc');
        }
        $TemplatesInfo=$q->paginate(10);

        return view('superadmin.emailtemplates.index')->with('TemplatesInfo',$TemplatesInfo)->with('search',$search)->with('type',$type); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$name = Input::get('name');//for get input field value
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages = array(
        );
        $userData = array(
            'title'      => $data['title'],
            'type'      => $data['type'],
            'text'     =>  $data['text']
        );
        $template_id = Input::get('template_id');
        if($template_id){
            $rules = array(
                'title'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß ]+$)+/',
                'type'     =>  'required',
                'text'     =>  'required|min:6'
            );
        }else{
            $rules = array(
                'title'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß ]+$)+/',
                'type'     =>  'required',
                'text'     =>  'required|min:6',
            );
        }
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($template_id){
            $count= EmailTemplate:: where('id','!=',$template_id)->where('title', '=',$request->title)->where('status', '!=','2')->count();
        }else{
            $count= EmailTemplate:: where('title', '=',$request->title)->where('status', '!=','2')->count();
        }
        if($count){
            $errors = array('title'=>'Title has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($template_id){
            $type=EmailTemplate::find($template_id);
        }else{
            $type= new EmailTemplate;
            $type->created_at=date("Y-m-d H:i:s");
            $type->created_by=Auth::User('user')->id;
        }
        $type->title=$request->title;
        $type->type=$request->type;
        $type->text=$request->text;
        $type->updated_by=Auth::User('user')->id;
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=$request->status;
        $type->save();
        
        if($template_id){
            Session::flash('message', 'Email template updated Successfully!'); 
            $json['success']=1;
            $json['message']='Email template updated Successfully.';
        }else{
            Session::flash('message', 'Email template added Successfully!'); 
            $json['success']=1;
            $json['message']='Email template added Successfully.';
        }
        echo json_encode($json);
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $checkedTemplate = $data['checkedTemplate'];
        
        $data['templates']=EmailTemplate::find($checkedTemplate);
        $templates = $data['templates']->toArray();
        if(count($templates)>0){
            $json['success']=1;
            $json['id']=$templates['id'];
            $json['title']=$templates['title'];
            $json['type']=$templates['type'];
            $json['text']=$templates['text'];
            $json['status']=$templates['status'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedTemplates = $data['selectedTemplate'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedTemplates as $key => $value)
        {
            $user = EmailTemplate::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	public function download(Request $request)
    {
       if($request->ajax()){
            $data = Input::all();
            $selectedTemplate = $data['selectedTemplate'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Title</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Text</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Type</th>
						<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedTemplate as $key => $value)
        {
             $data = DB::table('email_templates')
			->where('id','=',$value)
			->select('*')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if($data['0']['type']=='1')
				{
					$type='Superadmin';
				}
				if($data['0']['type']=='2')
				{
					$type='Admin';
				}	
				if($data['0']['type']=='3')
				{
					$type='Users';
				}
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['title'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['text'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$type.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
}
