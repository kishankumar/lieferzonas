<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\Spice;
use File;

class SpiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = DB::table('spices')->where('status','!=', '2');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $data=$q->paginate(10);
        
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data[$i] = (array) $data[$i];
        }

        
        return view('superadmin.spices.spice')->with('search',$search)->with('type',$type)
            ->with('spices',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	  public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedSpices = $data['selectedSpices'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedSpices as $key => $value)
        {
            $spice = Spice::find($value);
			//print_r($spice);
			$img = $spice->image; 
			$pathtofile = 'public/superadmin/uploads/spices/'.$img; 
			if($img!='')
			{
				File::Delete($pathtofile);
			}
            $spice->status = '2';
            $spice->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	 
	 
	 public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedspice = $data['spice_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['spice']=Spice::where('id',$selectedspice)->first();
        $spice = $data['spice']->toArray();
        if(count($spice)>0){
            $json['success']=1;
            $json['id']= $spice['id'];
            $json['name']=$spice['name'];
			$json['image']=$spice['image'];
            $json['description']=$spice['description'];
            $json['status']=$spice['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

      public function add_spice(Request $request)
     {
        if($request->spice_id=='0')
        {
			$spice=Spice::where('name',$request->name)->where('status','!=',2)->get();
			$spice = count($spice->toArray()); 
			if($spice>0)
			{
			  Session::flash('errmsg', 'This Name is already been taken!');

			  return redirect(route('root.spices.index'));
			}
			else{
				$this->set_validation('',$request); 
				$auth_id = Auth::User()->id;
				$spice= new Spice;
				$spice->name=$request->name;
				$destinationpath=public_path()."/superadmin/uploads/spices";


				$image=Input::file('image');
				
				 if($image!='')
				 {
					  $extension=  $image->getClientOriginalExtension(); // getting image extension
					  $filename=time().rand(111,999).'.'.$extension; // renameing image
					  $image->move($destinationpath,$filename);
					  $spice->image=$filename;
				 } 
				$spice->description=$request->description;
				$spice->status=$request->status;
				$spice->created_by=$auth_id;
				$spice->created_at=date("Y-m-d H:i:s");
				$spice->save();
				Session::flash('message', 'Added Successfully!');
				return redirect(route('root.spices.index'));
			}
        }
        else
        {  
	     $spice=Spice::where('name',$request->name)->where('id','!=',$request->spice_id)->where('status','!=',2)->get();
			$spice = count($spice->toArray()); 
			if($spice>0)
			{
			  Session::flash('errmsg', 'This Name is already been taken!');

			  return redirect(route('root.spices.index'));
			}
			else{
				$this->set_validation($request->spice_id,$request); 
				$auth_id = Auth::User()->id;
				$spice=Spice::find($request->spice_id);
				$spice->name=$request->name;
				$destinationpath=public_path()."/superadmin/uploads/spices";


				$image=Input::file('image');
				
				 if($image!='')
				 {
					  $extension=  $image->getClientOriginalExtension(); // getting image extension
					  $filename=time().rand(111,999).'.'.$extension; // renameing image
					  $image->move($destinationpath,$filename);
					  $spice->image=$filename;
				 } 
				$spice->description=$request->description;
				$spice->status=$request->status;
				$spice->updated_by=$auth_id;
				$spice->updated_at=date("Y-m-d H:i:s");
				$spice->save();
				Session::flash('message', 'Updated Successfully!');
				return redirect(route('root.spices.index'));
			}
        }
        
        
     }
	 
	 	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedspice = $data['selectedSpices'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Name</th>
                        
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedspice as $key => $value)
        {
             $data = DB::table('spices')
			
			->where('id','=',$value)
			->select('*')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if($data['0']['image']!='')
				{
					$img = "<img class='img-upload' src='http://localhost/lieferzonas/public/uploads/superadmin/spices/".$data['0']['image']."' style='max-width: 5%'>";
				}
				else
				{
					$img = '';
				}
				if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
	    public function set_validation($id=null,$request)
     {
        $message=array(
            "name.required"=>"Name is required",
            "description.required"=>"Description is required",
            );

        $this->validate($request,[
        //'name' => 'required|unique:spices,name,'.$id.',id',
		'name' => 'required',
		//'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
        'description' => 'required',
        ],$message);
     }
}
