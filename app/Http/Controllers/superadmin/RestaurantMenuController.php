<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\MenuCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RestaurantMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($restaurantId)
    {

        $rest_detail = DB::table("rest_details")->where("id", $restaurantId)->first();

        $categories = DB::table("rest_categories")->where("rest_detail_id", $restaurantId)->get();

        $menus = DB::table("rest_menus")->where("rest_detail_id", $restaurantId)->get();

        $meals = DB::table("rest_sub_menus")->
        where("rest_detail_id", $restaurantId)->
        orderBy("priority")->get();

        $kitchens = DB::table("kitchens")->get();
        return view("superadmin.restaurants.menu.edit_new")->with([
            "categories" => $categories,
            "kitchens" => $kitchens,
            "menus" => $menus,
            "meals" => $meals,
            "rest_detail" => $rest_detail
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
