<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\Country;
use App\superadmin\FrontCountryMap;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
class FrontCountryController extends Controller
{
    public function index()
    {
        $data['map'] = FrontCountryMap::where('status','!=',2)->orderBy('priority','asc')->get();
        $data['country'] = Country::where('status','=',1)->OrderBy('name','asc')->lists('name', 'id');
        $data['country']->prepend('Select Country', '');
      
        return view('superadmin/homepagelist/country')->with('data',$data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        for($i=1;$i<=10;$i++)
        {
            $country = FrontCountryMap::where('priority','=',$request['priority'.$i])->first();
            $country->country_id = $request['country'.$i];
            $country->save();
        }

        return redirect(route('root.countryfrontlist.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
