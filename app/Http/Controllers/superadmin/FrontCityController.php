<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\City;
use App\superadmin\FrontCityMap;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
class FrontCityController extends Controller
{
    public function index()
    {
        $data['map'] = FrontCityMap::where('status','!=',2)->orderBy('priority','asc')->get();
        $data['city'] = City::where('status','=',1)->OrderBy('name','asc')->lists('name', 'id');
        $data['city']->prepend('Select City', '');
       
        return view('superadmin/homepagelist/city')->with('data',$data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        for($i=1;$i<=10;$i++)
        {
            $state = FrontCityMap::where('priority','=',$request['priority'.$i])->first();
            $state->city_id = $request['city'.$i];
            $state->save();
        }

        return redirect(route('root.cityfrontlist.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
