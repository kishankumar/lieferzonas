<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\MenuCategory;
use Validator;
use Auth;

class MenuCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = MenuCategory::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('cat_name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('cat_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('cat_name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='priority-desc'){
            $q->orderBy('priority', 'desc');
        }elseif($type=='priority'){
            $q->orderBy('priority', 'asc');
        }else{
            $q->orderBy('cat_name', 'asc');
        }
        $categories = $q->paginate(10);
        
        //$categories = MenuCategory::where('status','!=',2)->orderBy('priority', 'desc')->paginate(10);
        return view('superadmin.MenuCategory.index')->with('categories',$categories)->with('search',$search)->with('type',$type);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = Input::get('name');//for get input field value
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages = array(
            'cat_name.unique' => "category name already exists."
        );
        $userData = array(
            'cat_name'      => $data['name'],
            'priority'     =>  $data['priority'],
            'description'     =>  $data['description'],
        );
        
        $category_id = Input::get('category_id');
        if($category_id){
            $rules = array(
                //'cat_name'      =>  'required|min:3|Regex:/(^[a-zA-Z ]+$)+/|unique:menu_categories,cat_name,'.$category_id,
                'cat_name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'priority'     =>  'required|numeric',
                'description'     =>  'required|min:6',
            );
        }else{
            $rules = array(
                //'cat_name'      =>  'required|min:3|unique:menu_categories|Regex:/(^[a-zA-Z ]+$)+/',
                'cat_name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'priority'     =>  'required|numeric',
                'description'     =>  'required|min:6',
            );
        }
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($category_id){
            $count= MenuCategory:: where('id','!=',$category_id)->where('cat_name', '=',$request->name)->where('status', '!=','2')->count();
        }else{
            $count= MenuCategory:: where('cat_name', '=',$request->name)->where('status', '!=','2')->count();
        }
        if($count){
            $errors = array('name1'=>'Menu name has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($category_id){
            $type=MenuCategory::find($category_id);
        }else{
            $type= new MenuCategory;
            $type->created_at=date("Y-m-d H:i:s");
            $type->created_by=Auth::User('user')->id;
        }
        $type->cat_name=$request->name;
        $type->description=$request->description;
        $type->priority=$request->priority;
        $type->updated_by=Auth::User('user')->id;
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=$request->status;
        $type->save();
        if($category_id){
            Session::flash('message', 'Category updated Successfully!'); 
            $json['success']=1;
            $json['message']='Category updated Successfully.';
        }else{
            Session::flash('message', 'Category added Successfully!'); 
            $json['success']=1;
            $json['message']='Category added Successfully.';
        }
        echo json_encode($json);
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedCategory = $data['checkedCategory'];
        
        $data['categories']=MenuCategory::find($selectedCategory);
        $categories = $data['categories']->toArray();
        if(count($categories)>0){
            $json['success']=1;
            $json['id']=$categories['id'];
            $json['cat_name']=$categories['cat_name'];
            $json['description']=$categories['description'];
            $json['priority']=$categories['priority'];
            $json['status']=$categories['status'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCategories = $data['selectedCategories'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedCategories as $key => $value)
        {
            $user = MenuCategory::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
		/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCategories = $data['selectedCategories'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Category Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Category Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Category Priority</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedCategories as $key => $value)
        {
             $data = DB::table('menu_categories')
			
			->where('id','=',$value)
			->select('*')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['cat_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['priority'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
