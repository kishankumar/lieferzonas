<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\SetBonusPoint;

class BonusPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = DB::table('set_bonus_points')
            ->where('status','!=', '2')
            ->select('*')
            ->paginate(10);
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
                 $data[$i] = (array) $data[$i];
        }
        
        return view('superadmin.promocode.bonus')
        ->with('bonuspoints',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->set_validation('',$request);    
        $auth_id = Auth::User()->id; 
        $bonus=SetBonusPoint::find($request->bonus_id); 
        $bonus->points=$request->points;
        $bonus->status=$request->status;
        $bonus->description=$request->description;
        $bonus->updated_by=$auth_id;
        $bonus->updated_at=date("Y-m-d H:i:s");
        $bonus->save();
		Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.bonus.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedbonus = $data['bonus_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['bonus']=SetBonusPoint::where('id',$selectedbonus)->first();
        $bonus = $data['bonus']->toArray();
        if(count($bonus)>0){
            $json['success']=1;
            $json['id']= $bonus['id'];
            $json['activity_title']=$bonus['activity_title'];
            $json['description']=$bonus['description'];
            $json['points']=$bonus['points'];
            $json['status']=$bonus['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
	
	/*added by Rajlakshmi(01-06-16)*/
		public function download(Request $request)
    {

        if($request->ajax()){
            $data = Input::all();
           
            $selectedBonus = $data['selectedBonus'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Activity Title</th>
        <th style="border:solid 1px #81C02F; padding:10px">Points</th>
		<th style="border:solid 1px #81C02F; padding:10px">Description</th>
		
        <th style="border:solid 1px #81C02F; padding:10px">Status</th></tr><tr></tr></thead>';
        foreach($selectedBonus as $key => $value)
        {
            $data = DB::table('set_bonus_points')
            

            ->where('id','=', $value)
           
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				if((strlen($data['description']) > 50))
				{
					$desc = substr($data['description'],0,50).'...';
				}
				else
				{
					$desc = $data['description'];
				}
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['activity_title'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['points'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
				
		}
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
    
	/*added by Rajlakshmi(01-06-16)*/

    public function set_validation($id=null,$request)
     {
        $message=array(
            "points.required"=>"Bonus points is required",
            "description.required"=>"Description is required",
            );

        $this->validate($request,[
        'points' => 'required',
        'description' => 'required',
        ],$message);
     }
}
