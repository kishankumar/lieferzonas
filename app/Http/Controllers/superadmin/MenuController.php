<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\superadmin\RestDetail;
use App\superadmin\RestMenu;
use App\superadmin\RestCategory;
use App\superadmin\RestKitchenMap;
use App\superadmin\Alergic_content;
use App\superadmin\Spice;
use App\superadmin\MenuPriceMap;
use App\superadmin\RestMenuAllergicMap;
use App\superadmin\RestMenuSpiceMap;

use Session;
use DB;
use Auth;
use Validator;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['restro_detail_id']=$request->restaurant;
        $data['restro_names'] = RestDetail::where('status','!=',2)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();

        $data['getRestroMenus'] = RestMenu::where('status','!=',2)->where('rest_detail_id',$data['restro_detail_id'])->select('id','rest_detail_id','rest_category_id','kitchen_id','name','image','code','status')->paginate(20);
        
        return view('superadmin/restaurants/menu/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['restro_names'] = DB::table('rest_details')->orderBy('f_name', 'asc')->where('status',1)->select('id','f_name','l_name')->get();
        $data['alergic_contents'] = Alergic_content ::where('status',1)->select('id','name')->get();
        $data['spices'] = Spice ::where('status',1)->select('id','name')->get();
        

        return view('superadmin/restaurants/menu/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       /*$values=RestMenu::where('status','!=',2)->where('rest_detail_id',$request->restro_name)->where('name',$request->cat_name)->select('id')->get();
       
        if(count($values)>0){

            Session::flash('category_error', 'This menu is already exist for this restaurant');
            return redirect(route('root.menu.create'));
        }
        else{*/


        $this->set_validation('',$request);
		$getmenucode=RestMenu::where('code',$request->menu_code)->where('status','!=',2)->get();
		$menucodecount = count($getmenucode->toArray()); 
		if($menucodecount>0)
		{
		  Session::flash('message', 'Menü Code existiert bereits!');
		  return redirect(url('root/edit_restaurant_menu/' . $request->input("restro_name")));
		}
		else{
				$restaurant_menus= new RestMenu;
				 //dd($request);
				 ///////////File Upload////////////////////////////

				$destinationpath=public_path()."/uploads/superadmin/menu";
				$pic=Input::file('file');
				if($pic){
				$extension=  $pic->getClientOriginalExtension(); // getting image extension
				$filename=time().rand(111,999).'.'.$extension; // renameing image
				$pic->move($destinationpath,$filename);
				}
				else{
					$filename='';
				}

				if(($request->have_submenu)==''){

					$having_submenu=0;
				}
				else{
					$having_submenu=1;
				}
				if(($request->alergic)==''){

					$is_alergic=0;
				}
				else{
					$is_alergic=1;
				}
				if(($request->spicy)==''){

					$is_spicy=0;
				}
				else{
					$is_spicy=1;
				}

				 ////////////////Add Basic Information////////////////
				 $restaurant_menus->rest_detail_id=$request->restro_name;
				 $restaurant_menus->rest_category_id=$request->cat_name;
				 $restaurant_menus->kitchen_id=$request->kitchen_name;
				 $restaurant_menus->name=$request->menu_name;
				 $restaurant_menus->short_name=$request->short_menu_name;
				 $restaurant_menus->code=$request->menu_code;
				 $restaurant_menus->description=$request->description;
				 $restaurant_menus->image=$filename;
				 $restaurant_menus->info=$request->info;
				 $restaurant_menus->priority=$request->priority;
				 $restaurant_menus->have_submenu=$having_submenu;
				 $restaurant_menus->is_alergic=$is_alergic;
				 $restaurant_menus->is_spicy=$is_spicy;
				 $restaurant_menus->created_by=Auth::User()->id;
				 $restaurant_menus->updated_by=Auth::User()->id;
			   
				 $restaurant_menus->save();

				$menu_id = $restaurant_menus['id'];

				//////////////Add Price Type and Alergic and Spicy Value/////////////////////

				if(($request->have_submenu)==''){

					$totalPriceCount=count($request->price_types);
					$price=$request->price_types;
					$priceTypeValue=$request->price_types_id;

					for ($i=0 ;$i<$totalPriceCount ;$i++){

						$menuPriceMap= new MenuPriceMap;

						$menuPriceMap->rest_detail_id=$request->restro_name;
						$menuPriceMap->menu_id=$menu_id;
						$menuPriceMap->price_type_id=$priceTypeValue[$i];
						$menuPriceMap->price=$price[$i];
						$menuPriceMap->created_by=Auth::User()->id;
						$menuPriceMap->updated_by=Auth::User()->id;

						$menuPriceMap->save();

					}

					///////////////Add alergic to menus///////////////////////////

					$totalalergicCount=count($request->alergic_contents);
					$alergicContents=$request->alergic_contents;

					for ($i=0 ;$i<$totalalergicCount ;$i++){

						$menuAlergicMap= new RestMenuAllergicMap;

						$menuAlergicMap->rest_detail_id=$request->restro_name;
						$menuAlergicMap->menu_id=$menu_id;
						$menuAlergicMap->allergic_id=$alergicContents[$i];
						$menuAlergicMap->created_by=Auth::User()->id;
						$menuAlergicMap->updated_by=Auth::User()->id;

						$menuAlergicMap->save();

					}


					///////////////Add spice to menus///////////////////////////

					$totalspiceCount=count($request->spicy_content);
					$spiceContents=$request->spicy_content;

					for ($i=0 ;$i<$totalspiceCount ;$i++){

						$menuSpiceMap= new RestMenuSpiceMap;

						$menuSpiceMap->rest_detail_id=$request->restro_name;
						$menuSpiceMap->menu_id=$menu_id;
						$menuSpiceMap->spice_id=$spiceContents[$i];
						$menuSpiceMap->created_by=Auth::User()->id;
						$menuSpiceMap->updated_by=Auth::User()->id;

						$menuSpiceMap->save();

					}

				}
				Session::flash('message', 'Added Successfully!');
				 return redirect(url('root/edit_restaurant_menu/' . $request->input("restro_name")));
		}
         
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id){
            $count= DB::table('rest_menus')->where('id','=',$id)->where('status','!=','2')->count();
            if(!$count){
                Session::flash('message', 'Please select menu again.'); 
                return redirect(route('root.menu.index'));
            }
            if( ! preg_match('/^\d+$/', $id) ){
                Session::flash('message', 'Please select menu again.'); 
                return redirect(route('root.menu.index'));
            }
        }
        $data['viewDetails'] = RestMenu :: where('id',$id)->get();
        return view('superadmin.restaurants.menu.viewpage')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['restro_names'] = DB::table('rest_details')->orderBy('f_name', 'asc')->where('status',1)->select('id','f_name','l_name')->get();
        $data['menus']=RestMenu::find($id);
        $restro_id=$data['menus']['rest_detail_id'];
        $data['categories'] = RestCategory:: where('rest_detail_id',$restro_id)->where('status',1)->select('id','name')->get();
        $data['kitchens'] = RestKitchenMap:: where('rest_detail_id',$restro_id)->where('status',1)->select('id','kitchen_id')->get();


        $data['alergic_contents'] = Alergic_content ::where('status',1)->select('id','name')->get();
        $data['spices'] = Spice ::where('status',1)->select('id','name')->get();

        $data['allergicMaps']=RestMenuAllergicMap :: where('menu_id',$id)->lists('allergic_id')->toArray();

        $data['spiceMaps']=RestMenuSpiceMap :: where('menu_id',$id)->lists('spice_id')->toArray();

        $data['priceTypesWithValue']=DB::table('rest_price_maps')
            ->join('price_types', 'rest_price_maps.price_type_id', '=', 'price_types.id')
            ->leftjoin('menu_price_maps', 'rest_price_maps.price_type_id', '=', 'menu_price_maps.price_type_id')
            ->select('price_types.id','price_types.name','menu_price_maps.price')
            ->where('rest_price_maps.rest_detail_id',$restro_id)
            ->where('menu_price_maps.menu_id',$id)
            ->get();

        $data['priceTypes']=DB::table('rest_price_maps')
            ->join('price_types', 'rest_price_maps.price_type_id', '=', 'price_types.id')
            ->select('price_types.id','price_types.name')
            ->where('rest_price_maps.rest_detail_id',$restro_id)
            ->get();

         return view('superadmin/restaurants/menu/edit')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->set_validation('',$request);
		$getmenucode=RestMenu::where('code',$request->menu_code)->where('id','!=',$id)->where('status','!=',2)->get();
		$menucodecount = count($getmenucode->toArray()); 
		if($menucodecount>0)
		{
		  Session::flash('message', 'This Menu code is already exist!');
		  return redirect(url('root/menu/'.$id.'/edit'));
		}
		else{
				$restaurant_menus= RestMenu:: find($id);
				 
				 ///////////File Upload////////////////////////////

				$destinationpath=public_path()."/uploads/superadmin/menu";
				$pic=Input::file('file');
				if($pic){
				$extension=  $pic->getClientOriginalExtension(); // getting image extension
				$filename=time().rand(111,999).'.'.$extension; // renameing image
				$pic->move($destinationpath,$filename);
				$restaurant_menus->image=$filename;
				}

				if(($request->have_submenu)==''){

					$having_submenu=0;
				}
				else{
					$having_submenu=1;
				}
				if(($request->alergic)==''){

					$is_alergic=0;
				}
				else{
					$is_alergic=1;
				}
				if(($request->spicy)==''){

					$is_spicy=0;
				}
				else{
					$is_spicy=1;
				}
				

				 ////////////////Update Information////////////////
				 $restaurant_menus->rest_detail_id=$request->restro_name;
				 $restaurant_menus->rest_category_id=$request->cat_name;
				 $restaurant_menus->kitchen_id=$request->kitchen_name;
				 $restaurant_menus->name=$request->menu_name;
				 $restaurant_menus->short_name=$request->short_menu_name;
				 $restaurant_menus->code=$request->menu_code;
				 $restaurant_menus->description=$request->description;
				 
				 $restaurant_menus->info=$request->info;
				 $restaurant_menus->priority=$request->priority;
				 $restaurant_menus->have_submenu=$having_submenu;
				 $restaurant_menus->is_alergic=$is_alergic;
				 $restaurant_menus->is_spicy=$is_spicy;
				 $restaurant_menus->status=$request->status;;
				 $restaurant_menus->created_by=Auth::User()->id;
				 $restaurant_menus->updated_by=Auth::User()->id;
			   
				 $restaurant_menus->save();

				//////////////Add Price Type and Alergic and Spicy Value/////////////////////

				if(($request->have_submenu)==''){


					$totalPriceCount=count($request->price_types);
					$price=$request->price_types;
					$priceTypeValue=$request->price_types_id;

					$deleted= MenuPriceMap :: where('menu_id',$id)->delete();

					for ($i=0 ;$i<$totalPriceCount ;$i++){

						$menuPriceMap= new MenuPriceMap;

						$menuPriceMap->rest_detail_id=$request->restro_name;
						$menuPriceMap->menu_id=$id;
						$menuPriceMap->price_type_id=$priceTypeValue[$i];
						$menuPriceMap->price=$price[$i];
						$menuPriceMap->created_by=Auth::User()->id;
						$menuPriceMap->updated_by=Auth::User()->id;

						$menuPriceMap->save();

					}

					///////////////Add alergic to menus///////////////////////////

					$totalalergicCount=count($request->alergic_contents);
					$alergicContents=$request->alergic_contents;

					$deleted= RestMenuAllergicMap :: where('menu_id',$id)->delete();

					for ($i=0 ;$i<$totalalergicCount ;$i++){

						$menuAlergicMap= new RestMenuAllergicMap;

						$menuAlergicMap->rest_detail_id=$request->restro_name;
						$menuAlergicMap->menu_id=$id;
						$menuAlergicMap->allergic_id=$alergicContents[$i];
						$menuAlergicMap->created_by=Auth::User()->id;
						$menuAlergicMap->updated_by=Auth::User()->id;

						$menuAlergicMap->save();

					}


					///////////////Add spice to menus///////////////////////////

					$totalspiceCount=count($request->spicy_content);
					$spiceContents=$request->spicy_content;

					$deleted= RestMenuSpiceMap :: where('menu_id',$id)->delete();

					for ($i=0 ;$i<$totalspiceCount ;$i++){

						$menuSpiceMap= new RestMenuSpiceMap;

						$menuSpiceMap->rest_detail_id=$request->restro_name;
						$menuSpiceMap->menu_id=$id;
						$menuSpiceMap->spice_id=$spiceContents[$i];
						$menuSpiceMap->created_by=Auth::User()->id;
						$menuSpiceMap->updated_by=Auth::User()->id;

						$menuSpiceMap->save();

					}

				}
				else{
					$deleted1= MenuPriceMap :: where('menu_id',$id)->delete();

					$deleted2= RestMenuAllergicMap :: where('menu_id',$id)->delete();

					$deleted3= RestMenuSpiceMap :: where('menu_id',$id)->delete();

				}


				 Session::flash('message', 'Updated Successfully!');
				 return redirect(route('root.menu.index'));
		}
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RestMenu::find($id)->delete();
        return response()->json([
            "status" => "success"
        ]);
    }

    /*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
         if($request->ajax()){
            $data = Input::all();
            $selectedMenus = $data['selectedMenus'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Menu Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Restaurant</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Category</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Kitchen</th>
						<th style="border:solid 1px #81C02F; padding:10px">Menu Code</th>
						<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedMenus as $key => $value)
        {
            
             $data = DB::table('rest_menus')
			 ->leftjoin('rest_details','rest_menus.rest_detail_id','=','rest_details.id')
			 ->leftjoin('rest_categories' ,'rest_menus.rest_category_id','=','rest_categories.id')
			  ->leftjoin('kitchens' ,'rest_menus.kitchen_id','=','kitchens.id')
			 ->where('rest_menus.id','=',$value)
			->select('rest_menus.*','rest_details.f_name','rest_details.l_name',
			'rest_categories.name as catname','kitchens.name as kitchenname')->get();
			for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		    } 
			
            
			  
		      
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['f_name'].' '.$data['0']['l_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['catname'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['kitchenname'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['code'].'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
 
    /////Add validation////////
     public function set_validation($id=null,$request)
     {
        $message=array(
            "restro_name.required"=>"Select restaurant name",
            "cat_name.required"=>"Enter valid category",
            "menu_name.required"=>"Enter valid menu name",
            "menu_code.required"=>"Enter valid menu code",
            "priority.required"=>"Enter priority",
            "priority.numeric"=>"Priority should be numeric",
  
            );

        $this->validate($request,[
            'restro_name'=> 'required',
            'cat_name'=> 'required',
            'menu_name'=> 'required',
            'menu_code'=> 'required',
            'priority'=> 'required|numeric',
            
        ],$message);
     }
}
