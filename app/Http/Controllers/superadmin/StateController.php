<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\Country;
use App\superadmin\State;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Session;
use Auth;
use DB;
class StateController extends Controller
{
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = State::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('created_at', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $data['state'] = $q->paginate(10);
        
        //$data['state'] = State::where('status','!=',2)->get();
        $data['country'] = Country::where('status','=',1)->OrderBy('name','asc')->lists('name', 'id');
        $data['country']->prepend('Select Country', '');

        
        return view('superadmin/state/index')->with('data',$data)->with('search',$search)->with('type',$type);

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|unique:states|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/',
            //'description'=>'required|min:5',
            'country'=>'required',
            'myslug'=>'required'
        ]);

        $state = new State;
        $state->name = $request->name;
        $state->slug = $request->myslug;
        $state->description = $request->description;
        $state->country_id = $request->country;
        $state->status = $request->status;
        //$state->created_by = session user id;
        $state->save();
        Session::flash('message', 'Added Successfully!');
        return redirect(route('root.state.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['state'] = State::where('id',$id)->first();
        $data['country'] = Country::where('status','=',1)->OrderBy('name','asc')->lists('name', 'id');

        return view('superadmin/state/edit')->with('data',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/|unique:states,name,'.$id.",id",
            //'description' => 'required|min:10'
        ]);
        $state = State::find($id);
        $state->name = $request->name;
        $state->description = $request->description;
        $state->country_id = $request->country;
        $state->status = $request->status;
        $state->save();
        Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.state.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selecteStates = $data['selecteStates'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selecteStates as $key => $value)
        {
            $state = State::find($value);
            $state->status = '2';
            $state->save();
        }
        //Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }
     /*added by Rajlakshmi(31-05-16)*/
     public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selecteStates = $data['selecteStates'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        
    $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
        <thead style="background:#81C02F; color:#fff;">
        <tr>            <th style="border:solid 1px #81C02F; padding:10px">State name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Country name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created at</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th>
                        </tr><tr></tr></thead>';
        foreach($selecteStates as $key => $value)
        {
             $data = DB::table('states')
             ->leftjoin('countries', 'countries.id', '=', 'states.country_id')
            ->leftjoin('users', 'users.id', '=', 'countries.created_by')
            ->where('states.id','=',$value)
            ->select('states.*','users.email','countries.name as cname')
            ->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
           } 
            
               //print_r($data); die; 
                
                if($data['0']['status']=='1')
                {
                    $status='Active';
                }
                else
                {
                    $status='Deactive';
                }
                if((strlen($data['0']['description']) > 50))
                {
                    $desc = substr($data['0']['description'],0,50).'...';
                }
                else
                {
                    $desc = $data['0']['description'];
                }                   
                $cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
                
                $data1.= '<tr>
                <td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
                <td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['cname'].'</td>
                <td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
                <td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
                
                <td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
        
        $data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }

 /*added by Rajlakshmi(31-05-16)*/
    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $state = State::find($id);
            $state->status = $status;
            $state->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }

    public function generateSlug(Request $request)
    {
        $rawslug = $request->rawslug;
        $slug = Country::process_url($rawslug);
        echo '<div class="form-group"><label for="" class="col-md-3 col-sm-3 col-xs-12 control-label"> Slug : </label><div class="col-md-6 col-sm-6 col-xs-12"> <label class="col-md-3 col-sm-3 col-xs-12 form-control"><span id="myslug">'.$slug.'</span>&nbsp;&nbsp;</label> <button class="btn btn-default btn-xs" type="button" onclick="editSlug()"> Edit </button></div><input type="hidden" name="myslug" value="'.$slug.'" /></div>';
        exit();
    }
}
