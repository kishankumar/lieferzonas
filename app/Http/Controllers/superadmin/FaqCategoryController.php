<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\FaqCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Session;
use Auth;
use DB;

class FaqCategoryController extends Controller
{
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = FaqCategory::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('created_at', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $cat = $q->paginate(10);
        //$cat = FaqCategory::where('status','!=',2)->get();
        return view('superadmin/faq/catindex')->with('cats',$cat)->with('search',$search)->with('type',$type); 

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|unique:faq_categories|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/',
            'description'=>'required|min:10'
        ]);

        $cat = new FaqCategory;
        $cat->name = $request->name;
        $cat->description = $request->description;
        $cat->status = $request->status;
        //$cat->created_by = session user id;
        $cat->save();
        Session::flash('message', 'Added Successfully!');
        return redirect(route('root.faqcat.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $cat = FaqCategory::where('id',$id)->first();
        //print_r($cat);exit();
		
		
        return view('superadmin/faq/catedit')->with('cats',$cat);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/|unique:faq_categories,name,'.$id.",id",
            'description' => 'required|min:10'
        ]);
        $cat = FaqCategory::find($id);
        $cat->name = $request->name;
        $cat->description = $request->description;
        $cat->status = $request->status;
        $cat->save();
        Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.faqcat.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selected = $data['selectedCountries'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selected as $key => $value)
        {
            $cat = FaqCategory::find($value);
            $cat->status = '2';
            $cat->save();
        }
        Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }

    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $cat = FaqCategory::find($id);
            $cat->status = $status;
            $cat->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
         if($request->ajax()){
            $data = Input::all();
            $selected = $data['selectedCountries'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Category Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created By</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Created At</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selected as $key => $value)
        {
             $data = DB::table('faq_categories')
			->leftjoin('users', 'users.id', '=', 'faq_categories.created_by')
			->where('faq_categories.id','=',$value)
			->select('faq_categories.*','users.email')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['email'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
