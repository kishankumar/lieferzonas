<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\ShopMetricCategory;
use App\superadmin\ShopCategory;
use Validator;use Auth;

class ShopMetricCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = ShopCategory::where('status','=',1);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $data['cat_names']=$q->paginate(10);
        $data['pages']=$data['cat_names']->toarray($data['cat_names']);
        $data['type']=$type;
        $data['search']=$search;

        return view('superadmin/ShopManagement/shopmetcategories/index')->with($data);

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->category){
            $count= ShopCategory:: where('id','=',$request->category)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select category again.'); 
                return redirect(route('root.shop.metricscatmap.create'));
            }
            if( ! preg_match('/^\d+$/', $request->category) ){
                Session::flash('message', 'Please select category again.'); 
                return redirect(route('root.shop.metricscatmap.create'));
            }
        }
        $data['category_names'] = DB::table('shop_categories')->where('status',1)->orderBy('name', 'asc')->select('id','name')->get();
        $data['metrics']=DB:: table('metrics')->where('status',1)->get();
        $data['category_id']=$request->category;
        $data['root_cat_maps']=DB:: table('shop_metric_categories')->where('shop_category_id',$request->category)->lists('metric_id');
        //echo '<pre>';print_r($data);die;
		
	
        return view('superadmin/ShopManagement/shopmetcategories/create')->with($data);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);die;
        if($request->categoryId){
            $count= ShopCategory:: where('id','=',$request->categoryId)->where('status', '1')->count();
            if(!$count){
                Session::flash('message', 'Please select category again.'); 
                return redirect(route('root.shop.metricscatmap.create'));
            }
            if( ! preg_match('/^\d+$/', $request->categoryId) ){
                Session::flash('message', 'Please select category again.'); 
                return redirect(route('root.shop.metricscatmap.create'));
            }
        }
        $categoryId=$request->categoryId;
        $total_metrics_selected=count($request->metrics);
        $metrics=$request->metrics;
        
        $exist_category = DB::table('shop_metric_categories')->where('shop_category_id',$categoryId)->count();
        $created_by=Auth::User('user')->id;$updated_by=Auth::User('user')->id;
        if($exist_category>0){
            DB::beginTransaction();
            $deleted= DB::table('shop_metric_categories')->where('shop_category_id',$categoryId)->delete();
            for ($i=0; $i <$total_metrics_selected ; $i++) {
                $created_at=date("Y-m-d H:i:s");$updated_at=date("Y-m-d H:i:s");
               DB::table('shop_metric_categories')->insert(
                    ['shop_category_id'=>$categoryId,'metric_id'=>$metrics[$i],'created_by'=>$created_by,'updated_by'=>$updated_by,
                    'created_at'=>$created_at,'updated_at'=>$updated_at,'status'=>'1' ]
               );
            }
            if($deleted and ($i==$total_metrics_selected)){
                DB::commit();
            }
            else{
                DB::rollBack();
            }
        }
        else{
            DB::beginTransaction();
            for ($i=0; $i <$total_metrics_selected ; $i++) { 
                $created_at=date("Y-m-d H:i:s");$updated_at=date("Y-m-d H:i:s");
                DB::table('shop_metric_categories')->insert(
                    ['shop_category_id'=>$categoryId,'metric_id'=>$metrics[$i],'created_by'=>$created_by,'updated_by'=>$updated_by,
                    'created_at'=>$created_at,'updated_at'=>$updated_at,'status'=>'1' ]
                );
            }
            if($i==$total_metrics_selected){
                DB::commit();
            }
            else{
                DB::rollBack();
            }
        }
        Session::flash('message', 'Metrics updated successfully.'); 
        return redirect(route('root.shop.metricscatmap.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getdata(Request $request){
        if($request) {
            $category_id=$request->category_id;
            $rootCategoryMaps= DB::table('shop_metric_categories')
                ->join('metrics', 'shop_metric_categories.metric_id', '=', 'metrics.id')
                ->select('metrics.name')
                ->where('shop_metric_categories.shop_category_id',$category_id)
                ->get();
            $div='';
            foreach ($rootCategoryMaps as $rootCategoryMap ) {
                $div .='<div class="col-md-4">'.$rootCategoryMap->name.'</div>';
            }
            echo $div;
        }
    }
}
