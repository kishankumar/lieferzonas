<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\MarketingTip;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Session;
use Auth;
use DB;
class MarketingTipController extends Controller
{
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='topic';
        }
        
        $q= MarketingTip::where('status','!=','2');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('topic', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='topic'){
            $q->orderBy('topic', 'asc');
        }elseif($type=='topic-desc'){
            $q->orderBy('topic', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='priority-desc'){
            $q->orderBy('priority', 'desc');
        }elseif($type=='priority'){
            $q->orderBy('priority', 'asc');
        }elseif($type=='created-desc'){
            $q->orderBy('created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('created_at', 'asc');
        }else{
            $q->orderBy('topic', 'asc');
        }
        $tip = $q->paginate(10);
        
        return view('superadmin/marketingtips/index')->with('tips',$tip)->with('search',$search)->with('type',$type);

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'topic'=>'required|unique:marketing_tips|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/',
            'description'=>'required|min:20',
            //'priority'=>'required'
        ]);

        $tip = new MarketingTip;
        $tip->topic = $request->topic;
        $tip->description = $request->description;
        //$tip->priority = $request->priority;
        $tip->status = $request->status;
        //$tip->created_by = session user id;
        $tip->save();
        Session::flash('message', 'Added Successfully!');
        return redirect(route('root.marketingtips.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $tip = MarketingTip::where('id',$id)->first();
        //print_r($tip);exit();
		
        return view('superadmin/marketingtips/edit')->with('tips',$tip);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'topic' => 'required|Regex:/^[a-zA-ZäÄöÖüÜß«» -]*$/|unique:marketing_tips,topic,'.$id.",id",
            'description' => 'required|min:10',
            //'priority' => 'required'
        ]);
        $tip = MarketingTip::find($id);
        $tip->topic = $request->topic;
        $tip->description = $request->description;
        //$tip->priority = $request->priority;
        $tip->status = $request->status;
        $tip->save();
        Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.marketingtips.index'));
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedTips = $data['selectedTips'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedTips as $key => $value)
        {
            $tip = MarketingTip::find($value);
            $tip->status = '2';
            $tip->save();
        }
        //Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }

    public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $tip = MarketingTip::find($id);
            $tip->status = $status;
            $tip->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
	 /*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedTips = $data['selectedTips'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
         $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Topic</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
						
                      
						<th style="border:solid 1px #81C02F; padding:10px">Created at</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedTips as $key => $value)
        {
             $data = DB::table('marketing_tips')
			->leftjoin('users', 'users.id', '=', 'marketing_tips.created_by')
			->where('marketing_tips.id','=',$value)
			->select('marketing_tips.*','users.email')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}		
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['topic'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$cdate.'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(31-05-16)*/
}
