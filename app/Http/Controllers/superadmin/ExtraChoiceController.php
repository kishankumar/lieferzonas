<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\ExtraChoice;
use App\superadmin\ExtraChoiceElement;
use App\superadmin\SubMenuExtraMap;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Log;
use Illuminate\Support\Facades\Session;

class ExtraChoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restaurant_id = $request->input("restaurant_id");
        $name = $request->input("name");
        $type = $request->input("type");

        $extra_choice = new ExtraChoice;
        $extra_choice->restaurant_id = $restaurant_id;
        $extra_choice->name = $name;
        $extra_choice->type = $type;
        $extra_choice->save();

        $element_name_prefix = "element-name-";
        $element_price_prefix = "element-price-";

        foreach ($request->all() as $key => $value) {
            if (0 === strpos($key, $element_name_prefix)) {
                $index_str = substr($key, strlen($element_name_prefix));
                $index = (int)$index_str;
                $name = $value;
                $price = $request->input($element_price_prefix . $index_str);

                $extra_choice_element = new ExtraChoiceElement();
                $extra_choice_element->extra_choice_id = $extra_choice->id;
                $extra_choice_element->name = $name;
                $extra_choice_element->price = $price;
                $extra_choice_element->save();
            }
        }

        Session::flash('message', 'Extra erfolgreich hinzugefügt!');
        return redirect(url('root/edit_restaurant_menu/' . $restaurant_id));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addExtraToSubMenu(Request $request) {
        $sub_menu = $request->input("sub_menu");
        $extras = $request->input("extras");

        SubMenuExtraMap::where("sub_menu_id", $sub_menu)->delete();

        foreach ($extras as $extra) {
            $sub_menu_extra_map = new SubMenuExtraMap;
            $sub_menu_extra_map->sub_menu_id = $sub_menu;
            $sub_menu_extra_map->extra_choice_id = $extra;
            $sub_menu_extra_map->save();
        }

        return response()->json([
            "status" => "success"
        ]);
    }
}
