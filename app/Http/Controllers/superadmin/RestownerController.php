<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


use Session;
use DB;
use Auth;
use validator;
use App\superadmin\RestOwner;
use Hash;

class RestownerController extends Controller
{
    


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = DB::table('rest_owners');
        $q->leftjoin('rest_details', 'rest_owners.rest_detail_id', '=', 'rest_details.id');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('rest_details.f_name', 'LIKE', "%$search%");
                $q2->orWhere('rest_details.l_name', 'LIKE', "%$search%");
                $q2->orWhere('rest_owners.firstname', 'LIKE', "%$search%");
                $q2->orWhere('rest_owners.lastname', 'LIKE', "%$search%");
                $q2->orWhere('rest_owners.email', 'LIKE', "%$search%");
                $q2->orWhere('rest_owners.contact_no', 'LIKE', "%$search%");
                //$q2->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%$search%");
            });
        }
        $q->where('rest_owners.status','!=', 2);
        
        if($type=='name'){
            $q->orderBy('rest_details.f_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('rest_details.f_name', 'desc');
        }elseif($type=='oname-desc'){
            $q->orderBy('rest_owners.firstname', 'desc');
        }elseif($type=='oname'){
            $q->orderBy('rest_owners.firstname', 'asc');
        }elseif($type=='email-desc'){
            $q->orderBy('rest_owners.email', 'desc');
        }elseif($type=='email'){
            $q->orderBy('rest_owners.email', 'asc');
        }elseif($type=='contact-desc'){
            $q->orderBy('rest_owners.contact_no', 'desc');
        }elseif($type=='contact'){
            $q->orderBy('rest_owners.contact_no', 'asc');
        }elseif($type=='from-desc'){
            $q->orderBy('rest_owners.valid_from', 'desc');
        }elseif($type=='from'){
            $q->orderBy('rest_owners.valid_from', 'asc');
        }elseif($type=='to-desc'){
            $q->orderBy('rest_owners.valid_to', 'desc');
        }elseif($type=='to'){
            $q->orderBy('rest_owners.valid_to', 'asc');
        }else{
            $q->orderBy('rest_details.f_name', 'asc');
        }
        
        $q->select('rest_owners.id','rest_owners.firstname','rest_owners.lastname','rest_owners.contact_no','rest_owners.email','rest_owners.valid_from','rest_owners.status','rest_owners.valid_to','rest_details.f_name','rest_details.l_name');
        $data=$q->paginate(10);
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data[$i] = (array) $data[$i];
        }
        return view('superadmin.restaurants.restaurantowner.restowner', ['ownerinfo' => $data])->with('search',$search)->with('type',$type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {



           $restselected = DB::table('rest_owners')->select('rest_detail_id')->where('status','=','1')->get();
        
             
            for ($i = 0, $c = count($restselected); $i < $c; ++$i) {
               $restselected[$i] = (array) $restselected[$i];
                
            }
           $restlist = DB::table('rest_details')->orderBy('f_name', 'asc')->select('id', 'f_name','l_name')->whereNotIn('id',$restselected)->where('status','=','1')->get();
        
             
            for ($i = 0, $c = count($restlist); $i < $c; ++$i) {
               $restlist[$i] = (array) $restlist[$i];
                
            }
            

            $time_val = DB::table('time_validations')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($time_val); $i < $c; ++$i) {
            $time_val[$i] = (array) $time_val[$i];
            
        }

        return view('superadmin/restaurants/restaurantowner/add_restowner')->with('restlists',$restlist)->with('time_validations',$time_val); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->set_validation('',$request); 
        $auth_id = Auth::User()->id;
		$restowner_list=RestOwner::where('email',$request->email)->where('status','!=',2)->get();
        $restownercount = count($restowner_list->toArray()); 
		if($restownercount>0)
		{
		   Session::flash('message', 'Email id has already been registered!');
		  return redirect(route('root.restowner.create'));
		}
		else{
			
	    $restowner = new RestOwner;
        $restowner->validity_type=$request->validity;
        if($request->validity=='T')
        {
           $validity_table =$request->validity_table;
           
           $valid_from = date("Y-m-d H:i:s");
           $time_validation = DB::table('time_validations')->select('year','month','week' ,'day')->where('status','=','1')->where('id','=',$validity_table)->get();
        
             
           for ($i = 0, $c = count($time_validation); $i < $c; ++$i) {
            $time_validation[$i] = (array) $time_validation[$i];
            
        } 
           $year = $time_validation['0']['year'];
           $month = $time_validation['0']['month'];
           $week = $time_validation['0']['week'];
           $day = $time_validation['0']['day'];
           $totaltime = ($year*365) + ($month*30) + ($week*7) + ($day);
           $valid_to = date('Y-m-d', strtotime("+".$totaltime."days"));
           $restowner->validity_table=$validity_table;
           $restowner->valid_from=$valid_from;
           $restowner->valid_to=$valid_to;
        }
         
      else
        {
             $restowner->valid_from=$request->valid_from;
             $restowner->valid_to=$request->valid_to;
        }

        $restowner->rest_detail_id=$request->restuarant;
        $restowner->firstname=$request->firstname;
        $restowner->lastname=$request->lastname;
        $restowner->email=$request->email;
        $restowner->contact_no=$request->contact;
        $restowner->password=bcrypt($request->password);
        $restowner->status=$request->status;
        $restowner->created_by=$auth_id;
        $restowner->created_at=date("Y-m-d H:i:s");
        $restowner->save();
		Session::flash('message', 'Restaurant Owner Added Successfully!');
        return redirect(route('root.restowner.index'));
		}
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $rest = DB::table('rest_details')->select('id', 'f_name','l_name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($rest); $i < $c; ++$i) {
            $rest[$i] = (array) $rest[$i];
            
        } 
        $time_val = DB::table('time_validations')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($time_val); $i < $c; ++$i) {
            $time_val[$i] = (array) $time_val[$i];
            
        } 

        $restowner = RestOwner::where('id',$id)->get();
        $restowner =$restowner->toArray();
        $resturantname = DB::table('rest_details')->select('f_name','l_name')->where('id','=',$restowner['0']['rest_detail_id'])->get();
         for ($i = 0, $c = count($resturantname); $i < $c; ++$i) {
            $resturantname[$i] = (array) $resturantname[$i];
            
        } 

        return view('superadmin/restaurants/restaurantowner/edit_restowner')->with('restowner',$restowner)->with('resturants',$rest) 
        ->with('time_validations',$time_val)->with('resturantname',$resturantname);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->set_validation1($id,$request);
        $auth_id = Auth::User()->id; 
		$restowner_list=RestOwner::where('id','!=',$id)->where('email',$request->email)->where('status','!=',2)->get();
        $restownercount = count($restowner_list->toArray()); 
		if($restownercount>0)
		{
		  Session::flash('message', 'Email id has already been registered!');
		  return redirect(route('root.restowner.create'));
		}
		else{
        $restowner=RestOwner::find($id);
        $restowner->validity_type=$request->validity;
        if($request->validity=='T')
        {
           $validity_table =$request->validity_table;
           
           $valid_from = date("Y-m-d H:i:s");
           $time_validation = DB::table('time_validations')->select('year','month','week' ,'day')->where('status','=','1')->where('id','=',$validity_table)->get();
        
             
           for ($i = 0, $c = count($time_validation); $i < $c; ++$i) {
            $time_validation[$i] = (array) $time_validation[$i];
            
        } 
           $year = $time_validation['0']['year'];
           $month = $time_validation['0']['month'];
           $week = $time_validation['0']['week'];
           $day = $time_validation['0']['day'];
           $totaltime = ($year*365) + ($month*30) + ($week*7) + ($day);
           $valid_to = date('Y-m-d', strtotime("+".$totaltime."days"));
           $restowner->validity_table=$validity_table;
           $restowner->valid_from=$valid_from;
           $restowner->valid_to=$valid_to;
        }
         
      else
        {
             $restowner->valid_from=$request->valid_from;
             $restowner->valid_to=$request->valid_to;
             $restowner->validity_table='0';
        }

       
        $restowner->firstname=$request->firstname;
        $restowner->lastname=$request->lastname;
        $restowner->email=$request->email;
        $restowner->contact_no=$request->contact;
       
        $restowner->status=$request->status;
        $restowner->updated_by=$auth_id;
        $restowner->updated_at=date("Y-m-d H:i:s");
        $restowner->save();
		Session::flash('message', 'Restaurant Owner Updated Successfully!');
        return redirect(route('root.restowner.index'));
	 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedOwners = $data['selectedOwners'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedOwners as $key => $value)
        {
            $restowner = RestOwner::find($value);
            $restowner->status = '2';
            $restowner->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }


    public function set_validation($id=null,$request)
     {
        $message=array(
            "restuarant.required"=>"Restuarant is required",
            "firstname.required"=>"First Name is required",
            "lastname.required"=>"Last Name is required",
            "email.required"=>"Email Id is required",
            "contact.required"=>"Contact no is required",
            "password.required"=>"Password is required",
            );

        $this->validate($request,[
        'restuarant' => 'required',
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required|Regex:/^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i',
        'contact' => 'required|numeric',
        'password' => 'required|min:5',
        ],$message);
     }

     public function set_validation1($id=null,$request)
     {
        $message=array(
            
            "firstname.required"=>"First Name is required",
            "lastname.required"=>"Last Name is required",
            "email.required"=>"Email Id is required",
            "contact.required"=>"Contact no is required",
           
            );

        $this->validate($request,[
        
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required|Regex:/^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i',
        'contact' => 'required|numeric',
        
        ],$message);
     }
     
    public function change_pass(Request $request)
    {
        $restowner_list = RestOwner::where('id','=',$request->restowner_id)->first();
        if(Hash::check($request->confirm_password, $restowner_list->password)) {
            Session::flash('message1',  'Please use different password from old password !');
            Session::flash('wrongPwd','yes');
            Session::flash('RestOwnerId',$request->restowner_id);
            return redirect(route('root.restowner.index'));
        }
        else{
            $restowner=RestOwner::find($request->restowner_id);
            $restowner->password=bcrypt($request->confirm_password);
            $restowner->save();
            Session::flash('message',  'Successfully changed password!');
            return redirect(route('root.restowner.index'));
        }
    }
	
	/*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {
		//echo 'rrrrr';exit();
        if($request->ajax()){
            $data = Input::all();
            $selectedOwners = $data['selectedOwners'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		$data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Restaurant Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Owner Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Owner Email Id</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Owner Contact No</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Valid From</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Valid to</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedOwners as $key => $value)
        {
             $data = DB::table('rest_owners')
            ->leftjoin('rest_details', 'rest_owners.rest_detail_id', '=', 'rest_details.id')
            ->where('rest_owners.id','=', $value)
            ->select('rest_owners.firstname','rest_owners.lastname','rest_owners.contact_no','rest_owners.email','rest_owners.valid_from','rest_owners.status','rest_owners.valid_to','rest_details.f_name','rest_details.l_name')
            ->get();
            for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
             
             }
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				$valid_from = date("d M Y", strtotime($data['0']['valid_from']));
				$valid_to = date("d M Y", strtotime($data['0']['valid_to']));
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['f_name'].' '.$data['0']['l_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.ucfirst($data['0']['firstname']).' '.$data['0']['lastname'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['email'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['contact_no'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_from.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_to.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
}
