<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use Validator;
use Auth;
use App\superadmin\RestDetail;
use App\superadmin\RestMealValidDay;
use App\superadmin\RestMealItemMap;
use App\superadmin\RestDailyMeal;
use App\front\OrderItem;
use App\front\UserOrder;
use App\superadmin\Rest_day;
use App\superadmin\RestMenu;
use App\superadmin\RestSubMenu;

class RestDailyMealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = RestDailyMeal::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('meal_name', 'LIKE', "%$search%");
                $q2->orWhere('meal_desc', 'LIKE', "%$search%");
                $q2->orWhere('meal_price', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('meal_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('meal_name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('meal_desc', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('meal_desc', 'asc');
        }elseif($type=='price-desc'){
            $q->orderBy('meal_price', 'desc');
        }elseif($type=='price'){
            $q->orderBy('meal_price', 'asc');
        }elseif($type=='from-desc'){
            $q->orderBy('valid_from', 'desc');
        }elseif($type=='from'){
            $q->orderBy('valid_from', 'asc');
        }elseif($type=='to-desc'){
            $q->orderBy('valid_to', 'desc');
        }elseif($type=='to'){
            $q->orderBy('valid_to', 'asc');
        }else{
            $q->orderBy('meal_name', 'asc');
        }
        $mealInfo = $q->paginate(10);
        
        return view('superadmin.dailyMeal.index')->with('mealInfo',$mealInfo)->with('search',$search)->with('type',$type); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restInfo = DB::table('rest_details')->where('status',1)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();
        $fromtime = array();$totime = array();$toTime=1;
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            $fromtime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            $fromtime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            if($toTime>1){
                $totime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            }$toTime=2;
            $totime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        $totime['11:59 PM'] = '11:59 PM';
        $days = Rest_day:: where('status', '!=','2')->orderBy('id', 'asc')->get();
        $day_maps = array();
        
        return view('superadmin.dailyMeal.add_meal',['restInfo'=>$restInfo,'fromtime' => $fromtime,'totime' => $totime,
                    'days'=>$days,'day_maps'=>$day_maps]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message=array(
            "restaurant.required"=>"Please select restaurant",
            "category.required"=>"Please select category ",
            "meal_name.required"=>"Please enter meal name ",
            "meal_exists"=>"Meal Name already exists.",
            "description.required"=>"Please enter description",
            "meal_price.required"=>"Please enter meal price ",
            "meal_price.greater_than_zero_meal_price"=>"Meal price must be greater than 0",
            'end_after' => "Valid from date should be equal or before valid to date.",
            'check_date' => "Date should be equal or greater than current date.",
            'check_valid_to_date' => "Valid to Date should be equal or greater than current date.",
        );
        $this->validate($request,[
            'restaurant'=> 'required|numeric',
            'meal_name'=> 'required|min:3|meal_exists',
            'description'=> 'required|min:6',
            'meal_price'=> 'required|numeric|greater_than_zero_meal_price:meal_price',
            'valid_from'  =>  'required|date|date_format:m/d/Y|check_date:valid_from',
            'valid_to'  =>  'required|date|date_format:m/d/Y|end_after:valid_from',
        ],$message);
        
        $object = new RestDailyMeal;
        $object->rest_id=$request->restaurant;
        $object->meal_name=$request->meal_name;
        $object->meal_desc=$request->description;
        $object->meal_price=round($request->meal_price,2);
        $object->valid_from = date("Y-m-d", strtotime($request->valid_from));
        $object->valid_to = date("Y-m-d", strtotime($request->valid_to));
        $object->active_time_from=$request->active_time_from;
        $object->active_time_to=$request->active_time_to;
        $object->created_by=Auth::User('user')->id;
        $object->updated_by=Auth::User('user')->id;
        $object->created_at=date("Y-m-d H:i:s");
        $object->updated_at=date("Y-m-d H:i:s");
        $object->status=1;
        $object->save();
        
        $mealId = $object->id;
        
        $selecteddays = $request->selecteddays;
        if(count($selecteddays)){
            foreach($selecteddays as $key => $value)
            {
                $object1 = new RestMealValidDay;
                $object1->meal_id = $mealId;
                $object1->day_id = $value;
                $object1->created_by = Auth::User('user')->id;
                $object1->updated_by = Auth::User('user')->id;
                $object1->created_at = date("Y-m-d H:i:s");
                $object1->updated_at = date("Y-m-d H:i:s");
                $object1->status = 1;
                $object1->save();
            }
        }
        
        if($request->quantitySub){
            $quantitySub = $request->quantitySub;
            if(count($quantitySub)){
                foreach($quantitySub as $submenuId => $quanty)
                {
                    $submenuItem = RestSubMenu::where('id', $submenuId)->first();
                    $submenu_item_name = $submenuItem->name;
                    
                    $object2 = new RestMealItemMap;
                    $object2->rest_id=$request->restaurant;
                    $object2->meal_id=$mealId;
                    $object2->item_id=$submenuId;
                    $object2->item_name= $submenu_item_name;
                    $object2->item_type= 'submenu';
                    $object2->item_quantity=$quanty;
                    //$object2->item_price=$request->item_price;
                    $object2->created_by=Auth::User('user')->id;
                    $object2->updated_by=Auth::User('user')->id;
                    $object2->created_at=date("Y-m-d H:i:s");
                    $object2->updated_at=date("Y-m-d H:i:s");
                    $object2->status=1;
                    $object2->save();
                }
            }
        }
        
        if($request->quantity){
            $quantity = $request->quantity;
            if(count($quantity)){
                foreach($quantity as $menuId => $quanty)
                {
                    $menuItem = RestMenu::where('id', $menuId)->first();
                    $menu_item_name = $menuItem->name;
                    $object2 = new RestMealItemMap;
                    $object2->rest_id=$request->restaurant;
                    $object2->meal_id=$mealId;
                    $object2->item_id=$menuId;
                    $object2->item_name= $menu_item_name;
                    $object2->item_type='menu';
                    $object2->item_quantity=$quanty;
                    //$object2->item_price=$request->item_price;
                    $object2->created_by=Auth::User('user')->id;
                    $object2->updated_by=Auth::User('user')->id;
                    $object2->created_at=date("Y-m-d H:i:s");
                    $object2->updated_at=date("Y-m-d H:i:s");
                    $object2->status=1;
                    $object2->save();
                }
            }
        }
        
        Session::flash('message', 'Meal added Successfully!'); 
        return redirect(route('root.daily.meal.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id){
            $count= DB::table('rest_daily_meals')->where('id','=',$id)->where('status','!=',2)->count();
            if(!$count){
                Session::flash('message', 'Please select meal again.'); 
                return redirect(route('root.daily.meal.index'));
            }
            if( ! preg_match('/^\d+$/', $id) ){
                Session::flash('message', 'Please select meal again.'); 
                return redirect(route('root.daily.meal.index'));
            }
        }
        $restInfo = DB::table('rest_details')->where('status',1)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();
        $fromtime = array();$totime = array();$toTime=1;
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            $fromtime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            $fromtime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            if($toTime>1){
                $totime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            }$toTime=2;
            $totime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        $totime['11:59 PM'] = '11:59 PM';
        $days = Rest_day:: where('status', '!=','2')->orderBy('id', 'asc')->get();
        $day_maps = array();
        $mealInfo = RestDailyMeal::find($id);
        $day_maps=DB:: table('rest_meal_valid_days')->where('meal_id',$id)->lists('day_id');
        
        $itemMaps = RestMealItemMap:: where('meal_id',$id)->get();
        
        //echo '<pre>';print_R($mealInfo);die;
        return view('superadmin.dailyMeal.edit_meal',['mealInfo'=>$mealInfo,'restInfo'=>$restInfo,'fromtime' => $fromtime,'totime' => $totime,
            'days'=>$days,'day_maps'=>$day_maps,'itemMaps'=>$itemMaps]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message=array(
            "restaurant.required"=>"Please select restaurant",
            "category.required"=>"Please select category ",
            "meal_name.required"=>"Please enter meal name ",
            "meal_exists"=>"Meal Name already exists.",
            "description.required"=>"Please enter description",
            "meal_price.required"=>"Please enter meal price ",
            "meal_price.greater_than_zero_meal_price"=>"Meal price must be greater than 0",
            'end_after' => "Valid from date should be equal or before valid to date.",
            'check_date' => "Date should be equal or greater than current date.",
            'check_valid_to_date' => "Valid to Date should be equal or greater than current date.",
        );
        $this->validate($request,[
            'restaurant'=> 'required|numeric',
            'meal_name'=> 'required|min:3|meal_exists',
            'description'=> 'required|min:6',
            'meal_price'=> 'required|numeric|greater_than_zero_meal_price:meal_price',
            'valid_to'  =>  'required|date|date_format:m/d/Y|check_valid_to_date:valid_to',
        ],$message);
        
        $object = RestDailyMeal::find($id);
        //$object->rest_id=$request->restaurant;
        $object->meal_name=$request->meal_name;
        $object->meal_desc=$request->description;
        $object->meal_price=round($request->meal_price,2);
        $object->valid_to = date("Y-m-d", strtotime($request->valid_to));
        $object->active_time_from=$request->active_time_from;
        $object->active_time_to=$request->active_time_to;
        $object->created_by=Auth::User('user')->id;
        $object->updated_by=Auth::User('user')->id;
        $object->created_at=date("Y-m-d H:i:s");
        $object->updated_at=date("Y-m-d H:i:s");
        $object->status=$request->status;
        $object->save();
        
        $mealId = $id;
        $selecteddays = $request->selecteddays;
        if(count($selecteddays)){
            foreach($selecteddays as $key => $value)
            {
                $count= RestMealValidDay:: where('meal_id','=',$mealId)->where('day_id', '=',$value)->count();
                if($count){
                    $type=RestMealValidDay:: where('meal_id','=',$mealId)->where('day_id', '=',$value)->first();
                }else{
                    $type= new RestMealValidDay;
                    $type->created_at=date("Y-m-d H:i:s");
                    $type->created_by=Auth::User('user')->id;
                }
                $type->meal_id = $mealId;
                $type->day_id = $value;
                $type->updated_by = Auth::User('user')->id;
                $type->updated_at = date("Y-m-d H:i:s");
                $type->status = 1;
                $type->save();
            }
            $deleted= DB::table('rest_meal_valid_days')->where('meal_id',$mealId)->whereNotIn('day_id',$selecteddays)->delete();
        }
        
        $subMenuItemsId =array();
        if($request->quantitySub){
            $quantitySub = $request->quantitySub;
            if(count($quantitySub)){
                foreach($quantitySub as $submenuId => $quanty)
                {
                    $submenuItem = RestSubMenu::where('id', $submenuId)->first();
                    $submenu_item_name = $submenuItem->name;
                    array_push($subMenuItemsId,$submenuId);
                    
                    $count1= RestMealItemMap:: where('item_type','submenu')->where('meal_id', '=',$mealId)->where('item_id', '=',$submenuId)->count();
                    if($count1){
                        $object2=RestMealItemMap:: where('item_type','submenu')->where('meal_id', '=',$mealId)->where('item_id', '=',$submenuId)->first();
                    }else{
                        $object2= new RestMealItemMap;
                        $object2->created_at=date("Y-m-d H:i:s");
                        $object2->created_by=Auth::User('user')->id;
                    }
                    $object2->rest_id=$request->restaurant;
                    $object2->meal_id=$mealId;
                    $object2->item_id=$submenuId;
                    $object2->item_name= $submenu_item_name;
                    $object2->item_type= 'submenu';
                    $object2->item_quantity=$quanty;
                    //$object2->item_price=$request->item_price;
                    $object2->updated_by=Auth::User('user')->id;
                    $object2->updated_at=date("Y-m-d H:i:s");
                    $object2->status=1;
                    $object2->save();
                }
            }
        }
        $deleted1 = DB::table('rest_meal_item_maps')->where('item_type','submenu')->where('meal_id',$mealId)->whereNotIn('item_id',$subMenuItemsId)->delete();
        
        $MenuItemsId =array();
        if($request->quantity){
            $quantity = $request->quantity;
            if(count($quantity)){
                foreach($quantity as $menuId => $quanty)
                {
                    $menuItem = RestMenu::where('id', $menuId)->first();
                    $menu_item_name = $menuItem->name;
                    array_push($MenuItemsId,$menuId);
                    
                    $count2= RestMealItemMap:: where('item_type','menu')->where('meal_id', '=',$mealId)->where('item_id', '=',$menuId)->count();
                    if($count2){
                        $object2=RestMealItemMap:: where('item_type','menu')->where('meal_id', '=',$mealId)->where('item_id', '=',$menuId)->first();
                    }else{
                        
                        $object2->created_at=date("Y-m-d H:i:s");
                        $object2->created_by=Auth::User('user')->id;
                    }

                    $object2->rest_id=$request->restaurant;
                    $object2->meal_id=$mealId;
                    $object2->item_id=$menuId;
                    $object2->item_name= $menu_item_name;
                    $object2->item_type='menu';
                    $object2->item_quantity=$quanty;
                    //$object2->item_price=$request->item_price;
                    $object2->updated_by=Auth::User('user')->id;
                    $object2->updated_at=date("Y-m-d H:i:s");
                    $object2->status=1;
                    $object2->save();
                }
            }
        }
        $deleted2 = DB::table('rest_meal_item_maps')->where('item_type','menu')->where('meal_id',$mealId)->whereNotIn('item_id',$MenuItemsId)->delete();
        
        Session::flash('message', 'Meal updated Successfully!'); 
        return redirect(route('root.daily.meal.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedMeal = $data['selectedMeals'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedMeal as $key => $value)
        {
            $user = RestDailyMeal::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	 public function download(Request $request)
    {
		
        if($request->ajax()){
            $data = Input::all();
            $selectedMeals = $data['selectedMeals'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		$data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Meal Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Meal Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Meal Price</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Valid From</th>
						<th style="border:solid 1px #81C02F; padding:10px">Valid To</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Restaurant Name</th>
						<th>Status</th></tr><tr></tr></thead>';
        foreach($selectedMeals as $key => $value)
        {
            $data = DB::table('rest_daily_meals')
            ->leftjoin('rest_details', 'rest_details.id', '=', 'rest_daily_meals.rest_id')
            ->select('rest_daily_meals.*','rest_details.f_name')
			->where('rest_daily_meals.id','=',$value)->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['meal_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['meal_desc'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['meal_price'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.date("d M Y", strtotime($data['valid_from'])).'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.date("d M Y", strtotime($data['valid_to'])).'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['f_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
}
