<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\superadmin\RestDetail;
use DB;
use Auth;
use Validator;

class RestExtraTypeMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = RestDetail::where('status','=',1);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('f_name', 'LIKE', "%$search%");
                $q2->orWhere('l_name', 'LIKE', "%$search%");
            });
        }
        
        if($type=='name'){
            $q->orderBy('f_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('f_name', 'desc');
        }else{
            $q->orderBy('f_name', 'asc');
        }
        $data['restro_names']=$q->paginate(10);
        $data['pages']=$data['restro_names']->toarray($data['restro_names']);
        $data['search']=$search;
        $data['type']=$type;
        
        return view('superadmin/restaurants/extra_type_map/index')->with($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['restro_names'] = DB::table('rest_details')->orderBy('f_name', 'asc')->where('status',1)->select('id','f_name','l_name')->get();
        $data['extraTypes']=DB:: table('menu_extra_types')->where('status',1)->get();
        $data['restro_detail_id']=$request->restaurant;
        $data['extraTypeMaps']=DB:: table('rest_extra_type_maps')->where('rest_detail_id',$request->restaurant)->lists('menu_extra_type_id');
        
		
        return view('superadmin/restaurants/extra_type_map/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rest_detail_id=$request->restaurant;
        $total_extra_type_selected=count($request->extra_type);
        $extraTypes=$request->extra_type;
        $preExtraTypes=explode('-', $request->pre_extra_type);

        $insertResult=array_diff($extraTypes,$preExtraTypes);

        foreach ($insertResult as $insertKey => $insertValue) {

        	DB::table('rest_extra_type_maps')->insert(
                
                ['rest_detail_id'=> $rest_detail_id,'menu_extra_type_id' =>  $insertValue,'created_by' =>  Auth::User('user')->id,'updated_by' =>  Auth::User('user')->id,'created_at' =>  date('Y-m-d H:i:s'),'updated_at' =>  date('Y-m-d H:i:s') ]
               );
        }

        $deleteResult=array_diff($preExtraTypes,$extraTypes);

        foreach ($deleteResult as $deleteKey => $deleteValue) {
        	
        	$deletedExtraType= DB::table('rest_extra_type_maps')->where('rest_detail_id',$rest_detail_id)->where('menu_extra_type_id',$deleteValue)->delete();

        	$deletedMenuExtra= DB::table('rest_extra_type_maps')->where('rest_detail_id',$rest_detail_id)->where('menu_extra_type_id',$deleteValue)->delete();
        }

        return redirect(route('root.extratypemap.index'));    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
