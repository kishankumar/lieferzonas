<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\ShopCategory;
use App\superadmin\ShopItem;
use Validator;
use Auth;

class ShopCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = ShopCategory::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $catInfo=$q->paginate(10);

        return view('superadmin.ShopManagement.category.index')->with('categories',$catInfo)->with('search',$search)->with('type',$type); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo '<pre>';print_r(Auth::User());die;
        //$name = Input::get('name');//for get input field value
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages = array(
            'end_after' => "Valid from date should be before valid to date.",
        );
        $userData = array(
            'name'      => $data['name'],
            'description'     =>  $data['description'],
        );
        
        $category_id = Input::get('category_id');
        if($category_id){
            $rules = array(
                'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'description'     =>  'required|min:6',
            );
        }else{
            $rules = array(
                'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'description'     =>  'required|min:6',
            );
        }
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($category_id){
            $count= ShopCategory:: where('id','!=',$category_id)->where('name', '=',$request->name)->where('status', '!=','2')->count();
        }else{
            $count= ShopCategory:: where('name', '=',$request->name)->where('status', '!=','2')->count();
        }
        if($count){
            $errors = array('name'=>'category name has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($category_id){
            $object =ShopCategory::find($category_id);
        }else{
            $object= new ShopCategory;
            $object->created_at=date("Y-m-d H:i:s");
            $object->created_by=Auth::User('user')->id;
        }
        $object->name=$request->name;
        $object->description=$request->description;
        $object->updated_by=Auth::User('user')->id;
        $object->updated_at=date("Y-m-d H:i:s");
        $object->status=$request->status;
        $object->save();
        
        if($category_id){
            Session::flash('message', 'category updated Successfully!'); 
            $json['success']=1;
            $json['message']='category updated Successfully.';
        }else{
            Session::flash('message', 'category added Successfully!'); 
            $json['success']=1;
            $json['message']='category added Successfully.';
        }
        echo json_encode($json);
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedCategories= $data['checkedCategories'];
        
        $data['categories']=ShopCategory::find($selectedCategories);
        $categories = $data['categories']->toArray();
        if(count($categories)>0){
            $json['success']=1;
            $json['id']=$categories['id'];
            $json['name']=$categories['name'];
            $json['description']=$categories['description'];
            $json['status']=$categories['status'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCategories = $data['selectedCategories'];
        }else{
            $json['success']=0;
            $errors = array('name'=>'Please enter valid data.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        foreach($selectedCategories as $key => $value)
        {
            $count= ShopItem:: where('shop_category_id', $value)->where('status', '!=','2')->count();
            if($count){
                $errors = array('name'=>'Selected category already assigned to shop items.');
                $json['success']=0;
                $json['errors']=$errors;
                echo json_encode($json);
                return;
            }
        }
        
        foreach($selectedCategories as $key => $value)
        {
            $user = ShopCategory::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCategories = $data['selectedCategories'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Category Name</th>
                        
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th></tr><tr></tr></thead>';
        foreach($selectedCategories as $key => $value)
        {
            $data = DB::table('shop_categories')
           

            ->where('id','=', $value)
           
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}
               	if((strlen($data['description']) > 50))
				{
					$desc = substr($data['description'],0,50).'...';
				}
				else
				{
					$desc = $data['description'];
				}	
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['name'].'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['description'].'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
}
