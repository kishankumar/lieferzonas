<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use validator;
use App\superadmin\RestNotification;
use App\superadmin\RestNotificationMap;

class RestNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='title';
        }
        
        $q = DB::table('rest_notifications')->where('status','!=', '2');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('title', 'LIKE', "%$search%");
                $q2->orWhere('comment', 'LIKE', "%$search%");
            });
        }
        if($type=='title'){
            $q->orderBy('title', 'asc');
        }elseif($type=='title-desc'){
            $q->orderBy('title', 'desc');
        }elseif($type=='comment-desc'){
            $q->orderBy('comment', 'desc');
        }elseif($type=='comment'){
            $q->orderBy('comment', 'asc');
        }else{
            $q->orderBy('title', 'asc');
        }
        $data=$q->paginate(10);
        
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data[$i] = (array) $data[$i];
        }
        return view('superadmin.notifications.notification')->with('search',$search)->with('type',$type)
              ->with('notiInfo',$data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rest = DB::table('rest_details')->select('id', 'f_name','l_name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($rest); $i < $c; ++$i) {
            $rest[$i] = (array) $rest[$i];
            
        }  
		 
           return view('superadmin.notifications.addnotification')->with('resturants',$rest);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->set_validation('',$request);
        $auth_id = Auth::User()->id;
		$count= RestNotification:: where('title', '=',$request->title)->where('status', '!=','2')->count();
		if($count>0)
		{
			Session::flash('message', 'This title has already been taken!');
			return redirect(route('root.restnotification.create'));
		}
		else
		{
			$restnotification = new RestNotification;
			$restnotification->title=$request->title;
			$restnotification->send_type=$request->is_rest_specific;
			$restnotification->is_type=1;
			$restnotification->comment=$request->comment;
			$restnotification->status=$request->status;
			$restnotification->created_by=$auth_id;
			$restnotification->created_at=date("Y-m-d H:i:s");
			$restnotification->save();
			$id =$restnotification->id;
			$restlist = array();
			$restlist = Input::get('restlist');
			
			for($i=0; $i<count($restlist); $i++ )
			{
			  $notification_restmap = new RestNotificationMap;
			  $notification_restmap->notification_id=$id;
			  $notification_restmap->rest_detail_id=$restlist[$i];
			  $notification_restmap->status=$request->status;
			  $notification_restmap->created_by=$auth_id;
			  $notification_restmap->created_at=date("Y-m-d H:i:s");
			  $notification_restmap->save();
			  
			  
			}
			Session::flash('message', 'Added Successfully!');
			 return redirect(route('root.restnotification.index'));
		}
	   
		

		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rest = DB::table('rest_details')->select('id', 'f_name','l_name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($rest); $i < $c; ++$i) {
            $rest[$i] = (array) $rest[$i];
            
        }  
		
		
        $selrest=RestNotificationMap::where('notification_id',$id)->where('status','!=','2')->lists('rest_detail_id');		
        $selrest = $selrest->toArray();
     
        $notification = RestNotification::where('id',$id)->get();
	    $notification =$notification->toArray();

        return view('superadmin/notifications/editnotification')->with('notification',$notification)
		->with('resturants',$rest)->with('selected_rest',$selrest);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
		$this->set_validation($id,$request);
        $auth_id = Auth::User()->id;
		$count= RestNotification:: where('id','!=',$id)->where('title', '=',$request->title)->where('status', '!=','2')->count();
		if($count>0)
		{
			Session::flash('message', 'This title has already been taken!');
			return redirect('root/restnotification/'.$id.'/edit');
		}
		else
		{
			$restnotification = RestNotification::find($id);
			$restnotification->title=$request->title;
			$restnotification->send_type=$request->is_rest_specific;
			$restnotification->comment=$request->comment;
			$restnotification->status=$request->status;
			$restnotification->updated_by=$auth_id;
			$restnotification->updated_at=date("Y-m-d H:i:s");
			$restnotification->save();
			$id =$restnotification->id;
			
			if($request->is_rest_specific=='2')
			{
				$countrest = RestNotificationMap::where('notification_id', '=', $id)->where('status','=','1')->get();
				$countrest = count($countrest->toArray());
				
				if($countrest!= null)
				{
					$affected_data = DB::table('rest_notification_maps')
					->where('notification_id', '=', $id)
					->where('status', '=', '1')
					->update(array('status' => '2'));
				}
				$restlist = array();
				$restlist = Input::get('restlist');
				//print_r($restlist); 
				for($i=0; $i<count($restlist); $i++ )
				{
				  $notification_restmap = new RestNotificationMap;
				  $notification_restmap->notification_id=$id;
				  $notification_restmap->rest_detail_id=$restlist[$i];
				  $notification_restmap->status=$request->status;
				  $notification_restmap->created_by=$auth_id;
				  $notification_restmap->created_at=date("Y-m-d H:i:s");
				  $notification_restmap->save();
				  
				  
				}
			
			}
			else
			{
				$countrest = RestNotificationMap::where('notification_id', '=', $id)->where('status','=','1')->get();
				$countrest = count($countrest->toArray());
				
				if($countrest!= null)
				{
					$affected_data = DB::table('rest_notification_maps')
					->where('notification_id', '=', $id)
					->where('status', '=', '1')
					->update(array('status' => '2'));
				}
			}
			
			
		    Session::flash('message', 'Added Successfully!');
			 return redirect(route('root.restnotification.index'));
		}	 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function delete(Request $request)
    {

        if($request->ajax()){
            $data = Input::all();
           
            $selectedNotification = $data['selectedNotification'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedNotification as $key => $value)
        {
            $notification = RestNotification::find($value);
            $notification->status = '2';
            $notification->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	 /*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {

        if($request->ajax()){
            $data = Input::all();
           
            $selectedNotification = $data['selectedNotification'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Coupon code</th>
						<th style="border:solid 1px #81C02F; padding:10px">Comment</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Is Resturant specific</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach( $selectedNotification as $key => $value)
        {
            $data = DB::table('rest_notifications')
            ->where('id','=', $value)
            ->select('*')
            ->get(10);
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			for ($i = 0, $c = count($data); $i < $c; ++$i) {
					 $data[$i] = (array) $data[$i];
			}
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if($data['0']['send_type']=='2')
				{
					$send_type='Yes';
				}
				else
				{
					$send_type='No';
				}
				if((strlen($data['0']['comment']) > 50))
				{
					$comment = substr($data['0']['comment'],0,50).'...';
				}
				else
				{
					$comment = $data['0']['comment'];
				}	
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['title'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['comment'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$send_type.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
    /*added by Rajlakshmi(31-05-16)*/
	public function set_validation($id=null,$request)
     {
        $message=array(
            "title.required"=>"title is required",
			"title.max"=>"title should be max 40 characters",
            "comment.required"=>"comment is required",
            );

        $this->validate($request,[
        'title' => 'required|max:40',
        'comment' => 'required',
        ],$message);
     }
}
