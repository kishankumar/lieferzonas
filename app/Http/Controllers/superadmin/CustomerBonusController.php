<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\RestCustBonus;
use Validator;
use Auth;

class CustomerBonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
        $Info= RestCustBonus:: where('status','1')->orderBy('id', 'desc')->paginate(10);
        return view('superadmin.customer_bonus.index')->with('bonus',$Info); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages=array(
            "value.Regex"=>"Bonus amount format not valid",
        );
        $userData = array(
            'value'=>$data['value']
        );
        $bonus_id = Input::get('bonus_id');
        $rules = array(
            'value' =>  'required|Regex:/^\d*(\.\d{1,2})?$/'
        );
        
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        $bonus_id=1;
        $type=RestCustBonus::find($bonus_id);
        $type->value=  number_format($request->value,2);
        $type->created_by=Auth::User('user')->id;
        $type->updated_by=Auth::User('user')->id;
        $type->created_at=date("Y-m-d H:i:s");
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=$request->status;
        $type->save();
        
        Session::flash('message', 'Customer Bonus updated Successfully!'); 
        $json['success']=1;
        $json['message']='Customer Bonus updated Successfully.';
        
        echo json_encode($json);
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedBonus = $data['selectedBonus'];
        
        $data['bonus']=RestCustBonus::find($selectedBonus);
        $bonus = $data['bonus']->toArray();
        if(count($bonus)>0){
            $json['success']=1;
            $json['id']=$bonus['id'];
            $json['value']=round($bonus['value'],2);
            $json['status']=$bonus['status'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/*added by Rajlakshmi(01-06-16)*/
	 public function download(Request $request)
    {
		//echo 'rrrrr';exit();
        if($request->ajax()){
            $data = Input::all();
            $selectedBonus = $data['selectedBonus'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		$data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Bonus value</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Bonus type</th>
                        <th>Status</th></tr></thead>';
        foreach($selectedBonus as $key => $value)
        {
            $data = DB::table('rest_cust_bonus')
            

            ->where('id','=', $value)
           
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if($data['type']=='p')
				{
					$type='Percentage';
				}
				else
				{
					$type='Cash';
				}	
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['value'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$type.'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
    /*added by Rajlakshmi(01-06-16)*/
}
