<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\superadmin\RestDetail;
use App\superadmin\RestSetting;
use App\superadmin\Color_setting;
use App\superadmin\RestPrivilege;
use App\superadmin\RestPrivilegeLog;

use Session;
use DB;
use Auth;
use Validator;
use Input;
class RestaurantSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['restro_detail_id']=$request->restaurant;
        $data['restro_names'] = RestDetail::where('status',1)->orderBy('f_name', 'asc')->select('id','f_name','l_name')->get();
        $data['getSettingsData'] = RestSetting::where('status',1)->where('rest_detail_id',$data['restro_detail_id'])
        ->select('id','rest_detail_id','is_open','is_new','header_color','body_color','privilege_id')->get();
		
        return view('superadmin/restaurants/settings/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['colors'] = Color_setting::where('status',1)->select('id','color','color_code')->get();
        $data['privileges'] = RestPrivilege::where('status',1)->select('id','name')->get();

        $data['restSettings'] = RestSetting::where('status',1)->where('id',$id)->select('is_new_valid','header_color_valid','body_color_valid','privilege_valid','id','rest_detail_id','is_open','is_new','allow_admin','header_color','body_color','privilege_id')->first();
        $auth_id = Auth::User()->id;
			$username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
			for ($i = 0, $c = count($username); $i < $c; ++$i) {
				$username[$i] = (array) $username[$i];
				
			}
		return view('superadmin/restaurants/settings/edit')->with($data)->with('username',$username);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $is_open=$request->is_open;
        $is_new=$request->is_new;
        
        if ($is_open==1){
            $is_open=$is_open;
        }
        else{
            $is_open=0;
        }
        
        if ($is_new==1){
            $is_new=$is_new;
        }
        else{
            $is_new=0;
        }
        
        $restSettings= RestSetting:: find($id);
        ////////////////Update Information////////////////
        $restSettings->is_open=$is_open;
        $restSettings->is_new=$is_new;
        $restSettings->header_color=$request->header_color;
        $restSettings->body_color=$request->body_color;
        $restSettings->privilege_id=$request->privilege;
        
        $restSettings->allow_admin=$request->allow_admin;
        $restSettings->is_new_valid = date("Y-m-d", strtotime($request->is_new_valid));
        $restSettings->header_color_valid = date("Y-m-d", strtotime($request->header_color_valid));
        $restSettings->body_color_valid = date("Y-m-d", strtotime($request->body_color_valid));
        $restSettings->privilege_valid = date("Y-m-d", strtotime($request->privilege_valid));
        
        $restSettings->updated_by=Auth::User()->id;
        $restSettings->save();
        
        ///////////////////Add Privilege to log table////////////////
        $privilegeLog=new RestPrivilegeLog;
        $privilegeLog->rest_detail_id=$request->rest_id;
        $privilegeLog->privilege_id=$request->privilege;
        $privilegeLog->created_by=Auth::User()->id;
        $privilegeLog->updated_by=Auth::User()->id;
        $privilegeLog->save();
        Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.restsetting.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
         if($request->ajax()){
            $data = Input::all();
            $selectedSettings = $data['selectedSettings'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Restaurant</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Open/Close</th>
                        <th style="border:solid 1px #81C02F; padding:10px">New/Not New</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Header Color</th>
						<th style="border:solid 1px #81C02F; padding:10px">Body Color</th>
						<th style="border:solid 1px #81C02F; padding:10px">Privilege</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedSettings as $key => $value)
        {
            
             $data['restname'] = DB::table('rest_details')
			 ->where('id','=',$value)
			->select('f_name','l_name','status')->get();
			for ($i = 0, $c = count($data['restname']); $i < $c; ++$i) {
             $data['restname'][$i] = (array) $data['restname'][$i];
		    } 
			 $data['getSettingsData'] = DB::table('rest_settings')
			 ->where('rest_detail_id','=',$value)
			->select('id','rest_detail_id','is_open','is_new','header_color','body_color','privilege_id')->get();
            for ($i = 0, $c = count($data['getSettingsData']); $i < $c; ++$i) {
             $data['getSettingsData'][$i] = (array) $data['getSettingsData'][$i];
		    } 
            
			  
		      
				
				if($data['restname']['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if($data['getSettingsData']['0']['is_open']==1)
				{
					$ocstatus='Open';
				}
				else
				{
					$ocstatus='Close';
				}	
				if($data['getSettingsData']['0']['is_new']==1)
				{
					$onstatus='New';
				}
				else
				{
					$onstatus='Not New';
				}	
				if($data['getSettingsData']['0']['privilege_id']==1)
				{
					$privilege='Normal';
				}
				else
				{
					$privilege='Featured';
				}	
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['restname']['0']['f_name'].' '.$data['restname']['0']['l_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$ocstatus.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$onstatus.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['getSettingsData']['0']['header_color'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['getSettingsData']['0']['body_color'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$privilege.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
