<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use App\superadmin\RestSubMenu;
use App\superadmin\RestMenu;
use App\superadmin\RestDetail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\superadmin\Alergic_content;
use App\superadmin\Spice;
use App\superadmin\SubMenuPriceMap;
use App\superadmin\RestSubmenuAllergicMap;
use App\superadmin\RestSubmenuSpiceMap;

use Session;
use DB;
use Auth;
use Validator;

class SubMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['restro_detail_id'] = $request->restaurant;

        $data['restro_names'] = RestDetail::where('status', 1)->orderBy('f_name', 'asc')->select('id', 'f_name', 'l_name')->get();

        $data['getRestroSubMenus'] = RestSubMenu::where('status', '!=', 2)->where('rest_detail_id', $data['restro_detail_id'])->select('id', 'rest_detail_id', 'menu_id', 'name', 'code', 'image', 'status')->paginate(20);


        return view('superadmin/restaurants/submenu/index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['restro_names'] = DB::table('rest_details')->orderBy('f_name', 'asc')->where('status', 1)->select('id', 'f_name', 'l_name')->get();


        $data['alergic_contents'] = Alergic_content::where('status', 1)->select('id', 'name')->get();
        $data['spices'] = Spice::where('status', 1)->select('id', 'name')->get();


        return view('superadmin/restaurants/submenu/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->set_validation('', $request);
        $restaurant_submenus = new RestSubMenu;
        //dd($request);
        ///////////File Upload////////////////////////////

        $destinationpath = public_path() . "/uploads/superadmin/submenu";
        $pic = Input::file('file');
        if ($pic) {
            $extension = $pic->getClientOriginalExtension(); // getting image extension
            $filename = time() . rand(111, 999) . '.' . $extension; // renameing image
            $pic->move($destinationpath, $filename);
        } else {
            $filename = '';
        }

        if (($request->alergic) == '') {

            $is_alergic = 0;
        } else {
            $is_alergic = 1;
        }
        if (($request->spicy) == '') {

            $is_spicy = 0;
        } else {
            $is_spicy = 1;
        }

        ////////////////Add Basic Information////////////////
        $restaurant_submenus->rest_detail_id = $request->restro_name;
        $restaurant_submenus->menu_id = $request->menu_name;
        $restaurant_submenus->name = $request->submenu_name;
        $restaurant_submenus->short_name = $request->shortname;
        $restaurant_submenus->description = $request->description;
        $restaurant_submenus->info = $request->info;
        $restaurant_submenus->code = $request->code;
        $restaurant_submenus->priority = $request->priority;
        $restaurant_submenus->image = $filename;
        $restaurant_submenus->is_alergic = $is_alergic;
        $restaurant_submenus->is_spicy = $is_spicy;
        $restaurant_submenus->created_by = Auth::User()->id;
        $restaurant_submenus->updated_by = Auth::User()->id;


        $restaurant_submenus->save();

        $submenu_id = $restaurant_submenus['id'];

        ///////////////Add price to submenus///////////////////////////

        $totalPriceCount = count($request->price_types);
        $price = $request->price_types;
        $priceTypeValue = $request->price_types_id;

        if ($totalPriceCount > 0) {

            for ($i = 0; $i < $totalPriceCount; $i++) {

                $submenuPriceMap = new SubMenuPriceMap;

                $submenuPriceMap->rest_detail_id = $request->restro_name;
                $submenuPriceMap->menu_id = $request->menu_name;
                $submenuPriceMap->sub_menu_id = $submenu_id;
                $submenuPriceMap->price_type_id = $priceTypeValue[$i];
                $submenuPriceMap->price = $price[$i];
                $submenuPriceMap->created_by = Auth::User()->id;
                $submenuPriceMap->updated_by = Auth::User()->id;

                $submenuPriceMap->save();

            }

        }

        ///////////////Add alergic to submenus///////////////////////////

        if (($request->alergic) != '') {

            $totalalergicCount = count($request->alergic_contents);
            $alergicContents = $request->alergic_contents;

            for ($i = 0; $i < $totalalergicCount; $i++) {

                $submenuAlergicMap = new RestSubmenuAllergicMap;

                $submenuAlergicMap->rest_detail_id = $request->restro_name;
                $submenuAlergicMap->menu_id = $request->menu_name;
                $submenuAlergicMap->sub_menu_id = $submenu_id;
                $submenuAlergicMap->allergic_id = $alergicContents[$i];
                $submenuAlergicMap->created_by = Auth::User()->id;
                $submenuAlergicMap->updated_by = Auth::User()->id;

                $submenuAlergicMap->save();

            }

        }


        ///////////////Add spice to submenus///////////////////////////

        if (($request->spicy) != '') {

            $totalspiceCount = count($request->spicy_content);
            $spiceContents = $request->spicy_content;

            for ($i = 0; $i < $totalspiceCount; $i++) {

                $submenuSpiceMap = new RestSubmenuSpiceMap;

                $submenuSpiceMap->rest_detail_id = $request->restro_name;
                $submenuSpiceMap->menu_id = $request->menu_name;
                $submenuSpiceMap->sub_menu_id = $submenu_id;
                $submenuSpiceMap->spice_id = $spiceContents[$i];
                $submenuSpiceMap->created_by = Auth::User()->id;
                $submenuSpiceMap->updated_by = Auth::User()->id;

                $submenuSpiceMap->save();

            }

        }


        Session::flash('message', 'Added Successfully!');
        return redirect(url('root/edit_restaurant_menu/' . $request->input("restro_name")));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['restro_names'] = DB::table('rest_details')->orderBy('f_name', 'asc')->where('status', 1)->select('id', 'f_name', 'l_name')->get();
        $data['submenus'] = RestSubMenu::find($id);
        $restro_id = $data['submenus']['rest_detail_id'];

        $data['menus'] = RestMenu:: where('rest_detail_id', $restro_id)->where('status', 1)->select('id', 'name')->get();

        $data['alergic_contents'] = Alergic_content::where('status', 1)->select('id', 'name')->get();
        $data['spices'] = Spice::where('status', 1)->select('id', 'name')->get();

        $data['allergicMaps'] = RestSubmenuAllergicMap:: where('sub_menu_id', $id)->lists('allergic_id')->toArray();

        $data['spiceMaps'] = RestSubmenuSpiceMap:: where('sub_menu_id', $id)->lists('spice_id')->toArray();

        $data['priceTypesWithValue'] = DB::table('rest_price_maps')
            ->join('price_types', 'rest_price_maps.price_type_id', '=', 'price_types.id')
            ->leftjoin('sub_menu_price_maps', 'rest_price_maps.price_type_id', '=', 'sub_menu_price_maps.price_type_id')
            ->select('price_types.id', 'price_types.name', 'sub_menu_price_maps.price')
            ->where('rest_price_maps.rest_detail_id', $restro_id)
            ->where('sub_menu_price_maps.sub_menu_id', $id)
            ->get();

        $data['priceTypes'] = DB::table('rest_price_maps')
            ->join('price_types', 'rest_price_maps.price_type_id', '=', 'price_types.id')
            ->select('price_types.id', 'price_types.name')
            ->where('rest_price_maps.rest_detail_id', $restro_id)
            ->get();


        return view('superadmin/restaurants/submenu/edit')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $this->set_validation('', $request);
        $restaurant_submenus = RestSubMenu:: find($id);
        //dd($request);
        ///////////File Upload////////////////////////////

        $destinationpath = public_path() . "/uploads/superadmin/submenu";
        $pic = Input::file('file');
        if ($pic) {
            $extension = $pic->getClientOriginalExtension(); // getting image extension
            $filename = time() . rand(111, 999) . '.' . $extension; // renameing image
            $pic->move($destinationpath, $filename);
            $restaurant_submenus->image = $filename;
        }

        if (($request->alergic) == '') {

            $is_alergic = 0;
        } else {
            $is_alergic = 1;
        }
        if (($request->spicy) == '') {

            $is_spicy = 0;
        } else {
            $is_spicy = 1;
        }


        ////////////////Add Basic Information////////////////
        $restaurant_submenus->rest_detail_id = $request->restro_name;
        $restaurant_submenus->menu_id = $request->menu_name;
        $restaurant_submenus->name = $request->submenu_name;
        $restaurant_submenus->short_name = $request->shortname;
        $restaurant_submenus->description = $request->description;
        $restaurant_submenus->info = $request->info;
        $restaurant_submenus->code = $request->code;
        $restaurant_submenus->priority = $request->priority;
        $restaurant_submenus->status = $request->status;
        $restaurant_submenus->is_alergic = $is_alergic;
        $restaurant_submenus->is_spicy = $is_spicy;
        $restaurant_submenus->created_by = Auth::User()->id;
        $restaurant_submenus->updated_by = Auth::User()->id;


        $restaurant_submenus->save();

        ///////////////Add price to submenus///////////////////////////

        $totalPriceCount = count($request->price_types);
        $price = $request->price_types;
        $priceTypeValue = $request->price_types_id;

        if ($totalPriceCount > 0) {

            $deleted = SubMenuPriceMap:: where('sub_menu_id', $id)->delete();

            for ($i = 0; $i < $totalPriceCount; $i++) {

                $submenuPriceMap = new SubMenuPriceMap;

                $submenuPriceMap->rest_detail_id = $request->restro_name;
                $submenuPriceMap->menu_id = $request->menu_name;
                $submenuPriceMap->sub_menu_id = $id;
                $submenuPriceMap->price_type_id = $priceTypeValue[$i];
                $submenuPriceMap->price = $price[$i];
                $submenuPriceMap->created_by = Auth::User()->id;
                $submenuPriceMap->updated_by = Auth::User()->id;

                $submenuPriceMap->save();

            }

        }

        ///////////////Add alergic to submenus///////////////////////////

        if (($request->alergic) != '') {

            $totalalergicCount = count($request->alergic_contents);
            $alergicContents = $request->alergic_contents;

            $deleted = RestSubmenuAllergicMap:: where('sub_menu_id', $id)->delete();

            for ($i = 0; $i < $totalalergicCount; $i++) {

                $submenuAlergicMap = new RestSubmenuAllergicMap;

                $submenuAlergicMap->rest_detail_id = $request->restro_name;
                $submenuAlergicMap->menu_id = $request->menu_name;
                $submenuAlergicMap->sub_menu_id = $id;
                $submenuAlergicMap->allergic_id = $alergicContents[$i];
                $submenuAlergicMap->created_by = Auth::User()->id;
                $submenuAlergicMap->updated_by = Auth::User()->id;

                $submenuAlergicMap->save();

            }

        } else {
            $deleted = RestSubmenuAllergicMap:: where('sub_menu_id', $id)->delete();
        }


        ///////////////Add spice to submenus///////////////////////////

        if (($request->spicy) != '') {

            $totalspiceCount = count($request->spicy_content);
            $spiceContents = $request->spicy_content;

            $deleted = RestSubmenuSpiceMap:: where('sub_menu_id', $id)->delete();

            for ($i = 0; $i < $totalspiceCount; $i++) {

                $submenuSpiceMap = new RestSubmenuSpiceMap;

                $submenuSpiceMap->rest_detail_id = $request->restro_name;
                $submenuSpiceMap->menu_id = $request->menu_name;
                $submenuSpiceMap->sub_menu_id = $id;
                $submenuSpiceMap->spice_id = $spiceContents[$i];
                $submenuSpiceMap->created_by = Auth::User()->id;
                $submenuSpiceMap->updated_by = Auth::User()->id;

                $submenuSpiceMap->save();

            }

        } else {
            $deleted = RestSubmenuSpiceMap:: where('sub_menu_id', $id)->delete();
        }
        Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.submenu.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /*added by Rajlakshmi(02-06-16)*/
    public function download(Request $request)
    {
        if ($request->ajax()) {
            $data = Input::all();
            $selectedSubmenus = $data['selectedSubmenus'];
        } else {
            $json['success'] = 0;
            echo json_encode($json);
            return;
        }
        $data1 = '';
        $data1 .= '<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                      
                        <th style="border:solid 1px #81C02F; padding:10px">Restaurant</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Menu Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Submenu Name</th>
						<th style="border:solid 1px #81C02F; padding:10px">Code</th>
						<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach ($selectedSubmenus as $key => $value) {

            $data = DB::table('rest_sub_menus')
                ->leftjoin('rest_details', 'rest_sub_menus.rest_detail_id', '=', 'rest_details.id')
                ->leftjoin('rest_menus', 'rest_sub_menus.menu_id', '=', 'rest_menus.id')
                ->where('rest_sub_menus.id', '=', $value)
                ->select('rest_menus.*', 'rest_details.f_name', 'rest_details.l_name',
                    'rest_menus.name as menuname')->get();
            for ($i = 0, $c = count($data); $i < $c; ++$i) {
                $data[$i] = (array)$data[$i];
            }


            if ($data['0']['status'] == '1') {
                $status = 'Active';
            } else {
                $status = 'Deactive';
            }

            $data1 .= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">' . $data['0']['f_name'] . ' ' . $data['0']['l_name'] . '</td>
				<td style="border:solid 1px #81C02F; padding:10px">' . $data['0']['menuname'] . '</td>
				<td style="border:solid 1px #81C02F; padding:10px">' . $data['0']['code'] . '</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">' . $status . '</td>';
        }

        $data1 .= '</tr></table>';
        $sendata['data1'] = $data1;
        echo json_encode($sendata);
        return;
    }
    /*added by Rajlakshmi(02-06-16)*/

    /////Add validation////////
    public function set_validation($id = null, $request)
    {
        $message = array(
            "restro_name.required" => "Select restaurant name",
            "menu_name.required" => "Select menu name",
            "submenu_name.email" => "Enter Sub menu",
            "priority.required" => "Enter priority",
            "priority.numeric" => " Priority should be numeric",


        );

        $this->validate($request, [
            'restro_name' => 'required',
            'menu_name' => 'required',
            'submenu_name' => 'required',
            'priority' => 'required|numeric',


        ], $message);
    }
}
