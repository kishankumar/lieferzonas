<?php
namespace App\Http\Controllers\superadmin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Validator;
use Input;
use App\superadmin\RestDetail;
use App\front\OrderItem;
use App\front\FrontUserDetail;
use App\front\UserOrder;
use App\admin\FrontOrderStatus;
use App\admin\FrontOrderStatusMap;
//use App\library\Smsapi;

class RestaurantOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$smsObj = new Smsapi();
        //$number='917876222011',$message='Hello Friends';
        //$sms = $smsObj->sendsms_api($number,$message);
        
        $search='';$type='';
        if($request->search){
            $search=$request->search;
            $search = trim($search);
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='created-desc';
        }
        $q = DB::table('user_orders');
        $q->leftjoin('front_user_details', 'user_orders.front_user_id', '=', 'front_user_details.front_user_id');
        $q->leftjoin('front_users', 'front_users.id', '=', 'user_orders.front_user_id');
        $q->leftjoin('rest_details', 'rest_details.id', '=', 'user_orders.rest_detail_id');
        $q->leftjoin('front_order_statuses', 'front_order_statuses.id', '=', 'user_orders.front_order_status_id');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('user_orders.order_id', 'LIKE', "%$search%");
                $q2->orWhere('front_user_details.fname', 'LIKE', "%$search%");
                $q2->orWhere('front_user_details.lname', 'LIKE', "%$search%");
                $q2->orWhere('front_user_details.email', 'LIKE', "%$search%");
                $q2->orWhere('front_user_details.mobile', 'LIKE', "%$search%");
                $q2->orWhere('front_users.email', 'LIKE', "%$search%");
                $q2->orWhere('rest_details.f_name', 'LIKE', "%$search%");
                $q2->orWhere('rest_details.l_name', 'LIKE', "%$search%");
            });
        }
        $q->where('user_orders.status','=', '1');
        //$q->where('user_orders.rest_detail_id',Auth::User('admin')->rest_detail_id);
        if($type=='order'){
            $q->orderBy('user_orders.order_id', 'asc');
        }elseif($type=='order-desc'){
            $q->orderBy('user_orders.order_id', 'desc');
        }elseif($type=='created-desc'){
            $q->orderBy('user_orders.created_at', 'desc');
        }elseif($type=='created'){
            $q->orderBy('user_orders.created_at', 'asc');
        }else if($type=='name'){
            $q->orderBy('front_user_details.fname', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('front_user_details.fname', 'desc');
        }elseif($type=='email-desc'){
            $q->orderBy('front_users.email', 'desc');
        }elseif($type=='email'){
            $q->orderBy('front_users.email', 'asc');
        }elseif($type=='number-desc'){
            $q->orderBy('front_user_details.mobile', 'desc');
        }elseif($type=='number'){
            $q->orderBy('front_user_details.mobile', 'asc');
        }else{
            $q->orderBy('user_orders.created_at', 'desc');
        }
        $q->select('user_orders.*','front_user_details.fname','front_user_details.lname','front_user_details.mobile','front_user_details.dob',
                'front_users.email','rest_details.f_name','rest_details.l_name','front_order_statuses.status_name');
        $data['OrderInfo']=$q->paginate(20);
        $data['type']=$type;
        $data['search']=$search;
        
        return view('superadmin.restaurantOrders.orders')->with($data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id){
            $count= UserOrder:: where('status','=','1')->where('order_id','=',$id)->count();
            if($count < 1){
                Session::flash('message', 'Please select order again.'); 
                return redirect('root/front/orders');
            }
        }
        
        $q = DB::table('user_orders');
        $q->leftjoin('front_user_details', 'user_orders.front_user_id', '=', 'front_user_details.front_user_id');
        $q->leftjoin('front_users', 'front_users.id', '=', 'user_orders.front_user_id');
        $q->leftjoin('rest_details', 'rest_details.id', '=', 'user_orders.rest_detail_id');
        $q->leftjoin('front_user_addresses',  'user_orders.front_user_address_id', '=','front_user_addresses.id');
        $q->leftjoin('countries',  'front_user_addresses.country_id', '=','countries.id');
        $q->leftjoin('states',  'front_user_addresses.state_id', '=','states.id');
        $q->leftjoin('cities',  'front_user_addresses.city_id', '=','cities.id');
        $q->leftjoin('front_order_statuses', 'front_order_statuses.id', '=', 'user_orders.front_order_status_id');
        $q->leftjoin('order_payments', 'order_payments.order_id', '=','user_orders.order_id');
        
        $q->where('user_orders.status','=', '1');
        $q->where('user_orders.order_id',$id);
        $q->select('user_orders.*','front_user_details.fname','front_user_details.lname','front_user_details.mobile','front_user_details.dob',
                'front_users.email','rest_details.f_name','rest_details.l_name','front_user_addresses.booking_person_name','front_user_addresses.zipcode',
                'front_user_addresses.address','front_user_addresses.landmark','countries.name as country_name',
                'states.name as state_name','cities.name as city_name','front_order_statuses.status_name',
                'order_payments.payment_method','order_payments.order_type','order_payments.discount_amount');
        $data['OrderInfo']=$q->get();
		//print_r($data['OrderInfo']); //die;
        $data['ItemInfo'] = OrderItem:: where('status','=','1')->where('order_id','=',$id)->get();
        
        $data['order_status'] = DB::table('front_order_status_maps')
		->leftjoin('front_order_statuses', 'front_order_statuses.id', '=', 'front_order_status_maps.status_id')
		->where('front_order_status_maps.order_id','=',$id)
		->select('front_order_statuses.status_name','front_order_statuses.id','front_order_status_maps.created_at')
		->get();
        
        $data['TransactionInfo'] = DB::table('front_order_transactions')->where('order_id','=',$id)->get();
        
        return view('superadmin.restaurantOrders.viewpage')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedPages = $data['selectedPages'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedPages as $key => $value)
        {
            $user = UserOrder::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }

	public function printBill($orderId=0)
    {
	    $order_id = $orderId; 
		$order=DB::table('user_orders')->where('order_id',$order_id)->where('status','!=','2')->select('rest_detail_id','front_user_address_id')->get();
		for ($i = 0, $c = count($order); $i < $c; ++$i) {
            $order[$i] = (array) $order[$i];
        }
		
		$rest_id = $order['0']['rest_detail_id'];
        $front_user_address_id = $order['0']['front_user_address_id'];		
        $rest_detail = DB ::table('rest_details')
		->leftjoin('countries','rest_details.country', '=','countries.id')
		->leftjoin('states','rest_details.state', '=','states.id')
		->leftjoin('cities','rest_details.city', '=','cities.id')
		->where('rest_details.id',$rest_id)
		->select('rest_details.f_name','rest_details.l_name','rest_details.add1','rest_details.add2',
		'countries.name as country_name','states.name as state_name','cities.name as city_name','rest_details.pincode')->get();
		
		for ($i = 0, $c = count($rest_detail); $i < $c; ++$i) {
            $rest_detail[$i] = (array) $rest_detail[$i];
        }
		
		$customer_detail = DB ::table('front_user_addresses')
		->leftjoin('countries','front_user_addresses.country_id', '=','countries.id')
		->leftjoin('states','front_user_addresses.state_id', '=','states.id')
		->leftjoin('cities','front_user_addresses.city_id', '=','cities.id')
		->where('front_user_addresses.id',$front_user_address_id)
		->select('front_user_addresses.booking_person_name','front_user_addresses.mobile','front_user_addresses.address','front_user_addresses.landmark',
		'countries.name as country_name','states.name as state_name','cities.name as city_name','front_user_addresses.zipcode')->get();
		for ($i = 0, $c = count($customer_detail); $i < $c; ++$i) {
            $customer_detail[$i] = (array) $customer_detail[$i];
        }
		$item=OrderItem::where('order_id',$order_id)->where('status','!=','2')->select('*')->get();	
		
        $order_payment=DB ::table('order_payments')
		->leftjoin('front_user_details','front_user_details.front_user_id', '=','order_payments.front_user_id')
		->leftjoin('order_services','order_services.id', '=','order_payments.order_type')
		->where('order_payments.order_id',$order_id)->where('order_payments.status','!=','2')
		->select('order_payments.*','front_user_details.fname','front_user_details.lname','order_services.name as order_type_name')->get();
		for ($i = 0, $c = count($order_payment); $i < $c; ++$i) {
            $order_payment[$i] = (array) $order_payment[$i];
        }
	    return view('superadmin/restaurantOrders/printbill')->with('customer_detail',$customer_detail)
		->with('rest_detail',$rest_detail)->with('item',$item)->with('order_payment',$order_payment);
	}

}
