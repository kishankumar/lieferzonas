<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\superadmin\Kitchen;
use App\superadmin\RestKitchenMap;
use Session;
use DB;
use Auth;
use Validator;

class AjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public  function  getkitchen(Request $request){

        if($request) {

            $restro_detail_id=$request->restro_detail_id;
            
            $kitchenMaps= DB::table('rest_kitchen_maps')
            ->join('kitchens', 'rest_kitchen_maps.kitchen_id', '=', 'kitchens.id')
            ->select('kitchens.name')
            ->where('rest_kitchen_maps.rest_detail_id',$restro_detail_id)
            ->get();

            $div='';
            foreach ($kitchenMaps as $kitchenmap ) {

                $div .='<div class="col-md-4">'.$kitchenmap->name.'</div>';
               
            }
            echo $div;
            
        }


    }

    public  function  getExtraType(Request $request){

        if($request) {

            $restro_detail_id=$request->restro_detail_id;
            
            $extraTypeMaps= DB::table('rest_extra_type_maps')
            ->join('menu_extra_types', 'rest_extra_type_maps.menu_extra_type_id', '=', 'menu_extra_types.id')
            ->select('menu_extra_types.name')
            ->where('rest_extra_type_maps.rest_detail_id',$restro_detail_id)
            ->get();

            $div='';
            foreach ($extraTypeMaps as $extraTypeMap ) {

                $div .='<div class="col-md-4">'.$extraTypeMap->name.'</div>';
               
            }
            echo $div;
            
        }


    }

    public  function  getRootCategory(Request $request){

        if($request) {

            $restro_detail_id=$request->restro_detail_id;
            
            $rootCategoryMaps= DB::table('rest_root_cat_maps')
            ->join('root_categories', 'rest_root_cat_maps.root_cat_id', '=', 'root_categories.id')
            ->select('root_categories.name')
            ->where('rest_root_cat_maps.rest_detail_id',$restro_detail_id)
            ->get();

            $div='';
            
            foreach ($rootCategoryMaps as $rootCategoryMap ) {

                $div .='<div class="col-md-4">'.$rootCategoryMap->name.'</div>';
               
            }
            echo $div;
            
        }


    }

    public function getData(Request $request)
    {
        if($request) {
            $table=$request->table;
            $mappingTable=$request->maptable;
            $cond=$request->value;
            $joinColumn=$request->joincolumn;
            $selectColumn=$request->selectcolumn;
            $whereColumn=$request->wherecolumn;
            $secondjoin=$request->secondjoincolumn;
            $selectedOption=$request->selectedOption;
            
            $values= DB::table($table)
                ->join($mappingTable, $table.'.'.$joinColumn, '=', $mappingTable.'.'.$secondjoin)
                ->select($mappingTable.'.'.'id',$mappingTable.'.'.$selectColumn)
                ->where($table.'.'.$whereColumn,$cond)
                ->get();
            
            $option='<option value="">Select</option>';
            
            foreach ($values as $value ) {
                $option .='<option value="'.$value->id.'" '.(($selectedOption==$value->id)?'selected':'').'>'.$value->$selectColumn.'</option>';
            }
            echo $option;
        }
    }

    public function getExistValue(Request $request)
    {
        if($request) {

            $firstVal=$request->firstValue;
            $secondVal=$request->secondValue;
            $table=$request->table;
            $firstCond=$request->firstCond;
            $secondCond=$request->secondCond;
            $id=$request->id;

            $values=DB::table($table)->select('id')
            ->where($firstCond,$firstVal)
            ->where($secondCond,$secondVal)
            ->where('status','!=',2)
            ->where('id','!=',$id)
            ->get();

            $count=count($values);
            if($count>0){
                echo 'error';
            }
            else{
                echo 'success';
            }
        }
     }

    

     public function deleteItems(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $selectedItems = $data['selectedItems'];
            $table=$request->tableName;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedItems as $key => $value)
        {
            DB::table($table)->where('id', $value)->update(['status' => '2']);
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }


    public function getCatKitchenPriceType(Request $request){
        if($request) {

            $restroId=$request->restroId;

            $categories=DB::table('rest_categories')->select('id','name')
            ->where('rest_detail_id',$restroId)
            ->where('status','=',1)
            ->get();

            $kitchens= DB::table('rest_kitchen_maps')
            ->join('kitchens', 'rest_kitchen_maps.kitchen_id', '=', 'kitchens.id')
            ->select('kitchens.id','kitchens.name')
            ->where('rest_kitchen_maps.rest_detail_id',$restroId)
            ->get();

            $priceTypes=DB::table('rest_price_maps')
                ->join('price_types', 'rest_price_maps.price_type_id', '=', 'price_types.id')
                ->select('price_types.id','price_types.name')
                ->where('rest_price_maps.rest_detail_id',$restroId)
                ->get();

            $data['categoryItem']='';
            
            $data['option']='<option value="">Select</option>';
            foreach ($categories as $category ) {

                $data['categoryItem'] .='<option value="'.$category->id.'">'.$category->name.'</option>';
              
            }

            $data['kitchenItem']='';
            foreach ($kitchens as $kitchen ) {

                $data['kitchenItem'] .='<option value="'.$kitchen->id.'">'.$kitchen->name.'</option>';
              
            }

            $data['priceTypesItem']='';
            $data['priceTypesSpan']=' <span><strong>Price Type</strong></span>';
            foreach ($priceTypes as $priceType ) {

                $data['priceTypesItem'] .='<input type="text" name="price_types[]" placeholder="'.$priceType->name.' price'.'">
                                            <input type="hidden" name="price_types_id[]" value="'.$priceType->id.'">';

            }
            echo json_encode($data);
        }
     }

     public function gatDataSingleTable(Request $request){

         if($request) {

            $table=$request->table;
            $firstSelectColumn=$request->firstColumn;
            $secondSelectColumn=$request->secondColumn;
            $whereColumn=$request->whereColumn;
            $cond=$request->cond;

            $values=DB::table($table)->select($firstSelectColumn,$secondSelectColumn)
            ->where($whereColumn,$cond)
            ->where('status','=',1)
            ->get();

            $data['option']='<option value="">Select</option>';

            $data['info']='';
            foreach ($values as $value ) {

                $data['info'] .='<option value="'.$value->$firstSelectColumn.'">'.$value->$secondSelectColumn.'</option>';
              
            }
            echo json_encode($data);

        }    


     }

     public  function  getservice(Request $request){

        if($request) {

            $restro_detail_id=$request->restro_detail_id;
            
            $serviceMaps= DB::table('rest_service_maps')
            ->join('front_services', 'rest_service_maps.service_id', '=', 'front_services.id')
            ->select('front_services.name')
            ->where('rest_service_maps.rest_detail_id',$restro_detail_id)
            ->get();

            $div='';
            foreach ($serviceMaps as $servicemap ) {

                $div .='<div class="col-md-4">'.$servicemap->name.'</div>';
               
            }
            echo $div;
            
        }


    }

    public function deleteRestaurants(Request $request){
        if($request->ajax()){
            $data = Input::all();
            $selectedItems = $data['selectedItems'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        DB::beginTransaction();
        $totalSelectedItem=count($selectedItems);
        $i=0;
        foreach($selectedItems as $key => $value)
        {
            DB::table('rest_details')->where('id', $value)->update(['status' => '2']);

            DB::table('rest_email_maps')->where('rest_detail_id', $value)->update(['status' => '2']);

            DB::table('rest_landline_maps')->where('rest_detail_id', $value)->update(['status' => '2']);

            DB::table('rest_mobile_maps')->where('rest_detail_id', $value)->update(['status' => '2']);

            $i++;
        }

        if($i==$totalSelectedItem){
            DB::commit();
        }
        else{
            DB::rollBack();
        }

        Session::flash('message', 'Deleted Successfully!');
        $json['success']=1;
        echo json_encode($json);
        return;
    }

    public function gatPriceType(Request $request){
        if($request) {

            $restroId=$request->restId;

            $priceTypes=DB::table('rest_price_maps')
                ->join('price_types', 'rest_price_maps.price_type_id', '=', 'price_types.id')
                ->select('price_types.id','price_types.name')
                ->where('rest_price_maps.rest_detail_id',$restroId)
                ->get();
            
            $data['priceTypesItem']='';
            $data['priceTypesSpan']=' <span><strong>Price Type</strong></span>';
            foreach ($priceTypes as $priceType ) {

                $data['priceTypesItem'] .='<input type="text" name="price_types[]" placeholder="'.$priceType->name.' price'.'">
                                            <input type="hidden" name="price_types_id[]" value="'.$priceType->id.'">';

            }
            echo json_encode($data);
        }
    }

    /*public  function  getExtras(Request $request){

        if($request) {

            $restro_detail_id=$request->restro_detail_id;
            
            $extraTypeMaps= DB::table('rest_extra_type_maps')
            ->join('menu_extra_types', 'rest_extra_type_maps.menu_extra_type_id', '=', 'menu_extra_types.id')
            ->select('menu_extra_types.name')
            ->where('rest_extra_type_maps.rest_detail_id',$restro_detail_id)
            ->get();

            $div='';
            foreach ($extraTypeMaps as $extraTypeMap ) {

                $div .='<div class="col-md-4">'.$extraTypeMap->name.'</div>';
               
            }
            echo $div;
            
        }


    }*/

}
