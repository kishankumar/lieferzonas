<?php

namespace App\Http\Controllers\superadmin;
date_default_timezone_set('Asia/Calcutta'); 
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\OrderItem;
use App\front\UserOrder;
use App\front\UserOrderReview;
use App\front\UserRestFavourite;
class OrderRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		$order_rate= DB ::table('user_order_reviews')
		->leftjoin('user_orders','user_order_reviews.order_id','=','user_orders.order_id')
		->leftjoin('rest_details','user_orders.rest_detail_id', '=','rest_details.id')
		->leftjoin('front_user_addresses','user_orders.front_user_address_id', '=','front_user_addresses.id')
		->where('user_order_reviews.status','!=',2)
		->orderby('user_orders.created_at','desc')
		->select('user_order_reviews.id','user_order_reviews.is_reported','user_order_reviews.quality_rating','user_order_reviews.service_rating','user_orders.order_id','user_orders.created_at','rest_details.image','rest_details.f_name','rest_details.l_name','front_user_addresses.booking_person_name')
		->paginate(10);
		//print_r($order_rate); die;
        return view('superadmin/userorder/order_rate')->with('order_rate',$order_rate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->set_validation('',$request);    
         $auth_id = Auth::User()->id; 
         $orderrating=UserOrderReview::find($request->review_id); 
		 $orderrating->rest_id=$request->rest_id;
		 $orderrating->user_id=$request->user_id;
		 $orderrating->order_id=$request->order_id;
		 $orderrating->comment=$request->comment;
		 $orderrating->quality_rating = round($request->qrating,2);
		 $orderrating->service_rating =	round($request->srating,2);
		 $orderrating->status=1;
		 $orderrating->updated_by=$auth_id;
		 $orderrating->updated_at=date("Y-m-d H:i:s");
		 $orderrating->save();
        
		Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.userorder.order_rating.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	 public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedReviews = $data['selectedReviews'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedReviews as $key => $value)
        {
            $user = UserOrderReview::find($value);
			$user->status = '2';
            $user->save();
			
			
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	
	public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedreview = $data['review_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		
		$review =DB ::table('user_order_reviews')
		->leftjoin('user_orders','user_order_reviews.order_id','=','user_orders.order_id')
		->leftjoin('rest_details','user_orders.rest_detail_id', '=','rest_details.id')
		->leftjoin('front_user_addresses','user_orders.front_user_address_id', '=','front_user_addresses.id')
		->where('user_order_reviews.id',$selectedreview)
		->select('user_order_reviews.id','user_order_reviews.comment','user_order_reviews.user_id','user_order_reviews.rest_id','user_order_reviews.is_reported','user_order_reviews.quality_rating','user_order_reviews.service_rating','user_orders.order_id','user_orders.created_at','rest_details.image','rest_details.f_name','rest_details.l_name','front_user_addresses.booking_person_name')
		->get();
        
		 for ($i = 0, $c = count($review); $i < $c; ++$i) {
            $review[$i] = (array) $review[$i];
        }
		
        if(count($review)>0){
            $json['success']=1;
            $json['id']= $review[0]['id'];
            $json['user_id']=$review[0]['user_id'];
            $json['rest_id']=$review[0]['rest_id'];
            $json['order_id']=$review[0]['order_id'];
            $json['qrating']=$review[0]['quality_rating'];
            $json['srating']=$review[0]['service_rating'];
			$json['description']=$review[0]['comment'];
			$json['booking_person_name'] = $review[0]['booking_person_name'];
			$json['rest_name'] = $review[0]['f_name'].' '.$review[0]['l_name'];
			
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
}
