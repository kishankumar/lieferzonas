<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\RestDayTimeMap;
use App\superadmin\Rest_day;
use App\superadmin\Rest_detail;
use Validator;
use Auth;

class DaytimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$restaurantid=0)
    {
        $count= Rest_detail:: where('id','=',$restaurantid)->where('status', '!=','2')->count();
        if( ! preg_match('/^\d+$/', $restaurantid) ){
            return redirect('root/day/time/search');
        }
        if(!$count){
            return redirect('root/day/time/search');
        }
        $days = Rest_day:: where('status', '!=','2')->orderBy('id', 'asc')->get();
        $restInfo= Rest_detail:: where('id','=',$restaurantid)->where('status', '!=','2')->get();
        
        $fromtime = array();$totime = array();$toTime=1;
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            $fromtime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            $fromtime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            if($toTime>1){
                $totime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            }$toTime=2;
            $totime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        $totime['11:59 PM'] = '11:59 PM';
        
        return view('superadmin.daytime.index', ['restInfo'=>$restInfo,'restaurantid' => $restaurantid,'days' => $days,'fromtime' => $fromtime,'totime' => $totime]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(){
        $restInfo= Rest_detail:: where('status', '=','1')->orderBy('f_name', 'asc')->get();
        $restInfo = $restInfo->toArray();
        $drop[''] = '-Please Select Restaurant-';
        foreach($restInfo as $roles){
            $drop[$roles['id']] = ucfirst($roles['f_name']).' '.ucfirst($roles['l_name']);
        }
        $restaurant='';
        return view('superadmin.daytime.daytimesetting')->with('restInfo',$drop)->with('restaurantid',0)
		->with('restdetail',$restaurant); 
    }
    
    public function search(Request $request,$id=0){
        $restaurantid = $request->restaurantid;
//        if(!$restaurantid){
//            Session::flash('message', 'Please Select Restaurant!');
//        }
        $restInfo= Rest_detail:: where('status', '=','1')->orderBy('f_name', 'asc')->get();
        $restInfo = $restInfo->toArray();
        $drop[''] = '-Please Select Restaurant-';
        foreach($restInfo as $roles){
            $drop[$roles['id']] = ucfirst($roles['f_name']).' '.ucfirst($roles['l_name']);
        }
        $restaurant =array();
        $restaurant= Rest_detail:: where('id', '=',$restaurantid)->orderBy('f_name', 'asc')->get();
        if($restaurantid){
            $restaurant = DB::table('rest_details')
                ->join('rest_day_time_maps', 'rest_details.id', '=', 'rest_day_time_maps.rest_detail_id')
                ->join('rest_days', 'rest_days.id', '=', 'rest_day_time_maps.day_id')
                ->where('rest_details.status','!=', '2')
                ->where('rest_details.id','=', $restaurantid)
                //->orderBy('rest_days.id', 'asc')
                ->orderBy('rest_day_time_maps.id', 'asc')
                ->select('rest_details.*', 'rest_day_time_maps.day_id','rest_day_time_maps.open_time','rest_day_time_maps.close_time','rest_day_time_maps.is_open', 'rest_day_time_maps.is_24', 'rest_days.day')
                ->get();
        }
        return view('superadmin.daytime.daytimesetting')->with('restInfo',$drop)
		->with('restaurantid',$restaurantid)->with('restdetail',$restaurant); 
    }

//    public function create()
//    {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $messages=array(
            'restaurant_id.required'=>"Restaurant Not Selected | Choose Restautrant",
        );
        $this->validate($request,[
            'restaurant_id'=>'required|integer'
        ],$messages);
        //dd($request);die;
        $days = Rest_day:: where('status', '!=','2')->orderBy('id', 'asc')->select('id', 'day')->get();
        foreach($days  as $dayid){
            $dyid[] = ucfirst($dayid->day);
        }
        $restaurantid = $request->restaurant_id;
        
        $type11 = RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->delete();
        
        foreach($days  as $dayid){
            $dayStatus = 'days_'.ucfirst($dayid->day);
            if($request->$dayStatus){
                $isOpen = 'is_open_'.ucfirst($dayid->day);
                $created_at=date("Y-m-d H:i:s");$updated_at=date("Y-m-d H:i:s");
                $created_by=Auth::User('user')->id;$updated_by=Auth::User('user')->id;
                
                if($request->$isOpen){
                    $day_id = $dayid->id;
                    $open_time = '00:00 AM';
                    $close_time = '11:59 PM';
                    $is_open = 1;
                    $is_24 = 1;
                    $count= RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->count();
                    if($count){
                        $type=RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->delete();
                    }else{
                        $type= new RestDayTimeMap;
                        $type->created_at=$created_at;
                    }
                    $type->rest_detail_id=$restaurantid;
                    $type->day_id=$day_id;
                    $type->open_time=$open_time;
                    $type->close_time=$close_time;
                    $type->is_open=$is_open;
                    $type->is_24=$is_24;
                    $type->created_by=$created_by;
                    $type->updated_by=$updated_by;
                    $type->updated_at=$updated_at;
                    $type->status=1;
                    $type->save();
                }else{
                    $StartTime = 'from_'.ucfirst($dayid->day);
                    $EndTime = 'to_'.ucfirst($dayid->day);
                    $day_id = $dayid->id;
                    $open_time = $request->$StartTime;
                    $close_time = $request->$EndTime;
                    $is_open = 1;
                    $is_24 = 0;
                    
                    $count= RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->count();
                    if($count){
                        $type1=RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->delete();
                    }
                    foreach($open_time as $key=>$value){
                        foreach($close_time as $key1=>$value1){
                            if($key == $key1){
                                $type= new RestDayTimeMap;
                                $type->created_at=$created_at;
                                $type->rest_detail_id=$restaurantid;
                                $type->day_id=$day_id;
                                $type->open_time=$value;
                                $type->close_time=$value1;
                                $type->is_open=$is_open;
                                $type->is_24=$is_24;
                                $type->created_by=$created_by;
                                $type->updated_by=$updated_by;
                                $type->updated_at=$updated_at;
                                $type->status=1;
                                $type->save();
                            }else{
                                continue;
                            }
                        }
                    }
                }
            }else{
                $type= new RestDayTimeMap;
                $type->rest_detail_id=$restaurantid;
                $type->created_at=date("Y-m-d H:i:s");$type->updated_at=date("Y-m-d H:i:s");
                $type->created_by=Auth::User('user')->id;$type->updated_by=Auth::User('user')->id;
                $type->open_time='';
                $type->close_time='';
                $type->is_24=0;
                $type->day_id=$dayid->id;$type->status=1;
                $type->is_open=0;
                $type->save();
            }
        }
        Session::flash('message', 'Day time added Successfully.'); 
        //return redirect()->route('root.day.time.add-time');
        //return redirect('root/day/time/add-time/'.$request->restaurant_id);
        return redirect('root/day/time/search');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $count= Rest_detail:: where('id','=',$id)->where('status', '!=','2')->count();
        if( ! preg_match('/^\d+$/', $id) ){
            return redirect('root/day/time/search');
        }
        if(!$count){
            return redirect('root/day/time/search');
        }
        $count1= RestDayTimeMap:: where('rest_detail_id','=',$id)->where('status', '!=','2')->count();
        if(!$count1){
            return redirect('root/day/time/search');
        }
        
        $fromtime = array();$totime = array();$toTime=1;
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            $fromtime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            $fromtime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            if($toTime>1){
                $totime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            }$toTime=2;
            $totime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        $totime['11:59 PM'] = '11:59 PM';
        
        $days = Rest_day:: where('status', '!=','2')->orderBy('id', 'asc')->get();
        $restInfo= Rest_detail:: where('id','=',$id)->where('status', '!=','2')->get();
        
        $restaurant = DB::table('rest_details')
                ->join('rest_day_time_maps', 'rest_details.id', '=', 'rest_day_time_maps.rest_detail_id')
                ->join('rest_days', 'rest_days.id', '=', 'rest_day_time_maps.day_id')
                ->where('rest_details.status','!=', '2')
                ->where('rest_day_time_maps.is_open','=', '1')
                ->where('rest_details.id','=', $id)
                //->orderBy('rest_days.id', 'asc')
                ->orderBy('rest_day_time_maps.id', 'asc')
                ->select('rest_details.id', 'rest_day_time_maps.day_id','rest_day_time_maps.open_time','rest_day_time_maps.close_time','rest_day_time_maps.is_open', 'rest_day_time_maps.is_24', 'rest_days.day')
                ->get();
        //echo '<pre>';print_r($restaurant);die;
        $auth_id = Auth::User()->id;
        $username = DB::table('user_masters')->select('first_name','profile_pic')->where('user_id','=',$auth_id)->get();
        for ($i = 0, $c = count($username); $i < $c; ++$i) {
            $username[$i] = (array) $username[$i];
        }
        return view('superadmin.daytime.edit', ['restTimings'=>$restaurant,'restInfo'=>$restInfo,'restaurantid' => $id,'days' => $days,'fromtime' => $fromtime,'totime' => $totime])->with('username',$username);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages=array(
            'restaurant_id.required'=>"Restaurant Not Selected | Choose Restautrant",
        );
        $this->validate($request,[
            'restaurant_id'=>'required|integer'
        ],$messages);
        
        $restaurantid = $id;
        $days = Rest_day:: where('status', '!=','2')->orderBy('id', 'asc')->select('id', 'day')->get();
        $type11 = RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->delete();
        
        foreach($days  as $dayid){
            $dayStatus = 'days_'.ucfirst($dayid->day);
            if($request->$dayStatus){
                $isOpen = 'is_open_'.ucfirst($dayid->day);
                $created_at=date("Y-m-d H:i:s");$updated_at=date("Y-m-d H:i:s");
                $created_by=Auth::User('user')->id;$updated_by=Auth::User('user')->id;
                
                if($request->$isOpen){
                    $day_id = $dayid->id;
                    $open_time = '00:00 AM';
                    $close_time = '11:59 PM';
                    $is_open = 1;
                    $is_24 = 1;
                    $count= RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->count();
                    if($count){
                        $type=RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->delete();
                    }else{
                        $type= new RestDayTimeMap;
                        $type->created_at=$created_at;
                    }
                    $type->rest_detail_id=$restaurantid;
                    $type->day_id=$day_id;
                    $type->open_time=$open_time;
                    $type->close_time=$close_time;
                    $type->is_open=$is_open;
                    $type->is_24=$is_24;
                    $type->created_by=$created_by;
                    $type->updated_by=$updated_by;
                    $type->updated_at=$updated_at;
                    $type->status=1;
                    $type->save();
                }else{
                    $StartTime = 'from_'.ucfirst($dayid->day);
                    $EndTime = 'to_'.ucfirst($dayid->day);
                    $day_id = $dayid->id;
                    $open_time = $request->$StartTime;
                    $close_time = $request->$EndTime;
                    $is_open = 1;
                    $is_24 = 0;
                    $count= RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->count();
                    if($count){
                        $type1=RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->delete();
                    }
                    foreach($open_time as $key=>$value){
                        foreach($close_time as $key1=>$value1){
                            if($key == $key1){
                                $type= new RestDayTimeMap;
                                $type->created_at=$created_at;
                                $type->rest_detail_id=$restaurantid;
                                $type->day_id=$day_id;
                                $type->open_time=$value;
                                $type->close_time=$value1;
                                $type->is_open=$is_open;
                                $type->is_24=$is_24;
                                $type->created_by=$created_by;
                                $type->updated_by=$updated_by;
                                $type->updated_at=$updated_at;
                                $type->status=1;
                                $type->save();
                            }else{
                                continue;
                            }
                        }
                    }
                }
            }else{
                $countNo= RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->count();
                if($countNo){
                    $type=RestDayTimeMap:: where('rest_detail_id','=',$restaurantid)->where('day_id', '=',$dayid->id)->first();
                }else{
                    $type= new RestDayTimeMap;
                    $type->created_at=date("Y-m-d H:i:s");
                }
                $type->rest_detail_id=$restaurantid;
                $type->updated_at=date("Y-m-d H:i:s");
                $type->created_by=Auth::User('user')->id;$type->updated_by=Auth::User('user')->id;
                $type->open_time='';
                $type->close_time='';
                $type->is_24=0;
                $type->day_id=$dayid->id;$type->status=1;
                $type->is_open=0;
                $type->save();
            }
        }
        Session::flash('message', 'Day time updated Successfully.');
        return redirect('root/day/time/search');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
