<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\superadmin\RestDetail;
use DB;
use Auth;
use Validator;

class KitchenMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = RestDetail::where('status','=',1);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('f_name', 'LIKE', "%$search%");
                $q2->orWhere('l_name', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('f_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('f_name', 'desc');
        }else{
            $q->orderBy('f_name', 'asc');
        }
        $data['restro_names']=$q->paginate(10);
        $data['search']=$search;
        $data['type']=$type;
        $data['pages']=$data['restro_names']->toarray($data['restro_names']);
        
        return view('superadmin/restaurants/kitchenmap/index')->with($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $current_date = date('Y-m-d');
        $data['restro_names'] = DB::table('rest_details')->orderBy('f_name', 'asc')->where('status',1)->select('id','f_name','l_name')->get();
        $data['kitchens']=DB:: table('kitchens')->where('status',1)->where('valid_from', '<=',$current_date)
		->where('valid_to' ,'>',$current_date)->get();
        $data['restro_detail_id']=$request->restaurant;
        $data['kitchen_maps']=DB:: table('rest_kitchen_maps')->where('rest_detail_id',$request->restaurant)->lists('kitchen_id');
        
        return view('superadmin/restaurants/kitchenmap/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rest_detail_id=$request->restaurant;
        $total_kitchen_selected=count($request->kitchen);
        $kitchens=$request->kitchen;

        $exist_restro = DB::table('rest_kitchen_maps')->where('rest_detail_id',$rest_detail_id)->count();
        
        if($exist_restro>0){

            DB::beginTransaction();

           $deleted= DB::table('rest_kitchen_maps')->where('rest_detail_id',$rest_detail_id)->delete();


           for ($i=0; $i <$total_kitchen_selected ; $i++) { 

               DB::table('rest_kitchen_maps')->insert(
                
                ['rest_detail_id'=> $rest_detail_id,'kitchen_id' =>  $kitchens[$i],'created_by' =>  Auth::User('user')->id,'updated_by' =>  Auth::User('user')->id,'created_at' =>  date('Y-m-d H:i:s'),'updated_at' =>  date('Y-m-d H:i:s') ]
               );
            }
            
            if($deleted and ($i==$total_kitchen_selected)){
                DB::commit();
            }
            else{
                DB::rollBack();
            }

        }
        else{
           
            DB::beginTransaction();

            for ($i=0; $i <$total_kitchen_selected ; $i++) { 

               DB::table('rest_kitchen_maps')->insert(

                ['rest_detail_id'=> $rest_detail_id,'kitchen_id' =>  $kitchens[$i],'created_by' =>  Auth::User('user')->id,'updated_by' =>  Auth::User('user')->id,'created_at' =>  date('Y-m-d H:i:s'),'updated_at' =>  date('Y-m-d H:i:s') ]
               );
            }

            if($i==$total_kitchen_selected){
                DB::commit();
            }
            else{
                DB::rollBack();
            }


        }
        return redirect(route('root.kitchenmap.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
