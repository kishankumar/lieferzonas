<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use App\front\UserRestFavourite;

class UserFavouriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		
		$favlist= DB ::table('user_rest_favourites')
		->leftjoin('rest_details', 'user_rest_favourites.rest_id', '=', 'rest_details.id')
		->leftjoin('front_user_details', 'user_rest_favourites.user_id', '=', 'front_user_details.front_user_id')
		->groupBy('user_rest_favourites.rest_id')
		->select(DB::raw('lie_user_rest_favourites.rest_id,count(lie_user_rest_favourites.rest_id) as user_count,lie_rest_details.f_name,lie_rest_details.l_name'))
		
		->paginate(10);
		/*$favlist= DB ::table('user_rest_favourites')
		
		->groupBy('rest_id')
		->select(DB::raw('count(rest_id) as user_count, rest_id'))
		->paginate(10);*/
		
        return view('superadmin/userbonus/favourites')->with('favlist',$favlist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function showuser()
	{
		$rest_id = Input::get('rest_id');
		$showuser= DB ::table('user_rest_favourites')
		->leftjoin('rest_details', 'user_rest_favourites.rest_id', '=', 'rest_details.id')
		->leftjoin('front_user_details', 'user_rest_favourites.user_id', '=', 'front_user_details.front_user_id')
		->where('user_rest_favourites.rest_id','=',$rest_id)
		->select('front_user_details.fname','front_user_details.lname')
		->paginate(10);
		//print_r($showuser); die;
		$data= '';
		if(count($showuser)>0)
		{
			foreach($showuser as $showusers)
			{
				$data.= "<div class='col-md-4'>".$showusers->fname.' '.$showusers->lname."</div>";
			}
            $json['success']=1;
            $sendata['data']=$data;
			$sendata['success']=1;
            print_r(json_encode($sendata));
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		
	}
}
