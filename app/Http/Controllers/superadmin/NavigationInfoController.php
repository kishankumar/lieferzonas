<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Validator;
use Input;
use App\superadmin\NavigationInfo;

class NavigationInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = NavigationInfo::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('title', 'LIKE', "%$search%");
                $q2->orWhere('link', 'LIKE', "%$search%");
            });
        }
        if($type=='title'){
            $q->orderBy('title', 'asc');
        }elseif($type=='title-desc'){
            $q->orderBy('title', 'desc');
        }elseif($type=='link-desc'){
            $q->orderBy('link', 'desc');
        }elseif($type=='link'){
            $q->orderBy('link', 'asc');
        }else{
            $q->orderBy('title', 'asc');
        }
        $infoDetail = $q->paginate(10);
        return view('superadmin.navigationInfo.index')->with('infoDetail', $infoDetail)->with('search',$search)->with('type',$type);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        
        $messages=array('title.unique'=>'Title Already Exists.');
        $userData = array(
            'title'      => $data['title'],
            'link'     =>  $data['link']
        );
        $title_id = Input::get('title_id');
        if($title_id){
            $rules = array(
                'title'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'link'     =>  'required'
            );
        }else{
            $rules = array(
                'title'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'link'     =>  'required',
            );
        }
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($title_id){
            $count= NavigationInfo:: where('id','!=',$title_id)->where('title', '=',$request->title)->where('status', '!=','2')->count();
        }else{
            $count= NavigationInfo:: where('title', '=',$request->title)->where('status', '!=','2')->count();
        }
        if($count){
            $errors = array('name1'=>'Title has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($title_id){
            $type=NavigationInfo::find($title_id);
        }else{
            $type= new NavigationInfo;
            $type->created_at=date("Y-m-d H:i:s");
            $type->created_by=Auth::User('user')->id;
        }
        $type->title=$request->title;
        $type->link=$request->link;
        $type->updated_by=Auth::User('user')->id;
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=$request->status;
        $type->save();
        if($title_id){
            Session::flash('message', 'Information updated Successfully!'); 
            $json['success']=1;
            $json['message']='Information updated Successfully.';
        }else{
            Session::flash('message', 'Information added Successfully!'); 
            $json['success']=1;
            $json['message']='Information added Successfully.';
        }
        echo json_encode($json);
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedInfo = $data['checkedInfo'];
        $data['infor']=NavigationInfo::find($selectedInfo);
        $currencies = $data['infor']->toArray();
        if(count($currencies)>0){
            $json['success']=1;
            $json['id']=$currencies['id'];
            $json['title']=$currencies['title'];
            $json['link']=$currencies['link'];
            $json['status']=$currencies['status'];
            echo json_encode($json);
            return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCurrency = $data['selectedCurrency'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedCurrency as $key => $value)
        {
            $user = NavigationInfo::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
}
