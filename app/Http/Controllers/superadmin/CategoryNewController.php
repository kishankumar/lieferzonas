<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\MenuCategory;
use App\superadmin\RestCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = null;
        if ($request->hasFile("file")) {
            if (!$request->file("file")->isValid()) {
                return response()->json([
                    'status' => 'failure',
                    'reason' => 'Invalid image uploaded'
                ]);
            }
            $extension = pathinfo($request->file("file")->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = uniqid() . "." . $extension;
            $request->file('file')->move("uploads/superadmin/category/", $filename);
        }
        $new_category = new RestCategory;
        $new_category->rest_detail_id = $request->input("restaurant");
        $new_category->name = $request->input("name");
        $new_category->root_cat_id = $request->input("root");
        $new_category->description = $request->input("description");
        $new_category->valid_from = $request->input("validFrom");
        $new_category->valid_to = $request->input("validTo");
        $new_category->image = $filename;
        $new_category->save();
        return response()->json([
            "status" => "success"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
