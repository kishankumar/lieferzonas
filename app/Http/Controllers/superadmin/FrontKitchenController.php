<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\FrontKitchenMap;
use App\superadmin\Kitchen;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class FrontKitchenController extends Controller
{
    public function index()
    {
        $data['map'] = FrontKitchenMap::where('status','!=',2)->orderBy('priority','asc')->get();
        $data['kitchen'] = Kitchen::where('status','=',1)->OrderBy('name','asc')->lists('name', 'id');
        $data['kitchen']->prepend('Select Kitchen', '');
        
        return view('superadmin/homepagelist/kitchen')->with('data',$data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        for($i=1;$i<=10;$i++)
        {
            $state = FrontKitchenMap::where('priority','=',$request['priority'.$i])->first();
            $state->kitchen_id = $request['kitchen'.$i];
            $state->save();
        }

        return redirect(route('root.kitchenfrontlist.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
