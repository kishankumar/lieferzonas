<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\superadmin\RestDetail;
use App\superadmin\MenuExtra;
use DB;
use Auth;
use Validator;

class RestExtraMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = RestDetail::where('status','=',1);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('f_name', 'LIKE', "%$search%");
                $q2->orWhere('l_name', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('f_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('f_name', 'desc');
        }else{
            $q->orderBy('f_name', 'asc');
        }
        $data['restro_names']=$q->paginate(10);
        $data['search']=$search;
        $data['type']=$type;
        
        $data['pages']=$data['restro_names']->toarray($data['restro_names']);
        
        return view('superadmin/restaurants/extras_map/index')->with($data);

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['restro_names'] = DB::table('rest_details')->orderBy('f_name', 'asc')->where('status',1)->select('id','f_name','l_name')->get();
        //$data['extras']=DB:: table('rest_extra_maps')->where('status',1)->get();
        $data['restro_detail_id']=$request->restaurant;
        $data['extrasMaps']=DB:: table('rest_extra_maps')->where('rest_detail_id',$request->restaurant)->lists('menu_extra_id');

        $data['extraTypeMaps']= DB::table('rest_extra_type_maps')
            ->join('menu_extra_types', 'rest_extra_type_maps.menu_extra_type_id', '=', 'menu_extra_types.id')
            ->select('menu_extra_types.id','menu_extra_types.name')
            ->where('rest_extra_type_maps.rest_detail_id',$data['restro_detail_id'])
            ->where('menu_extra_types.status','1')
            ->get();
        
        return view('superadmin/restaurants/extras_map/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rest_detail_id=$request->restaurant;
        $total_extra_selected=$request->total_value;

        $exist_restro = DB::table('rest_extra_maps')->where('rest_detail_id',$rest_detail_id)->count();

        if($exist_restro>0){

            DB::beginTransaction();

           $deleted= DB::table('rest_extra_maps')->where('rest_detail_id',$rest_detail_id)->delete();


           for ($i=1; $i <$total_extra_selected ; $i++) { 
            
                $extra_type=$request->input('extra_type'.$i);
                $extras=$request->input('extras'.$i);

                if(!$extras){
                    continue;
                }
                
                foreach ($extras as $extra => $extrasValue) { 

                    DB::table('rest_extra_maps')->insert(
                        
                        ['rest_detail_id'=> $rest_detail_id,'menu_extra_type_id' =>  $extra_type ,'menu_extra_id' =>  $extrasValue,'created_by' =>  Auth::User('user')->id,'updated_by' =>  Auth::User('user')->id,'created_at' =>  date('Y-m-d H:i:s'),'updated_at' =>  date('Y-m-d H:i:s')]
                       );
                }
            }
            
            if($deleted and ($i==$total_extra_selected)){
                DB::commit();
            }
            else{
                DB::rollBack();
            }

        }

        else{
           
            DB::beginTransaction();

            for ($i=1; $i <$total_extra_selected ; $i++) { 
            

                $extra_type=$request->input('extra_type'.$i);
                $extras=$request->input('extras'.$i);
                if(!$extras){
                    continue;
                }
                /*print_r($extras);
                exit();*/
                foreach ($extras as $extra => $extrasValue) {

                    DB::table('rest_extra_maps')->insert(
                        
                        ['rest_detail_id'=> $rest_detail_id,'menu_extra_type_id' =>  $extra_type ,'menu_extra_id' =>  $extrasValue,'created_by' =>  Auth::User('user')->id,'updated_by' =>  Auth::User('user')->id,'created_at' =>  date('Y-m-d H:i:s'),'updated_at' =>  date('Y-m-d H:i:s')]
                       );
                }
            }

            if($i==$total_extra_selected){
                DB::commit();
            }
            else{
                DB::rollBack();
            }

        }

        return redirect(route('root.extramap.index'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
