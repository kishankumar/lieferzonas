<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use App\superadmin\RestCategory;
use App\superadmin\RestDetail;
use App\superadmin\RestRootCatMap;
use Session;
use DB;
use Auth;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['restro_detail_id']=$request->restaurant;
        $data['restro_names'] = RestDetail::where('status',1)->select('id','f_name','l_name')->get();

        $data['getRestroCategories'] = RestCategory::where('status','!=',2)->where('rest_detail_id',$data['restro_detail_id'])->select('id','rest_detail_id','root_cat_id','name','description','image','valid_from','valid_to','status')->paginate(20);
        
		
        return view('superadmin/restaurants/category/index')->with($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['restro_names'] = DB::table('rest_details')->where('status',1)->select('id','f_name','l_name')->get();

        return view('superadmin/restaurants/category/create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $values=RestCategory::where('status','!=',2)->where('rest_detail_id',$request->restro_name)->where('name',$request->cat_name)->select('id')->get();
       
        if(count($values)>0){

            Session::flash('category_error', 'This category is already exist for this restaurant');
            return redirect(route('root.category.create'));
        }
        else{


         $this->set_validation('',$request);
         $restaurant_categories= new RestCategory;
         //dd($request);
         ///////////File Upload////////////////////////////

        $destinationpath=public_path()."/uploads/superadmin/category";
        $pic=Input::file('file');
        if($pic){
        $extension=  $pic->getClientOriginalExtension(); // getting image extension
        $filename=time().rand(111,999).'.'.$extension; // renameing image
        $pic->move($destinationpath,$filename);
        }
        else{
            $filename='';
        }
         ////////////////Add Basic Information////////////////
         $restaurant_categories->rest_detail_id=$request->restro_name;
         $restaurant_categories->root_cat_id=$request->root_cat_name;
         $restaurant_categories->name=$request->cat_name;
         $restaurant_categories->description=$request->description;
         $restaurant_categories->image=$filename;
         $restaurant_categories->valid_from=date('Y-m-d', strtotime(str_replace('/', '-', $request->valid_from)));
         $restaurant_categories->valid_to=date('Y-m-d', strtotime(str_replace('/', '-', $request->valid_to)));
         $restaurant_categories->created_by=Auth::User()->id;
         $restaurant_categories->updated_by=Auth::User()->id;
       
         
         $restaurant_categories->save();

         return redirect(route('root.category.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['restro_names'] = DB::table('rest_details')->where('status',1)->select('id','f_name','l_name')->get();
        $data['categories']=RestCategory::find($id);
        $restro_id=$data['categories']['rest_detail_id'];

        $data['rootCategories'] = RestRootCatMap:: where('rest_detail_id',$restro_id)->where('status',1)->select('id','root_cat_id')->get();
        /*dd($data['rootCategories']);
        exit();*/

        return view('superadmin/restaurants/category/edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $values=RestCategory::where('status','!=',2)->where('rest_detail_id',$request->restro_name)->where('name',$request->cat_name)->where('id','!=',$id)->select('id')->get();
        
        if(count($values)>0){

            //$request->session()->flash('category_error', 'This category is already exist for this category');
            Session::flash('category_error', 'This category is already exist for this restaurant');
            //return view('superadmin/restaurants/category/create');
            return redirect(route('root.category.edit'));
        }
        else{


         $this->set_validation('',$request);
         $restaurant_categories= RestCategory:: find($id);
         //dd($request);
         ///////////File Upload////////////////////////////

        $destinationpath=public_path()."/uploads/superadmin/category";
        $pic=Input::file('file');
        if($pic){
        $extension=  $pic->getClientOriginalExtension(); // getting image extension
        $filename=time().rand(111,999).'.'.$extension; // renameing image
        $pic->move($destinationpath,$filename);
        $restaurant_categories->image=$filename;
        }
        

         ////////////////Edit Information////////////////
         $restaurant_categories->rest_detail_id=$request->restro_name;
         $restaurant_categories->root_cat_id=$request->root_cat_name;
         $restaurant_categories->name=$request->cat_name;
         $restaurant_categories->description=$request->description;
         
         $restaurant_categories->valid_from=date('Y-m-d', strtotime(str_replace('/', '-', $request->valid_from)));
         $restaurant_categories->status=$request->status;
         $restaurant_categories->valid_to=date('Y-m-d', strtotime(str_replace('/', '-', $request->valid_to)));
         $restaurant_categories->created_by=Auth::User()->id;
         $restaurant_categories->updated_by=Auth::User()->id;
       
         
         $restaurant_categories->save();

         return redirect(route('root.category.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
         if($request->ajax()){
            $data = Input::all();
            $selectedCategories = $data['selectedCategories'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Resturant</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Root Category</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Category</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Valid From</th>
						<th style="border:solid 1px #81C02F; padding:10px">Valid To</th>
						<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedCategories as $key => $value)
        {
            
             $data = DB::table('rest_categories')
			 ->leftjoin('rest_details','rest_categories.rest_detail_id','=','rest_details.id')
			 ->leftjoin('root_categories' ,'rest_categories.root_cat_id','=','root_categories.id')
			
			 ->where('rest_categories.id','=',$value)
			->select('rest_categories.*','rest_details.f_name','rest_details.l_name',
			'root_categories.name as rootcatname')->get();
			for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		    } 
			
            
			  
		      
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				$valid_from = date("Y-m-d",strtotime($data['0']['valid_from']));
				$valid_to = date("Y-m-d",strtotime($data['0']['valid_to']));
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['f_name'].' '.$data['0']['l_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['rootcatname'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_from.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_to.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/

    /////Add validation////////
     public function set_validation($id=null,$request)
     {
        $message=array(
            "restro_name.required"=>"Select restaurant name",
            "root_cat_name.required"=>"Select root category name",
            "cat_name.required"=>"Enter valid category",
            "valid_from.email"=>"Enter valid from date",
            "valid_to.required"=>"Enter valid to date",
            );

        $this->validate($request,[
            'restro_name'=> 'required',
            'root_cat_name'=> 'required',
            'cat_name'=> 'required',
            'valid_from'=> 'required|date_format:"d/m/Y"',
            'valid_to'=> 'required|date_format:"d/m/Y"',
        ],$message);
     }
}
