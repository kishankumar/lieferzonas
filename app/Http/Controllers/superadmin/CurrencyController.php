<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\Currencie;
use Validator;
use Auth;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = Currencie::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('currency_code', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='code-desc'){
            $q->orderBy('currency_code', 'desc');
        }elseif($type=='code'){
            $q->orderBy('currency_code', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $currencie = $q->paginate(10);
        
        //$currencie = Currencie::where('status', '!=', '2')->orderBy('name', 'desc')->paginate(10);
        return view('superadmin.currency.index')->with('currencie', $currencie)->with('search',$search)->with('type',$type);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = Input::get('name');//for get input field value
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        
        $messages=array('name.unique'=>'Currency Name Already Exists.');
        $userData = array(
            'name'      => $data['name'],
            'currency_code'     =>  $data['currency_code']
        );
        
        $currency_id = Input::get('currency_id');
        if($currency_id){
            $rules = array(
                //'name'      =>  'required|min:3|Regex:/(^[a-zA-Z ]+$)+/|unique:currencies,name,'.$currency_id,
                'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'currency_code'     =>  'required|min:3'
            );
        }else{
            $rules = array(
//                'name'      =>  'required|min:3|unique:currencies|Regex:/(^[a-zA-Z ]+$)+/',
                'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'currency_code'     =>  'required|min:3',
            );
        }
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            //$errors = $validation->errors();
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($currency_id){
            $count= Currencie:: where('id','!=',$currency_id)->where('name', '=',$request->name)->where('status', '!=','2')->count();
        }else{
            $count= Currencie:: where('name', '=',$request->name)->where('status', '!=','2')->count();
        }
        if($count){
            $errors = array('name1'=>'Currency name has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($currency_id){
            $type=Currencie::find($currency_id);
        }else{
            $type= new Currencie;
            $type->created_at=date("Y-m-d H:i:s");
            $type->created_by=Auth::User('user')->id;
        }
        $type->name=$request->name;
        $type->currency_code=$request->currency_code;
        $type->updated_by=Auth::User('user')->id;
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=$request->status;
        $type->save();
        if($currency_id){
            Session::flash('message', 'Currency updated Successfully!'); 
            $json['success']=1;
            $json['message']='Currency updated Successfully.';
        }else{
            Session::flash('message', 'Currency added Successfully!'); 
            $json['success']=1;
            $json['message']='Currency added Successfully.';
        }
        echo json_encode($json);
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedCurrency = $data['checkedCurrency'];
        
        $data['currency']=Currencie::find($selectedCurrency);
        $currencies = $data['currency']->toArray();
        if(count($currencies)>0){
            $json['success']=1;
            $json['id']=$currencies['id'];
            $json['name']=$currencies['name'];
            $json['currency_code']=$currencies['currency_code'];
            $json['status']=$currencies['status'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCurrency = $data['selectedCurrency'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedCurrency as $key => $value)
        {
            $user = Currencie::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCurrency = $data['selectedCurrency'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Currency</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Currency Code</th>
                       
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedCurrency as $key => $value)
        {
             $data = DB::table('currencies')
			
			->where('id','=',$value)
			->select('*')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['currency_code'].'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
