<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\ShopItem;
use App\superadmin\ShopCategory;
use App\superadmin\ShopInventory;
use Validator;use Auth;
use Illuminate\Support\Str;

class ShopItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = ShopItem::where('status','!=','2');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('item_number', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='inumber-desc'){
            $q->orderBy('item_number', 'desc');
        }elseif($type=='inumber'){
            $q->orderBy('item_number', 'asc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='price-desc'){
            $q->orderBy('price', 'desc');
        }elseif($type=='price'){
            $q->orderBy('price', 'asc');
        }elseif($type=='fprice-desc'){
            $q->orderBy('front_price', 'desc');
        }elseif($type=='fprice'){
            $q->orderBy('front_price', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $data['ItemInfo']=$q->paginate(10);
        $data['type']=$type;
        $data['search']=$search;
        
		
        return view('superadmin.ShopManagement.items.index')->with($data); 

    }
    
    function search(Request $request,$item_id=0,$category_id=0){
        if($item_id){
            if( ! preg_match('/^\d+$/',$item_id) ){
                Session::flash('message', 'Please select item details again.'); 
                return redirect(route('root.shop.items.index'));
            }
        }
        $toDate = date('Y-m-d');
        $fromDate = date("Y-m-d", strtotime("-1 months"));
        
        $data['to_date']=date('m/d/Y');
        $data['from_date']=date("m/d/Y", strtotime("-1 months"));
        if($request->from){
            $fromDate = date("Y-m-d", strtotime($request->from));
            $data['from_date']=date("m/d/Y", strtotime($request->from));
        }
        if($request->to){
            $toDate = date("Y-m-d", strtotime($request->to));
            $data['to_date']=date("m/d/Y", strtotime($request->to));
        }
        //$data['InventoryInfo']= ShopInventory:: where('status','!=','2')->whereBetween('created_at', array($fromDate, $toDate))->where('shop_item_id',$item_id)->orderBy('created_at','desc')->paginate(10);
        $data['InventoryInfo']= ShopInventory:: where('status','!=','2')
            ->where(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d")'),'>=',$fromDate)
            ->where(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d")'),'<=',$toDate)
            ->where('shop_item_id',$item_id)
            ->orderBy('created_at','desc')
            ->paginate(10);
        $data['pages']=$data['InventoryInfo']->toarray($data['InventoryInfo']);
        $data['category_id']=$category_id;
        $data['item_id']=$item_id;
		
        return view('superadmin/ShopManagement/items/search')->with($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category_names'] = DB::table('shop_categories')->where('status',1)->orderBy('name', 'asc')->select('id','name')->get();

        return view('superadmin/ShopManagement/items/create')->with($data);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->set_validation('',$request);
        
        $object = new ShopItem;
        $object->shop_category_id=$request->category_id;
        $object->metric_id=$request->metric_id;
        $object->name=$request->item_name;
        $object->item_number=$request->item_number;
        $object->description=$request->description;
        $object->price=round($request->price,2);
        $object->front_price=round($request->front_price,2);
        $object->created_by=Auth::User('user')->id;
        $object->updated_by=Auth::User('user')->id;
        $object->created_at=date("Y-m-d H:i:s");
        $object->updated_at=date("Y-m-d H:i:s");
        $object->status=1;
        if($request->file('image')){
            $destinationPath=public_path()."/uploads/superadmin/shop/items"; // give path of directory where you want to save your files
            $filename = time().Str::random() . '.' . $request->image->getClientOriginalExtension();
            $upload_success = $request->image->move($destinationPath,$filename);
            $object->item_image=$filename;
        }
        $object->save();
        Session::flash('message', 'Item added Successfully!'); 
        return redirect(route('root.shop.items.index'));
    }
    
    public function set_validation($id=null,$request)
    {
        $message=array(
            "category_id.required"=>"Please select Category",
            "metric_id.required"=>"Please select Metric ",
            "item_name.item_exists"=>"Item Name already exists.",
            "item_number.item_number_exists"=>"Item number already exists.",
            'greater_than_zero' => "Price must be greater than 0.",
            'greater_than_price' => "Front price must be greater than price.",
            'image.required' => "Please upload image",
        );
        if($id){
            $rules = [
                'category_id'=> 'required|numeric',
                'metric_id'=> 'required|numeric',
                'item_name'=> 'required|min:3|item_exists',
                'item_number'=> 'required|min:3|item_number_exists',
                'price'=> 'required|numeric|greater_than_zero:price',
                'front_price'=> 'required|numeric|greater_than_price:price',
                'description'=> 'required|min:6',
            ];
            //if($request->image!='')
            //{
                //$rules['image']= 'mimes:png,jpeg,jpg,gif|max:10000'; 
            //}
            $this->validate($request,$rules,$message);
        }else{
            $rules = [
                'category_id'=> 'required|numeric',
                'metric_id'=> 'required|numeric',
                'item_name'=> 'required|min:3|item_exists',
                'item_number'=> 'required|min:3|item_number_exists',
                'price'=> 'required|numeric|greater_than_zero:price',
                'front_price'=> 'required|numeric|greater_than_price:price',
                'description'=> 'required|min:6',
                //'image'=>'mimes:png,jpeg,jpg,gif|required|max:10000'
            ];
            $this->validate($request,$rules,$message);
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category_names'] = DB::table('shop_categories')->where('status',1)->orderBy('name', 'asc')->select('id','name')->get();
        $data['item_detail']=ShopItem::find($id);

        return view('superadmin/ShopManagement/items/edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->set_validation($id,$request);
        
        $object=ShopItem :: find($id);
        $object->shop_category_id=$request->category_id;
        $object->metric_id=$request->metric_id;
        $object->name=$request->item_name;
        $object->item_number=$request->item_number;
        $object->description=$request->description;
        $object->price=round($request->price,2);
        $object->front_price=round($request->front_price,2);
        //$object->created_by=Auth::User('user')->id;
        $object->updated_by=Auth::User('user')->id;
        $object->created_at=date("Y-m-d H:i:s");
        $object->updated_at=date("Y-m-d H:i:s");
        $object->status=$request->status;
        if($request->file('image')){
            $destinationPath=public_path()."/uploads/superadmin/shop/items"; // give path of directory where you want to save your files
            $filename = time().Str::random() . '.' . $request->image->getClientOriginalExtension();
            $upload_success = $request->image->move($destinationPath,$filename);
            $object->item_image=$filename;
        }
        $object->save();
        Session::flash('message', 'Item updated Successfully!'); 
        return redirect(route('root.shop.items.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedItems = $data['selectedItems'];
        }else{
            $json['success']=0;
            $errors = array('name'=>'Please enter valid data.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        foreach($selectedItems as $key => $value)
        {
            $user = ShopItem::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
 /*added by Rajlakshmi(31-05-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedItems = $data['selectedItems'];
        }else{
            $json['success']=0;
            $errors = array('name'=>'Please enter valid data.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Item Name</th>
        <th style="border:solid 1px #81C02F; padding:10px">Item No.</th>
        <th style="border:solid 1px #81C02F; padding:10px">Description</th>		
		<th style="border:solid 1px #81C02F; padding:10px">Price</th>
		<th style="border:solid 1px #81C02F; padding:10px">Front Price</th>\
		<th style="border:solid 1px #81C02F; padding:10px">Category</th>
		<th style="border:solid 1px #81C02F; padding:10px">Metrics</th>
		<th style="border:solid 1px #81C02F; padding:10px">Quantity</th>
		<th style="border:solid 1px #81C02F; padding:10px">Status</th></tr></thead>';
        foreach($selectedItems as $key => $value)
        {
            $data = DB::table('shop_items')
            ->leftjoin('shop_categories', 'shop_items.shop_category_id', '=', 'shop_categories.id')
            ->leftjoin('metrics' , 'shop_items.metric_id', '=', 'metrics.id')
            ->where('shop_items.id','=', $value)
           
            ->select('shop_items.*','shop_categories.name as catname','metrics.name as metricsname')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}
               	if((strlen($data['description']) > 50))
				{
					$desc = substr($data['description'],0,50).'...';
				}
				else
				{
					$desc = $data['description'];
				}		
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['item_number'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$desc.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['price'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['front_price'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['catname'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['metricsname'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['quantity'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
}
