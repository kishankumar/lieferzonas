<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\Kitchen;
use Validator;use Auth;
//use Illuminate\Validation\Validator;

class KitchenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = Kitchen::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='priority-desc'){
            $q->orderBy('priority', 'desc');
        }elseif($type=='priority'){
            $q->orderBy('priority', 'asc');
        }elseif($type=='from-desc'){
            $q->orderBy('valid_from', 'desc');
        }elseif($type=='from'){
            $q->orderBy('valid_from', 'asc');
        }elseif($type=='to-desc'){
            $q->orderBy('valid_to', 'desc');
        }elseif($type=='to'){
            $q->orderBy('valid_to', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $data['kitcheninfo'] = $q->paginate(10);

        return view('superadmin.kitchens.kitchen')->with($data)->with('search',$search)->with('type',$type);

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        //$name = Input::get('name');//for get input field value
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages = array(
            'end_after' => "Valid to date should be equal or greater than current date",
            'check_date' => "Valid from should be equal or greater than current date",
            'check_valid_to_date' => "Valid to Date should be equal or greater than current date",
        );
        $userData = array(
            'name'      => $data['name'],
            'description'     =>  $data['description'],
            'priority'     =>  $data['priority'],
            'valid_from'  =>  $data['valid_from'],
            'valid_to' =>  $data[ 'valid_to'],
        );
        $rules = array(
            'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            'description'     =>  'required|min:6',
            'priority'     =>  'required|numeric',
            'valid_from'  =>  'required|date|date_format:m/d/Y|check_date:valid_from',
            'valid_to'  =>  'required|date|date_format:m/d/Y|end_after:valid_from',
        );
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        $count= Kitchen:: where('name', '=',$request->name)->where('status', '!=','2')->count();
        if($count){
            $errors = array('name1'=>'Kitchen name has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        $kitchen= new Kitchen;
        $kitchen->name=$request->name;
        $kitchen->description=$request->description;
        $kitchen->priority=$request->priority;
        $kitchen->valid_from = date("Y-m-d", strtotime($request->valid_from));
        $kitchen->valid_to = date("Y-m-d", strtotime($request->valid_to));
        $kitchen->created_by=Auth::User('user')->id;
        $kitchen->updated_by=Auth::User('user')->id;
        $kitchen->created_at=date("Y-m-d H:i:s");
        $kitchen->updated_at=date("Y-m-d H:i:s");
        $kitchen->status=$request->status;
        $kitchen->save();
        
        Session::flash('message', 'Kitchen added Successfully!'); 
        $json['success']=1;
        $json['message']='Kitchen added Successfully';
        echo json_encode($json);
        return;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedKitchens = $data['checkedKitchen'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['kitchens']=Kitchen::find($selectedKitchens);
        $kitchen = $data['kitchens']->toArray();
        if(count($kitchen)>0){
            $json['success']=1;
            $json['id']=$kitchen['id'];
            $json['name']=$kitchen['name'];
            $json['description']=$kitchen['description'];
            $json['priority']=$kitchen['priority'];
            $json['status']=$kitchen['status'];
            $json['valid_from']= date("m/d/Y", strtotime($kitchen['valid_from']));
            $json['valid_to']= date("m/d/Y", strtotime($kitchen['valid_to']));
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=2;
            $json['error']='Please select kitchen again';
            echo json_encode($json);
            return;
        }
        if($id ==0 || $id ==''){
            $json['success']=2;
            $json['error']='Please select kitchen again';
            echo json_encode($json);
            return;
        }
        $messages = array(
            'end_after' => "Valid to date should be equal or greater than current date",
            'check_date' => "Valid from should be equal or greater than current date",
            'check_valid_to_edit_date' => "Valid to Date should be equal or greater than current date",
        );
        $userData = array(
            'name'      => $data['edit_name'],
            'description'     =>  $data['edit_description'],
            'priority'     =>  $data['edit_priority'],
            'valid_to' =>  $data[ 'edit_valid_to'],
        );
        $rules = array(
//            'name'      =>  'required|min:3|Regex:/(^[a-zA-Z ]+$)+/|unique:kitchens,name,'.$id,
            'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
            'description'     =>  'required|min:6',
            'priority'     =>  'required|numeric',
            'valid_to'  =>  'required|date|date_format:m/d/Y|check_valid_to_edit_date:valid_to',
        );
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        $count= Kitchen:: where('id','!=',$id)->where('name', '=',$request->edit_name)->where('status', '!=','2')->count();
        if($count){
            $errors = array('name1'=>'Kitchen name has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        $kitchen=Kitchen::find($id);
        $kitchen->name=$request->edit_name;
        $kitchen->description=$request->edit_description;
        $kitchen->priority=$request->edit_priority;
        //$kitchen->valid_from = date("Y-m-d", strtotime($request->edit_valid_from));
        $kitchen->valid_to = date("Y-m-d", strtotime($request->edit_valid_to));
        //$kitchen->created_by=Auth::User('user')->id;
        $kitchen->updated_by=Auth::User('user')->id;
        $kitchen->created_at=date("Y-m-d H:i:s");
        $kitchen->updated_at=date("Y-m-d H:i:s");
        $kitchen->status=$request->edit_status;
        $kitchen->save();
        
        Session::flash('message', 'Kitchen updated Successfully!'); 
        $json['success']=1;
        $json['message']='Kitchen updated Successfully';
        echo json_encode($json);
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedKitchens = $data['selectedKitchens'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedKitchens as $key => $value)
        {
            $user = Kitchen::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
       if($request->ajax()){
            $data = Input::all();
            $selectedKitchens = $data['selectedKitchens'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Kitchen</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Priority</th>
						<th style="border:solid 1px #81C02F; padding:10px">Valid From</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Valid To</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedKitchens as $key => $value)
        {
             $data = DB::table('kitchens')
			
			->where('id','=',$value)
			->select('*')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}
                if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}				
				$valid_from = date('d M Y h:i:s',strtotime($data['0']['valid_from']));
				$valid_to = date('d M Y h:i:s',strtotime($data['0']['valid_to']));
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['priority'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_from.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_to.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
