<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\FrontService;

class FrontServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = DB::table('front_services')->where('status','!=', '2');
        //$q = FrontService::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='from-desc'){
            $q->orderBy('valid_from', 'desc');
        }elseif($type=='from'){
            $q->orderBy('valid_from', 'asc');
        }elseif($type=='to-desc'){
            $q->orderBy('valid_to', 'desc');
        }elseif($type=='to'){
            $q->orderBy('valid_to', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $data=$q->paginate(10);
        
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data[$i] = (array) $data[$i];
        }
//        echo '<pre>';print_r($data);die;
        return view('superadmin.restaurants.frontservice.frontservice')->with('frontservices',$data)->with('search',$search)->with('type',$type);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

      public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedFrontservices = $data['selectedFrontservices'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedFrontservices as $key => $value)
        {
            $frontservice = FrontService::find($value);
            $frontservice->status = '2';
            $frontservice->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	/*added by Rajlakshmi(31-05-16)*/
	  public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedFrontservices = $data['selectedFrontservices'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Valid From</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Valid to</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedFrontservices as $key => $value)
        {
             $data = DB::table('front_services')
            ->where('id','=', $value)
            ->select('*')
            ->get(10);
			for ($i = 0, $c = count($data); $i < $c; ++$i) {
					 $data[$i] = (array) $data[$i];
			}
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}
				$valid_from = date("d M Y", strtotime($data['0']['valid_from']));
				$valid_to = date("d M Y", strtotime($data['0']['valid_to']));
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
			
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_from.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_to.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
    /*added by Rajlakshmi(31-05-16)*/

     public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedfrontservice = $data['frontservice_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['frontservice']=FrontService::where('id',$selectedfrontservice)->first();
        $frontservice = $data['frontservice']->toArray();
        if(count($frontservice)>0){
            $json['success']=1;
            $json['id']= $frontservice['id'];
            $json['name']=$frontservice['name'];
            $json['description']=$frontservice['description'];
            $json['valid_from']=$frontservice['valid_from'];
            $json['valid_to']=$frontservice['valid_to'];
            $json['status']=$frontservice['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }


     public function add_frontservice(Request $request)
     {
        if($request->frontservice_id=='0')
        {
			

			$fservice=FrontService::where('name',$request->name)->where('status','!=',2)->get();
			$fservice = count($fservice->toArray()); 
			if($fservice>0)
			{
			  Session::flash('errmsg', 'This Service name is already been taken!');

			  return redirect(route('root.frontservice.index'));
			}
			else{
				$this->set_validation('',$request); 
				$auth_id = Auth::User()->id;
				$frontservice= new FrontService;
				$frontservice->name=$request->name;
				$frontservice->description=$request->description;
				$frontservice->valid_from=$request->valid_from;
				$frontservice->valid_to=$request->valid_to;
				$frontservice->status=$request->status;
				$frontservice->created_by=$auth_id;
				$frontservice->created_at=date("Y-m-d H:i:s");
				$frontservice->save();
                Session::flash('message', 'Front Service Added Successfully!');
				 return redirect(route('root.frontservice.index'));
			   }
        }
        else
        {  
	      $fservice=FrontService::where('name',$request->name)->where('id','!=',$request->frontservice_id)->where('status','!=',2)->get();
          $fservice = count($fservice->toArray()); 
			if($fservice>0)
			{
			  Session::flash('errmsg', 'This Service name is already been taken!');
			  return redirect(route('root.frontservice.index'));
			}
			else{
				$this->set_validation($request->frontservice_id,$request); 
				$auth_id = Auth::User()->id;
				$frontservice=FrontService::find($request->frontservice_id);
				$frontservice->name=$request->name;
				$frontservice->description=$request->description;
				$frontservice->valid_from=$request->valid_from;
				$frontservice->valid_to=$request->valid_to;
				$frontservice->status=$request->status;
				$frontservice->updated_by=$auth_id;
				$frontservice->updated_at=date("Y-m-d H:i:s");
				$frontservice->save();
				Session::flash('message', 'Front Service Updated Successfully!');
				 return redirect(route('root.frontservice.index'));
			}

        }
        
       
     }

      public function set_validation($id=null,$request)
     {
        $message=array(
            "name.required"=>"Name is required",
            "description.required"=>"Description is required",
            );

        $this->validate($request,[
        //'name' => 'required|unique:front_services,name,'.$id.',id',
		'name' => 'required',
        'description' => 'required',
        ],$message);
     }
}
