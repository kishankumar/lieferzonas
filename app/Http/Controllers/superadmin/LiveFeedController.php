<?php

namespace App\Http\Controllers\superadmin;

use App\superadmin\LiveFeed;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
class LiveFeedController extends Controller
{
    public function index()
    {
        $feed = LiveFeed::first();
       
        return view('superadmin/homepagelist/livefeed')->with('feed',$feed);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'facebook' => 'required',
            'twitter' => 'required',
            'youtube' => 'required',
            'google' => 'required'
        ]);

        $feed = LiveFeed::find(1);
        $feed->facebook = $request->facebook;
        $feed->twitter = $request->twitter;
        $feed->youtube = $request->youtube;
        $feed->google = $request->google;
        $feed->linkedin = $request->linkedin;
        $feed->vk = $request->vk;
        $feed->instagram = $request->instagram;
        $feed->pinterest = $request->pinterest;
        $feed->save();

        return redirect(route('root.livefeed.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
