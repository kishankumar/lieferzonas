<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\MenuExtraType;
use App\superadmin\MenuExtra;

class MenuExtraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        
        $q = DB::table('menu_extras');
        $q->leftjoin('menu_extra_types', 'menu_extras.extra_type_id', '=', 'menu_extra_types.id');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('menu_extras.name', 'LIKE', "%$search%");
                $q2->orWhere('menu_extras.description', 'LIKE', "%$search%");
                $q2->orWhere('menu_extras.price', 'LIKE', "%$search%");
            });
        }
        $q->where('menu_extras.status','!=', 2);
        
        if($type=='name'){
            $q->orderBy('menu_extras.name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('menu_extras.name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('menu_extras.description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('menu_extras.description', 'asc');
        }elseif($type=='price-desc'){
            $q->orderBy('menu_extras.price', 'desc');
        }elseif($type=='price'){
            $q->orderBy('menu_extras.price', 'asc');
        }else{
            $q->orderBy('menu_extras.name', 'asc');
        }
        $q->select('menu_extras.*','menu_extra_types.name as extra_name');
        $data=$q->paginate(10);
        
        
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data[$i] = (array) $data[$i];
        }
        
        $extratype = DB::table('menu_extra_types') ->select('id','name')
            ->where('status','=', '1')
            ->get();
        for ($i = 0, $c = count($extratype); $i < $c; ++$i) {
            $extratype[$i] = (array) $extratype[$i];
        }
        
        return view('superadmin.restaurants.extra.menuextra')->with('search',$search)->with('type',$type)
            ->with('menuextras',$data)->with('extratypes',$extratype);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
      public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedMenuextras = $data['selectedMenuextras'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedMenuextras as $key => $value)
        {
            $menuextra = MenuExtra::find($value);
            $menuextra->status = '2';
            $menuextra->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
     public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedmenuextra = $data['menuextra_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['menuextra']=MenuExtra::where('id',$selectedmenuextra )->first();
        $menuextra = $data['menuextra']->toArray();
        if(count($menuextra)>0){
            $json['success']=1;
            $json['id']= $menuextra['id'];
            $json['name']=$menuextra['name'];
            $json['extratype']=$menuextra['extra_type_id'];
            $json['price']=$menuextra['price'];
            $json['description']=$menuextra['description'];
            $json['status']=$menuextra['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

      public function add_menuextra(Request $request)
     {
        if($request->menuextra_id=='0')
        {
			$Menuextra=MenuExtra::where('name',$request->name)->where('status','!=',2)->get();
			$menuextracount = count($Menuextra->toArray()); 
			if($menuextracount>0)
			{
			  Session::flash('errmsg', 'This Name is already been taken!');

			  return redirect(route('root.menuextra.index'));
			}
			else{
				$this->set_validation('',$request->extratype,$request); 
				$auth_id = Auth::User()->id;
				$menuextra= new MenuExtra;
				$menuextra->name=$request->name;
				$menuextra->extra_type_id=$request->extratype;
				$menuextra->price=$request->price;
				$menuextra->description=$request->description;
				$menuextra->status=$request->status;
				$menuextra->created_by=$auth_id;
				$menuextra->save();
				Session::flash('message', 'Added Successfully!');
			}
        }
        else
        {  
	        $Menuextra=MenuExtra::where('name',$request->name)->where('id','!=',$request->menuextra_id)->where('status','!=',2)->get();
			$menuextracount = count($Menuextra->toArray()); 
			if($menuextracount>0)
			{
			  Session::flash('errmsg', 'This Name is already been taken!');

			  return redirect(route('root.menuextra.index'));
			}
			else{
				$this->set_validation($request->menuextra_id,$request->extratype,$request); 
				$auth_id = Auth::User()->id;
				$menuextra=MenuExtra::find($request->menuextra_id);
				$menuextra->name=$request->name;
				$menuextra->extra_type_id=$request->extratype;
				$menuextra->price=$request->price;
				$menuextra->description=$request->description;
				$menuextra->status=$request->status;
				$menuextra->updated_by=$auth_id;
				$menuextra->updated_at=date("Y-m-d H:i:s");
				$menuextra->save();
				Session::flash('message', 'Updated Successfully!');
			}	
        }
        
        return redirect(route('root.menuextra.index'));
     }
	 
	  	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedMenuextras = $data['selectedMenuextras'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Name</th>
						<th style="border:solid 1px #81C02F; padding:10px">Description</th>
						<th style="border:solid 1px #81C02F; padding:10px">Price</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Menu Extra type</th>
				        <th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedMenuextras as $key => $value)
        {
             $data = DB::table('menu_extras')
			->leftjoin('menu_extra_types', 'menu_extras.extra_type_id', '=', 'menu_extra_types.id')
			->where('menu_extras.id','=',$value)
			->select('menu_extras.*','menu_extra_types.name as typename')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['price'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['typename'].'</td>
		        <td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/

      public function set_validation($id=null,$type=null,$request)
     {
        $message=array(
            "name.required"=>"Name is required",
			"name.unique"=>"This menu of this type is alraedy exist",
            "extratype.required"=>"Menu extra type is required",
            "price.required"=>"Price is required",
            "description.required"=>"Description is required",
            );

        $this->validate($request,[
        //'name' => 'required|unique:menu_extras,name,'.$id.',id,extra_type_id,"'.$type.'"',
		'name' => 'required',
        'extratype' => 'required',
        'price' => 'required|numeric',
        'description' => 'required',
        ],$message);
     }
}
