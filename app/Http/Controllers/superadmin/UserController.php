<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use validator;
//use Input;
use App\superadmin\User;
use App\superadmin\User_contact_map;
use App\superadmin\User_master;
use App\superadmin\User_email_map;
use App\superadmin\Role;
use App\superadmin\Page_list;
use App\superadmin\Access_list;
use App\superadmin\Access_map;
use Mail;
use File;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getaccess(Request $request)
    {

    $Page_list=Page_list::where('status','1')->get();
    $Access_map=Access_map::where('user_id',$request->user_id)->where('status','1')->lists('page_list_id');
    $Access_map=$Access_map->toArray();
    $data='';
    $data.='<ul id="pagelist" class="box-body list-unstyled">';
    $data1='';
    $data1.='<ul id="pagelist" class="box-body list-unstyled">';
  
     
    if(count($Page_list)>0)
    {
        foreach($Page_list as $pages):
            if(in_array($pages->id,$Access_map))
            {
             $Access_list=Access_list::where('status','1')->get();
             $Access_list=$Access_list->toArray();
             $Selaccess_list=Access_map::where('user_id',$request->user_id)->where('page_list_id',$pages->id)->where('status','1')->lists('access_list_id');

              $Selaccess_list=$Selaccess_list->toArray();
              $data.='<li onclick="rtol('.$pages->id.')" id="right_id'.$pages->id.'"><b>'.$pages->page_name.'</b> 
              <input type="hidden" id="page'.$pages->id.'" name="page[]" value="'.$pages->id.'"> 
              <div>';
         
            foreach($Access_list as $access):
            if(in_array($access['id'],$Selaccess_list))
            {
          
               $data.=   '<label class="checkbox-inline"><input type="checkbox" checked value='.$access['id'].' name="access'.$pages->id.'['.$access['id'].']">'.$access['access_name'].'</label>';
              
            
             }
             else
             {
                 $data.=   '<label class="checkbox-inline"><input type="checkbox"  value="'.$access['id'].'" name="access'.$pages->id.'['.$access['id'].']">'.$access['access_name'].'</label>';
                
             }
            endforeach;     
             $data.=  '</div> </li>';
            
            }
            else
            {
              $data1.='<li id="'.$pages->id.'" onclick="ltor('.$pages->id.')"><b>'.$pages->page_name.'</b> 
              <input type="hidden" id="page'.$pages->id.'" name="page[]" value="'.$pages->id.'"> </li>';
              
            }
            endforeach;
    }
$data.="</ul>";
$data1.="</ul>";

$sendata['data']=$data;
$sendata['data1']=$data1;

print_r(json_encode($sendata));

    }
    
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='names';
        }
        
        $q = DB::table('users');
        $q->leftjoin('user_roles', 'users.user_role_id', '=', 'user_roles.id');
        $q->leftjoin('user_masters', 'users.id', '=', 'user_masters.user_id');
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('user_masters.first_name', 'LIKE', "%$search%");
                $q2->orWhere('user_masters.last_name', 'LIKE', "%$search%");
                $q2->orWhere('user_masters.email', 'LIKE', "%$search%");
                $q2->orWhere('user_roles.role_name', 'LIKE', "%$search%");
                $q2->orWhere('user_masters.contact', 'LIKE', "%$search%");
                //$q2->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', "%$search%");
                //$q2->orWhere(DB::raw('CONCAT_WS(" ", first_name, last_name)'), 'like', $search);
            });
        }

        $q->where('users.status','!=', '2');

        
        if($type=='name'){
            $q->orderBy('user_masters.first_name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('user_masters.first_name', 'desc');
        }elseif($type=='email-desc'){
            $q->orderBy('users.email', 'desc');
        }elseif($type=='email'){
            $q->orderBy('users.email', 'asc');
        }elseif($type=='role-desc'){
            $q->orderBy('user_roles.role_name', 'desc');
        }elseif($type=='role'){
            $q->orderBy('user_roles.role_name', 'asc');
        }else{
            //$q->orderBy('user_masters.first_name', 'asc');
            $q->orderBy('users.created_at', 'desc');
        }
        
        $q->select('users.*', 'user_roles.role_name','user_masters.first_name', 'user_masters.contact','user_masters.dob','user_masters.doj','user_masters.add1','user_masters.add2','user_masters.city'
            ,'user_masters.state','user_masters.country');
        $data=$q->paginate(10);
        
        //echo count($data);die;
//        $data = DB::table('users')
//            ->leftjoin('user_roles', 'users.user_role_id', '=', 'user_roles.id')
//            ->leftjoin('user_masters', 'users.id', '=', 'user_masters.user_id')
//            ->where('users.status','!=', '2')
//            ->orderBy('users.email', 'asc')
//            ->select('users.*', 'user_roles.role_name','user_masters.first_name', 'user_masters.contact','user_masters.dob','user_masters.doj','user_masters.add1','user_masters.add2','user_masters.city'
//            ,'user_masters.state','user_masters.country')
//            ->paginate(10);
        
        
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data[$i] = (array) $data[$i];
        }
        
        $page = DB::table('page_lists')->where('status','=','1')->get();
        
        for ($i = 0, $c = count($page); $i < $c; ++$i) {
            $page[$i] = (array) $page[$i];
        } 
        
        $access = DB::table('access_lists')->select('id', 'access_name')->where('status','=','1')->get();
        
        for ($i = 0, $c = count($access); $i < $c; ++$i) {
            $access[$i] = (array) $access[$i];
        }


        return view('superadmin.users.user', ['usersinfo' => $data])->with('pages',$page)->with('access',$access)->with('search',$search)->with('type',$type);  

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        		
        
        $role = DB::table('user_roles')->orderBy('role_name', 'asc')->select('id', 'role_name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($role); $i < $c; ++$i) {
            $role[$i] = (array) $role[$i];
            
        } 
        $country = DB::table('countries')->orderBy('name', 'asc')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($country); $i < $c; ++$i) {
            $country[$i] = (array) $country[$i];
            
        }
        
    return view('superadmin/users/add_user')->with('roles',$role)->with('country',$country);

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $this->set_validation('',$request);  
		$email = $request->email;
		$User_list=User::where('email',$email)->where('status','!=',2)->get();
        $usercount = count($User_list->toArray()); 
		if($usercount>0)
		{
		  Session::flash('errmessage', 'User already exist!');
		  return redirect(route('root.user.create'));
		}
		else{
		$auth_id = Auth::User()->id;
        $user = new User;

        $user->email=$request->email;

        $user->user_role_id=$request->role;
        $user->password=bcrypt($request->password);
        $user->status=$request->status;
		$user->created_by=$auth_id;
        $user->created_at=date("Y-m-d H:i:s");
		$user->save();
        $id =$user->id;
        
        $usercontact= new User_contact_map;
        $usercontact->user_id=$id;
        $usercontact->contact=$request->phone;
        $usercontact->status=$request->status;
	    $usercontact->created_by=$auth_id;
        $usercontact->created_at=date("Y-m-d H:i:s");
        $usercontact->flag='1';
        $usercontact->save();

        $usercontact1= new User_contact_map;
        $usercontact1->user_id=$id;
        $usercontact1->contact=$request->altphone;
        $usercontact1->status=$request->status;
		$usercontact1->created_by=$auth_id;
        $usercontact1->created_at=date("Y-m-d H:i:s");
        $usercontact1->flag='0';
        $usercontact1->save();
        
        $useremail= new User_email_map;
        $useremail->user_id=$id;
        $useremail->email=$request->email;
        $useremail->status=$request->status;
		$useremail->created_by=$auth_id;
        $useremail->created_at=date("Y-m-d H:i:s");
        $useremail->flag='1';
        $useremail->save();

        $useremail1= new User_email_map;
        $useremail1->user_id=$id;
        $useremail1->email=$request->altemail;
        $useremail1->status=$request->status;
		$useremail1->created_by=$auth_id;
        $useremail1->created_at=date("Y-m-d H:i:s");
        $useremail1->flag='0';
        $useremail1->save();

        $destinationpath=public_path()."/uploads/superadmin/users";


        $pic=Input::file('pics');
		
		 if($pic!='')
         {
              $extension=  $pic->getClientOriginalExtension(); // getting image extension
              $filename=time().rand(111,999).'.'.$extension; // renameing image
              $pic->move($destinationpath,$filename);
              $usermaster->profile_pic=$filename;
         } 
       
        
        $usermaster= new User_master;
        $usermaster->user_id=$id;
        $usermaster->first_name=$request->firstname;
        $usermaster->last_name=$request->lastname;
        $usermaster->user_id=$id;
        $usermaster->email=$request->email;
        $usermaster->contact=$request->phone;
      
        $usermaster->dob=$request->dob;
        $usermaster->doj=$request->doj;
        $usermaster->add1=$request->address1;
        $usermaster->add2=$request->address2;
        $usermaster->city=$request->city;
        $usermaster->state=$request->state;
        $usermaster->country=$request->country;
        $usermaster->zipcode=$request->zipcode;
        $usermaster->status=$request->status;
		$usermaster->created_by=$auth_id;
        $usermaster->created_at=date("Y-m-d H:i:s");
        $usermaster->save();
        /* mail */
		
		$uemail = $request->email;
		$uname = $request->firstname;
		$password = $request->password;
        $from_email = 'vikas.alivenetsolutions@gmail.com';
		$from_name = 'Lieferzonas Superadmin';
		$to_admin = 'rajlakshmi.alivenetsolution@gmail.com';
        $data = array(
            
            'uemail' => $uemail,
            'password' => $password,
			'uname' => $uname
        );
		
		$pageuser = 'superadmin.emails.user.touser';
		$subject = 'User registration';
		$this->sendusermail($pageuser,$data,$subject,$from_email,$from_name,$uemail);
		
		 /* mail */
		 
        Session::flash('message', 'User Added Successfully!');
        
        return redirect(route('root.user.index'));
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id){
            $count= DB::table('users')->where('id','=',$id)->where('status','!=','2')->count();
            if(!$count){
                Session::flash('message', 'Please select user again.'); 
                return redirect(route('root.user.index'));
            }
            if( ! preg_match('/^\d+$/', $id) ){
                Session::flash('message', 'Please select user again.'); 
                return redirect(route('root.user.index'));
            }
        }
        
        $q = DB::table('users');
        $q->leftjoin('user_roles', 'users.user_role_id', '=', 'user_roles.id');
        $q->leftjoin('user_masters', 'users.id', '=', 'user_masters.user_id');
        $q->where('users.status','!=', '2');
        $q->where('users.id','=',$id);
        $q->select('users.*', 'user_roles.role_name','user_masters.first_name','user_masters.last_name', 'user_masters.contact','user_masters.dob','user_masters.doj','user_masters.add1','user_masters.add2','user_masters.city'
            ,'user_masters.state','user_masters.country','user_masters.zipcode');
        $data['viewDetails']=$q->get();
//        echo '<pre>';print_r($data['viewDetails']);die;
        return view('superadmin.users.viewpage')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
             $data = DB::table('users')
            ->leftjoin('user_roles', 'users.user_role_id', '=', 'user_roles.id')
            ->leftjoin('user_masters', 'users.id', '=', 'user_masters.user_id')
            ->where('users.id','=', $user_id)
            ->select('users.*', 'user_roles.role_name','user_masters.email','user_masters.first_name','user_masters.last_name','user_masters.contact','user_masters.dob','user_masters.doj','user_masters.add1','user_masters.add2','user_masters.city'
                ,'user_masters.state','user_masters.country','user_masters.zipcode','user_masters.profile_pic')->get();
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
            $data = (array) $data[$i];
        }
		$final = (object)$data;
		
		$get_altcontact = DB::table('user_contact_maps')->select('contact')->where('status','=','1')->where('user_id','=',$user_id)->where('flag','=', '0')->get();
        for ($i = 0, $c = count($get_altcontact); $i < $c; ++$i) {
            $get_altcontact[$i] = (array) $get_altcontact[$i];
            
        } 
		//print_r($get_altcontact); die;
		
		$get_altemail = DB::table('user_email_maps')->select('email')->where('user_id','=',$user_id)->where('status','=','1')->where('flag','=', '0')->get();
        for ($i = 0, $c = count($get_altemail); $i < $c; ++$i) {
            $get_altemail[$i] = (array) $get_altemail[$i];
            
        }
        
         $role = DB::table('user_roles')->orderBy('role_name', 'asc')->select('id', 'role_name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($role); $i < $c; ++$i) {
            $role[$i] = (array) $role[$i];
            
        } 
         $country = DB::table('countries')->orderBy('name', 'asc')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($country); $i < $c; ++$i) {
            $country[$i] = (array) $country[$i];
            
        } 

         $state = DB::table('states')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($state); $i < $c; ++$i) {
            $state[$i] = (array) $state[$i];
            
        } 
        $city = DB::table('cities')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($city); $i < $c; ++$i) {
            $city[$i] = (array) $city[$i];
            
        } 
        $zipcode = DB::table('zipcodes')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($zipcode); $i < $c; ++$i) {
            $zipcode[$i] = (array) $zipcode[$i];
            
        } 
        return view('superadmin/users/edit_user')->with('users',$final)->with('roles',$role)->with('country',$country)
		->with('state',$state)->with('city',$city)->with('zipcode',$zipcode)->with('get_altcontact',$get_altcontact)
		->with('get_altemail',$get_altemail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->set_validation($id,$request);
		
		
        $auth_id = Auth::User()->id; 
		$email = $request->email;
		$User_list=User::where('id','!=',$id)->where('email',$email)->where('status','!=',2)->get();
        $usercount = count($User_list->toArray()); 
		if($usercount>0)
		{
		  Session::flash('errmessage', 'User already exist!');
		  return redirect(url('root/user/'.$id.'/edit'));
		}
		else
		{
        $user=User::find($id);

        $user->email=$request->email;
        
        $user->user_role_id=$request->role;
        $user->password=bcrypt($request->password);
        $user->status=$request->status;
		$user->updated_by=$auth_id;
        $user->updated_at=date("Y-m-d H:i:s");
        $user->save();

        $phonecount = User_contact_map::where('contact', '=', Input::get('phone'))->where('status','!=',2)->where('flag','=','1')->first();

        if($phonecount== null)
        {
            
             $affectedcontact = DB::table('user_contact_maps')
            ->where('user_id', '=', $id)
            ->where('flag', '=', '1')
            ->update(array('status' => '2'));

            $usercontact= new User_contact_map;
            $usercontact->user_id=$id;
            $usercontact->contact=$request->phone;
			$usercontact->updated_by=$auth_id;
            $usercontact->updated_at=date("Y-m-d H:i:s");
            $usercontact->flag='1';
			$usercontact->status='1';
            $usercontact->save();
        } 

        $altphonecount = User_contact_map::where('contact', '=', Input::get('altphone'))->where('status','!=',2)->where('flag','=','0')->first();
        if($altphonecount== null)
        {
			  if($request->altphone!='')
			{
				 $affectedaltcontact = DB::table('user_contact_maps')
				->where('user_id', '=', $id)
				->where('flag', '=', '0')
				->update(array('status' => '2'));

				$usercontact1= new User_contact_map;
				$usercontact1->user_id=$id;
				$usercontact1->contact=$request->altphone;
				$usercontact1->updated_by=$auth_id;
				$usercontact1->updated_at=date("Y-m-d H:i:s");
				$usercontact1->flag='0';
				$usercontact1->status='1';
				$usercontact1->save();
			}
        }


        $emailcount = User_email_map::where('email', '=', Input::get('email'))->where('status','!=',2)->where('flag','=','1')->first();

        if($emailcount== null)
        {

            $affectedaltcontact = DB::table('user_email_maps')
            ->where('user_id', '=', $id)
            ->where('flag', '=', '1')
            ->update(array('status' => '2'));
            $useremail= new User_email_map;
            $useremail->user_id=$id;
            $useremail->email=$request->email;
            $useremail->flag='1';
			$useremail->status='1';
			$useremail->updated_by=$auth_id;
            $useremail->updated_at=date("Y-m-d H:i:s");
            $useremail->save();
        }
        $altemailcount = User_email_map::where('email', '=', Input::get('altemail'))->where('status','!=',2)->where('flag','=','0')->first();

        if($altemailcount== null)
        {
            if($request->altemail!='')
			{
				$affectedaltcontact = DB::table('user_email_maps')
				->where('user_id', '=', $id)
				->where('flag', '=', '0')
				->update(array('status' => '2'));
				$useremail1= new User_email_map;
				$useremail1->user_id=$id;
				$useremail1->email=$request->altemail;
				$useremail1->updated_by=$auth_id;
				$useremail1->updated_at=date("Y-m-d H:i:s");
				$useremail1->flag='0';
				$useremail1->status='1';
				$useremail1->save();
			}
            
        }
        $extension = '';
        $destinationpath=public_path()."/superadmin/uploads/users";
		//$destinationpath = $path = app_path('public\uploads\users');
        $pic=Input::file('pics');
        

       
        
        $usermaster= new User_master;
        $usermaster = User_master::where('user_id', $id)->first();
        $usermaster->first_name=$request->firstname;
        $usermaster->last_name=$request->lastname;
        $usermaster->email=$request->email;
        $usermaster->contact=$request->phone;
        $usermaster->dob=$request->dob;
        $usermaster->doj=$request->doj;

         if($pic!='')
         {
             $extension=  $pic->getClientOriginalExtension(); // getting image extension
             $filename=time().rand(111,999).'.'.$extension; // renameing image
             $pic->move($destinationpath,$filename);
             $usermaster->profile_pic=$filename;
         }  
        $usermaster->add1=$request->address1;
        $usermaster->add2=$request->address2;
        $usermaster->city=$request->city;
        $usermaster->state=$request->state;
        $usermaster->country=$request->country;
        $usermaster->zipcode=$request->zipcode;
		$usermaster->updated_by=$auth_id;
        $usermaster->updated_at=date("Y-m-d H:i:s");
        $usermaster->save();

        Session::flash('message','User Updated Successfully');
		
		return redirect(route('root.user.index'));
	 }   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	 public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedUsers = $data['selectedUsers'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedUsers as $key => $value)
        {
            $user = User::find($value);
			$userpic = User_master::where('user_id',$user->id)->get();
			
			$pathtofile = 'public/superadmin/uploads/users/'.$userpic['0']['profile_pic'];
            $user->status = '2';
            $user->save();
			if($userpic['0']['profile_pic']!='')
			{
				File::Delete($pathtofile);
			}
			
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(31-05-16)*/
	 public function download(Request $request)
    {
		//echo 'rrrrr';exit();
        if($request->ajax()){
            $data = Input::all();
            $selectedUsers = $data['selectedUsers'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
		$data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Email Id</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Role</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Contact</th><th>Status</th></tr></thead>';
        foreach($selectedUsers as $key => $value)
        {
            $data = DB::table('users')
            ->leftjoin('user_roles', 'users.user_role_id', '=', 'user_roles.id')
            ->leftjoin('user_masters', 'users.id', '=', 'user_masters.user_id')

            ->where('users.id','=', $value)
           
            ->select('users.*', 'user_roles.role_name','user_masters.first_name'
			,'user_masters.contact')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				$data1.= '<tr><td style="border:solid 1px #81C02F; padding:10px">'.$data['first_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['email'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['role_name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['contact'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
    /*added by Rajlakshmi(31-05-16)*/
    public function add_access(Request $request) 
    { 
	    $auth_id = Auth::User()->id;
        $pagelist = array();
        $pagelist = Input::get('page');
       
          $id = Input::get('userid');
          $count = Access_map::where('user_id', '=', $id)->where('status','=','1')->get();
          $count = count($count->toArray());
       
        if($count!= null)
        {
             $affected_data = DB::table('access_maps')
            ->where('user_id', '=', $id)
            ->where('status', '=', '1')
            ->update(array('status' => '2'));

        }
       
        for($i=0; $i<count($pagelist); $i++ )
         {
              $accesslist = array();
              $accesslist = Input::get('access'. $pagelist[$i]);
              if($accesslist!='')
			  {
				    foreach($accesslist as $access_id)
             {

                $query = DB::table('access_maps');
                $query->where('status', '=', '1');
                $query->where('page_list_id', '=', $pagelist[$i]);
                $query->where('access_list_id', '=', $access_id);
                $query->where('user_id', '=', $id);
                $result =$query->count();
                
                if(!$result)
                {
                    $accessmap= new Access_map;
                    $accessmap->user_id=$id;
                    $accessmap->page_list_id=$pagelist[$i];
                    $accessmap->access_list_id=$access_id;
                    $accessmap->status='1';
					$accessmap->created_by=$auth_id;
					$accessmap->updated_by=$auth_id;
                    $accessmap->updated_at=date("Y-m-d H:i:s");
                    $accessmap->created_at=date("Y-m-d H:i:s");
                    $accessmap->save();
               

                }
                else
                {
                    $accessmap= new Access_map;
                }
                    
            }
			  }
             
         
         }
          return redirect(route('root.user.index'));
    }


    /////Add validation////////
     public function set_validation($id=null,$request)
     {
        $message=array(
            "firstname.required"=>"First name is required",
            "firstname.min"=>"The First name must be at least 3 characters.",
            "firstname.alpha"=>"The First name must be alphabet",
            "lastname.required"=>"Last name is required",
            "lastname.min"=>"The Last name must be at least 3 characters.",
            "lastname.alpha"=>"The Last name must be alphabet",
            "password.required"=>"Password is required",
			
			"pics.mimes"=>"please choose profile pics of type jpg,png,tif,bmp",
            "phone.required"=>"Phone no is required",
            
            "email.required"=>"Email is required",
            "email.email"=>"Enter valid email",
            "address1.required"=>"Address is required",
            "city.required"=>"City is required",
            "state.required"=>"State is required",
            "country.required"=>"Country is required",
            "zipcode.required"=>"Zipcode is required",
           
            "dob.required"=>"Date of birth is required",
            "doj.required"=>"Date of joining is required",
            
           
            
            );

        $this->validate($request,[
        'firstname' => 'required|min:3|Regex:/^[a-zA-ZäÄöÖüÜß]+$/',
        'lastname' => 'required|min:3|Regex:/^[a-zA-ZäÄöÖüÜß]+$/',
        'password' => 'required',
		'pics' => 'mimes:jpeg,bmp,png,tif',
        'phone' => 'required|numeric',
        'email' => 'required|Regex:/^[a-zA-Z0-9._-äÄöÖüÜß]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i',
        'role' => 'required',
        'address1' => 'required',
        'city' => 'required',
        'state' => 'required',
        'country' => 'required',
        'zipcode' => 'required',
        'dob' => 'required',
        'doj' => 'required'

            
            
        ],$message);
     }

	public function sendusermail($pageuser,$data,$subject,$from_email,$from_name,$uemail)
	{
		Mail::send($pageuser, $data, function($messa) use ($subject,$from_email,$from_name,$uemail)  {
            $messa->from($from_email, $from_name);
            $messa->to($uemail)->subject($subject);
           
        });
	}
        
        
}

