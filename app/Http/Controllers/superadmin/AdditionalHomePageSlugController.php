<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\AdditionalHomePageSlug;
use Validator;
use Auth;
use App\superadmin\FrontCityMap;
use App\superadmin\FrontCountryMap;
use App\superadmin\FrontStateMap;
use App\superadmin\City;
use App\superadmin\Country;
use App\superadmin\State;

class AdditionalHomePageSlugController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='slug';
        }
        $q = AdditionalHomePageSlug::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('slug', 'LIKE', "%$search%");
                $q2->orWhere('description1', 'LIKE', "%$search%");
                $q2->orWhere('description2', 'LIKE', "%$search%");
                $q2->orWhere('is_type', 'LIKE', "%$search%");
            });
        }
        if($type=='slug'){
            $q->orderBy('slug', 'asc');
        }else if($type=='slug-desc'){
            $q->orderBy('slug', 'desc');
        }else if($type=='descrip1-desc'){
            $q->orderBy('description1', 'desc');
        }else if($type=='descrip1'){
            $q->orderBy('description1', 'asc');
        }else if($type=='descrip2-desc'){
            $q->orderBy('description2', 'desc');
        }else if($type=='descrip2'){
            $q->orderBy('description2', 'asc');
        }else{
            $q->orderBy('slug', 'asc');
        }
        $homepageslug = $q->paginate(10);
        
        //$alergic = Alergic_content::where('status','!=','2')->orderBy('name','desc')->paginate(10);
        return view('superadmin.HomePageSlug.homepage')->with('homepageslug',$homepageslug)->with('search',$search)->with('type',$type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['city'] = City::where('status','=','1')->orderBy('name','asc')->get();
        $data['state'] = State::where('status','=','1')->orderBy('name','asc')->get();
        $data['country'] = Country::where('status','=','1')->orderBy('name','asc')->get();
        $data['city_slug'] = array();
        $data['state_slug'] = array();
        $data['country_slug'] = array();
        foreach($data['city'] as $city){
            $data['city_slug'][$city->id] = ($city->slug);
        }
        foreach($data['state'] as $state){
            $data['state_slug'][$state->id] = ($state->slug);
        }
        foreach($data['country'] as $country){
            $data['country_slug'][$country->id] = ($country->slug);
        }
        //echo '<pre>';print_r($data['country_slug']);die;
        return view('superadmin/HomePageSlug/add_homepage')->with($data);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->set_validation('',$request);
        
        $images=Input::file('images');
         $destinationpath=public_path()."/superadmin/homepage/images";
        $extension=  $images->getClientOriginalExtension(); // getting image extension
        $filename=time().rand(111,999).'.'.$extension; // renameing image
        $images->move($destinationpath,$filename);
        
        $vedioUpload = Input::file('upvideo');
        $video_type = $request->video_type;
        if($video_type == 'url'){
            $filename1 = $request->urlName;
        }else{
            if($vedioUpload){
                $extension1 = $request->file('upvideo')->getClientOriginalExtension();
                if($extension1 != '')
                {
                    $destinationpath1=public_path()."/superadmin/homepage/allvedios";
                    $vedioName=time().rand(111,999).'.'.$extension1; // renameing image
                    $request->file('upvideo')->move($destinationpath1,$vedioName);
                }
            }else{
                $vedioName='';
            }
        }
        
        $slugType = $request->slugType;
        if($slugType == 'city'){
            $slugDetail = City::where('id','=',$request->selectedSlugCityId)->where('status','=','1')->first();
        }else if($slugType == 'state'){
            $slugDetail = State::where('id','=',$request->selectedSlugStateId)->where('status','=','1')->first();
        }else if($slugType == 'country'){
            $slugDetail = Country::where('id','=',$request->selectedSlugCountryId)->where('status','=','1')->first();
        }else{
            $slugDetail = City::where('id','=',$request->selectedSlugCityId)->where('status','=','1')->first();
        }
        $SlugName = $slugDetail->slug;
        
        $type= new AdditionalHomePageSlug;
        if($slugType == 'city'){
            $type->fk_id=$request->selectedSlugCityId;
        }else if($slugType == 'state'){
            $type->fk_id=$request->selectedSlugStateId;
        }else{
            $type->fk_id=$request->selectedSlugCountryId;
        }
        $type->is_type="$request->slugType";
        $type->slug=$SlugName;
        $type->description1=$request->description1;
        $type->description2=$request->description2;
        $type->image=$filename;
        if($video_type == 'url'){
            $type->url=$filename1;
            $type->vedio_upload_type='U';
        }else{
            $type->video=$vedioName;
            $type->vedio_upload_type='V';
        }
        $type->created_by=Auth::User('user')->id;
        $type->updated_by=Auth::User('user')->id;
        $type->created_at=date("Y-m-d H:i:s");
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=1;
        $type->save();
        
        Session::flash('message', 'Home Page Added Successfully!');
        return redirect(route('root.home.pages.index'));
    }
    
    public function set_validation($id=null,$request)
    {
        $message=array(
            "slug_name.required"=>"Please select slug type again",
            "slug_name.slug_exists"=>"Slug Type already exists",
            "images.required"=>"Please upload image",
            "description1.required"=>"Please enter description1",
            "description2.required"=>"Please enter description2",
        );
        $this->validate($request,[
            'slug_name'=> 'required|slug_exists',
            'images'=> 'required',
            'description1'=> 'required|min:5',
            'description2'=> 'required|min:5',
        ],$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['city'] = City::where('status','=','1')->orderBy('id','asc')->get();
        $data['state'] = State::where('status','=','1')->orderBy('id','asc')->get();
        $data['country'] = Country::where('status','=','1')->orderBy('id','asc')->get();
        foreach($data['city'] as $city){
            $data['city_slug'][$city->id] = ($city->slug);
        }
        foreach($data['state'] as $state){
            $data['state_slug'][$state->id] = ($state->slug);
        }
        foreach($data['country'] as $country){
            $data['country_slug'][$country->id] = ($country->slug);
        }
        $data['home_page_detail'] = AdditionalHomePageSlug::find($id);
        
        return view('superadmin/HomePageSlug/edit_homepage')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message=array(
            "slug_name.required"=>"Please select slug type again",
            "slug_name.slug_exists"=>"Slug Type already exists",
            "description1.required"=>"Please enter description1",
            "description2.required"=>"Please enter description2",
        );
        $this->validate($request,[
            'slug_name'=> 'required|slug_exists',
            'description1'=> 'required|min:5',
            'description2'=> 'required|min:5',
        ],$message);
        
        $images=Input::file('images');
        if($images){
            $destinationpath=public_path()."/superadmin/homepage/images";
            $extension=  $images->getClientOriginalExtension(); // getting image extension
            $filename=time().rand(111,999).'.'.$extension; // renameing image
            $images->move($destinationpath,$filename);
        }else{
            $filename=$request->image_name;
        }
        
        $vedioUpload = Input::file('upvideo');
        $video_type = $request->video_type;
        
        if($video_type == 'url'){
            $filename1 = $request->urlName;
        }else{
            if($vedioUpload){
                $extension1 = $request->file('upvideo')->getClientOriginalExtension();
                if($extension1 != '')
                {
                    $destinationpath1=public_path()."/superadmin/homepage/allvedios";
                    $vedioName=time().rand(111,999).'.'.$extension1; // renameing image
                    $request->file('upvideo')->move($destinationpath1,$vedioName);
                }
            }else{
                $vedioName=$request->video_name;
            }
        }
        
        $slugType = $request->slugType;
        if($slugType == 'city'){
            $slugDetail = City::where('id','=',$request->selectedSlugCityId)->where('status','=','1')->first();
        }else if($slugType == 'state'){
            $slugDetail = State::where('id','=',$request->selectedSlugStateId)->where('status','=','1')->first();
        }else if($slugType == 'country'){
            $slugDetail = Country::where('id','=',$request->selectedSlugCountryId)->where('status','=','1')->first();
        }else{
            $slugDetail = City::where('id','=',$request->selectedSlugCityId)->where('status','=','1')->first();
        }
        $SlugName = $slugDetail->slug;
        
        $type=AdditionalHomePageSlug :: find($id);
        if($slugType == 'city'){
            $type->fk_id=$request->selectedSlugCityId;
        }else if($slugType == 'state'){
            $type->fk_id=$request->selectedSlugStateId;
        }else{
            $type->fk_id=$request->selectedSlugCountryId;
        }
        $type->is_type="$request->slugType";
        $type->slug=$SlugName;
        $type->description1=$request->description1;
        $type->description2=$request->description2;
        $type->image=$filename;
        if($video_type == 'url'){
            $type->url=$filename1;
            $type->vedio_upload_type='U';
        }else{
            $type->video=$vedioName;
            $type->vedio_upload_type='V';
        }
        $type->created_by=Auth::User('user')->id;
        $type->updated_by=Auth::User('user')->id;
        $type->created_at=date("Y-m-d H:i:s");
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=$request->status;
        $type->save();
        
        Session::flash('message', 'Home Page Updated Successfully!');
        return redirect(route('root.home.pages.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedPages = $data['selectedPages'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedPages as $key => $value)
        {
            $user = AdditionalHomePageSlug::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
}
