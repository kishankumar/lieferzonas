<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\front\RestSuggestion;
use App\front\SuggestionKitchenMap;
use Illuminate\Support\Facades\Input;
class RestSuggestionNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	  $restsuggestion_notiInfo = RestSuggestion::where('status','!=','2')->select('id', 'fname','lname','email','mobile','is_read')->orderby('id','desc')->paginate(10);
	  $restsuggestion = RestSuggestion::where('status','=','2')->where('is_read','=','0')->select('id', 'fname','lname','email','mobile','is_read')->paginate(10);
      $noticount = count($restsuggestion);  
      return view('superadmin.notifications.restsuggestion_notification')->with('restsuggestion_notiInfo',$restsuggestion_notiInfo)->with('noticount',$noticount);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function restsuggestiondetail($id)
	{
	  $restsuggestion_notiInfo = RestSuggestion::where('id','=',$id)->select('*')->get();
	  //print_r($restsuggestion_notiInfo);
      return view('superadmin.notifications.restsuggestion_notification_detail')->with('restsuggestion_notiInfo',$restsuggestion_notiInfo);
	}
	
	public function readstatus()
	{
	    $id = Input::get('id'); 
	    $restsuggestion_noti=RestSuggestion::find($id);
		$restsuggestion_noti->is_read=1;
		$restsuggestion_noti->save();
		$json['success']=1;
        echo json_encode($json);
        return ;
	}
}
