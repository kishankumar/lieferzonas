<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\SetCashbackPoint;


class CashbackPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('set_cashback_points')
            ->where('status','!=', '2')
            ->select('*')
            ->paginate(10);
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
                 $data[$i] = (array) $data[$i];
        }
       
        return view('superadmin.promocode.cashback')
        ->with('cashbackpoints',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->set_validation('',$request); 
        $auth_id = Auth::User()->id; 
        $cashback=SetCashbackPoint::find($request->cashback_id); 
        $cashback->points=$request->points;
        $cashback->status=$request->status;
        $cashback->description=$request->description;
        $cashback->updated_by=$auth_id;
        $cashback->updated_at=date("Y-m-d H:i:s");
        $cashback->save();
		Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.cashback.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	/*added by Rajlakshmi(01-06-16)*/
	
    public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedcashback = $data['cashback_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['cashback']=SetCashbackPoint::where('id',$selectedcashback)->first();
        $cashback = $data['cashback']->toArray();
        if(count($cashback)>0){
            $json['success']=1;
            $json['id']= $cashback['id'];
            $json['cashback_title']=$cashback['cashback_title'];
            $json['description']=$cashback['description'];
            $json['points']=$cashback['points'];
            $json['status']=$cashback['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
	
		public function download(Request $request)
    {

        if($request->ajax()){
            $data = Input::all();
           
            $selectedCashback = $data['selectedCashback'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr><th style="border:solid 1px #81C02F; padding:10px">Cashback Title</th>
        <th style="border:solid 1px #81C02F; padding:10px">Points</th>
		<th style="border:solid 1px #81C02F; padding:10px">Description</th>
		
        <th style="border:solid 1px #81C02F; padding:10px">Status</th></tr><tr></tr></thead>';
        foreach($selectedCashback as $key => $value)
        {
            $data = DB::table('set_cashback_points')
            

            ->where('id','=', $value)
           
            ->select('*')->get();
				for ($i = 0, $c = count($data); $i < $c; ++$i) {
				$data = (array) $data[$i];
				}
		        
				//print_r($data); die;
				if($data['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				
				if((strlen($data['description']) > 50))
				{
					$desc = substr($data['description'],0,50).'...';
				}
				else
				{
					$desc = $data['description'];
				}
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['cashback_title'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['points'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
				
		}
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }

	/*added by Rajlakshmi(01-06-16)*/
	
    public function set_validation($id=null,$request)
     {
        $message=array(
            "points.required"=>"Cashback points is required",
            "description.required"=>"Description is required",
            );

        $this->validate($request,[
        'points' => 'required',
        'description' => 'required',
        ],$message);
     }
}
