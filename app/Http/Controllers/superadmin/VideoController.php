<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\Video;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = DB::table('videos')
            ->where('status','!=', '2')
            ->select('*')
            ->paginate(10);
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
                 $data[$i] = (array) $data[$i];
        }
        
        return view('superadmin.videos.video')
        ->with('videos',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->set_validation($id,$request);
      
        $auth_id = Auth::User()->id; 
        $videoup=Video::find($request->video_id); 
       
        $destinationpath=public_path()."/superadmin/uploads/videos";
        $videoupload=Input::file('upvideo'); //die;
		
		if($videoupload!='')
		{
			
			  $extension=  $videoupload->getClientOriginalExtension(); // getting image extension
			  $filename=time().rand(111,999).'.'.$extension; // renameing image
			  $videoupload->move($destinationpath,$filename);
			  $videoup->video_name=$filename;	
			  
		      
		}			
			 
			
        $videoup->updated_by=$auth_id;
        $videoup->updated_at=date("Y-m-d H:i:s");
        $videoup->save();
		Session::flash('message', 'Updated Successfully!');
        return redirect(route('root.video.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedvideo = $data['video_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['video']=Video::where('id',$selectedvideo)->first();
        $video = $data['video']->toArray();
        if(count($video)>0){
            $json['success']=1;
            $json['id']= $video['id'];
            $json['video_title']=$video['video_title'];
           
            $json['video']=$video['video_name'];
            $json['status']=$video['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
	

public function set_validation($id=null,$request)
     {
        $message=array(
            "upvideo.mimetypes"=>"Please choose video of format mp4,avi,mpeg",
            
            );

        $this->validate($request,[
		
        'upvideo' => 'mimetypes:video/mp4,video/avi,video/mpeg,video/quicktime'
        ],$message);
     }
    
}
