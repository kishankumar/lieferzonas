<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\Root_categorie;
use Validator;
use Auth;

class RootcatergoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='name';
        }
        $q = Root_categorie::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('name', 'LIKE', "%$search%");
                $q2->orWhere('description', 'LIKE', "%$search%");
            });
        }
        if($type=='name'){
            $q->orderBy('name', 'asc');
        }elseif($type=='name-desc'){
            $q->orderBy('name', 'desc');
        }elseif($type=='descrip-desc'){
            $q->orderBy('description', 'desc');
        }elseif($type=='descrip'){
            $q->orderBy('description', 'asc');
        }elseif($type=='priority-desc'){
            $q->orderBy('priority', 'desc');
        }elseif($type=='priority'){
            $q->orderBy('priority', 'asc');
        }elseif($type=='from-desc'){
            $q->orderBy('valid_from', 'desc');
        }elseif($type=='from'){
            $q->orderBy('valid_from', 'asc');
        }elseif($type=='to-desc'){
            $q->orderBy('valid_to', 'desc');
        }elseif($type=='to'){
            $q->orderBy('valid_to', 'asc');
        }else{
            $q->orderBy('name', 'asc');
        }
        $catInfo = $q->paginate(10);
        return view('superadmin.root_categories.categories')->with('categoies',$catInfo)->with('search',$search)->with('type',$type); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$name = Input::get('name');//for get input field value
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages = array(
            'end_after' => "Valid from date should be before valid to date.",
            'check_date' => "Date should be equal or greater than current date.",
            'check_valid_to_date' => "Valid to Date should be equal or greater than current date.",
            //'priority.regex' => 'priority should be greater than 0.'
        );
        
        $category_id = Input::get('category_id');
        
        if($category_id){
            $userData = array(
                'name'      => $data['name'],
                'description'     =>  $data['description'],
                'priority'     =>  $data['priority'],
                'valid_to' =>  $data[ 'valid_to'],
            );
            $rules = array(
                //'name'      =>  'required|min:3|Regex:/(^[a-zA-Z ]+$)+/|unique:root_categories,name,'.$category_id,
                'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'description'     =>  'required|min:6',
                'priority'     =>  'required|numeric',
                'valid_to'  =>  'required|date|date_format:m/d/Y|check_valid_to_date:valid_to',
            );
        }else{
            $userData = array(
                'name'      => $data['name'],
                'description'     =>  $data['description'],
                'priority'     =>  $data['priority'],
                'valid_from'  =>  $data['valid_from'],
                'valid_to' =>  $data[ 'valid_to'],
            );
            $rules = array(
                'name'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'description'     =>  'required|min:6',
                'priority'     =>  'required|numeric',
                'valid_from'  =>  'required|date|date_format:m/d/Y|check_date:valid_from',
                'valid_to'  =>  'required|date|date_format:m/d/Y|end_after:valid_from',
            );
        }
        
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($category_id){
            $count= Root_categorie:: where('id','!=',$category_id)->where('name', '=',$request->name)->where('status', '!=','2')->count();
        }else{
            $count= Root_categorie:: where('name', '=',$request->name)->where('status', '!=','2')->count();
        }
        if($count){
            $errors = array('name1'=>'The name has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($category_id){
            $category=Root_categorie::find($category_id);
        }else{
            $category= new Root_categorie;
            $category->created_at=date("Y-m-d H:i:s");
            $category->created_by=Auth::User('user')->id;
            $category->valid_from = date("Y-m-d", strtotime($request->valid_from));
        }
        $category->name=$request->name;
        $category->description=$request->description;
        $category->priority=$request->priority;
        $category->valid_to = date("Y-m-d", strtotime($request->valid_to));
        $category->updated_by=Auth::User('user')->id;
        $category->updated_at=date("Y-m-d H:i:s");
        $category->status=$request->status;
        $category->save();
        
        if($category_id){
            Session::flash('message', 'Category updated Successfully!'); 
            $json['success']=1;
            $json['message']='Category updated Successfully.';
        }else{
            Session::flash('message', 'Category added Successfully!'); 
            $json['success']=1;
            $json['message']='Category added Successfully.';
        }
        echo json_encode($json);
        return;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id){
            $count= Root_categorie:: where('id','=',$id)->where('status','!=', '2')->count();
            if(!$count){
                Session::flash('message', 'Please select Categories again.'); 
                return redirect(route('root.categories.index'));
            }
            if( ! preg_match('/^\d+$/', $id) ){
                Session::flash('message', 'Please select Categories again.'); 
                return redirect(route('root.categories.index'));
            }
        }
        $data['root_cat'] = Root_categorie:: where('id','=',$id)->where('status','!=', '2')->get();
        return view('superadmin.root_categories.viewpage')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedCategory = $data['checkedCategory'];
        
        $data['categories']=Root_categorie::find($selectedCategory);
        $categories = $data['categories']->toArray();
        if(count($categories)>0){
            $json['success']=1;
            $json['id']=$categories['id'];
            $json['name']=$categories['name'];
            $json['description']=$categories['description'];
            $json['status']=$categories['status'];
            $json['priority']=$categories['priority'];
            $json['valid_from']= date("m/d/Y", strtotime($categories['valid_from']));
            $json['valid_to']= date("m/d/Y", strtotime($categories['valid_to']));
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCategories = $data['selectedCategories'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedCategories as $key => $value)
        {
            $user = Root_categorie::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedCategories = $data['selectedCategories'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Root Category</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Priority</th>
						<th style="border:solid 1px #81C02F; padding:10px">Valid From</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Valid To</th>
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedCategories as $key => $value)
        {
             $data = DB::table('root_categories')
			
			->where('id','=',$value)
			->select('*')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
				if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}
				$valid_from = date('d M Y h:i:s',strtotime($data['0']['valid_from']));
				$valid_to = date('d M Y h:i:s',strtotime($data['0']['valid_to']));
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['description'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['priority'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_from.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$valid_to.'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
