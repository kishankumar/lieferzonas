<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\DiscountType;

class DiscountTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('discount_types')
            ->where('status','!=', '2')
            ->select('*')
            ->paginate(10);
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
                 $data[$i] = (array) $data[$i];
        }
       
        return view('superadmin.SpecialOffer.discounts.discounttype')
        ->with('discounttypes',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
		  $distype=DiscountType::where('name',$request->name)->where('id','!=',$request->discounttype_id)->where('status','!=',2)->get();
          $distype = count($distype->toArray()); 
			if($distype>0)
			{
			  Session::flash('errmsg', 'This Name is already been taken!');
			  return redirect(route('root.discounttype.index'));
			}
			else{
				$this->set_validation('',$request); 
				$auth_id = Auth::User()->id; 
				$discounttype=DiscountType::find($request->discounttype_id); 
				
				$discounttype->name=$request->name;
				$discounttype->type=$request->type;
				$discounttype->status=$request->status;
				$discounttype->description=$request->description;
				$discounttype->updated_by=$auth_id;
				$discounttype->updated_at=date("Y-m-d H:i:s");
				$discounttype->save();
				Session::flash('message', 'Updated Successfully!');
				return redirect(route('root.discounttype.index'));
			}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	 public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selecteddiscounttype = $data['discounttype_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['discounttype']=DiscountType::where('id',$selecteddiscounttype)->first();
        $discounttype = $data['discounttype']->toArray();
        if(count($discounttype)>0){
            $json['success']=1;
            $json['id']= $discounttype['id'];
            $json['name']=$discounttype['name'];
            $json['description']=$discounttype['description'];
            $json['type']=$discounttype['type'];
            $json['status']=$discounttype['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }
	
	 /*added by Rajlakshmi(31-05-16)*/
	   public function download(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedDiscounttypes = $data['selectedDiscounttypes'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
       $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>            <th style="border:solid 1px #81C02F; padding:10px">Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Description</th>
						<th style="border:solid 1px #81C02F; padding:10px">Type</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr><tr></tr></thead>';
        foreach($selectedDiscounttypes as $key => $value)
        {
             $data = DB::table('discount_types')
			
			->leftjoin('users', 'users.id', '=', 'discount_types.created_by')
			->where('discount_types.id','=',$value)
			->select('discount_types.*','users.email')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}
                if($data['0']['type']=='1')
				{
					$type='Pop Up';
				}
				else
				{
					$type='Not Pop Up';
				}	
                if((strlen($data['0']['description']) > 50))
				{
					$desc = substr($data['0']['description'],0,50).'...';
				}
				else
				{
					$desc = $data['0']['description'];
				}					
				$cdate = date('d M Y h:i:s',strtotime($data['0']['created_at']));
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['name'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$desc.'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$type.'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
	 /*added by Rajlakshmi(31-05-16)*/
	 
    public function set_validation($id=null,$request)
     {
        $message=array(
            "name.required"=>"Name is required",
            "description.required"=>"Description is required",
            );

        $this->validate($request,[
        'name' => 'required',
        'description' => 'required',
        ],$message);
     }
}
