<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Input;
use App\superadmin\Color_setting;
use Validator;
use Auth;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $search='';$type='';
        if($request->search){
            $search=$request->search;
        }
        if($request->type){
            $type=$request->type;
        }else{
            $type='color';
        }
        $q = Color_setting::where('status','!=',2);
        if(!empty($search)){
            $q->where( function ( $q2 ) use ( $search ) {
                $q2->where('color', 'LIKE', "%$search%");
                $q2->orWhere('color_code', 'LIKE', "%$search%");
            });
        }
        if($type=='color'){
            $q->orderBy('color', 'asc');
        }elseif($type=='color-desc'){
            $q->orderBy('color', 'desc');
        }elseif($type=='code-desc'){
            $q->orderBy('color_code', 'desc');
        }elseif($type=='code'){
            $q->orderBy('color_code', 'asc');
        }else{
            $q->orderBy('color', 'asc');
        }
        $alergic = $q->paginate(10);
        //$alergic = Color_setting::where('status','!=','2')->orderBy('color','desc')->paginate(10);
        return view('superadmin.colors.index')->with('alergic',$alergic)->with('search',$search)->with('type',$type);

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
        }else{
            $json['success']=0;
            $json['errors']='Please enter valid data';
            echo json_encode($json);
            return;
        }
        $messages = array(
            'end_after' => "Valid from date should be before valid to date."
        );
        $userData = array(
            'color'      => $data['color'],
            'color_code'     =>  $data['color_code']
        );
        
        $color_id = Input::get('color_id');
        if($color_id){
            $rules = array(
                //'color'      =>  'required|min:3|Regex:/(^[a-zA-Z ]+$)+/|unique:color_settings,color,'.$color_id,
                'color'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'color_code'     =>  'required|min:3'
            );
        }else{
            $rules = array(
                //'color'      =>  'required|min:3|unique:color_settings|Regex:/(^[a-zA-Z ]+$)+/',
                'color'      =>  'required|min:3|Regex:/(^[a-zA-ZäÄöÖüÜß«» -]+$)+/',
                'color_code'     =>  'required|min:6',
            );
        }
        $validation  = Validator::make($userData,$rules,$messages);
        if($validation->fails())
        {
            $errors = $validation->getMessageBag()->toArray();
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        if($color_id){
            $count= Color_setting:: where('id','!=',$color_id)->where('color', '=',$request->color)->where('status', '!=','2')->count();
        }else{
            $count= Color_setting:: where('color', '=',$request->color)->where('status', '!=','2')->count();
        }
        if($count){
            $errors = array('name1'=>'color name has already been taken.');
            $json['success']=0;
            $json['errors']=$errors;
            echo json_encode($json);
            return;
        }
        
        if($color_id){
            $type=Color_setting::find($color_id);
        }else{
            $type= new Color_setting;
            $type->created_at=date("Y-m-d H:i:s");
            $type->created_by=Auth::User('user')->id;
        }
        $type->color=$request->color;
        
        $code = $request->color_code;
        if($code[0]!='#'){
            $code = '#'.$request->color_code;
        }
        $type->color_code=$code;
        $type->updated_by=Auth::User('user')->id;
        $type->updated_at=date("Y-m-d H:i:s");
        $type->status=$request->status;
        $type->save();
        if($color_id){
            Session::flash('message', 'color updated Successfully!'); 
            $json['success']=1;
            $json['message']='color updated Successfully.';
        }else{
            Session::flash('message', 'color added Successfully!'); 
            $json['success']=1;
            $json['message']='color added Successfully.';
        }
        echo json_encode($json);
        return;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Input::all();
        $selectedColor = $data['checkedColor'];
        
        $data['categories']=Color_setting::find($selectedColor);
        $categories = $data['categories']->toArray();
        if(count($categories)>0){
            $json['success']=1;
            $json['id']=$categories['id'];
            $json['color']=$categories['color'];
            $json['color_code']=$categories['color_code'];
            $json['status']=$categories['status'];
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedColors = $data['selectedColors'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedColors as $key => $value)
        {
            $user = Color_setting::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
	
	/*added by Rajlakshmi(02-06-16)*/
	public function download(Request $request)
    {
       if($request->ajax()){
            $data = Input::all();
            $selectedColors = $data['selectedColors'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data1='';
        $data1.='<table id="table1" style="border-collapse: collapse; width:100%; font-family:arial; font-size:14px; color:#444;">
		<thead style="background:#81C02F; color:#fff;">
		<tr>
                        <th style="border:solid 1px #81C02F; padding:10px">Color Name</th>
                        <th style="border:solid 1px #81C02F; padding:10px">Color Code</th>
                       
            			<th style="border:solid 1px #81C02F; padding:10px">Status</th>
						</tr></thead>';
        foreach($selectedColors as $key => $value)
        {
             $data = DB::table('color_settings')
			
			->where('id','=',$value)
			->select('*')
			->get();
           for ($i = 0, $c = count($data); $i < $c; ++$i) {
             $data[$i] = (array) $data[$i];
		   } 
			
		       //print_r($data); die; 
				
				if($data['0']['status']=='1')
				{
					$status='Active';
				}
				else
				{
					$status='Deactive';
				}	
			
				
				$data1.= '<tr>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['color'].'</td>
				<td style="border:solid 1px #81C02F; padding:10px">'.$data['0']['color_code'].'</td>
				
				<td style="border:solid 1px #81C02F; padding:10px">'.$status.'</td>'; 
        }
		
		$data1.='</tr></table>';
        $sendata['data1']=$data1;
        echo json_encode($sendata);
        return;
    }
 /*added by Rajlakshmi(02-06-16)*/
}
