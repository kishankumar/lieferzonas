<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Start routing by vikas for facebook login
Route::get('auth/facebook', 'Auth\FrontAuthController@redirectToProvider');
Route::get('auth/google', 'Auth\FrontAuthController@redirectToProviderGoogle');
Route::get('auth/twitter', 'Auth\FrontAuthController@redirectToProviderTwitter');
Route::get('auth/facebook/callback', 'Auth\FrontAuthController@handleProviderCallback');
Route::get('auth/google/callback', 'Auth\FrontAuthController@handleProviderCallbackGoogle');
Route::get('auth/twitter/callback', 'Auth\FrontAuthController@handleProviderCallbackTwitter');
Route::post('register','Auth\FrontAuthController@registeruser');
//Route::get('auth/{provider?}', 'Auth\FrontAuthController@redirectToProvider');
//End routing by vikas for facebook login

//start login route by vikas
Route::resource('/','front\HomeController');
Route::get('superadmin', function () {
    if (\Auth::check('user'))
        return redirect('/root/dashboard');
    else
        return view('superadmin/login');
});
Route::get('superadmin/login', function () {
    if (\Auth::check('user'))
        return redirect('/root/dashboard');
    else
        return view('superadmin/login');
});
Route::get('admin', function () {
    if (\Auth::check('admin'))
        return redirect('/rzone/dashboard');
    else
        return view('admin/login');
});
Route::get('admin/login', function () {
    if (\Auth::check('admin'))
        return redirect('/rzone/dashboard');
    else
        return view('admin/login');
});
//end login route by vikas

//start Multi-auth for admin and super-admin panel by vikas
//superadmin route
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('authadmin/login', 'Auth\AdminAuthController@getLogin');
Route::post('authadmin/login', 'Auth\AdminAuthController@postLogin');
Route::get('authadmin/logout', 'Auth\AdminAuthController@getLogout');

Route::get('frontuser/login', 'Auth\FrontAuthController@getLogin');
Route::get('frontuser/register', 'Auth\FrontAuthController@getRegister');
Route::post('frontuser/register','Auth\FrontAuthController@postRegister');
Route::post('frontuser/login', 'Auth\FrontAuthController@postLogin');
Route::get('frontuser/logout', 'Auth\FrontAuthController@getLogout');

// Super Admin route
//Route::get('superadmin/dashboard', 'UserController@getHome');
Route::get('root/dashboard', 'superadmin\DashboardController@index');
Route::controller('/user', 'Auth\AuthController');
Route::controller('/password', 'Auth\PasswordController');

// Admin route
//Route::get('/admin/dashboard', 'AdminController@getHome');
Route::get('rzone/dashboard', 'admin\DashboardController@index');
Route::controller('/admin/password', 'Auth\AdminPasswordController');
Route::controller('/admin', 'Auth\AdminAuthController');

// Front route
//Route::get('/front/dashboard', 'FrontController@getHome');
Route::get('front/dashboard', 'front\DashboardController@index');
Route::controller('/frontuser/password', 'Auth\FrontPasswordController');
Route::controller('/frontuser', 'Auth\FrontAuthController');

//end Multi-auth for admin and super-admin panel by vikas

//Nikhil's Code--------------------------------------------------------------------------------------------------------
Route::resource('root/roles','superadmin\RoleController');
Route::resource('root/roles/delete','superadmin\RoleController@delete');
Route::resource('root/roles/changeStatus','superadmin\RoleController@changeStatus');

Route::resource('root/accesslist','superadmin\AccessListController');
Route::resource('root/accesslist/delete','superadmin\AccessListController@delete');
Route::resource('root/accesslist/changeStatus','superadmin\AccessListController@changeStatus');

Route::resource('root/country','superadmin\CountryController');
Route::resource('root/country/delete','superadmin\CountryController@delete');
Route::resource('root/country/changeStatus','superadmin\CountryController@changeStatus');
Route::post('root/country/generateSlug','superadmin\CountryController@generateSlug');

Route::resource('root/state','superadmin\StateController');
Route::resource('root/state/delete','superadmin\StateController@delete');
Route::resource('root/state/changeStatus','superadmin\StateController@changeStatus');
Route::post('root/state/generateSlug','superadmin\StateController@generateSlug');

Route::resource('root/city','superadmin\CityController');
Route::resource('root/city/delete','superadmin\CityController@delete');
Route::resource('root/city/changeStatus','superadmin\CityController@changeStatus');
Route::post('root/city/generateSlug','superadmin\CityController@generateSlug');

Route::resource('root/zipcode','superadmin\ZipcodeController');
Route::resource('root/zipcode/delete','superadmin\ZipcodeController@delete');
Route::resource('root/zipcode/changeStatus','superadmin\ZipcodeController@changeStatus');

Route::resource('root/faqcat','superadmin\FaqCategoryController');
Route::resource('root/faqcat/delete','superadmin\FaqCategoryController@delete');
Route::resource('root/faqcat/changeStatus','superadmin\FaqCategoryController@changeStatus');

Route::resource('root/faq','superadmin\FaqQuestionController');
Route::resource('root/faq/delete','superadmin\FaqQuestionController@delete');
Route::resource('root/faq/changeStatus','superadmin\FaqQuestionController@changeStatus');

Route::resource('root/infopage','superadmin\FrontPageController');
Route::resource('root/infopage/delete','superadmin\FrontPageController@delete');
Route::resource('root/infopage/changeStatus','superadmin\FrontPageController@changeStatus');

Route::resource('root/marketingtips','superadmin\MarketingTipController');
Route::resource('root/marketingtips/delete','superadmin\MarketingTipController@delete');
Route::resource('root/marketingtips/changeStatus','superadmin\MarketingTipController@changeStatus');

Route::resource('root/countryfrontlist','superadmin\FrontCountryController');

Route::resource('root/statefrontlist','superadmin\FrontStateController');

Route::resource('root/cityfrontlist','superadmin\FrontCityController');

Route::resource('root/kitchenfrontlist','superadmin\FrontKitchenController');

Route::resource('root/sociallinks','superadmin\SocialNetworkController');

Route::resource('root/livefeed','superadmin\LiveFeedController');

Route::resource('root/searchfilter','superadmin\RestSearchFilterController');
Route::resource('root/searchfilter/changeStatus','superadmin\RestSearchFilterController@changeStatus');
//Route::resource('/register','front\RegistrationController@register');
//Route::resource('/register/verifyemail/{code}','front\RegistrationController@verifyemail');
Route::resource('/get_zipcodes','front\HomeController@get_zipcodes');

Route::get('/search','front\RestSearchController@index');
Route::post('/search','front\RestSearchController@index');
Route::post('/userfav','front\RestSearchController@user_favourite');
Route::post('/checkzip','front\RestSearchController@check_zip');
Route::post('/checkzip_new','front\RestSearchController@checkzip_new');


//Nikhil's Code ends----------------------------------------------------------------------------------------------------

////////////////////////Start kishan's routing////////////////////////

      /////////////////Start Superadmin Routing///////////////////

Route::resource('root/restaurant','superadmin\RestaurantController'); //20_04_2016
Route::resource('root/kitchenmap','superadmin\KitchenMapController'); //28_04_2016
Route::resource('ajax/getkitchen','superadmin\AjaxController@getkitchen'); //30_04_2016
Route::resource('ajax/getRootCategory','superadmin\AjaxController@getRootCategory'); //02_05_2016
Route::resource('ajax/gatData','superadmin\AjaxController@getData'); //02_05_2016
Route::resource('root/rootcatmap','superadmin\RootCategoryMapController'); //02_05_2016
Route::resource('root/category','superadmin\CategoryController'); //02_05_2016

Route::resource('ajax/getExistValue','superadmin\AjaxController@getExistValue'); //06_05_2016
Route::resource('ajax/deleteItems','superadmin\AjaxController@deleteItems'); //09_05_2016
Route::resource('root/menu','superadmin\MenuController'); //09_05_2016
Route::resource('ajax/getCatKitchenPriceType','superadmin\AjaxController@getCatKitchenPriceType'); //09_05_2016
Route::resource('root/submenu','superadmin\SubMenuController'); //11_05_2016
Route::resource('ajax/gatDataSingleTable','superadmin\AjaxController@gatDataSingleTable'); //11_05_2016
Route::resource('root/extratypemap','superadmin\RestExtraTypeMapController'); //13_05_2016
Route::resource('ajax/getExtraType','superadmin\AjaxController@getExtraType'); //13_05_2016
Route::resource('root/extramap','superadmin\RestExtraMapController'); //13_05_2016
Route::resource('ajax/deleteRestaurants','superadmin\AjaxController@deleteRestaurants'); //19_05_2016
Route::resource('root/restsetting','superadmin\RestaurantSettingController'); //19_05_2016
Route::resource('ajax/gatPriceType','superadmin\AjaxController@gatPriceType'); //27_05_2016
Route::resource('cart/addcart','front\FrontCartController@addItem'); //03_06_2016
Route::post('registercart','Auth\FrontAuthController@registerCartUser');

     /////////////////End Superadmin Routing///////////////////

   /////////////////Start Front Routing///////////////////

Route::resource('restaurant/{restId}/search','front\RestaurantDetailController@getDetails');
Route::resource('restaurant/{restId}/review-order','front\FrontCartController@reviewOrder');
Route::resource('restaurant/{restId}/shipping-area','front\FrontCartController@shippingArea');
Route::resource('front/ajax/getExistValOneCond','front\FrontCartController@getExistValOneCond');
Route::resource('front/ajax/getAddress','front\FrontAjaxController@getAddress');
Route::resource('front/ajax/setAddress','front\FrontAjaxController@setAddress');
Route::resource('front/ajax/deleteAddress','front\FrontAjaxController@deleteAddress');
// Route::post('checkout','front\FrontCartController@cartCheckout');
Route::post('checkout','front\FrontCartController@place_order');

Route::resource('deleteAddress','front\FrontCartController@deleteAddress');
Route::resource('front/ajax/setTimePreOrder','front\FrontAjaxController@setTimePreOrder');
Route::resource('front/ajax/checkCouponCode','front\FrontAjaxController@checkCouponCode');    
Route::get('confirm-order/{id}','front\FrontCartController@orderConfirmation');

//Nikhil's-------------------------------------------------
Route::resource('check-order-status','front\FrontCartController@check_order_status');
Route::get('live-route/{id}','front\FrontCartController@live_route');
Route::GET('front/order/payment','front\FrontCartController@payment');
////////////////End Front Routing///////////////////

////////////////////////End kishan's routing////////////////////////


//------------------------------------------Start Routing by vikas---------------------------------------
Route::resource('root/categories','superadmin\RootcatergoriesController');
Route::post('root/categories/delete', 'superadmin\RootcatergoriesController@delete');
Route::resource('root/kitchen','superadmin\KitchenController');
Route::post('root/kitchens/delete', 'superadmin\KitchenController@delete');
Route::post('root/kitchens/fetchdata','superadmin\KitchenController@fetchdata');
Route::post('root/kitchens/update/{kitchen_id}','superadmin\KitchenController@update');
Route::resource('root/alergic-contents','superadmin\AlergicController');
Route::post('root/alergic_contents/delete', 'superadmin\AlergicController@delete');
Route::resource('root/colors','superadmin\ColorController');
Route::post('root/colors/delete', 'superadmin\ColorController@delete');
Route::resource('root/day-time','superadmin\DaytimeController');
Route::resource('root/day/time/search','superadmin\DaytimeController@search');
Route::GET('root/day/time/add-time/{restaurant_id}','superadmin\DaytimeController@create');
Route::resource('root/pages','superadmin\PageController');
Route::post('root/pages/delete', 'superadmin\PageController@delete');
Route::resource('root/email-types','superadmin\EmailtypeController');
Route::post('root/email_types/delete', 'superadmin\EmailtypeController@delete');
Route::resource('root/currency','superadmin\CurrencyController');
Route::post('root/currency/delete', 'superadmin\CurrencyController@delete');
Route::resource('root/pricetypes','superadmin\PriceTypeController');
Route::post('root/pricetypes/delete', 'superadmin\PriceTypeController@delete');

Route::resource('root/services','superadmin\ServiceController');
Route::post('root/services/delete', 'superadmin\ServiceController@delete');
Route::resource('root/menus/categories','superadmin\MenuCategoryController');
Route::post('root/menus/categories/delete', 'superadmin\MenuCategoryController@delete');
Route::resource('root/tax/setting','superadmin\TaxSettingController');
Route::post('root/tax/setting/delete', 'superadmin\TaxSettingController@delete');

Route::resource('root/metrics','superadmin\MetricsController');
Route::post('root/metrics/delete', 'superadmin\MetricsController@delete');
Route::resource('root/shop/categories','superadmin\ShopCategoryController');
Route::post('root/shop/categories/delete', 'superadmin\ShopCategoryController@delete');
Route::resource('root/shop/metricscatmap','superadmin\ShopMetricCategoryController'); 
Route::resource('shop/metricscatmap/getdata','superadmin\ShopMetricCategoryController@getdata');
Route::resource('root/shop/items','superadmin\ShopItemController');
Route::post('root/shop/items/delete', 'superadmin\ShopItemController@delete');
Route::resource('root/item/detail/{item_id}/{category_id}/search','superadmin\ShopItemController@search');
Route::resource('root/shop/inventory','superadmin\ShopInventoryController');
Route::post('root/shop/inventory/getCatItems','superadmin\ShopInventoryController@getCatItems');
Route::post('root/shop/inventory/delete', 'superadmin\ShopInventoryController@delete');
//End Routing by vikas

///////////Start Routing By Raj Lakshmi/////////////////
Route::resource('root/user','superadmin\UserController');
Route::post('root/users/delete', 'superadmin\UserController@delete');
Route::post('root/userss/download', 'superadmin\UserController@download');
Route::resource('root/user/add_access','superadmin\UserController@add_access');
Route::resource('getaccess','superadmin\UserController@getaccess');
Route::resource('getdata','superadmin\AjaxController@getData');

Route::resource('root/timevalidity','superadmin\TimevalidityController');
Route::post('root/timevalidity/delete', 'superadmin\TimevalidityController@delete');
Route::resource('root/promocode','superadmin\PromocodeController');
Route::post('root/promocode/delete', 'superadmin\PromocodeController@delete');
Route::resource('root/bonus','superadmin\BonusPointController');
Route::resource('root/cashback','superadmin\CashbackPointController');
Route::resource('fetchdata','superadmin\BonusPointController@fetchdata');
Route::resource('fetchdataa', 'superadmin\CashbackPointController@fetchdata');

Route::resource('root/restowner','superadmin\RestownerController');
Route::post('root/restowner/delete', 'superadmin\RestownerController@delete');

Route::resource('root/restowner/change_pass', 'superadmin\RestownerController@change_pass');

Route::resource('root/frontservice','superadmin\FrontServiceController');
Route::resource('fetchdatas', 'superadmin\FrontServiceController@fetchdata');
Route::post('root/frontservice/delete','superadmin\FrontServiceController@delete');
Route::resource('root/frontservice/add_frontservice', 'superadmin\FrontServiceController@add_frontservice');

Route::resource('root/menuextratype','superadmin\MenuExtraTypeController');
Route::resource('fetchmenutypedatas', 'superadmin\MenuExtraTypeController@fetchdata');
Route::post('root/menuextratype/delete','superadmin\MenuExtraTypeController@delete');
Route::resource('root/menuextratype/add_extratype', 'superadmin\MenuExtraTypeController@add_extratype');

Route::resource('root/menuextra','superadmin\MenuExtraController');
Route::resource('fetchmenudatas', 'superadmin\MenuExtraController@fetchdata');
Route::post('root/menuextra/delete','superadmin\MenuExtraController@delete');
Route::resource('root/menuextra/add_menuextra', 'superadmin\MenuExtraController@add_menuextra');
Route::resource('root/servicemap','superadmin\RestServiceMapController'); 
Route::resource('ajax/getservice','superadmin\AjaxController@getservice');

///////////End Routing By Raj Lakshmi/////////////////

//Start routing  by vikas for superadmin
Route::resource('root/stamps','superadmin\StampCardController');
Route::resource('root/custbonus','superadmin\CustomerBonusController');
Route::resource('root/assignstamp','superadmin\RestStampMapController');
Route::resource('root/assignbonus','superadmin\RestCustBonusMapController');
Route::resource('root/shop/orders','superadmin\ShopOrderController');
Route::post('root/shop/orders/getdata','superadmin\ShopOrderController@getdata');
Route::resource('root/etemplate','superadmin\EmailTemplateController');
Route::post('root/etemplate/delete', 'superadmin\EmailTemplateController@delete');
Route::resource('root/zipcodes/setting','superadmin\ZipCodeSettingController');
Route::resource('root/zipcodes/getdata','superadmin\ZipCodeSettingController@getdata');
//end routing by vikas for superadmin

//Start routing by vikas for admin panel
Route::resource('rzone/shop/cart','admin\ShopCartController');
Route::post('rzone/payment/address','admin\ShopCartController@showAddress');
Route::GET('rzone/shopcart/payment','admin\ShopCartController@payment');
Route::post('rzone/shop/cart/getItemPrice','admin\ShopCartController@getItemPrice');
Route::post('rzone/shop/cart/removeCartItems','admin\ShopCartController@removeCartItems');
Route::post('rzone/shop/cart/updateCartItems','admin\ShopCartController@updateCartItems');
Route::resource('rzone/shop/orders','admin\ShopOrderController');
Route::post('rzone/shop/orders/getdata','admin\ShopOrderController@getdata');
Route::resource('rzone/contactus','admin\RestAdminContactUsController');
Route::resource('rzone/marketingtips','admin\MarketingTipController');

Route::resource('rzone/menu','admin\MenuController');
Route::post('rzone/menu/checkstock','admin\MenuController@updateStock');
//End routing  by vikas for admin panel

//Start routing  by vikas for front panel
Route::GET('restaurant/details/{restoo_id}','front\RestaurantDetailController@getDetails');
//End routing  by vikas for front panel

Route::resource('root/restnotification','superadmin\RestNotificationController');
Route::post('root/restnotification/delete', 'superadmin\RestNotificationController@delete');
Route::resource('rzone/profile','admin\ProfileController');
Route::resource('rzone/taxinvoicenumber','admin\TinInvoiceController');
Route::resource('admin/dashboard','admin\DashboardController');
Route::resource('getdata','admin\ProfileController@getData');
Route::resource('showmore','admin\ShowNotificationController@showmore');
Route::resource('root/spices','superadmin\SpiceController');
Route::post('root/spices/delete','superadmin\SpiceController@delete');
Route::resource('fetchspicedatas', 'superadmin\SpiceController@fetchdata');
Route::resource('root/spices/add_spice', 'superadmin\SpiceController@add_spice');

Route::resource('root/orderservice','superadmin\OrderServiceController');
Route::resource('fetchorderservicedatas', 'superadmin\OrderServiceController@fetchdata');
Route::post('root/orderservice/delete','superadmin\OrderServiceController@delete');
Route::resource('root/orderservice/add_orderservice', 'superadmin\OrderServiceController@add_orderservice');
Route::resource('root/discounttype','superadmin\DiscountTypeController');
Route::resource('fetchdiscount', 'superadmin\DiscountTypeController@fetchdata');
Route::resource('rzone/specialoffer','admin\SpecialOfferController');
Route::resource('getdatatbl','admin\SpecialOfferController@getDatatbl');

Route::get('rzone/specialoffer/details/{offerId}', 'admin\SpecialOfferController@getDetails');

Route::resource('getdatatbl','admin\SpecialOfferController@getDatatbl');
Route::resource('root/specialoffer','superadmin\SpecialOfferController');
Route::post('root/specialoffers/delete', 'superadmin\SpecialOfferController@delete');
Route::post('root/specialoffers/changeStatus', 'superadmin\SpecialOfferController@changeStatus');
Route::resource('root/profile','superadmin\ProfileController');
Route::get('root/specialoffers/detail/{specialofferId}', 'superadmin\SpecialOfferController@getofferDetail');
Route::post('root/roles/download', 'superadmin\RoleController@download');
Route::post('root/pages/download', 'superadmin\PageController@download');
Route::post('root/restaurant/download', 'superadmin\RestaurantController@download');
Route::post('root/restowner/download', 'superadmin\RestownerController@download');
Route::post('root/frontservice/download','superadmin\FrontServiceController@download');
Route::post('root/restnotification/download','superadmin\RestNotificationController@download');
Route::post('root/country/download','superadmin\CountryController@download');
Route::post('root/state/download','superadmin\StateController@download');
Route::post('root/city/download','superadmin\CityController@download');
Route::post('root/zipcode/download','superadmin\ZipcodeController@download');
Route::post('root/marketingtips/download','superadmin\MarketingTipController@download');
Route::post('root/searchfilter/download','superadmin\RestSearchFilterController@download');
Route::post('root/orderservice/download','superadmin\OrderServiceController@download');
Route::post('root/discounttype/download','superadmin\DiscountTypeController@download');
Route::post('root/custbonus/download','superadmin\CustomerBonusController@download');
Route::post('root/email_types/download','superadmin\EmailtypeController@download');
Route::post('root/stamps/download','superadmin\StampCardController@download');
Route::post('root/specialoffer/download','superadmin\SpecialOfferController@download');
Route::post('root/metrics/download','superadmin\MetricsController@download');
Route::post('root/shop/categories/download','superadmin\ShopCategoryController@download');
Route::post('root/shop/items/download','superadmin\ShopItemController@download');
Route::post('root/shop/inventory/download','superadmin\ShopInventoryController@download');
Route::post('root/etemplate/download','superadmin\EmailTemplateController@download');
Route::post('root/promocode/download','superadmin\PromocodeController@download');
Route::post('root/bonus/download','superadmin\BonusPointController@download');
Route::post('root/cashback/download','superadmin\CashbackPointController@download');
Route::post('root/timevalidity/download','superadmin\TimevalidityController@download');
Route::post('root/faq/download','superadmin\FaqQuestionController@download');
Route::post('root/faqcat/download','superadmin\FaqCategoryController@download');
Route::post('root/infopage/download','superadmin\FrontPageController@download');
Route::post('root/categories/download','superadmin\RootcatergoriesController@download');
Route::post('root/kitchens/download','superadmin\KitchenController@download');
Route::post('root/alergic_contents/download','superadmin\AlergicController@download');
Route::post('root/colors/download','superadmin\ColorController@download');
Route::post('root/currency/download','superadmin\CurrencyController@download');
Route::post('root/pricetypes/download','superadmin\PriceTypeController@download');
Route::post('root/services/download','superadmin\ServiceController@download');
Route::post('root/menus/categories/download','superadmin\MenuCategoryController@download');
Route::post('root/tax/setting/download','superadmin\TaxSettingController@download');
Route::post('root/accesslist/download','superadmin\AccessListController@download');
Route::post('root/spices/download','superadmin\SpiceController@download');
Route::post('root/menuextra/download','superadmin\MenuExtraController@download');
Route::post('root/menuextratype/download','superadmin\MenuExtraTypeController@download');
Route::post('root/restsetting/download','superadmin\RestaurantSettingController@download');
Route::post('root/menu/download','superadmin\MenuController@download');
Route::post('root/submenu/download','superadmin\SubmenuController@download');
Route::post('root/category/download','superadmin\SubmenuController@download');
Route::post('rzone/specialoffer/download','admin\SpecialOfferController@download');
Route::post('root/category/download','superadmin\CategoryController@download');
Route::resource('root/video','superadmin\VideoController');
Route::resource('fetchvdatas','superadmin\VideoController@fetchdata');
Route::resource('front/profile','front\ProfileController');
Route::resource('front/address','front\AddressController');
Route::resource('getdatas','front\AddressController@getDatas');
Route::get('front/address/delete/{id}', 'front\AddressController@delete');
Route::resource('front/changepass','front\ChangePasswordController');

Route::get('front/resetpassword/{act_key}','front\ForgotPasswordController@resetpassword');
Route::resource('front/forgotpass','front\ForgotPasswordController@store');
Route::get('front/invalidpasswordurl','front\ForgotPasswordController@Invalidurl');
Route::get('front/resetpasswordsuccess','front\ForgotPasswordController@successresetmsg');
Route::post('front/fpasssendmail','front\ForgotPasswordController@sendforgotpassmail');


Route::post('front/suggestrest','front\SuggestRestController@add_suggestion');
Route::resource('front/favourite','front\FavouriteController');
Route::post('front/addfavourite','front\FavouriteController@add_favourite');
Route::get('front/order/detail/{Id}','front\OrderDetailController@order_detail');
Route::resource('recent_visit', 'front\RestSearchController@recent_visit');
Route::resource('readstatus','admin\DashboardController@readstatus');
Route::resource('root/restsuggestion_notification','superadmin\RestSuggestionNotificationController');
Route::resource('changereadstatus','superadmin\RestSuggestionNotificationController@readstatus');
Route::get('root/restsuggestion_notification/suggestiondetail/{sugg_id}','superadmin\RestSuggestionNotificationController@restsuggestiondetail');
Route::resource('front/order','front\OrderController');
Route::resource('front/orderrate','front\OrderRateController');
Route::resource('front/orderrate/add','front\OrderRateController@add_review');
Route::resource('front/bonuspoint','front\UserBonusPointController');
Route::resource('front/cashbackpoint','front\UserCashbackPointController');
Route::resource('front/stamp','front\UserStampController');
Route::resource('root/userbonus/stamp','superadmin\UserStampController');
Route::resource('rzone/userbonus/stamp','admin\UserStampController');
Route::resource('root/userorder/order_rating','superadmin\OrderRateController');
Route::resource('rzone/userorder/order_rating','admin\OrderRateController');
Route::resource('rzone/userorder/order_rating/report','admin\OrderRateController@report');
Route::post('root/userorder/order_rating/delete','superadmin\OrderRateController@delete');
Route::resource('fetchreviewdata','superadmin\OrderRateController@fetchdata');
Route::resource('rzone/userbonus/favourites','admin\UserFavouriteController');
Route::resource('root/userbonus/favourites','superadmin\UserFavouriteController');
Route::resource('showuser','superadmin\UserFavouriteController@showuser');
Route::resource('root/userbonus/cashback_list','superadmin\UserCashbackPointController');
Route::resource('root/userbonus/bonus_list','superadmin\UserBonusPointController');
Route::resource('root/userbonus/new_customer_bonus','superadmin\NewCustomerBonusUserListController');
Route::resource('rzone/userbonus/new_customer_bonus','admin\NewCustomerBonusUserListController');
Route::resource('front/bonus_point_history','front\UserBonusPointLogController');
Route::resource('front/cashback_point_history','front\UserCashbackPointLogController');
Route::resource('root/userbonus/bonus_point_history','superadmin\UserBonusPointLogController');
Route::resource('root/userbonus/cashback_point_history','superadmin\UserCashbackPointLogController');
Route::resource('root/bonus_point/history','superadmin\UserBonusPointLogController@bonuspointlog');
Route::resource('root/cashback_point/history','superadmin\UserCashbackPointLogController@cashbackpointlog');
Route::get('front/order/track/{orderId}', 'front\OrderController@trackorder');
Route::resource('root/frontuser','superadmin\FrontUserController');
Route::post('root/frontuser/delete', 'superadmin\FrontUserController@delete');
Route::get('root/frontuser/detail/{FUserId}', 'superadmin\FrontUserController@FUserDetail');
Route::get('root/frontuser/showaddress/{Id}', 'superadmin\FrontUserController@Showaddress');
Route::post('root/frontuser/add_address','superadmin\FrontUserController@add_address');
Route::post('root/frontuser/edit_address','superadmin\FrontUserController@edit_address');
Route::resource('root/front/orders','superadmin\RestaurantOrderController');
Route::post('root/front/orders/delete','superadmin\RestaurantOrderController@delete');
Route::resource('fetchaddress','superadmin\FrontUserController@fetch_address');
Route::resource('deleteaddress','superadmin\FrontUserController@delete_address');
Route::resource('root/fusersubscription','superadmin\FrontUserSubscriptionController');
Route::resource('unsubscribe','superadmin\FrontUserSubscriptionController@unsubscribe');
Route::resource('root/setting','superadmin\SuperAdminSettingController');
Route::resource('getdataa','superadmin\SuperAdminSettingController@fetchdata');
Route::resource('rzone/total_sales','admin\TotalSaleController');
Route::get('front/order/printbill/{orderId}', 'front\OrderController@printBill');
Route::get('rzone/restaurant/orders/printbill/{orderId}', 'admin\RestaurantOrderController@printBill');
Route::get('root/front/orders/printbill/{orderId}', 'superadmin\RestaurantOrderController@printBill');
Route::resource('rzone/generate_invoice','admin\GenerateInvoiceController');
Route::post('rzone/invoice/generatepdf', 'admin\GenerateInvoiceController@invoice');
Route::resource('root/generate_invoice','superadmin\GenerateInvoiceController');
Route::post('root/invoice/generatepdf', 'superadmin\GenerateInvoiceController@invoice');

Route::post('sendmail','admin\ForgotPasswordController@sendforgotpassmail');
Route::get('resetpass/{act_key}','admin\ForgotPasswordController@resetpass');
Route::resource('forgotpassword','admin\ForgotPasswordController@store');
Route::get('invalidpassurl','admin\ForgotPasswordController@Invalidurl');
Route::get('resetpasssuccess','admin\ForgotPasswordController@successresetmsg');
Route::get('root/promocode/detail/{promocodeId}', 'superadmin\PromocodeController@getpromocodeDetail');
Route::resource('showuser_list','superadmin\PromocodeController@showuser');
Route::resource('showrest_list','superadmin\PromocodeController@showrest');
Route::post('root/daily/meal/download','superadmin\RestDailyMealController@download');
Route::resource('front/order/cancel','front\OrderController@order_cancel');
Route::resource('rzone/userorder/order_rating/getcomment','admin\OrderRateController@getcomment');

///////////End Routing By Raj Lakshmi/////////////////

//Start routing  by vikas for superadmin panel on 15-6-16
Route::resource('root/home/pages','superadmin\AdditionalHomePageSlugController');
Route::post('root/home/pages/delete','superadmin\AdditionalHomePageSlugController@delete');
Route::GET('front/home/slug/','front\HomeSlugController@getSlugDetails');
Route::resource('root/taxassign','superadmin\TaxAssignController');
Route::resource('root/taxAssign/fetchData','superadmin\TaxAssignController@fetchData');
//End routing  by vikas for superadmin panel on 15-6-16

//Start routing  by vikas for front panel on 25-6-16
Route::resource('front/complaint','front\RestComplaintController');
Route::post('front/complaint/getdata','front\RestComplaintController@getdata');

Route::resource('root/rest/complaint','superadmin\RestComplaintController');
Route::post('root/rest/complaint/delete','superadmin\RestComplaintController@delete');
Route::resource('rzone/category/image/change','admin\ChangeCatImageController');
Route::resource('root/navigation/info','superadmin\NavigationInfoController');
Route::post('root/navigation/info/delete','superadmin\NavigationInfoController@delete');
Route::resource('rzone/restaurant/status/change','admin\DashboardController@openStatusChange');
Route::resource('rzone/restaurant/orders','admin\RestaurantOrderController');
Route::GET('rzone/restaurant/order/list','admin\RestaurantOrderController@orderList');
Route::GET('rzone/restaurant/order/{orderId}/status','admin\RestaurantOrderController@orderStatus');
Route::post('rzone/restaurant/order/confirmed','admin\RestaurantOrderController@orderConfirmed');
Route::post('rzone/restaurant/order/rejected','admin\RestaurantOrderController@orderRejected');
Route::post('rzone/restaurant/order/cookdelivery','admin\RestaurantOrderController@startCookingDelivery');

Route::resource('root/daily/meal','superadmin\RestDailyMealController');
Route::resource('root/daily/meal/delete','superadmin\RestDailyMealController@delete');
Route::resource('root/rank/setting','superadmin\RestSearchRankSettingController');
Route::resource('rzone/rank/setting','admin\RestSearchRankSettingController');
//End routing  by vikas for front panel on 25-6-16

// Start routing by Hakan Aras for front
Route::get('restaurant/{restId}/details','front\RestaurantDetailController@getRestaurantDetails');
Route::post('set_pickup', 'front\RestSearchController@set_pickup');
Route::post('front/send_email', 'front\EmailController@sendEmail');
Route::post('front/add_item_to_cart', 'front\FrontCartController@addItemToCart');
Route::post('front/clear_cart', 'front\FrontCartController@clearCart');
Route::post('front/increment_cart_item', 'front\FrontCartController@incrementCartItem');
Route::post('front/decrement_cart_item', 'front\FrontCartController@decrementCartItem');
Route::get('front/get_cart', 'front\FrontCartController@getCart');
Route::post('front/favourite/toggle', 'front\FavouriteController@toggle_favourite');
Route::resource('front/help', 'front\HelpController');
Route::get('restaurant/cart/{id}', 'front\NewCartController@getJson');
Route::post('restaurant/cart/{id}', 'front\NewCartController@update');
Route::get('checkout/{restaurant_id}', 'front\NewCartController@checkout');
Route::post('checkout/place_order', 'front\NewCartController@place_order');
Route::resource('front/addresses', 'front\UserAddressController');
Route::get('front/json/addresses', 'front\UserAddressController@addressesJson');
Route::get('service/{service_id}', 'front\HomeSlugController@showServiceSlug');
Route::post('front/register_ajax', 'Auth\FrontAuthController@newRegisterAjax');
Route::resource('front/settings', 'front\UserSettingsController');

Route::post("front/send_invite/sms", "front\SendInviteController@sendSms");
Route::post("front/send_invite/email", "front\SendInviteController@sendEmail");
// End routing by Hakan Aras for front

// Start routing by Hakan Aras for superadmin
Route::get('root/edit_restaurant_menu/{restaurantId}', 'superadmin\RestaurantMenuController@index');
Route::resource('root/category_new', 'superadmin\CategoryNewController');
Route::resource('root/extra_choice', 'superadmin\ExtraChoiceController');
Route::post('root/add_extra', 'superadmin\ExtraChoiceController@addExtraToSubMenu');
Route::resource("root/special_offer", 'superadmin\NewSpecialOfferController');
Route::get("root/special_offer/create/{restaurant_id}", 'superadmin\NewSpecialOfferController@createForRestaurant');
// End routing by Hakan Aras for superadmin