<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class SuperadminServiceTaxInvoice extends Model
{
   protected $table="super_admin_service_tax_invoices";
}
