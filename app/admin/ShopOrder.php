<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class ShopOrder extends Model
{
    protected $table = 'shop_orders';
    public function restaurant_address(){
    	return $this->belongsTo('App\superadmin\RestDetail','rest_detail_id');
    }
    public function emails(){
    	return $this->hasMany('App\superadmin\RestEmailMap','rest_detail_id','rest_detail_id');
    }
    public function mobiles(){
    	return $this->hasMany('App\superadmin\RestMobileMap','rest_detail_id','rest_detail_id');
    }
    public static function get_shop_order_address($orderId=''){
        if($orderId){
            $shopOrderDetail = DB ::table('shop_transactions')
                    ->leftjoin('countries',  'shop_transactions.country_id', '=','countries.id')
                    ->leftjoin('states',  'shop_transactions.state_id', '=','states.id')
                    ->leftjoin('cities',  'shop_transactions.city_id', '=','cities.id')
                    ->where('shop_transactions.shop_order_id','=',$orderId)
                    ->select('shop_transactions.*','countries.name as country_name','states.name as state_name','cities.name as city_name')
                    ->first();
            if(count($shopOrderDetail)){
                $orderAddr = $shopOrderDetail->add1.' '.$shopOrderDetail->add2.' '.$shopOrderDetail->city_name.', '.$shopOrderDetail->state_name.', '.$shopOrderDetail->country_name;
                return $orderAddr;
            }else{
                return 'None';
            }
        }else{
            return 'None';
        }
    }
    
    public static function get_shop_order_number($orderId=''){
        if($orderId){
            $shopOrderDetail = DB ::table('shop_transactions')
                    ->where('shop_transactions.shop_order_id','=',$orderId)
                    ->select('shop_transactions.mobile_no')
                    ->first();
            if(count($shopOrderDetail)){
                return $shopOrderDetail->mobile_no;
            }else{
                return 'None';
            }
        }else{
            return 'None';
        }
    }
    
    public static function get_shop_order_status($statusId=''){
        if($statusId){
            $shopOrderStatus = DB ::table('shop_order_statuses')
                    ->where('id','=',$statusId)
                    ->select('shop_order_statuses.status_name')
                    ->first();
            if(count($shopOrderStatus)){
                return $shopOrderStatus->status_name;
            }else{
                return 'None';
            }
        }else{
            return 'None';
        }
    }
}
