<?php

namespace App\admin;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SpecialOfferSubmenuMap extends Model
{

    public static function getQueryByRestaurant($restaurant_id)
    {
        return SpecialOfferSubmenuMap::
        join("rest_sub_menus", "special_offer_submenu_maps.sub_menu_id", "=", "rest_sub_menus.id")->
        join("special_offers", "special_offer_submenu_maps.special_offer_id", "=", "special_offers.id")->
        where("special_offer_submenu_maps.status", 1)->
        where("rest_sub_menus.status", 1)->
        where("special_offers.status", 1)->
        where("special_offers.valid_to", ">=", Carbon::now())->
        where("special_offers.valid_from", "<=", Carbon::now())->
        where("rest_sub_menus.rest_detail_id", $restaurant_id);
    }

}
