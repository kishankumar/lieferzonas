<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class RestMealValidDay extends Model
{
    protected $table = 'rest_meal_valid_days';
}
