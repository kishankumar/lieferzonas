<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class ShopRestCartTemp extends Model
{
    protected $table = 'shop_rest_cart_temps';
    public function item_name(){
    	return $this->belongsTo('App\superadmin\ShopItem','shop_item_id');
    }
}
