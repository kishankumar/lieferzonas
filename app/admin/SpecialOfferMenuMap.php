<?php

namespace App\admin;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SpecialOfferMenuMap extends Model
{

    public static function getQueryByRestaurant($restaurant_id)
    {

        return SpecialOfferMenuMap::
        join("rest_menus", "special_offer_menu_maps.menu_id", "=", "rest_menus.id")->
        join("special_offers", "special_offer_menu_maps.special_offer_id", "=", "special_offers.id")->
        where("special_offer_menu_maps.status", 1)->
        where("rest_menus.status", 1)->
        where("special_offers.status", 1)->
        where("special_offers.valid_to", ">=", Carbon::now())->
        where("special_offers.valid_from", "<=", Carbon::now())->
        where("rest_menus.rest_detail_id", $restaurant_id);
    }

}
