<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class RestSearchRank extends Model
{
    protected $table="rest_search_ranks";
}
