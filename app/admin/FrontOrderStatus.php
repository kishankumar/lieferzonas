<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class FrontOrderStatus extends Model
{
    protected $table = 'front_order_statuses';
}
