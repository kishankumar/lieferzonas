<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class ShopTransaction extends Model
{
    protected $table = 'shop_transactions';
}
