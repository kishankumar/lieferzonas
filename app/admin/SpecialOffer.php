<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class SpecialOffer extends Model
{

    // Offers that are valid without daytime restriction
    public static function hasRestaurantGeneralOffer($restaurant_id) {
        $categoryRows = SpecialOfferCatMap::getQueryByRestaurant($restaurant_id)->count();
        $menuRows = SpecialOfferMenuMap::getQueryByRestaurant($restaurant_id)->count();
        $submenuRows = SpecialOfferSubmenuMap::getQueryByRestaurant($restaurant_id)->count();
        return true;
    }

    protected static function isOfferValidNow($special_offer_row) {

        $current_day_id = date("N", time());
        $current_time = time();

        $day_map_row = SpecialOfferDayMap::where("status", 1)->where("special_offer_id", $special_offer_row["special_offers.id"])->where("day_id", $current_day_id)->first();
        if (!$day_map_row) {
            return false;
        }
        $time_map_row = SpecialOfferDayTimeMap::where("status", 1)->where("special_offer_id", $special_offer_row["special_offers.id"])->where("day_id", $current_day_id)->first();
        if (!$time_map_row || strtotime($time_map_row->time_from) > $current_time || strtotime($time_map_row->time_to) < $current_time) {
            return false;
        }

        return true;
    }

    protected static function getSpecialOfferArray($row) {
        $data = array();
        $data["type"] = $row["special_offers.discount_type_id"];
        $data["value"] = $row["special_offers.discount_type_value"];
    }

    public static function getRestaurantSpecialOffers($restaurant_id) {
        $categoryRows = SpecialOfferCatMap::getQueryByRestaurant($restaurant_id)->get();
        $menuRows = SpecialOfferMenuMap::getQueryByRestaurant($restaurant_id)->get();
        $submenuRows = SpecialOfferSubmenuMap::getQueryByRestaurant($restaurant_id)->get();

        $data = array();

        $data["categories"] = array();
        $data["menus"] = array();
        $data["submenus"] = array();
        foreach($categoryRows as $categoryRow) {
            if (self::isOfferValidNow($categoryRow)) {
                $data["categories"][$categoryRow["rest_categories.id"]] = self::getSpecialOfferArray($categoryRow);
            }
        }
        foreach($menuRows as $menuRow) {
            if (self::isOfferValidNow($menuRow)) {
                $data["menus"][$menuRow["rest_menus.id"]] = self::getSpecialOfferArray($menuRow);
            }
        }
        foreach($submenuRows as $submenuRow) {
            if (self::isOfferValidNow($submenuRow)) {
                $data["submenus"][$submenuRow["rest_sub_menus.id"]] = self::getSpecialOfferArray($submenuRow);
            }
        }
    }

}
