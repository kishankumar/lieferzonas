<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class ShopRestCartMap extends Model
{
    protected $table = 'shop_rest_cart_maps';
}
