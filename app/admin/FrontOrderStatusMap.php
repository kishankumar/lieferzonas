<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class FrontOrderStatusMap extends Model
{
    protected $table = 'front_order_status_maps';
}
