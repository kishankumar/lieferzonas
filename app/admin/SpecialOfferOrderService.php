<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class SpecialOfferOrderService extends Model
{
    protected $table="special_offer_order_services";
}
