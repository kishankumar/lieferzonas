<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class RestMealItemMap extends Model
{
    protected $table = 'rest_meal_item_maps';
}
