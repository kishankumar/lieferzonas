<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class SpecialOfferDayTimeMap extends Model
{
    protected $table="special_offer_day_time_maps";
}
