<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class RestAdminContactUs extends Model
{
    protected $table = 'rest_admin_contact_us';
}
