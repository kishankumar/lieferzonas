<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class Zipcode extends Model
{
	protected $table = 'zipcodes';
    public function users()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function city()
    {
        return $this->belongsTo('App\superadmin\City','city_id');
    }
}
