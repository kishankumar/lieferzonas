<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\OrderService;
class OrderServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = DB::table('order_services')
            ->where('status','!=', '2')
            ->select('*')
            ->paginate(10);
        for ($i = 0, $c = count($data); $i < $c; ++$i) {
                 $data[$i] = (array) $data[$i];
        }
        
        return view('superadmin.orders.orderservice.orderservice')
        ->with('orderservices',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	  public function delete(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selectedorderservices = $data['selectedorderservices'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedorderservices as $key => $value)
        {
            $orderservice = OrderService::find($value);
            $orderservice->status = '2';
            $orderservice->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }
     public function fetchdata(Request $request)
    {
        if($request->ajax()){
            $data = Input::all();
            $selecteddiscounttype = $data['discounttype_id'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        $data['discounttype']=DiscountType::where('id',$selecteddiscounttype)->first();
        $discounttype = $data['discounttype']->toArray();
        if(count($discounttype)>0){
            $json['success']=1;
            $json['id']= $discounttype['id'];
            $json['name']=$discounttype['name'];
            $json['description']=$discounttype['description'];
            $json['status']=$discounttype['status'];
            
            echo json_encode($json);return;
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
    }

      public function add_orderservice(Request $request)
     {
        if($request->orderservice_id=='0')
        {
            $this->set_validation('',$request); 
            $auth_id = Auth::User()->id;
            $orderService= new OrderService;
            $orderService->name=$request->name;
            $orderService->description=$request->description;
            $orderService->status=$request->status;
            $orderService->created_by=$auth_id;
            $orderService->created_at=date("Y-m-d H:i:s");
            $orderService->save();
        }
        else
        {  
            $this->set_validation($request->orderservice_id,$request); 
            $auth_id = Auth::User()->id;
            $orderService=OrderService::find($request->orderservice_id);
            $orderService->name=$request->name;
            $orderService->description=$request->description;
            $orderService->status=$request->status;
            $orderService->updated_by=$auth_id;
            $orderService->updated_at=date("Y-m-d H:i:s");
            $orderService->save();
        }
        
        return redirect(route('root.orderservice.index'));
     }
    
      public function set_validation($id=null,$request)
     {
        $message=array(
            "name.required"=>"Name is required",
            "description.required"=>"Description is required",
            );

        $this->validate($request,[
        'name' => 'required|unique:order_services,name,'.$id.',id',
        'description' => 'required',
        ],$message);
     }
}
