<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class SubMenuExtraMap extends Model
{
    protected $table = "sub_menu_extra_map";

    public static function hasSubMenuExtra($sub_menu, $extra_choice) {
        return SubMenuExtraMap::where('sub_menu_id', $sub_menu)->where("extra_choice_id", $extra_choice)->count() > 0;
    }

    public static function hasSubMenuAnyExtras($sub_menu) {
        return SubMenuExtraMap::where('sub_menu_id', $sub_menu)->count() > 0;
    }
}
