<?php

namespace App\superadmin;

use App\front\UserOrder;
use App\front\UserOrderReview;
use App\front\UserRestFavourite;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class RestDetail extends Model
{
    public function emails()
    {
        return $this->hasMany('App\superadmin\RestEmailMap', 'rest_detail_id');
    }

    public function mobiles()
    {
        return $this->hasMany('App\superadmin\RestMobileMap', 'rest_detail_id');
    }

    public function landlines()
    {
        return $this->hasMany('App\superadmin\RestLandlineMap', 'rest_detail_id');
    }

    public function kitchens()
    {
        return $this->hasMany('App\superadmin\RestKitchenMap', 'rest_detail_id');
    }

    public function rootCategories()
    {
        return $this->hasMany('App\superadmin\RestRootCatMap', 'rest_detail_id');
    }


    public function city_data()
    {
        return $this->belongsTo('App\superadmin\City', 'city');
    }

    public function state_data()
    {
        return $this->belongsTo('App\superadmin\State', 'state');
    }

    public function country_data()
    {
        return $this->belongsTo('App\superadmin\Country', 'country');
    }

    public function zipcode_data()
    {
        return $this->belongsTo('App\superadmin\Zipcode', 'pincode');
    }

    public static function getRestaurantSearchKeywords($restaurant_id)
    {
        $keywords = array();
        // add name
        $row = RestDetail::where("id", $restaurant_id)->where("status", 1)->first();
        if (!$row) {
            return $keywords;
        }
        array_push($keywords, $row->f_name);
        // add menus
        $menu_rows = RestMenu::where("rest_detail_id", $restaurant_id)->where("status", 1)->get();
        foreach ($menu_rows as $menu_row) {
            array_push($keywords, $menu_row->name);
        }
        // add submenus
        $sub_menu_rows = RestSubMenu::where("rest_detail_id", $restaurant_id)->where("status", 1)->get();
        foreach ($sub_menu_rows as $sub_menu_row) {
            array_push($keywords, $sub_menu_row->name);
        }

        return $keywords;
    }

    public static function getRestaurantLogoForDisplay($restaurant_id)
    {
        $restaurant_row = RestDetail::find($restaurant_id);
        $logo_field = $restaurant_row->logo;
        if ($logo_field && File::exists(public_path() . "/uploads/superadmin/restaurants/" . $logo_field)) {
            return url('/public/uploads/superadmin/restaurants').'/'.$logo_field;

        }
        else {
            return url('/public/uploads/superadmin/restaurants/default.png');
            
        }
    }

    public static function getSuggestedRestaurants() {
        return self::where("status", 1)->get()->random(5);
    }

    public static function getRestaurantDisplayInformationArray($restaurant_id)
    {
        $rest_detail_row = self::where("id", $restaurant_id)->first();
        if (!$rest_detail_row) {
            return false;
        }

        $data = array();

        $data["id"] = $rest_detail_row->id;
        $data["name"] = $rest_detail_row->f_name;

        $data["has_logo"] = $rest_detail_row->logo && File::exists(
                public_path() . "/uploads/superadmin/restaurants/" . $rest_detail_row->logo
            );
        $data["logo"] = "/uploads/superadmin/restaurants/" . $rest_detail_row->logo;
        $data["display_logo"] = RestDetail::getRestaurantLogoForDisplay($restaurant_id);

        $kitchen_rows = RestKitchenMap::getByRestaurant($restaurant_id);
        $kitchens_text = "";
        for ($i = 0; $i < 3 && $i < $kitchen_rows->count(); $i++) {
            if ($i > 0) $kitchens_text .= ", ";
            $kitchens_text .= $kitchen_rows[$i]->name;
            if ($i == 2 && $kitchen_rows->count() > 3) $kitchens_text .= ", ...";
        }
        $data["kitchens_text"] = $kitchens_text;

        $data["rest_favorites"] = UserRestFavourite::rest_favourite_count($restaurant_id);
        $data["is_user_favorite"] = UserRestFavourite::is_rest_user_favourite($restaurant_id);

        $cashback_row = RestCashbackMap::where("status", 1)->where("restaurant_id", $restaurant_id)->first();
        if ($cashback_row) {
            $data["cashback"] = $cashback_row->cashback_percentage;
        }
        else {
            $data["cashback"] = 0;
        }

        $data["closes_in"] = RestDayTimeMap::closesInText($restaurant_id);

        $data["average_completion_time"] = UserOrder::getAverageCompletionTimeByRestaurant($restaurant_id, 10);

        $data["address_line_1"] = $rest_detail_row->add1;
        $data["address_line_2"] = $rest_detail_row->add2;

        $deliveryCosts = \App\superadmin\RestDeliveryCost::getRestaurantDeliveryCosts($restaurant_id);
        $data["minimum_order_amount"] = $deliveryCosts[0];
        $data["below_minimum_delivery_cost"] = $deliveryCosts[1];
        $data["above_minimum_delivery_cost"] = $deliveryCosts[2];

        $order_types = \App\superadmin\RestPriceMap::getRestaurantOrderTypes($restaurant_id);
        $data["has_delivery"] = $order_types[0];
        $data["has_pickup"] = $order_types[1];

        $active_user_stamps = \App\front\UserStampRestCurrent::getActiveUserStamps($restaurant_id);
        $max_stamps = \App\superadmin\RestStampMap::getStampCardTarget($restaurant_id);
        $data["has_stamp_cards"] = \App\superadmin\RestStampMap::hasRestaurantStampCards($restaurant_id);
        if ($data["has_stamp_cards"]) {
            $data["user_stamps"] = $active_user_stamps % $max_stamps;
            $data["max_stamps"] = $max_stamps;
            $data["full_stamp_cards"] = floor($active_user_stamps / $max_stamps);
        }

        $data["open_hours"] = array();
        $daytimeRows = \App\superadmin\RestDayTimeMap::daytime($restaurant_id);
        $data["is_open_now"] = false;
        $data["closed_today"] = false;
        $currentTime = time();

        $data["rating"] = UserOrderReview::rest_review_rating($restaurant_id);

        if (\App\superadmin\RestSetting::isClosedToday($restaurant_id) || count($daytimeRows) == 0) {
            $data["closed_today"] = true;
            $data["is_open_now"] = false;
            return $data;
        }
        foreach ($daytimeRows as $time) {
            if (!$time["is_open"]) {
                $data["open_hours"] = array();
                $data["closed_today"] = true;
                $data["is_open_now"] = false;
                break;
            }
            $openTime = $time["open_time"];
            $closeTime = $time["close_time"];
            if (substr($openTime, 0, 2) == "00") $openTime = "12" . substr($openTime, 2);
            if (substr($closeTime, 0, 2) == "00") $closeTime = "12" . substr($closeTime, 2);
            array_push($data["open_hours"], date('H:i', strtotime($openTime)) . " - " . date('H:i', strtotime($closeTime)));
            if ($currentTime > strtotime($openTime) && $currentTime < strtotime($closeTime)) {
                $data["is_open_now"] = true;
            }
        }
        return $data;
    }

}
