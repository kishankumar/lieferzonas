<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestCategory extends Model
{
    public function getResturant()
    {
    	return $this->belongsTo('App\superadmin\RestDetail','rest_detail_id');
    }

    public function getRootCategory()
    {
    	return $this->belongsTo('App\superadmin\Root_categorie','root_cat_id');
    }

    public static function getByRestaurant($restaurant_id) {
        return self::where("status", 1)->where("rest_detail_id", $restaurant_id)->get();
    }
    
}
