<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class City extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function state()
    {
        return $this->belongsTo('App\superadmin\State', 'state_id');
    }

    public static function getByZipcodeAndName($zipcode, $name)
    {
        $city = City::
        join("zipcodes", "zipcodes.city_id", "=", "cities.id")->
        join("states", "states.id", "=", "cities.state_id")->
        join("countries", "countries.id", "=", "states.country_id")->
        where("zipcodes.name", $zipcode)->
        where("cities.name", $name)->
        first();
        return $city;
    }

    public static function getCoordinates($address, $zipcode_name, $city_name)
    {
        $address = $address . ", " . $zipcode_name . " " . $city_name;
        $url_encoded_address = str_replace(' ', '+', $address);
        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $url_encoded_address . '&sensor=false');
        $output = json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;
        return [
            "latitude" => $latitude,
            "longitude" => $longitude
        ];
    }
}
