<?php

namespace App\superadmin;

use App\front\UserRestFavourite;
use Illuminate\Database\Eloquent\Model;

class RestZipcodeDeliveryMap extends Model
{
    protected $table="rest_zipcode_delivery_maps";

    public function rest_data()
    {
        return $this->belongsTo('App\superadmin\RestDetail','rest_id');
    }

    public static function rest_kitchen($id)
    {
        $kitchen = RestKitchenMap::select('kitchen_id')->where('rest_detail_id',$id)->get();
        //echo($kitchen);exit;
        return $kitchen;
    }

    public static function rest_setting($id)
    {
        $restset = RestSetting::where('rest_detail_id',$id)->get();
        return $restset->toArray();
    }

    public static function rest_time($id,$day)
    {
        $time = RestDayTimeMap::where('rest_detail_id',$id)->where('day_id',$day)->get();
        return $time->toArray();
    }

    public static function rest_fav($id)
    {
        $fav = UserRestFavourite::where('rest_id',$id)->get();
        return $fav->toArray();
    }

    public static function rest_user_fav($rid,$uid)
    {
        $fav = UserRestFavourite::where('rest_id',$rid)->where('user_id',$uid)->get();
        if(count($fav) > 0)
            return $fav->toArray();
        else
            return $fav;
    }

    public static function getRestaurantDeliveredZipcodes($restaurant_id) {
        return self::where("zipcodes.status", 1)->where("rest_zipcode_delivery_maps.status", 1)->where("rest_id", $restaurant_id)->
        join("zipcodes", "rest_zipcode_delivery_maps.zipcode_id", "=", "zipcodes.id")->
        orderBy("zipcode")->get();
    }

}
