<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class TaxSetting extends Model
{
    protected $table="tax_settings";
}
