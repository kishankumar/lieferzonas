<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\superadmin\RestNotificationMap;
class RestNotification extends Model
{
     protected $table="rest_notifications";
	public static function restnotification()
    {
		
		$res_owner_id = Auth::User('admin')->id;
		$ownerinfo = DB::table('rest_owners')->select('rest_detail_id')->where('id','=',$res_owner_id)->get();
		for ($i = 0, $c = count($ownerinfo); $i < $c; ++$i) {
             $ownerinfo[$i] = (array) $ownerinfo[$i];
             
             }
		$rest_id = $ownerinfo['0']['rest_detail_id'];
	    $not_read_noti = DB::table('rest_notifications')
        ->select('id','title','comment')
        ->where('status','!=','2')->where('send_type','=','1')->where('is_type','=','1')->where('is_read','=','0');
        

       
        
         $not_read_noti_count = DB::table('rest_notifications')
         ->leftjoin('rest_notification_maps', 'rest_notification_maps.notification_id', '=', 'rest_notifications.id')
           ->select('rest_notifications.id','rest_notifications.title','rest_notifications.comment')
           ->where('rest_notifications.status','!=','2')->where('rest_notifications.send_type','=','2')
           ->where('rest_notifications.is_type','=','1')
		   ->where('rest_notifications.is_read','=','0')
		   ->where('rest_notification_maps.rest_detail_id','=',$rest_id)
            ->union($not_read_noti)
            ->get();     
		$not_read_noti_count = count($not_read_noti_count); 
		return $not_read_noti_count;
	}
}
