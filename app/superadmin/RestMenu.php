<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\superadmin\RestMenuSpiceMap;

class RestMenu extends Model
{
    public function getResturant()
    {
    	return $this->belongsTo('App\superadmin\RestDetail','rest_detail_id');
    }

    public function getCategory()
    {
    	return $this->belongsTo('App\superadmin\RestCategory','rest_category_id');
    }

    public function getKitchen()
    {
    	return $this->belongsTo('App\superadmin\Kitchen','kitchen_id');
    }

    public static function getMenuByCategory($id){
        $manuName=RestMenu::where('rest_category_id',$id)->get();
        return $manuName->toArray();

    }

    public static function getMenuPrice($id){
        $manuPrice=DB::table('menu_price_maps')->where('menu_id',$id)->where('price_type_id',1)->get();
        return $manuPrice;
    }

    public static function getMenuAlergicData($id){
        $menuAlergic=DB::table('rest_menu_allergic_maps')->where('menu_id',$id)->select('allergic_id')->get();
        return $menuAlergic;
    }

    public static function getMenuSpicesData($id){
        $menuSpice=RestMenuSpiceMap::where('menu_id',$id)->select('spice_id')->get();
        return $menuSpice;
    }
    
    //functions added by vikas
    public static function getMenuDeliveryTakePrice($id,$priceTypeId,$restId){
        $menuPrice=DB::table('menu_price_maps')->where('rest_detail_id',$restId)->where('menu_id',$id)->where('price_type_id',$priceTypeId)->first();
        if(count($menuPrice)){
            return number_format($menuPrice->price,2);
        }else{
            return 0;
        }
    }
    public static function getSubMenuDeliveryTakePrice($id,$priceTypeId,$restId){
        $submenuPrice=DB::table('sub_menu_price_maps')->where('rest_detail_id',$restId)->where('sub_menu_id',$id)->where('price_type_id',$priceTypeId)->first();
        if(count($submenuPrice)){
            return number_format($submenuPrice->price,2);
        }else{
            return 0;
        }
    }
    //functions added by vikas
	
	public static function Specialofferlist()
	{
       $sevendays_ago_date = date('Y-m-d', strtotime('+8 days', strtotime(date('Y-m-d'))));
       $specialoffer_list = DB :: table('special_offers')->where('special_offers.status','!=',2)
	   ->where('special_offer_day_time_maps.status','!=',2)
	   ->leftjoin('special_offer_day_time_maps', 'special_offer_day_time_maps.special_offer_id', '=', 'special_offers.id')
	   ->leftjoin('rest_days', 'rest_days.id', '=', 'special_offer_day_time_maps.day_id')
	   ->whereBetween( DB::raw('date(lie_special_offers.valid_to)'), [date('Y-m-d'),$sevendays_ago_date ] )
	   ->select('special_offers.name','special_offers.discount_price','special_offers.original_price',
	   'special_offer_day_time_maps.time_from','special_offer_day_time_maps.time_to','rest_days.day')->get();
	    for ($i = 0, $c = count($specialoffer_list); $i < $c; ++$i) {
            $specialoffer_list[$i] = (array) $specialoffer_list[$i];
        }
	   return $specialoffer_list;
    }
	public static function getrestemail($id)
	{
		$getrestemail = DB :: table('rest_email_maps')->where('rest_detail_id','=',$id)->where('is_primary','=',1)
		->where('status','!=',2)->select('email')->get();
		for ($i = 0, $c = count($getrestemail); $i < $c; ++$i) {
            $getrestemail[$i] = (array) $getrestemail[$i];
        }
	    return $getrestemail;
	}
    
}
