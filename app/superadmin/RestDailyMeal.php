<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestDailyMeal extends Model
{

    public function getResturant()
    {
        return $this->belongsTo('App\superadmin\RestDetail','rest_id');
    }

}
