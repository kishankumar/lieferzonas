<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestKitchenMap extends Model
{

    public function kitchen(){

    	return $this->belongsTo('app\superadmin\Kitchen');
    }

    public function getKitchen()
    {
    	return $this->belongsTo('App\superadmin\Kitchen','kitchen_id');
    }

    public static function getRestaurantKitchenArray($restaurant_array) {
        $rest_kitchen_rows = RestKitchenMap::where("rest_kitchen_maps.status", 1)->whereIn("rest_detail_id", $restaurant_array)->get();
        $rest_kitchen_maps = array();
        foreach($rest_kitchen_rows as $row) {
            $rest = $row["rest_detail_id"];
            $kitchen = $row["kitchen_id"];
            if (array_key_exists($rest, $rest_kitchen_maps)) {
                array_push($rest_kitchen_maps[$rest], $kitchen);
            }
            else {
                $rest_kitchen_maps[$rest] = [$kitchen, ];
            }
        }
        return $rest_kitchen_maps;
    }

    public static function getByRestaurant($restaurant_id) {
        return self::join("kitchens", "kitchen_id", "=", "kitchens.id")->where("rest_detail_id", $restaurant_id)->get();
    }

}
