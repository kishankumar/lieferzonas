<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $table="email_templates";

    public static function getNewAccountPasswordText($email, $password) {
        $row = self::find(5);
        $text = $row->text;
        $text = str_replace("%EMAIL%", $email, $text);
        $text = str_replace("%PASSWORD%", $password, $text);
        return $text;
    }

}
