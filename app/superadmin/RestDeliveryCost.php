<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestDeliveryCost extends Model
{
    public static function getRestaurantDeliveryCostRow($restaurant_id) {
        return RestDeliveryCost::where("status", 1)->where("restaurant", $restaurant_id)->first();
    }

    public static function getRestaurantDeliveryCosts($restaurant_id) {
        $delivery_cost_rows = RestDeliveryCost::where("status", 1)->where("restaurant", $restaurant_id)->get();
        if ($delivery_cost_rows->count() != 1) {
            return null;
        }
        else {
            return [
                $delivery_cost_rows[0]->minimum_order_amount,
                $delivery_cost_rows[0]->below_order_delivery_cost,
                $delivery_cost_rows[0]->above_order_delivery_cost
            ];
        }
    }

    public static function getDeliveryCostRanges($restaurant_ids) {
        $delivery_cost_rows = RestDeliveryCost::where("status", 1)->whereIn("restaurant", $restaurant_ids)->get();
        if ($delivery_cost_rows->count() == 0) {
            return null;
        }
        $min_minimum_order = PHP_INT_MAX;
        $max_minimum_order = 0;
        $min_delivery_cost = PHP_INT_MAX;
        $max_delivery_cost = 0;
        foreach($delivery_cost_rows as $row) {
            $min_minimum_order = min($min_minimum_order, $row->minimum_order_amount);
            $max_minimum_order = max($max_minimum_order, $row->minimum_order_amount);
            $min_delivery_cost = min($min_delivery_cost, $row->above_order_delivery_cost);
            $max_delivery_cost = max($max_delivery_cost, $row->above_order_delivery_cost);
        }
        return [
            "min_minimum_order" => $min_minimum_order,
            "max_minimum_order" => $max_minimum_order,
            "min_delivery_cost" => $min_delivery_cost,
            "max_delivery_cost" => $max_delivery_cost
        ];
    }
}
