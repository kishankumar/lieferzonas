<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class ExtraChoiceElement extends Model
{
    protected $table = "extra_choice_element";

    public static function getByExtraChoice($extra_choice_id) {
        return ExtraChoiceElement::where("extra_choice_id", $extra_choice_id)->get();
    }
}
