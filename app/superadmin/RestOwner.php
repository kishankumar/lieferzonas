<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use Auth;
class RestOwner extends Model
{
    protected $table="rest_owners";
    public static function adminuserdetail()
    {
        $auth_id = Auth::User('admin')->id;
        $adminuserdetail = RestOwner::where('id',$auth_id)->get();
        return  $adminuserdetail;
    }
    public static function getAdminSettingsData()
    {
        $restro_detail_id = Auth::User('admin')->rest_detail_id;
        $adminuserdetail = RestSetting::where('rest_detail_id',$restro_detail_id)->where('status',1)->first();
        return  $adminuserdetail;
    }
    public static function getRestaurantDetail(){
        $restro_detail_id = Auth::User('admin')->rest_detail_id;
        $restDetail = RestDetail::where('id',$restro_detail_id)->where('status',1)->first();
        return  $restDetail;
    }
    public static function getTimeSettingsData(){
        $dayname = date('l');
        $days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
        $day_number = array_search($dayname, $days);
        $restroId = Auth::User('admin')->rest_detail_id;
        $dayTime = RestDayTimeMap:: where('rest_detail_id','=',$restroId)->where('status','1')->where('day_id','=',$day_number)->first();
        if(count($dayTime)){
            if($dayTime->is_open ==1){
                $open_time  = $dayTime->open_time;
                $close_time  = $dayTime->close_time;
                $currentTime = date('H:i A');
                $open_time = strtotime("1/1/1980 $open_time"); 
                $close_time = strtotime("1/1/1980 $close_time"); 
                $currentTime = strtotime("1/1/1980 $currentTime"); 
                if ($currentTime >= $open_time && $currentTime <= $close_time) {
                    return 'open';
                } else {
                    return 'notopen';
                }
            }else{
                return 'notopen';
            }
        }else{
            return 'notopen';
        }
    }

    public static function getByRestaurantId($restaurant_id) {
        return self::where("status", 1)->where("rest_detail_id", $restaurant_id)->first();
    }
}
