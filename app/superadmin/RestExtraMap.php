<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class RestExtraMap extends Model
{
    public static function getExtrasByResturant($restId){
        $extras=DB::table('rest_extra_maps')
            ->join('menu_extras', 'rest_extra_maps.menu_extra_id', '=', 'menu_extras.id')
            ->select('menu_extras.id','menu_extras.name','menu_extras.price')
            ->where('rest_extra_maps.rest_detail_id',$restId)
            ->where('rest_extra_maps.status','1')
            ->get();
        return $extras;
    }
}
