<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class MenuCategory extends Model
{
    protected $table="menu_categories";
}
