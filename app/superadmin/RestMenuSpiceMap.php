<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestMenuSpiceMap extends Model
{
    public function getSpiceData()
    {
        return $this->belongsTo('App\superadmin\Spice','spice_id');
    }
}
