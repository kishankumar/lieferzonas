<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestNotificationMap extends Model
{
    protected $table="rest_notification_maps";
}
