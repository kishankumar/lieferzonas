<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class FrontCityMap extends Model
{
    public function city()
    {
        return $this->belongsTo('App\superadmin\City','city_id');
    }
}
