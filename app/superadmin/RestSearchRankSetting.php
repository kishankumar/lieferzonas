<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestSearchRankSetting extends Model
{
    protected $table="rest_search_rank_settings";
}
