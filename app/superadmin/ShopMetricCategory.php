<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class ShopMetricCategory extends Model
{
    protected $table="shop_metric_categories";
}
