<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class DiscountType extends Model
{
     protected $table="discount_types";
}
