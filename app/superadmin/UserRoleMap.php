<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class UserRoleMap extends Model
{
    public function role()
    {
        return $this->belongsTo('App\superadmin\UserRole','user_role_id');
    }
}
