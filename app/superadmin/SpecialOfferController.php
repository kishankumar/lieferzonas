<?php

namespace App\Http\Controllers\superadmin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Auth;
use Input;
use validator;
use App\superadmin\OrderService;
use App\superadmin\DiscountType;
use App\admin\SpecialOffer;
use App\admin\SpecialOfferCatMap;
use App\admin\SpecialOfferDayMap;
use App\admin\SpecialOfferDayTimeMap;
use App\admin\SpecialOfferMenuMap;
use App\admin\SpecialOfferSubmenuMap;
use App\admin\SpecialOfferOrderService;
class SpecialOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $specialoffer = DB::table('special_offers')
            ->leftjoin('rest_details', 'special_offers.rest_id', '=', 'rest_details.id')
            ->where('special_offers.status','!=', '2')
           
            ->select('special_offers.*', 'rest_details.f_name','rest_details.l_name')
         
            ->paginate(10);
             for ($i = 0, $c = count($specialoffer); $i < $c; ++$i) {
             $specialoffer[$i] = (array) $specialoffer[$i];
             
             }
     

       return view('superadmin.specialoffer.index')->with('specialoffers',$specialoffer);      
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
     $specialoffer = DB::table('special_offers')->select('*')->where('id','=',$id)->get();
        
             
        for ($i = 0, $c = count($specialoffer); $i < $c; ++$i) {
            $specialoffer [$i] = (array) $specialoffer [$i];
            
        }  

		$time_val = DB::table('time_validations')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($time_val); $i < $c; ++$i) {
            $time_val[$i] = (array) $time_val[$i];
            
        } 
		$dis_type = DB::table('discount_types')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($dis_type); $i < $c; ++$i) {
            $dis_type[$i] = (array) $dis_type[$i];
            
        } 
		$order_service = DB::table('order_services')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($order_service); $i < $c; ++$i) {
            $order_service[$i] = (array) $order_service[$i];
            
        } 
		$categories = DB::table('rest_categories')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($categories); $i < $c; ++$i) 
        {
            $categories[$i] = (array) $categories[$i];

            
        } 
        //print_r($categories); 

        $menus = DB::table('rest_menus')->select('id', 'name')->where('rest_category_id',1)->where('status','=','1')->get();
        
             
            for ($i = 0, $c = count($menus); $i < $c; ++$i) {
            $menus[$i] = (array) $menus[$i];
            
        } 
		$day = DB::table('rest_days')->select('id', 'day')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($day); $i < $c; ++$i) {
            $day[$i] = (array) $day[$i];
            
        } 
        $res_owner_id = Auth::User()->id;
		$rest_id = DB::table('rest_owners')->select('rest_detail_id')->where('id','=',$res_owner_id)->get();
		 for ($i = 0, $c = count($rest_id); $i < $c; ++$i) {
            $rest_id[$i] = (array) $rest_id[$i];
             
        }
        
        $selected_cat=SpecialOfferCatMap::where('special_offer_id','=',$id)->where('status','!=','2')->lists('category_id');		
        $selected_cat= $selected_cat->toArray();
        
        //print_r($selected_cat);

        


        $submenus = DB::table('rest_sub_menus')->select('id', 'name')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($submenus); $i < $c; ++$i) {
            $submenus[$i] = (array) $submenus[$i];
            
        }


             $listcat = DB::table('special_offer_cat_maps')
		   ->leftjoin('rest_categories', 'special_offer_cat_maps.category_id', '=', 'rest_categories.id')
           ->select('rest_categories.name','rest_categories.id')
           ->where('special_offer_cat_maps.status','=','1')
           ->where('special_offer_cat_maps.special_offer_id','=',$id)->get();

           for ($i = 0, $c = count($listcat); $i < $c; ++$i) {
            $listcat[$i] = (array) $listcat[$i];
            
            } 



            $listmenu = DB::table('special_offer_menu_maps')
		   ->leftjoin('rest_menus', 'special_offer_menu_maps.menu_id', '=', 'rest_menus.id')
		  ->leftjoin('rest_categories', 'special_offer_menu_maps.category_id', '=', 'rest_categories.id')
           ->select('special_offer_menu_maps.*','rest_menus.name as menuname','rest_categories.name as catname')
           ->where('special_offer_menu_maps.status','=','1')
           ->where('special_offer_menu_maps.special_offer_id','=',$id)->get();

           for ($i = 0, $c = count($listmenu); $i < $c; ++$i) {
            $listmenu[$i] = (array) $listmenu[$i];
            
            } 
            
              //print_r($listmenu); die;

            $listsubmenu = DB::table('special_offer_submenu_maps')
		   ->leftjoin('rest_menus', 'special_offer_submenu_maps.menu_id', '=', 'rest_menus.id')
		  ->leftjoin('rest_categories', 'special_offer_submenu_maps.category_id', '=', 'rest_categories.id')
		   ->leftjoin('rest_sub_menus', 'special_offer_submenu_maps.sub_menu_id', '=', 'rest_sub_menus.id')
           ->select('special_offer_submenu_maps.*','rest_menus.name as menuname','rest_categories.name as catname','rest_sub_menus.name as submenuname')
           ->where('special_offer_submenu_maps.status','=','1')
            ->where('special_offer_submenu_maps.special_offer_id','=',$id)->get();


           for ($i = 0, $c = count($listsubmenu); $i < $c; ++$i) {
           $listsubmenu[$i] = (array) $listsubmenu[$i];
            
            } 


        $selected_menu=SpecialOfferMenuMap::where('special_offer_id','=',$id)->where('status','!=','2')->select('category_id','menu_id')->get();		
        $selected_menu= $selected_menu->toArray();
     

        $selected_submenu=SpecialOfferSubmenuMap::where('special_offer_id','=',$id)->where('status','!=','2')->select('category_id','menu_id','sub_menu_id')->get();		
        $selected_submenu= $selected_submenu->toArray();
        //print_r($selected_submenu); 
        //echo count($selected_submenu);
         //die;
      

        $selected_order=SpecialOfferOrderService::where('special_offer_id','=',$id)->where('status','!=','2')->lists('order_service_id');		
        $selected_order= $selected_order->toArray();

       
        $selected_day=SpecialOfferDayMap::where('special_offer_id','=',$id)->where('status','!=','2')->lists('day_id');		
        $selected_day= $selected_day->toArray();
        $selected_daytime=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->lists('day_id');		
        $selected_daytime= $selected_daytime->toArray();

        $selected_daytimeall='';
        $selected_daytimeall = array_merge($selected_day,$selected_daytime);

        $gettimemon=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->where('day_id','=','1')->get();		
        $gettimemon=  $gettimemon->toArray();
        $gettimetues=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->where('day_id','=','2')->get();		
        $gettimetues=  $gettimetues->toArray();
        $gettimewed=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->where('day_id','=','3')->get();		
        $gettimewed=  $gettimewed->toArray();
        $gettimethus=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->where('day_id','=','4')->get();		
        $gettimethus=  $gettimethus->toArray();
        $gettimefri=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->where('day_id','=','5')->get();		
        $gettimefri=  $gettimefri->toArray();
        $gettimesat=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->where('day_id','=','6')->get();		
        $gettimesat=  $gettimesat->toArray();
        $gettimesun=SpecialOfferDayTimeMap::where('special_offer_id','=',$id)->where('status','!=','2')->where('day_id','=','7')->get();		
        $gettimesun=  $gettimesun->toArray();
        //print_r( $gettimemon);
        //echo count($gettimemon);
        //$selected_daytimeall='';
        //$selected_daytimeall = array_merge($selected_day,$selected_daytime);

         $fromtime = array();$totime = array();$toTime=1;
         $fromtime['']="Select";$totime['']="Select";
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            $fromtime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            $fromtime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        foreach (range(0,23) as $fullhour) 
        {
            $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour;
            $sufix = $fullhour > 11 ? " PM" : " AM";
            if($parthour<10){
                $parthour = '0'.$parthour;
            }
            if($toTime>1){
                $totime["$parthour:00$sufix"] = $parthour.":00".$sufix;
            }$toTime=2;
            $totime["$parthour:30$sufix"] = $parthour.":30".$sufix;
        }
        $totime['11:59 PM'] = '11:59 PM';

         return view('superadmin/specialoffer/editspecialoffer')->with('dis_types',$dis_type)->with('order_services',$order_service)
		->with('time_validations',$time_val)->with('categories',$categories)->with('days',$day)->with('rest_id',$rest_id)
		->with('specialoffer',$specialoffer)->with('selected_order',$selected_order)
		->with('selected_daytimeall',$selected_daytimeall)->with('selected_cat',$selected_cat)->with('selected_menu' ,$selected_menu)->with('selected_submenu',$selected_submenu)->with('menus' ,$menus)->with('submenus',$submenus)->with('gettimemon',$gettimemon)->with('gettimetues',$gettimetues)->with('gettimewed',$gettimewed)->with('gettimethus',$gettimethus)->with('gettimefri',$gettimefri)->with('gettimesat',$gettimesat)->with('gettimesun',$gettimesun)
		->with('fromtime',$fromtime)->with('totime',$totime)->with('listcat',$listcat)->with('listmenu',$listmenu)->with('listsubmenu',$listsubmenu);
    }
	
	 public function delete(Request $request)
    {
		
        if($request->ajax()){
            $data = Input::all();
            $selectedOffers = $data['selectedOffers'];
        }else{
            $json['success']=0;
            echo json_encode($json);
            return;
        }
        foreach($selectedOffers as $key => $value)
        {
            $user = SpecialOffer::find($value);
            $user->status = '2';
            $user->save();
        }
        Session::flash('message', 'Deleted Successfully!'); 
        $json['success']=1;
        echo json_encode($json);
        return;
    }

	public function changeStatus(Request $request)
    {
        if($request->ajax())
        {
            $status = $request['status'];
            $id = $request['id'];
            $offer = SpecialOffer::find($id);
            $offer->status = $status;
            $offer->save();
            $json['success']=1;
            echo json_encode($json);
        }
        else
        {
            $json['success']=0;
            echo json_encode($json);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function update(Request $request, $id)
    {
            $auth_id = Auth::User()->id; 
	    
		
			$specialoffer = SpecialOffer::find($id);

			$specialoffer->discount_type_id=$request->discount_type;
			$specialoffer->discount_type_value=$request->discount;
			$specialoffer->original_price=$request->original_price;
		    $specialoffer->discount_price=$request->discount_price;
		    $specialoffer->type=$request->type;
		    $specialoffer->validity_type=$request->validity;
				if($request->validity=='T')
				{
				  $validity_table =$request->validity_table;
				   
				   $valid_from = date("Y-m-d H:i:s");
				   $time_validation = DB::table('time_validations')->select('year','month','week' ,'day')->where('status','=','1')->where('id','=',$validity_table)->get();
				
					 
				   for ($i = 0, $c = count($time_validation); $i < $c; ++$i) {
					$time_validation[$i] = (array) $time_validation[$i];
					
				   } 
				   $year = $time_validation['0']['year'];
				   $month = $time_validation['0']['month'];
				   $week = $time_validation['0']['week'];
				   $day = $time_validation['0']['day'];
				   $totaltime = ($year*365) + ($month*30) + ($week*7) + ($day);
				   $valid_to = date('Y-m-d', strtotime("+".$totaltime."days"));
				   $specialoffer->validity_table=$validity_table;
				   $specialoffer->valid_from=$valid_from;
				   $specialoffer->valid_to=$valid_to;
				}
				 
			  else
				{
					 $specialoffer->validity_table='0';
					 $specialoffer->valid_from=$request->valid_from;
					 $specialoffer->valid_to=$request->valid_to;
				}
				
				$specialoffer->updated_by=$auth_id;
				$specialoffer->updated_at=date("Y-m-d H:i:s");
				
				$specialoffer->save();
				$orderservices = array();
                $orderservices = $request->order_service;

		$countorderservice= SpecialOfferOrderService::where('special_offer_id', '=', $id)->where('status','=','1')->get();
        $countorderservice = count($countorderservice->toArray());
        if($countorderservice!= null)
        {
            $affected_data = DB::table('special_offer_order_services')
            ->where('special_offer_id', '=', $id)
            ->where('status', '=', '1')
            ->update(array('status' => '2'));
        }
        $restlist = array();
        $restlist = Input::get('restlist');

		foreach($orderservices as $orderservice)
		{
		   $specialofferorder = new SpecialOfferOrderService;
		   $specialofferorder->special_offer_id=$id;
		   $specialofferorder->order_service_id= $orderservice;
		  
		   $specialofferorder->updated_by=$auth_id;
		   $specialofferorder->updated_at=date("Y-m-d H:i:s");
		   $specialofferorder->status='1';
		   $specialofferorder->save();
		}


		    $countcat= SpecialOfferCatMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countcat = count($countcat->toArray());
	        if($countcat!= null)
	        {
	             $affected_data = DB::table('special_offer_cat_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }


		   $category1 = array();
		   $category1= $request->catid1;
		   if(count($category1)>0)
		   {

		   	foreach($category1 as $categories1)
			  {
			   	   $specialoffercat = new SpecialOfferCatMap;
				   $specialoffercat->special_offer_id=$id;
				   $specialoffercat->category_id=$categories1; 

				   $specialoffercat->created_by=$auth_id;
				   $specialoffercat->created_at=date("Y-m-d H:i:s");
				   $specialoffercat->status='1';
				   $specialoffercat->save();
			   }
		   }
			  



		    $countmenu= SpecialOfferMenuMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countmenu = count($countmenu->toArray());
	        if($countmenu!= null)
	        {
	             $affected_data = DB::table('special_offer_menu_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }

	       $category2 = array();
		   $category2= $request->catid2;
		   $menu2 = array();
		   $menu2= $request->menuid2;
		   if(count($category2)>0)
		   {
			    for($i=0; $i< count($category2);$i++)
			   {
			   	//echo $menu2[$i];
			   $specialoffermenu = new SpecialOfferMenuMap;
			   $specialoffermenu ->special_offer_id=$id;
			   $specialoffermenu ->category_id=$category2[$i]; 
			   $specialoffermenu ->menu_id=$menu2[$i]; 
			   $specialoffermenu->created_by=$auth_id;
			   $specialoffermenu->created_at=date("Y-m-d H:i:s");
			   $specialoffermenu->status='1';
			   $specialoffermenu ->save();
			  }
           }
            $countsubmenu= SpecialOfferSubmenuMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countsubmenu = count($countsubmenu->toArray());
	        if($countsubmenu!= null)
	        {
	             $affected_data = DB::table('special_offer_submenu_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }

		   $category3 = array();
		   $category3= $request->catid3;
		   $menu3 = array();
		   $menu3= $request->menuid3;
		   $submenu3 = array();
		   $submenu3= $request->submenuid3;
		 

            if(count($category3)>0)
		    {
		        for($i=0; $i<count($category3);$i++)
			   {
			   $specialoffersubmenu = new SpecialOfferSubmenuMap;
			   $specialoffersubmenu->special_offer_id=$id;
			   $specialoffersubmenu->category_id=$category3[$i]; 
			   $specialoffersubmenu->menu_id=$menu3[$i]; 
			   $specialoffersubmenu->sub_menu_id=$submenu3[$i]; 
			   $specialoffersubmenu->created_by=$auth_id;
			   $specialoffersubmenu->created_at=date("Y-m-d H:i:s");
			   $specialoffersubmenu->status='1';
			   $specialoffersubmenu ->save();
			  }
		
            }
	      
            $countdaytime= SpecialOfferDayTimeMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countdaytime = count($countdaytime->toArray());
	        //echo $countdaytime; die;
	        if($countdaytime!= null)
	        {
	             $affected_data = DB::table('special_offer_day_time_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }
	        $countday= SpecialOfferDayMap::where('special_offer_id', '=', $id)->where('status','=','1')->get();
	        $countday = count($countday->toArray());
	        //echo $countdaytime; die;
	        if($countday!= null)
	        {
	             $affected_data = DB::table('special_offer_day_maps')
	            ->where('special_offer_id', '=', $id)
	            ->where('status', '=', '1')
	            ->update(array('status' => '2'));
	        }
	        $day = array();
            $day = Input::get('days');
          //print_r($day);
          //echo count($day);
	    
	    for($i=0; $i<count($day); $i++ )
         {


         	
	        
			 $start_times = array();
			 $start_times = Input::get('start_time'.$day[$i]);
			 //echo count($start_times);
			 //print_r($start_times);
			 $end_times= array();
			 $end_times = Input::get( 'end_time'.$day[$i]);
			 if( $start_times['0']=='' &&  $end_times['0']=='')
			 {
				 $specialofferdaymap = new SpecialOfferDayMap;
				 $specialofferdaymap->special_offer_id=$id;
				 $specialofferdaymap->day_id=$day[$i];
				 $specialofferdaymap->updated_by=$auth_id;
		         $specialofferdaymap->updated_at=date("Y-m-d H:i:s");
				 $specialofferdaymap->status='1';
				 $specialofferdaymap->save();
			 }
			 else
			 {
			
			 	
				 for($j=0; $j<count($start_times); $j++ )
                 {
                 	 
                 	 $specialofferdaytimemap = new SpecialOfferDayTimeMap;
				     $specialofferdaytimemap->special_offer_id=$id;
				     $specialofferdaytimemap->day_id=$day[$i];
                 	
					 $specialofferdaytimemap->time_from=$start_times[$j]; 
					 $specialofferdaytimemap->time_to=$end_times[$j]; 
					 $specialofferdaytimemap->created_by=$auth_id;
					 $specialofferdaytimemap->created_at=date("Y-m-d H:i:s");
					 $specialofferdaytimemap->status='1';
					 $specialofferdaytimemap->save();
					
				 }
			}	  
			
		 }
		 return redirect(route('root.specialoffer.index'));
	   
    }
	
	
	public function getofferDetail($specialofferId=0)
    {

            $specialoffer = DB::table('special_offers')
            ->leftjoin('discount_types', 'special_offers.discount_type_id', '=', 'discount_types.id')
           ->where('special_offers.id','=',$specialofferId)
           ->select('special_offers.*','discount_types.name as discount_type_name')->get();
              for ($i = 0, $c = count($specialoffer); $i < $c; ++$i) {
             $specialoffer[$i] = (array) $specialoffer[$i];
             
             }
             
            $orderservice = DB::table('special_offer_order_services')
            ->leftjoin('order_services', 'special_offer_order_services.order_service_id', '=', 'order_services.id')
            ->where('special_offer_order_services.special_offer_id','=',$specialofferId)
            ->where('special_offer_order_services.status','!=',2)
            ->select('special_offer_order_services.order_service_id','order_services.name as order_service_name')->get();

              for ($i = 0, $c = count($orderservice); $i < $c; ++$i) {
             $orderservice[$i] = (array) $orderservice[$i];
             
             }


              $listcat = DB::table('special_offer_cat_maps')
		   ->leftjoin('rest_categories', 'special_offer_cat_maps.category_id', '=', 'rest_categories.id')
           ->select('rest_categories.name','rest_categories.id')
           ->where('special_offer_cat_maps.status','=','1')
           ->where('special_offer_cat_maps.special_offer_id','=',$specialofferId)->get();

           for ($i = 0, $c = count($listcat); $i < $c; ++$i) {
            $listcat[$i] = (array) $listcat[$i];
            
            } 



            $listmenu = DB::table('special_offer_menu_maps')
		   ->leftjoin('rest_menus', 'special_offer_menu_maps.menu_id', '=', 'rest_menus.id')
		  ->leftjoin('rest_categories', 'special_offer_menu_maps.category_id', '=', 'rest_categories.id')
           ->select('special_offer_menu_maps.*','rest_menus.name as menuname','rest_categories.name as catname')
           ->where('special_offer_menu_maps.status','=','1')
           ->where('special_offer_menu_maps.special_offer_id','=',$specialofferId)->get();

           for ($i = 0, $c = count($listmenu); $i < $c; ++$i) {
            $listmenu[$i] = (array) $listmenu[$i];
            
            } 
            
              //print_r($listmenu); die;

            $listsubmenu = DB::table('special_offer_submenu_maps')
		   ->leftjoin('rest_menus', 'special_offer_submenu_maps.menu_id', '=', 'rest_menus.id')
		  ->leftjoin('rest_categories', 'special_offer_submenu_maps.category_id', '=', 'rest_categories.id')
		   ->leftjoin('rest_sub_menus', 'special_offer_submenu_maps.sub_menu_id', '=', 'rest_sub_menus.id')
           ->select('special_offer_submenu_maps.*','rest_menus.name as menuname','rest_categories.name as catname','rest_sub_menus.name as submenuname')
           ->where('special_offer_submenu_maps.status','=','1')
            ->where('special_offer_submenu_maps.special_offer_id','=',$specialofferId)->get();


           for ($i = 0, $c = count($listsubmenu); $i < $c; ++$i) {
           $listsubmenu[$i] = (array) $listsubmenu[$i];
            
            } 

        
		$day = DB::table('rest_days')->select('id', 'day')->where('status','=','1')->get();
        
             
        for ($i = 0, $c = count($day); $i < $c; ++$i) {
            $day[$i] = (array) $day[$i];
            
        } 
		
        $selected_day=SpecialOfferDayMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->lists('day_id');     
        $selected_day= $selected_day->toArray();
        $selected_daytime=SpecialOfferDayTimeMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->lists('day_id');     
        $selected_daytime= $selected_daytime->toArray();

        $selected_daytimeall='';
        $selected_daytimeall = array_merge($selected_day,$selected_daytime);
        //print_r($selected_daytimeall); 

        $gettimemon=SpecialOfferDayTimeMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->where('day_id','=','1')->get();        
        $gettimemon=  $gettimemon->toArray();
        $gettimetues=SpecialOfferDayTimeMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->where('day_id','=','2')->get();       
        $gettimetues=  $gettimetues->toArray();
        $gettimewed=SpecialOfferDayTimeMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->where('day_id','=','3')->get();        
        $gettimewed=  $gettimewed->toArray();
        $gettimethus=SpecialOfferDayTimeMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->where('day_id','=','4')->get();       
        $gettimethus=  $gettimethus->toArray();
        $gettimefri=SpecialOfferDayTimeMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->where('day_id','=','5')->get();        
        $gettimefri=  $gettimefri->toArray();
        $gettimesat=SpecialOfferDayTimeMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->where('day_id','=','6')->get();        
        $gettimesat=  $gettimesat->toArray();
        $gettimesun=SpecialOfferDayTimeMap::where('special_offer_id','=',$specialofferId)->where('status','!=','2')->where('day_id','=','7')->get();        
        $gettimesun=  $gettimesun->toArray();
        return view('superadmin.specialoffer.specialoffer_detail')->with('specialoffer',$specialoffer)
		->with('orderservice',$orderservice)->with('listcat',$listcat)->with('listmenu',$listmenu)->with('days',$day)
		->with('listsubmenu',$listsubmenu)->with('gettimemon',$gettimemon)->with('gettimetues',$gettimetues)
		->with('gettimewed',$gettimewed)->with('gettimethus',$gettimethus)->with('gettimefri',$gettimefri)
		->with('gettimesat',$gettimesat)->with('gettimesun',$gettimesun)->with('selected_daytimeall',$selected_daytimeall);
    }
    
     public function set_validation($id=null,$request)
     {
        $message=array(
            "coupon_code.required"=>"Coupon code is required",
            "min_amount.required"=>"Min amount is required",
            );

        $this->validate($request,[
        'coupon_code' => 'required|unique:promocodes,coupon_code,'.$id.',id',
        'min_amount' => 'required',
        ],$message);
     }
}
