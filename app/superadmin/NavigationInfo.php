<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class NavigationInfo extends Model
{
    protected $table="navigation_infos";
}
