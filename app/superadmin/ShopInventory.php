<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class ShopInventory extends Model
{
    protected $table="shop_inventories";
    public function shop_category(){
    	return $this->belongsTo('App\superadmin\ShopCategory','shop_category_id');
    }
    public function item_name(){
    	return $this->belongsTo('App\superadmin\ShopItem','shop_item_id');
    }
    public function metric_name(){
    	return $this->belongsTo('App\superadmin\Metric','metric_id');
    }
}
