<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestRootCatMap extends Model
{
    public function getRootCategory()
    {
    	return $this->belongsTo('App\superadmin\Root_categorie','root_cat_id');
    }
}
