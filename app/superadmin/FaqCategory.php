<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
