<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function country()
    {
        return $this->belongsTo('App\superadmin\Country','country_id');
    }

}
