<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class FrontPage extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
