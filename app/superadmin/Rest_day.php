<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\admin\SpecialOfferDayTimeMap;
class Rest_day extends Model
{
     protected $table="rest_days";
	 public static function daytime($day_id,$special_offer_id)
     {
		$daytime=SpecialOfferDayTimeMap::where('special_offer_id','=',$special_offer_id)->where('status','!=','2')
		->where('day_id','=',$day_id)->orderby('day_id')
		->get();		
        $daytime =  $daytime->toArray();
		//print_r($daytime); 
		return $daytime;
     }
	
	 public static function dayname($day_id)
     {
		$dayname=Rest_day::where('id','=',$day_id)->where('status','!=','2')->get();		
        $dayname =  $dayname->toArray();
		//print_r($dayname); die;
		return $dayname;
     }
}
