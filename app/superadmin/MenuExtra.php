<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class MenuExtra extends Model
{
    public static function extraType($id){
        $extraTypeanme=MenuExtra::where('extra_type_id',$id)->get();
        return $extraTypeanme->toArray();

    }
}
