<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;


 class Kitchen extends Model
{
    
    public static function kitchenlist()
    {
		
        $kitchenlist = Kitchen::where('status','=','1')->get();
        return  $kitchenlist;
    }

}
