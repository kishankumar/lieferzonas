<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class FrontCountryMap extends Model
{
    public function country()
    {
        return $this->belongsTo('App\superadmin\Country','country_id');
    }
}
