<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestSubmenuAllergicMap extends Model
{
    public static function getSubmenuAllergenics($submenu_id)
    {
        $rows = self::where("status", 1)->where("sub_menu_id", $submenu_id)->get();
        $allergenics = array();
        foreach ($rows as $row) {
            if (!in_array($row->allergic_id, $allergenics)) {
                array_push($allergenics, $row->allergic_id);
            }
        }
        return $allergenics;
    }
}
