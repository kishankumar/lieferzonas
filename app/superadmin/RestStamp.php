<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestStamp extends Model
{
    protected $table="rest_stamps";
}
