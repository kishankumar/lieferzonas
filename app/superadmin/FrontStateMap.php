<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class FrontStateMap extends Model
{
    public function state()
    {
        return $this->belongsTo('App\superadmin\State','state_id');
    }
}
