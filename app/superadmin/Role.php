<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table="user_roles";
}
