<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestServiceMap extends Model
{
    protected $table="rest_service_maps";
	public function service(){

    	return $this->belongsTo('app\superadmin\FrontService');
    }

}
