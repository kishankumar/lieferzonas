<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Log;

class RestDayTimeMap extends Model
{
    protected $table="rest_day_time_maps";

	public static function daytime($rest_id)
    {
	   $x = date("d-m-Y");
       $day_id = date('N', strtotime($x)); 
	   $daytime = DB::table('rest_day_time_maps')->where('status','!=',2)->where('rest_detail_id','=',$rest_id)->where('day_id','=',$day_id)
	   ->select('*')->get();
	   for ($i = 0, $c = count($daytime); $i < $c; ++$i) {
            $daytime[$i] = (array) $daytime[$i];
        } 
	    return $daytime;
    }

    public static function getRestaurantOpenHours($restaurant_id) {
        return self::where("status", 1)->where("is_open", 1)->where("rest_detail_id", $restaurant_id)->get();
    }

    protected static function getRestaurantOpenHoursByDay($restaurant_id, $day) {
        return self::where("status", 1)->where("is_open", 1)->where('rest_detail_id', $restaurant_id)->where('day_id', $day)->get();
    }

    protected static function getRoundedTimes($hours, $after_now = false) {
        $rounded = array();
        foreach ($hours as $interval) {
            $open = $interval->open_time;
            $close = $interval->close_time;
            $open_hour = intval(date('H', strtotime($open)));
            $open_minute = intval(date('i', strtotime($open)));
            $close_hour = intval(date('H', strtotime($close)));
            $close_minute = intval(date('i', strtotime($close)));
            if ($open_minute > 0) {
                $open_hour += 1;
            }
            for ($i = $open_hour; $i <= $close_hour; $i++) {
                $daytime = str_pad($i, 2, '0', STR_PAD_LEFT) . ":00";
                if (!$after_now || time() < strtotime(date('Y-m-d') . " " . $daytime)) {
                    array_push($rounded, $daytime);
                }
            }
        }
        return $rounded;
    }

    public static function getPreorderTimes($restaurant_id) {
        $today = date('N', time());
        $tomorrow = date('N', strtotime(date("Y-m-d") . "+1 day"));
        $day_after_tomorrow = date('N', strtotime(date("Y-m-d") . "+2 day"));
        $today_hours = self::getRestaurantOpenHoursByDay($restaurant_id, $today);
        $tomorrow_hours = self::getRestaurantOpenHoursByDay($restaurant_id, $tomorrow);
        $day_after_tomorrow_hours = self::getRestaurantOpenHoursByDay($restaurant_id, $day_after_tomorrow);
        return [
            "today" => self::getRoundedTimes($today_hours, true),
            "tomorrow" => self::getRoundedTimes($tomorrow_hours),
            "day_after_tomorrow" => self::getRoundedTimes($day_after_tomorrow_hours)
        ];
    }

    protected static function getCurrentlyActiveHours($restaurant_id) {
        $today = date('N', time());
        $today_hours = self::getRestaurantOpenHoursByDay($restaurant_id, $today);
        foreach ($today_hours as $hours) {
            $open_time = strtotime($hours->open_time);
            $close_time = strtotime($hours->close_time);
            if (time() >= $open_time and time() <= $close_time) {
                return $hours;
            }
        }
        return null;
    }

    public static function closesInText($restaurant_id) {
        $active_hours = self::getCurrentlyActiveHours($restaurant_id);
        if (!$active_hours) return null;
        $close_time = strtotime($active_hours->close_time);
        $left_time = $close_time - time();
        $hours_left = floor($left_time / 3600);
        $minutes_left = floor($left_time / 60) - ($hours_left * 60);

        $text = "";
        if ($hours_left > 0) {
            $text .= $hours_left . " Stunden und ";
        }
        $text .= $minutes_left . " Minuten";
        return $text;
    }

}
