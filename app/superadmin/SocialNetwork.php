<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    public static function social()
    {
        $social_links = SocialNetwork::first();
        return $social_links;
    }
}
