<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestMealValidDay extends Model
{
    protected $table = 'rest_meal_valid_days';
}
