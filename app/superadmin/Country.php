<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Country extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public static function process_url($url)
    {
        $url=strtolower(preg_replace('/[^a-zA-Z0-9 s]/', '-', trim($url)));
        $url = str_replace("-"," ", $url);
        $url=preg_replace("/[[:blank:]]+/"," ",$url);
        $url = str_replace(" ","-", $url);
        $trem1=substr($url,0, 1);
        if($trem1=='-')
            $text=substr($url,1);
        else
            $text = $url;

        $len=strlen($text);
        $trem2=substr($text,-1,$len);
        if($trem2=='-')
            $text=substr($text,0,$len-1);

        $b=DB::table('countries')->where('slug',$text)->count();
        $c=DB::table('states')->where('slug',$text)->count();
        $d=DB::table('cities')->where('slug',$text)->count();

        if($b>0 || $c>0 || $d>0)
        {
            $text = $text.'-'.$b;
            return $text;
        }
        else
        {
            $text = $text;
            return $text;
        }

        $trem1=substr($text,0, 1);

        if($trem1=='-')
        {
            $text=substr($text,1);
            $text  = $text.'-'.$b;
            return $text;
        }
        else
        {
            $text= $text;
            return $text;
        }
    }
}
