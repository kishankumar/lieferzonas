<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestNewCustBonusLog extends Model
{
    protected $table="rest_new_cust_bonus_logs";
}
