<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use Auth;

class User_master extends Model
{
    protected $table="user_masters";
    public static function userdetail()
    {
        $auth_id = Auth::User()->id;
        $userdetail = User_master::where('user_id',$auth_id)->where('status',1)->get();
        return  $userdetail;
    }

}
