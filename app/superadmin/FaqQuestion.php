<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class FaqQuestion extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function category()
    {
        return $this->belongsTo('App\superadmin\FaqCategory','faq_category_id');
    }
}
