<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestSetting extends Model
{
    public function getResturant()
    {
        return $this->belongsTo('App\superadmin\RestDetail','rest_detail_id');
    }

    public static function isClosedToday($rest_id) {
        $rows = RestSetting::where("rest_detail_id", $rest_id)->where("status", 1)->get();
        if ($rows->count() == 1) {
            return !$rows[0]->is_open;
        }
        else {
            return false;
        }
    }
}
