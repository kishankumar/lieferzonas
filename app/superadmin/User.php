<?php
namespace App;
namespace App\superadmin;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table="users";
    
    public static function city_data($id)
    {
        $userdetail = City::where('id',$id)->where('status','!=',2)->first();
        return  $userdetail;
    }
    public static function state_data($id)
    {
        $userdetail = State::where('id',$id)->where('status','!=',2)->first();
        return  $userdetail;
    }
    public static function country_data($id)
    {
        $userdetail = Country::where('id',$id)->where('status','!=',2)->first();
        return  $userdetail;
    }
    public static function zip_data($id)
    {
        $userdetail = Zipcode::where('id',$id)->where('status','!=',2)->first();
        return  $userdetail;
    }
}
