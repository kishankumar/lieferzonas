<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class FrontService extends Model
{
     protected $table="front_services";

    public static function getActiveServices() {
        return self::where("status", 1)->get();
    }
}
