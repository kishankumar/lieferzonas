<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model
{
    protected $table="shop_categories";
}
