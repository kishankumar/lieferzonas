<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class RestPriceMap extends Model
{
    public static function getRestaurantOrderTypes($rest_id) {
        $rows = RestPriceMap::where("rest_detail_id", $rest_id)->get();
        $hasPickup = false;
        $hasDelivery = false;
        foreach ($rows as $row) {
            if ($row->price_type_id == 1) {
                $hasDelivery = true;
            }
            else if($row->price_type_id == 2) {
                $hasPickup = true;
            }
        }
        return [$hasDelivery, $hasPickup];
    }
}
