<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class ShopItem extends Model
{
    protected $table="shop_items";
    public function shop_category(){
    	return $this->belongsTo('App\superadmin\ShopCategory','shop_category_id');
    }
    public function metric_name(){
    	return $this->belongsTo('App\superadmin\Metric','metric_id');
    }
}
