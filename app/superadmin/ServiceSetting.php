<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class ServiceSetting extends Model
{
    protected $table="service_settings";
}
