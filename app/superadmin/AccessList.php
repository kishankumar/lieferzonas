<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class AccessList extends Model
{
    public function access()
    {
        return $this->belongsTo('App\User','created_by');
    }
}

