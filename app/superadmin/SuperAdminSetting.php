<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class SuperAdminSetting extends Model
{
    protected $table="super_admin_settings";
}
