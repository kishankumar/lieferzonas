<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\superadmin\RestSubmenuSpiceMap;
use Illuminate\Support\Facades\Session;

class RestSubMenu extends Model
{
    public function getResturant()
    {
        return $this->belongsTo('App\superadmin\RestDetail', 'rest_detail_id');
    }

    public function getMenu()
    {
        return $this->belongsTo('App\superadmin\RestMenu', 'menu_id');
    }

    public static function getSubMenuByMenu($id)
    {
        $submenuName = RestSubMenu::where('menu_id', $id)->get();
        return $submenuName->toArray();

    }

    public static function getSubMenuCurrentPrice($submenu_id) {
        if (Session::get("is_pickup")) {
            $subMenuPrice = SubMenuPriceMap::where('sub_menu_id', $submenu_id)->where('price_type_id', 2)->first();
        } else {
            $subMenuPrice = SubMenuPriceMap::where('sub_menu_id', $submenu_id)->where('price_type_id', 1)->first();
        }
        return $subMenuPrice;
    }

    public static function getSubMenuDeliveryPrice($id)
    {
        return SubMenuPriceMap::where('sub_menu_id', $id)->where('price_type_id', 1)->first();
    }

    public static function getSubMenuPickupPrice($id)
    {
        return SubMenuPriceMap::where('sub_menu_id', $id)->where('price_type_id', 2)->first();
    }

    public static function getSubMenuAlergicData($id)
    {
        $subMenuAlergic = DB::table('rest_submenu_allergic_maps')->where('sub_menu_id', $id)->select('allergic_id')->get();
        return $subMenuAlergic;
    }

    public static function getSubMenuSpicesData($id)
    {
        $subMenuSpice = RestSubmenuSpiceMap::where('sub_menu_id', $id)->select('spice_id')->get();
        return $subMenuSpice;
    }


}
