<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class FrontKitchenMap extends Model
{
    public function kitchen()
    {
        return $this->belongsTo('App\superadmin\Kitchen','id');
    }
}
