<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class RestStampMap extends Model
{

    public static function getRestaurantsWithStampCards() {
        throw new \Exception("Unimplemented");
    }

    public static function hasRestaurantStampCards($restaurant) {
        $row = RestStampMap::where("status", 1)->where("stamp_status", 1)->where("rest_detail_id", $restaurant)->first();
        if ($row) return true;
        else return false;
    }

    public static function getStampCardTarget($restaurant) {
        $row = RestStampMap::
        join("rest_stamps", "rest_stamp_maps.stamp_id", "=", "rest_stamps.id")->
        where("rest_stamps.status", 1)->
        where("rest_stamp_maps.status", 1)->
        where("rest_stamp_maps.rest_detail_id", $restaurant)->first();
        if ($row) return $row->stamp_count;
        else return 0;
    }

}
