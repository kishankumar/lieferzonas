<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class AdditionalHomePageSlug extends Model
{
    protected $table="additional_home_page_slugs";
}
