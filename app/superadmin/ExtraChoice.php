<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;

class ExtraChoice extends Model
{
    protected $table = "extra_choice";

    public static function getByRestaurant($restaurant_id)
    {
        return ExtraChoice::where("restaurant_id", $restaurant_id)->get();
    }

    public static function getBySubMenu($sub_menu_id) {
        // TODO: Optimize to use a join
        $extra_choices = array();
        foreach (SubMenuExtraMap::where("sub_menu_id", $sub_menu_id)->get() as $subMenuExtraMap) {
            array_push($extra_choices, ExtraChoice::find($subMenuExtraMap->extra_choice_id));
        }
        return $extra_choices;
    }

}
