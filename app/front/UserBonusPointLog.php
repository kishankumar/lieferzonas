<?php

namespace App\front;
use Illuminate\Database\Eloquent\Model;
use Auth;

class UserBonusPointLog extends Model
{
    protected $table = 'user_bonus_point_logs';
}
