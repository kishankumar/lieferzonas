<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;

class FrontUserSocialMap extends Model
{
    protected $table = 'front_user_social_maps';
}
