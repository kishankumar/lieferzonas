<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use Auth;

class RestSuggestion extends Model
{
   protected $table = 'rest_suggestions';
   public static function restsuggestion()
   {
	   $restsuggestion = RestSuggestion::where('status','!=','2')->where('is_read','=','0')->select('*')->get();
	   //print_r($restsuggestion);
	   $restsuggestioncount = count($restsuggestion);  
	   return $restsuggestioncount;
   }
}
