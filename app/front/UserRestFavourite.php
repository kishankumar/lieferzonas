<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserRestFavourite extends Model
{
    public static function user_rest_favourites()
    {
        return UserRestFavourite::
        join("rest_details", "user_rest_favourites.rest_id", "=", "rest_details.id")->
        where("rest_details.status", 1)->
        where("user_rest_favourites.status", 1)->
        where("user_id", Auth::user('front')->id)->
        get();
    }

    public static function rest_favourite_count($restaurant) {
        return UserRestFavourite::where("status", 1)->where("rest_id", $restaurant)->count();
    }

    public static function user_rest_favourite_count()
    {
        return UserRestFavourite::where("status", 1)->where("user_id", Auth::user('front')->id)->count();
    }

    public static function is_rest_user_favourite($rest_id) {
        if (!Auth::check('front')) return false;
        $count = UserRestFavourite::where("status", 1)->where("user_id", Auth::user('front')->id)->where("rest_id", $rest_id)->count();
        return $count > 0;
    }

    public static function toggle_favourite($rest_id) {
        $current = UserRestFavourite::where("status", 1)->where("user_id", Auth::user('front')->id)->where("rest_id", $rest_id)->get();
        if ($current->count() > 0) {
            UserRestFavourite::where("status", 1)->where("user_id", Auth::user('front')->id)->where("rest_id", $rest_id)->delete();
        }
        else {
            $restfavourite = new UserRestFavourite;
            $restfavourite->user_id=Auth::user('front')->id;
            $restfavourite->rest_id=$rest_id;
            $restfavourite->status=1;
            $restfavourite->created_by=Auth::user('front')->id;
            $restfavourite->created_at=date("Y-m-d H:i:s");
            $restfavourite->save();
        }
    }
}
