<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use DB;
class OrderPayment extends Model
{
      public static function totalsale_monthly($id)
      {
		$Restowner= DB ::table('rest_owners')
		->where('id','=',$id)
		->select('rest_detail_id')->get();
		for ($i = 0, $c = count($Restowner); $i < $c; ++$i) {
            $Restowner[$i] = (array) $Restowner[$i];
        }
		$rest_id = $Restowner['0']['rest_detail_id'];  
        $month_totalsale = DB :: table('order_payments')->leftjoin('user_orders','order_payments.order_id','=','user_orders.order_id')
		->where('order_payments.rest_detail_id',$rest_id)->where('order_payments.status','!=',2)
	    ->whereNotIn('user_orders.front_order_status_id', [1, 3, 4])
		->whereRaw("DATE_FORMAT(lie_order_payments.created_at, '%y-%m') = ?", array(date('y-m')))
		->select('order_payments.grand_total')
		->get();
		return $month_totalsale;
		
      }

	
	  public static function totalsale_weekly($id)
      {
		$Restowner= DB ::table('rest_owners')
		->where('id','=',$id)
		->select('rest_detail_id')->get();
		for ($i = 0, $c = count($Restowner); $i < $c; ++$i) {
            $Restowner[$i] = (array) $Restowner[$i];
        }
		$sevendays_ago_date = date('Y-m-d', strtotime('-8 days', strtotime(date('Y-m-d'))));
		$rest_id = $Restowner['0']['rest_detail_id']; 
        $week_totalsale = DB :: table('order_payments')->leftjoin('user_orders','order_payments.order_id','=','user_orders.order_id')
		->where('order_payments.rest_detail_id',$rest_id)->where('order_payments.status','!=',2)
		->whereNotIn('user_orders.front_order_status_id', [1, 3, 4])
		->whereBetween( DB::raw('date(lie_order_payments.created_at)'), [$sevendays_ago_date, date('Y-m-d')] )
		->select('order_payments.grand_total')
		->get();
		
		return $week_totalsale;
      }
	  public static function totalsale_today($id)
      {
		$Restowner= DB ::table('rest_owners')
		->where('id','=',$id)
		->select('rest_detail_id')->get();
		for ($i = 0, $c = count($Restowner); $i < $c; ++$i) {
            $Restowner[$i] = (array) $Restowner[$i];
        }
		$rest_id = $Restowner['0']['rest_detail_id']; 
        $today_totalsale = DB :: table('order_payments')->leftjoin('user_orders','order_payments.order_id','=','user_orders.order_id')
		->where('order_payments.rest_detail_id',$rest_id)->where('order_payments.status','!=',2)
		->whereNotIn('user_orders.front_order_status_id', [1, 3, 4])
		->whereRaw("DATE_FORMAT(lie_order_payments.created_at, '%y-%m-%d') = ?", array(date('y-m-d')))
		->select('order_payments.grand_total')
		->get();
		return $today_totalsale;
      }

	  
}
