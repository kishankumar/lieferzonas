<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserOrderReview extends Model
{

    public static function getActiveUserReviews() {
        return self::where("status", 1)->where("user_id", Auth::user("front")->id)->get();
    }

    public static function getRestaurantReviews($restaurant_id) {
        $rows = UserOrderReview::
        where('rest_id', $restaurant_id)->
        where("status", 1)->
        orderBy("created_at")->
        get();
        return $rows;
    }

    public static function rest_quality_rating($restaurant_id) {
        $reviews = UserOrderReview::where('status', 1)->where('rest_id', $restaurant_id)->get();
        $quality_rating = 0;
        foreach ($reviews as $review) {
            $quality_rating += $review["quality_rating"];
        }
        if ($reviews->count() == 0) {
            return 0;
        }
        $total_rating = ($quality_rating) / ($reviews->count());
        return $total_rating;
    }

    public static function rest_service_rating($restaurant_id) {
        $reviews = UserOrderReview::where('status', 1)->where('rest_id', $restaurant_id)->get();
        $service_rating = 0;
        foreach ($reviews as $review) {
            $service_rating += $review["service_rating"];
        }
        if ($reviews->count() == 0) {
            return 0;
        }
        $total_rating = ($service_rating) / ($reviews->count());
        return $total_rating;
    }

    public static function rest_review_keys($restaurant_id) {
        $rows = UserOrderReview::where('rest_id', $restaurant_id)->where("status", 1)->get();
        $oneStar = 0;
        $twoStar = 0;
        $threeStar = 0;
        $fourStar = 0;
        $fiveStar = 0;
        foreach ($rows as $row) {
            $rating = round(($row->quality_rating + $row->service_rating) / 2.0);
            if ($rating == 1) {
                $oneStar++;
            } else if ($rating == 2) {
                $twoStar++;
            } else if ($rating == 3) {
                $threeStar++;
            } else if ($rating == 4) {
                $fourStar++;
            } else if ($rating == 5) {
                $fiveStar++;
            }
        }
        $total = $oneStar + $twoStar + $threeStar + $fourStar + $fiveStar;
        return [$oneStar, $twoStar, $threeStar, $fourStar, $fiveStar, $total];
    }

    public static function rest_review_count_range($restaurants) {
        $counts = UserOrderReview::select(DB::raw("count(*) as total"))->groupBy('rest_id')->whereIn('rest_id', $restaurants)->where("status", 1)->get();
        $max_count = 0;
        $min_count = PHP_INT_MAX;
        foreach($counts as $row) {
            if ($row->total > $max_count) $max_count = $row->total;
            if ($row->total < $min_count) $min_count = $row->total;
        }
        if ($counts->count() < count($restaurants)) {
            $min_count = 0;
        }
        return [$min_count, $max_count];
    }

    public static function rest_review_count($rest_id) {
        $count = UserOrderReview::where('rest_id', $rest_id)->count();
        return $count;
    }

    public static function rest_review_rating($rest_id) {
        $reviews = UserOrderReview::where('status', 1)->where('rest_id', $rest_id)->get();
        $quality_rating = 0;
        $service_rating = 0;
        foreach ($reviews as $review) {
            $quality_rating += $review["quality_rating"];
            $service_rating += $review["service_rating"];
        }
        if ($reviews->count() == 0) {
            return 0;
        }
        $total_rating = ($service_rating + $quality_rating) / (2.0 * $reviews->count());
        return $total_rating;
    }


    public static function totalrating($rest_id)
     {
        $totalrating = DB ::table('user_order_reviews')->where('rest_id',$rest_id)->select('*')->get();
        $count = count($totalrating);
        $totalrating = 10*($count);
        return  $totalrating; 
    }
    public static function rating($rest_id)
    {
        $rating = DB ::table('user_order_reviews')->where('rest_id',$rest_id)->select('*')->get();
        return  $rating; 
    }
    public static function orderrating($user_id,$order_id)
    {
        $orderrating = DB ::table('user_order_reviews')->where('user_id',$user_id)->where('order_id',$order_id)->select('*')->get();
        for ($i = 0, $c = count($orderrating); $i < $c; ++$i) {
            $orderrating[$i] = (array) $orderrating[$i];
        }
        return  $orderrating; 
    }
    public static function getLatestReviews($rest_id)
    {
        $reviews = DB ::table('user_order_reviews')->where('rest_id',$rest_id)->where('status',1)->orderBy('created_at', 'desc')->paginate(5);
        return  $reviews; 
    }
    public static function userdetail($user_id)
    {
        $fuserdetail = FrontUserDetail::where('front_user_id',$user_id)->where('status',1)->first();
        return  $fuserdetail;
    }
    public static function getRestaurantInfo($restId)
    {
        $restdetail = DB ::table('rest_details')->where('id',$restId)->where('status',1)->first();
        return  $restdetail;
    }
    public static function getDayTime($rest_id)
    {
        $dayname = date('l');
        $days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
        $day_number = array_search($dayname, $days);
        $DayTiming = DB ::table('rest_day_time_maps')->where('rest_detail_id','=',$rest_id)->where('status','1')->where('day_id','=',$day_number)->first();
        return $DayTiming;
    }
	    
	 public static function restopentimediff($rest_id)
	 {
		 $x = date("d-m-Y");
         $day_id = date('N', strtotime($x));
         $restuarant_time = DB:: table('rest_day_time_maps')
		 ->where('rest_detail_id','=',$rest_id)
		 ->where('day_id','=',$day_id)
		 ->select('open_time')->get();
		for ($i = 0, $c = count($restuarant_time); $i < $c; ++$i) {
            $restuarant_time[$i] = (array) $restuarant_time[$i];
        }
		//print_r($restuarant_time); die;
		 
		if(count($restuarant_time)!=0)
		{
			$open_time = $restuarant_time['0']['open_time'];
			$open_time = date('H:i', strtotime($open_time));
			$current_time =date('H:i', time());
			if($open_time>$current_time)
			{
				$timediff = (strtotime($open_time)-strtotime($current_time))/60; 
			}
			else
			{
				$timediff =0;
			}
			return $timediff;
		}
			
	 }

}
