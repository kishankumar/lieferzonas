<?php

namespace App\front;

use App\superadmin\City;
use App\superadmin\EmailTemplate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class FrontUser extends Model
{
   protected $table = 'front_users';

   protected static function checkDateOfBirth($dob) {
       $dob_time = strtotime($dob);
       $turn_18 = strtotime("+18 years", $dob_time);
       if (time() < $turn_18) {
           return false;
       }
       else {
           return true;
       }
   }

   public static function registerByInput($email, $first_name, $last_name, $password, $phone, $dob) {

       if (FrontUser::where("status", 1)->where("email", $email)->first()) {
           throw new \Exception("Unter dieser E-Mail existiert bereits ein Benutzer-Konto!");
       }

       if (!self::checkDateOfBirth($dob)) {
           throw new \Exception("Du musst 18 Jahre alt oder älter sein um ein Benutzerkonto anzulegen!");
       }

       try {
           DB::beginTransaction();

           $new_user = new self;
           $new_user->email = $email;
           $new_user->password = bcrypt($password);
           $new_user->created_by = 1;
           $new_user->updated_by = 1;
           $new_user->created_at = Carbon::now();
           $new_user->updated_at = Carbon::now();
           $new_user->status = 1;
           $new_user->save();

           $user_detail = new FrontUserDetail;
           $user_detail->front_user_id = $new_user->id;
           $user_detail->nickname = $first_name . " " . $last_name;
           $user_detail->fname = $first_name;
           $user_detail->lname = $last_name;
           $user_detail->mobile = $phone;
           $user_detail->email = $email;
           $user_detail->dob = $dob;
           $user_detail->registered_via = '2';
           $user_detail->created_by = 2;
           $user_detail->updated_by = 2;
           $user_detail->created_at = Carbon::now();
           $user_detail->updated_at = Carbon::now();
           $user_detail->status = 1;
           $user_detail->save();

           DB::commit();
       }
       catch(Exception $e) {
           DB::rollback();
           throw new \Exception("Es gab einen Fehler beim Erstellen des Benutzer-Kontos. Bitte versuche es später erneut!");
       }
   }

    /**
     * @param $email
     * @return bool Created?
     */
    public static function registerGuest($email, $request) {
        $existing_account = self::where("email", $email)->where("status", 1)->first();
        if ($existing_account) {
            $user_address = new FrontUserAddress;
            $user_address->front_user_id = $existing_account->id;
            $user_address->booking_person_name = $request->input("first-name") . " " . $request->input("last-name");
            $user_address->mobile = $request->phone;
            $user_address->address = lie_format_address($request->street, $request->house, $request->staircase, $request->floor, $request->apartment);
            $user_address->street = $request->street;
            $user_address->house = $request->house;
            $user_address->staircase = $request->staircase;
            $user_address->floor = $request->floor;
            $user_address->apartment = $request->apartment;
            $coordinates = City::getCoordinates($user_address->address, $request->zipcode, $request->city);
            $user_address->latitude = $coordinates["latitude"];
            $user_address->longitude = $coordinates["longitude"];
            $user_address->is_type = 'p';
            $user_address->created_by = $existing_account->id;
            $user_address->updated_by = $existing_account->id;
            $user_address->created_at = Carbon::now();
            $user_address->updated_at = Carbon::now();
            $user_address->status = 1;
            $city = City::getByZipcodeAndName($request->zipcode, $request->city);
            $user_address->city_id = $city->city_id;
            $user_address->state_id = $city->state_id;
            $user_address->country_id = $city->country_id;
            $user_address->zipcode = $request->zipcode;
            $user_address->company = $request->company;
            $user_address->save();
            return [
                "user_id" => $existing_account->id,
                "address_id" => $user_address->id
            ];
        }
        else {
            $new_user = new self;
            $new_user->email = $email;
            $random_password = str_random(8);
            $new_user->password = bcrypt($random_password);
            $new_user->created_by = 1;
            $new_user->updated_by = 1;
            $new_user->created_at = Carbon::now();
            $new_user->updated_at = Carbon::now();
            $new_user->status = 1;
            $new_user->save();

            $user_detail = new FrontUserDetail;
            $user_detail->front_user_id = $new_user->id;
            $user_detail->nickname = $request->input("first-name") . " " . $request->input("last-name");
            $user_detail->fname = $request->input("first-name");
            $user_detail->lname = $request->input("last-name");
            $user_detail->mobile = $request->input("phone");
            $user_detail->email = $email;
            $user_detail->dob = $request->input("dob");
            $user_detail->registered_via = '2';
            $user_detail->created_by = 2;
            $user_detail->updated_by = 2;
            $user_detail->created_at = Carbon::now();
            $user_detail->updated_at = Carbon::now();
            $user_detail->status = 1;
            $user_detail->save();

            $user_address = new FrontUserAddress;
            $user_address->front_user_id = $new_user->id;
            $user_address->booking_person_name = $user_detail->nickname;
            $user_address->mobile = $user_detail->mobile;
            $user_address->address = lie_format_address($request->street, $request->house, $request->staircase, $request->floor, $request->apartment);
            $user_address->street = $request->street;
            $user_address->house = $request->house;
            $user_address->staircase = $request->staircase;
            $user_address->floor = $request->floor;
            $user_address->apartment = $request->apartment;
            $coordinates = City::getCoordinates($user_address->address, $request->zipcode, $request->city);
            $user_address->latitude = $coordinates["latitude"];
            $user_address->longitude = $coordinates["longitude"];
            $user_address->is_type = 'p';
            $user_address->created_by = 2;
            $user_address->updated_by = 2;
            $user_address->created_at = Carbon::now();
            $user_address->updated_at = Carbon::now();
            $user_address->status = 1;
            $city = City::getByZipcodeAndName($request->zipcode, $request->city);
            $user_address->city_id = $city->city_id;
            $user_address->state_id = $city->state_id;
            $user_address->country_id = $city->country_id;
            $user_address->zipcode = $request->zipcode;
            $user_address->company = $request->company;
            $user_address->save();

            $text = EmailTemplate::getNewAccountPasswordText($email, $random_password);
            Mail::raw($text, function($message) use ($email) {
                $message->to($email, $name = null);
                $message->subject("Passwort für Ihren neuen Account");
            });

            return [
                "user_id" => $new_user->id,
                "address_id" => $user_address->id
            ];
        }

    }
}
