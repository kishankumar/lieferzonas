<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SuggestionKitchenMap extends Model
{
   protected $table = 'suggestion_kitchen_maps';
}
