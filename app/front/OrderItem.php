<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;

use Auth;
class OrderItem extends Model
{
     public static function userorderitem($order_id)
    {
		$auth_id = Auth::user('front')->id;
        $userorderitem = OrderItem ::where('front_user_id',$auth_id)->where('status',1)->where('order_id',$order_id)->get();
        return  $userorderitem;
    }

}
