<?php

namespace App\front;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserRecentRestVisit extends Model
{

    public static function getActiveUser4RecentRestaurantVisits() {
        return self::where("user_id", Auth::user("front")->id)->where("status", 1)->orderBy("updated_at", "desc")->take(4)->get();
    }

    public static function registerVisit($restaurant_id) {
        if (!Auth::check("front")) return;
        $now = Carbon::now();
        $existing = self::where("status", 1)->where("user_id", Auth::user("front")->id)->where("rest_detail_id", $restaurant_id)->first();
        if ($existing) {
            $existing->updated_by = Auth::user("front")->id;
            $existing->updated_at = $now;
            $existing->save();
        }
        else {
            $new = new self;
            $new->user_id = Auth::user("front")->id;
            $new->rest_detail_id = $restaurant_id;
            $new->created_by = Auth::user("front")->id;
            $new->updated_by = Auth::user("front")->id;
            $new->created_at = $now;
            $new->updated_at = $now;
            $new->status = 1;
            $new->save();
        }
    }

}
