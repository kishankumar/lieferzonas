<?php

namespace App\front;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class UserOrder extends Model
{
    public static function rest_order_count($restaurant) {
        return UserOrder::where("status", 1)->where("rest_detail_id", $restaurant)->count();
    }

    public static function userorder()
    {
        $auth_id = Auth::user('front')->id;
        $userorder = DB::table('user_orders')
            ->leftjoin('rest_details', 'user_orders.rest_detail_id', '=', 'rest_details.id')
            ->leftjoin('rest_mobile_maps', 'rest_details.id', '=', 'rest_mobile_maps.rest_detail_id')
            ->leftjoin('front_user_addresses', 'user_orders.front_user_address_id', '=', 'front_user_addresses.id')
            ->leftjoin('countries', 'front_user_addresses.country_id', '=', 'countries.id')
            ->leftjoin('states', 'front_user_addresses.state_id', '=', 'states.id')
            ->leftjoin('cities', 'front_user_addresses.city_id', '=', 'cities.id')
            ->where('user_orders.front_user_id', $auth_id)
            ->where('user_orders.status', 1)
            ->where('rest_mobile_maps.is_primary', 1)
            ->select('rest_mobile_maps.mobile', 'rest_details.logo', 'rest_details.f_name', 'front_user_addresses.booking_person_name', 'front_user_addresses.mobile', 'front_user_addresses.zipcode', 'front_user_addresses.address', 'front_user_addresses.landmark', 'user_orders.*', 'countries.name as country_name', 'states.name as state_name', 'cities.name as city_name')
            ->orderby('user_orders.created_at', 'desc')
            ->limit(3)->offset(0)
            ->get();
        for ($i = 0, $c = count($userorder); $i < $c; ++$i) {

            $userorder[$i] = (array)$userorder[$i];
        }
        return $userorder;

    }

    public function rest_detail()
    {
        return $this->hasOne('App\superadmin\RestDetail', 'id', 'rest_detail_id');

    }

    public static function FrontUserAddress($user_id)
    {
        $fuseraddress = DB::table('front_user_addresses')->where('front_user_id', '=', $user_id)->where('status', '1')->first();
        if (count($fuseraddress)) {
            return $fuseraddress->address;
        } else {
            return 'No Address';
        }
    }

    public static function FrontUserFullAddress($user_id)
    {
        $fuseraddress = DB::table('user_orders')
            ->leftjoin('front_user_addresses', 'user_orders.front_user_address_id', '=', 'front_user_addresses.id')
            ->leftjoin('countries', 'front_user_addresses.country_id', '=', 'countries.id')
            ->leftjoin('states', 'front_user_addresses.state_id', '=', 'states.id')
            ->leftjoin('cities', 'front_user_addresses.city_id', '=', 'cities.id')
            ->where('user_orders.front_user_id', $user_id)
            ->where('user_orders.status', 1)
            ->select('user_orders.*', 'front_user_addresses.booking_person_name', 'front_user_addresses.mobile', 'front_user_addresses.zipcode',
                'front_user_addresses.address', 'front_user_addresses.landmark', 'countries.name as country_name',
                'states.name as state_name', 'cities.name as city_name')
            ->first();
        //$fuseraddress = DB ::table('front_user_addresses')->where('front_user_id','=',$user_id)->where('status','1')->first();
        if (count($fuseraddress)) {
            return $fuseraddress;
        } else {
            return 'No Address';
        }
    }

    public static function get_order_confirmed_time($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 2)->orderBy('id', 'desc')->first();
        if (count($orderStatus)) {
            $orderConfirmedTime = date('d M Y h:i:s A', strtotime($orderStatus->created_at));
            return $orderConfirmedTime;
        } else {
            $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 3)->orderBy('id', 'desc')->first();
            if (count($orderStatus)) {
                return 'Rejected';
            } else {
                return 'Not Accepted Yet';
            }
        }
    }

    public static function getOrderConfirmationTime($order_id) {
        $orderStatus = FrontOrderStatusMap::where('order_id', $order_id)->where('status_id', '=', 2)->orderBy('id', 'desc')->first();
        if ($orderStatus) {
            $orderConfirmedTime = strtotime($orderStatus->created_at);
            return $orderConfirmedTime;
        } else {
            return null;
        }
    }

    public static function get_confirmed_time($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 2)->orderBy('id', 'desc')->first();
        if (count($orderStatus)) {
            $orderConfirmedTime = date('d M Y h:i:s A', strtotime($orderStatus->created_at));
            return $orderConfirmedTime;
        } else {
            return 'Not Confirmed';
        }
    }

    public static function get_order_status($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 2)->first();
        $message = '';
        if (count($orderStatus)) {
            $orderConfirmedTime = date('d M Y h:i:s A', strtotime($orderStatus->created_at));
            $message = 'This order has been confirmed on ' . $orderConfirmedTime;
            return $message;
        } else {
            $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 3)->first();
            if (count($orderStatus)) {
                $orderRejectedTime = date('d M Y h:i:s A', strtotime($orderStatus->created_at));
                $message = 'This order has been rejected on ' . $orderRejectedTime;
                return $message;
            } else {
                return $message;
            }
        }
    }

    public static function get_order_confirmedOrNot($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 2)->first();
        if (count($orderStatus)) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function get_order_cookedOrNot($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 5)->first();
        if (count($orderStatus)) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function get_order_cookedTime($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 5)->first();
        if (count($orderStatus)) {
            $orderCookingTime = date('d M Y h:i:s A', strtotime($orderStatus->created_at));
            $message = 'Cooking has been started on ' . $orderCookingTime;
            return $message;
        } else {
            return 0;
        }
    }

    public static function get_order_deliveredOrNot($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 6)->first();
        if (count($orderStatus)) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function get_order_deliveredTime($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 6)->first();
        if (count($orderStatus)) {
            $orderDeliveryTime = date('d M Y h:i:s A', strtotime($orderStatus->created_at));
            $message = 'Delivery has been started on ' . $orderDeliveryTime;
            return $message;
        } else {
            return 0;
        }
    }

    public static function get_order_DeliveryStartOrNot($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 6)->first();
        if (count($orderStatus)) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function getFinishOrderDeliveredTime($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 7)->first();
        if (count($orderStatus)) {
            return $orderStatus->created_at;
        } else {
            return 0;
        }
    }

    public static function getFinishOrderDeliveredTimeMsg($order_id)
    {
        $orderStatus = DB::table('front_order_status_maps')->where('order_id', '=', $order_id)->where('status_id', '=', 7)->first();
        if (count($orderStatus)) {
            $orderDeliveryTime = date('d M Y h:i:s A', strtotime($orderStatus->created_at));
            $message = 'Order has been delivered at ' . $orderDeliveryTime;
            return $message;
        } else {
            return 0;
        }
    }

    public function cityname()
    {
        return $this->belongsTo('App\superadmin\City', 'city');
    }

    public function statename()
    {
        return $this->belongsTo('App\superadmin\State', 'state');
    }

    public function countryname()
    {
        return $this->belongsTo('App\superadmin\Country', 'country');
    }

    public function zipcode()
    {
        return $this->belongsTo('App\superadmin\Zipcode', 'zipcode');
    }

    public static function user_order_count()
    {
        $order_count = UserOrder::where("status", 1)->where("front_user_id", Auth::user('front')->id)->count();
        return $order_count;
    }

    public static function user_order_count_unrated()
    {
        $user_reviews = DB::table('user_order_reviews')->select('order_id')->where("status", 1)->where("front_user_id", \Illuminate\Support\Facades\Auth::user('front')->id);
        return UserOrder::where("status", 1)->where("front_user_id", \Illuminate\Support\Facades\Auth::user('front')->id)->whereNotIn('order_id', $user_reviews)->count();
    }

    public static function getLastCompleteOrdersByRestaurant($restaurant_id, $count) {
        $rows = self::where("rest_detail_id", $restaurant_id)->where("front_order_status_id", 7)->where("status", 1)->take($count)->get();
        return $rows;
    }

    public static function getUnreviewedCompleteOrders() {
        $rows = self::where("front_user_id", Auth::user("front")->id)->where("front_order_status_id", 7)->where("status", 1)->whereNotIn("order_id", UserOrderReview::lists("order_id"))->get();
        return $rows;
    }

    public static function getCompletionTimeMinutes($complete_order_id) {
        $accepted_row = FrontOrderStatusMap::where("order_id", $complete_order_id)->where("status_id", 2)->first();
        $completed_row = FrontOrderStatusMap::where("order_id", $complete_order_id)->where("status_id", 7)->first();
        if (!$accepted_row or !$completed_row) return 0;
        $start_time = Carbon::parse($accepted_row->created_at);
        $end_time = Carbon::parse($completed_row->created_at);
        return $end_time->diffInMinutes($start_time);
    }

    public static function getAverageCompletionTimeByRestaurant($restaurant_id, $order_count) {
        $orders = self::where("rest_detail_id", $restaurant_id)->where("front_order_status_id", 7)->where("status", 1)->take($order_count)->get();
        $totalMinutes = 0;
        $entries = 0;
        foreach ($orders as $order) {
            $accepted_row = FrontOrderStatusMap::where("order_id", $order->order_id)->where("status_id", 2)->first();
            $completed_row = FrontOrderStatusMap::where("order_id", $order->order_id)->where("status_id", 7)->first();
            $start_time = Carbon::parse($accepted_row->created_at);
            $end_time = Carbon::parse($completed_row->created_at);
            $totalMinutes += $end_time->diffInMinutes($start_time);
            $entries++;
        }
        if ($entries == 0) return 0;
        else {
            return $totalMinutes / $entries;
        }
    }

    public static function getLastOrder() {
        return self::where("status", 1)
            ->where("front_user_id", Auth::user('front')->id)
            ->orderBy("created_at", "desc")
            ->first();
    }
}

