<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserNotificationMap extends Model
{

    public static function hasActiveUserNotification($notification_id) {
        return !!self::where("status", 1)->
        where("front_user_id", Auth::user("front")->id)->
        where("notification_id", $notification_id)->
            first();
    }

}
