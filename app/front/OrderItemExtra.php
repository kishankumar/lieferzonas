<?php
/**
 * Created by PhpStorm.
 * User: Aras
 * Date: 20.11.2016
 * Time: 20:13
 */

namespace App\front;


use Illuminate\Database\Eloquent\Model;

class OrderItemExtra extends Model
{
    protected $table = "order_item_extras";
}