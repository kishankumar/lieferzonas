<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;

use DB;
use Illuminate\Support\Facades\Auth;

class FrontUserAddress extends Model
{
    protected $table = 'front_user_addresses';

    public function cityname()
    {
        return $this->belongsTo('App\superadmin\City', 'city');
    }

    public function statename()
    {
        return $this->belongsTo('App\superadmin\State', 'state');
    }

    public function countryname()
    {
        return $this->belongsTo('App\superadmin\Country', 'country');
    }

    public function zipcode()
    {
        return $this->belongsTo('App\superadmin\Zipcode', 'zipcode');
    }

    public static function useraddress()
    {
        $useraddress = DB::table('front_user_addresses')
            ->leftjoin('countries', 'front_user_addresses.country_id', '=', 'countries.id')
            ->leftjoin('cities', 'front_user_addresses.city_id', '=', 'cities.id')
            ->leftjoin('states', 'front_user_addresses.state_id', '=', 'states.id')
            ->where('front_user_addresses.status', '=', 1)
            ->select('front_user_addresses.*', 'countries.name as country_name', 'states.name as state_name', 'cities.name as city_name')
            ->orderby('front_user_addresses.created_at', 'desc')
            ->limit(4)->offset(0)
            ->get();
        for ($i = 0, $c = count($useraddress); $i < $c; ++$i) {
            $useraddress[$i] = (array)$useraddress[$i];
        }
        return $useraddress;
    }

    public static function user_address_count()
    {
        $count = DB::table('front_user_addresses')->where('front_user_id', Auth::user('front')->id)->where("status", 1)->count();
        return $count;
    }

    public static function user_addresses_json()
    {
        $rows = self::leftjoin('countries', 'front_user_addresses.country_id', '=', 'countries.id')
            ->leftjoin('cities', 'front_user_addresses.city_id', '=', 'cities.id')
            ->leftjoin('states', 'front_user_addresses.state_id', '=', 'states.id')
            ->leftjoin('zipcodes', 'front_user_addresses.zipcode', '=', 'zipcodes.name')
            ->where('front_user_addresses.status', '=', 1)
            ->where('front_user_addresses.front_user_id', '=', Auth::user('front')->id)
            ->select('front_user_addresses.*', 'zipcodes.description as zipcode_name', 'countries.name as country_name', 'states.name as state_name', 'cities.name as city_name')
            ->orderby('front_user_addresses.created_at', 'desc')
            ->get();
        return $rows;
    }

    public static function getUserAddresses($take = -1) {
        if ($take == -1) {
            $rows = self::where('status', '=', 1)
                ->where('front_user_id', '=', Auth::user('front')->id)
                ->orderby('created_at', 'desc')
                ->get();
        }
        else {
            $rows = self::where('status', '=', 1)
                ->where('front_user_id', '=', Auth::user('front')->id)
                ->orderby('created_at', 'desc')
                ->take($take)
                ->get();
        }
        return $rows;
    }

    public static function getActiveUserPrimaryAddress() {
        if (!Auth::check("front")) return null;
        return self::where("status", 1)->where("front_user_id", Auth::user("front")->id)->where("is_type", "p")->first();
    }

}
