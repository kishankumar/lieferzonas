<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
class UserBonusPoint extends Model
{
    public static function bonuspoint()
    {
		$expcdate = date("Y-m-d H:i:s", strtotime(" -3 months"));
		$auth_id = Auth::user('front')->id;
		$totalbonuspoint = UserBonusPoint::where('user_id','=',$auth_id)->where('status','=',1)->where('created_at','>=',$expcdate)
		->select('used_points','awarded_points')->get();
		$apoints = 0;
		$upoints = 0;
		foreach($totalbonuspoint as $totalbonuspoints)
		{
		  $apoints = $apoints + (int)$totalbonuspoints->awarded_points;
		  $upoints = $upoints + (int)$totalbonuspoints->used_points;
		} 
		$totalbonuspoint = ($apoints-$upoints);
		return $totalbonuspoint;
	}
	
	public static function awardedpoint($user_id)
    {
		$expcdate = date("Y-m-d H:i:s", strtotime(" -3 months"));
		
		$awardedpoint = UserBonusPoint::where('user_id','=',$user_id)
		->select('awarded_points')->get();
		
		$apoints = 0;
		foreach($awardedpoint as $awardedpoints)
		{
		  
		  $apoints = $apoints + (int)$awardedpoints->awarded_points;
		} 
		$awardedpoint = $apoints;
		return $awardedpoint;
	}
	
	public static function usedpoint($user_id)
    {
		$expcdate = date("Y-m-d H:i:s", strtotime(" -3 months"));
		
		$usedpoint = UserBonusPoint::where('user_id','=',$user_id)
		->select('used_points')->get();
		
		$upoints = 0;
		foreach($usedpoint as $usedpoints)
		{
		  
		  $upoints = $upoints + (int)$usedpoints->used_points;
		} 
		$usedpoint = $upoints;
		return $usedpoint;
	}
	
	public static function expirepoint($user_id)
    {
		$expcdate = date("Y-m-d H:i:s", strtotime(" -3 months"));
		
		$expirepoint = UserBonusPoint::where('user_id','=',$user_id)->where('status','=',1)->where('created_at','<',$expcdate)
		->select('used_points','awarded_points')->get();
		$apoints = 0;
		$upoints = 0;
		foreach($expirepoint as $expirepoints)
		{
		  $apoints = $apoints + (int)$expirepoints->awarded_points;
		  $upoints = $upoints + (int)$expirepoints->used_points;
		} 
		$expirepoint = ($apoints-$upoints);
		return $expirepoint;
	}
	
	public static function availablepoint($user_id)
    {
		$expcdate = date("Y-m-d H:i:s", strtotime(" -3 months"));
		
		$availablepoint = UserBonusPoint::where('user_id','=',$user_id)->where('status','=',1)->where('created_at','>=',$expcdate)
		->select('used_points','awarded_points')->get();
		$apoints = 0;
		$upoints = 0;
		foreach($availablepoint as $availablepoints)
		{
		  $apoints = $apoints + (int)$availablepoints->awarded_points;
		  $upoints = $upoints + (int)$availablepoints->used_points;
		} 
		$availablepoint = ($apoints-$upoints);
		return $availablepoint;
	}
}
