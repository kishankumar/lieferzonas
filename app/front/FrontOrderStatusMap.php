<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use DB;
class FrontOrderStatusMap extends Model
{
     public static function status_date($order_id,$status_id)
     {
        $status_date = FrontOrderStatusMap ::where('order_id','=',$order_id)->where('status_id',$status_id)->select('created_at')->get();
        return  $status_date; 
     }

     public static function getOrderStatus($order_id) {
         return self::where("status", 1)->where("order_id", $order_id)->orderBy("created_at", "desc")->first();
     }
	 
}
