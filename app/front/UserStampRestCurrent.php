<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserStampRestCurrent extends Model
{

    public static function getActiveUserStamps($restaurant) {
        // TODO: Make sure the restaurant card hasnt changed since acquiring these stamps
        if (!Auth::check("front")) return 0;
        $row = UserStampRestCurrent::where("status", 1)->where("rest_detail_id", $restaurant)->where("user_id", Auth::user("front")->id)->first();
        if (!$row) return 0;
        else return $row->stamp_count;
    }

    public static function getActiveUserStampedCards() {
        if (!Auth::check("front")) return 0;
        $rows = UserStampRestCurrent::where("status", 1)->where("user_id", Auth::user("front")->id)->where("stamp_count", ">", 0)->get();
        return $rows;
    }

}
