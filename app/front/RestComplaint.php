<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;

class RestComplaint extends Model
{
    protected $table = 'rest_complaints';
    
    public static function countUnreadComplaints()
    {
        $count = RestComplaint:: where('check_status','U')->where('status',1)->count();
        return  $count;
    }
    public function rest_name()
    {
        return $this->belongsTo('App\superadmin\RestDetail','rest_id');
    }
    public function order_detail()
    {
        return $this->belongsTo('App\front\UserOrder','order_id');
    }
}
