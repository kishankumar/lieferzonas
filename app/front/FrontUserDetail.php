<?php

namespace App\front;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Facades\File;

class FrontUserDetail extends Model
{
    public static function userdetail()
    {
        if (!Auth::check("front")) return null;
		$auth_id = Auth::user('front')->id;
        $fuserdetail = FrontUserDetail::where("status", 1)->where('front_user_id',$auth_id)->first();
        return $fuserdetail;
    }

    public static function getProfilePictureForDisplay($user_id) {
        $row = FrontUserDetail::where("status", 1)->where('front_user_id',$user_id)->first();
        if (!$row or !$row->profile_pic or !File::exists(public_path() . "/front/uploads/users/" . $row->profile_pic)) {

            return url('/public/front/uploads/users/default.png');
        }
        else {

            return url('/public/front/uploads/users').'/'. $row->profile_pic;
        }
    }

    public static function getActiveUserProfilePictureForDisplay() {
        if (!Auth::check("front")) return null;
        $row = FrontUserDetail::where("status", 1)->where('front_user_id', Auth::user("front")->id)->first();
        if (!$row or !$row->profile_pic or !File::exists(public_path() . "/front/uploads/users/" . $row->profile_pic)) {
            return url('/public/front/uploads/users/default.png');
        }
        else {

            return url('/public/front/uploads/users').'/'. $row->profile_pic;
        }
    }

    public static function getUserDetailsById($user_id) {
        $row = FrontUserDetail::where("status", 1)->where('front_user_id',$user_id)->first();
        return $row;
    }

}
