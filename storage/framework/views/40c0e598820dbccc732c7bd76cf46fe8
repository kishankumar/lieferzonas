

<?php $__env->startSection('title'); ?>
    Lieferzonas - Bewertungen
<?php $__env->stopSection(); ?>

<?php $__env->startSection('user_head'); ?>
    <style>
        .lie-header-cell {
            text-align: center;
            padding-bottom: 15px;
        }

        .lie-body-cell {
            text-align: center;
        }

        fieldset, label {
            margin: 0;
            padding: 0;
        }

        h1 {
            font-size: 1.5em;
            margin: 10px;
        }

        .rating {
            border: none;
            float: left;
        }

        .rating > input {
            display: none;
        }

        .rating > label:before {
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating > .half:before {
            content: "\f089";
            position: absolute;
        }

        .rating > label {
            color: #ddd;
            float: right;
        }

        .rating > input:checked ~ label,
        .rating:not(:checked) > label:hover,
        .rating:not(:checked) > label:hover ~ label {
            color: #FFD700;
        }

        .rating > input:checked + label:hover,
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label,
        .rating > input:checked ~ label:hover ~ label {
            color: #FFED85;
        }

        .lie-rating-text {
            font-size: 24px;
            font-weight: bolder;
        }

        .lie-rating-stars {
            font-size: 22px;
        }

        .lie-rating-numbers {
            font-size: 16px;
        }

    </style>
<?php $__env->stopSection(); ?>

<?php
$pending_reviews = \App\front\UserOrder::getUnreviewedCompleteOrders();
$reviews = \App\front\UserOrderReview::getActiveUserReviews();
?>

<?php $__env->startSection('user_contents'); ?>

    <?php if($pending_reviews->count() > 0): ?>
        <div class="lie-container">
            <div class="content-heading">Offene Bewertungen</div>
        </div>
    <?php endif; ?>

    <?php foreach($pending_reviews as $pending_review): ?>
        <?php
        $address = \App\front\FrontUserAddress::find($pending_review->front_user_address_id);
        $display_information = \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($pending_review->rest_detail_id);
        ?>
        <div class="lie-container" style="padding: 8px;">
            <table width="100%" style="font-size: 13px;">
                <thead>
                <tr>
                    <th class="lie-header-cell" width="20%">Zustellservice</th>
                    <th class="lie-header-cell" width="20%">Lieferadresse</th>
                    <th class="lie-header-cell" width="20%">Zustellzeit</th>
                    <th class="lie-header-cell" width="20%">Preis</th>
                    <th class="lie-header-cell" width="20%"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="lie-body-cell" style="vertical-align: top;"><?php echo e($pending_review->delivery_time); ?></td>
                    <td class="lie-body-cell"
                        style="vertical-align: middle; padding: 10px 0px;"><?php echo e($address->address); ?></td>
                    <td class="lie-body-cell"
                        style="vertical-align: middle; padding: 10px 0px;"><?php echo e(\App\front\UserOrder::getCompletionTimeMinutes($pending_review->order_id)); ?>

                        Minuten
                    </td>
                    <td class="lie-body-cell"
                        style="vertical-align: middle; padding: 10px 0px;"><?php echo e(currency_format($pending_review->grand_total)); ?></td>
                    <td class="lie-body-cell lie-rating-golden"
                        style="vertical-align: middle; padding: 10px 0px;"><?php echo lie_rating_bundle($display_information["rating"]); ?></td>
                </tr>
                </tbody>
            </table>
            <table width="100%">
                <tbody>
                <tr>
                    <td width="20%"><img src="<?php echo e($display_information["display_logo"]); ?>" width="100%"
                                         class="img-rounded"></td>
                    <td width="80%" style="border-top: 1px solid #27ae60; text-align: center">
                    <span style="padding: 8px; border: 1px solid yellow"
                          onclick="showReviewModal('<?php echo e($pending_review->order_id); ?>', '<?php echo e($pending_review->rest_detail_id); ?>')">
                        Bewerte jetzt deine Bestellung und erhalte 5 Bonuspunkte!
                    </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <?php echo e($pending_review->order_id); ?>


        </div>
    <?php endforeach; ?>

    <?php if($reviews->count() > 0): ?>
        <div class="lie-container">
            <div class="content-heading">Abgegebene Bewertungen</div>
        </div>
    <?php endif; ?>

    <?php foreach($reviews as $review): ?>
        <!-- <?php echo e(var_dump($review)); ?> -->
        <?php
        $review_order = \App\front\UserOrder::where("order_id", $review->order_id)->first();
        $address = \App\front\FrontUserAddress::find($review_order->front_user_address_id);
        $display_information = \App\superadmin\RestDetail::getRestaurantDisplayInformationArray($review_order->rest_detail_id);

        $quality = $review->quality_rating;
        $service = $review->service_rating;
        $average = ($quality + $service) / 2;
        $comment = $review->comment;

        ?>
        <div class="lie-container" style="padding: 8px;">
            <table width="100%" style="font-size: 13px;">
                <thead>
                <tr>
                    <th class="lie-header-cell" width="20%">Zustellservice</th>
                    <th class="lie-header-cell" width="20%">Lieferadresse</th>
                    <th class="lie-header-cell" width="20%">Zustellzeit</th>
                    <th class="lie-header-cell" width="20%">Preis</th>
                    <th class="lie-header-cell" width="20%"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="lie-body-cell" style="vertical-align: top;"><?php echo e($review_order->delivery_time); ?></td>

                    <td class="lie-body-cell"
                        style="vertical-align: middle; padding: 10px 0px;">
                        <?php if($address): ?>
                            <?php echo e($address->address); ?>

                        <?php else: ?>
                            <?php echo e(""); ?>

                        <?php endif; ?>
                    </td>
                    <td class="lie-body-cell"
                        style="vertical-align: middle; padding: 10px 0px;"><?php echo e(\App\front\UserOrder::getCompletionTimeMinutes($review_order->order_id)); ?>

                        Minuten
                    </td>
                    <td class="lie-body-cell"
                        style="vertical-align: middle; padding: 10px 0px;"><?php echo e(currency_format($review_order->grand_total)); ?></td>
                    <td class="lie-body-cell lie-rating-golden"
                        style="vertical-align: middle; padding: 10px 0px;"><?php echo lie_rating_bundle($display_information["rating"]); ?></td>
                </tr>
                </tbody>
            </table>
            <table width="100%">
                <tbody>
                <tr>
                    <td width="20%"><img src="<?php echo e($display_information["display_logo"]); ?>" width="100%"
                                         class="img-rounded"></td>
                    <td width="80%" style="border-top: 1px solid #27ae60; text-align: center">
                        <span class="lie-rating-text"><?php echo e(lie_rating_text($average)); ?></span><br>
                        <span class="lie-rating-golden lie-rating-stars"><?php echo lie_rating_stars($average); ?></span><br>
                        <span class="lie-rating-numbers"><?php echo e(lie_rating_format($average)); ?>/5</span><br>
                        <br>
                        <div style="width: 100%; padding: 15px;">
                            <table width="100%" style="font-size: 14px;">
                                <tbody>
                                <tr>
                                    <td width="60%">Qualität</td>
                                    <td width="20%" class="lie-rating-golden"><?php echo lie_rating_stars($quality); ?></td>
                                    <td width="20%"><?php echo e(lie_rating_format($quality) . " " . lie_rating_text($quality)); ?></td>
                                </tr>
                                <tr>
                                    <td width="60%">Service</td>
                                    <td width="20%" class="lie-rating-golden"><?php echo lie_rating_stars($service); ?></td>
                                    <td width="20%"><?php echo e(lie_rating_format($service) . " " . lie_rating_text($service)); ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <?php echo e($review_order->order_id); ?>

        </div>
    <?php endforeach; ?>

    <div class="modal fade" role="dialog" id="lie-review-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Bestellung bewerten</h4>
                </div>
                <div class="modal-body">
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td>Qualität:</td>
                            <td>
                                <fieldset class="rating">
                                    <input type="radio" id="lie-quality-star5" name="quality" value="5"/><label class="full"
                                                                                                   for="lie-quality-star5"
                                                                                                   title="Awesome - 5 stars"></label>
                                    <input type="radio" id="lie-quality-star4half" name="quality" value="4.5"/><label
                                            class="half" for="lie-quality-star4half" title="Pretty good - 4.5 stars"></label>
                                    <input type="radio" id="lie-quality-star4" name="quality" value="4"/><label class="full"
                                                                                                   for="lie-quality-star4"
                                                                                                   title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="lie-quality-star3half" name="quality" value="3.5"/><label
                                            class="half" for="lie-quality-star3half" title="Meh - 3.5 stars"></label>
                                    <input type="radio" id="lie-quality-star3" name="quality" value="3"/><label class="full"
                                                                                                   for="lie-quality-star3"
                                                                                                   title="Meh - 3 stars"></label>
                                    <input type="radio" id="lie-quality-star2half" name="quality" value="2.5"/><label
                                            class="half" for="lie-quality-star2half" title="Kinda bad - 2.5 stars"></label>
                                    <input type="radio" id="lie-quality-star2" name="quality" value="2"/><label class="full"
                                                                                                   for="lie-quality-star2"
                                                                                                   title="Kinda bad - 2 stars"></label>
                                    <input type="radio" id="lie-quality-star1half" name="quality" value="1.5"/><label
                                            class="half" for="lie-quality-star1half" title="Meh - 1.5 stars"></label>
                                    <input type="radio" id="lie-quality-star1" name="quality" value="1"/><label class="full"
                                                                                                   for="lie-quality-star1"
                                                                                                   title="Sucks big time - 1 star"></label>
                                    <input type="radio" id="lie-quality-starhalf" name="quality" value="0.5"/><label class="half"
                                                                                                         for="lie-quality-starhalf"
                                                                                                         title="Sucks big time - 0.5 stars"></label>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>Lieferung:</td>
                            <td>
                                <fieldset class="rating">
                                    <input type="radio" id="lie-service-star5" name="service" value="5"/><label class="full"
                                                                                                    for="lie-service-star5"
                                                                                                    title="Awesome - 5 stars"></label>
                                    <input type="radio" id="lie-service-star4half" name="service" value="4.5"/><label
                                            class="half" for="lie-service-star4half" title="Pretty good - 4.5 stars"></label>
                                    <input type="radio" id="lie-service-star4" name="service" value="4"/><label class="full"
                                                                                                    for="lie-service-star4"
                                                                                                    title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="lie-service-star3half" name="service" value="3.5"/><label
                                            class="half" for="lie-service-star3half" title="Meh - 3.5 stars"></label>
                                    <input type="radio" id="lie-service-star3" name="service" value="3"/><label class="full"
                                                                                                    for="lie-service-star3"
                                                                                                    title="Meh - 3 stars"></label>
                                    <input type="radio" id="lie-service-star2half" name="service" value="2.5"/><label
                                            class="half" for="lie-service-star2half" title="Kinda bad - 2.5 stars"></label>
                                    <input type="radio" id="lie-service-star2" name="service" value="2"/><label class="full"
                                                                                                    for="lie-service-star2"
                                                                                                    title="Kinda bad - 2 stars"></label>
                                    <input type="radio" id="lie-service-star1half" name="service" value="1.5"/><label
                                            class="half" for="lie-service-star1half" title="Meh - 1.5 stars"></label>
                                    <input type="radio" id="lie-service-star1" name="service" value="1"/><label class="full"
                                                                                                    for="lie-service-star1"
                                                                                                    title="Sucks big time - 1 star"></label>
                                    <input type="radio" id="lie-service-starhalf" name="service" value="0.5"/><label class="half"
                                                                                                         for="lie-service-starhalf"
                                                                                                         title="Sucks big time - 0.5 stars"></label>
                                </fieldset>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <textarea class="form-control" placeholder="Beschreibung" id="lie-review-comment"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="onClickSendReview()">Absenden</button>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('user_scripts'); ?>
    <script>
        $("#lie-sidebar-reviews").addClass("active");

        var orderId = null;
        var restaurantId = null;

        function onClickSendReview() {
            var quality = $("input[name='quality']:checked").val();
            var service = $("input[name='service']:checked").val();

            var text = $("#lie-review-comment").val();

            if (!quality || !service) {
                alert("Bitte eine Wertung für Qualität und Lieferung auswählen!");
            }

            $.ajax({
                url: '<?php echo e(url("front/orderrate/add")); ?>',
                data: {
                    '_token': '<?php echo e(csrf_token()); ?>',
                    'rest_id': restaurantId,
                    'order_id': orderId,
                    'qrating': quality,
                    'drating': service,
                    'comment': text
                },
                type: "POST",
                dataType: "JSON",
                success: function(response) {
                    if (response.success) window.location.reload(true);
                    $("#lie-review-modal").modal("hide");
                }
            });

        }

        function showReviewModal(pOrderId, pRestaurantId) {
            orderId = pOrderId;
            restaurantId = pRestaurantId;
            $("#lie-review-modal").modal("show");
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front_new.user_profile.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>