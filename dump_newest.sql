-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: lieferzonas
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lie_access_lists`
--

DROP TABLE IF EXISTS `lie_access_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_access_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_access_lists`
--

LOCK TABLES `lie_access_lists` WRITE;
/*!40000 ALTER TABLE `lie_access_lists` DISABLE KEYS */;
INSERT INTO `lie_access_lists` VALUES (1,'Add','Add',0,0,'0000-00-00 00:00:00','2016-05-19 23:17:49','1'),(2,'Edit','Edit',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(3,'View','View',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(4,'Delete','View',0,0,'0000-00-00 00:00:00','2016-05-19 23:18:12','1'),(5,'Update','Update Update Update',0,0,'2016-05-25 11:30:07','2016-05-25 11:30:20','2'),(6,'Restaurant Verwalten','Nur Restaurant Verwalten',0,0,'2016-06-01 00:29:03','2016-06-01 00:29:03','1'),(7,'Bestellungen','Beobachten / Ansehen',0,0,'2016-06-01 00:30:40','2016-06-01 00:31:00','1'),(8,'Bestellungen Bearbeiten','Bestellungen Bearbeiten / User ansicht /  Restaurant Ansicht / Benachrichtigungen Full Ansicht /',0,0,'2016-06-01 00:31:54','2016-06-01 00:38:29','1'),(9,'Delete the Option','Need to correct this',0,0,'2016-06-15 08:15:34','2016-06-15 08:15:34','1'),(10,'Test','Test test test test test test',0,0,'2016-07-04 12:13:29','2016-07-04 12:13:57','1'),(11,'manage','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events',0,0,'2016-08-01 16:17:03','2016-08-01 16:21:25','1'),(12,'store','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events',0,0,'2016-08-01 16:22:31','2016-09-05 05:11:03','1');
/*!40000 ALTER TABLE `lie_access_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_access_maps`
--

DROP TABLE IF EXISTS `lie_access_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_access_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_list_id` int(11) NOT NULL,
  `page_list_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_access_list_id` (`access_list_id`),
  KEY `lie_page_list_id` (`page_list_id`),
  KEY `lie_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_access_maps`
--

LOCK TABLES `lie_access_maps` WRITE;
/*!40000 ALTER TABLE `lie_access_maps` DISABLE KEYS */;
INSERT INTO `lie_access_maps` VALUES (1,7,2,8,2,2,'2016-07-27 05:41:08','2016-07-27 05:41:08','2'),(2,1,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(3,2,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(4,3,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(5,4,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(6,6,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(7,7,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(8,8,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(9,9,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(10,10,1,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(11,1,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(12,2,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(13,3,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(14,4,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(15,6,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(16,7,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(17,8,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(18,9,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(19,10,2,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(20,1,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(21,2,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(22,3,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(23,4,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(24,6,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(25,7,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(26,8,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(27,9,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(28,10,4,9,2,2,'2016-08-01 13:46:09','2016-08-01 13:46:09','1'),(29,1,1,10,2,2,'2016-08-01 17:34:38','2016-08-01 17:34:38','1'),(30,2,1,10,2,2,'2016-08-01 17:34:38','2016-08-01 17:34:38','1'),(31,1,2,10,2,2,'2016-08-01 17:34:38','2016-08-01 17:34:38','1'),(32,2,2,10,2,2,'2016-08-01 17:34:38','2016-08-01 17:34:38','1'),(33,1,4,10,2,2,'2016-08-01 17:34:38','2016-08-01 17:34:38','1'),(34,2,4,10,2,2,'2016-08-01 17:34:38','2016-08-01 17:34:38','1');
/*!40000 ALTER TABLE `lie_access_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_additional_home_page_slugs`
--

DROP TABLE IF EXISTS `lie_additional_home_page_slugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_additional_home_page_slugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_id` int(11) NOT NULL,
  `is_type` enum('city','state','country') NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description1` text NOT NULL,
  `description2` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `vedio_upload_type` enum('U','V') NOT NULL DEFAULT 'U' COMMENT 'U->URL,V->Vedio',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_additional_home_page_slugs`
--

LOCK TABLES `lie_additional_home_page_slugs` WRITE;
/*!40000 ALTER TABLE `lie_additional_home_page_slugs` DISABLE KEYS */;
INSERT INTO `lie_additional_home_page_slugs` VALUES (3,4,'country','austria','<p>hjk</p>\r\n','<p>fghjkl</p>\r\n','1469752065663.png','https://www.youtube.com/watch?v=nQ87zafslew','','U',2,2,'2016-08-30 15:26:08','2016-08-30 15:26:08','1'),(4,4,'state','punjab','<p>asdsadasdas dasd asd ads</p>\r\n','<p>sdasdas</p>\r\n','1469876096456.jpg','http://alivenetsolution.co/lieferzonas/root/home/pages/create','','U',2,2,'2016-07-30 16:42:58','2016-08-31 05:50:51','2'),(6,13,'state','niederoesterreich','<p><strong>Description</strong>&nbsp;is one of four&nbsp;<a href=\"https://en.wikipedia.org/wiki/Rhetorical_modes\">rhetorical modes</a>&nbsp;(also known as&nbsp;<em>modes of discourse</em>), along with&nbsp;<a href=\"https://en.wikipedia.org/wiki/Exposition_(literary_technique)\">exposition</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Argumentation\">argumentation</a>, and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Narrative_mode\">narration</a>. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Convention_(norm)\">conventions</a>. The act of description may be related to that of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Definition\">definition</a>. Description is also the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Fiction-writing_modes\">fiction-writing mode</a>&nbsp;for transmitting a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Imagination\">mental image</a>&nbsp;of the particulars of a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Narrative\">story</a>.[<em><a href=\"https://en.wikipedia.org/wiki/Wikipedia:Citation_needed\">citation needed</a></em>]&nbsp;Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events</p>\r\n','<p><strong>Description</strong>&nbsp;is one of four&nbsp;<a href=\"https://en.wikipedia.org/wiki/Rhetorical_modes\">rhetorical modes</a>&nbsp;(also known as&nbsp;<em>modes of discourse</em>), along with&nbsp;<a href=\"https://en.wikipedia.org/wiki/Exposition_(literary_technique)\">exposition</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Argumentation\">argumentation</a>, and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Narrative_mode\">narration</a>. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Convention_(norm)\">conventions</a>. The act of description may be related to that of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Definition\">definition</a>. Description is also the&nbsp;<a href=\"https://en.wikipedia.org/wiki/Fiction-writing_modes\">fiction-writing mode</a>&nbsp;for transmitting a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Imagination\">mental image</a>&nbsp;of the particulars of a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Narrative\">story</a>.[<em><a href=\"https://en.wikipedia.org/wiki/Wikipedia:Citation_needed\">citation needed</a></em>]&nbsp;Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events</p>\r\n','1473119940985.PNG','https://www.youtube.com/watch?v=bU9CFf3cXaA','','U',2,2,'2016-09-06 05:29:00','2016-09-06 05:29:00','1'),(7,25,'city','bregenz','<p>zuijkl</p>\r\n','<p>zjkl&ouml;</p>\r\n','1470871965812.jpg','youtube.com','','U',2,2,'2016-08-11 05:02:45','2016-08-11 05:02:45','1'),(8,21,'state','wien','<p>Ob Sushi oder Schnitzel, Pizza oder Pasta, traditionell oder exotisch: Lieferzonas.at vereint Qualit&auml;t und Vielfalt, und das in ganz &Ouml;sterreich.</p>\r\n\r\n<p>Wer spontan G&auml;ste empf&auml;ngt, wer nicht kochen mag, f&uuml;r den ist der neue Food-Delivery-Service</p>\r\n\r\n<p>ma&szlig;geschneidert &ndash; sogar wenn das Portemonnaie ebenso leer ist <a href=\"http://alivenetsolution.co/lieferzonas/\" target=\"_top\"><img alt=\"\" src=\"http://alivenetsolution.co/lieferzonas/\" style=\"height:10px; width:10px\" /></a>wie der Magen.</p>\r\n\r\n<p>Die Kunden zahlen auf Wunsch bargeldlos und sammeln mit jeder Bestellung Bonus- und Rabattpunkte sowie Cashback.</p>\r\n\r\n<p>Diese und andere einmalige Besonderheiten des neuen Services lassen sich nicht &uuml;ber Nacht implementieren.</p>\r\n\r\n<p>Damit von Beginn an die Technik reibungslos funktioniert, hat Lieferzonas.at seinen &ouml;sterreichweiten Start im sommer geplant.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>-</p>\r\n','<p>Ob Sushi oder Schnitzel, Pizza oder Pasta, traditionell oder exotisch: Lieferzonas.at vereint Qualit&auml;t und Vielfalt, und das in ganz &Ouml;sterreich. Wer spontan G&auml;ste empf&auml;ngt, wer nicht kochen mag, f&uuml;r den ist der neue Food-Delivery-Service ma&szlig;geschneidert &ndash; sogar wenn das Portemonnaie ebenso leer ist wie der Magen. Die Kunden zahlen auf Wunsch bargeldlos und sammeln mit jeder Bestellung Bonus- und Rabattpunkte sowie Cashback. Diese und andere einmalige Besonderheiten des neuen Services lassen sich nicht &uuml;ber Nacht implementieren. Damit von Beginn an die Technik reibungslos funktioniert, hat Lieferzonas.at seinen &ouml;sterreichweiten Start im sommer geplant.</p>\r\n','1472589753627.png','','1472589753493.mp4','V',2,2,'2016-08-31 05:44:12','2016-08-31 05:44:12','1'),(9,32,'city','baden','<p>dfcvbnnnnnnnnnnnnn</p>\r\n','<p>nnnnnnnnnnnnnnnn</p>\r\n','1472602833758.png','','1472602833568.mp4','V',2,2,'2016-08-31 05:50:33','2016-08-31 05:50:33','1'),(10,20,'state','kaernten-1','<p>Hier kann der user per Bundesland sein PLZ suchen, &nbsp;welche Restaurant zu der User Liefern.</p>\r\n','<p>Text 2</p>\r\n','1473118653386.PNG','https://www.youtube.com/watch?v=RBXajgKQ7XY','','U',2,2,'2016-09-06 05:07:33','2016-09-06 05:07:33','1'),(11,14,'state','burgenland','<p>Das&nbsp;<strong>Burgenland</strong>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Burgenlandkroatische_Sprache\">burgenlandkroatisch</a>&nbsp;<em>Gradi&scaron;?e,</em>&nbsp;<a href=\"https://de.wikipedia.org/wiki/Ungarische_Sprache\">ungarisch</a>&nbsp;Fels??rvid&eacute;k/<em>?rvid&eacute;k</em>,&nbsp;<em><a href=\"https://de.wikipedia.org/wiki/Lajtab%C3%A1ns%C3%A1g\">Lajtab&aacute;ns&aacute;g</a></em>&nbsp;oder neuerdings&nbsp;<em><a href=\"https://de.wikipedia.org/w/index.php?title=V%C3%A1rvid%C3%A9k&amp;action=edit&amp;redlink=1\">V&aacute;rvid&eacute;k</a></em>) ist ein&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_(%C3%96sterreich)\">Land</a>&nbsp;im Osten von&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">&Ouml;sterreich</a>. Landeshauptstadt ist&nbsp;<a href=\"https://de.wikipedia.org/wiki/Eisenstadt\">Eisenstadt</a>. Von den neun Bundesl&auml;ndern &Ouml;sterreichs ist es das &ouml;stlichste und gemessen an seiner Einwohnerzahl kleinste. Das Gebiet geh&ouml;rte einst zum&nbsp;<a href=\"https://de.wikipedia.org/wiki/K%C3%B6nigreich_Ungarn\">K&ouml;nigreich Ungarn</a>, das im&nbsp;<a href=\"https://de.wikipedia.org/wiki/Vertrag_von_Trianon\">Vertrag von Trianon</a>&nbsp;1920 verpflichtet wurde, das damalige<em><strong>Deutsch-Westungarn</strong></em>&nbsp;an die neue Republik &Ouml;sterreich abzutreten. 1921 kam die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Landnahme_des_Burgenlandes\">Landnahme des Burgenlandes</a>&nbsp;zu einem Abschluss; das neu hinzugekommene Land wurde danach in Burgenland umbenannt.</p>\r\n\r\n<p>Das Burgenland grenzt im Norden an das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Bratislavsk%C3%BD_kraj\">Pressburger Land</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Slowakei\">Slowakei</a>), im Osten an die Komitate&nbsp;<a href=\"https://de.wikipedia.org/wiki/Komitat_Gy%C5%91r-Moson-Sopron\">Gy?r-Moson-Sopron</a>&nbsp;und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Komitat_Vas\">Vas</a>&nbsp;(beide&nbsp;<a href=\"https://de.wikipedia.org/wiki/Ungarn\">Ungarn</a>), im S&uuml;den auf wenigen Kilometern an die zwei Gemeinden&nbsp;<a href=\"https://de.wikipedia.org/wiki/Kuzma\">Kuzma</a>und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Roga%C5%A1ovci\">Roga&scaron;ovci</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Slowenien\">Slowenien</a>) und im Westen an die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Steiermark\">Steiermark</a>&nbsp;und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Nieder%C3%B6sterreich\">Nieder&ouml;sterreich</a>.</p>\r\n\r\n<p>Die Au&szlig;engrenzen zu den vorgenannten Staaten bildeten bis zum 21.&nbsp;Dezember 2007 auf 397&nbsp;km die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Schengener_Abkommen\">Schengener</a>&nbsp;Au&szlig;engrenze der&nbsp;<a href=\"https://de.wikipedia.org/wiki/EU\">EU</a>.</p>\r\n\r\n<p>Das Burgenland ist gepr&auml;gt vom&nbsp;<a href=\"https://de.wikipedia.org/wiki/Neusiedler_See\">Neusiedler See</a>&nbsp;im Norden und den Ausl&auml;ufern der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Alpen\">Alpen</a>&nbsp;im h&uuml;geligen S&uuml;den, es ist langgezogen und verengt sich bei&nbsp;<a href=\"https://de.wikipedia.org/wiki/Sieggraben\">Sieggraben</a>&nbsp;im&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96denburger_Gebirge\">&Ouml;denburger Gebirge</a>&nbsp;auf 4&nbsp;km. Das Burgenland ist Mitglied der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Europaregion\">Europaregion</a>&nbsp;<a href=\"https://de.wikipedia.org/wiki/Centrope\">Centrope</a>.</p>\r\n','<p>Das&nbsp;<strong>Burgenland</strong>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Burgenlandkroatische_Sprache\">burgenlandkroatisch</a>&nbsp;<em>Gradi&scaron;?e,</em>&nbsp;<a href=\"https://de.wikipedia.org/wiki/Ungarische_Sprache\">ungarisch</a>&nbsp;Fels??rvid&eacute;k/<em>?rvid&eacute;k</em>,&nbsp;<em><a href=\"https://de.wikipedia.org/wiki/Lajtab%C3%A1ns%C3%A1g\">Lajtab&aacute;ns&aacute;g</a></em>&nbsp;oder neuerdings&nbsp;<em><a href=\"https://de.wikipedia.org/w/index.php?title=V%C3%A1rvid%C3%A9k&amp;action=edit&amp;redlink=1\">V&aacute;rvid&eacute;k</a></em>) ist ein&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_(%C3%96sterreich)\">Land</a>&nbsp;im Osten von&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">&Ouml;sterreich</a>. Landeshauptstadt ist&nbsp;<a href=\"https://de.wikipedia.org/wiki/Eisenstadt\">Eisenstadt</a>. Von den neun Bundesl&auml;ndern &Ouml;sterreichs ist es das &ouml;stlichste und gemessen an seiner Einwohnerzahl kleinste. Das Gebiet geh&ouml;rte einst zum&nbsp;<a href=\"https://de.wikipedia.org/wiki/K%C3%B6nigreich_Ungarn\">K&ouml;nigreich Ungarn</a>, das im&nbsp;<a href=\"https://de.wikipedia.org/wiki/Vertrag_von_Trianon\">Vertrag von Trianon</a>&nbsp;1920 verpflichtet wurde, das damalige<em><strong>Deutsch-Westungarn</strong></em>&nbsp;an die neue Republik &Ouml;sterreich abzutreten. 1921 kam die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Landnahme_des_Burgenlandes\">Landnahme des Burgenlandes</a>&nbsp;zu einem Abschluss; das neu hinzugekommene Land wurde danach in Burgenland umbenannt.</p>\r\n\r\n<p>Das Burgenland grenzt im Norden an das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Bratislavsk%C3%BD_kraj\">Pressburger Land</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Slowakei\">Slowakei</a>), im Osten an die Komitate&nbsp;<a href=\"https://de.wikipedia.org/wiki/Komitat_Gy%C5%91r-Moson-Sopron\">Gy?r-Moson-Sopron</a>&nbsp;und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Komitat_Vas\">Vas</a>&nbsp;(beide&nbsp;<a href=\"https://de.wikipedia.org/wiki/Ungarn\">Ungarn</a>), im S&uuml;den auf wenigen Kilometern an die zwei Gemeinden&nbsp;<a href=\"https://de.wikipedia.org/wiki/Kuzma\">Kuzma</a>und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Roga%C5%A1ovci\">Roga&scaron;ovci</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Slowenien\">Slowenien</a>) und im Westen an die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Steiermark\">Steiermark</a>&nbsp;und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Nieder%C3%B6sterreich\">Nieder&ouml;sterreich</a>.</p>\r\n\r\n<p>Die Au&szlig;engrenzen zu den vorgenannten Staaten bildeten bis zum 21.&nbsp;Dezember 2007 auf 397&nbsp;km die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Schengener_Abkommen\">Schengener</a>&nbsp;Au&szlig;engrenze der&nbsp;<a href=\"https://de.wikipedia.org/wiki/EU\">EU</a>.</p>\r\n\r\n<p>Das Burgenland ist gepr&auml;gt vom&nbsp;<a href=\"https://de.wikipedia.org/wiki/Neusiedler_See\">Neusiedler See</a>&nbsp;im Norden und den Ausl&auml;ufern der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Alpen\">Alpen</a>&nbsp;im h&uuml;geligen S&uuml;den, es ist langgezogen und verengt sich bei&nbsp;<a href=\"https://de.wikipedia.org/wiki/Sieggraben\">Sieggraben</a>&nbsp;im&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96denburger_Gebirge\">&Ouml;denburger Gebirge</a>&nbsp;auf 4&nbsp;km. Das Burgenland ist Mitglied der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Europaregion\">Europaregion</a>&nbsp;<a href=\"https://de.wikipedia.org/wiki/Centrope\">Centrope</a>.</p>\r\n','1473120889250.PNG','https://www.youtube.com/watch?v=SVLdOHGy3X8','','U',2,2,'2016-09-06 05:44:49','2016-09-06 05:44:49','1'),(12,15,'state','oberoesterreich','<p><strong>Ober&ouml;sterreich</strong>&nbsp;ist ein&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_(%C3%96sterreich)\">&ouml;sterreichisches Bundesland</a>;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Landeshauptstadt_(%C3%96sterreich)\">Landeshauptstadt</a>&nbsp;ist&nbsp;<a href=\"https://de.wikipedia.org/wiki/Linz\">Linz</a>. Ober&ouml;sterreich ist mit 11.982&nbsp;Quadratkilometern fl&auml;chenm&auml;&szlig;ig das viertgr&ouml;&szlig;te und mit 1,4&nbsp;Millionen Einwohnern bev&ouml;lkerungsm&auml;&szlig;ig das drittgr&ouml;&szlig;te Bundesland&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">&Ouml;sterreichs</a>. Es grenzt an&nbsp;<a href=\"https://de.wikipedia.org/wiki/Bayern\">Bayern</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Deutschland\">Deutschland</a>),&nbsp;<a href=\"https://de.wikipedia.org/wiki/Jiho%C4%8Desk%C3%BD_kraj\">S&uuml;db&ouml;hmen</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Tschechien\">Tschechien</a>) sowie inner&ouml;sterreichisch an&nbsp;<a href=\"https://de.wikipedia.org/wiki/Nieder%C3%B6sterreich\">Nieder&ouml;sterreich</a>, die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Steiermark\">Steiermark</a>&nbsp;und das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_Salzburg\">Land Salzburg</a>. Der Name des Landes leitet sich ab vom Namen des Vorg&auml;ngerterritoriums, des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Erzherzogtum_%C3%96sterreich\">Erzherzogtums</a>&nbsp;<em><a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich_ob_der_Enns\">&Ouml;sterreich ob der Enns</a></em>, einem der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Habsburgische_Erblande\">habsburgischen Erblande</a>.</p>\r\n','<p><strong>Ober&ouml;sterreich</strong>&nbsp;ist ein&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_(%C3%96sterreich)\">&ouml;sterreichisches Bundesland</a>;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Landeshauptstadt_(%C3%96sterreich)\">Landeshauptstadt</a>&nbsp;ist&nbsp;<a href=\"https://de.wikipedia.org/wiki/Linz\">Linz</a>. Ober&ouml;sterreich ist mit 11.982&nbsp;Quadratkilometern fl&auml;chenm&auml;&szlig;ig das viertgr&ouml;&szlig;te und mit 1,4&nbsp;Millionen Einwohnern bev&ouml;lkerungsm&auml;&szlig;ig das drittgr&ouml;&szlig;te Bundesland&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">&Ouml;sterreichs</a>. Es grenzt an&nbsp;<a href=\"https://de.wikipedia.org/wiki/Bayern\">Bayern</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Deutschland\">Deutschland</a>),&nbsp;<a href=\"https://de.wikipedia.org/wiki/Jiho%C4%8Desk%C3%BD_kraj\">S&uuml;db&ouml;hmen</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Tschechien\">Tschechien</a>) sowie inner&ouml;sterreichisch an&nbsp;<a href=\"https://de.wikipedia.org/wiki/Nieder%C3%B6sterreich\">Nieder&ouml;sterreich</a>, die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Steiermark\">Steiermark</a>&nbsp;und das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_Salzburg\">Land Salzburg</a>. Der Name des Landes leitet sich ab vom Namen des Vorg&auml;ngerterritoriums, des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Erzherzogtum_%C3%96sterreich\">Erzherzogtums</a>&nbsp;<em><a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich_ob_der_Enns\">&Ouml;sterreich ob der Enns</a></em>, einem der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Habsburgische_Erblande\">habsburgischen Erblande</a>.</p>\r\n','1473121472494.PNG','https://www.youtube.com/watch?v=EvgWlbJEL0c','','U',2,2,'2016-09-06 05:54:32','2016-09-06 05:54:32','1'),(13,16,'state','salzburg-1','<p>Die&nbsp;<strong>Stadt Salzburg</strong>, durch welche die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Salzach\">Salzach</a>&nbsp;flie&szlig;t, liegt in der Mitte des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Salzburger_Becken\">Salzburger Beckens</a>. Sie ist Hauptstadt des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_Salzburg\">Landes Salzburg</a>, einem&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_(%C3%96sterreich)\">&Ouml;sterreichischen Bundesland</a>, und mit 150.887 Einwohnern (Stand 1.&nbsp;J&auml;nner 2016)<a href=\"https://de.wikipedia.org/wiki/Salzburg#cite_note-2\">[2]</a>&nbsp;die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Liste_der_St%C3%A4dte_in_%C3%96sterreich#St.C3.A4dte_.C3.96sterreichs\">viertgr&ouml;&szlig;te Stadt</a>&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">&Ouml;sterreichs</a>.<a href=\"https://de.wikipedia.org/wiki/Salzburg#cite_note-3\">[3]</a>&nbsp;Der Nordwesten der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Statutarstadt_(%C3%96sterreich)\">Statutarstadt</a>&nbsp;Salzburg grenzt an&nbsp;<a href=\"https://de.wikipedia.org/wiki/Freilassing\">Freilassing</a>&nbsp;im&nbsp;<a href=\"https://de.wikipedia.org/wiki/Bayern\">Freistaat Bayern</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Landkreis_Berchtesgadener_Land\">Landkreis Berchtesgadener Land</a>), das &uuml;brige Stadtgebiet an den&nbsp;<a href=\"https://de.wikipedia.org/wiki/Bezirk_Salzburg-Umgebung\">Bezirk Salzburg-Umgebung</a>, landl&auml;ufig&nbsp;<em>Flachgau</em>&nbsp;genannt.</p>\r\n\r\n<p>Im Jahr 488 begann am selben Ort der Niedergang der r&ouml;mischen Stadt&nbsp;<a href=\"https://de.wikipedia.org/wiki/Iuvavum\">Iuvavum</a>. Salzburg wurde 696 als Bischofssitz neu gegr&uuml;ndet und 798 zum Sitz des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Erzbischof\">Erzbischofs</a>. Die Haupteinnahmequellen Salzburgs bildeten Salzgewinnung und -handel sowie zeitweise der Goldbergbau. Die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Festung_Hohensalzburg\">Festung Hohensalzburg</a>&nbsp;stammt im Kern aus dem 11. Jahrhundert. Sie ist eine der gr&ouml;&szlig;ten mittelalterlichen Burganlagen in Europa und ein Wahrzeichen der Stadt. Ab dem 17. Jahrhundert wurde die Stadt von Erzbischof&nbsp;<a href=\"https://de.wikipedia.org/wiki/Wolf_Dietrich_von_Raitenau\">Wolf Dietrich</a>&nbsp;und dessen Nachfolgern als&nbsp;<a href=\"https://de.wikipedia.org/wiki/Residenzstadt\">Residenzstadt</a>&nbsp;prunkvoll ausgestattet. Zu dieser Zeit wurde im S&uuml;den der Stadt auch das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Schloss_Hellbrunn\">Schloss Hellbrunn</a>&nbsp;samt Schlosspark, Wasserspielen und Alleen errichtet. Als bekanntester Salzburger gilt der 1756 hier geborene Komponist<a href=\"https://de.wikipedia.org/wiki/Wolfgang_Amadeus_Mozart\">Wolfgang Amadeus Mozart</a>, weshalb die Stadt den Beinamen &bdquo;Mozartstadt&ldquo; und der Flughafen den Namen&nbsp;<em>Salzburg Airport W. A. Mozart</em>&nbsp;tr&auml;gt. Das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Historisches_Zentrum_der_Stadt_Salzburg\">historische Zentrum der Stadt</a>&nbsp;steht seit 1996 auf der Liste des&nbsp;<a href=\"https://de.wikipedia.org/wiki/UNESCO-Welterbe\">Weltkulturerbes</a>&nbsp;der&nbsp;<a href=\"https://de.wikipedia.org/wiki/UNESCO\">UNESCO</a>.</p>\r\n\r\n<p>Heute ist Salzburg ein bedeutender Messe- und Kongressstandort mit vielen Handels- und Dienstleistungsbetrieben sowie einem leistungsf&auml;higen Tourismusbereich. Daneben ist die Stadt durch die<a href=\"https://de.wikipedia.org/wiki/Salzburger_Festspiele\">Salzburger Festspiele</a>&nbsp;international bedeutsam, was ihr den weiteren Beinamen &bdquo;Festspielstadt&ldquo; einbrachte. Die Stadt Salzburg bildet auf Grund ihrer verkehrsg&uuml;nstigen Lage den Kern der grenz&uuml;berschreitenden&nbsp;<em><a href=\"https://de.wikipedia.org/wiki/EuRegio_Salzburg_%E2%80%93_Berchtesgadener_Land_%E2%80%93_Traunstein\">EuRegio Salzburg &ndash; Berchtesgadener Land &ndash; Traunstein</a></em>. Zudem ist sie Verkehrsknotenpunkt f&uuml;r je eine der wichtigsten West&ndash;Ost- sowie transalpinen Stra&szlig;en- und Schienenrouten Europas (<a href=\"https://de.wikipedia.org/wiki/M%C3%BCnchen\">M&uuml;nchen</a>&nbsp;&ndash;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Wien\">Wien</a>&nbsp;&ndash;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Budapest\">Budapest</a>, Salzburg &ndash;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Villach\">Villach</a>&nbsp;&ndash;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Udine\">Udine</a>).</p>\r\n','<p>Die&nbsp;<strong>Stadt Salzburg</strong>, durch welche die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Salzach\">Salzach</a>&nbsp;flie&szlig;t, liegt in der Mitte des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Salzburger_Becken\">Salzburger Beckens</a>. Sie ist Hauptstadt des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_Salzburg\">Landes Salzburg</a>, einem&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_(%C3%96sterreich)\">&Ouml;sterreichischen Bundesland</a>, und mit 150.887 Einwohnern (Stand 1.&nbsp;J&auml;nner 2016)<a href=\"https://de.wikipedia.org/wiki/Salzburg#cite_note-2\">[2]</a>&nbsp;die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Liste_der_St%C3%A4dte_in_%C3%96sterreich#St.C3.A4dte_.C3.96sterreichs\">viertgr&ouml;&szlig;te Stadt</a>&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">&Ouml;sterreichs</a>.<a href=\"https://de.wikipedia.org/wiki/Salzburg#cite_note-3\">[3]</a>&nbsp;Der Nordwesten der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Statutarstadt_(%C3%96sterreich)\">Statutarstadt</a>&nbsp;Salzburg grenzt an&nbsp;<a href=\"https://de.wikipedia.org/wiki/Freilassing\">Freilassing</a>&nbsp;im&nbsp;<a href=\"https://de.wikipedia.org/wiki/Bayern\">Freistaat Bayern</a>&nbsp;(<a href=\"https://de.wikipedia.org/wiki/Landkreis_Berchtesgadener_Land\">Landkreis Berchtesgadener Land</a>), das &uuml;brige Stadtgebiet an den&nbsp;<a href=\"https://de.wikipedia.org/wiki/Bezirk_Salzburg-Umgebung\">Bezirk Salzburg-Umgebung</a>, landl&auml;ufig&nbsp;<em>Flachgau</em>&nbsp;genannt.</p>\r\n\r\n<p>Im Jahr 488 begann am selben Ort der Niedergang der r&ouml;mischen Stadt&nbsp;<a href=\"https://de.wikipedia.org/wiki/Iuvavum\">Iuvavum</a>. Salzburg wurde 696 als Bischofssitz neu gegr&uuml;ndet und 798 zum Sitz des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Erzbischof\">Erzbischofs</a>. Die Haupteinnahmequellen Salzburgs bildeten Salzgewinnung und -handel sowie zeitweise der Goldbergbau. Die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Festung_Hohensalzburg\">Festung Hohensalzburg</a>&nbsp;stammt im Kern aus dem 11. Jahrhundert. Sie ist eine der gr&ouml;&szlig;ten mittelalterlichen Burganlagen in Europa und ein Wahrzeichen der Stadt. Ab dem 17. Jahrhundert wurde die Stadt von Erzbischof&nbsp;<a href=\"https://de.wikipedia.org/wiki/Wolf_Dietrich_von_Raitenau\">Wolf Dietrich</a>&nbsp;und dessen Nachfolgern als&nbsp;<a href=\"https://de.wikipedia.org/wiki/Residenzstadt\">Residenzstadt</a>&nbsp;prunkvoll ausgestattet. Zu dieser Zeit wurde im S&uuml;den der Stadt auch das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Schloss_Hellbrunn\">Schloss Hellbrunn</a>&nbsp;samt Schlosspark, Wasserspielen und Alleen errichtet. Als bekanntester Salzburger gilt der 1756 hier geborene Komponist<a href=\"https://de.wikipedia.org/wiki/Wolfgang_Amadeus_Mozart\">Wolfgang Amadeus Mozart</a>, weshalb die Stadt den Beinamen &bdquo;Mozartstadt&ldquo; und der Flughafen den Namen&nbsp;<em>Salzburg Airport W. A. Mozart</em>&nbsp;tr&auml;gt. Das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Historisches_Zentrum_der_Stadt_Salzburg\">historische Zentrum der Stadt</a>&nbsp;steht seit 1996 auf der Liste des&nbsp;<a href=\"https://de.wikipedia.org/wiki/UNESCO-Welterbe\">Weltkulturerbes</a>&nbsp;der&nbsp;<a href=\"https://de.wikipedia.org/wiki/UNESCO\">UNESCO</a>.</p>\r\n\r\n<p>Heute ist Salzburg ein bedeutender Messe- und Kongressstandort mit vielen Handels- und Dienstleistungsbetrieben sowie einem leistungsf&auml;higen Tourismusbereich. Daneben ist die Stadt durch die<a href=\"https://de.wikipedia.org/wiki/Salzburger_Festspiele\">Salzburger Festspiele</a>&nbsp;international bedeutsam, was ihr den weiteren Beinamen &bdquo;Festspielstadt&ldquo; einbrachte. Die Stadt Salzburg bildet auf Grund ihrer verkehrsg&uuml;nstigen Lage den Kern der grenz&uuml;berschreitenden&nbsp;<em><a href=\"https://de.wikipedia.org/wiki/EuRegio_Salzburg_%E2%80%93_Berchtesgadener_Land_%E2%80%93_Traunstein\">EuRegio Salzburg &ndash; Berchtesgadener Land &ndash; Traunstein</a></em>. Zudem ist sie Verkehrsknotenpunkt f&uuml;r je eine der wichtigsten West&ndash;Ost- sowie transalpinen Stra&szlig;en- und Schienenrouten Europas (<a href=\"https://de.wikipedia.org/wiki/M%C3%BCnchen\">M&uuml;nchen</a>&nbsp;&ndash;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Wien\">Wien</a>&nbsp;&ndash;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Budapest\">Budapest</a>, Salzburg &ndash;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Villach\">Villach</a>&nbsp;&ndash;&nbsp;<a href=\"https://de.wikipedia.org/wiki/Udine\">Udine</a>).</p>\r\n','1473121769169.PNG','https://www.youtube.com/watch?v=nPntDiARQYw','','U',2,2,'2016-09-06 05:59:29','2016-09-06 05:59:29','1'),(14,17,'state','steiermarkt-1','<p>Die&nbsp;<strong>Steiermark</strong>&nbsp;ist eines der neun&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_(%C3%96sterreich)\">Bundesl&auml;nder</a>&nbsp;der&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">Republik &Ouml;sterreich</a>. Ihre Hauptstadt ist&nbsp;<a href=\"https://de.wikipedia.org/wiki/Graz\">Graz</a>. Es ist fl&auml;chenbezogen das zweitgr&ouml;&szlig;te, der Einwohnerzahl nach das viertgr&ouml;&szlig;te Bundesland &Ouml;sterreichs und grenzt an die &ouml;sterreichischen Bundesl&auml;nder&nbsp;<a href=\"https://de.wikipedia.org/wiki/K%C3%A4rnten\">K&auml;rnten</a>,&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_Salzburg\">Salzburg</a>,&nbsp;<a href=\"https://de.wikipedia.org/wiki/Ober%C3%B6sterreich\">Ober&ouml;sterreich</a>,&nbsp;<a href=\"https://de.wikipedia.org/wiki/Nieder%C3%B6sterreich\">Nieder&ouml;sterreich</a>&nbsp;und das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Burgenland\">Burgenland</a>&nbsp;sowie im S&uuml;den an die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Slowenien\">Republik Slowenien</a>. Die Bewohner werden als&nbsp;<em>Steirer</em>&nbsp;bezeichnet.</p>\r\n\r\n<p>Bis zum Ende des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Erster_Weltkrieg\">Ersten Weltkrieges</a>&nbsp;gab es das weitaus gr&ouml;&szlig;ere&nbsp;<a href=\"https://de.wikipedia.org/wiki/Herzogtum_Steiermark\">Herzogtum Steiermark</a>&nbsp;als&nbsp;<a href=\"https://de.wikipedia.org/wiki/Kronland_(%C3%96sterreich)\">Kronland</a>&nbsp;der Habsburger Donaumonarchie&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich-Ungarn\">&Ouml;sterreich-Ungarn</a>. Seit dem&nbsp;<a href=\"https://de.wikipedia.org/wiki/Vertrag_von_Saint-Germain\">Vertrag von Saint-Germain</a>&nbsp;(1919) und dem Untergang des Vielv&ouml;lkerstaates geh&ouml;rt die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Untersteiermark\">Untersteiermark</a>&nbsp;zu Slowenien.</p>\r\n','<p>Die&nbsp;<strong>Steiermark</strong>&nbsp;ist eines der neun&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_(%C3%96sterreich)\">Bundesl&auml;nder</a>&nbsp;der&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">Republik &Ouml;sterreich</a>. Ihre Hauptstadt ist&nbsp;<a href=\"https://de.wikipedia.org/wiki/Graz\">Graz</a>. Es ist fl&auml;chenbezogen das zweitgr&ouml;&szlig;te, der Einwohnerzahl nach das viertgr&ouml;&szlig;te Bundesland &Ouml;sterreichs und grenzt an die &ouml;sterreichischen Bundesl&auml;nder&nbsp;<a href=\"https://de.wikipedia.org/wiki/K%C3%A4rnten\">K&auml;rnten</a>,&nbsp;<a href=\"https://de.wikipedia.org/wiki/Land_Salzburg\">Salzburg</a>,&nbsp;<a href=\"https://de.wikipedia.org/wiki/Ober%C3%B6sterreich\">Ober&ouml;sterreich</a>,&nbsp;<a href=\"https://de.wikipedia.org/wiki/Nieder%C3%B6sterreich\">Nieder&ouml;sterreich</a>&nbsp;und das&nbsp;<a href=\"https://de.wikipedia.org/wiki/Burgenland\">Burgenland</a>&nbsp;sowie im S&uuml;den an die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Slowenien\">Republik Slowenien</a>. Die Bewohner werden als&nbsp;<em>Steirer</em>&nbsp;bezeichnet.</p>\r\n\r\n<p>Bis zum Ende des&nbsp;<a href=\"https://de.wikipedia.org/wiki/Erster_Weltkrieg\">Ersten Weltkrieges</a>&nbsp;gab es das weitaus gr&ouml;&szlig;ere&nbsp;<a href=\"https://de.wikipedia.org/wiki/Herzogtum_Steiermark\">Herzogtum Steiermark</a>&nbsp;als&nbsp;<a href=\"https://de.wikipedia.org/wiki/Kronland_(%C3%96sterreich)\">Kronland</a>&nbsp;der Habsburger Donaumonarchie&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich-Ungarn\">&Ouml;sterreich-Ungarn</a>. Seit dem&nbsp;<a href=\"https://de.wikipedia.org/wiki/Vertrag_von_Saint-Germain\">Vertrag von Saint-Germain</a>&nbsp;(1919) und dem Untergang des Vielv&ouml;lkerstaates geh&ouml;rt die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Untersteiermark\">Untersteiermark</a>&nbsp;zu Slowenien.</p>\r\n','1473122237638.PNG','https://youtu.be/6rbrBBv20GM','','U',2,2,'2016-09-06 06:07:17','2016-09-06 06:07:17','1'),(15,19,'state','tirol-1','<p><strong>Tirol</strong>&nbsp;ist eine Region in den&nbsp;<a href=\"https://de.wikipedia.org/wiki/Alpen\">Alpen</a>&nbsp;im Westen&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">&Ouml;sterreichs</a>&nbsp;und Norden&nbsp;<a href=\"https://de.wikipedia.org/wiki/Italien\">Italiens</a>. Seit dem Jahr 2011 besitzt die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Europaregion_Tirol-S%C3%BCdtirol-Trentino\">Region Tirol</a>&nbsp;eine eigene Rechtspers&ouml;nlichkeit in Form eines&nbsp;<a href=\"https://de.wikipedia.org/wiki/Europ%C3%A4ischer_Verbund_f%C3%BCr_territoriale_Zusammenarbeit\">Europ&auml;ischen Verbundes f&uuml;r territoriale Zusammenarbeit</a>.</p>\r\n\r\n<p>Das Gebiet stand als&nbsp;<em><a href=\"https://de.wikipedia.org/wiki/Gef%C3%BCrstete_Grafschaft_Tirol\">Grafschaft Tirol</a></em>&nbsp;lange Zeit unter einer gemeinsamen Herrschaft. Nach dem&nbsp;<a href=\"https://de.wikipedia.org/wiki/Erster_Weltkrieg\">Ersten Weltkrieg</a>&nbsp;und dem Untergang des Habsburgerreiches (<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich-Ungarn\">&Ouml;sterreich-Ungarn</a>) wurde Tirol im Jahre 1919 durch den&nbsp;<a href=\"https://de.wikipedia.org/wiki/Vertrag_von_St._Germain\">Vertrag von St.&nbsp;Germain</a>&nbsp;aufgeteilt:</p>\r\n\r\n<ol>\r\n	<li><a href=\"https://de.wikipedia.org/wiki/Nordtirol\">Nordtirol</a>&nbsp;und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Osttirol\">Osttirol</a>&nbsp;(das heutige&nbsp;<a href=\"https://de.wikipedia.org/wiki/Tirol_(Bundesland)\">Bundesland Tirol</a>) verblieben bei der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Geschichte_%C3%96sterreichs#Erste_Republik_und_Austrofaschismus_.281918.E2.80.931938.29\">Republik &Ouml;sterreich</a></li>\r\n	<li><a href=\"https://de.wikipedia.org/wiki/S%C3%BCdtirol\">S&uuml;dtirol</a>&nbsp;und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Trentino\">Welschtirol</a>, die die heutige&nbsp;<a href=\"https://de.wikipedia.org/wiki/Trentino-S%C3%BCdtirol\">Autonome Region Trentino-S&uuml;dtirol</a>&nbsp;bilden, kamen zur&nbsp;<a href=\"https://de.wikipedia.org/wiki/Italien\">Italienischen Republik</a>.</li>\r\n</ol>\r\n','<p><strong>Tirol</strong>&nbsp;ist eine Region in den&nbsp;<a href=\"https://de.wikipedia.org/wiki/Alpen\">Alpen</a>&nbsp;im Westen&nbsp;<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich\">&Ouml;sterreichs</a>&nbsp;und Norden&nbsp;<a href=\"https://de.wikipedia.org/wiki/Italien\">Italiens</a>. Seit dem Jahr 2011 besitzt die&nbsp;<a href=\"https://de.wikipedia.org/wiki/Europaregion_Tirol-S%C3%BCdtirol-Trentino\">Region Tirol</a>&nbsp;eine eigene Rechtspers&ouml;nlichkeit in Form eines&nbsp;<a href=\"https://de.wikipedia.org/wiki/Europ%C3%A4ischer_Verbund_f%C3%BCr_territoriale_Zusammenarbeit\">Europ&auml;ischen Verbundes f&uuml;r territoriale Zusammenarbeit</a>.</p>\r\n\r\n<p>Das Gebiet stand als&nbsp;<em><a href=\"https://de.wikipedia.org/wiki/Gef%C3%BCrstete_Grafschaft_Tirol\">Grafschaft Tirol</a></em>&nbsp;lange Zeit unter einer gemeinsamen Herrschaft. Nach dem&nbsp;<a href=\"https://de.wikipedia.org/wiki/Erster_Weltkrieg\">Ersten Weltkrieg</a>&nbsp;und dem Untergang des Habsburgerreiches (<a href=\"https://de.wikipedia.org/wiki/%C3%96sterreich-Ungarn\">&Ouml;sterreich-Ungarn</a>) wurde Tirol im Jahre 1919 durch den&nbsp;<a href=\"https://de.wikipedia.org/wiki/Vertrag_von_St._Germain\">Vertrag von St.&nbsp;Germain</a>&nbsp;aufgeteilt:</p>\r\n\r\n<ol>\r\n	<li><a href=\"https://de.wikipedia.org/wiki/Nordtirol\">Nordtirol</a>&nbsp;und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Osttirol\">Osttirol</a>&nbsp;(das heutige&nbsp;<a href=\"https://de.wikipedia.org/wiki/Tirol_(Bundesland)\">Bundesland Tirol</a>) verblieben bei der&nbsp;<a href=\"https://de.wikipedia.org/wiki/Geschichte_%C3%96sterreichs#Erste_Republik_und_Austrofaschismus_.281918.E2.80.931938.29\">Republik &Ouml;sterreich</a></li>\r\n	<li><a href=\"https://de.wikipedia.org/wiki/S%C3%BCdtirol\">S&uuml;dtirol</a>&nbsp;und&nbsp;<a href=\"https://de.wikipedia.org/wiki/Trentino\">Welschtirol</a>, die die heutige&nbsp;<a href=\"https://de.wikipedia.org/wiki/Trentino-S%C3%BCdtirol\">Autonome Region Trentino-S&uuml;dtirol</a>&nbsp;bilden, kamen zur&nbsp;<a href=\"https://de.wikipedia.org/wiki/Italien\">Italienischen Republik</a>.</li>\r\n</ol>\r\n','1473122909261.PNG','https://youtu.be/DOhR6AngT0M','','U',2,2,'2016-09-06 06:18:29','2016-09-06 06:18:29','1'),(16,18,'state','vorarlberg-1','<p>fghjkl&ouml;jjjjjjjjjjj</p>\r\n','<p>hhhhhhhhhhhhh</p>\r\n','1473123246508.PNG','https://youtu.be/xD-pKTJPofU','','U',2,2,'2016-09-06 06:24:06','2016-09-06 06:24:06','1');
/*!40000 ALTER TABLE `lie_additional_home_page_slugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_alergic_contents`
--

DROP TABLE IF EXISTS `lie_alergic_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_alergic_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_alergic_contents`
--

LOCK TABLES `lie_alergic_contents` WRITE;
/*!40000 ALTER TABLE `lie_alergic_contents` DISABLE KEYS */;
INSERT INTO `lie_alergic_contents` VALUES (1,'Chilli','This is a chilli ',2,2,'2016-07-19 17:53:09','2016-08-13 05:55:25','2'),(2,'Fish oil','This is a fish oil',2,2,'2016-07-19 17:53:34','2016-08-13 05:55:25','2'),(3,'fatty','fattyyyyy',2,2,'2016-07-19 18:07:13','2016-08-13 05:55:25','2'),(4,'asdasdad','sdasdasdasdasd',2,2,'2016-07-19 18:07:35','2016-07-19 18:07:40','2'),(5,'GLUTEN','Gluten-haltiges Getreide und daraus Gewonnene Erzeugung.\r\n\r\nz.B. Brot und Gebäck, Kuchen, Teigwaren, Suppen, Soßen,\r\nPaniermehl, Semmelbrösel, Wurstwaren, Backerbsen,\r\nFrischkornbreie, Desserts, Schokolade',2,2,'2016-08-13 05:54:39','2016-08-13 05:55:25','2'),(6,'Gluten','Gluten-haltiges Getreide und daraus Gewonnene Erzeugung.\r\n\r\nz.B. Brot und Gebäck, Kuchen, Teigwaren, Suppen, Soßen,\r\nPaniermehl, Semmelbrösel, Wurstwaren, Backerbsen,\r\nFrischkornbreie, Desserts, Schokolade',2,2,'2016-08-13 05:55:46','2016-08-13 05:56:11','1'),(7,'Krebstiere','Krebstiere und daraus gewonnene Erzeugung\r\n\r\nz.B. Feinkostsalate, Suppen, Soßen, Paella, Bouillabaisse, Sashimi, Surimi',2,2,'2016-08-13 05:59:03','2016-08-13 05:59:03','1'),(8,'Eier','Eier vom Geflügel und daraus gewonnene Erzeugung\r\n\r\nz.B. Eierteigwaren, panierte Speisen, Mayonnaise, Palatschinken, Kuchen,\r\nGebäck, Brot, Nudeln, Croutons, Faschierter Braten, Burger,\r\nFeinkostsalate, Pasteten, Quiches, Soßen, Dressings, Desserts',2,2,'2016-08-13 06:00:44','2016-08-13 06:00:44','1'),(9,'Fisch','Fisch und daraus gewonnene Erzeugung\r\n(AUSSER FISCHGELATINE)\r\n\r\nz.B. Kräcker, Soßen, Suppen, Würzpasteten, Würste, Surimi,\r\nSardellenwurst, Brotaufstriche, Feinkostsalate, Pasteten,\r\n Vitello tonnato\r\n',2,2,'2016-08-13 06:02:00','2016-08-13 06:02:41','1'),(10,'Erdnuesse','Erdnüsse und daraus gewonnene Erzeugung\r\n\r\nz.B. Margarine, Brot, Kuchen, Gebäck, Schokocreme, Brotaufstriche,\r\nCerealien, Müsli, Frühstücksflocken, Schokolade, Feinkostsalate, Marinaden,\r\nSatésauße, Eis, aromatisierter Kaffee, Likör, (Pommes Frites)',2,2,'2016-08-13 06:05:13','2016-08-13 06:05:13','1'),(11,'Sojabohnen','Sojabohnen und daraus gewonnene Erzeugnisse\r\n\r\nz.B. Brot, Kuchen, Gebäck, Feinkostsalate, Margarine,\r\nSchokocreme, Brotaufstriche, Müsli, Schokolade, Kekse, Kaugummi, Soßen,\r\nDressings, Marinaden, Mayonnaise, Eis, Sportlernahrung, Diätdrinks,\r\nKaffeeweißer',2,2,'2016-08-13 06:07:30','2016-08-13 06:07:30','1'),(12,'Milch','Milch vom Säugetieren und Milcherzeugnisse\r\n(Inklusive Laktose)\r\n\r\nz.B. Brot, Kuchen, Gebäck, Brüh-, Koch-, Roh,Bratwurst, Feinkostsalate,\r\nMargarine, Nussnougatcreme, Müsli, Schokolade, Karamell, Aufläufe, Gratin,\r\nKartoffelpürree, Kroketten, Pommes Frites, Chips, Suppen, Soßen, Dressing,\r\nMarinaden, Desserts, Kakao, Wein ',2,2,'2016-08-13 06:09:41','2016-08-13 06:09:41','1'),(13,'Schalenfruechte','Schalenfrüchte und daraus gewonnene Erzeugung\r\n\r\nz.B. Brot, Kuchen, Gebäck, Brühwürste (Pistazien), Rohwürste (Walnüsse),\r\nPasteten, Feinkostsalate (Waldorf), Joghurt, Käse, Nuss-/ Nougatcreme,\r\nAufstriche, Müsli, Schokolade, Marzipan, Müsliriegel, Kekse,\r\nDressings, Curry, Pesto, Desserts, Likör, aromatisierter Kaffee\r\n\r\n',2,2,'2016-08-13 06:12:17','2016-08-13 06:12:17','1'),(14,'Sellerie','Sellerie und daraus gewonnene Erzeugnisse\r\n\r\nz.B. Suppengrün, Gewürzbrot, Wurst, Fleischerzeugnisse,\r\nFleischzubereitungen, Kräuterkäse, Fertiggerichte, Feinkostsalate,\r\nBrühe, Suppen, Eintopf, Marinaden, Gewürzmischungen, Curry,\r\nsalzige Snacks (Chips)',2,2,'2016-08-13 06:14:59','2016-08-13 06:14:59','1'),(15,'Senf','Senf und daraus gewonnene Erzeugnisse\r\n\r\nz.B. Fleischerzeugnisse, Feinkostsalate, Suppen, Soßen, Dressing, Mayonnaise,\r\nKetchup, eingelegtes Gemüse und Gewürzmischungen, Käse, Essiggurken\r\n',2,2,'2016-08-13 06:16:40','2016-08-13 06:16:40','1'),(16,'Sesam','Sesamsamen und daraus gewonnene Erzeugnisse\r\n\r\nz.B. Brot, Knäckebrot, Gebäck (süß und salzig), Müsli, vegetarische Gerichte,\r\nFalafel, Salate, Humus, Feinkostsalate, Marinaden, Desserts',2,2,'2016-08-13 06:18:10','2016-08-13 06:18:10','1'),(17,'Schwefeldioxid u Sulfite','Schwefeldioxid und Sulfite\r\n\r\nz.B. Fruchtzubereitungen, Müsli, Brot, Fleischerzeugnisse, Feinkostsalate,\r\nSuppen, Soßen, Sauerkraut, Fruchtsaft, Chips und andere\r\ngetrocknete Kartoffelerzeugnisse, gesalzener Trockenfisch',2,2,'2016-08-13 06:20:26','2016-08-13 06:24:59','1'),(18,'Lupinen','Lupinen und daraus Gewonnene Erzeugnisse\r\n\r\nz.B. Brot, Gebäck, Pizza, Nudeln, Snacks, fettreduzierte\r\nFleischerzeugnisse, Fleischersatz/ vegetarische Produkte, glutenfreie\r\nProdukte, Desserts, milchfreier Eierersatz, Kaffeeersatz,\r\nFlüssiggewürze',2,2,'2016-08-13 06:22:29','2016-08-13 06:22:29','1'),(19,'Weichtiere','Weichtiere wie Schnecken, Muscheln, Tintenfische und daraus gewonnene Erzeugnisse\r\n\r\nz.B. Würzpasten, Paella, Suppen, Soßen, Marinaden, Feinkostsalate',2,2,'2016-08-13 06:24:15','2016-08-13 06:24:15','1'),(20,'Test','Just for test',2,2,'2016-09-05 11:30:38','2016-09-05 11:30:38','1');
/*!40000 ALTER TABLE `lie_alergic_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_cities`
--

DROP TABLE IF EXISTS `lie_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_cities`
--

LOCK TABLES `lie_cities` WRITE;
/*!40000 ALTER TABLE `lie_cities` DISABLE KEYS */;
INSERT INTO `lie_cities` VALUES (1,1,1,'Dwarka','dwarka','dwarka dwarka',0,0,'2016-07-19 16:22:21','2016-08-11 03:20:11','2'),(2,1,1,'Shalimar Bagh','shalimar-bagh','This is a north delhi area',0,0,'2016-07-19 16:24:28','2016-08-11 03:46:30','2'),(3,2,2,'Banglore','banglore','',0,0,'0000-00-00 00:00:00','2016-08-11 03:20:11','2'),(4,3,3,'New London','new-london','',0,0,'0000-00-00 00:00:00','2016-08-11 03:20:11','2'),(5,4,4,'Baker Street','baker-street','',0,0,'0000-00-00 00:00:00','2016-08-11 03:20:11','2'),(6,6,6,'East County','east-county','',0,0,'0000-00-00 00:00:00','2016-08-11 03:20:11','2'),(7,7,7,'Janakpuri','janakpuri','',0,0,'0000-00-00 00:00:00','2016-08-11 03:20:11','2'),(8,8,8,'Mohan Estate','mohan-estate','',0,0,'0000-00-00 00:00:00','2016-08-11 03:20:11','2'),(9,9,9,'Marine Drive','marine-drive','',0,0,'0000-00-00 00:00:00','2016-08-11 03:20:11','2'),(10,10,10,'Juhu Beach','juhu-beach','',0,0,'0000-00-00 00:00:00','2016-08-11 03:20:11','2'),(11,4,21,'Wien','wein','Stadt Wien',0,0,'2016-07-20 05:30:13','2016-08-12 05:42:52','1'),(12,1,11,'Faridabad','faridabad','Faridabad city',0,0,'2016-07-25 16:03:19','2016-08-11 03:20:11','2'),(13,11,12,'Pataya Beach','pataya-beach','pataya beach',0,0,'2016-07-30 16:44:06','2016-08-11 03:46:30','2'),(14,4,17,'Graz','graz','Stadt Graz',0,0,'2016-08-11 03:25:19','2016-08-11 03:25:19','1'),(15,4,15,'Linz','linz','Stadt Linz',0,0,'2016-08-11 03:26:09','2016-08-11 03:26:09','1'),(16,4,16,'Salzburg','salzburg-1','Stadt Salzburg',0,0,'2016-08-11 03:27:05','2016-08-11 03:27:05','1'),(17,4,19,'Innsbruck','innsbruck','Stadt Innsbruck',0,0,'2016-08-11 03:27:34','2016-08-11 03:27:34','1'),(18,4,20,'Klagenfurt am Woerthersee','klagenfurt-am-w-rthersee','Stadt Klagenfurt am Wörthersee',0,0,'2016-08-11 03:30:08','2016-08-11 03:30:47','2'),(19,4,20,'Villach','villach','Stadt Villach',0,0,'2016-08-11 04:02:42','2016-08-11 04:02:42','1'),(20,4,15,'Wels ','wels','Wels (Stadt)',0,0,'2016-08-11 04:03:20','2016-08-11 04:03:20','1'),(21,4,18,'Dornbirn','dornbirn','Stadtgemeinde Dornbirn',0,0,'2016-08-11 04:05:17','2016-08-11 04:05:17','1'),(22,4,13,'Wiener Neustadt','wiener-neustadt','Wiener Neustadt',0,0,'2016-08-11 04:05:53','2016-08-11 04:05:53','1'),(23,4,15,'Steyr','steyr','Gemeinde Steyr',0,0,'2016-08-11 04:06:47','2016-08-11 04:06:47','1'),(24,4,18,'Feldkirch','feldkirch','Feldkirch ( Stadt )',0,0,'2016-08-11 04:07:58','2016-08-11 04:07:58','1'),(25,4,18,'Bregenz','bregenz','Bregenz ( Stadt )',0,0,'2016-08-11 04:08:53','2016-08-11 04:08:53','1'),(26,4,17,'Leoben','leoben',' Stadt des österreichischen Bundeslandes Steiermark',0,0,'2016-08-11 04:09:50','2016-08-11 04:09:50','1'),(27,4,21,'Gurugram','gurugram','Gurgaon City',0,0,'2016-08-12 14:50:57','2016-08-12 14:50:57','1'),(28,4,20,'Klagenfurt am Wörthersee-','klagenfurt-am-worthersee','Klagenfurt am Wörthersee',0,0,'2016-08-31 01:37:15','2016-08-31 01:37:15','1'),(29,4,13,'Sankt Pölten','st-poelten','Sankt Pölten',0,0,'2016-08-31 02:05:28','2016-08-31 02:05:28','1'),(30,4,15,'Leonding','leonding','Leoding - Oberösterreich',0,0,'2016-08-31 02:09:19','2016-08-31 02:09:19','1'),(31,4,13,'Klosterneuburg','klosterneuburg','Klosterneuburg',0,0,'2016-08-31 02:50:27','2016-08-31 02:50:27','1'),(32,4,13,'Baden','baden','Niederösterreich - Baden',0,0,'2016-08-31 02:51:29','2016-08-31 02:51:29','1'),(33,4,20,'Wolfsberg','wolfsberg','Kärnten - Wolfsberg',0,0,'2016-08-31 02:52:26','2016-08-31 02:52:26','1'),(34,4,13,'Krems','krems','Krems an der Donau',0,0,'2016-08-31 03:01:22','2016-08-31 03:01:22','1'),(35,4,15,'Traun ','traun','Oberösterreich - Traun',0,0,'2016-08-31 03:02:39','2016-08-31 03:02:39','1'),(36,4,13,'Amstetten','amstetten','Niederösterreich Amstetten',0,0,'2016-08-31 03:04:49','2016-08-31 03:04:49','1'),(37,4,17,'Kapfenberg ','kapfenberg','Steiermarkt - Kapfenberg',0,0,'2016-08-31 03:05:46','2016-08-31 03:05:46','1'),(38,4,18,'Lustenau ','lustenau','Vorarlberg - Lustenau',0,0,'2016-08-31 03:06:36','2016-08-31 03:06:36','1'),(39,4,16,'Hallein ','hallein','Salzburg - Hallein',0,0,'2016-08-31 03:07:54','2016-08-31 03:07:54','1'),(40,4,13,'Mödling ','moedling','Niederösterreich - Mödling',0,0,'2016-08-31 03:08:42','2016-08-31 03:08:42','1'),(41,4,19,'Kufstein ','kufstein','Tirol - Kufstein',0,0,'2016-08-31 03:09:19','2016-08-31 03:09:19','1'),(42,4,13,'Traiskirchen ','traiskirchen',' Traiskirchen',0,0,'2016-08-31 03:09:54','2016-08-31 03:09:54','1'),(43,4,13,'Schwechat ','schwechat','Niederösterreich - Schwechat',0,0,'2016-08-31 03:10:36','2016-08-31 03:10:36','1'),(44,4,15,'Braunau','braunau','Oberösterreich - Braunau am Inn',0,0,'2016-08-31 03:19:20','2016-08-31 03:19:20','1'),(45,4,13,'Stockerau ','stockerau','Niederösterreich - Stockerau',0,0,'2016-08-31 03:19:59','2016-08-31 03:19:59','1'),(46,4,16,'Saalfelden am Steinernen Meer ','saalfelden-am-steinernen-meer','Saalfelden am Steinernen Meer',0,0,'2016-08-31 03:22:16','2016-08-31 03:22:16','1'),(47,4,13,'Tulln an der Donau ','tulln','Niederösterreich - Tulln an der Donau',0,0,'2016-08-31 03:23:15','2016-08-31 03:23:15','1'),(48,4,15,'Ansfelden ','ansfelden','Oberösterreich - Ansfelden',0,0,'2016-08-31 03:23:58','2016-08-31 03:23:58','1'),(49,4,18,'Hohenems','hohenems','Vorarlberg - Hohenems',0,0,'2016-08-31 03:25:31','2016-08-31 03:25:31','1'),(50,4,13,'Bruck an der Mur ','bruck-an-der-mur','Niederösterreich - Bruck an der Mur',0,0,'2016-08-31 03:26:48','2016-08-31 03:26:48','1'),(51,4,20,'Spittal an der Drau ','spittal-an-der-drau','Kärnten - Spittal an der Drau',0,0,'2016-08-31 03:28:40','2016-08-31 03:28:40','1'),(52,4,19,'Telfs ','telfs','Tirol - Telfs',0,0,'2016-08-31 03:31:54','2016-08-31 03:31:54','1'),(53,4,13,'Perchtoldsdorf ','perchtoldsdorf','Niederösterreich - Perchtoldsdorf',0,0,'2016-08-31 03:32:28','2016-08-31 03:32:28','1'),(54,4,13,'Ternitz ','ternitz','Niederösterreich - Ternitz',0,0,'2016-08-31 03:33:13','2016-08-31 03:33:13','1'),(55,4,14,'Eisenstadt ','eisenstadt','Burgenland - Eisenstadt',0,0,'2016-08-31 03:34:04','2016-08-31 03:34:04','1'),(56,4,20,'Feldkirchen in Kärnten ','feldkirchen-in-k-rnten','Kärnten - Feldkirchen in Kärnten',0,0,'2016-08-31 03:35:04','2016-08-31 03:35:04','1'),(57,4,18,'Bludenz ','bludenz','Vorarlberg - Bludenz',0,0,'2016-08-31 03:35:44','2016-08-31 03:35:44','1'),(58,4,15,'Bad Ischl ','bad-ischl','Oberösterreich - Bad Ischl',0,0,'2016-08-31 03:36:25','2016-08-31 03:36:25','1'),(59,4,19,'Hall in Tirol ','hall-in-tirol','Tirol - Hall in Tirol',0,0,'2016-08-31 03:37:06','2016-08-31 03:37:06','1'),(60,4,19,'Schwaz ','schwaz','Tirol - Schwaz',0,0,'2016-08-31 03:37:48','2016-08-31 03:37:48','1'),(61,4,17,'Feldbach ','feldbach','Steiermarkt - Feldbach',0,0,'2016-08-31 03:38:24','2016-08-31 03:38:24','1'),(62,4,19,'Wörgl ','woergl','Tirol - Wörgl',0,0,'2016-08-31 03:39:11','2016-08-31 03:39:11','1'),(63,4,16,'Wals-Siezenheim ','wals-siezenheim','Salzburg - Wals-Siezenheim',0,0,'2016-08-31 03:41:19','2016-08-31 03:41:19','1'),(64,4,18,'Hard ','hard','Vorarlberg Hard',0,0,'2016-08-31 03:42:11','2016-08-31 03:42:11','1'),(65,4,15,'Gmunden ','gmunden','Oberösterreich - Gmunden',0,0,'2016-08-31 03:42:44','2016-08-31 03:42:44','1'),(66,4,15,'Marchtrenk ','marchtrenk','Oberösterreich - Marchtrenk',0,0,'2016-08-31 03:43:34','2016-08-31 03:43:34','1'),(67,4,13,'Korneuburg ','korneuburg','Niederösterreich - Korneuburg',0,0,'2016-08-31 03:44:38','2016-08-31 03:44:38','1'),(68,4,17,'Gratwein-Strassengel','gratwein-strassengel','Steiermarkt - gratwein-strassengel',0,0,'2016-08-31 03:49:04','2016-08-31 03:49:04','1'),(69,4,17,'Knittelfeld ','knittelfeld','Steiermarkt - Knittelfeld',0,0,'2016-08-31 03:49:45','2016-08-31 03:49:45','1'),(70,4,13,'Neunkirchen ','neunkirchen','Niederösterreich - Neunkirchen',0,0,'2016-08-31 03:52:46','2016-08-31 03:52:46','1'),(71,4,20,'Sankt  Veit ','sankt-veit','Kärnten - St. Veit an der Glan',0,0,'2016-08-31 03:54:56','2016-08-31 03:54:56','1'),(72,4,15,'Vöcklabruck ','v-cklabruck','Oberösterreich - Vöcklabruck',0,0,'2016-08-31 03:55:42','2016-08-31 03:55:42','1'),(73,4,19,'Lienz ','lienz','Tirol - Lienz',0,0,'2016-08-31 03:56:10','2016-08-31 03:56:10','1'),(74,4,17,'Leibnitz ','leibnitz','Steiermarkt - Leibnitz',0,0,'2016-08-31 03:56:50','2016-08-31 03:56:50','1'),(75,4,15,'Enns ','enns','Oberösterreich - Enns',0,0,'2016-08-31 03:57:31','2016-08-31 03:57:31','1'),(76,4,13,'Hollabrunn ','hollabrunn','Niederösterreich - Hollabrunn',0,0,'2016-08-31 03:58:27','2016-08-31 03:58:27','1'),(77,4,18,'Rankweil ','rankweil','Vorarlberg - Rankweil',0,0,'2016-08-31 03:59:10','2016-08-31 03:59:10','1'),(78,4,13,'Bad Vöslau','badvoeslau','Niederösterreich - Bad Vöslau',0,0,'2016-08-31 04:01:23','2016-08-31 04:01:23','1'),(79,4,13,'Brunn am Gebirge ','brunn-am-gebirge','Niederösterreich - Brunn am Gebirge',0,0,'2016-08-31 04:04:29','2016-08-31 04:04:29','1'),(80,4,15,'Ried im Innkreis ','ried-im-innkreis','Oberösterreich - Ried im Innkreis',0,0,'2016-08-31 04:07:09','2016-08-31 04:07:09','1'),(81,4,17,'Deutschlandsberg ','deutschlandsberg','Steiermarkt - Deutschlandsberg',0,0,'2016-08-31 04:07:45','2016-08-31 04:07:45','1'),(82,4,17,'Weiz ','weiz','Steiermarkt - Weiz',0,0,'2016-08-31 04:09:29','2016-08-31 04:09:29','1'),(83,4,13,'Waidhofen an der Ybbs ','waidhofen-an-der-ybbs','Niederösterreich - Waidhofen an der Ybbs',0,0,'2016-08-31 04:10:38','2016-08-31 04:10:38','1'),(84,4,13,'Mistelbach','mistelbach','Niederösterreich - Mistelbach',0,0,'2016-08-31 04:11:36','2016-08-31 04:11:36','1'),(85,4,18,'Götzis ','g-tzis','Vorarlberg - Götzis',0,0,'2016-08-31 04:12:58','2016-08-31 04:12:58','1'),(86,4,17,'Trofaiach ','trofaiach','Steiermarkt - Trofaiach',0,0,'2016-08-31 04:13:27','2016-08-31 04:13:27','1'),(87,4,13,'Gänserndorf ','g-nserndorf','Niederösterreich - Gänserndorf',0,0,'2016-08-31 04:14:03','2016-08-31 04:14:03','1'),(88,4,13,'Zwettl ','zwettl','Niederösterreich - ',0,0,'2016-08-31 04:14:53','2016-08-31 04:14:53','1'),(89,4,20,'Völkermarkt ','voelkermarkt','Kärnten - Völkermarkt',0,0,'2016-08-31 04:15:57','2016-08-31 04:15:57','1'),(90,4,16,'Sankt Johann im Pongau ','sankt-johann-im-pongau','Salzburg - Sankt Johann im Pongau',0,0,'2016-08-31 04:17:14','2016-08-31 04:17:14','1'),(91,4,13,'Gerasdorf ','gerasdorf','Niederösterreich - Gerasdorf',0,0,'2016-08-31 04:17:48','2016-08-31 04:17:48','1'),(92,4,17,'Seiersberg-Pirka ','seiersberg-pirka','Steiermarkt - Seiersberg-Pirka',0,0,'2016-08-31 04:21:07','2016-08-31 04:21:07','1'),(93,4,13,'Ebreichsdorf ','ebreichsdorf','Niederösterreich - Ebreichsdorf',0,0,'2016-08-31 04:21:39','2016-08-31 04:21:39','1'),(94,4,16,'Seekirchen am Wallersee ','seekirchen-am-wallersee','Salzburg - Seekirchen am Wallersee',0,0,'2016-08-31 04:22:38','2016-08-31 04:22:38','1'),(95,4,16,'Bischofshofen ','bischofshofen','Salzburg - Bischofshofen',0,0,'2016-08-31 04:23:10','2016-08-31 04:23:10','1'),(96,4,13,'Groß-Enzersdorf ','gross-enzersdorf','Niederösterreich - Groß-Enzersdorf',0,0,'2016-08-31 04:24:02','2016-08-31 04:24:02','1'),(97,4,17,'Gleisdorf ','gleisdorf','Steiermarkt - Gleisdorf',0,0,'2016-08-31 04:24:40','2016-08-31 04:24:40','1'),(98,4,17,'Judenburg ','judenburg','Steiermarkt - Judenburg',0,0,'2016-08-31 04:25:16','2016-08-31 04:25:16','1'),(99,4,19,'Imst ','imst','Tirol - Imst',0,0,'2016-08-31 04:25:42','2016-08-31 04:25:42','1'),(100,4,17,'Köflach ','koeflach','Steiermarkt - Köflach',0,0,'2016-08-31 04:26:25','2016-08-31 04:26:25','1'),(101,4,20,'Sankt Andrä ','sankt-andrae','Kärnten - Sankt Andrä',0,0,'2016-08-31 04:27:39','2016-08-31 04:27:39','1'),(102,4,18,'Lauterach ','lauterach','Vorarlberg - Lauterach',0,0,'2016-08-31 04:28:23','2016-08-31 04:28:23','1');
/*!40000 ALTER TABLE `lie_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_color_settings`
--

DROP TABLE IF EXISTS `lie_color_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_color_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(50) NOT NULL,
  `color_code` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_color_settings`
--

LOCK TABLES `lie_color_settings` WRITE;
/*!40000 ALTER TABLE `lie_color_settings` DISABLE KEYS */;
INSERT INTO `lie_color_settings` VALUES (1,'Yellow','#220044',2,2,'2016-07-19 17:50:22','2016-08-05 05:21:41','1'),(2,'Red','#d41e33',2,2,'2016-07-19 17:51:01','2016-07-19 17:51:01','1'),(3,'black','#000000',2,2,'2016-07-19 18:08:11','2016-08-01 12:21:31','1'),(4,'asdasdas','#000000',2,2,'2016-07-19 18:08:38','2016-07-19 18:08:57','2'),(5,'purple','#205162',2,2,'2016-07-22 15:00:58','2016-07-22 15:00:58','1'),(6,'grau','#d3d0d4',2,2,'2016-08-11 04:16:56','2016-09-05 11:42:01','2');
/*!40000 ALTER TABLE `lie_color_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_countries`
--

DROP TABLE IF EXISTS `lie_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_countries`
--

LOCK TABLES `lie_countries` WRITE;
/*!40000 ALTER TABLE `lie_countries` DISABLE KEYS */;
INSERT INTO `lie_countries` VALUES (1,'India','india','India India',0,0,'2016-07-19 16:06:53','2016-08-11 03:06:36','2'),(2,'UK','uk','uk uk uk uk uk',0,0,'0000-00-00 00:00:00','2016-08-11 03:07:01','2'),(3,'Australi','australia','',0,0,'0000-00-00 00:00:00','2016-08-11 03:06:36','2'),(4,'Austria','austria','rejthert emuivrhi3rcu4i3h4 32x4gy324g23egu2e3u4ec2i3ec4egbrhcu3r33uh4g234 bv43g4yu34 34bv3uy4 324h3bg43 43bg423u c32br y324 34hu234 23bhv4h32 2b 43h4v3 hurghr ser uhte tetjherebiuererebdeuhrnbuierb I nam tesing here.....',0,0,'0000-00-00 00:00:00','2016-09-07 17:36:20','1'),(5,'Russia','russia','',0,0,'0000-00-00 00:00:00','2016-08-11 03:06:36','2'),(6,'China','china','',0,0,'0000-00-00 00:00:00','2016-08-11 03:06:36','2'),(7,'Japan','japan','',0,0,'0000-00-00 00:00:00','2016-08-11 03:06:36','2'),(8,'USA','usa','',0,0,'0000-00-00 00:00:00','2016-08-11 03:07:01','2'),(9,'Brazil','brazil','',0,0,'0000-00-00 00:00:00','2016-08-11 03:06:36','2'),(10,'Africa','africa','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events\r\nDescription is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events',0,0,'0000-00-00 00:00:00','2016-08-11 03:06:36','2'),(11,'Thailand','thailand','thailand thailand',0,0,'2016-07-30 16:42:17','2016-08-11 03:07:01','2'),(12,'Steiermarkt','steiermarkt','Steiermarkt',0,0,'2016-08-05 07:21:08','2016-08-11 03:07:01','2'),(13,'Salzburg','salzburg','Land Salzburg      ',0,0,'2016-08-05 07:21:48','2016-08-11 03:06:36','2'),(14,'Kaernten','kaernten','Bundesland kärnten',0,0,'2016-08-05 07:22:37','2016-08-11 03:06:36','2'),(15,'Tirol','tirol','Bundesland Tirol',0,0,'2016-08-05 07:23:09','2016-08-11 03:07:01','2'),(16,'Vorarlberg','vorarlberg','Bundesland Vorarlberg',0,0,'2016-08-05 07:23:56','2016-08-11 03:07:01','2'),(17,'hjj','hjj','fdhfjghkhhhhhhhhhhhhhhhhhhhhhhhhhh',0,0,'2016-09-07 17:39:15','2016-09-07 17:39:22','2');
/*!40000 ALTER TABLE `lie_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_currencies`
--

DROP TABLE IF EXISTS `lie_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `currency_code` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_currencies`
--

LOCK TABLES `lie_currencies` WRITE;
/*!40000 ALTER TABLE `lie_currencies` DISABLE KEYS */;
INSERT INTO `lie_currencies` VALUES (12,'Euro','&euro;',2,2,'2016-07-19 18:16:34','2016-08-11 10:08:57','1'),(13,'rupees','INR',2,2,'2016-07-19 18:17:06','2016-09-08 17:13:45','0'),(14,'fgdg','dfgdfgdgfg',2,2,'2016-07-19 18:17:40','2016-07-19 18:17:51','2'),(15,'sdsd','dsdsdsds',2,2,'2016-07-20 10:30:08','2016-07-20 10:30:14','2');
/*!40000 ALTER TABLE `lie_currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_discount_types`
--

DROP TABLE IF EXISTS `lie_discount_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_discount_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `type` enum('0','1') NOT NULL COMMENT '0 for ''no popup'',1 for ''popup''',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_discount_types`
--

LOCK TABLES `lie_discount_types` WRITE;
/*!40000 ALTER TABLE `lie_discount_types` DISABLE KEYS */;
INSERT INTO `lie_discount_types` VALUES (1,'Fixed','Der Preis wird auf einen bestimmten Wert gesetzt (z.B. 5,50 für jede Pizza)','0',9,2,'2016-05-02 00:00:00','2016-12-03 11:15:05','1'),(2,'Percentage','Der Preis wird um einen bestimmten Prozentsatz vergünstigt (z.B. -10% auf alle vegetarischen Gerichte)','0',9,2,'2016-05-24 00:00:00','2016-12-03 11:17:05','1'),(3,'One free on purchase','Kostenloses Gericht bei Kauf','1',9,2,'2016-05-31 00:00:00','2016-12-03 12:07:39','1');
/*!40000 ALTER TABLE `lie_discount_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_email_templates`
--

DROP TABLE IF EXISTS `lie_email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `type` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT '1 for superadmin,2 for admin,3 for users',
  `text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_email_templates`
--

LOCK TABLES `lie_email_templates` WRITE;
/*!40000 ALTER TABLE `lie_email_templates` DISABLE KEYS */;
INSERT INTO `lie_email_templates` VALUES (1,'User Order','3','<!DOCTYPE html>\r\n<html>\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/>\r\n    <meta http-equiv=\"pragma\" content=\"no-cache\"/>\r\n    <meta name=\"apple-mobile-web-app-capable\" content=\"yes\"/>\r\n    <meta name=\"apple-mobile-web-app-status-bar-style\" content=\"default\"/>\r\n    <meta name=\"format-detection\" content=\"telephone=no\"/>\r\n    <meta name=\"viewport\" content=\"user-scalable=0, width=device-width, initial-scale=1.0, maximum-scale=1.0\"/>\r\n    <meta name=\"msapplication-TileImage\" content=\"prem/16.1320.14.2056223/resources/images/0/owa_browserpinnedtile.png\"/>\r\n    <meta name=\"msapplication-TileColor\" content=\"#0072c6\"/>\r\n    <meta name=\"msapplication-tap-highlight\" content=\"no\" />\r\n    <meta name=\"google\" value=\"notranslate\">\r\n    <meta name=\"apple-mobile-web-app-title\" content=\"OWA\"/>\r\n    <meta name=\"referrer\" content=\"origin\" />\r\n    \r\n    <link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\">\r\n    \r\n\r\n    <title>Outlook</title>\r\n\r\n    <script>\r\n        var startLoadTime = new Date();\r\n        var appCachedPage = true;\r\n        window.dateZero = new Date(0);\r\n        var scriptStart = ((new Date()) - window.dateZero);\r\n        window.clientId = \'2415AFF4D4614556943D37C6C55C7079\';        \r\n        window.corrId = window.clientId + \"_\" + (new Date()).getTime();\r\n        window.traceTid= \'84df9e7f-e9f6-40af-b435-aaaaaaaaaaaa\';\r\n        window.traceMguid = \'98d22368-f568-4b0a-9b71-d169dc4cf8a6\';\r\n        window.owaUserNetId = \'00034001829E7265\';\r\n        window.owaMbxGuid = \'00034001-829e-7265-0000-000000000000\';\r\n        window.bootTraceUrl = \'https://xsi.outlook.com/\';\r\n        var onunloadbeforeboot = null;\r\n        window.owaSDState = {};\r\n        window.owaBootStatus = {};\r\n        \r\n        \r\n        var isPopOut = window.location.href.indexOf(\'ispopout=1\') > 0;\r\n\r\n        if (!isPopOut) {\r\n            try\r\n            {\r\n                \r\n                window.owaSDConsumable=false;\r\n                window.owaSDConsumed = false;\r\n                sendOwaSDRequest(false);\r\n            }\r\n            catch (ex) {\r\n                window.owaLastErrorReported = ex;\r\n                throw ex;\r\n            }\r\n        }\r\n        else {\r\n            onunloadbeforeboot = function() {\r\n                callPopOutBootErrorCallback(\"PopOutClosed\");\r\n            };\r\n            if (window.attachEvent) {\r\n                window.attachEvent(\"onunload\", onunloadbeforeboot);\r\n            }\r\n            else {\r\n                window.addEventListener(\"unload\", onunloadbeforeboot, false);\r\n            }\r\n        }\r\n\r\n        function createOwaSDXMLHttpRequest() {\r\n            var request = null;\r\n            try\r\n            {\r\n                request = new window.XMLHttpRequest();\r\n            }\r\n            catch (e) {\r\n                \r\n                if (window.ActiveXObject) {\r\n                    request = new window.ActiveXObject(\"Microsoft.XMLHTTP\");\r\n                }\r\n            }\r\n\r\n            return request;\r\n        }\r\n        \r\n        function sendOwaSDRequest(retryAttempt) {\r\n            \r\n            window.owaSD = createOwaSDXMLHttpRequest();\r\n            window.owaSD.retryAttempt = retryAttempt;\r\n            window.owaSD.open(\"POST\",\"sessiondata.ashx\" + getSdqp(retryAttempt));\r\n            window.owaSD.onreadystatechange = sdResponseHandler;\r\n            window.owaSD.setRequestHeader(\"X-OWA-CorrelationId\", window.corrId);\r\n            window.owaSD.clientRequestId = window.corrId;\r\n            window.owaSD.setRequestHeader(\"client-request-id\", window.owaSD.clientRequestId);\r\n            \r\n            try {\r\n                if (navigator.userAgent.indexOf(\"OWASMIME/\" > 0)) {\r\n                    window.owaSD.setRequestHeader(\"X-OWA-SmimeInstalled\", \"1\");\r\n                }\r\n                else if (window.ActiveXObject) {\r\n                    var smimePlugin = new ActiveXObject(\"Microsoft.Exchange.Clients.SmimeAX\");\r\n                    window.owaSD.setRequestHeader(\"X-OWA-SmimeInstalled\", \"1\");\r\n                    smimePlugin.Dispose();\r\n                }\r\n            }\r\n            catch (e) {\r\n            }\r\n\r\n            window.owaSD.send();\r\n        }\r\n\r\n        function getSdqp(retryAttempt) {\r\n            var sdqp = \"?appcacheclient=1\";\r\n            \r\n             return sdqp;\r\n        }\r\n\r\n        function sdResponseHandler() {\r\n            try {\r\n            \r\n                if (!window.owaSD) {\r\n                    return;\r\n                }\r\n\r\n                if (window.owaSD.readyState != 4) {\r\n                    return;\r\n                }\r\n                \r\n                if (!tryAuthOwaSD()) {\r\n                    return;\r\n                }\r\n\r\n                window.owaSDState.data = window.owaSD.responseText;\r\n                \r\n                completeOwaSD();\r\n            \r\n            }\r\n            catch (ex) {\r\n                window.owaLastErrorReported = ex;\r\n                throw ex;\r\n            }\r\n        }\r\n\r\n        function tryAuthOwaSD() {\r\n            if ((window.owaSD.status == 440 || window.owaSD.status == 401) && !isPalEnabled()) {\r\n                    postBootTrace(\"AuthRedirect\");\r\n\r\n                    \r\n                    trackRedirectToAuth();\r\n                    redirect(\'authRedirect\', \'true\');\r\n                    return false;\r\n                    }\r\n\r\n            return true;\r\n        }\r\n\r\n        function completeOwaSD() {\r\n            window.owaSDdidHandlerExecute = true;\r\n            window.owaSDReceivedTime = (new Date()).toString();\r\n            window.owaSDReceivedTimeStamp = ((new Date()) - window.dateZero);\r\n\r\n            if (window.owaSDConsumable && !window.owaSDConsumed) {\r\n                owastart();\r\n            }\r\n        }\r\n\r\n        var LT_ANY=\"Any\";var LT_MOUSE=\"Mouse\";var LT_TNARROW=\"TNarrow\";var LT_TWIDE=\"TWide\";var layout,bootTraceTimerId,cdnEndPointName,lcver,readingPaneOn,parseEndTimes={},owaRedirecting=!1;function getQueryStr(){return window.location.search?window.location.search:\"\"}function isPalEnabled(){var n=getQueryStr();return document.cookie.indexOf(\"PALEnabled\")!=-1||n.indexOf(\"palenabled=1\")!=-1}function validateLocalStorage(){if(isLocalStorageEnabled==undefined)try{if(window.localStorage!=null){window.localStorage.dummy=\"dummy\";window.localStorage.removeItem(\"dummy\");isLocalStorageEnabled=!0}else isLocalStorageEnabled=!1}catch(n){isLocalStorageEnabled=!1}return isLocalStorageEnabled}var isLocalStorageEnabled=validateLocalStorage();function getLocalStorageValue(n){return validateLocalStorage()?window.localStorage[n]:undefined}function setLocalStorageValue(n,t){if(!validateLocalStorage())return undefined;var i=window.localStorage[n];window.localStorage[n]=t;return i}function deleteLocalStorageValue(n){if(!validateLocalStorage())return undefined;var t=window.localStorage[n];t&&delete window.localStorage[n];return t}function getCookie(n){var t=new RegExp(\"(?:^|; )\"+encodeURIComponent(n)+\"=([^;]*)\").exec(document.cookie);return t?decodeURIComponent(t[1]):null}function eraseCookie(n,t){document.cookie=n+\"=; expires=Thu, 01 Jan 1970 00:00:01 GMT; domain=\"+t+\"; path=/\"}function loadSlabSources(n,t,i){n!==\"\"&&n[n.length-1]!=\"/\"&&(n+=\"/\");for(var r=0;r<t.length;r++){var f=t[r];var u=n+f.name;document.write(\"<script src=\'\"+u+\"\'></\"+\"script>\")}}function userEnabledOffline(){if(isPalEnabled())return!0;else if(isLocalStorageEnabled){var n=window.localStorage.userEnabledOffline||\"\";return n!==\"false\"&&n!=null&&n!=\"\"}}function updateStatusText(n,t){if(!isPopOut){var i=document.getElementById(\"statusText\");if(i){i.className=n;window.setTimeout(function(){i.className==n&&(i.className=n+\"Delay\")},t*1e3)}}}function appendQueryWhenBootError(n){var t=getQueryStr();(t.indexOf(\"aC=1\")>=0||t.indexOf(\"bFS=1\")>=0)&&n.indexOf(\"?\")==-1&&(n=n+\"?bO=1\");return n}function includeScripts(n,t,i,r,u,f){for(var s=0;s<t.length;s++){var l=t[s];var h=l.fileName;var e=n+h;e=appendQueryWhenBootError(e);var c=document.getElementsByTagName(\"head\")[0];if(!c.querySelector(\"script[src=\'\"+e+\"\']\"))if(r){var o=document.createElement(\"script\");o.type=\"text/javascript\";o.src=e;o.onload=u;o.onerror=f;c.appendChild(o)}else{document.write(\"<script src=\'\"+e+\"\' onerror=\'onScriptLoadError2(this)\'\");document.write(\"></\"+\"script>\");document.write(\"<script>parseEndTimes[\'\"+getFileName(h)+\"\'] = ((new Date()) - window.dateZero);</\"+\"script>\")}}}function addCssLink(n){if(document.createStyleSheet)document.createStyleSheet(n);else{var i=document.getElementsByTagName(\"head\")[0];var t=document.createElement(\"link\");t.setAttribute(\"href\",n);t.setAttribute(\"rel\",\"stylesheet\");t.setAttribute(\"type\",\"text/css\");i.appendChild(t)}}function includeStyles(n,t,i){var s=window.matchMedia&&window.matchMedia(\"screen and (-webkit-min-device-pixel-ratio: 2)\")&&window.matchMedia(\"screen and (-webkit-min-device-pixel-ratio: 2)\").matches;n!==\"\"&&n[n.length-1]!=\"/\"&&(n+=\"/\");for(var o=i.length,f=0;f<o;f++){var r=i[f];var h=r.fileName||r.name;var e=n;(r.type==\"Sprite\"||r.type==\"HighResolution\")&&(e=t);var u=e+h;u=appendQueryWhenBootError(u);r.highResolution||r.type==\"HighResolution\"?s&&addCssLink(u):addCssLink(u)}}function includeScriptsAndStyles(n,t,i,r,u,f){if(!f)for(var e in n)if(n[e]==n.boot||u){var o=n[e].PackagedSources;o||(o=n[e].Sources);loadSlabSources(t,o,layout);var s=n[e].Styles;includeStyles(i,r,s,layout)}}var appcacheLoaded=!1;function suppressErrorRedirect(){return sver==\"bld_ver\"||window.location.href.indexOf(\"noErrR=1\")!=-1}(function(){isPalEnabled()||(window.onerror=function(n,t,i){if(!appcacheLoaded)if(window.owaLastErrorReported){window.owaLastErrorReported.message&&(n=window.owaLastErrorReported.message);window.owaLastErrorReported.stack&&(t=window.owaLastErrorReported.stack);window.owaLastErrorReported=null;handleBootError2(\"ClientError\",formatErrorMsg(n,t,i))}else{if(!window.owaErrorState){window.owaErrorState={};window.owaErrorState.owaUncaughtError=[]}window.owaErrorState.owaUncaughtError.push(formatErrorMsg(n,t,i))}})})();function formatErrorMsg(n,t,i){return\"exMsg=\"+n+\";file=\"+t+\":\"+i}function handleBootError2(n,t,i,r){try{if(owaRedirecting)return;var u=getQueryStr();if(appCachedPage&&(n==\"ScriptLoadError\"||n==\"ClientError\")&&!isPalEnabled()){postBootTrace(n,t);var a=userEnabledOffline()?\"10\":\"2\";trackRebootReason(a);redirect(\"aC\",\"1\")}else if(!appCachedPage||userEnabledOffline()||isPalEnabled())if(u.indexOf(\"bO=1\")!=-1||u.indexOf(\"aC=1\")!=-1||u.indexOf(\"bFS=1\")!=-1||n==\"ScriptLoadError\"||n==\"ClientError\"||n==\"USRLoadError\"||userEnabledOffline()||isPalEnabled())if(u.indexOf(\"bO=1\")!=-1||u.indexOf(\"bFS=1\")!=-1||n!=\"ScriptLoadError\"&&n!=\"ClientError\"&&n!=\"USRLoadError\"||userEnabledOffline()||isPalEnabled()){postBootTrace(n,t);isPopOut&&callPopOutErrorCallback(\"BootError_\"+n);if(!isPalEnabled()&&!userEnabledOffline()){if(t){t=encodeURIComponent(t);t.length>1024&&(t=t.substring(0,1024))}var f;var c;var s;var h;var e;var o;try{f=window.owaSD.getResponseHeader(\"X-OWAErrorMessageID\");c=window.owaSD.getResponseHeader(\"request-id\");s=window.owaSD.getResponseHeader(\"X-InnerException\");e=window.owaSD.getResponseHeader(\"X-OWA-CorrelationId\");o=window.owaSD.getResponseHeader(\"X-FEServer\");h=window.owaSD.clientRequestId}catch(v){}f||(f=n);suppressErrorRedirect()||redirectToUrl(\"/owa/auth/errorfe.aspx?owaError=\"+n+\";\"+t+\"&owaVer=\"+i+\"&be=\"+r+\"&msg=\"+f+\"&reqid=\"+c+\"&inex=\"+s+\"&creqid=\"+h+\"&fe=\"+o+\"&cid=\"+e)}}else{postBootTrace(n,t);trackRebootReason(\"9\");redirect(\"bFS\",\"1\")}else{postBootTrace(n,t);trackRebootReason(\"8\");redirect(\"bO\",\"1\")}else{postBootTrace(n,t);trackRebootReason(\"1\");redirect(\"bO\",\"1\")}}catch(v){if(!suppressErrorRedirect()){var l=v.message+\";\"+v.stack;redirectToUrl(\"/owa/auth/errorfe.aspx?owaError=Unknown;handle boot error failed;\"+l+\"&owaVer=\"+i+\"&be=\"+r)}}}function onScriptLoadError2(n){var t=\"Failed to load script: \";n&&(t+=n.src);handleBootError2(\"ScriptLoadError\",t,null)}function htmlDec(n){var t=document.createElement(\"div\");t.innerHTML=n;return t.innerText||t.textContent}function loadScripts(n,t,i,r,u){for(var f=0;f<n.length;++f)n[f].fileName=n[f].fileName.replace(\"##LANG##\",userLanguageVar).replace(\"##CULTURE##\",userCultureVar).toLowerCase();includeScripts(\"\",n,t,i,r,u)}function loadStyles(n){includeStyles(\"\",\"\",n,layout)}function redirect(n,t,i,r,u,f,e,o,s,h){if(!owaRedirecting){var c=addParamsToUrl(window.location.href,n,t);i!==undefined&&(c=addParamsToUrl(c,i,r));u!==undefined&&(c=addParamsToUrl(c,u,f));e!==undefined&&(c=addParamsToUrl(c,e,o));s!==undefined&&(c=addParamsToUrl(c,s,h));redirectToUrl(c)}}function redirectToUrl(n){if(n!=window.location.href&&!owaRedirecting){setTimeout(function(){window.location.href=n},50);owaRedirecting=!0;detachUnloadEvent()}}function addParamsToUrl(n,t,i){var u=t+\"=\"+i;var r=n.split(\"#\");var f=r.length==1?\"\":n.substr(n.indexOf(\"#\"));if(r[0].indexOf(u)>=0)return n;n=r[0].indexOf(\"?\")>=0?r[0]+\"&\"+u+f:r[0]+\"?\"+u+f;return n}function isMajorVersionChanged(n,t){if(n==null||t==null)return!0;var r=n.split(\".\");var u=t.split(\".\");if(r.length<3||u.length<3)return n!=t;for(var i=0;i<3;++i)if(r[i]!=u[i])return!0;return!1}var measure;var measureTitle;var measureDict={};var indentStr=\"\";var consoleLogger=null;function startMeasure(n){if(consoleLogger){var t=new Date;measureTitle=n;consoleLogger.log(\"Start Scenario --- \"+measureTitle+\"@\"+(t-measure));measure=t}}function endMeasure(){if(consoleLogger&&measure){var n=new Date;consoleLogger.log(\"End Scenario --- \"+measureTitle+\"@\"+(n-measure))}}function timeStamp(n){if(consoleLogger&&measure){var t=new Date;consoleLogger.log(indentStr+\" Stamp-\"+n+\"@\"+(t-measure))}}function time(n){if(consoleLogger){var t=new Date;measureDict[n]=t;measure?consoleLogger.log(indentStr+\"??Start-\"+n+\"@\"+(t-measure)):consoleLogger.log(indentStr+\"??Start-\"+n);indentStr=indentStr+\"? \"}}function timeEnd(n){if(consoleLogger){var i=measureDict[n];var r=new Date;delete measureDict[n];indentStr=indentStr.substr(2);var t=\"\";i&&(t=r-i);measure?consoleLogger.log(indentStr+\"??End  -\"+n+\"@\"+(r-measure)+\" \"+t+\"ms\"):consoleLogger.log(indentStr+\"??End  -\"+n+\" \"+t+\"ms\")}}function trackRedirectToAuth(){setLocalStorageValue(\"authRedirect\",\"1\")}function trackRedirectToAuthDone(){var n=deleteLocalStorageValue(\"authRedirect\");return n==\"1\"}function trackRebootReason(n){setLocalStorageValue(\"rebootReason\",n)}function getRebootReasonAndReset(){var n=deleteLocalStorageValue(\"rebootReason\");return n?n:\"0\"}function getClientId(){return getLocalStorageValue(\"cid\")}function setClientId(n){setLocalStorageValue(\"cid\",n)}function updateLastClientVersion(n){return setLocalStorageValue(\"LastClientVersion\",n)}function isAppCacheSupported(){try{return window.applicationCache!=null}catch(n){return!1}}function getMissingBootFiles(){var t=\"\";try{if(!!window.scriptsLoaded)for(var n in window.scriptsLoaded)t+=!window.scriptsLoaded[n]||window.scriptsLoaded[n]==0?n+\";\":\"\";return t?t:\"nf\"}catch(i){return null}}function postBootTrace(n,t){try{if(owaRedirecting)return;if(isPopOut)return;var v=window.bootTraceUrl,it=appCachedPage,rt=trackRedirectToAuthDone();var a=null,l=null,w=null,y=null,p=null,c=null,u=null;if(window.owaSD&&window.owaSD.readyState==4){a=window.owaSD.getResponseHeader(\"X-FEServer\");l=window.owaSD.getResponseHeader(\"X-BEServer\");w=window.owaSD.getResponseHeader(\"X-CalculatedBETarget\");p=window.owaSD.getResponseHeader(\"X-OWA-Version\");y=window.owaSD.status;u=window.owaSD.getResponseHeader(\"X-MSEdge-Ref\");c=u?\"1\":\"0\"}perfData=getPerformanceNumbers(it);var nt=getRebootReasonAndReset();var i=\"?cId=\"+window.clientId+\"&msg=\"+n+\"&tg=\"+window.traceTid+\"&MDB=\"+window.traceMguid+\"&nId=\"+window.owaUserNetId+\"&MBX=\"+window.owaMbxGuid+\"&sdCoId=\"+window.corrId+\"&sds=\"+y+\"&fe=\"+a+\"&be=\"+l+\"&cbe=\"+w+\"&cver=\"+sver+\"&sdver=\"+p+\"&rpo=\"+(readingPaneOn?\"1\":\"0\")+\"&off=\"+(userEnabledOffline()?\"1\":\"0\")+\"&pal=\"+(isPalEnabled()?\"1\":\"0\")+\"&rfe=\"+nt+\"&te=\"+c+\"&\"+perfData[0];if(window.performance&&window.performance.timing&&window.performance.timing.domLoading&&window.scriptStart){var tt=window.scriptStart-window.performance.timing.domLoading;i+=\"&tcd=\"+tt}rt&&(i+=\"&backFromAuth=true\");if(isLocalStorageEnabled){var ut=window.localStorage.BootVer;var ot=window.localStorage.InstalledCacheVer;var st=window.localStorage.InstallAttemptCacheVer;var ft=window.localStorage.UICulture;var et=window.localStorage.UITheme;var g=window.localStorage.DownloadedCacheCount;var b=window.localStorage.LastHostName;var k=window.location.hostname;i+=\"&lbv=\"+ut+\"&icv=\"+ot+\"&iacr=\"+st+\"&lcver=\"+lcver+\"&accu=\"+ft+\"&acth=\"+et+\"&acdc=\"+g+\"&lhn=\"+b+\"&chn=\"+k}i+=\"&acs=\"+(isAppCacheSupported()?\"1\":0);i+=\"&mf=\"+getMissingBootFiles();if(window.owaErrorState&&window.owaErrorState.owaUncaughtError&&window.owaErrorState.owaUncaughtError.length>0){for(var h=\"\",s=0;s<window.owaErrorState.owaUncaughtError.length;s++)h+=window.owaErrorState.owaUncaughtError[s]+\";\";i+=\"&ue=\"+encodeURIComponent(h)}var d=setLocalStorageValue(\"featureChanges\",\"null\");i+=\"&fc=\"+encodeURIComponent(d);var r=window.location.href;r&&(r=encodeURIComponent(r));var e=new XMLHttpRequest;e.open(\"POST\",\"plt1.ashx\"+i,!0);var f=(r?\"&refUrl=\"+r:\"\")+(u?\"&edgeRef=\"+u:\"\")+(t?\"&Err=\"+encodeURIComponent(t):\"\");e.setRequestHeader(\"X-OWA-PLT-Info\",f);perfData[1].length>0&&(f=\"&\"+perfData[1]+f);perfData[2].length>0&&(f=perfData[2]+f);e.send(f);if(v){i=i+(r?\"&refUrl=\"+r:\"\")+(u?\"&edgeRef=\"+u:\"\")+(t?\"&Err=\"+encodeURIComponent(t):\"\");i.length>4096&&(i=i.substring(0,4096));var o=document.getElementById(\"traceFrame\");if(o){o.src=v+i;bootTraceTimerId!=null&&window.clearTimeout(bootTraceTimerId);bootTraceTimerId=window.setTimeout(function(){bootTraceTimerId=null;o.src=\"about:blank\"},3e4)}}}catch(ht){}}function getPerformanceNumbers(n){var r=window.performance;typeof r!=\"undefined\"&&typeof window.webkitPerformance!=\"undefined\"&&(r=window.webkitPerformance);var u=[];var o=new Date-window.dateZero;u.push((n?\"ALT=\":\"PLT=\")+getPerformanceTimings(r?r.timing:null,o));u.push(\"nowTS=\"+o);var s=getCookie(\"wlidperf\");if(s){var t=s.split(\"&\");for(i=0;i<t.length;i++)if(t[i].indexOf(\"ST=\")>=0&&t[i].length==16){var h=t[i].substring(3,t[i].length);u.push(\"authTS=\"+h)}}eraseCookie(\"wlidperf\",\".live.com\");var e=[];getResourceEntries(r,e);var f=\"\";isLocalStorageEnabled&&(f=window.localStorage.OwaStartupPerfTrace?window.localStorage.OwaStartupPerfTrace:\"\");return[u.join(\"&\"),e.join(\"&\"),f]}var renderStartTime=0;function setStartRenderTime(){renderStartTime=new Date-window.dateZero}var compositeUsrTime=0;function setCompositeUsrTime(){compositeUsrTime=new Date-window.dateZero}function getPerformanceTimings(n,t){var i=[],r=window.scriptStart;if(n){r=n.navigationStart;if(n.unloadEventStart){i.push(\"uES\");i.push(n.unloadEventStart-r|0)}if(n.unloadEventEnd){i.push(\"uEE\");i.push(n.unloadEventEnd-r|0)}r=fillTimingValues(n,r,i);if(window.owaSDReceivedTimeStamp){i.push(\"sdR\");i.push(window.owaSDReceivedTimeStamp-r|0)}if(parseEndTimes.allBootScripts){i.push(\"pEab\");i.push(parseEndTimes.allBootScripts-r|0)}if(parseEndTimes.allDone){i.push(\"pE\");i.push(parseEndTimes.allDone-r|0)}if(renderStartTime){i.push(\"rSt\");i.push(renderStartTime-r|0)}if(compositeUsrTime){i.push(\"cUsr\");i.push(compositeUsrTime-r|0)}i.push(\"now\");i.push(t-r)}if(window.scriptStart){i.push(\"nowNoTim\");i.push(t-window.scriptStart|0);if(renderStartTime){i.push(\"rStNoTim\");i.push(renderStartTime-window.scriptStart|0)}}return i.join(\",\")}function getResourceEntries(n,t){if(!n||!n.getEntries&&!n.webkitGetEntries)return null;for(var e=n.getEntries?n.getEntries():n.webkitGetEntries(),r=0;r<e.length;r++){var i=e[r];if(i.name.lastIndexOf(\".js\")>0||i.name.lastIndexOf(\".ashx\")>0||i.name.lastIndexOf(\".png\")>0||i.name.lastIndexOf(\".gif\")>0||i.name.lastIndexOf(\".css\")>0||i.name.lastIndexOf(\".eot\")>0||i.name.lastIndexOf(\".ttf\")>0||i.name.lastIndexOf(\".htm\")>0||i.name.lastIndexOf(\".woff\")>0){var f=\"Res=\"+getFileName(i.name)+\",tim=\"+getResourceTiming(i);var u=getFileName(i.name);parseEndTimes[u]&&(f+=\",pE,\"+(parseEndTimes[u]-n.timing.fetchStart));t.push(f)}}}function getResourceTiming(n){var t=[];var i=n.startTime|0;t.push(\"st\");t.push(i);fillTimingValues(n,i,t);return t.join(\",\")}function getFileName(n){var u=Math.max(n.lastIndexOf(\"/\"),n.lastIndexOf(\"\\\\\"));var i=n.indexOf(\"?\");var r=n.indexOf(\"#\");var t=-1;t=i==-1||r==-1?Math.max(i,r):Math.min(i,r);t=t==-1?n.length:t;return n.substring(u+1,t)}function fillTimingValues(n,t,i){if(n.redirectStart){i.push(\"reds\");i.push(n.redirectStart-t|0)}if(n.redirectEnd){i.push(\"redE\");i.push(n.redirectEnd-t|0)}if(n.fetchStart){i.push(\"fS\");i.push(n.fetchStart-t|0);t=n.fetchStart}if(n.domainLookupStart){i.push(\"dLS\");i.push(n.domainLookupStart-t|0)}if(n.domainLookupEnd){i.push(\"dLE\");i.push(n.domainLookupEnd-t|0)}if(n.connectStart){i.push(\"cS\");i.push(n.connectStart-t|0)}if(n.connectEnd){i.push(\"cE\");i.push(n.connectEnd-t|0)}if(n.secureConnectionStart){i.push(\"sCS\");i.push(n.secureConnectionStart-t|0)}if(n.requestStart){i.push(\"reqS\");i.push(n.requestStart-t|0)}if(n.responseStart){i.push(\"resS\");i.push(n.responseStart-t|0)}if(n.responseEnd){i.push(\"resE\");i.push(n.responseEnd-t|0)}if(n.domLoading){i.push(\"domL\");i.push(n.domLoading-t|0)}if(n.domContentLoadedEventStart){i.push(\"domCLES\");i.push(n.domContentLoadedEventStart-t|0)}if(n.domContentLoadedEventEnd){i.push(\"domCLEE\");i.push(n.domContentLoadedEventEnd-t|0)}if(n.domComplete){i.push(\"domC\");i.push(n.domComplete-t|0)}if(n.loadEventStart){i.push(\"lES\");i.push(n.loadEventStart-t|0)}if(n.loadEventEnd){i.push(\"lEE\");i.push(n.loadEventEnd-t|0)}return t}function callPopOutErrorCallback(n){try{if(window.opener&&window.opener.popOutErrorCallbacks){var t=getParameterByName(\"wid\");window.opener.popOutErrorCallbacks[t](n)}cleanupErrorCallback()}catch(i){}}function cleanupErrorCallback(){try{detachUnloadEvent();if(window.opener&&window.opener.popOutErrorCallbacks){var n=getParameterByName(\"wid\");window.opener.popOutErrorCallbacks[n]=null}}catch(t){}}function getParameterByName(n){var i=new RegExp(\"[\\\\#&]\"+n+\"=([^&#]*)\");var t=i.exec(location.hash);return t==null?null:decodeURIComponent(t[1])}function detachUnloadEvent(){try{onunloadbeforeboot&&(window.detachEvent?window.detachEvent(\"onunload\",onunloadbeforeboot):window.removeEventListener(\"unload\",onunloadbeforeboot,!1))}catch(n){}}var pbar={};pbar.startTime=Date.now();pbar.s={plt:6500,maxTime:2e4,sLoad:.05,pltLSKey:\"AvgPLT\"};pbar.caculatecubic=function(n){var t=n/pbar.s.maxTime;t>1&&(t=1);return\"cubic-bezier(\"+t+\",.9,\"+t+\",.9)\"};pbar.startScriptLoad=function(){try{var i=getLocalStorageValue(pbar.s.pltLSKey);i&&(pbar.s.plt=parseInt(i));var n=document.getElementById(\"progressBar\");if(n){var t=pbar.caculatecubic(pbar.s.plt);n.style.WebkitAnimationTimingFunction=t;n.style.animationTimingFunction=t}}catch(r){}};pbar.scriptLoadCompleted=function(){try{var n=document.getElementById(\"progressBar\");var i=(Date.now()-pbar.startTime)/pbar.s.sLoad;if(i<pbar.s.plt&&n){var t=pbar.caculatecubic(i);n.style.WebkitAnimationTimingFunction=t;n.style.animationTimingFunction=t}}catch(r){}};pbar.renderCompleted=function(){try{var t=Date.now()-pbar.startTime;var n=(t+pbar.s.plt)/2;setLocalStorageValue(pbar.s.pltLSKey,n)}catch(i){}}\r\n    </script>\r\n      \r\n        <style>\r\n        \r\n                \r\n                @font-face {\r\n                    font-family: \"wf_segoe-ui_light\";\r\n                    src: local(\"Segoe UI Light\"),\r\n                         local(\"Segoe WP Light\"),\r\n                         url(\'prem/fonts/segoeui-light.woff\') format(\'woff\'),\r\n                         url(\'prem/fonts/segoeui-light.ttf\') format(\'truetype\');\r\n                }\r\n                @font-face {\r\n                    font-family: \"wf_segoe-ui_normal\";\r\n                    src: local(\"Segoe UI\"),\r\n                         local(\"Segoe WP\"),\r\n                         url(\'prem/fonts/segoeui-regular.woff\') format(\'woff\'),\r\n                         url(\'prem/fonts/segoeui-regular.ttf\') format(\'truetype\');\r\n                }\r\n                @font-face {\r\n                    font-family: \"wf_segoe-ui_semibold\";\r\n                    src: local(\"Segoe UI Semibold\"),\r\n                         local(\"Segoe WP Semibold\"),\r\n                         url(\'prem/fonts/segoeui-semibold.woff\') format(\'woff\'),\r\n                         url(\'prem/fonts/segoeui-semibold.ttf\') format(\'truetype\');\r\n                    font-weight: bold;\r\n                }\r\n                @font-face {\r\n                    font-family: \"wf_segoe-ui_semilight\";\r\n                    src: local(\"Segoe UI Semilight\"),\r\n                         local(\"Segoe WP Semilight\"),\r\n                         url(\'prem/fonts/segoeui-semilight.woff\') format(\'woff\'),\r\n                         url(\'prem/fonts/segoeui-semilight.ttf\') format(\'truetype\');\r\n                }\r\n\r\n        \r\n\r\n        @font-face {\r\n            font-family: \'webfontPreload\';\r\n            src: url(\'prem/16.1320.14.2056223/resources/styles/fonts/office365icons.eot?#iefix\') format(\'embedded-opentype\'),\r\n                 url(\'prem/16.1320.14.2056223/resources/styles/fonts/office365icons.woff\') format(\'woff\'),\r\n                 url(\'prem/16.1320.14.2056223/resources/styles/fonts/office365icons.ttf\') format(\'truetype\');\r\n            font-weight: normal;\r\n            font-style: normal;\r\n        }\r\n      </style>\r\n\r\n       <script>\r\n        var LocaleFontFamilyTemplate = \".ms-font-su{color:#333;font-family:@font-family-semilight;font-size:42px;font-weight:normal}.ms-font-xxl{color:#333;font-family:@font-family-light;font-size:28px;font-weight:normal}.touch .ms-font-xxl{font-size:30px}.ms-font-xl{color:#333;font-family:@font-family-light;font-size:21px;font-weight:normal}.touch .ms-font-xl{font-size:22px}.ms-font-l{color:#333;font-family:@font-family-semilight;font-size:17px;font-weight:normal}.touch .ms-font-l{font-size:18px}.ms-font-m{color:#333;font-family:@font-family-regular;font-size:14px;font-weight:normal}.touch .ms-font-m{font-size:15px}.ms-font-s{color:#333;font-family:@font-family-regular;font-size:12px;font-weight:normal}.touch .ms-font-s{font-size:13px}.ms-font-xs{color:#333;font-family:@font-family-regular;font-size:11px;font-weight:normal}.touch .ms-font-xs{font-size:12px}.ms-font-mi{color:#333;font-family:@font-family-semibold;font-size:10px;font-weight:normal}.touch .ms-font-mi{font-size:11px}.ms-font-weight-light,.ms-fwt-l,.ms-font-weight-light-hover:hover,.ms-font-weight-light-before:before,.ms-fwt-l-h:hover,.ms-fwt-l-b:before{font-family:@font-family-light;}.ms-font-weight-semilight,.ms-fwt-sl,.ms-font-weight-semilight-hover:hover,.ms-font-weight-semilight-before:before,.ms-fwt-sl-h:hover,.ms-fwt-sl-b:before{font-family:@font-family-semilight}.ms-font-weight-regular,.ms-fwt-r,.ms-font-weight-regular-hover:hover,.ms-font-weight-regular-before:before,.ms-fwt-r-h:hover,.ms-fwt-r-b:before{font-family:@font-family-regular}.ms-font-weight-semibold,.ms-fwt-sb,.ms-font-weight-semibold-hover:hover,.ms-font-weight-semibold-before:before,.ms-fwt-sb-h:hover,.ms-fwt-sb-b:before{font-family:@font-family-semibold;font-weight:bold}\";\r\n        var ThemedColorTemplate = \".ms-bg-color-themeDark, .ms-bgc-td, .ms-bg-color-themeDark-hover:hover, .ms-bg-color-themeDark-focus:focus, .ms-bg-color-themeDark-before:before, .ms-bgc-td-h:hover, .ms-bgc-td-f:focus, .ms-bgc-td-b:before { background-color: @color-themeDark; }.ms-bg-color-themeDarkAlt, .ms-bgc-tda, .ms-bg-color-themeDarkAlt-hover:hover, .ms-bg-color-themeDarkAlt-focus:focus, .ms-bg-color-themeDarkAlt-before:before, .ms-bgc-tda-h:hover, .ms-bgc-tda-f:focus, .ms-bgc-tda-b:before { background-color: @color-themeDarkAlt; }.ms-bg-color-themeDarker, .ms-bgc-tdr, .ms-bg-color-themeDarker-hover:hover, .ms-bg-color-themeDarker-focus:focus, .ms-bg-color-themeDarker-before:before, .ms-bgc-tdr-h:hover, .ms-bgc-tdr-f:focus, .ms-bgc-tdr-b:before { background-color: @color-themeDarker; }.ms-bg-color-themePrimary, .ms-bgc-tp, .ms-bg-color-themePrimary-hover:hover, .ms-bg-color-themePrimary-focus:focus, .ms-bg-color-themePrimary-before:before, .ms-bgc-tp-h:hover, .ms-bgc-tp-f:focus, .ms-bgc-tp-b:before { background-color: @color-themePrimary; }.ms-bg-color-themeSecondary, .ms-bgc-ts, .ms-bg-color-themeSecondary-hover:hover, .ms-bg-color-themeSecondary-focus:focus, .ms-bg-color-themeSecondary-before:before, .ms-bgc-ts-h:hover, .ms-bgc-ts-f:focus, .ms-bgc-ts-b:before { background-color: @color-themeSecondary; }.ms-bg-color-themeTertiary, .ms-bgc-tt, .ms-bg-color-themeTertiary-hover:hover, .ms-bg-color-themeTertiary-focus:focus, .ms-bg-color-themeTertiary-before:before, .ms-bgc-tt-h:hover, .ms-bgc-tt-f:focus, .ms-bgc-tt-b:before { background-color: @color-themeTertiary; }.ms-bg-color-themeLight, .ms-bgc-tl, .ms-bg-color-themeLight-hover:hover, .ms-bg-color-themeLight-focus:focus, .ms-bg-color-themeLight-before:before, .ms-bgc-tl-h:hover, .ms-bgc-tl-f:focus, .ms-bgc-tl-b:before { background-color: @color-themeLight; }.ms-bg-color-themeLighter, .ms-bgc-tlr, .ms-bg-color-themeLighter-hover:hover, .ms-bg-color-themeLighter-focus:focus, .ms-bg-color-themeLighter-before:before, .ms-bgc-tlr-h:hover, .ms-bgc-tlr-f:focus, .ms-bgc-tlr-b:before { background-color: @color-themeLighter; }.ms-bg-color-themeLighterAlt, .ms-bgc-tlra, .ms-bg-color-themeLighterAlt-hover:hover, .ms-bg-color-themeLighterAlt-focus:focus, .ms-bg-color-themeLighterAlt-before:before, .ms-bgc-tlra-h:hover, .ms-bgc-tlra-f:focus, .ms-bgc-tlra-b:before { background-color: @color-themeLighterAlt; }.ms-border-color-themeDark, .ms-bcl-td, .ms-border-color-themeDark-hover:hover, .ms-border-color-themeDark-focus:focus, .ms-border-color-themeDark-before:before, .ms-bcl-td-h:hover, .ms-bcl-td-f:focus, .ms-bcl-td-b:before { border-color: @color-themeDark; }.ms-border-color-themeDarkAlt, .ms-bcl-tda, .ms-border-color-themeDarkAlt-hover:hover, .ms-border-color-themeDarkAlt-focus:focus, .ms-border-color-themeDarkAlt-before:before, .ms-bcl-tda-h:hover, .ms-bcl-tda-f:focus, .ms-bcl-tda-b:before { border-color: @color-themeDarkAlt; }.ms-border-color-themeDarker, .ms-bcl-tdr, .ms-border-color-themeDarker-hover:hover, .ms-border-color-themeDarker-focus:focus, .ms-border-color-themeDarker-before:before, .ms-bcl-tdr-h:hover, .ms-bcl-tdr-f:focus, .ms-bcl-tdr-b:before { border-color: @color-themeDarker; }.ms-border-color-themePrimary, .ms-bcl-tp, .ms-border-color-themePrimary-hover:hover, .ms-border-color-themePrimary-focus:focus, .ms-border-color-themePrimary-before:before, .ms-bcl-tp-h:hover, .ms-bcl-tp-f:focus, .ms-bcl-tp-b:before { border-color: @color-themePrimary; }.ms-border-color-themeSecondary, .ms-bcl-ts, .ms-border-color-themeSecondary-hover:hover, .ms-border-color-themeSecondary-focus:focus, .ms-border-color-themeSecondary-before:before, .ms-bcl-ts-h:hover, .ms-bcl-ts-f:focus, .ms-bcl-ts-b:before { border-color: @color-themeSecondary; }.ms-border-color-themeTertiary, .ms-bcl-tt, .ms-border-color-themeTertiary-hover:hover, .ms-border-color-themeTertiary-focus:focus, .ms-border-color-themeTertiary-before:before, .ms-bcl-tt-h:hover, .ms-bcl-tt-f:focus, .ms-bcl-tt-b:before { border-color: @color-themeTertiary; }.ms-border-color-themeLight, .ms-bcl-tl, .ms-border-color-themeLight-hover:hover, .ms-border-color-themeLight-focus:focus, .ms-border-color-themeLight-before:before, .ms-bcl-tl-h:hover, .ms-bcl-tl-f:focus, .ms-bcl-tl-b:before { border-color: @color-themeLight; }.ms-border-color-themeLighter, .ms-bcl-tlr, .ms-border-color-themeLighter-hover:hover, .ms-border-color-themeLighter-focus:focus, .ms-border-color-themeLighter-before:before, .ms-bcl-tlr-h:hover, .ms-bcl-tlr-f:focus, .ms-bcl-tlr-b:before { border-color: @color-themeLighter; }.ms-border-color-themeLighterAlt, .ms-bcl-tlra, .ms-border-color-themeLighterAlt-hover:hover, .ms-border-color-themeLighterAlt-focus:focus, .ms-border-color-themeLighterAlt-before:before, .ms-bcl-tlra-h:hover, .ms-bcl-tlra-f:focus, .ms-bcl-tlra-b:before { border-color: @color-themeLighterAlt; }.ms-border-color-top-themePrimary, .ms-bcl-t-tp, .ms-border-color-top-themePrimary-hover:hover, .ms-border-color-top-themePrimary-focus:focus, .ms-border-color-top-themePrimary-before:before, .ms-bcl-t-tp-h:hover, .ms-bcl-t-tp-f:focus, .ms-bcl-t-tp-b:before { border-top-color: @color-themePrimary; }.ms-font-color-themeDark, .ms-fcl-td, .ms-font-color-themeDark-hover:hover, .ms-font-color-themeDark-focus:focus, .ms-font-color-themeDark-before:before, .ms-fcl-td-h:hover, .ms-fcl-td-f:focus, .ms-fcl-td-b:before { color: @color-themeDark; }.ms-font-color-themeDarkAlt, .ms-fcl-tda, .ms-font-color-themeDarkAlt-hover:hover, .ms-font-color-themeDarkAlt-focus:focus, .ms-font-color-themeDarkAlt-before:before, .ms-fcl-tda-h:hover, .ms-fcl-tda-f:focus, .ms-fcl-tda-b:before { color: @color-themeDarkAlt; }.ms-font-color-themeDarker, .ms-fcl-tdr, .ms-font-color-themeDarker-hover:hover, .ms-font-color-themeDarker-focus:focus, .ms-font-color-themeDarker-before:before, .ms-fcl-tdr-h:hover, .ms-fcl-tdr-f:focus, .ms-fcl-tdr-b:before { color: @color-themeDarker; }.ms-font-color-themePrimary, .ms-fcl-tp, .ms-font-color-themePrimary-hover:hover, .ms-font-color-themePrimary-focus:focus, .ms-font-color-themePrimary-before:before, .ms-fcl-tp-h:hover, .ms-fcl-tp-f:focus, .ms-fcl-tp-b:before { color: @color-themePrimary; }.ms-font-color-themeSecondary, .ms-fcl-ts, .ms-font-color-themeSecondary-hover:hover, .ms-font-color-themeSecondary-focus:focus, .ms-font-color-themeSecondary-before:before, .ms-fcl-ts-h:hover, .ms-fcl-ts-f:focus, .ms-fcl-ts-b:before { color: @color-themeSecondary; }.ms-font-color-themeTertiary, .ms-fcl-tt, .ms-font-color-themeTertiary-hover:hover, .ms-font-color-themeTertiary-focus:focus, .ms-font-color-themeTertiary-before:before, .ms-fcl-tt-h:hover, .ms-fcl-tt-f:focus, .ms-fcl-tt-b:before { color: @color-themeTertiary; }.ms-font-color-themeLight, .ms-fcl-tl, .ms-font-color-themeLight-hover:hover, .ms-font-color-themeLight-focus:focus, .ms-font-color-themeLight-before:before, .ms-fcl-tl-h:hover, .ms-fcl-tl-f:focus, .ms-fcl-tl-b:before { color: @color-themeLight; }.ms-font-color-themeLighter, .ms-fcl-tlr, .ms-font-color-themeLighter-hover:hover, .ms-font-color-themeLighter-focus:focus, .ms-font-color-themeLighter-before:before, .ms-fcl-tlr-h:hover, .ms-fcl-tlr-f:focus, .ms-fcl-tlr-b:before { color: @color-themeLighter; }.ms-font-color-themeLighterAlt, .ms-fcl-tlra, .ms-font-color-themeLighterAlt-hover:hover, .ms-font-color-themeLighterAlt-focus:focus, .ms-font-color-themeLighterAlt-before:before, .ms-fcl-tlra-h:hover, .ms-fcl-tlra-f:focus, .ms-fcl-tlra-b:before { color: @color-themeLighterAlt; }\";\r\n        var o365ColorTemplate = \".o365cs-base.o365cst .o365cs-topnavLinkBackground-2{background-color:@topnavLinkBG;background-color:@topnavLinkBGrgb;}.o365cs-base.o365cst .o365cs-topnavText,.o365cs-base.o365cst .o365cs-topnavText:hover{color:@topnavText;}.o365cs-base.o365cst .o365cs-navMenuButton{color:@navmenu;}.o365cs-base.o365cst.o365cs-topnavBGColor-2{background-color:@topnavBG;}.o365cs-base.o365cst .o365cs-appLauncherBackground{background-color:@appLauncherBG}\";\r\n                \r\n        </script>\r\n        <style>.customScrollBar::-webkit-scrollbar{height:18px;width:18px}.customScrollBar::-webkit-scrollbar:disabled{display:none}.customScrollBar::-webkit-scrollbar-button{background-color:#fff;background-repeat:no-repeat;cursor:pointer}.customScrollBar::-webkit-scrollbar-button:horizontal:increment,.customScrollBar::-webkit-scrollbar-button:horizontal:decrement,.customScrollBar::-webkit-scrollbar-button:horizontal:increment:hover,.customScrollBar::-webkit-scrollbar-button:horizontal:decrement:hover,.customScrollBar::-webkit-scrollbar-button:vertical:increment,.customScrollBar::-webkit-scrollbar-button:vertical:decrement,.customScrollBar::-webkit-scrollbar-button:vertical:increment:hover,.customScrollBar::-webkit-scrollbar-button:vertical:decrement:hover{background-position:center;height:18px;width:18px}.customScrollBarLight::-webkit-scrollbar-button{display:none}.customScrollBar::-webkit-scrollbar-track{background-color:#fff}.customScrollBarLight::-webkit-scrollbar-track{background-color:#0072c6}.customScrollBar::-webkit-scrollbar-thumb{border-radius:9px;border:solid 6px #fff;background-color:#c8c8c8}.customScrollBarLight::-webkit-scrollbar-thumb{border-color:#0072c6;background-color:#95b1c1}.customScrollBar::-webkit-scrollbar-thumb:vertical{min-height:50px}.customScrollBar::-webkit-scrollbar-thumb:horizontal{min-width:50px}.customScrollBar::-webkit-scrollbar-thumb:hover{border-radius:9px;border:solid 6px #fff;background-color:#98a3a6}.customScrollBar::-webkit-scrollbar-corner{background-color:#fff}.nativeScrollInertia{-webkit-overflow-scrolling:touch}.csimg{display:inline-block;overflow:hidden}button::-moz-focus-inner{border-width:0;padding:0}.textbox{border-width:1px;border-style:solid;border-radius:0;-moz-border-radius:0;-webkit-border-radius:0;box-shadow:none;-moz-box-shadow:none;-webkit-box-shadow:none;-webkit-appearance:none;height:30px;padding:0 5px}.tnarrow .textbox,.twide .textbox{font-size:12px;background-color:#fff;height:14px;padding:3px 5px}.textbox::-webkit-input-placeholder{color:#a6a6a6}.textbox:-moz-placeholder{color:#a6a6a6}.textbox::-moz-placeholder{color:#a6a6a6}.textbox:-ms-input-placeholder{color:#a6a6a6}.textarea{padding:10px}.textarea:hover{background-color:transparent;border-color:transparent}.o365button{background:transparent;border-width:0;padding:0;cursor:pointer!important;font-size:14px}.o365button:disabled,label.o365button[disabled=true]{cursor:default!important}.o365buttonOutlined{padding-right:11px;padding-left:11px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border-width:1px;border-style:solid}.o365buttonOutlined .o365buttonLabel{display:inline-block}.o365buttonOutlined{height:30px}.twide .o365buttonOutlined,.tnarrow .o365buttonOutlined{height:22px}.o365buttonOutlined .o365buttonLabel{height:22px}.checkbox{border-style:none;cursor:pointer;vertical-align:middle}.popupShadow{box-shadow:0 0 20px rgba(0,0,0,.4);border:1px solid #eaeaea}.contextMenuDropShadow{box-shadow:0 0 7px rgba(0,0,0,.4);border:1px solid #eaeaea}.modalBackground{background-color:#fff;height:100%;width:100%;opacity:.65;filter:Alpha(opacity=65)}.clearModalBackground{background-color:#fff;opacity:0;filter:Alpha(opacity=0);height:100%;width:100%}.contextMenuPopup{background-color:#fff}.removeFocusOutline *:focus{outline:none}.addFocusOutline button:focus{outline:dotted 1px}.addFocusRingOutline button:focus{outline:auto 5px -webkit-focus-ring-color}.border-color-transparent{border-color:transparent}.vResize,.hResize{z-index:1000}.hResize,.hResizeCursor *{cursor:row-resize!important}.vResize,.vResizeCursor *{cursor:col-resize!important}.vResizing,.hResizing{filter:alpha(opacity=60);opacity:.6;-moz-opacity:.6;border:solid 1px #666}.vResizing{border-width:0 1px}.hResizing{border-width:1px 0}</style>\r\n        <style></style>\r\n        <style id=\"FontLocaleStyles\"></style>\r\n        <style id=\"ThemedColorStyles\"></style>\r\n        <style>.ms-bg-color-black,.ms-bgc-b,.ms-bg-color-black-hover:hover,.ms-bg-color-black-focus:focus,.ms-bg-color-black-before:before,.ms-bgc-b-h:hover,.ms-bgc-b-f:focus,.ms-bgc-b-b:before{background-color:#000}.ms-bg-color-neutralDark,.ms-bgc-nd,.ms-bg-color-neutralDark-hover:hover,.ms-bg-color-neutralDark-focus:focus,.ms-bg-color-neutralDark-before:before,.ms-bgc-nd-h:hover,.ms-bgc-nd-f:focus,.ms-bgc-nd-b:before{background-color:#212121}.ms-bg-color-neutralPrimary,.ms-bgc-np,.ms-bg-color-neutralPrimary-hover:hover,.ms-bg-color-neutralPrimary-focus:focus,.ms-bg-color-neutralPrimary-before:before,.ms-bgc-np-h:hover,.ms-bgc-np-f:focus,.ms-bgc-np-b:before{background-color:#333}.ms-bg-color-neutralSecondary,.ms-bgc-ns,.ms-bg-color-neutralSecondary-hover:hover,.ms-bg-color-neutralSecondary-focus:focus,.ms-bg-color-neutralSecondary-before:before,.ms-bgc-ns-h:hover,.ms-bgc-ns-f:focus,.ms-bgc-ns-b:before{background-color:#666}.ms-bg-color-neutralSecondaryAlt,.ms-bgc-nsa,.ms-bg-color-neutralSecondaryAlt-hover:hover,.ms-bg-color-neutralSecondaryAlt-focus:focus,.ms-bg-color-neutralSecondaryAlt-before:before,.ms-bgc-nsa-h:hover,.ms-bgc-nsa-f:focus,.ms-bgc-nsa-b:before{background-color:#767676}.ms-bg-color-neutralTertiary,.ms-bgc-nt,.ms-bg-color-neutralTertiary-hover:hover,.ms-bg-color-neutralTertiary-focus:focus,.ms-bg-color-neutralTertiary-before:before,.ms-bgc-nt-h:hover,.ms-bgc-nt-f:focus,.ms-bgc-nt-b:before{background-color:#a6a6a6}.ms-bg-color-neutralTertiaryAlt,.ms-bgc-nta,.ms-bg-color-neutralTertiaryAlt-hover:hover,.ms-bg-color-neutralTertiaryAlt-focus:focus,.ms-bg-color-neutralTertiaryAlt-before:before,.ms-bgc-nta-h:hover,.ms-bgc-nta-f:focus,.ms-bgc-nta-b:before{background-color:#c8c8c8}.ms-bg-color-neutralLight,.ms-bgc-nl,.ms-bg-color-neutralLight-hover:hover,.ms-bg-color-neutralLight-focus:focus,.ms-bg-color-neutralLight-before:before,.ms-bgc-nl-h:hover,.ms-bgc-nl-f:focus,.ms-bgc-nl-b:before{background-color:#eaeaea}.ms-bg-color-neutralLighter,.ms-bgc-nlr,.ms-bg-color-neutralLighter-hover:hover,.ms-bg-color-neutralLighter-focus:focus,.ms-bg-color-neutralLighter-before:before,.ms-bgc-nlr-h:hover,.ms-bgc-nlr-f:focus,.ms-bgc-nlr-b:before{background-color:#f4f4f4}.ms-bg-color-neutralLighterAlt,.ms-bgc-nlra,.ms-bg-color-neutralLighterAlt-hover:hover,.ms-bg-color-neutralLighterAlt-focus:focus,.ms-bg-color-neutralLighterAlt-before:before,.ms-bgc-nlra-h:hover,.ms-bgc-nlra-f:focus,.ms-bgc-nlra-b:before{background-color:#f8f8f8}.ms-bg-color-white,.ms-bgc-w,.ms-bg-color-white-hover:hover,.ms-bg-color-white-focus:focus,.ms-bg-color-white-before:before,.ms-bgc-w-h:hover,.ms-bgc-w-b:before{background-color:#fff}.ms-border-color-black,.ms-bcl-b,.ms-border-color-black-hover:hover,.ms-border-color-black-focus:focus,.ms-border-color-black-before:before,.ms-bcl-b-h:hover,.ms-bcl-b-f:focus,.ms-bcl-b-b:before{border-color:#000}.ms-border-color-neutralDark,.ms-bcl-nd,.ms-border-color-neutralDark-hover:hover,.ms-border-color-neutralDark-focus:focus,.ms-border-color-neutralDark-before:before,.ms-bcl-nd-h:hover,.ms-bcl-nd-f:focus,.ms-bcl-nd-b:before{border-color:#212121}.ms-border-color-neutralPrimary,.ms-bcl-np,.ms-border-color-neutralPrimary-hover:hover,.ms-border-color-neutralPrimary-focus:focus,.ms-border-color-neutralPrimary-before:before,.ms-bcl-np-h:hover,.ms-bcl-np-f:focus,.ms-bcl-np-b:before{border-color:#333}.ms-border-color-neutralSecondary,.ms-bcl-ns,.ms-border-color-neutralSecondary-hover:hover,.ms-border-color-neutralSecondary-focus:focus,.ms-border-color-neutralSecondary-before:before,.ms-bcl-ns-h:hover,.ms-bcl-ns-f:focus,.ms-bcl-ns-b:before{border-color:#666}.ms-border-color-neutralSecondaryAlt,.ms-bcl-nsa,.ms-border-color-neutralSecondaryAlt-hover:hover,.ms-border-color-neutralSecondaryAlt-focus:focus,.ms-border-color-neutralSecondaryAlt-before:before,.ms-bcl-nsa-h:hover,.ms-bcl-nsa-f:focus,.ms-bcl-nsa-b:before{border-color:#767676}.ms-border-color-neutralTertiary,.ms-bcl-nt,.ms-border-color-neutralTertiary-hover:hover,.ms-border-color-neutralTertiary-focus:focus,.ms-border-color-neutralTertiary-before:before,.ms-bcl-nt-h:hover,.ms-bcl-nt-f:focus,.ms-bcl-nt-b:before{border-color:#a6a6a6}.ms-border-color-neutralTertiaryAlt,.ms-bcl-nta,.ms-border-color-neutralTertiaryAlt-hover:hover,.ms-border-color-neutralTertiaryAlt-focus:focus,.ms-border-color-neutralTertiaryAlt-before:before,.ms-bcl-nta-h:hover,.ms-bcl-nta-f:focus,.ms-bcl-nta-b:before{border-color:#c8c8c8}.ms-border-color-neutralLight,.ms-bcl-nl,.ms-border-color-neutralLight-hover:hover,.ms-border-color-neutralLight-focus:focus,.ms-border-color-neutralLight-before:before,.ms-bcl-nl-h:hover,.ms-bcl-nl-f:focus,.ms-bcl-nl-b:before{border-color:#eaeaea}.ms-border-color-neutralLighter,.ms-bcl-nlr,.ms-border-color-neutralLighter-hover:hover,.ms-border-color-neutralLighter-focus:focus,.ms-border-color-neutralLighter-before:before,.ms-bcl-nlr-h:hover,.ms-bcl-nlr-f:focus,.ms-bcl-nlr-b:before{border-color:#f4f4f4}.ms-border-color-neutralLighterAlt,.ms-bcl-nlra,.ms-border-color-neutralLighterAlt-hover:hover,.ms-border-color-neutralLighterAlt-focus:focus,.ms-border-color-neutralLighterAlt-before:before,.ms-bcl-nlra-h:hover,.ms-bcl-nlra-f:focus,.ms-bcl-nlra-b:before{border-color:#f8f8f8}.ms-border-color-white,.ms-bcl-w,.ms-border-color-white-hover:hover,.ms-border-color-white-focus:focus,.ms-border-color-white-before:before,.ms-bcl-w-h:hover,.ms-bcl-w-f:focus,.ms-bcl-w-b:before{border-color:#fff}.ms-font-color-black,.ms-fcl-b,.ms-font-color-black-hover:hover,.ms-font-color-black-focus:focus,.ms-font-color-black-before:before,.ms-fcl-b-h:hover,.ms-fcl-b-f:focus,.ms-fcl-b-b:before{color:#000}.ms-font-color-neutralDark,.ms-fcl-nd,.ms-font-color-neutralDark-hover:hover,.ms-font-color-neutralDark-focus:focus,.ms-font-color-neutralDark-before:before,.ms-fcl-nd-h:hover,.ms-fcl-nd-f:focus,.ms-fcl-nd-b:before{color:#212121}.ms-font-color-neutralPrimary,.ms-fcl-np,.ms-font-color-neutralPrimary-hover:hover,.ms-font-color-neutralPrimary-focus:focus,.ms-font-color-neutralPrimary-before:before,.ms-fcl-np-h:hover,.ms-fcl-np-f:focus,.ms-fcl-np-b:before{color:#333}.ms-font-color-neutralSecondary,.ms-fcl-ns,.ms-font-color-neutralSecondary-hover:hover,.ms-font-color-neutralSecondary-focus:focus,.ms-font-color-neutralSecondary-before:before,.ms-fcl-ns-h:hover,.ms-fcl-ns-f:focus,.ms-fcl-ns-b:before{color:#666}.ms-font-color-neutralSecondaryAlt,.ms-fcl-nsa,.ms-font-color-neutralSecondaryAlt-hover:hover,.ms-font-color-neutralSecondaryAlt-focus:focus,.ms-font-color-neutralSecondaryAlt-before:before,.ms-fcl-nsa-h:hover,.ms-fcl-nsa-f:focus,.ms-fcl-nsa-b:before{color:#767676}.ms-font-color-neutralTertiary,.ms-fcl-nt,.ms-font-color-neutralTertiary-hover:hover,.ms-font-color-neutralTertiary-focus:focus,.ms-font-color-neutralTertiary-before:before,.ms-fcl-nt-h:hover,.ms-fcl-nt-f:focus,.ms-fcl-nt-b:before{color:#a6a6a6}.ms-font-color-neutralTertiaryAlt,.ms-fcl-nta,.ms-font-color-neutralTertiaryAlt-hover:hover,.ms-font-color-neutralTertiaryAlt-focus:focus,.ms-font-color-neutralTertiaryAlt-before:before,.ms-fcl-nta-h:hover,.ms-fcl-nta-f:focus,.ms-fcl-nta-b:before{color:#c8c8c8}.ms-font-color-neutralLight,.ms-fcl-nl,.ms-font-color-neutralLight-hover:hover,.ms-font-color-neutralLight-focus:focus,.ms-font-color-neutralLight-before:before,.ms-fcl-nl-h:hover,.ms-fcl-nl-f:focus,.ms-fcl-nl-b:before{color:#eaeaea}.ms-font-color-neutralLighter,.ms-fcl-nlr,.ms-font-color-neutralLighter-hover:hover,.ms-font-color-neutralLighter-focus:focus,.ms-font-color-neutralLighter-before:before,.ms-fcl-nlr-h:hover,.ms-fcl-nlr-f:focus,.ms-fcl-nlr-b:before{color:#f4f4f4}.ms-font-color-neutralLighterAlt,.ms-fcl-nlra,.ms-font-color-neutralLighterAlt-hover:hover,.ms-font-color-neutralLighterAlt-focus:focus,.ms-font-color-neutralLighterAlt-before:before,.ms-fcl-nlra-h:hover,.ms-fcl-nlra-f:focus,.ms-fcl-nlra-b:before{color:#f8f8f8}.ms-font-color-white,.ms-fcl-w,.ms-font-color-white-hover:hover,.ms-font-color-white-focus:focus,.ms-font-color-white-before:before,.ms-fcl-w-h:hover,.ms-fcl-w-f:focus,.ms-fcl-w-b:before{color:#fff}</style>\r\n        <style>.ms-bg-color-yellow,.ms-bgc-y,.ms-bg-color-yellow-hover:hover,.ms-bg-color-yellow-before:before,.ms-bgc-y-h:hover,.ms-bgc-y-b:before{background-color:#ffb900}.ms-bg-color-yellowLight,.ms-bgc-yl,.ms-bg-color-yellowLight-hover:hover,.ms-bg-color-yellowLight-before:before,.ms-bgc-yl-h:hover,.ms-bgc-yl-b:before{background-color:#fff100}.ms-bg-color-orange,.ms-bgc-o,.ms-bg-color-orange-hover:hover,.ms-bg-color-orange-before:before,.ms-bgc-o-h:hover,.ms-bgc-o-b:before{background-color:#d83b01}.ms-bg-color-orangeLight,.ms-bgc-ol,.ms-bg-color-orangeLight-hover:hover,.ms-bg-color-orangeLight-before:before,.ms-bgc-ol-h:hover,.ms-bgc-ol-b:before{background-color:#ff8c00}.ms-bg-color-redDark,.ms-bgc-rd,.ms-bg-color-redDark-hover:hover,.ms-bg-color-redDark-before:before,.ms-bgc-rd-h:hover,.ms-bgc-rd-b:before{background-color:#a80000}.ms-bg-color-red,.ms-bgc-r,.ms-bg-color-red-hover:hover,.ms-bg-color-red-before:before,.ms-bgc-r-h:hover,.ms-bgc-r-b:before{background-color:#e81123}.ms-bg-color-magentaDark,.ms-bgc-md,.ms-bg-color-magentaDark-hover:hover,.ms-bg-color-magentaDark-before:before,.ms-bgc-md-h:hover,.ms-bgc-md-b:before{background-color:#5c005c}.ms-bg-color-magenta,.ms-bgc-m,.ms-bg-color-magenta-hover:hover,.ms-bg-color-magenta-before:before,.ms-bgc-m-h:hover,.ms-bgc-m-b:before{background-color:#b4009e}.ms-bg-color-magentaLight,.ms-bgc-ml,.ms-bg-color-magentaLight-hover:hover,.ms-bg-color-magentaLight-before:before,.ms-bgc-ml-h:hover,.ms-bgc-ml-b:before{background-color:#e3008c}.ms-bg-color-purpleDark,.ms-bgc-pd,.ms-bg-color-purpleDark-hover:hover,.ms-bg-color-purpleDark-before:before,.ms-bgc-pd-h:hover,.ms-bgc-pd-b:before{background-color:#32145a}.ms-bg-color-purple,.ms-bgc-p,.ms-bg-color-purple-hover:hover,.ms-bg-color-purple-before:before,.ms-bgc-p-h:hover,.ms-bgc-p-b:before{background-color:#5c2d91}.ms-bg-color-purpleLight,.ms-bgc-pl,.ms-bg-color-purpleLight-hover:hover,.ms-bg-color-purpleLight-before:before,.ms-bgc-pl-h:hover,.ms-bgc-pl-b:before{background-color:#b4a0ff}.ms-bg-color-blueDark,.ms-bgc-bd,.ms-bg-color-blueDark-hover:hover,.ms-bg-color-blueDark-before:before,.ms-bgc-bd-h:hover,.ms-bgc-bd-b:before{background-color:#002050}.ms-bg-color-blueMid,.ms-bgc-bm,.ms-bg-color-blueMid-hover:hover,.ms-bg-color-blueMid-before:before,.ms-bgc-bm-h:hover,.ms-bgc-bm-b:before{background-color:#00188f}.ms-bg-color-blue,.ms-bgc-blu,.ms-bg-color-blue-hover:hover,.ms-bg-color-blue-before:before,.ms-bgc-blu-h:hover,.ms-bgc-blu-b:before{background-color:#0078d7}.ms-bg-color-blueLight,.ms-bgc-bl,.ms-bg-color-blueLight-hover:hover,.ms-bg-color-blueLight-before:before,.ms-bgc-bl-h:hover,.ms-bgc-bl-b:before{background-color:#00bcf2}.ms-bg-color-tealDark,.ms-bgc-ted,.ms-bg-color-tealDark-hover:hover,.ms-bg-color-tealDark-before:before,.ms-bgc-ted-h:hover,.ms-bgc-ted-b:before{background-color:#004b50}.ms-bg-color-teal,.ms-bgc-t,.ms-bg-color-teal-hover:hover,.ms-bg-color-teal-before:before,.ms-bgc-t-h:hover,.ms-bgc-t-b:before{background-color:#008272}.ms-bg-color-tealLight,.ms-bgc-tel,.ms-bg-color-tealLight-hover:hover,.ms-bg-color-tealLight-before:before,.ms-bgc-tel-h:hover,.ms-bgc-tel-b:before{background-color:#00b294}.ms-bg-color-greenDark,.ms-bgc-gd,.ms-bg-color-greenDark-hover:hover,.ms-bg-color-greenDark-before:before,.ms-bgc-gd-h:hover,.ms-bgc-gd-b:before{background-color:#004b1c}.ms-bg-color-green,.ms-bgc-g,.ms-bg-color-green-hover:hover,.ms-bg-color-green-before:before,.ms-bgc-g-h:hover,.ms-bgc-g-b:before{background-color:#107c10}.ms-bg-color-greenLight,.ms-bgc-gl,.ms-bg-color-greenLight-hover:hover,.ms-bg-color-greenLight-before:before,.ms-bgc-gl-h:hover,.ms-bgc-gl-b:before{background-color:#bad80a}.ms-border-color-yellow,.ms-bcl-y,.ms-border-color-yellow-hover:hover,.ms-border-color-yellow-before:before,.ms-bcl-y-h:hover,.ms-bcl-y-b:before{border-color:#ffb900}.ms-border-color-yellowLight,.ms-bcl-yl,.ms-border-color-yellowLight-hover:hover,.ms-border-color-yellowLight-before:before,.ms-bcl-yl-h:hover,.ms-bcl-yl-b:before{border-color:#fff100}.ms-border-color-orange,.ms-bcl-o,.ms-border-color-orange-hover:hover,.ms-border-color-orange-before:before,.ms-bcl-o-h:hover,.ms-bcl-o-b:before{border-color:#d83b01}.ms-border-color-orangeLight,.ms-bcl-ol,.ms-border-color-orangeLight-hover:hover,.ms-border-color-orangeLight-before:before,.ms-bcl-ol-h:hover,.ms-bcl-ol-b:before{border-color:#ff8c00}.ms-border-color-redDark,.ms-bcl-rd,.ms-border-color-redDark-hover:hover,.ms-border-color-redDark-before:before,.ms-bcl-rd-h:hover,.ms-bcl-rd-b:before{border-color:#a80000}.ms-border-color-red,.ms-bcl-r,.ms-border-color-red-hover:hover,.ms-border-color-red-before:before,.ms-bcl-r-h:hover,.ms-bcl-r-b:before{border-color:#e81123}.ms-border-color-magentaDark,.ms-bcl-md,.ms-border-color-magentaDark-hover:hover,.ms-border-color-magentaDark-before:before,.ms-bcl-md-h:hover,.ms-bcl-md-b:before{border-color:#5c005c}.ms-border-color-magenta,.ms-bcl-m,.ms-border-color-magenta-hover:hover,.ms-border-color-magenta-before:before,.ms-bcl-m-h:hover,.ms-bcl-m-b:before{border-color:#b4009e}.ms-border-color-magentaLight,.ms-bcl-ml,.ms-border-color-magentaLight-hover:hover,.ms-border-color-magentaLight-before:before,.ms-bcl-ml-h:hover,.ms-bcl-ml-b:before{border-color:#e3008c}.ms-border-color-purpleDark,.ms-bcl-pd,.ms-border-color-purpleDark-hover:hover,.ms-border-color-purpleDark-before:before,.ms-bcl-pd-h:hover,.ms-bcl-pd-b:before{border-color:#32145a}.ms-border-color-purple,.ms-bcl-p,.ms-border-color-purple-hover:hover,.ms-border-color-purple-before:before,.ms-bcl-p-h:hover,.ms-bcl-p-b:before{border-color:#5c2d91}.ms-border-color-purpleLight,.ms-bcl-pl,.ms-border-color-purpleLight-hover:hover,.ms-border-color-purpleLight-before:before,.ms-bcl-pl-h:hover,.ms-bcl-pl-b:before{border-color:#b4a0ff}.ms-border-color-blueDark,.ms-bcl-bd,.ms-border-color-blueDark-hover:hover,.ms-border-color-blueDark-before:before,.ms-bcl-bd-h:hover,.ms-bcl-bd-b:before{border-color:#002050}.ms-border-color-blueMid,.ms-bcl-bm,.ms-border-color-blueMid-hover:hover,.ms-border-color-blueMid-before:before,.ms-bcl-bm-h:hover,.ms-bcl-bm-b:before{border-color:#00188f}.ms-border-color-blue,.ms-bcl-blu,.ms-border-color-blue-hover:hover,.ms-border-color-blue-before:before,.ms-bcl-blu-h:hover,.ms-bcl-blu-b:before{border-color:#0078d7}.ms-border-color-blueLight,.ms-bcl-bl,.ms-border-color-blueLight-hover:hover,.ms-border-color-blueLight-before:before,.ms-bcl-bl-h:hover,.ms-bcl-bl-b:before{border-color:#00bcf2}.ms-border-color-tealDark,.ms-bcl-ted,.ms-border-color-tealDark-hover:hover,.ms-border-color-tealDark-before:before,.ms-bcl-ted-h:hover,.ms-bcl-ted-b:before{border-color:#004b50}.ms-border-color-teal,.ms-bcl-t,.ms-border-color-teal-hover:hover,.ms-border-color-teal-before:before,.ms-bcl-t-h:hover,.ms-bcl-t-b:before{border-color:#008272}.ms-border-color-tealLight,.ms-bcl-tel,.ms-border-color-tealLight-hover:hover,.ms-border-color-tealLight-before:before,.ms-bcl-tel-h:hover,.ms-bcl-tel-b:before{border-color:#00b294}.ms-border-color-greenDark,.ms-bcl-gd,.ms-border-color-greenDark-hover:hover,.ms-border-color-greenDark-before:before,.ms-bcl-gd-h:hover,.ms-bcl-gd-b:before{border-color:#004b1c}.ms-border-color-green,.ms-bcl-g,.ms-border-color-green-hover:hover,.ms-border-color-green-before:before,.ms-bcl-g-h:hover,.ms-bcl-g-b:before{border-color:#107c10}.ms-border-color-greenLight,.ms-bcl-gl,.ms-border-color-greenLight-hover:hover,.ms-border-color-greenLight-before:before,.ms-bcl-gl-h:hover,.ms-bcl-gl-b:before{border-color:#bad80a}.ms-font-color-yellow,.ms-fcl-y,.ms-font-color-yellow-hover:hover,.ms-font-color-yellow-before:before,.ms-fcl-y-h:hover,.ms-fcl-y-b:before{color:#ffb900}.ms-font-color-yellowLight,.ms-fcl-yl,.ms-font-color-yellowLight-hover:hover,.ms-font-color-yellowLight-before:before,.ms-fcl-yl-h:hover,.ms-fcl-yl-b:before{color:#fff100}.ms-font-color-orange,.ms-fcl-o,.ms-font-color-orange-hover:hover,.ms-font-color-orange-before:before,.ms-fcl-o-h:hover,.ms-fcl-o-b:before{color:#d83b01}.ms-font-color-orangeLight,.ms-fcl-ol,.ms-font-color-orangeLight-hover:hover,.ms-font-color-orangeLight-before:before,.ms-fcl-ol-h:hover,.ms-fcl-ol-b:before{color:#ff8c00}.ms-font-color-redDark,.ms-fcl-rd,.ms-font-color-redDark-hover:hover,.ms-font-color-redDark-before:before,.ms-fcl-rd-h:hover,.ms-fcl-rd-b:before{color:#a80000}.ms-font-color-red,.ms-fcl-r,.ms-font-color-red-hover:hover,.ms-font-color-red-before:before,.ms-fcl-r-h:hover,.ms-fcl-r-b:before{color:#e81123}.ms-font-color-magentaDark,.ms-fcl-md,.ms-font-color-magentaDark-hover:hover,.ms-font-color-magentaDark-before:before,.ms-fcl-md-h:hover,.ms-fcl-md-b:before{color:#5c005c}.ms-font-color-magenta,.ms-fcl-m,.ms-font-color-magenta-hover:hover,.ms-font-color-magenta-before:before,.ms-fcl-m-h:hover,.ms-fcl-m-b:before{color:#b4009e}.ms-font-color-magentaLight,.ms-fcl-ml,.ms-font-color-magentaLight-hover:hover,.ms-font-color-magentaLight-before:before,.ms-fcl-ml-h:hover,.ms-fcl-ml-b:before{color:#e3008c}.ms-font-color-purpleDark,.ms-fcl-pd,.ms-font-color-purpleDark-hover:hover,.ms-font-color-purpleDark-before:before,.ms-fcl-pd-h:hover,.ms-fcl-pd-b:before{color:#32145a}.ms-font-color-purple,.ms-fcl-p,.ms-font-color-purple-hover:hover,.ms-font-color-purple-before:before,.ms-fcl-p-h:hover,.ms-fcl-p-b:before{color:#5c2d91}.ms-font-color-purpleLight,.ms-fcl-pl,.ms-font-color-purpleLight-hover:hover,.ms-font-color-purpleLight-before:before,.ms-fcl-pl-h:hover,.ms-fcl-pl-b:before{color:#b4a0ff}.ms-font-color-blueDark,.ms-fcl-bd,.ms-font-color-blueDark-hover:hover,.ms-font-color-blueDark-before:before,.ms-fcl-bd-h:hover,.ms-fcl-bd-b:before{color:#002050}.ms-font-color-blueMid,.ms-fcl-bm,.ms-font-color-blueMid-hover:hover,.ms-font-color-blueMid-before:before,.ms-fcl-bm-h:hover,.ms-fcl-bm-b:before{color:#00188f}.ms-font-color-blue,.ms-fcl-blu,.ms-font-color-blue-hover:hover,.ms-font-color-blue-before:before,.ms-fcl-blu-h:hover,.ms-fcl-blu-b:before{color:#0078d7}.ms-font-color-blueLight,.ms-fcl-bl,.ms-font-color-blueLight-hover:hover,.ms-font-color-blueLight-before:before,.ms-fcl-bl-h:hover,.ms-fcl-bl-b:before{color:#00bcf2}.ms-font-color-tealDark,.ms-fcl-ted,.ms-font-color-tealDark-hover:hover,.ms-font-color-tealDark-before:before,.ms-fcl-ted-h:hover,.ms-fcl-ted-b:before{color:#004b50}.ms-font-color-teal,.ms-fcl-t,.ms-font-color-teal-hover:hover,.ms-font-color-teal-before:before,.ms-fcl-t-h:hover,.ms-fcl-t-b:before{color:#008272}.ms-font-color-tealLight,.ms-fcl-tel,.ms-font-color-tealLight-hover:hover,.ms-font-color-tealLight-before:before,.ms-fcl-tel-h:hover,.ms-fcl-tel-b:before{color:#00b294}.ms-font-color-greenDark,.ms-fcl-gd,.ms-font-color-greenDark-hover:hover,.ms-font-color-greenDark-before:before,.ms-fcl-gd-h:hover,.ms-fcl-gd-b:before{color:#004b1c}.ms-font-color-green,.ms-fcl-g,.ms-font-color-green-hover:hover,.ms-font-color-green-before:before,.ms-fcl-g-h:hover,.ms-fcl-g-b:before{color:#107c10}.ms-font-color-greenLight,.ms-fcl-gl,.ms-font-color-greenLight-hover:hover,.ms-font-color-greenLight-before:before,.ms-fcl-gl-h:hover,.ms-fcl-gl-b:before{color:#bad80a}</style>\r\n        <style>.owa-font-compose{font-family:Calibri,Arial,Helvetica,sans-serif}.owa-bg-color-neutral-orange{background-color:#D82300}.owa-bg-color-neutral-red{background-color:#A80F22}.owa-bg-color-neutral-yellow{background-color:#FFEE94}.owa-bg-color-neutral-green{background-color:#5DD255}.owa-bg-color-cal-green{background-color:#68A490}.owa-bg-color-cal-purple{background-color:#976CBE}.owa-border-color-neutral-orange{border-color:#D82300}.owa-border-color-neutral-red{border-color:#A80F22}.owa-border-color-neutral-yellow{border-color:#FFEE94}.owa-border-color-neutral-green{border-color:#5DD255}.owa-border-color-cal-green{border-color:#68A490}.owa-border-color-cal-purple{border-color:#976CBE}.owa-color-neutral-darkBlue{color:#00008B}.owa-color-neutral-orange{color:#D82300}.owa-color-neutral-red{color:#A80F22}.owa-color-neutral-yellow{color:#FFEE94}.owa-color-neutral-green{color:#5DD255}.owa-color-neutral-green-alt,.owa-color-neutral-green-alt:before{color:#107c10}.owa-color-cal-green{color:#68A490}.owa-color-cal-green-hover{color:#377353}.owa-color-cal-purple{color:#976CBE}.owa-color-cal-purple-hover{color:#67397B}.owa-color-cal-blue{color:#71C2EB}.owa-color-cal-brown{color:#AB9B81}.owa-color-cal-green-alt{color:#A9C47A}.owa-color-cal-grey{color:#999B9C}.owa-color-cal-orange{color:#E6975C}.owa-color-cal-pink{color:#CA6AAB}.owa-color-cal-red{color:#D57272}.owa-color-cal-teal{color:#7BCBC4}.owa-color-cal-yellow{color:#E3B75D}.owa-color-folder-brown{color:#EAC282}.ms-font-color-red{color:#E81123}.ms-font-color-redDark{color:#A80000}</style>\r\n\r\n    \r\n        <script>\r\n         var HeaderImageTemplate = \".o365cs-topnavBGImage{background:url(\'prem/16.1320.14.2056223/resources/themes/@theme/images/0/headerbgmaing2.png\'),url(\'prem/16.1320.14.2056223/resources/themes/@theme/images/0/headerbgmaing2.gif\');width:1px;height:1px}\";\r\n        </script>\r\n        <style id=\"HeaderImages\"></style>\r\n    \r\n\r\n    <script>\r\n        \r\n        (function () {\r\n            if (\"-ms-user-select\" in document.documentElement.style && navigator.userAgent.match(/IEMobile\\/10\\.0/)) {\r\n                var msViewportStyle = document.createElement(\"style\");\r\n                msViewportStyle.appendChild(\r\n                    document.createTextNode(\"@-ms-viewport{width:auto!important}\")\r\n                    );\r\n                document.getElementsByTagName(\"head\")[0].appendChild(msViewportStyle);\r\n            }\r\n        })();\r\n    </script>\r\n\r\n    <style>\r\n        body\r\n        {\r\n            width: 100%;\r\n            height: 100%;\r\n            margin: 0;\r\n            padding: 0;\r\n        }\r\n \r\n        #owaLoading\r\n        {\r\n            background-color: #FFF;\r\n            width: 100%;\r\n            height: 100%;\r\n            position: absolute;\r\n            z-index: 10001;\r\n        }\r\n        \r\n        #loadingLogo, #loadingSpinner, #statusText\r\n        {\r\n            display: block;\r\n            margin-left: auto;\r\n            margin-right: auto;\r\n            text-align: center;\r\n        }\r\n        \r\n        #loadingLogo\r\n        {\r\n            padding-top: 174px;\r\n            padding-bottom: 22px;\r\n        }\r\n        \r\n        .tnarrow #loadingLogo\r\n        {\r\n            padding-top: 52px;\r\n        }\r\n\r\n        #statusText\r\n        {\r\n            color: #0072c6;\r\n            font-family: \'wf_segoe-ui_normal\', \'Segoe UI\', \'Segoe WP\', Tahoma, Arial, sans-serif;\r\n            font-size: 12px;\r\n            margin-top: 20px;\r\n        }\r\n\r\n        #statusText > span\r\n        {\r\n            display: none;\r\n            margin-left: auto;\r\n            margin-right: auto;\r\n            line-height: 11px;\r\n        }\r\n\r\n        #statusText.script > .script\r\n        {\r\n            display: inline;\r\n        }\r\n\r\n        #statusText.scriptDelay > .scriptDelay\r\n        {\r\n            display: inline;\r\n        }\r\n\r\n        #statusText.data > .data\r\n        {\r\n            display: inline;\r\n        }\r\n\r\n        #statusText.dataDelay > .dataDelay\r\n        {\r\n            display: inline;\r\n        }\r\n\r\n        #statusText.render > .render\r\n        {\r\n            display: inline;\r\n        }\r\n    \r\n    </style>\r\n  </head>\r\n    <!--[if IE 8 ]> <body class=\"ie8 ms-fwt-r\"> <![endif]-->\r\n    <!--[if (gte IE 9)|!(IE)]><!--> <body class=\"notIE8 ms-fwt-r\"> <!--<![endif]-->\r\n    \r\n    <div id=\"owaLoading\">\r\n      <img id=\"loadingLogo\" alt=\"Outlook\" src=\'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABMCAYAAADX/oqbAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMC41ZYUyZQAAHcBJREFUeF7tXQmYJEWZ7eFwUVSgu7Kqm50FcRUFr3Vl1fXAFWRxEVFBTrnVWQRm6MqsHmbwaA8OQZdLBcZFWJHPxXGXc6a7Iqvb5mYXBuRmOEVBzgEc2BEZR3rfi4yuroyIrsqsrO6u7o73ff8305V/RPwRGfHyj7uj3bHFcSNbdgfD23b3hjt5frh7vhh+sGP56MbqsYODg8P0geTklSr7eIE4CXJV3g9vzfviAc8Xa/Dvn0Fay96ycOCvlLqDg4PD5IFks8WSFVt1Lh6Y3+WPvL3gV/b0gvDb+UCszAfh8/h3PYhpA4jqL/h7VBdHWA4ODpOG/KIrCrlA7JwvhV/2gsqJ8Jouhrc0gn8fBim9YiOleuIIy8HBoXmMjs7r2Hf5xh0LVm26Y//dr+kOhnYEsRwFUjoXpHQnvKdn8Pda/PsneE5WrymNOMJycHBIjJ4FV74u51e25+A3PKS9vFJYIjmBTFaBTNClsxNNq8QRloODw4QoBGI7ENSe+UB8DQQ1gH9vA0E9Co/pWXhMqbt0WcURloPDXEb/8tdsvaTS1bl4ZH6hd/AduUD8C4jhGyCllSCHR/D/DbIr54tXa4ljusQRloPDHAY8pUtABNfh3/sg63SCaDdxhOXgMIdhI4V2FkdYDg5zGDZSaGdxhOXgMIdhI4V2FkdYDg5zGDZSaGdxhOXgMIdhI4V2FkdYDq1GLgg/xTWFtSIXRDskQufCgTfq5ef5lY+ox62FjRTaWRxhObQaaGDPe754uVY6+kc2UY8dGqAQiHfq5QcZUY9bCxsptLOMEdb84o2v9UqVQ/Ml4U+leEF4CNetqeKbcmztl//GK4kP54vic7DlWNPGsBdyEBrhrrm+8K1y25RDXXiB+KNezzoWLNtUPXZogHxp4N16+UFuUI9bC0tCbS1jhNXjj+Tw/7tsOpMpqNwv49+r8FXZXBXhlMALBg8p+OJykNRvIM/mA/HSBHszX+UXjl4DdB73SuEwyO1gbqlSUTlocISVDY6w6sh0E5aSF+jBqCKcHPT3b9TVF25d8CsLkc9HQD7Zdhr44rfcXtWzqLKNG5+JwxFWNjjCqiPtQVjiEnhYeVWELQfzly+GAdK63Uw7k2yA7fcW/PKRHUhDJTfn4QgrGxxh1ZHpJCzZ1QrCb2x72MhmqvhaDh4JjS7cOfCI/qynr8l6kA+7hX+AXWvQTXyO/4e82Cgsu7XoXp6x1fGVLVSyMxij87r6rn9DrveaHpTB3hzbVA8SwxFWNjjCqiPTRVhoDM/k/MqR3Cyuiq7lKBw/+CaQ0NUgnHrdv9tzvrigUBSLcsVwD04fc9N6vjT0bv4/75d3kwPvgTgHDfFO6MOrssTjc1N75SKWo0p+RmHb/pHNkP+PIR8ngLAryNNa1Iebt1ly3VZKJTEcYWWDI6w6Mh2EhQZxD7po7+K4kiq2lsPrH3k9SOY6kJX9kEM/fDQXhPvlFw0V2FhVsAmxI4iV3VZ0//ZHg7x3gjj/AiK+rLN/4I0q2IxBl19+O/LwIKRKyI6wpgeOsOrIFBMWG4PI91berIpLgjf49KDBqD8zg10zkOLlWtpS8Ps6NKifeEev6FbqqcFlGIjnXJDhS5Y0NuTgpcy05Q9q7Q9Ps63mxRHW9MARVh2ZKsJC3P+HitzHVbyqqDo69h3d2Asqffj94XxfuJf6NRtAFOzWQIzDEGkDu3dJPKqG6B/ZJCe9rfB3ejokMq8v3Fdpzgg4wmofzEDCEq+iwXGw9zH8TTf9Qfz9EP5+Uja6IGzZ4X+Ib5IJS3Ad0xo07iM7+kerXUDv6JHXo2IvJbHIPLWIsDoXhzsg3md1O5hGAV3AWhsyY3R0HvK3AO/lRT09viuQ83yl2fZwhNU+mFGEhZfNNUk/QgPf0yuNjHdb4Dnk/JXbs9FB7yo0iMwXUFAmlbA42O2La71e8WGVCwmOlyCt5Wjs66lHMmkFYaHRbY44y7od8IL+hMa4mONQSrV1QEP0/PISPU3IBrzLk3iZiNJsazjCah/MGMJCw7qvs1h+PwlERWdBNO1cCIa+iArGZQHWuJLKJBLWq+h+DUekO76wkuNXeHa7JDOl2yrCyvmV/a1l4ofD2x524aQtnZAD/PJ8fj1dsTr20WljOMJqH8wMwvLDX9PzUNE0Rv/IJqhgx4DkMh3DPCmEJbtI4nu1xEvvBrbujTR4rn1MvxWEJfdCBqEx0I64n871Df29UpsszPNK5X2QZ8sgvFigdNoaM4GwOPbIm8y5p5ONGp7thzjbzL9zvYM9k7merxbzi794beG4wTcxXa9Yfg/tYPmN2VHf4WiMmUBYD/GqLxVFcoAESAyW+BILKmVrCcsPH8uXxOf0ge18UP4GiOwPtjCtICxZeUBORvx++NWpmLHj7dt4F7cZ6Qfhg0rFgBcMgsDLp8PuqnCZhXqcGtzfiPyeUBufVxTffGOx3KlUJNjAYjoQvJuLDKLxwye56FbXHRMeI2MbE2wtYY3OwwfnoyCEM5Hm/8DOB0Csv0GcT6C8X8Dfv+ffkHth7zX4+7vQ/YAK3DqgDiHeXRD/D/Bh/F+k/6C0wxdPSjuC8PHIjvAe/HZ1IaiciDb9XhU6FVpFWIVesQts+n7tO6PgnZ7C4SWpZEmovvjhK1zbU9ttItgA6Bl0B+KwfGnoYDK4lblRYVBAt1rjTiDIQKsIi128VXJ9VU1e2CXCCz4PNk445oZ0MxMW0jjCiDcQT+X6Bifbu6qCM56ovOaESKnyj0olBlZ+WS7RWJ+U7sXhDupxaswHMSGOB2rjQ/qP0StRKhKwkRMF4zrjunG7KTY9JWiUJ9mOjWkFYXFpilzzxjV73GnANLU4rcK1cDyOJQh/Ru8n68JktVPiYJTZ/antCEKWw4Vboe2mGctsBWGxHsn2XPO+lLzIelpto5aE6goK98YtNLe7sHj4HYh4kGQ2riteQEU4s8Pi9nqlyj4onD/VxptU',2,8,'2016-07-22 14:12:25','2016-07-26 05:26:16','1'),(2,'Test','3','Just testing',2,2,'2016-07-26 10:46:26','2016-07-26 10:47:10','2'),(3,'Birthday wishes ','1','<p>happy birthday dear customer gh sjahasdg asdgha asjkas dadasd ahdj</p>\r\n',2,2,'2016-08-01 18:13:18','2016-08-10 17:37:51','1'),(4,'Email Verification','3','Hallo %FIRST_NAME% %LAST_NAME% %USERNAME%,\r\n\r\nKlick den folgenden Link, um deine Registrierung abzuschließen:\r\n%VERIFICATION_LINK%\r\n\r\nMit freundlichen Grüßen\r\nDas Lieferzonas-Team',2,2,'2016-12-02 11:09:03','2016-12-02 11:09:03','1'),(5,'New Account from Order','3','Hallo %EMAIL%, hier ist dein Passwort: %PASSWORD%',2,2,'2016-12-31 09:52:18','2016-12-31 09:52:20','1');
/*!40000 ALTER TABLE `lie_email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_email_types`
--

DROP TABLE IF EXISTS `lie_email_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_email_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_email_types`
--

LOCK TABLES `lie_email_types` WRITE;
/*!40000 ALTER TABLE `lie_email_types` DISABLE KEYS */;
INSERT INTO `lie_email_types` VALUES (1,'Owner','This is Owner email type',2,2,2016,2016,'1'),(2,'Reception','This is a reception type',2,2,2016,2016,'1'),(3,'personal','sd asd asd asd asdas das das',2,2,2016,2016,'1'),(4,'dasdasdasda','asdasdad',2,2,2016,2016,'2');
/*!40000 ALTER TABLE `lie_email_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_extra_choice`
--

DROP TABLE IF EXISTS `lie_extra_choice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_extra_choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `type` enum('single-choice','multiple-choice') NOT NULL,
  `name` varchar(150) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `extra_choice_name_unique_index` (`name`,`restaurant_id`),
  KEY `restaurant_id` (`restaurant_id`),
  CONSTRAINT `lie_extra_choice_ibfk_1` FOREIGN KEY (`restaurant_id`) REFERENCES `lie_rest_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_extra_choice`
--

LOCK TABLES `lie_extra_choice` WRITE;
/*!40000 ALTER TABLE `lie_extra_choice` DISABLE KEYS */;
INSERT INTO `lie_extra_choice` VALUES (2,15,'multiple-choice','Pizzaauflagen',0,0,'2016-11-13 20:22:25','2016-11-13 20:22:25','1'),(3,15,'single-choice','Menü-Getränk',0,0,'2016-11-13 20:24:18','2016-11-13 20:24:18','1');
/*!40000 ALTER TABLE `lie_extra_choice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_extra_choice_element`
--

DROP TABLE IF EXISTS `lie_extra_choice_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_extra_choice_element` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extra_choice_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`extra_choice_id`),
  KEY `extra_choice_id` (`extra_choice_id`),
  CONSTRAINT `lie_extra_choice_element_ibfk_1` FOREIGN KEY (`extra_choice_id`) REFERENCES `lie_extra_choice` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_extra_choice_element`
--

LOCK TABLES `lie_extra_choice_element` WRITE;
/*!40000 ALTER TABLE `lie_extra_choice_element` DISABLE KEYS */;
INSERT INTO `lie_extra_choice_element` VALUES (2,2,'Mais',0.50,0,0,'2016-11-13 20:22:25','2016-11-13 20:22:25','1'),(3,2,'Pfefferoni',0.50,0,0,'2016-11-13 20:22:25','2016-11-13 20:22:25','1'),(4,3,'Coca Cola',0.00,0,0,'2016-11-13 20:24:18','2016-11-13 20:24:18','1'),(5,3,'Sprite',0.00,0,0,'2016-11-13 20:24:18','2016-11-13 20:24:18','1'),(6,3,'Eistee Zitrone',0.00,0,0,'2016-11-13 20:24:18','2016-11-13 20:24:18','1');
/*!40000 ALTER TABLE `lie_extra_choice_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_faq_categories`
--

DROP TABLE IF EXISTS `lie_faq_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_faq_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_faq_categories`
--

LOCK TABLES `lie_faq_categories` WRITE;
/*!40000 ALTER TABLE `lie_faq_categories` DISABLE KEYS */;
INSERT INTO `lie_faq_categories` VALUES (1,'hallo','kkkkkkkkkkkkkkkkkkk',0,0,'2016-07-22 08:00:43','2016-07-22 08:00:43','1');
/*!40000 ALTER TABLE `lie_faq_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_faq_questions`
--

DROP TABLE IF EXISTS `lie_faq_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_faq_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_faq_questions`
--

LOCK TABLES `lie_faq_questions` WRITE;
/*!40000 ALTER TABLE `lie_faq_questions` DISABLE KEYS */;
INSERT INTO `lie_faq_questions` VALUES (1,1,'ja genau','llllllllllllllllllllllllllll',0,0,'2016-07-22 08:01:03','2016-07-22 08:01:03','1'),(2,1,'jfjfjfjfj','answerfggh',0,0,'2016-07-22 16:22:47','2016-07-22 16:22:47','1');
/*!40000 ALTER TABLE `lie_faq_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_city_maps`
--

DROP TABLE IF EXISTS `lie_front_city_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_city_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_city_maps`
--

LOCK TABLES `lie_front_city_maps` WRITE;
/*!40000 ALTER TABLE `lie_front_city_maps` DISABLE KEYS */;
INSERT INTO `lie_front_city_maps` VALUES (1,11,1,0,0,0,2016,'1'),(2,14,2,0,0,0,2016,'1'),(3,15,3,0,0,0,2016,'1'),(4,16,4,0,0,0,2016,'1'),(5,17,5,0,0,0,2016,'1'),(6,19,6,0,0,0,2016,'1'),(7,20,7,0,0,0,2016,'1'),(8,21,8,0,0,0,2016,'1'),(9,22,9,0,0,0,2016,'1'),(10,23,10,0,0,0,2016,'1');
/*!40000 ALTER TABLE `lie_front_city_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_color_maps`
--

DROP TABLE IF EXISTS `lie_front_color_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_color_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `header_color` int(11) NOT NULL,
  `background_color` int(11) NOT NULL,
  `expire_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_color_maps`
--

LOCK TABLES `lie_front_color_maps` WRITE;
/*!40000 ALTER TABLE `lie_front_color_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_front_color_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_country_maps`
--

DROP TABLE IF EXISTS `lie_front_country_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_country_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_country_maps`
--

LOCK TABLES `lie_front_country_maps` WRITE;
/*!40000 ALTER TABLE `lie_front_country_maps` DISABLE KEYS */;
INSERT INTO `lie_front_country_maps` VALUES (1,1,1,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(2,2,2,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(3,3,3,0,0,'0000-00-00 00:00:00','2016-08-08 19:36:18','1'),(4,4,4,0,0,'0000-00-00 00:00:00','2016-07-26 18:50:56','1'),(5,5,5,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(6,6,6,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(7,7,7,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(8,8,8,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(9,9,9,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(10,10,10,0,0,'0000-00-00 00:00:00','2016-07-19 17:08:54','1');
/*!40000 ALTER TABLE `lie_front_country_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_kitchen_maps`
--

DROP TABLE IF EXISTS `lie_front_kitchen_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_kitchen_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kitchen_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_kitchen_maps`
--

LOCK TABLES `lie_front_kitchen_maps` WRITE;
/*!40000 ALTER TABLE `lie_front_kitchen_maps` DISABLE KEYS */;
INSERT INTO `lie_front_kitchen_maps` VALUES (1,1,1,0,0,'0000-00-00 00:00:00','2016-08-01 15:15:12','1'),(2,2,2,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(3,3,3,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(4,4,4,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(5,5,5,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(6,6,6,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(7,7,7,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(8,8,8,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(9,9,9,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(10,10,10,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1');
/*!40000 ALTER TABLE `lie_front_kitchen_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_order_status_maps`
--

DROP TABLE IF EXISTS `lie_front_order_status_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_order_status_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(50) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=332 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_order_status_maps`
--

LOCK TABLES `lie_front_order_status_maps` WRITE;
/*!40000 ALTER TABLE `lie_front_order_status_maps` DISABLE KEYS */;
INSERT INTO `lie_front_order_status_maps` VALUES (1,'363320470813OR2',1,4,4,'2016-07-20 19:08:13','2016-07-20 19:08:13','1'),(2,'363321323825YR2',1,3,3,'2016-07-21 04:38:25','2016-07-21 04:38:25','1'),(3,'363321394253KR2',1,4,4,'2016-07-21 11:42:53','2016-07-21 11:42:53','1'),(4,'363321394253KR2',2,2,2,'2016-07-21 11:47:30','2016-07-21 11:47:30','1'),(5,'363321430642MR2',1,5,5,'2016-07-21 15:06:42','2016-07-21 15:06:42','1'),(6,'363321432009HR2',1,4,4,'2016-07-21 15:20:09','2016-07-21 15:20:09','1'),(7,'363321432744YR2',1,4,4,'2016-07-21 15:27:44','2016-07-21 15:27:44','1'),(8,'363321432744YR2',2,2,2,'2016-07-21 15:29:02','2016-07-21 15:29:02','1'),(9,'363321432744YR2',5,2,2,'2016-07-21 15:51:46','2016-07-21 15:51:46','1'),(10,'363321430642MR2',3,2,2,'2016-07-21 15:54:27','2016-07-21 15:54:27','1'),(11,'363321435640NR2',1,5,5,'2016-07-21 15:56:40','2016-07-21 15:56:40','1'),(12,'363321435640NR2',2,2,2,'2016-07-21 15:58:45','2016-07-21 15:58:45','1'),(13,'363321435640NR2',5,2,2,'2016-07-21 16:00:50','2016-07-21 16:00:50','1'),(14,'363321452938OR2',1,3,3,'2016-07-21 17:29:38','2016-07-21 17:29:38','1'),(15,'363321452938OR2',2,2,2,'2016-07-21 17:30:22','2016-07-21 17:30:22','1'),(16,'363321452938OR2',5,2,2,'2016-07-21 17:31:05','2016-07-21 17:31:05','1'),(17,'363321452938OR2',6,2,2,'2016-07-21 17:31:34','2016-07-21 17:31:34','1'),(18,'363321432009HR2',2,2,2,'2016-07-21 17:35:00','2016-07-21 17:35:00','1'),(19,'363321432009HR2',5,2,2,'2016-07-21 17:35:03','2016-07-21 17:35:03','1'),(20,'363321432009HR2',6,2,2,'2016-07-21 17:35:06','2016-07-21 17:35:06','1'),(21,'363320334211LR2',1,2,2,'2016-07-22 02:44:26','2016-07-22 02:44:26','1'),(22,'363320334211LR2',2,2,2,'2016-07-22 02:44:29','2016-07-22 02:44:29','1'),(23,'363320334211LR2',5,2,2,'2016-07-22 02:44:34','2016-07-22 02:44:34','1'),(24,'363322382721TR2',1,4,4,'2016-07-22 10:27:21','2016-07-22 10:27:21','1'),(25,'363322433618FR2',1,3,3,'2016-07-22 15:36:18','2016-07-22 15:36:18','1'),(26,'363322433618FR2',2,2,2,'2016-07-22 15:36:48','2016-07-22 15:36:48','1'),(27,'363323321816ZR2',1,3,3,'2016-07-23 04:18:16','2016-07-23 04:18:16','1'),(28,'363323321816ZR2',2,2,2,'2016-07-23 04:19:42','2016-07-23 04:19:42','1'),(29,'363324323153TR2',1,3,3,'2016-07-24 04:31:53','2016-07-24 04:31:53','1'),(30,'363324323153TR2',2,2,2,'2016-07-24 04:32:14','2016-07-24 04:32:14','1'),(31,'363324323153TR2',5,2,2,'2016-07-24 04:32:18','2016-07-24 04:32:18','1'),(32,'363324323153TR2',6,2,2,'2016-07-24 04:32:20','2016-07-24 04:32:20','1'),(33,'363325404323IR2',1,5,5,'2016-07-25 12:43:23','2016-07-25 12:43:23','1'),(34,'363325404323IR2',2,2,2,'2016-07-25 18:45:02','2016-07-25 18:45:02','1'),(35,'363325404323IR2',5,2,2,'2016-07-25 18:45:34','2016-07-25 18:45:34','1'),(36,'363325404323IR2',6,2,2,'2016-07-25 18:45:47','2016-07-25 18:45:47','1'),(37,'363326323051YR2',1,3,3,'2016-07-26 04:30:51','2016-07-26 04:30:51','1'),(38,'363326323051YR2',2,2,2,'2016-07-26 04:36:40','2016-07-26 04:36:40','1'),(39,'363326323051YR2',5,2,2,'2016-07-26 04:40:07','2016-07-26 04:40:07','1'),(40,'363326332729OR2',1,3,3,'2016-07-26 05:27:29','2016-07-26 05:27:29','1'),(41,'363326332729OR2',3,2,2,'2016-07-27 04:48:19','2016-07-27 04:48:19','1'),(42,'363327325848WR2',1,3,3,'2016-07-27 04:58:48','2016-07-27 04:58:48','1'),(43,'363327330014ER2',1,3,3,'2016-07-27 05:00:14','2016-07-27 05:00:14','1'),(44,'363327330014ER2',2,2,2,'2016-07-27 05:01:01','2016-07-27 05:01:01','1'),(45,'363328330213JR2',1,3,3,'2016-07-28 05:02:13','2016-07-28 05:02:13','1'),(46,'363328405223OR2',1,5,5,'2016-07-28 12:52:23','2016-07-28 12:52:23','1'),(47,'363328405340VR2',1,5,5,'2016-07-28 12:53:40','2016-07-28 12:53:40','1'),(48,'363328405440ZR2',1,5,5,'2016-07-28 12:54:40','2016-07-28 12:54:40','1'),(49,'363328405609CR2',1,5,5,'2016-07-28 12:56:09','2016-07-28 12:56:09','1'),(50,'363328405845TR2',1,5,5,'2016-07-28 12:58:45','2016-07-28 12:58:45','1'),(51,'363328405923PR2',1,5,5,'2016-07-28 12:59:23','2016-07-28 12:59:23','1'),(52,'363328410724WR2',1,5,5,'2016-07-28 13:07:24','2016-07-28 13:07:24','1'),(53,'363328410822GR2',1,5,5,'2016-07-28 13:08:22','2016-07-28 13:08:22','1'),(54,'363328410910RR2',1,5,5,'2016-07-28 13:09:10','2016-07-28 13:09:10','1'),(55,'363328412251GR2',1,5,5,'2016-07-28 13:22:51','2016-07-28 13:22:51','1'),(56,'363328412315WR2',1,5,5,'2016-07-28 13:23:15','2016-07-28 13:23:15','1'),(57,'363328412518HR2',1,5,5,'2016-07-28 13:25:18','2016-07-28 13:25:18','1'),(58,'363328412704OR2',1,5,5,'2016-07-28 13:27:04','2016-07-28 13:27:04','1'),(59,'363328412728UR2',1,5,5,'2016-07-28 13:27:28','2016-07-28 13:27:28','1'),(60,'363328413125GR2',1,5,5,'2016-07-28 13:31:25','2016-07-28 13:31:25','1'),(61,'363328413244TR2',1,5,5,'2016-07-28 13:32:44','2016-07-28 13:32:44','1'),(62,'363328430604QR2',1,5,5,'2016-07-28 15:06:04','2016-07-28 15:06:04','1'),(63,'363328430640JR2',1,5,5,'2016-07-28 15:06:40','2016-07-28 15:06:40','1'),(64,'363328430800YR2',1,5,5,'2016-07-28 15:08:00','2016-07-28 15:08:00','1'),(65,'363328430800YR2',2,2,2,'2016-07-28 15:29:08','2016-07-28 15:29:08','1'),(66,'363328430800YR2',5,2,2,'2016-07-28 15:29:10','2016-07-28 15:29:10','1'),(67,'363328430800YR2',6,2,2,'2016-07-28 15:29:13','2016-07-28 15:29:13','1'),(68,'363328430640JR2',2,2,2,'2016-07-28 15:29:29','2016-07-28 15:29:29','1'),(69,'363328430640JR2',5,2,2,'2016-07-28 15:29:32','2016-07-28 15:29:32','1'),(70,'363328430640JR2',6,2,2,'2016-07-28 15:29:35','2016-07-28 15:29:35','1'),(71,'363328430604QR2',2,2,2,'2016-07-28 15:29:42','2016-07-28 15:29:42','1'),(72,'363328430604QR2',5,2,2,'2016-07-28 15:29:44','2016-07-28 15:29:44','1'),(73,'363328430604QR2',6,2,2,'2016-07-28 15:29:47','2016-07-28 15:29:47','1'),(74,'363328413244TR2',2,2,2,'2016-07-28 15:30:13','2016-07-28 15:30:13','1'),(75,'363328413244TR2',5,2,2,'2016-07-28 15:30:16','2016-07-28 15:30:16','1'),(76,'363328413244TR2',6,2,2,'2016-07-28 15:30:19','2016-07-28 15:30:19','1'),(77,'363328413125GR2',2,2,2,'2016-07-28 15:30:55','2016-07-28 15:30:55','1'),(78,'363328413125GR2',5,2,2,'2016-07-28 15:31:07','2016-07-28 15:31:07','1'),(79,'363328413125GR2',6,2,2,'2016-07-28 15:31:12','2016-07-28 15:31:12','1'),(80,'363328412728UR2',2,2,2,'2016-07-28 15:31:36','2016-07-28 15:31:36','1'),(81,'363328412728UR2',5,2,2,'2016-07-28 15:31:39','2016-07-28 15:31:39','1'),(82,'363328412728UR2',6,2,2,'2016-07-28 15:31:41','2016-07-28 15:31:41','1'),(83,'363328412704OR2',2,2,2,'2016-07-28 15:32:08','2016-07-28 15:32:08','1'),(84,'363328412704OR2',5,2,2,'2016-07-28 15:32:10','2016-07-28 15:32:10','1'),(85,'363328412704OR2',6,2,2,'2016-07-28 15:32:13','2016-07-28 15:32:13','1'),(86,'363328412518HR2',2,2,2,'2016-07-28 15:32:24','2016-07-28 15:32:24','1'),(87,'363328412518HR2',5,2,2,'2016-07-28 15:32:26','2016-07-28 15:32:26','1'),(88,'363328412518HR2',6,2,2,'2016-07-28 15:32:29','2016-07-28 15:32:29','1'),(89,'363328412315WR2',2,2,2,'2016-07-28 15:32:41','2016-07-28 15:32:41','1'),(90,'363328412315WR2',5,2,2,'2016-07-28 15:32:44','2016-07-28 15:32:44','1'),(91,'363328412315WR2',6,2,2,'2016-07-28 15:32:46','2016-07-28 15:32:46','1'),(92,'363328412251GR2',2,2,2,'2016-07-28 15:32:55','2016-07-28 15:32:55','1'),(93,'363328412251GR2',5,2,2,'2016-07-28 15:32:58','2016-07-28 15:32:58','1'),(94,'363328412251GR2',6,2,2,'2016-07-28 15:33:00','2016-07-28 15:33:00','1'),(95,'363328410910RR2',2,2,2,'2016-07-28 15:33:11','2016-07-28 15:33:11','1'),(96,'363328410910RR2',5,2,2,'2016-07-28 15:33:14','2016-07-28 15:33:14','1'),(97,'363328410910RR2',6,2,2,'2016-07-28 15:33:17','2016-07-28 15:33:17','1'),(98,'363329332536ZR1',1,3,3,'2016-07-29 05:25:36','2016-07-29 05:25:36','1'),(99,'363329391405AR1',1,4,4,'2016-07-29 11:14:05','2016-07-29 11:14:05','1'),(100,'363329392718TR1',1,4,4,'2016-07-29 11:27:18','2016-07-29 11:27:18','1'),(101,'363329393802CR1',1,4,4,'2016-07-29 11:38:02','2016-07-29 11:38:02','1'),(102,'363329393927DR1',1,4,4,'2016-07-29 11:39:27','2016-07-29 11:39:27','1'),(103,'363329394045NR1',1,4,4,'2016-07-29 11:40:45','2016-07-29 11:40:45','1'),(104,'363401442359PR1',1,4,4,'2016-08-01 16:23:59','2016-08-01 16:23:59','1'),(105,'363401442620AR1',1,4,4,'2016-08-01 16:26:20','2016-08-01 16:26:20','1'),(106,'363401494705CR1',1,3,3,'2016-08-01 21:47:05','2016-08-01 21:47:05','1'),(107,'363401494705CR1',2,1,1,'2016-08-01 21:49:14','2016-08-01 21:49:14','1'),(108,'363401494705CR1',5,1,1,'2016-08-01 21:49:47','2016-08-01 21:49:47','1'),(109,'363401494705CR1',6,1,1,'2016-08-01 21:50:00','2016-08-01 21:50:00','1'),(111,'363401503850ZR1',1,3,3,'2016-08-01 22:38:50','2016-08-01 22:38:50','1'),(112,'363402315221LR1',1,3,3,'2016-08-02 03:52:21','2016-08-02 03:52:21','1'),(113,'363402315221LR1',2,1,1,'2016-08-03 07:30:21','2016-08-03 07:30:21','1'),(114,'363402315221LR1',5,1,1,'2016-08-03 07:30:25','2016-08-03 07:30:25','1'),(115,'363402315221LR1',6,1,1,'2016-08-03 07:30:28','2016-08-03 07:30:28','1'),(116,'363405334239SR1',1,3,3,'2016-08-05 05:42:39','2016-08-05 05:42:39','1'),(117,'363405334239SR1',2,1,1,'2016-08-05 05:45:02','2016-08-05 05:45:02','1'),(118,'363401503850ZR1',2,1,1,'2016-08-09 05:39:48','2016-08-09 05:39:48','1'),(119,'363401503850ZR1',5,1,1,'2016-08-09 05:39:52','2016-08-09 05:39:52','1'),(120,'363401503850ZR1',6,1,1,'2016-08-09 05:39:55','2016-08-09 05:39:55','1'),(121,'363401442620AR1',3,1,1,'2016-08-09 05:40:39','2016-08-09 05:40:39','1'),(122,'363409340207FR1',1,3,3,'2016-08-09 06:02:07','2016-08-09 06:02:07','1'),(123,'363409340207FR1',2,1,1,'2016-08-09 06:02:52','2016-08-09 06:02:52','1'),(124,'363409340207FR1',5,1,1,'2016-08-09 06:08:14','2016-08-09 06:08:14','1'),(125,'363409340207FR1',6,1,1,'2016-08-09 06:08:17','2016-08-09 06:08:17','1'),(126,'363412362605OR1',1,3,3,'2016-08-12 08:26:05','2016-08-12 08:26:05','1'),(127,'363412362905AR1',1,3,3,'2016-08-12 08:29:05','2016-08-12 08:29:05','1'),(128,'363412362905AR1',2,1,1,'2016-08-12 08:29:27','2016-08-12 08:29:27','1'),(129,'363412362905AR1',5,1,1,'2016-08-12 08:29:39','2016-08-12 08:29:39','1'),(130,'363412362905AR1',6,1,1,'2016-08-12 08:29:41','2016-08-12 08:29:41','1'),(131,'363412362605OR1',2,1,1,'2016-08-19 04:40:35','2016-08-19 04:40:35','1'),(132,'363412362605OR1',5,1,1,'2016-08-19 04:40:37','2016-08-19 04:40:37','1'),(133,'363412362605OR1',6,1,1,'2016-08-19 04:40:40','2016-08-19 04:40:40','1'),(134,'363329332536ZR1',3,1,1,'2016-08-19 04:41:05','2016-08-19 04:41:05','1'),(136,'363422445634XR17',1,11,11,'2016-08-22 16:56:34','2016-08-22 16:56:34','1'),(137,'363422445647VR17',1,11,11,'2016-08-22 16:56:47','2016-08-22 16:56:47','1'),(138,'363422451004YR17',1,11,11,'2016-08-22 17:10:04','2016-08-22 17:10:04','1'),(139,'363422451004YR17',2,16,16,'2016-08-22 17:16:55','2016-08-22 17:16:55','1'),(140,'363422451004YR17',5,16,16,'2016-08-22 17:17:16','2016-08-22 17:17:16','1'),(141,'363422445647VR17',3,16,16,'2016-08-22 17:41:51','2016-08-22 17:41:51','1'),(142,'363422451004YR17',6,16,16,'2016-08-22 18:20:41','2016-08-22 18:20:41','1'),(143,'363423332354TR17',1,3,3,'2016-08-23 05:23:54','2016-08-23 05:23:54','1'),(144,'363423394332HR17',1,4,4,'2016-08-23 11:43:32','2016-08-23 11:43:32','1'),(145,'363423394332HR17',2,16,16,'2016-08-23 11:46:30','2016-08-23 11:46:30','1'),(146,'363423394738GR17',1,4,4,'2016-08-23 11:47:38','2016-08-23 11:47:38','1'),(147,'363423394738GR17',2,16,16,'2016-08-23 11:48:22','2016-08-23 11:48:22','1'),(148,'363423401236DR17',1,4,4,'2016-08-23 12:12:36','2016-08-23 12:12:36','1'),(149,'363423401236DR17',2,16,16,'2016-08-23 12:12:52','2016-08-23 12:12:52','1'),(150,'363423412759TR17',1,11,11,'2016-08-23 13:27:59','2016-08-23 13:27:59','1'),(151,'363423443310ZR17',1,4,4,'2016-08-23 16:33:10','2016-08-23 16:33:10','1'),(152,'363423443310ZR17',2,16,16,'2016-08-23 16:33:35','2016-08-23 16:33:35','1'),(153,'363423444601GR17',1,19,19,'2016-08-23 16:46:01','2016-08-23 16:46:01','1'),(154,'363423444601GR17',2,16,16,'2016-08-23 16:47:45','2016-08-23 16:47:45','1'),(155,'363423394738GR17',5,16,16,'2016-08-23 17:33:08','2016-08-23 17:33:08','1'),(156,'363423444601GR17',5,16,16,'2016-08-23 17:34:11','2016-08-23 17:34:11','1'),(157,'363423444601GR17',6,16,16,'2016-08-23 17:34:21','2016-08-23 17:34:21','1'),(158,'363423332354TR17',2,16,16,'2016-08-23 17:50:11','2016-08-23 17:50:11','1'),(159,'363424330338TR15',1,3,3,'2016-08-24 05:03:38','2016-08-24 05:03:38','1'),(160,'363424330338TR15',2,15,15,'2016-08-24 05:07:51','2016-08-24 05:07:51','1'),(161,'363424330338TR15',5,15,15,'2016-08-24 05:13:30','2016-08-24 05:13:30','1'),(162,'363424330338TR15',6,15,15,'2016-08-24 05:13:46','2016-08-24 05:13:46','1'),(163,'363401442359PR1',2,1,1,'2016-08-25 11:41:47','2016-08-25 11:41:47','1'),(164,'363401442359PR1',5,1,1,'2016-08-25 11:42:11','2016-08-25 11:42:11','1'),(165,'363329394045NR1',2,1,1,'2016-08-25 12:14:27','2016-08-25 12:14:27','1'),(166,'363329394045NR1',5,1,1,'2016-08-25 12:14:30','2016-08-25 12:14:30','1'),(167,'363329392718TR1',2,1,1,'2016-08-25 13:03:25','2016-08-25 13:03:25','1'),(168,'363329392718TR1',5,1,1,'2016-08-25 13:05:00','2016-08-25 13:05:00','1'),(171,'363329392718TR1',6,1,1,'2016-08-25 13:07:49','2016-08-25 13:07:49','1'),(172,'363329392718TR1',7,1,1,'2016-08-25 13:07:59','2016-08-25 13:07:59','1'),(173,'363329393802CR1',2,1,1,'2016-08-25 13:10:46','2016-08-25 13:10:46','1'),(174,'363425413334YR15',1,3,3,'2016-08-25 13:33:34','2016-08-25 13:33:34','1'),(175,'363412362605OR1',7,1,1,'2016-08-25 13:36:39','2016-08-25 13:36:39','1'),(176,'363425413334YR15',2,15,15,'2016-08-25 13:36:43','2016-08-25 13:36:43','1'),(177,'363425413334YR15',5,15,15,'2016-08-25 13:37:14','2016-08-25 13:37:14','1'),(178,'363425413334YR15',6,15,15,'2016-08-25 13:37:20','2016-08-25 13:37:20','1'),(179,'363425431020YR15',1,3,3,'2016-08-25 15:10:20','2016-08-25 15:10:20','1'),(180,'363426324658FR15',1,3,3,'2016-08-26 04:46:58','2016-08-26 04:46:58','1'),(181,'363426324658FR15',2,15,15,'2016-08-26 05:09:14','2016-08-26 05:09:14','1'),(182,'363426332145SR15',1,3,3,'2016-08-26 05:21:45','2016-08-26 05:21:45','1'),(183,'363426332145SR15',2,15,15,'2016-08-26 05:22:04','2016-08-26 05:22:04','1'),(184,'363426332145SR15',5,15,15,'2016-08-26 05:22:08','2016-08-26 05:22:08','1'),(185,'363426332145SR15',6,15,15,'2016-08-26 05:22:13','2016-08-26 05:22:13','1'),(186,'363426332145SR15',7,15,15,'2016-08-26 05:22:46','2016-08-26 05:22:46','1'),(187,'363426405855DR17',1,11,11,'2016-08-26 12:58:55','2016-08-26 12:58:55','1'),(188,'363426405855DR17',2,16,16,'2016-08-26 13:06:20','2016-08-26 13:06:20','1'),(189,'363426405855DR17',5,16,16,'2016-08-26 18:05:25','2016-08-26 18:05:25','1'),(190,'363426405855DR17',6,16,16,'2016-08-26 18:05:40','2016-08-26 18:05:40','1'),(191,'363426405855DR17',7,16,16,'2016-08-26 18:05:54','2016-08-26 18:05:54','1'),(192,'363412362905AR1',7,1,1,'2016-08-27 12:25:29','2016-08-27 12:25:29','1'),(193,'363329393927DR1',2,1,1,'2016-08-27 12:31:06','2016-08-27 12:31:06','1'),(194,'363428322305JR15',1,3,3,'2016-08-28 04:23:05','2016-08-28 04:23:05','1'),(195,'363428322305JR15',2,15,15,'2016-08-28 05:50:27','2016-08-28 05:50:27','1'),(196,'363428322305JR15',5,15,15,'2016-08-28 05:50:42','2016-08-28 05:50:42','1'),(197,'363428322305JR15',6,15,15,'2016-08-28 05:50:45','2016-08-28 05:50:45','1'),(198,'363428322305JR15',7,15,15,'2016-08-28 05:50:51','2016-08-28 05:50:51','1'),(199,'363429341457RR15',1,3,3,'2016-08-29 06:14:57','2016-08-29 06:14:57','1'),(200,'363429341457RR15',3,15,15,'2016-08-29 06:17:45','2016-08-29 06:17:45','1'),(201,'363429345930IR15',1,3,3,'2016-08-29 06:59:30','2016-08-29 06:59:30','1'),(202,'363429350103YR15',1,3,3,'2016-08-29 07:01:03','2016-08-29 07:01:03','1'),(203,'363429350103YR15',2,15,15,'2016-08-29 07:01:50','2016-08-29 07:01:50','1'),(204,'363429350103YR15',5,15,15,'2016-08-29 07:01:54','2016-08-29 07:01:54','1'),(205,'363429350103YR15',6,15,15,'2016-08-29 07:01:57','2016-08-29 07:01:57','1'),(206,'363429350103YR15',7,15,15,'2016-08-29 07:02:04','2016-08-29 07:02:04','1'),(207,'363429345930IR15',2,15,15,'2016-08-29 07:02:39','2016-08-29 07:02:39','1'),(208,'363429345930IR15',5,15,15,'2016-08-29 07:02:42','2016-08-29 07:02:42','1'),(209,'363429345930IR15',6,15,15,'2016-08-29 07:02:46','2016-08-29 07:02:46','1'),(210,'363429345930IR15',7,15,15,'2016-08-29 07:02:48','2016-08-29 07:02:48','1'),(211,'363429404729GR17',1,11,11,'2016-08-29 12:47:29','2016-08-29 12:47:29','1'),(212,'363429404729GR17',3,16,16,'2016-08-29 12:59:18','2016-08-29 12:59:18','1'),(213,'363329391405AR1',2,1,1,'2016-08-30 15:15:13','2016-08-30 15:15:13','1'),(214,'363425431020YR15',2,15,15,'2016-08-31 00:25:11','2016-08-31 00:25:11','1'),(215,'363425431020YR15',5,15,15,'2016-08-31 00:25:14','2016-08-31 00:25:14','1'),(216,'363425431020YR15',6,15,15,'2016-08-31 00:25:15','2016-08-31 00:25:15','1'),(217,'363425431020YR15',7,15,15,'2016-08-31 00:25:22','2016-08-31 00:25:22','1'),(218,'363431413340KR17',1,11,11,'2016-08-31 13:33:40','2016-08-31 13:33:40','1'),(219,'363431413340KR17',4,11,0,'2016-08-31 13:42:54','2016-08-31 13:42:54','1'),(220,'363501341315ZR15',1,3,3,'2016-09-01 06:13:15','2016-09-01 06:13:15','1'),(221,'363501341315ZR15',2,15,15,'2016-09-01 06:13:50','2016-09-01 06:13:50','1'),(222,'363502310112ER15',1,3,3,'2016-09-02 03:01:12','2016-09-02 03:01:12','1'),(223,'363502310112ER15',2,15,15,'2016-09-02 03:01:40','2016-09-02 03:01:40','1'),(224,'363502310112ER15',5,15,15,'2016-09-02 03:04:28','2016-09-02 03:04:28','1'),(225,'363502310112ER15',6,15,15,'2016-09-02 03:07:23','2016-09-02 03:07:23','1'),(226,'363502310112ER15',7,15,15,'2016-09-02 03:07:32','2016-09-02 03:07:32','1'),(227,'363503333720CR15',1,3,3,'2016-09-03 05:37:20','2016-09-03 05:37:20','1'),(228,'363503333720CR15',2,15,15,'2016-09-03 05:38:04','2016-09-03 05:38:04','1'),(229,'363503333720CR15',5,15,15,'2016-09-03 05:49:38','2016-09-03 05:49:38','1'),(230,'363503333720CR15',6,15,15,'2016-09-03 05:49:41','2016-09-03 05:49:41','1'),(231,'363503333720CR15',7,15,15,'2016-09-03 05:49:44','2016-09-03 05:49:44','1'),(232,'363506393236OR5',1,5,5,'2016-09-06 11:32:36','2016-09-06 11:32:36','1'),(233,'363506393236OR5',2,4,4,'2016-09-06 11:37:33','2016-09-06 11:37:33','1'),(234,'363506393236OR5',5,4,4,'2016-09-06 11:39:06','2016-09-06 11:39:06','1'),(235,'363506393236OR5',6,4,4,'2016-09-06 11:48:48','2016-09-06 11:48:48','1'),(236,'363506393236OR5',7,4,4,'2016-09-06 11:49:13','2016-09-06 11:49:13','1'),(237,'363506403409SR5',1,5,5,'2016-09-06 12:34:09','2016-09-06 12:34:09','1'),(238,'363506403409SR5',2,4,4,'2016-09-06 12:34:48','2016-09-06 12:34:48','1'),(239,'363506403409SR5',5,4,4,'2016-09-06 12:35:03','2016-09-06 12:35:03','1'),(240,'363506403409SR5',6,4,4,'2016-09-06 12:35:06','2016-09-06 12:35:06','1'),(241,'363506403409SR5',7,4,4,'2016-09-06 12:35:09','2016-09-06 12:35:09','1'),(242,'363506440656WR5',1,5,5,'2016-09-06 16:06:56','2016-09-06 16:06:56','1'),(243,'363506440656WR5',2,4,4,'2016-09-06 16:07:25','2016-09-06 16:07:25','1'),(244,'363506440656WR5',5,4,4,'2016-09-06 16:07:42','2016-09-06 16:07:42','1'),(245,'363506440656WR5',6,4,4,'2016-09-06 16:07:45','2016-09-06 16:07:45','1'),(246,'363506440656WR5',7,4,4,'2016-09-06 16:07:51','2016-09-06 16:07:51','1'),(247,'363506462416VR1',1,5,5,'2016-09-06 18:24:16','2016-09-06 18:24:16','1'),(248,'363506462553NR5',1,5,5,'2016-09-06 18:25:53','2016-09-06 18:25:53','1'),(249,'363506462553NR5',2,4,4,'2016-09-06 18:26:39','2016-09-06 18:26:39','1'),(250,'363506462553NR5',5,4,4,'2016-09-06 18:27:05','2016-09-06 18:27:05','1'),(251,'363506462553NR5',6,4,4,'2016-09-06 18:27:11','2016-09-06 18:27:11','1'),(252,'363506462553NR5',7,4,4,'2016-09-06 18:48:15','2016-09-06 18:48:15','1'),(253,'363506465018ZR1',1,21,21,'2016-09-06 18:50:18','2016-09-06 18:50:18','1'),(254,'363506465037JR1',1,21,21,'2016-09-06 18:50:37','2016-09-06 18:50:37','1'),(255,'363507352047SR15',1,3,3,'2016-09-07 07:20:47','2016-09-07 07:20:47','1'),(256,'363507352047SR15',2,15,15,'2016-09-07 07:21:41','2016-09-07 07:21:41','1'),(257,'363507395820ZR5',1,5,5,'2016-09-07 11:58:20','2016-09-07 11:58:20','1'),(258,'363508412812KR5',1,1,1,'2016-09-08 13:28:12','2016-09-08 13:28:12','1'),(259,'363509442214OR5',1,5,5,'2016-09-09 16:22:14','2016-09-09 16:22:14','1'),(260,'363510301713AR15',1,3,3,'2016-09-10 02:17:13','2016-09-10 02:17:13','1'),(261,'363510301713AR15',2,15,15,'2016-09-10 02:17:36','2016-09-10 02:17:36','1'),(262,'363510301713AR15',5,15,15,'2016-09-10 02:17:41','2016-09-10 02:17:41','1'),(263,'363510301713AR15',6,15,15,'2016-09-10 02:17:45','2016-09-10 02:17:45','1'),(264,'363510434445YR17',1,11,11,'2016-09-10 15:44:45','2016-09-10 15:44:45','1'),(265,'363510435558WR17',1,11,11,'2016-09-10 15:55:58','2016-09-10 15:55:58','1'),(266,'363422451004YR17',7,16,16,'2016-09-10 16:56:58','2016-09-10 16:56:58','1'),(267,'363511334553FR15',1,3,3,'2016-09-11 05:45:53','2016-09-11 05:45:53','1'),(268,'363511334553FR15',2,15,15,'2016-09-11 05:48:21','2016-09-11 05:48:21','1'),(269,'363511334553FR15',5,15,15,'2016-09-11 05:48:24','2016-09-11 05:48:24','1'),(270,'363511334553FR15',6,15,15,'2016-09-11 05:53:20','2016-09-11 05:53:20','1'),(271,'363511334553FR15',7,15,15,'2016-09-11 05:54:09','2016-09-11 05:54:09','1'),(272,'363512413231UR17',1,4,4,'2016-09-12 13:32:31','2016-09-12 13:32:31','1'),(273,'363512413304WR15',1,11,11,'2016-09-12 13:33:04','2016-09-12 13:33:04','1'),(274,'363512413304WR15',2,15,15,'2016-09-12 15:07:40','2016-09-12 15:07:40','1'),(275,'363512413304WR15',5,15,15,'2016-09-12 15:07:46','2016-09-12 15:07:46','1'),(276,'363512441804FR15',1,3,3,'2016-09-12 16:18:04','2016-09-12 16:18:04','1'),(277,'363512441804FR15',2,15,15,'2016-09-12 16:18:44','2016-09-12 16:18:44','1'),(278,'363512441804FR15',5,15,15,'2016-09-12 16:18:47','2016-09-12 16:18:47','1'),(279,'363512441804FR15',6,15,15,'2016-09-12 16:18:50','2016-09-12 16:18:50','1'),(280,'363512441804FR15',7,15,15,'2016-09-12 16:18:52','2016-09-12 16:18:52','1'),(281,'363513310244LR15',1,3,3,'2016-09-13 03:02:44','2016-09-13 03:02:44','1'),(282,'363513310244LR15',4,3,0,'2016-09-13 08:03:32','2016-09-13 08:03:32','1'),(283,'363515325428FR15',1,3,3,'2016-09-15 04:54:28','2016-09-15 04:54:28','1'),(284,'363515325428FR15',2,15,15,'2016-09-15 04:56:34','2016-09-15 04:56:34','1'),(285,'363515325428FR15',5,15,15,'2016-09-15 04:56:38','2016-09-15 04:56:38','1'),(286,'363515325428FR15',6,15,15,'2016-09-15 05:19:02','2016-09-15 05:19:02','1'),(287,'363515325428FR15',7,15,15,'2016-09-15 05:19:06','2016-09-15 05:19:06','1'),(288,'363501341315ZR15',5,15,15,'2016-09-16 04:48:47','2016-09-16 04:48:47','1'),(289,'363501341315ZR15',6,15,15,'2016-09-16 04:48:50','2016-09-16 04:48:50','1'),(290,'363501341315ZR15',7,15,15,'2016-09-16 04:48:53','2016-09-16 04:48:53','1'),(291,'363518292750GR15',1,3,3,'2016-09-18 01:27:50','2016-09-18 01:27:50','1'),(292,'363518292750GR15',2,15,15,'2016-09-18 01:28:46','2016-09-18 01:28:46','1'),(293,'363518292750GR15',5,15,15,'2016-09-20 02:45:07','2016-09-20 02:45:07','1'),(294,'363518292750GR15',6,15,15,'2016-09-20 02:45:12','2016-09-20 02:45:12','1'),(295,'363518292750GR15',7,15,15,'2016-09-20 02:45:15','2016-09-20 02:45:15','1'),(296,'363520332906LR15',1,3,3,'2016-09-20 05:29:06','2016-09-20 05:29:06','1'),(297,'363520332906LR15',2,15,15,'2016-09-20 05:29:25','2016-09-20 05:29:25','1'),(298,'363520333555XR15',1,3,3,'2016-09-20 05:35:55','2016-09-20 05:35:55','1'),(299,'363520334810IR15',1,3,3,'2016-09-20 05:48:10','2016-09-20 05:48:10','1'),(300,'363520334810IR15',2,15,15,'2016-09-20 05:51:23','2016-09-20 05:51:23','1'),(301,'363520334810IR15',5,15,15,'2016-09-20 05:51:34','2016-09-20 05:51:34','1'),(302,'363520334810IR15',6,15,15,'2016-09-20 05:51:37','2016-09-20 05:51:37','1'),(303,'363520333555XR15',4,3,0,'2016-09-20 05:52:01','2016-09-20 05:52:01','1'),(304,'363524335113SR15',1,3,3,'2016-09-24 05:51:13','2016-09-24 05:51:13','1'),(305,'363524335113SR15',4,3,0,'2016-09-24 05:52:00','2016-09-24 05:52:00','1'),(306,'363524351237QR15',1,3,3,'2016-09-24 07:12:37','2016-09-24 07:12:37','1'),(307,'363524351237QR15',4,3,0,'2016-09-24 07:13:05','2016-09-24 07:13:05','1'),(308,'363604323156AR15',1,3,3,'2016-10-04 04:31:56','2016-10-04 04:31:56','1'),(309,'363604323156AR15',2,15,15,'2016-10-04 04:33:26','2016-10-04 04:33:26','1'),(310,'363604323156AR15',5,15,15,'2016-10-04 04:33:56','2016-10-04 04:33:56','1'),(311,'363604323156AR15',6,15,15,'2016-10-04 04:34:04','2016-10-04 04:34:04','1'),(312,'363604323156AR15',7,15,15,'2016-10-04 04:36:27','2016-10-04 04:36:27','1'),(313,'363720492649YR15',1,23,23,'2016-11-20 21:26:49','2016-11-20 21:26:49','1'),(314,'363720492649YR15',3,15,15,'2016-11-20 21:29:14','2016-11-20 21:29:14','1'),(315,'363720493052GR15',1,23,23,'2016-11-20 21:30:52','2016-11-20 21:30:52','1'),(316,'363720493052GR15',2,15,15,'2016-11-20 21:31:32','2016-11-20 21:31:32','1'),(317,'363726294709AR15',1,23,23,'2016-11-26 01:47:09','2016-11-26 01:47:09','1'),(318,'363726294709AR15',2,15,15,'2016-11-26 01:48:55','2016-11-26 01:48:55','1'),(319,'363726294709AR15',5,15,15,'2016-11-26 01:48:58','2016-11-26 01:48:58','1'),(320,'363726294709AR15',6,15,15,'2016-11-26 01:48:59','2016-11-26 01:48:59','1'),(321,'363726294709AR15',7,15,15,'2016-11-26 01:49:02','2016-11-26 01:49:02','1'),(323,'363828464612FR15',1,23,23,'2016-12-28 18:46:12','2016-12-28 18:46:12','1'),(324,'363828471509SR15',1,23,23,'2016-12-28 19:15:09','2016-12-28 19:15:09','1'),(325,'363828491818KR15',1,23,23,'2016-12-28 21:18:18','2016-12-28 21:18:18','1'),(326,'363830331021GR15',1,23,23,'2016-12-30 05:10:21','2016-12-30 05:10:21','1'),(327,'363831382659UR15',1,23,23,'2016-12-31 10:26:59','2016-12-31 10:26:59','1'),(328,'363831382659UR15',2,15,15,'2016-12-31 15:13:07','2016-12-31 15:13:07','1'),(329,'363831382659UR15',5,15,15,'2016-12-31 15:13:09','2016-12-31 15:13:09','1'),(330,'363831382659UR15',6,15,15,'2016-12-31 15:13:11','2016-12-31 15:13:11','1'),(331,'363831382659UR15',7,15,15,'2016-12-31 15:13:13','2016-12-31 15:13:13','1');
/*!40000 ALTER TABLE `lie_front_order_status_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_order_statuses`
--

DROP TABLE IF EXISTS `lie_front_order_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_order_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(255) NOT NULL,
  `status_desc` text NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_order_statuses`
--

LOCK TABLES `lie_front_order_statuses` WRITE;
/*!40000 ALTER TABLE `lie_front_order_statuses` DISABLE KEYS */;
INSERT INTO `lie_front_order_statuses` VALUES (1,'Placed','placed',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(2,'Accepted','accepted',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(3,'Rejected','rejected',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(4,'Cancelled','cancelled',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(5,'Cooking Started','cooking',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(6,'Out for Delivery','out',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(7,'Delivered','delivered',0,0,0,'2016-11-14 13:47:03','0000-00-00 00:00:00','1');
/*!40000 ALTER TABLE `lie_front_order_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_order_transactions`
--

DROP TABLE IF EXISTS `lie_front_order_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_order_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(100) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=->deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_order_transactions`
--

LOCK TABLES `lie_front_order_transactions` WRITE;
/*!40000 ALTER TABLE `lie_front_order_transactions` DISABLE KEYS */;
INSERT INTO `lie_front_order_transactions` VALUES (1,'363329394045NR1',4,'8614697726993343','2016-07-29 11:41:17',4,4,'2016-07-29 11:41:17','2016-07-29 11:41:17','1'),(2,'363401442359PR1',4,'8614700489284718','2016-08-01 16:25:27',4,4,'2016-08-01 16:25:27','2016-08-01 16:25:27','1'),(3,'363520334810IR15',3,'8814743308622245','2016-09-20 05:50:56',3,3,'2016-09-20 05:50:56','2016-09-20 05:50:56','1');
/*!40000 ALTER TABLE `lie_front_order_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_page_maps`
--

DROP TABLE IF EXISTS `lie_front_page_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_page_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_page_maps`
--

LOCK TABLES `lie_front_page_maps` WRITE;
/*!40000 ALTER TABLE `lie_front_page_maps` DISABLE KEYS */;
INSERT INTO `lie_front_page_maps` VALUES (1,1,'<p>Flat UI ColorsCHOOSE FORMATHEX - #1234EFHEX - 1234EFRGB - (255,255,255)RGBA - (12,12,12,1.0)</p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td><!--[if lt IE 9]></td>\r\n		</tr>\r\n		<tr>\r\n			<td> </td>\r\n			<td><script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script></td>\r\n		</tr>\r\n		<tr>\r\n			<td> </td>\r\n			<td><![endif]--></td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n			<td>favicon.ico&quot; /&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n			<td>img/apple-touch-icon-iphone.png&quot; /&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n			<td>img/apple-touch-icon-ipad.png&quot; /&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n			<td>img/apple-touch-icon-iphone4.png&quot; /&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n			<td>img/apple-touch-icon-ipad3.png&quot; /&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n			<td>//fonts.googleapis.com/css?family=Montserrat:400,700&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot; data-noprefix&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n			<td>app.8880e.css&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td><a href=\"&lt;a href=\">//usepanda.com</a>&quot; class=&quot;top-link&quot; target=&quot;_blank&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Daily inspiration</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td><a href=\"&lt;a href=\">http://panda.jobs?ref=flatuicolors</a>&quot; target=&quot;_blank&quot;&gt;PANDA JOBS</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td><a href=\"&lt;a href=\">https://twitter.com/share</a>&quot; class=&quot;twitter-share-button&quot; data-url=&quot;http://flatuicolors.com/&quot; data-text=&quot;Flat UI Color Picker&quot; data-via=&quot;ahmetsulek&quot; data-related=&quot;ahmetsulek&quot; data-hashtags=&quot;flatui&quot;&gt;Tweet</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FFlatUiColors&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=536182929745005&quot; scrolling=&quot;no&quot; frameborder=&quot;0&quot; style=&quot;border:none; overflow:hidden; width:100px; height:21px;&quot; allowTransparency=&quot;true&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>\r\n			<p>COPIED!</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>TURQUOISE</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>EMERALD</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>PETER RIVER</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>AMETHYST</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>WET ASPHALT</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>GREEN SEA</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>NEPHRITIS</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>BELIZE HOLE</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>WISTERIA</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>MIDNIGHT BLUE</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>SUN FLOWER</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>CARROT</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>ALIZARIN</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>CLOUDS</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>CONCRETE</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>ORANGE</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>PUMPKIN</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>POMEGRANATE</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>SILVER</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>ASBESTOS</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,0,'2016-05-16 07:46:10','2016-07-26 05:18:53','0'),(2,1,'<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>&lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Strict//EN&quot; &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;html&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;head&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;meta http-equiv=&quot;content-type&quot; content=&quot;text/html; charset=UTF-8&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;title&gt;Dark Matter S02E02 online schauen bei Filmpalast.to als Stream &amp; Download&lt;/title&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;meta name=&quot;title&quot; content=&quot;Dark Matter S02E02&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;meta name=&quot;description&quot; content=&quot;Dark Matter S02E02 stream online anschauen - Die Story von &bdquo;Dark Matter&ldquo; handelt von der Crew eines Raumschiffs, die mit Ged&auml;chtnisverlust in der N&auml;he einer Minenkolonie erwacht. Wie die Neuank&ouml;mmlinge erfahren, f&uuml;rchten die Bewohner der Kolonie sich vor einem Unternehmen, das seine S&ouml;ldner geschickt haben soll, um sich den Planeten unter den Nagel zu rei&szlig;en. Nat&uuml;rlich entpuppt sich die erinnerungsfreie Crew als die entsandte Killergruppe, doch nicht alle wollen sich ihren alten Auftraggebern beugen. Wer wird sich gegen die skrupellose Firma stellen und mit den Kolonisten k&auml;mpfen?&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;meta name=&quot;keywords&quot; content=&quot;Spaceship, Space, Warrior, Female Fighter, Female Warrior&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;meta name=&quot;robots&quot; content=&quot;index, follow&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;meta http-equiv=&quot;content-language&quot; content=&quot;de&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;link href=&quot;<a href=\"http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900\" target=\"_blank\">//fonts.googleapis.com/css?family=Lato:100,300,400,700,900</a>&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;link href=&quot;<a href=\"http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" target=\"_blank\">//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css</a>&quot; rel=&quot;stylesheet&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;link href=&quot;<a href=\"http://filmpalast.to/themes/downloadarchive/css/style.css?v=2\" target=\"_blank\">http://filmpalast.to/themes/downloadarchive/css/style.css?v=2</a>&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;link href=&quot;<a href=\"http://filmpalast.to/themes/downloadarchive/css/skin.css\" target=\"_blank\">http://filmpalast.to/themes/downloadarchive/css/skin.css</a>&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;link rel=&quot;shortcut icon&quot; href=&quot;<a href=\"http://filmpalast.to/themes/downloadarchive/images/favicon.ico\" target=\"_blank\">http://filmpalast.to/themes/downloadarchive/images/favicon.ico</a>&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/head&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;body&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;script&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>(function(i,s,o,g,r,a,m){i[&#39;GoogleAnalyticsObject&#39;]=r;i[r]=i[r]||function(){</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>})(window,document,&#39;script&#39;,&#39;//www.google-analytics.com/analytics.js&#39;,&#39;ga&#39;);</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>ga(&#39;create&#39;, &#39;UA-37462529-3&#39;, &#39;auto&#39;);</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>ga(&#39;send&#39;, &#39;pageview&#39;);</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/script&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;fb-root&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;script&gt;(function(d, s, id) {</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var js, fjs = d.getElementsByTagName(s)[0];</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>if (d.getElementById(id)) return;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>js = d.createElement(s); js.id = id;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>js.src = &quot;//connect.facebook.net/de_DE/sdk.js#xfbml=1&amp;version=v2.4&quot;;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>fjs.parentNode.insertBefore(js, fjs);</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>}(document, &#39;script&#39;, &#39;facebook-jssdk&#39;));&lt;/script&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a href=&quot;<a href=\"http://filmpalast.to/\" target=\"_blank\">http://filmpalast.to/</a>&quot; title=&quot;Deine Kino und Serien Streams&quot;&gt;&lt;img src=&quot;<a href=\"http://filmpalast.to/themes/downloadarchive/images/logo.png\" target=\"_blank\">/themes/downloadarchive/images/logo.png</a>&quot; id=&quot;logo&quot; alt=&quot;logo&quot; style=&quot;background: inherit; height: auto;&quot;/&gt;&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;img src=&quot;<a href=\"http://filmpalast.to/themes/downloadarchive/images/slider_headline.png\" target=\"_blank\">/themes/downloadarchive/images/slider_headline.png</a>&quot; id=&quot;title&quot; alt=&quot;claim&quot; style=&quot;background: inherit; height:auto;&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;form method=&quot;post&quot; class=&quot;rb&quot; id=&quot;headerSearchFormTop&quot; action=&quot;http://filmpalast.to/search&quot; name=&quot;searchheaderLeft&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;fieldset&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;input type=&quot;search&quot; id=&quot;headersearch_contens&quot; name=&quot;headerSearchText&quot; value=&quot;&quot; onchange=&quot;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var addSearchAction=&#39;&#39;;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>addSearchAction=this.value;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>document.getElementById(&#39;headerSearchFormTop&#39;).setAttribute(&#39;action&#39;,&#39;/search/title/&#39;+addSearchAction);&quot; onload=&quot;this.focus()&quot; placeholder=&quot;| Filme und Serien suchen&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;input type=&quot;hidden&quot; name=&quot;t1&quot; value=&quot;tags&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;input type=&quot;submit&quot; id=&quot;headersearchsubmit&quot; value=&quot;FINDEN&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/fieldset&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/form&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;container&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;featuredMoviesPlaceholder&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;header&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;script id=&quot;load_ad_402424&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>(function() {</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var _script = document.createElement(&#39;script&#39;);</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>_script.type = &#39;text/javascript&#39;;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>_script.async = true;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>_script.id = &#39;init_ad_402424&#39;;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>_script.src = &#39;http://d9ae99824.se/?placement=402424&amp;async&#39;;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>_script.onload = _script.onreadystatechange = function() {</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var rs = this.readyState;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>if(this.readyState &amp;&amp; this.readyState != &#39;complete&#39; &amp;&amp; this.readyState != &#39;loaded&#39;)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>return;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>if(typeof load_ad_402424 == &#39;function&#39;)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>load_ad_402424(_script);</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>};</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var _s = document.getElementById(&#39;load_ad_402424&#39;);</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>_s.parentNode.insertBefore(_script, _s)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>})();</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/script&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;nav role=&quot;navigation&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;a class=&quot;mb1 rb&quot; href=&quot;<a href=\"http://filmpalast.to/\" target=\"_blank\">http://filmpalast.to/</a>&quot;&gt;Neu&lt;/a&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;a class=&quot;mb1 rb &quot; href=&quot;<a href=\"http://filmpalast.to/movies/new\" target=\"_blank\">http://filmpalast.to/movies/new</a>&quot;&gt;Filme&lt;/a&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;a class=&quot;mb1 rb &quot; href=&quot;<a href=\"http://filmpalast.to/serien/view\" target=\"_blank\">http://filmpalast.to/serien/view</a>&quot;&gt;Serien&lt;/a&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;a class=&quot;mb1 rb &quot; href=&quot;<a href=\"http://filmpalast.to/movies/top\" target=\"_blank\">http://filmpalast.to/movies/top</a>&quot;&gt;Top&lt;/a&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/nav&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/header&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;script type=&quot;text/javascript&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>/* &lt;![CDATA[ */</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var addSizes = +10; // + h&ouml;he des covers</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var addStartSizes = -1;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var moveSizes = +0;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var imgOpac = 0.6; // cover Opacity</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var layerSizes = 100;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>var aniTimes = 100; // animation in milisec</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>/* ]]&gt; */</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/script&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;content&quot; role=&quot;main&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;article class=&quot;detail rb pDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;cite&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;h2 class=&quot;bgDark rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Dark Matter S02E02</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/h2&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span id=&quot;release_text&quot; style=&quot;vertical-align: top;&quot; class=&quot;rb&quot;&gt;Dark.Matter.S02E02.German.Dubbed.HDTV.x264-ITG&amp;nbsp;&amp;nbsp;nbsp;&lt;a target=&quot;_blank&quot; href=&quot;<a href=\"http://www.xrel.to/search.html?xrel_search_query=Dark.Matter.S02E02.German.Dubbed.HDTV.x264-ITG\" target=\"_blank\">http://www.xrel.to/search.html?xrel_search_query=Dark.Matter.S02E02.German.Dubbed.HDTV.x264-ITG</a>&quot;&gt;&lt;img src=&quot;<a href=\"http://filmpalast.to/themes/downloadarchive/images/xrel.png\" target=\"_blank\">http://filmpalast.to/themes/downloadarchive/images/xrel.png</a>&quot; width=&quot;27&quot;/&gt;&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;hidden &quot;&gt;Die Story von &bdquo;Dark Matter&ldquo; handelt von der Crew eines Raumschiffs, die mit Ged&auml;chtnisverlust in der N&auml;he einer Minenkolonie erwacht. Wie die Neuank&ouml;mmlinge erfahren, f&uuml;rchten die Bewohner der Kolonie sich vor einem Unternehmen, das seine S&ouml;ldner geschickt haben soll, um sich den Planeten unter den Nagel zu rei&szlig;en. Nat&uuml;rlich entpuppt sich die erinnerungsfreie Crew als die entsandte Killergruppe, doch nicht alle wollen sich ihren alten Auftraggebern beugen. Wer wird sich gegen die skrupellose Firma stellen und mit den Kolonisten k&auml;mpfen?&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;hidden&quot; id=&quot;viewID&quot; data-id=&quot;10455&quot;&gt;10455&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/cite&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;bgDark rb clearfix&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;detail rb pDetail&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;p style=&quot;font-size: 13px; color:rgb(102, 102, 102);&quot;&gt;Hier den Film bewerten!&lt;/p&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span role=&quot;complementary&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;hreview-aggregate&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;item&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;fn&quot;&gt;&lt;span class=&quot;value-title&quot; title=&quot;Dark Matter S02E02&quot;/&gt;&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;star-rate&quot; data-rating=&quot;9.4&quot; data-rated=&quot;7&quot; data-mid=&quot;10455&quot; data-readOnly=&quot;false&quot; class=&quot;star-rating&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;rating&quot; style=&quot;display:inline-block; font-size: 12px;&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;average&quot;&gt;9.4&lt;/span&gt;&amp;nbsp;/&amp;nbsp;10 von</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;votes&quot;&gt;7&lt;/span&gt; Votes</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;worst hidden&quot;&gt;1 (sehr schlecht)&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;best hidden&quot;&gt;10 (sehr gut)&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span&gt;&amp;ndash; Imdb: 7.4/10&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;rating-msg&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;progress&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;progress-bar&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;progress p2&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;progress-bar&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;img src=&quot;<a href=\"http://filmpalast.to/files/movies/450/dark-matter-s02e02.jpg\" target=\"_blank\">/files/movies/450/dark-matter-s02e02.jpg</a>&quot; alt=&quot;Dark Matter S02E02&quot; id=&quot;img__10455&quot; class=&quot;cover2&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a href=&quot;<a href=\"http://go.ad2up.com/afu.php?id=453646\" target=\"_blank\">//go.ad2up.com/afu.php?id=453646</a>&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;<a href=\"http://filmpalast.to/themes/downloadarchive/images/kappa.png\" target=\"_blank\">/themes/downloadarchive/images/kappa.png</a>&quot; width=&quot;43%&quot;&gt;&lt;/a&gt;&lt;a href=&quot;<a href=\"http://go.ad2up.com/afu.php?id=453645\" target=\"_blank\">//go.ad2up.com/afu.php?id=453645</a>&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;<a href=\"http://filmpalast.to/themes/downloadarchive/images/czechx.png\" target=\"_blank\">/themes/downloadarchive/images/czechx.png</a>&quot; width=&quot;43%&quot;&gt;&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;clearfix&quot; id=&quot;detail-content-list&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;p&gt;Shortinfos&lt;/p&gt;&lt;strong&gt;3792&lt;/strong&gt; Nutzer haben den Stream gesehen</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;br/&gt;Ver&amp;ouml;ffentlicht: 2016</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;br/&gt;Spielzeit: &lt;em&gt;42 min&lt;/em&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;br/&gt;(2)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a style=&quot;line-height:40px&quot; name=&quot;comments_link&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#comments_view\" target=\"_blank\">#comments_view</a>&quot; id=&quot;jump_to_comments&quot; data-show=&quot;comment-loop&quot; alt=&quot;Kommentare anzeigen&quot;&gt;Kommentare&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span style=&quot;line-height:29px; height:36px; display: inline-block; width: 200px;vertical-align: bottom;margin-left: 20px;&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;fb-like&quot; data-href=&quot;http://filmpalast.to/movies/view/dark-matter-s02e02&quot; data-layout=&quot;button_count&quot; data-action=&quot;like&quot; data-show-faces=&quot;true&quot; data-share=&quot;true&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;p&gt;Kategorien, Genre&lt;/p&gt;&lt;span&gt;&lt;a class=&quot;rb&quot; href=&quot;<a href=\"http://filmpalast.to/search/genre/Thriller\" target=\"_blank\">http://filmpalast.to/search/genre/Thriller</a>&quot;&gt;Thriller&lt;/a&gt; &lt;a class=&quot;rb&quot; href=&quot;<a href=\"http://filmpalast.to/search/genre/Sci-Fi\" target=\"_blank\">http://filmpalast.to/search/genre/Sci-Fi</a>&quot;&gt;Sci-Fi&lt;/a&gt; &lt;a class=&quot;rb&quot; href=&quot;<a href=\"http://filmpalast.to/search/genre/Drama\" target=\"_blank\">http://filmpalast.to/search/genre/Drama</a>&quot;&gt;Drama&lt;/a&gt; &lt;/span&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li itemscope itemtype=&quot;http://schema.org/Movie&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;p&gt;Filmhandlung &amp;ndash;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;em itemprop=&quot;name&quot;&gt;Dark Matter S02E02&lt;/em&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/p&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span itemprop=&quot;description&quot;&gt;Die Story von &bdquo;Dark Matter&ldquo; handelt von der Crew eines Raumschiffs, die mit Ged&auml;chtnisverlust in der N&auml;he einer Minenkolonie erwacht. Wie die Neuank&ouml;mmlinge erfahren, f&uuml;rchten die Bewohner der Kolonie sich vor einem Unternehmen, das seine S&ouml;ldner geschickt haben soll, um sich den Planeten unter den Nagel zu rei&szlig;en. Nat&uuml;rlich entpuppt sich die erinnerungsfreie Crew als die entsandte Killergruppe, doch nicht alle wollen sich ihren alten Auftraggebern beugen. Wer wird sich gegen die skrupellose Firma stellen und mit den Kolonisten k&auml;mpfen?&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;img itemprop=&quot;image&quot; src=&quot;<a href=\"http://filmpalast.to/files/movies/240/dark-matter-s02e02.jpg\" target=\"_blank\">/files/movies/240/dark-matter-s02e02.jpg</a>&quot; class=&quot;hidden&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;meta itemprop=&quot;duration&quot; content=&quot;T42M00S&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;meta itemprop=&quot;thumbnailUrl&quot; content=&quot;/files/movies/240/dark-matter-s02e02.jpg&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;p style=&quot;font-size:14px;&quot;&gt;Sch&ouml;pfer&lt;/p&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;rb&quot; style=&quot;white-space:nowrap; margin: 2px 0 3px 0px;&quot; href=&quot;<a href=\"http://filmpalast.to/search/director/Joseph%20Mallozzi\" target=\"_blank\">/search/director/Joseph Mallozzi</a>&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Joseph Mallozzi</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;rb&quot; style=&quot;white-space:nowrap; margin: 2px 0 3px 0px;&quot; href=&quot;<a href=\"http://filmpalast.to/search/director/%20Paul%20Mullie\" target=\"_blank\">/search/director/ Paul Mullie</a>&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Paul Mullie</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;p style=&quot;font-size:14px;&quot;&gt;Schauspieler&lt;/p&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;rb&quot; style=&quot;white-space:nowrap; margin: 2px 0 3px 0px;&quot; href=&quot;<a href=\"http://filmpalast.to/search/title/Anthony%20Lemke\" target=\"_blank\">/search/title/Anthony Lemke</a>&quot;&gt;Anthony Lemke&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;rb&quot; style=&quot;white-space:nowrap; margin: 2px 0 3px 0px;&quot; href=&quot;<a href=\"http://filmpalast.to/search/title/%20Jodelle%20Ferland\" target=\"_blank\">/search/title/ Jodelle Ferland</a>&quot;&gt; Jodelle Ferland&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;rb&quot; style=&quot;white-space:nowrap; margin: 2px 0 3px 0px;&quot; href=&quot;<a href=\"http://filmpalast.to/search/title/%20Melissa%20O\'Neil\" target=\"_blank\">/search/title/ Melissa O&amp;#x27;Neil</a>&quot;&gt; Melissa O&amp;#x27;Neil&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;rb&quot; style=&quot;white-space:nowrap; margin: 2px 0 3px 0px;&quot; href=&quot;<a href=\"http://filmpalast.to/search/title/%20Alex%20Mallari%20Jr.\" target=\"_blank\">/search/title/ Alex Mallari Jr.</a>&quot;&gt; Alex Mallari Jr.&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;rb&quot; style=&quot;white-space:nowrap; margin: 2px 0 3px 0px;&quot; href=&quot;<a href=\"http://filmpalast.to/search/title/%20Zoie%20Palmer\" target=\"_blank\">/search/title/ Zoie Palmer</a>&quot;&gt; Zoie Palmer&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;banner clearfix&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;detailMovieLinks clearfix showSeasons&quot; id=&quot;streamPlay&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;streamNav&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;button type=&quot;submit&quot; value=&quot;links&quot; data-layer=&quot;streamLinksWrapper&quot; class=&quot;active rb openButton&quot;&gt;&lt;i class=&quot;fa fa-play-circle&quot;&gt;&lt;/i&gt;Dark Matter S02E02&lt;/button&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;button type=&quot;submit&quot; value=&quot;Staffeln&quot; id=&quot;dropdown-btn&quot; data-layer=&quot;streamStaffelWrapper&quot; class=&quot;rb dropdownBtn openButton&quot;&gt;Dark Matter&amp;nbsp;&lt;span class=&quot;hint&quot;&gt;(Alle Staffeln/Episoden w&auml;hlen!)&lt;/span&gt;&lt;span class=&quot;fa fa-caret-down&quot; style=&quot;margin-left:10px&quot;&gt;&lt;/span&gt;&lt;/button&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;staffelNav clearfix fa-list&quot; id=&quot;staffel-nav&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a data-id=&quot;staffId_1&quot; id=&quot;sid1&quot; class=&quot;staffTab&quot; data-sid=&quot;1&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;&amp;nbsp; S01 &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a data-id=&quot;staffId_2&quot; id=&quot;sid2&quot; class=&quot;staffTab&quot; data-sid=&quot;2&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;&amp;nbsp; S02 &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;streamLinksWrapper clearfix showContent&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;currentTabInfo&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;p&gt;&lt;i class=&quot;fa fa-info-circle yellow&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;em&gt;Streamanbieter aussuchen&lt;/em&gt; und auf &quot;Play&quot; klicken!&lt;/p&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;grap-stream-list&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;currentStreamLinks&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;hostBg rb&quot;&gt; &lt;p class=&quot;hostName&quot;&gt;Streamcloud.eu&lt;/p&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;streamPlayBtn clearfix rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;streamEpisodeTitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;button rb iconPlay stream-src&quot; data-id=&quot;87647&quot; data-stamp=&quot;0&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa border fa-play-circle&quot;&gt;&lt;/i&gt; Play &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;currentStreamLinks&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;hostBg rb&quot;&gt; &lt;p class=&quot;hostName&quot;&gt;Vivo.sx&lt;/p&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;streamPlayBtn clearfix rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;streamEpisodeTitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;button rb iconPlay stream-src&quot; data-id=&quot;87654&quot; data-stamp=&quot;0&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa border fa-play-circle&quot;&gt;&lt;/i&gt; Play &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;currentStreamLinks&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;hostBg rb&quot;&gt; &lt;p class=&quot;hostName&quot;&gt;Flashx.tv&lt;/p&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;streamPlayBtn clearfix rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;streamEpisodeTitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;button rb iconPlay stream-src&quot; data-id=&quot;87644&quot; data-stamp=&quot;0&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa border fa-play-circle&quot;&gt;&lt;/i&gt; Play &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;currentStreamLinks&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;hostBg rb&quot;&gt; &lt;p class=&quot;hostName&quot;&gt;wholecloud.net&lt;/p&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;streamPlayBtn clearfix rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;streamEpisodeTitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;button rb iconPlay stream-src&quot; data-id=&quot;87649&quot; data-stamp=&quot;0&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa border fa-play-circle&quot;&gt;&lt;/i&gt; Play &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;currentStreamLinks&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;hostBg rb&quot;&gt; &lt;p class=&quot;hostName&quot;&gt;Openload.co&lt;/p&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;streamPlayBtn clearfix rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;streamEpisodeTitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;button rb iconPlay stream-src&quot; data-id=&quot;87645&quot; data-stamp=&quot;0&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa border fa-play-circle&quot;&gt;&lt;/i&gt; Play &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;currentStreamLinks&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;hostBg rb&quot;&gt; &lt;p class=&quot;hostName&quot;&gt;Streamin.to&lt;/p&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;streamPlayBtn clearfix rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;streamEpisodeTitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;button rb iconPlay stream-src&quot; data-id=&quot;87648&quot; data-stamp=&quot;0&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa border fa-play-circle&quot;&gt;&lt;/i&gt; Play &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;currentStreamLinks&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;hostBg rb&quot;&gt; &lt;p class=&quot;hostName&quot;&gt;Shared.sx&lt;/p&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;streamPlayBtn clearfix rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;streamEpisodeTitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;button rb iconPlay stream-src&quot; data-id=&quot;87646&quot; data-stamp=&quot;0&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa border fa-play-circle&quot;&gt;&lt;/i&gt; Play &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;currentStreamLinks&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;hostBg rb&quot;&gt; &lt;p class=&quot;hostName&quot;&gt;TheVideo.me&lt;/p&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;streamPlayBtn clearfix rb&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;streamEpisodeTitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a class=&quot;button rb iconPlay stream-src&quot; data-id=&quot;87655&quot; data-stamp=&quot;0&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa border fa-play-circle&quot;&gt;&lt;/i&gt; Play &lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;streamStaffelWrapper clearfix&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;currentTabInfo&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;p class=&quot;serieSubhead&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/p&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;rounded imgWrapper&quot;&gt;&lt;img src=&quot;<a href=\"http://filmpalast.to/files/movies/100/dark-matter-s02e02.jpg\" target=\"_blank\">/files/movies/100/dark-matter-s02e02.jpg</a>&quot; alt=&quot;small rounded image Dark Matter S02E02&quot; id=&quot;s_img__10455&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span id=&quot;staffel-id-holder&quot;&gt;&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;firstRun rb clearfix&quot; id=&quot;staffelWrapper&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffelWrapperLoop loop_1 rb clearfix active &quot; id=&quot;staffId_1&quot; data-sid=&quot;1&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;staffelEpisodenList&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e01\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e01</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;0&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E01 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E01.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_0&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e02\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e02</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;1&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E02 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E02.German.Dubbed.REPACK.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_1&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e03\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e03</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;2&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E03 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E03.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_2&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e04\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e04</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;3&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E04 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E04.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_3&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e05\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e05</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;4&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E05 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E05.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_4&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e06\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e06</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;5&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E06 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E06.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_5&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e07\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e07</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;6&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E07 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E07.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_6&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e08\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e08</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;7&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E08 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E08.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_7&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e09\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e09</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;8&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E09 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E09.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_8&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e10\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e10</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;9&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E10 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E10.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_9&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e11\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e11</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;10&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E11 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E11.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_10&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e12\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e12</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;11&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E12 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E12.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_11&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s01e13\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s01e13</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;12&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S01E13 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S01E13.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_12&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;&lt;/div&gt; &lt;div class=&quot;staffelWrapperLoop loop_2 rb clearfix hide &quot; id=&quot;staffId_2&quot; data-sid=&quot;2&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul class=&quot;staffelEpisodenList&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e01\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s02e01</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;13&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S02E01 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S02E01.German.Dubbed.REPACK.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_13&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;staffStreamsDetails&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li class=&quot;stitle&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;staffId_&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02\" target=\"_blank\">http://filmpalast.to/movies/view/dark-matter-s02e02</a>&quot; class=&quot;getStaffelStream&quot; data-sid=&quot;14&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-chain&quot;&gt;&lt;/i&gt; Dark Matter S02E02 &amp;nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-plus-square&quot;&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;small&gt;Dark.Matter.S02E02.German.Dubbed.HDTV.x264-ITG&lt;/small&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;loadedStreamLinks&quot; id=&quot;staffStreams_14&quot;&gt;&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/ul&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a name=&quot;comments_view&quot;&gt;&amp;nbsp;&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;detailComments&quot; class=&quot;clearfix&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;ul id=&quot;commentsNav&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span style=&quot;margin-left:-13px;&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;readComment&quot; data-show=&quot;comment-loop&quot; data-alt=&quot;Kommentare ausblenden&quot; alt=&quot;Kommentare anzeigen&quot; class=&quot;button rb&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;/i&gt;Kommentare Anzeigen (2)&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;a id=&quot;postComment&quot; data-show=&quot;comment-add&quot; data-alt=&quot;Formular ausblenden&quot; alt=&quot;Kommentar schreiben&quot; class=&quot;button rb&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot;&gt;&lt;i class=&quot;fa fa-microphone&quot;&gt;&lt;/i&gt; Kommentar schreiben&lt;/a&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/span&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;&lt;span class=&quot;hidden&quot;&gt;&lt;a href=&quot;<a href=\"http://filmpalast.to/movies/view/Dark%20Matter%20S02E02#comments\" target=\"_blank\">http://filmpalast.to/movies/view/Dark Matter S02E02#comments</a>&quot;&gt; Comments&lt;/a&gt;&lt;/span&gt;&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;comment-add&quot; class=&quot;clearfix&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;form action=&quot;/comment&quot; method=&quot;POST&quot; style=&quot;clear:left;&quot; name=&quot;commentForm&quot; id=&quot;commentFormID&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;fieldset id=&quot;fieldset_comments&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;input name=&quot;mid&quot; type=&quot;hidden&quot; value=&quot;10455&quot; id=&quot;mid&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;label for=&quot;addtitle&quot; id=&quot;ctitle&quot;&gt;Betreff, Titel, &amp;Uuml;berschrift&lt;/label&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;input name=&quot;title&quot; type=&quot;text&quot; value=&quot;&quot; maxlength=&quot;64&quot; data-value=&quot;&quot; size=&quot;50&quot; id=&quot;addtitle&quot; style=&quot;clear:left;&quot; class=&quot;rb inputfield half comment-field&quot;/&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;label for=&quot;comment&quot; id=&quot;text&quot;&gt;Kommentar, Meinung, Frage zum Stream&lt;/label&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;textarea name=&quot;comment&quot; value=&quot;comment&quot; size=&quot;50&quot; maxlength=&quot;624&quot; id=&quot;comment&quot; data-value=&quot;&quot; class=&quot;rb inputfield bg-comment comment-field&quot;/&gt;&lt;/textarea&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/fieldset&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;button name=&quot;commentSubmit&quot; value=&quot;send&quot; type=&quot;submit&quot; id=&quot;sendCommentForm&quot; href=&quot;<a href=\"http://filmpalast.to/movies/view/dark-matter-s02e02#\" target=\"_blank\">#</a>&quot; class=&quot;button rb&quot; style=&quot;width:180px; margin-top:20px; padding:3px 15px; height:35px &quot;&gt;&lt;i class=&quot;fa fa-microphone&quot;&gt;&lt;/i&gt; Kommentar senden&lt;/button&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/form&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/div&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;/li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;li&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div id=&quot;comment-loop&quot; style=&quot;display:none; margin-top: 20px;&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;commentBox&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;div class=&quot;userComment&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;h4&gt;Dark Matter&lt;/h4&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;span class=&quot;commentInfo&quot;&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-calendar&quot;&gt;&lt;span&gt;&amp;nbsp;Fri-Jul-2016&lt;/span&gt;&lt;/i&gt;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&lt;i class=&quot;fa fa-comments&quot;&gt;&lt;span&',0,0,'2016-07-23 04:02:49','2016-07-23 04:02:49','0');
/*!40000 ALTER TABLE `lie_front_page_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_pages`
--

DROP TABLE IF EXISTS `lie_front_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `type` enum('s','d') NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_pages`
--

LOCK TABLES `lie_front_pages` WRITE;
/*!40000 ALTER TABLE `lie_front_pages` DISABLE KEYS */;
INSERT INTO `lie_front_pages` VALUES (1,'wie','hallo',0,'s',0,0,'2016-07-23 04:02:49','2016-08-03 04:37:59','1');
/*!40000 ALTER TABLE `lie_front_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_services`
--

DROP TABLE IF EXISTS `lie_front_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_services`
--

LOCK TABLES `lie_front_services` WRITE;
/*!40000 ALTER TABLE `lie_front_services` DISABLE KEYS */;
INSERT INTO `lie_front_services` VALUES (1,'Test Front Service','Testing by Rajlakshmi.','2016-07-27','2016-07-28',2,0,'2016-07-19 16:28:58','2016-07-20 10:48:32','1'),(2,'25th July','Test test test test','2016-07-25','2016-07-31',2,0,'2016-07-25 18:54:14','2016-08-02 13:34:52','2'),(3,'front test','maxican         Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events.','2016-08-02','2016-08-29',2,0,'2016-08-02 13:35:42','2016-08-02 13:35:42','1'),(4,'lol','lol','2016-08-02','2016-08-03',2,0,'2016-08-02 17:30:19','2016-08-02 17:30:19','1');
/*!40000 ALTER TABLE `lie_front_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_state_maps`
--

DROP TABLE IF EXISTS `lie_front_state_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_state_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_state_maps`
--

LOCK TABLES `lie_front_state_maps` WRITE;
/*!40000 ALTER TABLE `lie_front_state_maps` DISABLE KEYS */;
INSERT INTO `lie_front_state_maps` VALUES (1,21,1,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(2,13,2,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(3,14,3,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(4,20,4,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(5,15,5,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(6,16,6,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(7,17,7,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(8,19,8,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(9,18,9,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1'),(10,23,10,0,0,'0000-00-00 00:00:00','2016-08-31 05:54:45','1');
/*!40000 ALTER TABLE `lie_front_state_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_user_addresses`
--

DROP TABLE IF EXISTS `lie_front_user_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_user_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `booking_person_name` varchar(150) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `street` varchar(256) NOT NULL,
  `house` varchar(16) DEFAULT NULL,
  `staircase` varchar(16) DEFAULT NULL,
  `floor` varchar(16) DEFAULT NULL,
  `apartment` varchar(16) DEFAULT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `is_type` enum('p','a') NOT NULL COMMENT 'p->primary, s->secondary',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  `company` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_user_addresses`
--

LOCK TABLES `lie_front_user_addresses` WRITE;
/*!40000 ALTER TABLE `lie_front_user_addresses` DISABLE KEYS */;
INSERT INTO `lie_front_user_addresses` VALUES (1,1,'Raj','9876435355','A-512,Gali No. 10,South Gnaesh Nagar','',NULL,NULL,NULL,NULL,'','','p',1,2,'2016-07-19 17:24:38','2016-08-19 13:56:41','Near Mother dairy',1,1,1,'110092','2',NULL),(2,3,'Recai','06644975080','margaretenstrasse 100','',NULL,NULL,NULL,NULL,'','','p',0,3,'2016-07-20 05:42:00','2016-09-16 06:19:44','',11,21,4,'8','2',NULL),(3,4,'Nikhil Malik','9555760001','new street address','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-07-20 19:08:02','2016-07-21 15:41:20','test landmark',11,3,4,'1010','2',NULL),(4,5,'test','9711366589','test FCA 1012 test123','',NULL,NULL,NULL,NULL,'','','',5,5,'2016-07-21 13:47:57','2016-08-19 17:05:32','test',12,11,1,'121001','2',NULL),(5,5,'Raj','9711366589','test12','',NULL,NULL,NULL,NULL,'','','',5,0,'2016-07-21 13:48:58','2016-07-21 13:51:47','test',1,1,1,'121001','2',NULL),(6,4,'test Address','9999777755','Address','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-07-21 15:08:15','2016-07-23 13:44:13','Landmark',4,3,3,'1040','2',NULL),(7,4,'bindu','7896541230','bindu','',NULL,NULL,NULL,NULL,'','','',4,4,'2016-07-23 13:44:05','2016-08-10 17:27:07','asas ',1,2,10,'1','1',NULL),(8,4,'Raj','233232322','sadasdada','',NULL,NULL,NULL,NULL,'','','',4,0,'2016-07-23 13:53:49','2016-07-23 13:54:24','dadasd',1,1,1,'222222','2',NULL),(9,4,'Raj','3244234423','fsfd','',NULL,NULL,NULL,NULL,'','','',4,0,'2016-07-23 13:55:52','2016-07-23 13:56:05','fsfsdfsdf',1,1,1,'233244','2',NULL),(10,5,'Raj','9898989898','hghkhhfh','',NULL,NULL,NULL,NULL,'','','',5,0,'2016-07-25 12:57:26','2016-07-25 13:02:05','fghdgfdg',1,1,1,'121001','2',NULL),(11,6,'Recai Firattt','066488399491','quellenstrasse 1/2/1','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-07-27 08:48:44','2016-07-27 08:48:44','wien',0,0,0,'1100','1',NULL),(12,8,'Bindu Kharub','9711366589','test test test test','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-07-28 12:30:59','2016-07-28 12:30:59','test',0,0,0,'121001','1',NULL),(13,5,'Bindu','971256','Just Testing ','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-07-28 12:50:33','2016-07-28 12:50:33','Testing',12,11,1,'121001','2',NULL),(14,4,'anjali11122','2222222222','asdasdasd','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-01 13:18:38','2016-08-01 16:17:02','sadasdasdas',0,0,0,'11213','1',NULL),(15,4,'fhfghf','4543646456','fghfghfghgf','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-01 15:21:50','2016-08-01 15:21:50','hgfhfgh',0,0,0,'ghfgh','2',NULL),(16,4,'raju','3221312321','asdsdasd','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-01 16:16:31','2016-08-01 16:16:31','asdasdasd',0,0,0,'123555','1',NULL),(17,10,'Raj','9090909090','a11 sec 37 ','',NULL,NULL,NULL,NULL,'','','',10,0,'2016-08-02 12:59:23','2016-08-02 12:59:23','saffron public school',1,1,1,'121003','1',NULL),(18,11,'Raj','8860280996','A81','',NULL,NULL,NULL,NULL,'','','',11,0,'2016-08-03 17:21:01','2016-08-03 18:03:26','SBI ATM',1,1,1,'201301','2',NULL),(19,11,'jitendrA ','8860280996','A81','',NULL,NULL,NULL,NULL,'','','',11,0,'2016-08-03 18:03:48','2016-08-31 13:29:45','GB nagar',0,0,0,'202135','2',NULL),(20,11,'bunty','1234567898','E11','',NULL,NULL,NULL,NULL,'','','',11,11,'2016-08-03 18:05:36','2016-08-31 13:30:11','alivenet',0,0,0,'54587','2',NULL),(21,15,'Raj','9711366585','faridabad  sector 16','',NULL,NULL,NULL,NULL,'','','',15,15,'2016-08-12 16:59:37','2016-08-12 17:01:37','test',11,21,4,'1','1',NULL),(22,16,'bindu kharub','3244324234343','asdasd sd sad asd asdas d','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-13 12:16:53','2016-08-13 12:16:53','fgfgfgf',0,0,0,'3232332','1',NULL),(23,17,'gh fhh','9711366589','gfdjfgk hlhjkghvj mbmmnbm','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-17 11:11:13','2016-08-17 12:31:28','trrwerwet',0,0,0,'12002','2',NULL),(24,18,'dsfsdfsdfsdfsd dsfsdfsdfsd','12212121','dsfsdfsdfsdf f sdf df sdf f sdfs df','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-19 12:05:16','2016-08-19 12:05:16','sdfsdf sfsd sfd sdf',0,0,0,'123456','1',NULL),(25,19,'suraj kumar','96521547896','1544/2 ambedakar nagar','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-23 16:45:49','2016-08-23 16:45:49','max hospital',0,0,0,'110088','1',NULL),(26,1,'Raj','9897878899','test','',NULL,NULL,NULL,NULL,'','','',1,1,'2016-08-24 11:00:41','2016-08-30 15:29:10','test',11,21,4,'1','1',NULL),(27,20,'dsfdsfsdfsdf dsfdsfsdfsd','1123456789','dfdsfsdfsdfsdfsdf','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-24 11:54:49','2016-08-24 11:54:49','dsfsdfsd',0,0,0,'123456','2',NULL),(28,20,'hjkjh','123456','ghfghfghffg gvcgfgfh','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-24 11:59:34','2016-08-24 11:59:34','fdfdff',0,0,0,'67776','1',NULL),(29,11,'!@#$%^&*','7845','asdasdas','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-31 13:31:48','2016-08-31 13:31:48','dasdasd',0,0,0,'547896','2',NULL),(30,11,'!@#$%^&*','7845','asdasdas','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-08-31 13:32:14','2016-08-31 13:32:14','dasdasd',0,0,0,'547896','2',NULL),(31,3,'','06644975080','Quellenstrasse 167','',NULL,NULL,NULL,NULL,'','','',3,0,'2016-09-03 08:01:55','2016-09-16 06:19:57','',11,21,4,'11','2',NULL),(32,5,'Bindu','9711365899','gfsdfsgjfd','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-09-06 11:32:26','2016-09-06 11:32:26','ffdhfhfg',0,0,0,'121001','1',NULL),(33,21,'raj kumar','3456789012','new Ashok nager Delhi','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-09-06 18:49:23','2016-09-06 18:49:23','New ashoke Metro station',0,0,0,'1234567890','1',NULL),(34,22,'Bindu kharub','9711366589','gfkjdhgfhkgf fjhfjkfhkjgkg','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-09-09 12:49:45','2016-09-09 12:49:45','church',0,0,0,'121001','1',NULL),(35,11,'Jitendra','08860280996','A81','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-09-12 13:27:48','2016-09-12 13:27:48','SBI ATM',0,0,0,'202135','1',NULL),(36,11,'rfgerg','43543534','fdgfdgdf','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-09-12 13:32:19','2016-09-12 13:32:19','gdfgfd',0,0,0,'34534','1',NULL),(37,11,'hytry','34543534','fgdfhgdf','',NULL,NULL,NULL,NULL,'','','p',0,0,'2016-09-12 13:32:52','2016-09-12 13:32:52','43545gdfg',0,0,0,'54645','1',NULL),(38,3,'Office','06644975080','Quellenstrasse 167 / E','',NULL,NULL,NULL,NULL,'','','p',2,0,'2016-09-16 06:07:40','2016-09-16 06:13:33','Favoriten',11,21,4,'1100','2',NULL),(39,3,'','06644975080','Quellenstrasse 100','',NULL,NULL,NULL,NULL,'','','',3,0,'2016-09-16 06:09:31','2016-09-16 06:09:31','Innere Stadt',11,21,4,'1','1',NULL),(40,3,'','06644975080','Gaadnerstrasse 3 ','',NULL,NULL,NULL,NULL,'','','',3,0,'2016-09-16 06:10:54','2016-09-16 06:10:54','wien',11,21,4,'3','1',NULL),(41,3,'Recai Firat','06644975080','Am Bahnhof Platz 1 ','',NULL,NULL,NULL,NULL,'','','',3,0,'2016-09-16 06:12:10','2016-09-20 05:35:36','wien',11,21,4,'1010','1',NULL),(47,23,'Hakan Aras','069910988886','Laxenburger Straße 68-72/2/5/2502','Laxenburger Straße','68-72','2','5','2502','48.1711977','16.3713288','a',0,0,'2016-12-23 20:14:45','2016-12-23 20:14:45','',11,21,4,'1100','1',''),(49,23,'Orhan Aras','069917190988','Fernkornstrasse 59/27','Fernkornstrasse','59','','','27','48.1722292','16.3631994','a',0,0,'2016-12-24 02:55:47','2016-12-31 11:07:33','',11,21,4,'1100','1','Orhan Aras'),(50,23,'Fatih Aras','069919562267','Friesenplatz 8','Friesenplatz','8','','','','48.1720538','16.3636861','a',0,0,'2016-12-24 05:14:09','2016-12-24 05:14:09','',11,21,4,'1120','1',''),(51,23,'Hakan Aras','069910988886','Laxenburger Straße 68-72/2/5/2502','Laxenburger Straße','68-72','2','5','2502','48.1711977','16.3713288','p',23,23,'2016-12-31 10:26:14','2016-12-31 11:06:38',NULL,11,21,4,'1100','2',''),(52,23,'Hakan Aras','069910988886','Laxenburger Straße 68-72/2/5/2502','Laxenburger Straße','68-72','2','5','2502','48.1711977','16.3713288','p',23,23,'2016-12-31 10:26:59','2016-12-31 10:26:59',NULL,11,21,4,'1100','1','');
/*!40000 ALTER TABLE `lie_front_user_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_user_details`
--

DROP TABLE IF EXISTS `lie_front_user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` enum('m','f','o') DEFAULT NULL COMMENT 'm->male, f->female, o->other',
  `dob` date NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `registered_via` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_user_details`
--

LOCK TABLES `lie_front_user_details` WRITE;
/*!40000 ALTER TABLE `lie_front_user_details` DISABLE KEYS */;
INSERT INTO `lie_front_user_details` VALUES (1,1,'Raj','Rajlakshmi','Pandey','9897878899','rajlakshmi.alivenetsolution@gmail.com','f','1989-03-14','1468929138281.jpg',1,0,0,'2016-07-19 17:20:48','2016-08-30 15:28:49','1'),(2,2,'Nikhil','Nikhil','Malik','9555760001','nikhil.alivenet@gmail.com','m','1993-03-28','1468934202372.jpg',1,0,2,'2016-07-19 18:28:52','2016-07-20 11:18:38','1'),(3,3,'Ilia','Lieferzonas','at','066488399491','info@lieferzonas.at','m','1982-09-17','1473984414498.jpg',1,0,8,'2016-07-20 05:26:32','2016-09-16 05:37:55','1'),(4,4,'nikki','Nikhil','Malik','1234567890','nikhil@gmail.com','m','1993-07-15','1473491265140.jpg',1,0,0,'2016-07-20 19:05:26','2016-09-10 12:37:45','1'),(5,5,'Bindu','Bindu','Kharub','7865892565','bindukharub@gmail.com','f','1990-11-06','',1,0,2,'2016-07-21 12:11:02','2016-07-25 16:37:29','1'),(6,6,'','Recai','Firattt','066488399491','belgien@lieferzonas.at','m','0000-00-00','',1,0,0,'2016-07-27 08:48:44','2016-07-27 08:48:44','1'),(7,7,'','test','test','','bindukharub123@gmail.com','m','0000-00-00','',1,0,0,'2016-07-27 13:03:22','2016-07-27 13:03:22','1'),(8,8,'','Bindu','Kharub','9711366589','bindu@gmail.com','m','0000-00-00','',1,0,0,'2016-07-28 12:30:59','2016-07-28 12:30:59','1'),(9,9,'','Anjali','Sharma','','anjali@gmail.com','m','0000-00-00','',1,0,0,'2016-08-01 17:04:34','2016-08-01 17:04:34','1'),(10,10,'','Arun','sharma','','arun.alivenetsolution@gmail.com','m','0000-00-00','',1,0,0,'2016-08-02 12:46:53','2016-08-02 12:46:53','1'),(11,11,'Banti','Jitendra','Sharma','8860280996','jitendra.alivenetsolutions@gmail.com','m','1993-12-25','1472034950587.jpg',1,0,2,'2016-08-03 15:59:05','2016-09-10 17:21:59','1'),(12,12,'','Rajj','Laxmi','','laxmi@gmail.com','m','0000-00-00','',1,0,0,'2016-08-03 16:07:33','2016-08-03 16:07:33','1'),(13,13,'','Arun','sharma','','arun@gmail.com','m','0000-00-00','',1,0,0,'2016-08-04 16:36:10','2016-08-04 16:36:10','1'),(14,14,'','arun','sharma','','arunzz@gmail.com','m','0000-00-00','',1,0,0,'2016-08-11 10:09:26','2016-08-11 10:09:26','1'),(15,15,'','Bindu','Kharub','','bindukharub+1@gmail.com','f','0000-00-00','',1,0,0,'2016-08-12 11:32:44','2016-08-12 11:32:44','1'),(16,16,'','bindu','kharub','3244324234343','bindu_kharub@rediffmail.com','m','0000-00-00','',1,0,0,'2016-08-13 12:16:53','2016-08-13 12:16:53','1'),(17,17,'','ghgh','fhh','9711366589','bindu@gmail.com','m','1996-11-05','',1,0,2,'2016-08-17 11:11:13','2016-08-17 12:26:51','1'),(18,18,'','dsfsdfsdfsdfsd','dsfsdfsdfsd','12212121','batra123@gmail.com','m','0000-00-00','',1,0,0,'2016-08-19 12:05:16','2016-08-19 12:05:16','1'),(19,19,'','suraj','kumar','96521547896','suraj@gmail.com','m','0000-00-00','',1,0,0,'2016-08-23 16:45:49','2016-08-23 16:45:49','1'),(20,20,'','dsfdsfsdfsdf','dsfdsfsdfsd','1123456789','!#$%@gmail.com','m','0000-00-00','',1,0,0,'2016-08-24 11:54:49','2016-08-24 11:54:49','1'),(21,21,'','raj','kumar','3456789012','raju129@gmail.com','m','0000-00-00','',1,0,0,'2016-09-06 18:49:23','2016-09-06 18:49:23','1'),(22,22,'','Bindu','kharub','9711366589','bindukharub@rediffmail.com','m','0000-00-00','',1,0,0,'2016-09-09 12:49:45','2016-09-09 12:49:45','1'),(23,23,'hakanaras','Hakan','Aras','069910988886','hakan.aras@live.at',NULL,'1990-05-08','',1,0,0,'2016-11-19 18:45:01','2016-11-26 00:26:41','1'),(36,39,'123','123','123','123','hakan.aras@wegwerfemail.de','m','2016-12-02','',1,0,0,'2016-12-02 12:04:33','2016-12-02 12:04:33','1'),(37,40,'asdf','qwer','yxcv','069910988886','hakan.aras1@wegwerfemail.de','m','2016-11-02','',1,0,0,'2016-12-02 12:11:19','2016-12-02 12:11:19','1'),(38,42,'Hakan Aras','Hakan','Aras','069910988886','hakan.aras1@byom.de',NULL,'1990-05-08','',2,2,2,'2016-12-31 09:51:06','2016-12-31 09:51:06','1'),(39,43,'Hakan Aras','Hakan','Aras','069910988886','hakan.aras2@byom.de',NULL,'1990-05-08','',2,2,2,'2016-12-31 09:53:10','2016-12-31 09:53:10','1'),(40,44,' ','','','','',NULL,'0000-00-00','',2,2,2,'2017-01-03 01:16:35','2017-01-03 01:16:35','1');
/*!40000 ALTER TABLE `lie_front_user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_user_email_verifications`
--

DROP TABLE IF EXISTS `lie_front_user_email_verifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_user_email_verifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `is_verified` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->no, 1->yes',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_user_email_verifications`
--

LOCK TABLES `lie_front_user_email_verifications` WRITE;
/*!40000 ALTER TABLE `lie_front_user_email_verifications` DISABLE KEYS */;
INSERT INTO `lie_front_user_email_verifications` VALUES (1,1,'cmFqbGFrc2htaS5hbGl2ZW5ldHNvbHV0aW9uQGdtYWlsLmNvbQ1468929048','rajlakshmi.alivenetsolution@gmail.com','0',0,0,'2016-07-19 17:20:48','2016-07-19 17:20:48','0'),(2,2,'bmlraGlsLmFsaXZlbmV0QGdtYWlsLmNvbQ1468933132','nikhil.alivenet@gmail.com','0',0,0,'2016-07-19 18:28:52','2016-07-19 18:28:52','0'),(3,3,'aW5mb0BsaWVmZXJ6b25hcy5hdA1468972592','info@lieferzonas.at','0',0,0,'2016-07-20 05:26:32','2016-07-20 05:26:32','0'),(4,4,'bmlraGlsQGdtYWlsLmNvbQ1469021726','nikhil@gmail.com','0',0,0,'2016-07-20 19:05:26','2016-07-20 19:05:26','0'),(5,5,'YmluZHVraGFydWJAZ21haWwuY29t1469083262','bindukharub@gmail.com','0',0,0,'2016-07-21 12:11:02','2016-07-21 12:11:02','0'),(6,6,'YmVsZ2llbkBsaWVmZXJ6b25hcy5hdA1469589524','belgien@lieferzonas.at','0',0,0,'2016-07-27 08:48:44','2016-07-27 08:48:44','0'),(7,7,'YmluZHVraGFydWIxMjNAZ21haWwuY29t1469604802','bindukharub123@gmail.com','0',0,0,'2016-07-27 13:03:22','2016-07-27 13:03:22','0'),(8,8,'YmluZHVAZ21haWwuY29t1469689259','bindu@gmail.com','0',0,0,'2016-07-28 12:30:59','2016-07-28 12:30:59','0'),(9,9,'YW5qYWxpQGdtYWlsLmNvbQ1470051274','anjali@gmail.com','0',0,0,'2016-08-01 17:04:34','2016-08-01 17:04:34','0'),(10,10,'YXJ1bi5hbGl2ZW5ldHNvbHV0aW9uQGdtYWlsLmNvbQ1470122213','arun.alivenetsolution@gmail.com','0',0,0,'2016-08-02 12:46:53','2016-08-02 12:46:53','0'),(11,11,'aml0ZW5kcmEuYWxpdmVuZXRzb2x1dGlvbnNAZ21haWwuY29t1470220145','jitendra.alivenetsolutions@gmail.com','0',0,0,'2016-08-03 15:59:05','2016-08-03 15:59:05','0'),(12,12,'bGF4bWlAZ21haWwuY29t1470220653','laxmi@gmail.com','0',0,0,'2016-08-03 16:07:33','2016-08-03 16:07:33','0'),(13,13,'YXJ1bkBnbWFpbC5jb201470308770','arun@gmail.com','0',0,0,'2016-08-04 16:36:10','2016-08-04 16:36:10','0'),(14,14,'YXJ1bnp6QGdtYWlsLmNvbQ1470890366','arunzz@gmail.com','0',0,0,'2016-08-11 10:09:26','2016-08-11 10:09:26','0'),(15,15,'YmluZHVraGFydWIrMUBnbWFpbC5jb201470981764','bindukharub+1@gmail.com','0',0,0,'2016-08-12 11:32:44','2016-08-12 11:32:44','0'),(16,16,'YmluZHVfa2hhcnViQHJlZGlmZm1haWwuY29t1471070813','bindu_kharub@rediffmail.com','0',0,0,'2016-08-13 12:16:53','2016-08-13 12:16:53','0'),(17,17,'YmluZHVAZ21haWwuY29t1471412473','bindu@gmail.com','0',0,0,'2016-08-17 11:11:13','2016-08-17 11:11:13','0'),(18,18,'YmF0cmExMjNAZ21haWwuY29t1471588516','batra123@gmail.com','0',0,0,'2016-08-19 12:05:16','2016-08-19 12:05:16','0'),(19,19,'c3VyYWpAZ21haWwuY29t1471950949','suraj@gmail.com','0',0,0,'2016-08-23 16:45:49','2016-08-23 16:45:49','0'),(20,20,'ISMkJUBnbWFpbC5jb201472019889','!#$%@gmail.com','0',0,0,'2016-08-24 11:54:49','2016-08-24 11:54:49','0'),(21,21,'cmFqdTEyOUBnbWFpbC5jb201473167963','raju129@gmail.com','0',0,0,'2016-09-06 18:49:23','2016-09-06 18:49:23','0'),(22,22,'YmluZHVraGFydWJAcmVkaWZmbWFpbC5jb201473405585','bindukharub@rediffmail.com','0',0,0,'2016-09-09 12:49:45','2016-09-09 12:49:45','0'),(23,23,'aGFrYW4uYXJhc0BsaXZlLmF01479577501','hakan.aras@live.at','0',0,0,'2016-11-19 18:45:01','2016-11-19 18:45:01','0'),(33,39,'eyJpdiI6IkFTK0dGTWZpZUpoY2ZrbXFlKytoY2c9PSIsInZhbHVlIjoiSDhUdkNYbmpCMjJlQ2ltcU8xcXZlVXlqZ1NVd1NwZ2w0Rk4yakJiMkNGUWVcL0FRaWtKUHIxTUl5RnA1eTR1d3MiLCJtYWMiOiI1NzYwNWY0MTg4NDhkZDIxYmNmNzlmNjE1YTBhMTY0YjBmZGIyMmIxODliOWUyYzEzMDk4YmMzZGY5YjJjMTBjIn0=','hakan.aras@wegwerfemail.de','0',0,0,'2016-12-02 12:04:33','2016-12-02 12:04:33','0'),(34,40,'eyJpdiI6ImRhMGVicGdzKzhDblBjb1dRcVpEU0E9PSIsInZhbHVlIjoiWjJEbWZsemVjTlJTZlkrdFJDNVdkTUhXK0RiSFBrQ1Q0V2xxaFBxcFhiVWwyZDZFNStOZ3BzRW4zSTl1bzBzRyIsIm1hYyI6ImNmYmU2ZDY4NzBjNDJlYjhkNDc5MTU4NDA0YmNmNWVmOTYxZjAxMTM2NjhjYTdmYjU1NzY2ZWZkMGI1OTk1ZjIifQ==','hakan.aras1@wegwerfemail.de','0',0,0,'2016-12-02 12:11:19','2016-12-02 12:11:19','0');
/*!40000 ALTER TABLE `lie_front_user_email_verifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_user_login_logs`
--

DROP TABLE IF EXISTS `lie_front_user_login_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_user_login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_user_login_logs`
--

LOCK TABLES `lie_front_user_login_logs` WRITE;
/*!40000 ALTER TABLE `lie_front_user_login_logs` DISABLE KEYS */;
INSERT INTO `lie_front_user_login_logs` VALUES (1,1,'O','180.151.7.42','2016-07-19 17:28:18',1,1,'2016-07-19 17:28:18','2016-07-19 17:28:18','1'),(2,3,'O','80.110.92.176','2016-07-20 05:38:25',3,3,'2016-07-20 05:38:25','2016-07-20 05:38:25','1'),(3,3,'O','80.110.92.176','2016-07-20 07:29:26',3,3,'2016-07-20 07:29:26','2016-07-20 07:29:26','1'),(4,1,'O','180.151.7.42','2016-07-20 11:40:26',1,1,'2016-07-20 11:40:26','2016-07-20 11:40:26','1'),(5,4,'O','180.151.7.42','2016-07-21 10:29:20',4,4,'2016-07-21 10:29:20','2016-07-21 10:29:20','1'),(6,4,'O','180.151.7.42','2016-07-21 13:54:19',4,4,'2016-07-21 13:54:19','2016-07-21 13:54:19','1'),(7,4,'O','180.151.7.42','2016-07-21 17:57:34',4,4,'2016-07-21 17:57:34','2016-07-21 17:57:34','1'),(8,3,'L','80.110.92.176','2016-07-22 14:22:25',3,3,'2016-07-22 14:22:25','2016-07-22 14:22:25','1'),(9,3,'O','80.110.92.176','2016-07-22 16:54:31',3,3,'2016-07-22 16:54:31','2016-07-22 16:54:31','1'),(10,4,'O','80.110.92.176','2016-07-22 16:55:56',4,4,'2016-07-22 16:55:56','2016-07-22 16:55:56','1'),(11,4,'O','80.110.92.176','2016-07-22 17:02:59',4,4,'2016-07-22 17:02:59','2016-07-22 17:02:59','1'),(12,4,'L','80.110.92.176','2016-07-22 17:06:46',4,4,'2016-07-22 17:06:46','2016-07-22 17:06:46','1'),(13,4,'O','80.110.92.176','2016-07-22 17:07:10',4,4,'2016-07-22 17:07:10','2016-07-22 17:07:10','1'),(14,3,'O','80.110.92.176','2016-07-22 17:07:25',3,3,'2016-07-22 17:07:25','2016-07-22 17:07:25','1'),(15,4,'O','180.151.7.42','2016-07-22 18:24:09',4,4,'2016-07-22 18:24:09','2016-07-22 18:24:09','1'),(16,3,'O','80.110.92.176','2016-07-23 04:13:30',3,3,'2016-07-23 04:13:30','2016-07-23 04:13:30','1'),(17,1,'O','180.151.7.42','2016-07-23 16:10:19',1,1,'2016-07-23 16:10:19','2016-07-23 16:10:19','1'),(18,3,'O','80.110.92.176','2016-07-24 04:29:25',3,3,'2016-07-24 04:29:25','2016-07-24 04:29:25','1'),(19,3,'O','80.110.92.176','2016-07-24 04:30:12',3,3,'2016-07-24 04:30:12','2016-07-24 04:30:12','1'),(20,3,'O','80.110.92.176','2016-07-25 06:07:13',3,3,'2016-07-25 06:07:13','2016-07-25 06:07:13','1'),(21,3,'O','80.110.92.176','2016-07-25 06:10:24',3,3,'2016-07-25 06:10:24','2016-07-25 06:10:24','1'),(22,3,'L','80.110.92.176','2016-07-25 06:12:18',3,3,'2016-07-25 06:12:18','2016-07-25 06:12:18','1'),(23,3,'O','80.110.92.176','2016-07-25 06:12:25',3,3,'2016-07-25 06:12:25','2016-07-25 06:12:25','1'),(24,3,'O','80.110.92.176','2016-07-25 06:12:36',3,3,'2016-07-25 06:12:36','2016-07-25 06:12:36','1'),(25,3,'O','80.110.92.176','2016-07-25 06:15:42',3,3,'2016-07-25 06:15:42','2016-07-25 06:15:42','1'),(26,3,'O','80.110.92.176','2016-07-25 07:19:20',3,3,'2016-07-25 07:19:20','2016-07-25 07:19:20','1'),(27,3,'O','80.110.92.176','2016-07-25 07:44:29',3,3,'2016-07-25 07:44:29','2016-07-25 07:44:29','1'),(28,5,'O','180.151.7.42','2016-07-25 11:33:45',5,5,'2016-07-25 11:33:45','2016-07-25 11:33:45','1'),(29,5,'O','180.151.7.42','2016-07-25 11:46:00',5,5,'2016-07-25 11:46:00','2016-07-25 11:46:00','1'),(30,4,'O','180.151.7.42','2016-07-25 12:13:53',4,4,'2016-07-25 12:13:53','2016-07-25 12:13:53','1'),(31,5,'O','180.151.7.42','2016-07-25 12:23:54',5,5,'2016-07-25 12:23:54','2016-07-25 12:23:54','1'),(32,4,'O','180.151.7.42','2016-07-25 12:33:17',4,4,'2016-07-25 12:33:17','2016-07-25 12:33:17','1'),(33,4,'O','180.151.7.42','2016-07-25 12:36:25',4,4,'2016-07-25 12:36:25','2016-07-25 12:36:25','1'),(34,5,'O','180.151.7.42','2016-07-25 12:36:53',5,5,'2016-07-25 12:36:53','2016-07-25 12:36:53','1'),(35,5,'O','180.151.7.42','2016-07-25 12:39:41',5,5,'2016-07-25 12:39:41','2016-07-25 12:39:41','1'),(36,5,'O','180.151.7.43','2016-07-27 13:00:24',5,5,'2016-07-27 13:00:24','2016-07-27 13:00:24','1'),(37,7,'L','180.151.7.43','2016-07-27 13:09:12',7,7,'2016-07-27 13:09:12','2016-07-27 13:09:12','1'),(38,7,'O','180.151.7.43','2016-07-27 13:18:18',7,7,'2016-07-27 13:18:18','2016-07-27 13:18:18','1'),(39,1,'O','180.151.7.42','2016-07-27 16:19:35',1,1,'2016-07-27 16:19:35','2016-07-27 16:19:35','1'),(40,7,'O','180.151.7.42','2016-07-28 12:09:17',7,7,'2016-07-28 12:09:17','2016-07-28 12:09:17','1'),(41,7,'O','180.151.7.42','2016-07-28 12:42:10',7,7,'2016-07-28 12:42:10','2016-07-28 12:42:10','1'),(42,8,'O','180.151.7.42','2016-07-28 12:44:22',8,8,'2016-07-28 12:44:22','2016-07-28 12:44:22','1'),(43,4,'O','180.151.7.42','2016-07-29 13:00:16',4,4,'2016-07-29 13:00:16','2016-07-29 13:00:16','1'),(44,4,'O','180.151.7.42','2016-07-29 15:52:13',4,4,'2016-07-29 15:52:13','2016-07-29 15:52:13','1'),(45,1,'O','180.151.7.42','2016-07-30 17:39:14',1,1,'2016-07-30 17:39:14','2016-07-30 17:39:14','1'),(46,1,'O','180.151.7.42','2016-08-01 17:03:55',1,1,'2016-08-01 17:03:55','2016-08-01 17:03:55','1'),(47,11,'O','180.151.7.42','2016-08-03 16:05:12',11,11,'2016-08-03 16:05:12','2016-08-03 16:05:12','1'),(48,11,'O','180.151.7.42','2016-08-03 16:07:00',11,11,'2016-08-03 16:07:00','2016-08-03 16:07:00','1'),(49,3,'O','80.110.92.176','2016-08-04 08:38:02',3,3,'2016-08-04 08:38:02','2016-08-04 08:38:02','1'),(50,4,'O','180.151.7.42','2016-08-04 16:20:44',4,4,'2016-08-04 16:20:44','2016-08-04 16:20:44','1'),(51,4,'O','180.151.7.42','2016-08-04 16:21:41',4,4,'2016-08-04 16:21:41','2016-08-04 16:21:41','1'),(52,3,'L','80.110.92.176','2016-08-05 04:03:39',3,3,'2016-08-05 04:03:39','2016-08-05 04:03:39','1'),(53,5,'O','180.151.7.42','2016-08-10 17:26:44',5,5,'2016-08-10 17:26:44','2016-08-10 17:26:44','1'),(54,5,'O','180.151.7.42','2016-08-10 18:01:26',5,5,'2016-08-10 18:01:26','2016-08-10 18:01:26','1'),(55,3,'O','80.110.89.221','2016-08-11 04:36:02',3,3,'2016-08-11 04:36:02','2016-08-11 04:36:02','1'),(56,4,'O','180.151.7.42','2016-08-11 10:57:18',4,4,'2016-08-11 10:57:18','2016-08-11 10:57:18','1'),(57,5,'O','180.151.7.42','2016-08-12 11:30:10',5,5,'2016-08-12 11:30:10','2016-08-12 11:30:10','1'),(58,4,'O','80.110.89.221','2016-08-15 06:11:46',4,4,'2016-08-15 06:11:46','2016-08-15 06:11:46','1'),(59,3,'O','80.110.89.221','2016-08-15 06:23:51',3,3,'2016-08-15 06:23:51','2016-08-15 06:23:51','1'),(60,4,'O','80.110.89.221','2016-08-15 06:25:17',4,4,'2016-08-15 06:25:17','2016-08-15 06:25:17','1'),(61,1,'O','80.110.89.221','2016-08-15 06:37:14',1,1,'2016-08-15 06:37:14','2016-08-15 06:37:14','1'),(62,17,'L','180.151.7.43','2016-08-17 11:15:42',17,17,'2016-08-17 11:15:42','2016-08-17 11:15:42','1'),(63,17,'O','180.151.7.43','2016-08-17 11:51:22',17,17,'2016-08-17 11:51:22','2016-08-17 11:51:22','1'),(64,17,'O','180.151.7.43','2016-08-17 11:55:02',17,17,'2016-08-17 11:55:02','2016-08-17 11:55:02','1'),(65,11,'O','180.151.7.43','2016-08-17 18:03:51',11,11,'2016-08-17 18:03:51','2016-08-17 18:03:51','1'),(66,3,'O','80.110.89.221','2016-08-18 02:41:41',3,3,'2016-08-18 02:41:41','2016-08-18 02:41:41','1'),(67,1,'L','180.151.7.43','2016-08-18 11:55:35',1,1,'2016-08-18 11:55:35','2016-08-18 11:55:35','1'),(68,1,'O','180.151.7.43','2016-08-18 17:48:56',1,1,'2016-08-18 17:48:56','2016-08-18 17:48:56','1'),(69,1,'O','180.151.7.43','2016-08-19 09:49:26',1,1,'2016-08-19 09:49:26','2016-08-19 09:49:26','1'),(70,1,'L','180.151.7.43','2016-08-19 10:44:11',1,1,'2016-08-19 10:44:11','2016-08-19 10:44:11','1'),(71,4,'O','180.151.7.43','2016-08-19 12:00:02',4,4,'2016-08-19 12:00:02','2016-08-19 12:00:02','1'),(72,4,'O','180.151.7.43','2016-08-19 12:04:38',4,4,'2016-08-19 12:04:38','2016-08-19 12:04:38','1'),(73,18,'O','180.151.7.43','2016-08-19 12:44:56',18,18,'2016-08-19 12:44:56','2016-08-19 12:44:56','1'),(74,5,'O','180.151.7.43','2016-08-19 17:10:45',5,5,'2016-08-19 17:10:45','2016-08-19 17:10:45','1'),(75,3,'O','80.110.127.191','2016-08-20 04:18:52',3,3,'2016-08-20 04:18:52','2016-08-20 04:18:52','1'),(76,1,'O','180.151.7.43','2016-08-23 11:10:48',1,1,'2016-08-23 11:10:48','2016-08-23 11:10:48','1'),(77,4,'O','180.151.7.43','2016-08-23 16:34:04',4,4,'2016-08-23 16:34:04','2016-08-23 16:34:04','1'),(78,20,'O','180.151.7.43','2016-08-24 16:03:51',20,20,'2016-08-24 16:03:51','2016-08-24 16:03:51','1'),(79,11,'O','180.151.7.43','2016-08-24 16:10:06',11,11,'2016-08-24 16:10:06','2016-08-24 16:10:06','1'),(80,11,'O','180.151.7.43','2016-08-24 16:21:15',11,11,'2016-08-24 16:21:15','2016-08-24 16:21:15','1'),(81,1,'O','180.151.7.42','2016-08-27 12:28:56',1,1,'2016-08-27 12:28:56','2016-08-27 12:28:56','1'),(82,3,'O','80.110.92.92','2016-08-30 06:30:27',3,3,'2016-08-30 06:30:27','2016-08-30 06:30:27','1'),(83,3,'O','80.110.92.92','2016-08-31 04:40:01',3,3,'2016-08-31 04:40:01','2016-08-31 04:40:01','1'),(84,3,'O','80.110.92.92','2016-08-31 06:00:04',3,3,'2016-08-31 06:00:04','2016-08-31 06:00:04','1'),(85,1,'O','180.151.7.42','2016-08-31 18:55:14',1,1,'2016-08-31 18:55:14','2016-08-31 18:55:14','1'),(86,3,'O','80.110.92.92','2016-09-02 08:49:19',3,3,'2016-09-02 08:49:19','2016-09-02 08:49:19','1'),(87,3,'O','80.110.92.92','2016-09-03 08:05:12',3,3,'2016-09-03 08:05:12','2016-09-03 08:05:12','1'),(88,5,'O','180.151.7.43','2016-09-08 13:28:04',5,5,'2016-09-08 13:28:04','2016-09-08 13:28:04','1'),(89,22,'L','180.151.7.43','2016-09-09 12:50:35',22,22,'2016-09-09 12:50:35','2016-09-09 12:50:35','1'),(90,4,'O','180.151.7.43','2016-09-09 18:22:39',4,4,'2016-09-09 18:22:39','2016-09-09 18:22:39','1'),(91,3,'O','80.110.92.92','2016-09-10 02:11:42',3,3,'2016-09-10 02:11:42','2016-09-10 02:11:42','1'),(92,11,'O','180.151.7.43','2016-09-10 16:06:57',11,11,'2016-09-10 16:06:57','2016-09-10 16:06:57','1'),(93,11,'O','180.151.7.43','2016-09-10 17:07:40',11,11,'2016-09-10 17:07:40','2016-09-10 17:07:40','1'),(94,4,'O','180.151.7.43','2016-09-10 17:08:06',4,4,'2016-09-10 17:08:06','2016-09-10 17:08:06','1'),(95,4,'O','180.151.7.43','2016-09-10 18:07:46',4,4,'2016-09-10 18:07:46','2016-09-10 18:07:46','1'),(96,3,'O','80.110.92.92','2016-09-14 07:53:53',3,3,'2016-09-14 07:53:53','2016-09-14 07:53:53','1'),(97,23,'O','::1','2016-11-30 20:04:35',23,23,'2016-11-30 20:04:35','2016-11-30 20:04:35','1'),(98,23,'O','::1','2016-12-02 00:59:45',23,23,'2016-12-02 00:59:45','2016-12-02 00:59:45','1'),(99,23,'O','::1','2016-12-02 02:29:03',23,23,'2016-12-02 02:29:03','2016-12-02 02:29:03','1'),(100,39,'O','::1','2016-12-02 12:18:34',39,39,'2016-12-02 12:18:34','2016-12-02 12:18:34','1'),(101,23,'O','::1','2016-12-30 00:45:04',23,23,'2016-12-30 00:45:04','2016-12-30 00:45:04','1'),(102,23,'O','::1','2016-12-30 05:45:37',23,23,'2016-12-30 05:45:37','2016-12-30 05:45:37','1'),(103,23,'O','::1','2016-12-30 19:55:01',23,23,'2016-12-30 19:55:01','2016-12-30 19:55:01','1'),(104,23,'O','::1','2017-01-10 18:12:03',23,23,'2017-01-10 18:12:03','2017-01-10 18:12:03','1'),(105,23,'O','::1','2017-01-10 18:54:02',23,23,'2017-01-10 18:54:02','2017-01-10 18:54:02','1'),(106,23,'O','::1','2017-01-10 19:19:26',23,23,'2017-01-10 19:19:26','2017-01-10 19:19:26','1');
/*!40000 ALTER TABLE `lie_front_user_login_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_user_mobile_verifications`
--

DROP TABLE IF EXISTS `lie_front_user_mobile_verifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_user_mobile_verifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `is_verified` enum('0','1') NOT NULL COMMENT '0->no, 1->yes',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_user_mobile_verifications`
--

LOCK TABLES `lie_front_user_mobile_verifications` WRITE;
/*!40000 ALTER TABLE `lie_front_user_mobile_verifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_front_user_mobile_verifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_user_social_maps`
--

DROP TABLE IF EXISTS `lie_front_user_social_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_user_social_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `social_type` enum('F','T','G') NOT NULL COMMENT 'F->Facebook,T->Twitter,G->Google',
  `social_id` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_user_social_maps`
--

LOCK TABLES `lie_front_user_social_maps` WRITE;
/*!40000 ALTER TABLE `lie_front_user_social_maps` DISABLE KEYS */;
INSERT INTO `lie_front_user_social_maps` VALUES (50,32,'G','107262330897010645175',32,32,'2016-06-13 10:38:13','2016-06-13 10:38:13','1'),(51,1,'T','740447293791477760',1,1,'2016-06-13 10:43:42','2016-06-13 10:43:42','1'),(52,1,'F','1059561607468700',1,1,'2016-06-27 16:24:35','2016-06-27 16:24:35','1'),(53,3,'F','1073782192713308',3,3,'2016-07-20 05:26:32','2016-07-20 05:26:32','1'),(54,5,'T','2578991132',5,5,'2016-07-25 11:56:20','2016-07-25 11:56:20','1'),(55,4,'T','1699869132',4,4,'2016-07-25 12:13:46','2016-07-25 12:13:46','1'),(56,5,'F','1137450652988960',5,5,'2016-07-25 12:24:16','2016-07-25 12:24:16','1'),(57,4,'F','10208356083958533',4,4,'2016-07-25 12:36:18','2016-07-25 12:36:18','1'),(58,5,'F','130625874047317',5,5,'2016-08-10 17:26:18','2016-08-10 17:26:18','1');
/*!40000 ALTER TABLE `lie_front_user_social_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_user_subscriptions`
--

DROP TABLE IF EXISTS `lie_front_user_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_user_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT 'User',
  `email` varchar(50) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_user_subscriptions`
--

LOCK TABLES `lie_front_user_subscriptions` WRITE;
/*!40000 ALTER TABLE `lie_front_user_subscriptions` DISABLE KEYS */;
INSERT INTO `lie_front_user_subscriptions` VALUES (1,3,'Lieferzonasat','info@lieferzonas.at','1',3,3,'2016-10-04 04:31:56','2016-10-04 04:31:56');
/*!40000 ALTER TABLE `lie_front_user_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_front_users`
--

DROP TABLE IF EXISTS `lie_front_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_front_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(50) NOT NULL DEFAULT '0',
  `activation_key` varchar(150) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_front_users`
--

LOCK TABLES `lie_front_users` WRITE;
/*!40000 ALTER TABLE `lie_front_users` DISABLE KEYS */;
INSERT INTO `lie_front_users` VALUES (1,'rajlakshmi.alivenetsolution@gmail.com','$2y$10$L.8p7gfq2ayt5lI/k60ZiO6bcy/PWiSf2/3uixHZMpcunFbv1/G7a','rmbOqoAO9E3NbIClUbbGasQuFwb1bl15r6ELjPBl67F5i3UCMJ','cmFqbGFrc2htaS5hbGl2ZW5ldHNvbHV0aW9uQGdtYWlsLmNvbQ==23',0,0,'2016-07-19 17:20:48','2016-09-12 16:08:58','1'),(3,'info@lieferzonas.at','$2y$10$EomKujASjbkzWSxQOP./.uP1B0q5wbcHokZeEMAJJp8kHg5OUhfpu','JYbi9uSJkOxYFntlKNHir6bbWLuHdIqCIgrGUrIwvzVuHRnCyR','aW5mb0BsaWVmZXJ6b25hcy5hdA==13',0,0,'2016-07-20 05:26:32','2016-09-14 07:53:53','1'),(4,'nikhil@gmail.com','$2y$10$ZJMSley6pCnhhBskZe73QuuXfpVLI38FBI/hxrzxsn4B.i.dV7qEG','oReDCZj6bnbHnN00LqkaIFvuW5jjbHB3MoXT1h4QSyrdS3h9rP','',0,0,'2016-07-20 19:05:26','2016-09-10 18:07:46','1'),(5,'bindukharub@gmail.com','$2y$10$ZC9ppM1X2Mk8ANcszmLvb.w9WVoN/46NJF2HFeaM9gbooeoqtINSq','Q4jJBWI6jLlJ4DB713eAz3ONbBimnBH1Xcb6y3vfVrQHaQYdEx','',0,0,'2016-07-21 12:11:02','2016-09-08 13:28:04','1'),(6,'belgien@lieferzonas.at','$2y$10$z6urY1f5u/qbEX0SnWSypuVPl1NUB5WhFR4CG/2eAeMFM3JETyu3G','e1K8MOnZdo9sgCufKJh82mlhs7frQkGEpbP3il0HDV7OqD9Qrw','',0,0,'2016-07-27 08:48:44','2016-07-27 08:48:44','1'),(7,'bindukharub123@gmail.com','$2y$10$Lu.PH/yIe9nd0u4IYAXHE.7oUlMcPBol7KWsHNxMnbKBImgNcod6a','KIdYHntYxrsgzDDxoX2nqGHlleAjvqcNFDtunkrpuvKqrF2DU2','',0,0,'2016-07-27 13:03:22','2016-07-28 12:42:10','1'),(8,'bindu@gmail.com','$2y$10$1IKnupV885/vcU6jh5jWlu5P4rBC1Qh5aIVMSuMsd0SHSpALUBwTq','I881uegqT8DLfx5npZuZWmP1YczJKPoGKMeAkImfF5Yw87sXvj','',0,0,'2016-07-28 12:30:59','2016-08-12 12:53:52','2'),(9,'anjali@gmail.com','$2y$10$4ZliI7Kdod8HwGxuRJQm0umlMbQA20k2yAsYaeBaezk.VJmH/hety','0','',0,0,'2016-08-01 17:04:34','2016-08-01 17:04:34','1'),(10,'arun.alivenetsolution@gmail.com','$2y$10$L74ei59OYIWMyJupnMNBE.J40WlIu1EWniie3dw5C.TIC150U9Xcy','0','',0,0,'2016-08-02 12:46:53','2016-08-02 12:46:53','1'),(11,'jitendra.alivenetsolutions@gmail.com','$2y$10$y/eOq0rQAU5GpvQHtOPY/ue2b5snwMwR2ATPkhfRdKP67j5jGqPc2','gEinZgxQp6RabYvhQlpxd4FioFTQVHRjXwIlyFiBW3SjqIly2C','',0,0,'2016-08-03 15:59:05','2016-09-10 17:07:40','1'),(12,'laxmi@gmail.com','$2y$10$4XyM9ZZPRz3HsO0O44M6BuHCTKR3MuW7ajQafdicv1VMj4Bf.KAIG','0','',0,0,'2016-08-03 16:07:33','2016-08-03 16:07:33','1'),(13,'arun@gmail.com','$2y$10$ZC/6e/hsC4FcanP2Jz5Z5ezKEB0uugUMnBJrYK7qJaFI2z/HXZODG','0','',0,0,'2016-08-04 16:36:10','2016-08-04 16:36:10','1'),(14,'arunzz@gmail.com','$2y$10$/1Hlgk9iWRXYq9Xh87ABVuqRkijvSMAsSc2nRyHZegqQx.ENZzYcu','0','',0,0,'2016-08-11 10:09:26','2016-08-11 10:09:26','1'),(15,'bindukharub+1@gmail.com','$2y$10$P1kqnQvSnAxEKplMwqV62e.G1DMD5ZPNDHwq/g9lTSSQu7JV1y6NW','0','',0,0,'2016-08-12 11:32:44','2016-08-12 12:47:00','2'),(16,'bindu_kharub@rediffmail.com','$2y$10$D4G9ZqZYyGpWfCUUL7C.NOsbMNhjOC5HCeRhAov0EieVryiuaBa/q','k72kGX0k06PGLWm8pfmO1WdmBtzyoQRn5otyPdDfihJelM3UjD','',0,0,'2016-08-13 12:16:53','2016-09-06 11:17:37','2'),(17,'bindu@gmail.com','$2y$10$Unsdvh4EsU3iahDTrKqAnumKTyEV3hCIlW6CqgIlHfDRunm5nTanq','0a07Udw1mVpLKhpSzEiEZYUmdvLtBqh9Ac74HwnmD1sIm8fASt','',0,0,'2016-08-17 11:11:13','2016-09-09 15:16:22','2'),(18,'batra123@gmail.com','$2y$10$wBU30sHNp4c9dPrBFB8lcer1am4O26JBWaJqviCkrvn5rgoEeF782','AbT07MaukTxhbKYq89jcO2pq18HZHJ4cHrpjyC8MaYijuqDat0','',0,0,'2016-08-19 12:05:16','2016-08-19 12:44:56','1'),(19,'suraj@gmail.com','$2y$10$b/ZKLIFYyECS68MBTfUbIeW6zoQt9C8efh..O9z/jDSbmD8vBEMFG','cZLyhCwh9PJtoXalP4AVIRWGUB1KaB2f77gsjQ5up3jgDX4NU9','',0,0,'2016-08-23 16:45:49','2016-08-23 16:45:49','1'),(20,'!#$%@gmail.com','$2y$10$yZRA5HKNrBqj.pQ7KxS0Re9D9rB6ReGj2.DWYDL2L4BeAnWldVJX6','gnkGmJjBvs7DrpODiGLs4sR9SNUW8vfvc03KbzDeNlRgVdjUXG','',0,0,'2016-08-24 11:54:49','2016-08-24 16:03:51','1'),(21,'raju129@gmail.com','$2y$10$mmFZ3F1bmHSsaUyAb0wRc.uTTaSLTGFXZhkZcx9KhszPTL6SGnRBC','cTNcRafhCAeiD9xjidx9PkecTBSYYjmo9cmM5DAvaCb3RTvI7j','',0,0,'2016-09-06 18:49:23','2016-09-06 18:49:23','1'),(22,'bindukharub@rediffmail.com','$2y$10$XzmIUr6JVzNz/z1FECg/fusZu6uxQ/CLe53rqjV2/NL0uujhrwxyS','QANE4clfzfn6wiJicqqpO3iqH4bYvWRFJFWQS3J45WXcEOj4VN','',0,0,'2016-09-09 12:49:45','2016-09-09 15:16:31','2'),(23,'hakan.aras@live.at','$2y$10$ceWiTT.9FNpJxzSUj2omQuNRaucx4kAvOc0I3HkeLvabYXHndCt6m','9exWw5J5a6MJgHunTm64dcO4alRtyxo0LxbT1C2isi5lqCoOGa','',0,0,'2016-11-19 18:45:01','2017-01-10 19:19:26','1'),(39,'hakan.aras@wegwerfemail.de','$2y$10$yEd8DX9/8RC0tUQCkBskHeOhDcKhEcqalC9aQdK3YpuDYYRFyT82.','20WIY2OvuBFQ7lEo1typrbVVMI7VhkVEpRrkEgQogbYXCXQn8o','',0,0,'2016-12-02 12:04:33','2016-12-02 12:18:34','1'),(40,'hakan.aras1@wegwerfemail.de','$2y$10$qGqVjl1By8sKswB9jfCiCuJsIypV0uTYoGspzwKE0QCzpruN1VZG6','0','',0,0,'2016-12-02 12:11:19','2016-12-02 12:11:19','1'),(41,'hakan.aras@byom.de','$2y$10$WW/VVct1r.5puis3bIAXreRiJZovUysQfq6n6wtvxXtrcbK1K9ktS','0','',1,1,'2016-12-31 09:46:05','2016-12-31 09:46:05','1'),(42,'hakan.aras1@byom.de','$2y$10$fFrN8sTKK20OAwmxWzbvseLl5QUBDzLeU3aNpcrNiUXbjL1oMkiqW','0','',1,1,'2016-12-31 09:51:06','2016-12-31 09:51:06','1'),(43,'hakan.aras2@byom.de','$2y$10$COLX3CNSWezjJ6DJgvqZG.EmwiDGP32lBosJhYZ1N7D/3DZZkXA/i','0','',1,1,'2016-12-31 09:53:10','2016-12-31 09:53:10','1'),(44,'','$2y$10$6FepGdyJ5wY2FElDN9EFL.QKukvXs4B6W8w93uDk9tbr9X6aOB9EC','0','',1,1,'2017-01-03 01:16:35','2017-01-03 01:16:35','1');
/*!40000 ALTER TABLE `lie_front_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_kitchens`
--

DROP TABLE IF EXISTS `lie_kitchens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_kitchens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_kitchens`
--

LOCK TABLES `lie_kitchens` WRITE;
/*!40000 ALTER TABLE `lie_kitchens` DISABLE KEYS */;
INSERT INTO `lie_kitchens` VALUES (1,'Pizza','pizza pizza','2016-07-19 00:00:00','2107-12-31 00:00:00',1,2,2,'2016-07-19 16:43:17','2016-07-19 16:43:17','1'),(2,'Indisch','Indische Küche','2016-07-19 00:00:00','2104-05-12 00:00:00',4,2,2,'2016-08-31 05:59:37','2016-08-31 05:59:37','1'),(3,'Mexikanisch','Mexikanische Küche','2016-07-19 00:00:00','2091-10-17 00:00:00',3,2,2,'2016-08-31 05:58:41','2016-08-31 05:58:41','1'),(4,'Chinese','chinese','2016-07-19 00:00:00','2065-06-16 00:00:00',4,2,2,'2016-07-19 16:46:09','2016-07-19 16:46:09','1'),(5,'Continental','continental','2016-07-19 00:00:00','2077-06-16 00:00:00',5,2,2,'2016-07-19 16:47:00','2016-07-19 16:47:00','1'),(6,'Spicey','spicey','2016-07-19 00:00:00','2105-10-20 00:00:00',6,2,2,'2016-07-19 16:47:56','2016-07-19 16:47:56','1'),(7,'Non Veg','Non veg','2016-07-19 00:00:00','2026-03-04 00:00:00',7,2,2,'2016-07-19 16:49:11','2016-07-19 16:49:11','1'),(8,'Cheesey','cheese','2016-07-19 00:00:00','2091-08-17 00:00:00',8,2,2,'2016-08-30 15:53:43','2016-08-30 15:53:43','1'),(9,'Italian','italian','2016-07-19 00:00:00','2078-07-14 00:00:00',9,2,2,'2016-07-19 16:51:55','2016-07-19 16:51:55','1'),(10,'Desi tadka','desi tadka','2016-07-19 00:00:00','2079-07-13 00:00:00',10,2,2,'2016-07-19 16:54:54','2016-07-19 16:54:54','1'),(11,'asdasdas','dasdasdasd','2016-07-20 00:00:00','2016-07-22 00:00:00',5,2,2,'2016-07-19 18:04:33','2016-07-19 18:05:00','2'),(12,'Italienische Kueche','Italienische Kueche','2016-07-20 00:00:00','2021-11-25 00:00:00',1,2,2,'2016-08-12 06:39:42','2016-08-12 06:39:42','1'),(13,'South Kitchen','Just for test','2016-08-02 00:00:00','2016-08-10 00:00:00',1,2,2,'2016-08-02 13:11:42','2016-08-02 13:11:42','1'),(14,'CHINA AND THAI KIN','Just For test','2016-08-02 00:00:00','2016-08-10 00:00:00',2,2,2,'2016-08-02 13:13:09','2016-08-02 13:13:09','1'),(15,'North Indian','Just For test','2016-08-22 00:00:00','2017-08-30 00:00:00',2,2,2,'2016-08-24 02:48:31','2016-08-24 02:48:31','1'),(16,'Pasta','Italienische Pasta gerichte','2016-08-24 00:00:00','2017-08-25 00:00:00',10,2,2,'2016-08-24 04:32:01','2016-08-24 04:32:01','1'),(17,'Salate','Salate','2016-08-24 00:00:00','2019-08-26 00:00:00',19,2,2,'2016-08-24 07:03:27','2016-08-24 07:03:27','1'),(18,'Italienisch','Italienische Küche','2016-08-31 00:00:00','2020-08-31 00:00:00',1,2,2,'2016-08-31 05:57:01','2016-08-31 05:57:01','1'),(19,'test','test test test','2016-09-05 00:00:00','2016-09-06 00:00:00',1,2,2,'2016-09-05 11:05:28','2016-09-05 11:05:28','1'),(20,'Bindu','jkbh;jk\'kl\'l','2016-09-05 00:00:00','2016-09-05 00:00:00',1,2,2,'2016-09-05 11:14:19','2016-09-05 11:14:19','1');
/*!40000 ALTER TABLE `lie_kitchens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_live_feeds`
--

DROP TABLE IF EXISTS `lie_live_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_live_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_live_feeds`
--

LOCK TABLES `lie_live_feeds` WRITE;
/*!40000 ALTER TABLE `lie_live_feeds` DISABLE KEYS */;
INSERT INTO `lie_live_feeds` VALUES (1,'New Order','Nikhil places new order with Bercos','WOW Amazing. Nikhil got 10% discount, Place now and enjoy','https://www.google.co.in',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1');
/*!40000 ALTER TABLE `lie_live_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_marketing_tips`
--

DROP TABLE IF EXISTS `lie_marketing_tips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_marketing_tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_marketing_tips`
--

LOCK TABLES `lie_marketing_tips` WRITE;
/*!40000 ALTER TABLE `lie_marketing_tips` DISABLE KEYS */;
INSERT INTO `lie_marketing_tips` VALUES (1,'Test Tip','This is marketing test tip.',0,0,0,'2016-07-19 17:37:10','2016-08-21 06:09:19','2'),(2,'jkm','g dgfjfghsd fgsfhsgf shfgsh fhsgfhsf sgf sf\r\nsdf\r\nsdf\r\nsf\r\ns\r\nfsdf\r\nsf\r\nsdfsfsdfsdfsdfsdfsdfsdfsdfsdfsdf\r\nsd\r\nfsdfsdfsdf',0,0,0,'2016-08-09 06:47:29','2016-08-21 06:09:11','2'),(3,'dfgdfg','gfdgdfggfd fg dfg dfg dfg dfg',0,0,0,'2016-08-12 12:58:28','2016-08-21 06:09:11','2'),(4,'fdgdg fd','dfg dfg dfg dfg fdg dfg',0,0,0,'2016-08-12 12:58:45','2016-08-21 06:09:11','2'),(5,'sdfsdfsdf ',' dsfsdfs fsf sf sdfs sf s',0,0,0,'2016-08-12 12:58:52','2016-08-21 06:09:19','2'),(6,'sdf sdf sf ',' sdf fsd sf sdf fs f sd dsf',0,0,0,'2016-08-12 12:59:01','2016-08-21 06:09:11','2'),(7,'sfsdf','fsdfsdfsdfsdfsdfsdfdf sdf sd sdf sd fsd',0,0,0,'2016-08-12 12:59:56','2016-08-21 06:09:19','2'),(8,'f sdfsdfsdfsdfsdfdsdf',' sdfsdf sdf sfsd sdf sdf sfsd sd',0,0,0,'2016-08-12 13:00:04','2016-08-21 06:09:11','2'),(9,'fdsdsfsdfsddfsd','sdfsdfsdfsfsdfsdfsdfsdfsdfsd d sdf sdf sd',0,0,0,'2016-08-12 13:00:12','2016-08-21 06:09:11','2'),(10,'dsfdsfffdsfddfsfdsdfsfsd','sdfsdfsdfsdfsdf sdf sf sd sd sd sfd sd fsd sdf',0,0,0,'2016-08-12 13:00:23','2016-08-21 06:09:11','2'),(11,'sdfdsfsdfsdffdsfsddsffsdfdsf','dsfsdfsdfsdfsf sdf sdf sdf sdf f sdf sdf sd',0,0,0,'2016-08-12 13:00:40','2016-08-21 06:09:19','2'),(12,'dsfsdfsdffsdfdsfdsffdsfs','dsfsdffdsfsfdfsff sdf sdf dsf dsf dsf d dsf dsf df dfs dsf',0,0,0,'2016-08-12 13:01:21','2016-08-21 06:09:11','2'),(13,'dfsdfdasdasdfsasfdfdddf','dsfsdffdfsddfsdfsdfsdfsdfsdf sdf sdf sdf sdfsd fd',0,0,0,'2016-08-12 13:02:03','2016-08-21 06:09:11','2'),(14,'sadasda','sadasdasdasdsdasdd asd asd asd asd ass d',0,0,0,'2016-08-12 13:02:21','2016-08-21 06:09:11','2'),(15,'Die Lieferzeiten ausdehnen','Je weniger Restaurants in Ihrem Umkreis geöffnet sind, desto besser die Chancen für für ihr Service. \r\nPassen Sie Ihre Lieferzeiten an. \r\nViele Kunden bestellen mittlerweile ihr Mittagessen online, und noch mehr neigen dazu, spät am Abend online zu bestellen. \r\nWomöglich können Sie Ihren Umsatz ganz leicht signifikant steigern, indem Sie durchgehend liefern und abends etwas länger. \r\n\r\nProbieren Sie es aus.',0,0,0,'2016-08-21 06:14:51','2016-08-21 06:21:06','2'),(16,'Mindestbestellwert','Je kleiner der Mindestbestellwert  desto eher ordert der Kunde\r\n\r\nMindestbestellwert und Lieferkosten beeinflussen, ob ein Kunde sich für Sie entscheidet. Kleine Mindestbestellwerte wirken verlockend, \r\nhohe schrecken ab, das zeigen unsere Daten. \r\nAus Erfahrung empfehlen wir Ihnen, den Mindestbestellwert möglichst niedrig anzusetzen, \r\ndafür den Wert, ab dem Sie kostenlos liefern, etwas höher. \r\nAuf diese Weise sichern Sie sich eine möglichst \r\nhohe Zahl von Kunden und haben zugleich stets die Kosten der Lieferung gedeckt, auch wenn wenig bestellt wurde.',0,0,0,'2016-08-21 06:17:15','2016-08-21 06:21:06','2'),(17,'Bio Produkte','bald erhältlich.............',0,0,0,'2016-08-21 06:20:21','2016-08-21 06:20:21','1'),(18,'Kleidung','Das Erscheinungsbild Ihrer Mitarbeiter bleibt beim Kunden hängen. \r\nWer sich seine Mahlzeit liefern lässt, der möchte, dass der Überbringer nicht unappetitlich aussieht. \r\nUnsere Arbeitskleidung sorgt für ein professionelles Erscheinungsbild, außerdem verbreitet sie die Internetadresse, \r\nunter der Ihre Speisen zu finden sind. \r\nBeides sollte sich positiv auf Ihren Umsatz auswirken.',0,0,0,'2016-08-21 06:22:00','2016-08-21 06:22:00','1'),(19,'Aufkleber','Aufkleber für ihr Restaurant und ihr Fahrzeuge\r\n\r\nWir bieten Ihnen Aufkleber an, mit denen Sie für den Kunden kenntlich machen können, dass Ihr Restaurant bei lieferzonas.at zu finden ist. \r\nAm Eingang Ihres Geschäfts und an Ihren Auslieferfahrzeugen angebracht, sorgen diese Sticker dafür, dass sich unsere Internetadresse einprägt. \r\nWer die Adresse einmal im Hinterkopf gespeichert hat, der wird sie aufrufen, sobald sich bei ihm der Hunger meldet.\r\n',0,0,0,'2016-08-21 06:32:09','2016-08-21 06:32:09','1'),(20,'Fotos und Beschreibungen','Niemand kauft gerne die Katze im Sack. \r\nWer eine Mahlzeit bestellt, der möchte sie vorher sehen\r\n. \r\nDie Erfahrung zeigt, dass abgebildete \r\nGerichte eher verlangt und häufiger verkauft werden, weil es den Appetit anregt und das Vertrauen in die Speise erhöht, wenn der Kunde \r\nsie zuvor gesehen hat. \r\nMit den Fotos Ihrer Gerichte wenden Sie sich bitte an unsere Kundeservice, der Ihnen hilft, sie ideal zu präsentieren.\r\n\r\nGleiches gilt für Beschreibungen, speziell für exotische Gerichte, die dem Kunden nicht vertraut sein mögen. \r\nBeschreiben Sie die Zutaten, den Geschmack, die Herkunft des Gerichts. \r\nJe besser sich der Kunde vorstellen kann, was er bekommt, desto eher wird er bestellen.',0,0,0,'2016-08-21 06:34:03','2016-08-21 06:34:03','1'),(21,'Eis und Nachspeisen','Wenn Sie Eis und andere süße Leckereien in Ihr Angebot aufnehmen, hilft das, Ihren Umsatz anzukurbeln. \r\nMit dem Nachtisch verhält es sich ähnlich wie mit den Getränken: Je größer die Auswahl (und die Versuchung), \r\ndesto höher die Wahrscheinlichkeit, dass der Kunde bestellt. \r\nEin Angebot verschiedener Nachspeisen wird sich rechnen.\r\n\r\n',0,0,0,'2016-08-21 06:35:38','2016-08-21 06:35:38','1'),(22,'Speisekarte und Drink','Je vielfältiger das Angebot, desto eher werden die Kunden zur Mahlzeit ein Getränk bestellen. \r\nDas gilt vor allem in den Abendstunden für alkoholische Getränke. \r\nDie Flasche Wein zum Gericht ist beliebt, aber bestellen kann der Kunde den Wein seiner Wahl nur, wenn er ihn in Ihrer Karte findet. \r\nGenerell gilt: je breiter die Getränkeauswahl, desto höher die Wahrscheinlichkeit, dass der Kunde sich für ein Getränk entscheidet. \r\nOft macht es sich bezahlt, wenn Ihr Fahrer gekühlte Getränke im Auto bereithält. \r\nDann kann er bei jeder Auslieferung fragen, ob womöglich noch ein gekühltes Getränk gewünscht ist.\r\n\r\n',0,0,0,'2016-08-21 06:43:08','2016-08-21 06:48:36','1'),(23,'Mindestbestellwert und Lieferkosten','Mindestbestellwert und Lieferkosten beeinflussen, ob ein Kunde sich für Sie entscheidet. \r\n\r\nKleine Mindestbestellwerte wirken verlockend, \r\nhohe schrecken ab, das zeigen unsere Daten. \r\n\r\nAus Erfahrung empfehlen wir Ihnen, den Mindestbestellwert möglichst niedrig anzusetzen, \r\ndafür den Wert, ab dem Sie kostenlos liefern, etwas höher. \r\nAuf diese Weise sichern Sie sich eine möglichst hohe Zahl von Kunden und haben zugleich stets die Kosten der Lieferung gedeckt, auch wenn wenig bestellt wurde.\r\n',0,0,0,'2016-08-21 06:48:20','2016-08-21 06:48:20','1'),(24,'Neukunden Rabatt Aktivieren','kommt bald................',0,0,0,'2016-08-21 06:49:41','2016-08-21 07:04:15','1'),(25,'Stempelkarten Aktivieren ','Wenn sie stempelkarte anbieten müssen sie im kategorie -Stempelkarte Aktivieren \r\n( vorsicht wenn sie einmal aktivieren kann man es nicht mehr deaktivieren)\r\n\r\nStempelkarte ist ein gute gelegenheit stammgunden zu gewinnen, denn sie gewehren kunden stempelkarte zu sammeln.\r\nsie geben den kunden ab 5 stempelkarte, 10% des gesammten 10 bestellungen gutschrift.\r\nein bestellung ist ein Stempelkarte und ab 5 stempelkarte ist es einlössbar. \r\nMindestbestellwert muss erreicht werden.\r\nGültigkeitsdauer der stempel ist 3 monate.\r\ndie stempelkarte funktioniert nur bei onlinezahlung und ohne aktionen ',0,0,0,'2016-08-21 07:01:44','2016-08-21 07:01:44','1'),(26,'Rabatte','Der Mensch ist ein Schnäppchenjäger, auch beim Essen. \r\n\r\nUnsere Erfahrung zeigt, dass viele Kunden gezielt nach Rabatten und Sonderangeboten suchen. \r\nSorgen Sie dafür, dass diese Kunden bei Ihnen fündig werden.\r\nSetzen Sie den Preis für ein bestimmtes Gericht herab, veranstalten Sie einen Probiermonat, \r\nund profitieren Sie davon, dass Sie vom beworbenen Gericht deutlich mehr verkaufen als zuvor. \r\nHaben Sie Interesse an einer gemeinsamen Rabattaktion mit uns? Unser Kundenservice berät Sie gerne.',0,0,0,'2016-08-21 07:05:47','2016-08-21 07:06:41','1'),(27,'Die Lieferzeiten','Je weniger Restaurants in Ihrem Umkreis geöffnet sind, desto besser die Chancen für Service. \r\nPassen Sie Ihre Lieferzeiten an. \r\n\r\nViele Kunden bestellen mittlerweile ihr Mittagessen online, und noch mehr neigen dazu, spät am Abend online zu bestellen. \r\nWomöglich können Sie Ihren Umsatz ganz leicht signifikant steigern, indem Sie durchgehend liefern und abends etwas länger. \r\n\r\nProbieren Sie es aus.\r\n',0,0,0,'2016-08-21 07:10:15','2016-08-21 07:10:15','1');
/*!40000 ALTER TABLE `lie_marketing_tips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_menu_categories`
--

DROP TABLE IF EXISTS `lie_menu_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_menu_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_menu_categories`
--

LOCK TABLES `lie_menu_categories` WRITE;
/*!40000 ALTER TABLE `lie_menu_categories` DISABLE KEYS */;
INSERT INTO `lie_menu_categories` VALUES (1,'test','testssss',2,2,2,'2016-07-20 11:36:16','2016-07-20 18:12:40','1'),(2,'asdasdas','sadasds',2,2,2,'2016-07-20 11:36:38','2016-07-20 11:36:44','2');
/*!40000 ALTER TABLE `lie_menu_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_menu_extra_types`
--

DROP TABLE IF EXISTS `lie_menu_extra_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_menu_extra_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_menu_extra_types`
--

LOCK TABLES `lie_menu_extra_types` WRITE;
/*!40000 ALTER TABLE `lie_menu_extra_types` DISABLE KEYS */;
INSERT INTO `lie_menu_extra_types` VALUES (1,'Combi','ein  menü zusammen stellen',2,2,'2016-07-19 16:35:46','2016-08-24 02:30:58','1'),(2,'combi 2','combi 2',2,2,'2016-08-24 02:43:57','2016-08-24 02:44:11','1');
/*!40000 ALTER TABLE `lie_menu_extra_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_menu_extras`
--

DROP TABLE IF EXISTS `lie_menu_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_menu_extras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extra_type_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` varchar(6) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_menu_extras`
--

LOCK TABLES `lie_menu_extras` WRITE;
/*!40000 ALTER TABLE `lie_menu_extras` DISABLE KEYS */;
INSERT INTO `lie_menu_extras` VALUES (1,1,'Test Menu Extra','Testing By Rajlakshmi...','100',2,2,'2016-07-19 16:36:27','2016-07-20 10:52:25','1'),(2,1,'yes','19999999999','19',2,0,'2016-08-24 02:33:17','2016-08-24 02:33:17','1');
/*!40000 ALTER TABLE `lie_menu_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_menu_price_maps`
--

DROP TABLE IF EXISTS `lie_menu_price_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_menu_price_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `price` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_menu_price_maps`
--

LOCK TABLES `lie_menu_price_maps` WRITE;
/*!40000 ALTER TABLE `lie_menu_price_maps` DISABLE KEYS */;
INSERT INTO `lie_menu_price_maps` VALUES (1,2,1,1,'50',2,2,'2016-07-19 17:42:58','2016-07-19 17:42:58','0'),(8,2,7,3,'155',2,2,'2016-07-25 16:47:18','2016-07-25 16:47:18','0'),(9,2,7,4,'250',2,2,'2016-07-25 16:47:18','2016-07-25 16:47:18','0'),(13,1,9,3,'100',2,2,'2016-07-30 12:40:29','2016-07-30 12:40:29','0'),(16,1,11,3,'140',2,2,'2016-08-01 11:28:29','2016-08-01 11:28:29','0'),(17,4,12,3,'200',2,2,'2016-08-01 11:31:13','2016-08-01 11:31:13','0'),(18,4,12,4,'300',2,2,'2016-08-01 11:31:13','2016-08-01 11:31:13','0'),(19,8,16,3,'100',2,2,'2016-08-02 18:34:24','2016-08-02 18:34:24','0'),(20,10,17,3,'100',2,2,'2016-08-02 18:37:31','2016-08-02 18:37:31','0'),(21,10,17,4,'100',2,2,'2016-08-02 18:37:31','2016-08-02 18:37:31','0'),(22,8,18,3,'122',2,2,'2016-08-02 18:49:41','2016-08-02 18:49:41','0'),(33,1,4,3,'150',2,2,'2016-08-12 08:16:17','2016-08-12 08:16:17','0'),(43,17,21,1,'200',2,2,'2016-08-22 16:52:07','2016-08-22 16:52:07','0'),(44,17,21,2,'100',2,2,'2016-08-22 16:52:07','2016-08-22 16:52:07','0'),(47,15,19,1,'12',2,2,'2016-08-24 02:57:33','2016-08-24 02:57:33','0'),(55,17,25,1,'150',2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','0'),(56,17,25,2,'150',2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','0'),(61,5,6,1,'200',2,2,'2016-09-07 11:54:01','2016-09-07 11:54:01','0'),(62,5,6,2,'150',2,2,'2016-09-07 11:54:01','2016-09-07 11:54:01','0');
/*!40000 ALTER TABLE `lie_menu_price_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_metrics`
--

DROP TABLE IF EXISTS `lie_metrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_metrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_metrics`
--

LOCK TABLES `lie_metrics` WRITE;
/*!40000 ALTER TABLE `lie_metrics` DISABLE KEYS */;
INSERT INTO `lie_metrics` VALUES (1,'Pieces','Pc','pieces',2,2,'2016-07-19 16:23:43','2016-08-10 14:58:19','1'),(2,'Size','xl','small item',2,2,'2016-07-19 16:24:59','2016-07-19 16:24:59','1'),(4,'numbers','nos','numbers',2,2,'2016-07-19 16:27:27','2016-07-19 16:28:39','1'),(5,'Kilogram','Kg','kilogramss',2,2,'2016-07-19 16:48:39','2016-07-19 16:48:39','1'),(6,'packets','pkt','packets',2,2,'2016-07-19 16:49:14','2016-07-19 16:49:14','1'),(7,'Litre','Ltr','Litres',2,2,'2016-07-27 11:27:33','2016-07-27 11:28:11','1'),(8,'Size','M','Medium',2,2,'2016-08-21 07:22:34','2016-08-21 07:22:34','1');
/*!40000 ALTER TABLE `lie_metrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_navigation_infos`
--

DROP TABLE IF EXISTS `lie_navigation_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_navigation_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_navigation_infos`
--

LOCK TABLES `lie_navigation_infos` WRITE;
/*!40000 ALTER TABLE `lie_navigation_infos` DISABLE KEYS */;
INSERT INTO `lie_navigation_infos` VALUES (3,'wer wie was','http://alivenetsolution.co/lieferzonas/rzone/dashboard',2,2,2016,2016,'2'),(4,'ja genau','http://alivenetsolution.co/lieferzonas/hallo',2,2,2016,2016,'1');
/*!40000 ALTER TABLE `lie_navigation_infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_new_customer_bonus_user_lists`
--

DROP TABLE IF EXISTS `lie_new_customer_bonus_user_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_new_customer_bonus_user_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `bonus_value` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_new_customer_bonus_user_lists`
--

LOCK TABLES `lie_new_customer_bonus_user_lists` WRITE;
/*!40000 ALTER TABLE `lie_new_customer_bonus_user_lists` DISABLE KEYS */;
INSERT INTO `lie_new_customer_bonus_user_lists` VALUES (1,2,1,'363320334211LR2',10,'1',2,2,'2016-07-18 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `lie_new_customer_bonus_user_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_order_discounts`
--

DROP TABLE IF EXISTS `lie_order_discounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_order_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `discount_type` varchar(50) NOT NULL,
  `coupon_code` varchar(100) NOT NULL,
  `discount_price_type` enum('P','C') NOT NULL DEFAULT 'C' COMMENT 'P=>Percent,C=>Cash',
  `discount_val` varchar(100) NOT NULL,
  `discount_amount` varchar(100) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_order_discounts`
--

LOCK TABLES `lie_order_discounts` WRITE;
/*!40000 ALTER TABLE `lie_order_discounts` DISABLE KEYS */;
INSERT INTO `lie_order_discounts` VALUES (1,4,11,'363322382721TR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-22 10:27:21','2016-07-22 10:27:21'),(2,3,12,'363322433618FR2',2,'couponCode','tX39y2r1','P','10','5','0',0,0,'2016-07-22 15:36:18','2016-07-22 15:36:18'),(3,3,18,'363327325848WR2',2,'couponCode','tX39y2r1','P','10','5','0',0,0,'2016-07-27 04:58:48','2016-07-27 04:58:48'),(4,3,19,'363327330014ER2',2,'couponCode','tX39y2r1','P','10','5','0',0,0,'2016-07-27 05:00:14','2016-07-27 05:00:14'),(5,5,27,'363328410724WR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-28 13:07:24','2016-07-28 13:07:24'),(6,5,28,'363328410822GR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-28 13:08:22','2016-07-28 13:08:22'),(7,5,29,'363328410910RR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-28 13:09:10','2016-07-28 13:09:10'),(8,5,30,'363328412251GR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-28 13:22:51','2016-07-28 13:22:51'),(9,5,31,'363328412315WR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-28 13:23:15','2016-07-28 13:23:15'),(10,5,32,'363328412518HR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-28 13:25:18','2016-07-28 13:25:18'),(11,5,33,'363328412704OR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-28 13:27:04','2016-07-28 13:27:04'),(12,5,34,'363328412728UR2',2,'bonusPoint','','C','','0','0',0,0,'2016-07-28 13:27:28','2016-07-28 13:27:28'),(13,3,51,'363402315221LR1',1,'bonusPoint','','C','','0.1','0',0,0,'2016-08-02 03:52:21','2016-08-02 03:52:21'),(14,3,53,'363409340207FR1',1,'bonusPoint','','C','','0.55','0',0,0,'2016-08-09 06:02:07','2016-08-09 06:02:07'),(15,3,54,'363412362605OR1',1,'bonusPoint','','C','','0.6','0',0,0,'2016-08-12 08:26:05','2016-08-12 08:26:05'),(16,3,55,'363412362905AR1',1,'bonusPoint','','C','','0.6','0',0,0,'2016-08-12 08:29:05','2016-08-12 08:29:05'),(17,3,74,'363429341457RR15',15,'bonusPoint','','C','','0.9','0',0,0,'2016-08-29 06:14:57','2016-08-29 06:14:57'),(18,3,75,'363429345930IR15',15,'bonusPoint','','C','','0.9','0',0,0,'2016-08-29 06:59:30','2016-08-29 06:59:30'),(19,11,77,'363429404729GR17',17,'couponCode','CC123456','C','50','50','0',0,0,'2016-08-29 12:47:29','2016-08-29 12:47:29'),(20,3,79,'363501341315ZR15',15,'bonusPoint','','C','','0.9','0',0,0,'2016-09-01 06:13:15','2016-09-01 06:13:15'),(21,3,80,'363502310112ER15',15,'bonusPoint','','C','','0.9','0',0,0,'2016-09-02 03:01:12','2016-09-02 03:01:12'),(22,3,81,'363503333720CR15',15,'bonusPoint','','C','','0.9','0',0,0,'2016-09-03 05:37:20','2016-09-03 05:37:20'),(23,5,84,'363506440656WR5',5,'cashbackPoint','','C','','0','0',0,0,'2016-09-06 16:06:56','2016-09-06 16:06:56'),(24,3,103,'363520332906LR15',15,'bonusPoint','','C','','1.25','0',0,0,'2016-09-20 05:29:06','2016-09-20 05:29:06'),(25,3,108,'363604323156AR15',15,'bonusPoint','','C','','1.3','0',0,0,'2016-10-04 04:31:56','2016-10-04 04:31:56');
/*!40000 ALTER TABLE `lie_order_discounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_order_item_extras`
--

DROP TABLE IF EXISTS `lie_order_item_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_order_item_extras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_item_id` int(11) NOT NULL,
  `extra_choice_id` int(11) NOT NULL,
  `extra_choice_element_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lie_order_item_extras_lie_order_items_id_fk` (`order_item_id`),
  KEY `lie_order_item_extras_lie_extra_choice_id_fk` (`extra_choice_id`),
  KEY `lie_order_item_extras_lie_extra_choice_element_id_fk` (`extra_choice_element_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_order_item_extras`
--

LOCK TABLES `lie_order_item_extras` WRITE;
/*!40000 ALTER TABLE `lie_order_item_extras` DISABLE KEYS */;
INSERT INTO `lie_order_item_extras` VALUES (1,163,2,2,'2016-11-20 21:26:49','2016-11-20 21:26:49'),(2,163,3,4,'2016-11-20 21:26:49','2016-11-20 21:26:49'),(3,164,3,4,'2016-11-20 21:30:52','2016-11-20 21:30:52'),(4,167,2,3,'2016-12-28 18:34:19','2016-12-28 18:34:19'),(5,168,2,3,'2016-12-28 18:37:41','2016-12-28 18:37:41'),(6,169,2,3,'2016-12-28 18:40:36','2016-12-28 18:40:36'),(7,170,2,3,'2016-12-28 18:46:12','2016-12-28 18:46:12'),(8,171,2,3,'2016-12-28 19:15:09','2016-12-28 19:15:09'),(9,172,2,3,'2016-12-28 21:18:18','2016-12-28 21:18:18'),(10,172,3,4,'2016-12-28 21:18:18','2016-12-28 21:18:18');
/*!40000 ALTER TABLE `lie_order_item_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_order_items`
--

DROP TABLE IF EXISTS `lie_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(100) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_qty` int(11) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  `item_type` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_order_items`
--

LOCK TABLES `lie_order_items` WRITE;
/*!40000 ALTER TABLE `lie_order_items` DISABLE KEYS */;
INSERT INTO `lie_order_items` VALUES (1,'363320334211LR2',2,1,1,'Chilli Potato',3,150.00,0,0,'2016-07-20 05:42:11','2016-07-20 05:42:11','1','menu'),(2,'363320334211LR2',2,1,1,'Test Menu Extra',1,100.00,0,0,'2016-07-20 05:42:11','2016-07-20 05:42:11','1','extras'),(3,'363320334211LR2',2,1,1,'Test Sbumenu',1,100.00,0,0,'2016-07-20 05:42:11','2016-07-20 05:42:11','1','submenu'),(4,'363320351800QR2',2,3,1,'Chilli Potato',1,50.00,0,0,'2016-07-20 07:18:00','2016-07-20 07:18:00','1','menu'),(5,'363320470813OR2',2,4,1,'Chilli Potato',1,50.00,0,0,'2016-07-20 19:08:13','2016-07-20 19:08:13','1','menu'),(6,'363320470813OR2',2,4,2,'Test Sbumenu 2',1,145.00,0,0,'2016-07-20 19:08:13','2016-07-20 19:08:13','1','submenu'),(7,'363321323825YR2',2,3,1,'Chilli Potato',3,150.00,0,0,'2016-07-21 04:38:25','2016-07-21 04:38:25','1','menu'),(8,'363321394253KR2',2,4,1,'Chilli Potato',1,50.00,0,0,'2016-07-21 11:42:53','2016-07-21 11:42:53','1','menu'),(9,'363321394253KR2',2,4,1,'Test Menu Extra',1,100.00,0,0,'2016-07-21 11:42:53','2016-07-21 11:42:53','1','extras'),(10,'363321430642MR2',2,5,1,'Chilli Potato',1,50.00,0,0,'2016-07-21 15:06:42','2016-07-21 15:06:42','1','menu'),(11,'363321432009HR2',2,4,1,'Test Sbumenu',3,300.00,0,0,'2016-07-21 15:20:09','2016-07-21 15:20:09','1','submenu'),(12,'363321432009HR2',2,4,1,'Chilli Potato',1,50.00,0,0,'2016-07-21 15:20:09','2016-07-21 15:20:09','1','menu'),(13,'363321432744YR2',2,4,1,'Test Sbumenu',3,300.00,0,0,'2016-07-21 15:27:44','2016-07-21 15:27:44','1','submenu'),(14,'363321432744YR2',2,4,1,'Chilli Potato',1,50.00,0,0,'2016-07-21 15:27:44','2016-07-21 15:27:44','1','menu'),(15,'363321435640NR2',2,5,1,'Test Sbumenu',1,100.00,0,0,'2016-07-21 15:56:40','2016-07-21 15:56:40','1','submenu'),(16,'363321452938OR2',2,3,1,'Chilli Potato',2,100.00,0,0,'2016-07-21 17:29:38','2016-07-21 17:29:38','1','menu'),(17,'363322382721TR2',2,4,1,'Chilli Potato',1,50.00,0,0,'2016-07-22 10:27:21','2016-07-22 10:27:21','1','menu'),(18,'363322433618FR2',2,3,1,'Chilli Potato',1,50.00,0,0,'2016-07-22 15:36:18','2016-07-22 15:36:18','1','menu'),(19,'363323321816ZR2',2,3,1,'Chilli Potato',1,50.00,0,0,'2016-07-23 04:18:16','2016-07-23 04:18:16','1','menu'),(20,'363324323153TR2',2,3,1,'Chilli Potato',1,50.00,0,0,'2016-07-24 04:31:53','2016-07-24 04:31:53','1','menu'),(21,'363325404323IR2',2,5,1,'Test Sbumenu',2,200.00,0,0,'2016-07-25 12:43:23','2016-07-25 12:43:23','1','submenu'),(22,'363325404323IR2',2,5,1,'Chilli Potato',1,50.00,0,0,'2016-07-25 12:43:23','2016-07-25 12:43:23','1','menu'),(23,'363326323051YR2',2,3,1,'Chilli Potato',1,50.00,0,0,'2016-07-26 04:30:51','2016-07-26 04:30:51','1','menu'),(24,'363326323051YR2',2,3,1,'Test Sbumenu',1,100.00,0,0,'2016-07-26 04:30:51','2016-07-26 04:30:51','1','submenu'),(25,'363326323051YR2',2,3,1,'Test Menu Extra',1,100.00,0,0,'2016-07-26 04:30:51','2016-07-26 04:30:51','1','extras'),(26,'363326332729OR2',2,3,1,'Chilli Potato',1,50.00,0,0,'2016-07-26 05:27:29','2016-07-26 05:27:29','1','menu'),(27,'363327325848WR2',2,3,1,'Chilli Potato',1,50.00,0,0,'2016-07-27 04:58:48','2016-07-27 04:58:48','1','menu'),(28,'363327330014ER2',2,3,1,'Chilli Potato',1,50.00,0,0,'2016-07-27 05:00:14','2016-07-27 05:00:14','1','menu'),(29,'363328330213JR2',2,3,1,'Test Menu Extra',1,100.00,0,0,'2016-07-28 05:02:13','2016-07-28 05:02:13','1','extras'),(30,'363328405223OR2',2,5,1,'Test Sbumenu',5,500.00,0,0,'2016-07-28 12:52:23','2016-07-28 12:52:23','1','submenu'),(31,'363328405340VR2',2,5,1,'Test Sbumenu',5,500.00,0,0,'2016-07-28 12:53:40','2016-07-28 12:53:40','1','submenu'),(32,'363328405440ZR2',2,5,1,'Test Sbumenu',5,500.00,0,0,'2016-07-28 12:54:40','2016-07-28 12:54:40','1','submenu'),(33,'363328405609CR2',2,5,1,'Test Sbumenu',5,500.00,0,0,'2016-07-28 12:56:09','2016-07-28 12:56:09','1','submenu'),(34,'363328405845TR2',2,5,1,'Test Sbumenu',5,500.00,0,0,'2016-07-28 12:58:45','2016-07-28 12:58:45','1','submenu'),(35,'363328405923PR2',2,5,1,'Test Sbumenu',5,500.00,0,0,'2016-07-28 12:59:23','2016-07-28 12:59:23','1','submenu'),(36,'363328410724WR2',2,5,1,'Test Sbumenu',5,500.00,0,0,'2016-07-28 13:07:24','2016-07-28 13:07:24','1','submenu'),(37,'363328410822GR2',2,5,1,'Test Sbumenu',1,100.00,0,0,'2016-07-28 13:08:22','2016-07-28 13:08:22','1','submenu'),(38,'363328410910RR2',2,5,1,'Test Sbumenu',1,100.00,0,0,'2016-07-28 13:09:10','2016-07-28 13:09:10','1','submenu'),(39,'363328412251GR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 13:22:51','2016-07-28 13:22:51','1','menu'),(40,'363328412315WR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 13:23:15','2016-07-28 13:23:15','1','menu'),(41,'363328412518HR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 13:25:18','2016-07-28 13:25:18','1','menu'),(42,'363328412704OR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 13:27:04','2016-07-28 13:27:04','1','menu'),(43,'363328412728UR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 13:27:28','2016-07-28 13:27:28','1','menu'),(44,'363328413125GR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 13:31:25','2016-07-28 13:31:25','1','menu'),(45,'363328413244TR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 13:32:44','2016-07-28 13:32:44','1','menu'),(46,'363328430604QR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 15:06:04','2016-07-28 15:06:04','1','menu'),(47,'363328430640JR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 15:06:40','2016-07-28 15:06:40','1','menu'),(48,'363328430800YR2',2,5,1,'Chilli Potato',2,100.00,0,0,'2016-07-28 15:08:00','2016-07-28 15:08:00','1','menu'),(49,'363329332536ZR1',1,3,1,'Test Sbumenu',1,100.00,0,0,'2016-07-29 05:25:36','2016-07-29 05:25:36','1','submenu'),(50,'363329391405AR1',1,4,1,'Test Sbumenu',1,100.00,0,0,'2016-07-29 11:14:05','2016-07-29 11:14:05','1','submenu'),(51,'363329392718TR1',1,4,1,'Test Sbumenu',1,100.00,0,0,'2016-07-29 11:27:18','2016-07-29 11:27:18','1','submenu'),(52,'363329393802CR1',1,4,1,'Test Sbumenu',1,100.00,0,0,'2016-07-29 11:38:02','2016-07-29 11:38:02','1','submenu'),(53,'363329393927DR1',1,4,1,'Test Sbumenu',1,100.00,0,0,'2016-07-29 11:39:27','2016-07-29 11:39:27','1','submenu'),(54,'363329394045NR1',1,4,1,'Test Sbumenu',1,100.00,0,0,'2016-07-29 11:40:45','2016-07-29 11:40:45','1','submenu'),(55,'363401442359PR1',1,4,1,'Chilli Potato',1,50.00,0,0,'2016-08-01 16:23:59','2016-08-01 16:23:59','1','menu'),(56,'363401442620AR1',1,4,1,'Chilli Potato',1,50.00,0,0,'2016-08-01 16:26:20','2016-08-01 16:26:20','1','menu'),(57,'363401494705CR1',1,3,1,'Chilli Potato',1,50.00,0,0,'2016-08-01 21:47:05','2016-08-01 21:47:05','1','menu'),(59,'363401503850ZR1',1,3,1,'Chilli Potato',3,150.00,0,0,'2016-08-01 22:38:50','2016-08-01 22:38:50','1','menu'),(60,'363401503850ZR1',1,3,2,'Test Sbumenu 2',1,145.00,0,0,'2016-08-01 22:38:50','2016-08-01 22:38:50','1','submenu'),(61,'363401503850ZR1',1,3,1,'Test Sbumenu',1,100.00,0,0,'2016-08-01 22:38:50','2016-08-01 22:38:50','1','submenu'),(62,'363402315221LR1',1,3,2,'Test Sbumenu 2',1,145.00,0,0,'2016-08-02 03:52:21','2016-08-02 03:52:21','1','submenu'),(63,'363405334239SR1',1,3,1,'Chilli Potato',2,100.00,0,0,'2016-08-05 05:42:39','2016-08-05 05:42:39','1','menu'),(64,'363409340207FR1',1,3,1,'Chilli Potato',1,50.00,0,0,'2016-08-09 06:02:07','2016-08-09 06:02:07','1','menu'),(65,'363412362605OR1',1,3,1,'Chilli Potato',2,100.00,0,0,'2016-08-12 08:26:05','2016-08-12 08:26:05','1','menu'),(66,'363412362905AR1',1,3,1,'Chilli Potato',2,100.00,0,0,'2016-08-12 08:29:05','2016-08-12 08:29:05','1','menu'),(68,'363422445634XR17',17,11,21,'Dosa',3,600.00,0,0,'2016-08-22 16:56:34','2016-08-22 16:56:34','1','menu'),(69,'363422445647VR17',17,11,21,'Dosa',3,600.00,0,0,'2016-08-22 16:56:47','2016-08-22 16:56:47','1','menu'),(70,'363422451004YR17',17,11,21,'Dosa',1,200.00,0,0,'2016-08-22 17:10:04','2016-08-22 17:10:04','1','menu'),(71,'363423332354TR17',17,3,21,'Dosa',1,200.00,0,0,'2016-08-23 05:23:54','2016-08-23 05:23:54','1','menu'),(72,'363423394332HR17',17,4,21,'Dosa',1,200.00,0,0,'2016-08-23 11:43:32','2016-08-23 11:43:32','1','menu'),(73,'363423394738GR17',17,4,21,'Dosa',2,400.00,0,0,'2016-08-23 11:47:38','2016-08-23 11:47:38','1','menu'),(74,'363423401236DR17',17,4,21,'Dosa',2,400.00,0,0,'2016-08-23 12:12:36','2016-08-23 12:12:36','1','menu'),(75,'363423412759TR17',17,11,21,'Dosa',5,1000.00,0,0,'2016-08-23 13:27:59','2016-08-23 13:27:59','1','menu'),(76,'363423443310ZR17',17,4,21,'Dosa',1,200.00,0,0,'2016-08-23 16:33:10','2016-08-23 16:33:10','1','menu'),(77,'363423444601GR17',17,19,21,'Dosa',1,200.00,0,0,'2016-08-23 16:46:01','2016-08-23 16:46:01','1','menu'),(78,'363424330338TR15',15,3,12,'Cardinale',2,12.00,0,0,'2016-08-24 05:03:38','2016-08-24 05:03:38','1','submenu'),(79,'363424330338TR15',15,3,11,'All Tonno',3,23.97,0,0,'2016-08-24 05:03:38','2016-08-24 05:03:38','1','submenu'),(80,'363424330338TR15',15,3,14,'Pizza Provenciale',1,6.90,0,0,'2016-08-24 05:03:38','2016-08-24 05:03:38','1','submenu'),(81,'363424330338TR15',15,3,15,'Contandino',4,27.60,0,0,'2016-08-24 05:03:38','2016-08-24 05:03:38','1','submenu'),(82,'363424330338TR15',15,3,18,'Pizza Cipolla',1,5.90,0,0,'2016-08-24 05:03:38','2016-08-24 05:03:38','1','submenu'),(83,'363424330338TR15',15,3,17,'Funghi',3,17.70,0,0,'2016-08-24 05:03:38','2016-08-24 05:03:38','1','submenu'),(84,'363424330338TR15',15,3,16,'Margarita',8,41.60,0,0,'2016-08-24 05:03:38','2016-08-24 05:03:38','1','submenu'),(85,'363425413334YR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-08-25 13:33:34','2016-08-25 13:33:34','1','submenu'),(86,'363425413334YR15',15,3,26,'Filetto al Griglia',1,14.50,0,0,'2016-08-25 13:33:34','2016-08-25 13:33:34','1','submenu'),(87,'363425413334YR15',15,3,13,'Pomodoro',1,5.90,0,0,'2016-08-25 13:33:34','2016-08-25 13:33:34','1','submenu'),(88,'363425431020YR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-08-25 15:10:20','2016-08-25 15:10:20','1','submenu'),(89,'363426324658FR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-08-26 04:46:58','2016-08-26 04:46:58','1','submenu'),(90,'363426332145SR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(91,'363426332145SR15',15,3,26,'Filetto al Griglia',1,14.50,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(92,'363426332145SR15',15,3,22,'Pollo ai Funghi',2,17.40,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(93,'363426332145SR15',15,3,21,'Pollo alla Milanese ',2,19.98,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(94,'363426332145SR15',15,3,23,'Pollo ai Gorgonzola',2,17.40,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(95,'363426332145SR15',15,3,24,'Pollo al Pepe',2,23.00,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(96,'363426332145SR15',15,3,13,'Pomodoro',2,11.80,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(97,'363426332145SR15',15,3,19,'Bolognesse',1,7.90,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(98,'363426332145SR15',15,3,11,'All Tonno',1,7.99,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(99,'363426332145SR15',15,3,12,'Cardinale',1,6.00,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(100,'363426332145SR15',15,3,14,'Pizza Provenciale',1,6.90,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(101,'363426332145SR15',15,3,15,'Contandino',1,6.90,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(102,'363426332145SR15',15,3,16,'Margarita',1,5.20,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(103,'363426332145SR15',15,3,17,'Funghi',1,5.90,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(104,'363426332145SR15',15,3,18,'Pizza Cipolla',1,5.90,0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45','1','submenu'),(105,'363426405855DR17',17,11,25,'Rajma Handi',2,300.00,0,0,'2016-08-26 12:58:55','2016-08-26 12:58:55','1','menu'),(106,'363426405855DR17',17,11,21,'Dosa',2,400.00,0,0,'2016-08-26 12:58:55','2016-08-26 12:58:55','1','menu'),(107,'363428322305JR15',15,3,25,'Bistecca alla Griglia (200g)',2,27.00,0,0,'2016-08-28 04:23:05','2016-08-28 04:23:05','1','submenu'),(108,'363428322305JR15',15,3,21,'Pollo alla Milanese ',1,9.99,0,0,'2016-08-28 04:23:05','2016-08-28 04:23:05','1','submenu'),(109,'363429341457RR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-08-29 06:14:57','2016-08-29 06:14:57','1','submenu'),(110,'363429345930IR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-08-29 06:59:30','2016-08-29 06:59:30','1','submenu'),(111,'363429350103YR15',15,3,13,'Pomodoro',1,5.90,0,0,'2016-08-29 07:01:03','2016-08-29 07:01:03','1','submenu'),(112,'363429404729GR17',17,11,25,'Rajma Handi',1,150.00,0,0,'2016-08-29 12:47:29','2016-08-29 12:47:29','1','menu'),(113,'363431413340KR17',17,11,25,'Rajma Handi',3,450.00,0,0,'2016-08-31 13:33:40','2016-08-31 13:33:40','1','menu'),(114,'363501341315ZR15',15,3,26,'Filetto al Griglia',1,14.50,0,0,'2016-09-01 06:13:15','2016-09-01 06:13:15','1','submenu'),(115,'363502310112ER15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-09-02 03:01:12','2016-09-02 03:01:12','1','submenu'),(116,'363503333720CR15',15,3,25,'Bistecca alla Griglia (200g)',2,27.00,0,0,'2016-09-03 05:37:20','2016-09-03 05:37:20','1','submenu'),(117,'363506393236OR5',5,5,28,'Parantha',2,400.00,0,0,'2016-09-06 11:32:36','2016-09-06 11:32:36','1','submenu'),(118,'363506403409SR5',5,5,28,'Parantha',1,200.00,0,0,'2016-09-06 12:34:09','2016-09-06 12:34:09','1','submenu'),(119,'363506440656WR5',5,5,28,'Parantha',3,600.00,0,0,'2016-09-06 16:06:56','2016-09-06 16:06:56','1','submenu'),(120,'363506462416VR1',1,5,1,'Chilli Potato',2,100.00,0,0,'2016-09-06 18:24:16','2016-09-06 18:24:16','1','menu'),(121,'363506462553NR5',5,5,28,'Parantha',2,400.00,0,0,'2016-09-06 18:25:53','2016-09-06 18:25:53','1','submenu'),(122,'363506465018ZR1',1,21,1,'Test Sbumenu',1,100.00,0,0,'2016-09-06 18:50:18','2016-09-06 18:50:18','1','submenu'),(123,'363506465018ZR1',1,21,2,'Test Sbumenu 2',2,290.00,0,0,'2016-09-06 18:50:18','2016-09-06 18:50:18','1','submenu'),(124,'363506465037JR1',1,21,1,'Test Sbumenu',1,100.00,0,0,'2016-09-06 18:50:37','2016-09-06 18:50:37','1','submenu'),(125,'363506465037JR1',1,21,2,'Test Sbumenu 2',2,290.00,0,0,'2016-09-06 18:50:37','2016-09-06 18:50:37','1','submenu'),(126,'363507352047SR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-09-07 07:20:47','2016-09-07 07:20:47','1','submenu'),(127,'363507395820ZR5',5,5,6,'Parantha',2,500.00,0,0,'2016-09-07 11:58:20','2016-09-07 11:58:20','1','submenu'),(128,'363508412812KR5',5,1,6,'Parantha',1,250.00,0,0,'2016-09-08 13:28:12','2016-09-08 13:28:12','1','submenu'),(129,'363509442214OR5',5,5,28,'Parantha',2,400.00,0,0,'2016-09-09 16:22:14','2016-09-09 16:22:14','1','submenu'),(130,'363510301713AR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-09-10 02:17:13','2016-09-10 02:17:13','1','submenu'),(131,'363510301713AR15',15,3,26,'Filetto al Griglia',2,29.00,0,0,'2016-09-10 02:17:13','2016-09-10 02:17:13','1','submenu'),(132,'363510434445YR17',17,11,25,'Rajma Handi',2,300.00,0,0,'2016-09-10 15:44:45','2016-09-10 15:44:45','1','menu'),(133,'363510435558WR17',17,11,25,'Rajma Handi',2,300.00,0,0,'2016-09-10 15:55:58','2016-09-10 15:55:58','1','menu'),(134,'363511334553FR15',15,3,26,'Filetto al Griglia',4,58.00,0,0,'2016-09-11 05:45:53','2016-09-11 05:45:53','1','submenu'),(135,'363512413231UR17',17,4,25,'Rajma Handi',1,150.00,0,0,'2016-09-12 13:32:31','2016-09-12 13:32:31','1','menu'),(136,'363512413304WR15',15,11,24,'Pollo al Pepe',2,23.00,0,0,'2016-09-12 13:33:04','2016-09-12 13:33:04','1','submenu'),(137,'363512413304WR15',15,11,23,'Pollo ai Gorgonzola',1,8.70,0,0,'2016-09-12 13:33:04','2016-09-12 13:33:04','1','submenu'),(138,'363512413304WR15',15,11,22,'Pollo ai Funghi',1,8.70,0,0,'2016-09-12 13:33:04','2016-09-12 13:33:04','1','submenu'),(139,'363512413304WR15',15,11,21,'Pollo alla Milanese ',1,9.99,0,0,'2016-09-12 13:33:04','2016-09-12 13:33:04','1','submenu'),(140,'363512441804FR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-09-12 16:18:04','2016-09-12 16:18:04','1','submenu'),(141,'363513310244LR15',15,3,11,'All Tonno',2,15.98,0,0,'2016-09-13 03:02:44','2016-09-13 03:02:44','1','submenu'),(142,'363515325428FR15',15,3,12,'Cardinale',1,6.00,0,0,'2016-09-15 04:54:28','2016-09-15 04:54:28','1','submenu'),(143,'363515325428FR15',15,3,11,'All Tonno',1,7.99,0,0,'2016-09-15 04:54:28','2016-09-15 04:54:28','1','submenu'),(144,'363518292750GR15',15,3,21,'Pollo alla Milanese ',2,19.98,0,0,'2016-09-18 01:27:50','2016-09-18 01:27:50','1','submenu'),(145,'363520332906LR15',15,3,21,'Pollo alla Milanese ',3,29.97,0,0,'2016-09-20 05:29:06','2016-09-20 05:29:06','1','submenu'),(146,'363520332906LR15',15,3,26,'Filetto al Griglia',1,14.50,0,0,'2016-09-20 05:29:06','2016-09-20 05:29:06','1','submenu'),(147,'363520333555XR15',15,3,11,'All Tonno',2,15.98,0,0,'2016-09-20 05:35:55','2016-09-20 05:35:55','1','submenu'),(148,'363520333555XR15',15,3,20,'alla Carbonara',2,15.00,0,0,'2016-09-20 05:35:55','2016-09-20 05:35:55','1','submenu'),(149,'363520333555XR15',15,3,19,'Bolognesse',1,7.90,0,0,'2016-09-20 05:35:55','2016-09-20 05:35:55','1','submenu'),(150,'363520333555XR15',15,3,13,'Pomodoro',1,5.90,0,0,'2016-09-20 05:35:55','2016-09-20 05:35:55','1','submenu'),(151,'363520333555XR15',15,3,24,'Pollo al Pepe',1,11.50,0,0,'2016-09-20 05:35:55','2016-09-20 05:35:55','1','submenu'),(152,'363520333555XR15',15,3,12,'Cardinale',1,6.00,0,0,'2016-09-20 05:35:55','2016-09-20 05:35:55','1','submenu'),(153,'363520333555XR15',15,3,14,'Pizza Provenciale',1,6.90,0,0,'2016-09-20 05:35:55','2016-09-20 05:35:55','1','submenu'),(154,'363520334810IR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-09-20 05:48:10','2016-09-20 05:48:10','1','submenu'),(155,'363524335113SR15',15,3,13,'Pomodoro',1,5.90,0,0,'2016-09-24 05:51:13','2016-09-24 05:51:13','1','submenu'),(156,'363524351237QR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-09-24 07:12:37','2016-09-24 07:12:37','1','submenu'),(157,'363604323156AR15',15,3,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-10-04 04:31:56','2016-10-04 04:31:56','1','submenu'),(158,'363604323156AR15',15,3,26,'Filetto al Griglia',1,14.50,0,0,'2016-10-04 04:31:56','2016-10-04 04:31:56','1','submenu'),(163,'363720492649YR15',15,23,30,'Lasagne Al Forno',1,30.50,0,0,'2016-11-20 21:26:49','2016-11-20 21:26:49','1','submenu'),(164,'363720493052GR15',15,23,30,'Lasagne Al Forno',1,20.00,0,0,'2016-11-20 21:30:52','2016-11-20 21:30:52','1','submenu'),(166,'363726294709AR15',15,23,25,'Bistecca alla Griglia (200g)',1,13.50,0,0,'2016-11-26 01:47:09','2016-11-26 01:47:09','1','submenu'),(170,'363828464612FR15',15,23,11,'All Tonno',1,8.49,0,0,'2016-12-28 18:46:12','2016-12-28 18:46:12','1','submenu'),(171,'363828471509SR15',15,23,11,'All Tonno',1,8.49,0,0,'2016-12-28 19:15:09','2016-12-28 19:15:09','1','submenu'),(172,'363828491818KR15',15,23,30,'Lasagne Al Forno',1,20.50,0,0,'2016-12-28 21:18:18','2016-12-28 21:18:18','1','submenu'),(173,'363830331021GR15',15,23,14,'Pizza Provenciale',1,6.90,0,0,'2016-12-30 05:10:21','2016-12-30 05:10:21','1','submenu'),(174,'363831382659UR15',15,23,16,'Margarita',2,20.80,0,0,'2016-12-31 10:26:59','2016-12-31 10:26:59','1','submenu');
/*!40000 ALTER TABLE `lie_order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_order_payments`
--

DROP TABLE IF EXISTS `lie_order_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_order_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `total_amount` bigint(20) NOT NULL,
  `grand_total` bigint(20) NOT NULL,
  `payment_method` enum('1','2') NOT NULL COMMENT '1 for COD,2for Online',
  `order_type` enum('1','2') NOT NULL COMMENT '1 for delivery,2 for takeaway',
  `discount_amount` bigint(20) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_order_payments`
--

LOCK TABLES `lie_order_payments` WRITE;
/*!40000 ALTER TABLE `lie_order_payments` DISABLE KEYS */;
INSERT INTO `lie_order_payments` VALUES (1,3,'363320334211LR2',2,350,350,'1','1',0,'0',0,0,'2016-07-20 05:42:11','2016-07-20 05:42:11'),(2,3,'363320351800QR2',2,50,50,'2','1',0,'0',0,0,'2016-07-20 07:18:00','2016-07-20 07:18:00'),(3,4,'363320470813OR2',2,195,195,'1','1',0,'0',0,0,'2016-07-20 19:08:13','2016-07-20 19:08:13'),(4,3,'363321323825YR2',2,150,150,'1','1',0,'0',0,0,'2016-07-21 04:38:25','2016-07-21 04:38:25'),(5,4,'363321394253KR2',2,150,150,'1','1',0,'0',0,0,'2016-07-21 11:42:53','2016-07-21 11:42:53'),(6,5,'363321430642MR2',2,50,50,'1','1',0,'0',0,0,'2016-07-21 15:06:42','2016-07-21 15:06:42'),(7,4,'363321432009HR2',2,350,350,'2','1',0,'0',0,0,'2016-07-21 15:20:09','2016-07-21 15:20:09'),(8,4,'363321432744YR2',2,350,350,'1','1',0,'0',0,0,'2016-07-21 15:27:44','2016-07-21 15:27:44'),(9,5,'363321435640NR2',2,100,100,'1','1',0,'0',0,0,'2016-07-21 15:56:40','2016-07-21 15:56:40'),(10,3,'363321452938OR2',2,100,100,'1','1',0,'0',0,0,'2016-07-21 17:29:38','2016-07-21 17:29:38'),(11,4,'363322382721TR2',2,50,50,'2','1',0,'0',0,0,'2016-07-22 10:27:21','2016-07-22 10:27:21'),(12,3,'363322433618FR2',2,45,45,'1','1',5,'0',0,0,'2016-07-22 15:36:18','2016-07-22 15:36:18'),(13,3,'363323321816ZR2',2,50,50,'1','1',0,'0',0,0,'2016-07-23 04:18:16','2016-07-23 04:18:16'),(14,3,'363324323153TR2',2,50,50,'1','1',0,'0',0,0,'2016-07-24 04:31:53','2016-07-24 04:31:53'),(15,5,'363325404323IR2',2,250,250,'1','1',0,'0',0,0,'2016-07-25 12:43:23','2016-07-25 12:43:23'),(16,3,'363326323051YR2',2,250,250,'1','1',0,'0',0,0,'2016-07-26 04:30:51','2016-07-26 04:30:51'),(17,3,'363326332729OR2',2,50,50,'1','1',0,'0',0,0,'2016-07-26 05:27:29','2016-07-26 05:27:29'),(18,3,'363327325848WR2',2,45,45,'2','1',5,'0',0,0,'2016-07-27 04:58:48','2016-07-27 04:58:48'),(19,3,'363327330014ER2',2,45,45,'1','1',5,'0',0,0,'2016-07-27 05:00:14','2016-07-27 05:00:14'),(20,3,'363328330213JR2',2,100,100,'1','1',0,'0',0,0,'2016-07-28 05:02:13','2016-07-28 05:02:13'),(21,5,'363328405223OR2',2,500,500,'2','1',0,'0',0,0,'2016-07-28 12:52:23','2016-07-28 12:52:23'),(22,5,'363328405340VR2',2,500,500,'2','1',0,'0',0,0,'2016-07-28 12:53:40','2016-07-28 12:53:40'),(23,5,'363328405440ZR2',2,500,500,'2','1',0,'0',0,0,'2016-07-28 12:54:40','2016-07-28 12:54:40'),(24,5,'363328405609CR2',2,500,500,'2','1',0,'0',0,0,'2016-07-28 12:56:09','2016-07-28 12:56:09'),(25,5,'363328405845TR2',2,500,500,'2','1',0,'0',0,0,'2016-07-28 12:58:45','2016-07-28 12:58:45'),(26,5,'363328405923PR2',2,500,500,'2','1',0,'0',0,0,'2016-07-28 12:59:23','2016-07-28 12:59:23'),(27,5,'363328410724WR2',2,500,500,'1','1',0,'0',0,0,'2016-07-28 13:07:24','2016-07-28 13:07:24'),(28,5,'363328410822GR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 13:08:22','2016-07-28 13:08:22'),(29,5,'363328410910RR2',2,100,100,'1','1',0,'0',0,0,'2016-07-28 13:09:10','2016-07-28 13:09:10'),(30,5,'363328412251GR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 13:22:51','2016-07-28 13:22:51'),(31,5,'363328412315WR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 13:23:15','2016-07-28 13:23:15'),(32,5,'363328412518HR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 13:25:18','2016-07-28 13:25:18'),(33,5,'363328412704OR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 13:27:04','2016-07-28 13:27:04'),(34,5,'363328412728UR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 13:27:28','2016-07-28 13:27:28'),(35,5,'363328413125GR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 13:31:25','2016-07-28 13:31:25'),(36,5,'363328413244TR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 13:32:44','2016-07-28 13:32:44'),(37,5,'363328430604QR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 15:06:04','2016-07-28 15:06:04'),(38,5,'363328430640JR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 15:06:40','2016-07-28 15:06:40'),(39,5,'363328430800YR2',2,100,100,'2','1',0,'0',0,0,'2016-07-28 15:08:00','2016-07-28 15:08:00'),(40,3,'363329332536ZR1',1,100,100,'2','2',0,'1',0,0,'2016-07-29 05:25:36','2016-07-29 05:25:36'),(41,4,'363329391405AR1',1,100,100,'2','1',0,'0',0,0,'2016-07-29 11:14:05','2016-07-29 11:14:05'),(42,4,'363329392718TR1',1,100,100,'2','1',0,'0',0,0,'2016-07-29 11:27:18','2016-07-29 11:27:18'),(43,4,'363329393802CR1',1,100,100,'2','1',0,'0',0,0,'2016-07-29 11:38:02','2016-07-29 11:38:02'),(44,4,'363329393927DR1',1,100,100,'2','1',0,'0',0,0,'2016-07-29 11:39:27','2016-07-29 11:39:27'),(45,4,'363329394045NR1',1,100,100,'2','1',0,'0',0,0,'2016-07-29 11:40:45','2016-07-29 11:40:45'),(46,4,'363401442359PR1',1,50,50,'2','1',0,'0',0,0,'2016-08-01 16:23:59','2016-08-01 16:23:59'),(47,4,'363401442620AR1',1,50,50,'1','1',0,'0',0,0,'2016-08-01 16:26:20','2016-08-01 16:26:20'),(48,3,'363401494705CR1',1,50,50,'1','1',0,'0',0,0,'2016-08-01 21:47:05','2016-08-01 21:47:05'),(50,3,'363401503850ZR1',1,395,395,'2','1',0,'0',0,0,'2016-08-01 22:38:50','2016-08-01 22:38:50'),(51,3,'363402315221LR1',1,145,145,'1','1',0,'0',0,0,'2016-08-02 03:52:21','2016-08-02 03:52:21'),(52,3,'363405334239SR1',1,100,100,'1','1',0,'0',0,0,'2016-08-05 05:42:39','2016-08-05 05:42:39'),(53,3,'363409340207FR1',1,49,49,'1','1',1,'0',0,0,'2016-08-09 06:02:07','2016-08-09 06:02:07'),(54,3,'363412362605OR1',1,99,99,'1','1',1,'0',0,0,'2016-08-12 08:26:05','2016-08-12 08:26:05'),(55,3,'363412362905AR1',1,99,99,'1','1',1,'0',0,0,'2016-08-12 08:29:05','2016-08-12 08:29:05'),(57,11,'363422445634XR17',17,600,600,'2','1',0,'0',0,0,'2016-08-22 16:56:34','2016-08-22 16:56:34'),(58,11,'363422445647VR17',17,600,600,'1','1',0,'0',0,0,'2016-08-22 16:56:47','2016-08-22 16:56:47'),(59,11,'363422451004YR17',17,200,200,'1','1',0,'0',0,0,'2016-08-22 17:10:04','2016-08-22 17:10:04'),(60,3,'363423332354TR17',17,200,200,'1','1',0,'0',0,0,'2016-08-23 05:23:54','2016-08-23 05:23:54'),(61,4,'363423394332HR17',17,200,200,'1','1',0,'0',0,0,'2016-08-23 11:43:32','2016-08-23 11:43:32'),(62,4,'363423394738GR17',17,400,400,'1','1',0,'0',0,0,'2016-08-23 11:47:38','2016-08-23 11:47:38'),(63,4,'363423401236DR17',17,400,400,'1','1',0,'0',0,0,'2016-08-23 12:12:36','2016-08-23 12:12:36'),(64,11,'363423412759TR17',17,1000,1000,'1','1',0,'0',0,0,'2016-08-23 13:27:59','2016-08-23 13:27:59'),(65,4,'363423443310ZR17',17,200,200,'1','1',0,'0',0,0,'2016-08-23 16:33:10','2016-08-23 16:33:10'),(66,19,'363423444601GR17',17,200,200,'1','1',0,'0',0,0,'2016-08-23 16:46:01','2016-08-23 16:46:01'),(67,3,'363424330338TR15',15,136,136,'1','1',0,'0',0,0,'2016-08-24 05:03:38','2016-08-24 05:03:38'),(68,3,'363425413334YR15',15,34,34,'1','1',0,'0',0,0,'2016-08-25 13:33:34','2016-08-25 13:33:34'),(69,3,'363425431020YR15',15,14,14,'2','1',0,'0',0,0,'2016-08-25 15:10:20','2016-08-25 15:10:20'),(70,3,'363426324658FR15',15,14,14,'1','1',0,'0',0,0,'2016-08-26 04:46:58','2016-08-26 04:46:58'),(71,3,'363426332145SR15',15,170,170,'1','1',0,'0',0,0,'2016-08-26 05:21:45','2016-08-26 05:21:45'),(72,11,'363426405855DR17',17,700,700,'1','1',0,'0',0,0,'2016-08-26 12:58:55','2016-08-26 12:58:55'),(73,3,'363428322305JR15',15,37,37,'1','1',0,'0',0,0,'2016-08-28 04:23:05','2016-08-28 04:23:05'),(74,3,'363429341457RR15',15,13,13,'1','1',1,'0',0,0,'2016-08-29 06:14:57','2016-08-29 06:14:57'),(75,3,'363429345930IR15',15,13,13,'1','1',1,'0',0,0,'2016-08-29 06:59:30','2016-08-29 06:59:30'),(76,3,'363429350103YR15',15,6,6,'1','1',0,'0',0,0,'2016-08-29 07:01:03','2016-08-29 07:01:03'),(77,11,'363429404729GR17',17,100,100,'1','1',50,'0',0,0,'2016-08-29 12:47:29','2016-08-29 12:47:29'),(78,11,'363431413340KR17',17,450,450,'1','1',0,'0',0,0,'2016-08-31 13:33:40','2016-08-31 13:33:40'),(79,3,'363501341315ZR15',15,14,14,'1','1',1,'0',0,0,'2016-09-01 06:13:15','2016-09-01 06:13:15'),(80,3,'363502310112ER15',15,13,13,'1','1',1,'0',0,0,'2016-09-02 03:01:12','2016-09-02 03:01:12'),(81,3,'363503333720CR15',15,26,26,'1','1',1,'0',0,0,'2016-09-03 05:37:20','2016-09-03 05:37:20'),(82,5,'363506393236OR5',5,400,400,'1','1',0,'0',0,0,'2016-09-06 11:32:36','2016-09-06 11:32:36'),(83,5,'363506403409SR5',5,200,200,'1','1',0,'0',0,0,'2016-09-06 12:34:09','2016-09-06 12:34:09'),(84,5,'363506440656WR5',5,600,600,'1','1',0,'0',0,0,'2016-09-06 16:06:56','2016-09-06 16:06:56'),(85,5,'363506462416VR1',1,100,100,'1','1',0,'0',0,0,'2016-09-06 18:24:16','2016-09-06 18:24:16'),(86,5,'363506462553NR5',5,400,400,'1','1',0,'0',0,0,'2016-09-06 18:25:53','2016-09-06 18:25:53'),(87,21,'363506465018ZR1',1,390,390,'2','1',0,'0',0,0,'2016-09-06 18:50:18','2016-09-06 18:50:18'),(88,21,'363506465037JR1',1,390,390,'1','1',0,'0',0,0,'2016-09-06 18:50:37','2016-09-06 18:50:37'),(89,3,'363507352047SR15',15,14,14,'1','1',0,'0',0,0,'2016-09-07 07:20:47','2016-09-07 07:20:47'),(90,5,'363507395820ZR5',5,500,500,'1','1',0,'0',0,0,'2016-09-07 11:58:20','2016-09-07 11:58:20'),(91,1,'363508412812KR5',5,250,250,'1','1',0,'0',0,0,'2016-09-08 13:28:12','2016-09-08 13:28:12'),(92,5,'363509442214OR5',5,400,400,'1','1',0,'0',0,0,'2016-09-09 16:22:14','2016-09-09 16:22:14'),(93,3,'363510301713AR15',15,43,43,'1','1',0,'0',0,0,'2016-09-10 02:17:13','2016-09-10 02:17:13'),(94,11,'363510434445YR17',17,300,300,'1','1',0,'0',0,0,'2016-09-10 15:44:45','2016-09-10 15:44:45'),(95,11,'363510435558WR17',17,300,300,'2','1',0,'0',0,0,'2016-09-10 15:55:58','2016-09-10 15:55:58'),(96,3,'363511334553FR15',15,58,58,'1','1',0,'0',0,0,'2016-09-11 05:45:53','2016-09-11 05:45:53'),(97,4,'363512413231UR17',17,150,150,'2','1',0,'0',0,0,'2016-09-12 13:32:31','2016-09-12 13:32:31'),(98,11,'363512413304WR15',15,50,50,'1','1',0,'0',0,0,'2016-09-12 13:33:04','2016-09-12 13:33:04'),(99,3,'363512441804FR15',15,14,14,'1','1',0,'0',0,0,'2016-09-12 16:18:04','2016-09-12 16:18:04'),(100,3,'363513310244LR15',15,16,16,'1','1',0,'0',0,0,'2016-09-13 03:02:44','2016-09-13 03:02:44'),(101,3,'363515325428FR15',15,14,14,'1','1',0,'0',0,0,'2016-09-15 04:54:28','2016-09-15 04:54:28'),(102,3,'363518292750GR15',15,20,20,'1','1',0,'0',0,0,'2016-09-18 01:27:50','2016-09-18 01:27:50'),(103,3,'363520332906LR15',15,43,43,'1','1',1,'0',0,0,'2016-09-20 05:29:06','2016-09-20 05:29:06'),(104,3,'363520333555XR15',15,69,69,'1','1',0,'0',0,0,'2016-09-20 05:35:55','2016-09-20 05:35:55'),(105,3,'363520334810IR15',15,14,14,'2','1',0,'0',0,0,'2016-09-20 05:48:10','2016-09-20 05:48:10'),(106,3,'363524335113SR15',15,6,6,'1','1',0,'0',0,0,'2016-09-24 05:51:13','2016-09-24 05:51:13'),(107,3,'363524351237QR15',15,14,14,'1','1',0,'0',0,0,'2016-09-24 07:12:37','2016-09-24 07:12:37'),(108,3,'363604323156AR15',15,27,27,'1','1',1,'0',0,0,'2016-10-04 04:31:56','2016-10-04 04:31:56'),(109,23,'363720492649YR15',15,31,31,'1','2',0,'0',0,0,'2016-11-20 21:26:49','2016-11-20 21:26:49'),(110,23,'363720493052GR15',15,20,20,'1','1',0,'0',0,0,'2016-11-20 21:30:52','2016-11-20 21:30:52'),(111,23,'363726294709AR15',15,14,14,'1','1',0,'0',0,0,'2016-11-26 01:47:09','2016-11-26 01:47:09'),(112,23,'363828464612FR15',15,8,8,'1','1',0,'0',0,0,'2016-12-28 18:46:12','2016-12-28 18:46:12'),(113,23,'363828471509SR15',15,8,8,'1','1',0,'0',0,0,'2016-12-28 19:15:09','2016-12-28 19:15:09'),(114,23,'363828491818KR15',15,21,21,'1','1',0,'0',0,0,'2016-12-28 21:18:18','2016-12-28 21:18:18'),(115,23,'363830331021GR15',15,7,7,'1','1',0,'0',0,0,'2016-12-30 05:10:21','2016-12-30 05:10:21'),(116,23,'363831382659UR15',15,21,21,'1','1',0,'0',0,0,'2016-12-31 10:26:59','2016-12-31 10:26:59');
/*!40000 ALTER TABLE `lie_order_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_order_services`
--

DROP TABLE IF EXISTS `lie_order_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_order_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_order_services`
--

LOCK TABLES `lie_order_services` WRITE;
/*!40000 ALTER TABLE `lie_order_services` DISABLE KEYS */;
INSERT INTO `lie_order_services` VALUES (1,'Delivery','Testing By Rajlakshmi...',9,2,'2016-05-24 00:00:00','2016-08-22 15:09:18','1'),(2,'Take away','Testing By Rajlakshmi...',9,2,'2016-05-20 13:24:50','2016-07-19 17:10:58','1'),(3,'Dine','Dine in restaurant',2,0,'2016-07-26 18:49:40','2016-07-26 18:49:40','1');
/*!40000 ALTER TABLE `lie_order_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_page_lists`
--

DROP TABLE IF EXISTS `lie_page_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_page_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_page_lists`
--

LOCK TABLES `lie_page_lists` WRITE;
/*!40000 ALTER TABLE `lie_page_lists` DISABLE KEYS */;
INSERT INTO `lie_page_lists` VALUES (1,'Home page','sdfdasdad gasjd asdg ajsd asgdajd hasgdasd asgda da dasdad',2,2,'2016-07-19 18:12:40','2016-07-19 18:13:24','1'),(2,'Contact','sdasdgadj asdgad adas djgad asjdsad',2,2,'2016-07-19 18:13:07','2016-07-20 18:11:48','1'),(3,'asd ad sa dasd','as dasd ad asd aas asd sd asd asd as',2,2,'2016-07-19 18:13:37','2016-07-19 18:13:51','2'),(4,'Menu','adasdas asd asdasd as dasd asd',2,2,'2016-07-20 10:23:10','2016-07-20 10:23:10','1');
/*!40000 ALTER TABLE `lie_page_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_price_types`
--

DROP TABLE IF EXISTS `lie_price_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_price_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_price_types`
--

LOCK TABLES `lie_price_types` WRITE;
/*!40000 ALTER TABLE `lie_price_types` DISABLE KEYS */;
INSERT INTO `lie_price_types` VALUES (1,'Zustellung','This is a home delivery price',2,2,'2016-07-19 18:19:03','2016-08-31 06:16:35','1'),(2,'Abholung','This is a takeaway price',2,2,'2016-07-19 18:19:57','2016-08-31 06:16:50','1');
/*!40000 ALTER TABLE `lie_price_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_promo_rest_maps`
--

DROP TABLE IF EXISTS `lie_promo_rest_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_promo_rest_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_promo_rest_maps`
--

LOCK TABLES `lie_promo_rest_maps` WRITE;
/*!40000 ALTER TABLE `lie_promo_rest_maps` DISABLE KEYS */;
INSERT INTO `lie_promo_rest_maps` VALUES (1,1,1,2,0,'2016-07-19 16:40:06','2016-07-19 16:40:06','2'),(2,1,1,2,0,'2016-07-19 16:40:31','2016-07-19 16:40:31','2'),(3,1,1,2,0,'2016-07-19 16:41:07','2016-07-19 16:41:07','2'),(4,1,1,2,0,'2016-07-19 16:44:26','2016-07-19 16:44:26','2'),(5,1,1,2,0,'2016-07-19 16:44:40','2016-07-19 16:44:40','2'),(6,2,2,2,0,'2016-07-28 18:25:08','2016-07-28 18:25:08','2'),(7,3,5,2,0,'2016-07-29 15:57:01','2016-07-29 15:57:01','2'),(8,3,5,2,0,'2016-07-30 15:45:47','2016-07-30 15:45:47','2'),(9,2,2,2,0,'2016-07-30 15:51:57','2016-07-30 15:51:57','2'),(10,3,5,2,0,'2016-07-30 16:01:06','2016-07-30 16:01:06','2'),(11,3,1,2,0,'2016-08-01 13:56:00','2016-08-01 13:56:00','1'),(12,3,5,2,0,'2016-08-01 13:56:00','2016-08-01 13:56:00','1'),(13,2,2,2,0,'2016-08-01 13:56:08','2016-08-01 13:56:08','2'),(14,2,3,2,0,'2016-08-01 13:56:08','2016-08-01 13:56:08','2'),(15,1,1,2,0,'2016-08-01 13:56:25','2016-08-01 13:56:25','1'),(16,1,3,2,0,'2016-08-01 13:56:25','2016-08-01 13:56:25','1'),(17,2,2,2,0,'2016-08-01 13:58:18','2016-08-01 13:58:18','1'),(18,2,3,2,0,'2016-08-01 13:58:18','2016-08-01 13:58:18','1'),(19,2,4,2,0,'2016-08-01 13:58:18','2016-08-01 13:58:18','1'),(20,4,5,2,0,'2016-08-10 15:57:13','2016-08-10 15:57:13','1');
/*!40000 ALTER TABLE `lie_promo_rest_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_promo_user_maps`
--

DROP TABLE IF EXISTS `lie_promo_user_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_promo_user_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_promo_user_maps`
--

LOCK TABLES `lie_promo_user_maps` WRITE;
/*!40000 ALTER TABLE `lie_promo_user_maps` DISABLE KEYS */;
INSERT INTO `lie_promo_user_maps` VALUES (1,1,2,2,0,'2016-07-19 16:40:06','2016-07-19 16:40:06','2'),(2,1,2,2,0,'2016-07-19 16:40:31','2016-07-19 16:40:31','2'),(3,1,2,2,0,'2016-07-19 16:41:07','2016-07-19 16:41:07','2'),(4,1,2,2,0,'2016-07-19 16:44:26','2016-07-19 16:44:26','2'),(5,1,2,2,0,'2016-07-19 16:44:40','2016-07-19 16:44:40','2'),(6,1,2,2,0,'2016-08-01 13:56:25','2016-08-01 13:56:25','1'),(7,1,8,2,0,'2016-08-01 13:56:25','2016-08-01 13:56:25','1');
/*!40000 ALTER TABLE `lie_promo_user_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_promocodes`
--

DROP TABLE IF EXISTS `lie_promocodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_promocodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(50) NOT NULL,
  `is_rest_specific` int(11) NOT NULL,
  `is_user_specific` int(11) NOT NULL,
  `validity_type` enum('T','C') NOT NULL COMMENT 'T for table,C for calendar',
  `validity_table` int(11) NOT NULL DEFAULT '0',
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `payment_mode` enum('P','C') NOT NULL COMMENT 'P for percentage and C for cash',
  `payment_amount` varchar(10) NOT NULL,
  `min_amount` varchar(10) NOT NULL,
  `order_service_id` enum('1','2','3') NOT NULL COMMENT '1=>delivery,2=>takeaway,3=>both',
  `payment_method` enum('0','1','2') NOT NULL COMMENT '0=>cash on delivery,1=>online,2=>both',
  `is_new_customer` enum('0','1') NOT NULL COMMENT '0=>no,1=>yes',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_promocodes`
--

LOCK TABLES `lie_promocodes` WRITE;
/*!40000 ALTER TABLE `lie_promocodes` DISABLE KEYS */;
INSERT INTO `lie_promocodes` VALUES (1,'tX39y2r1',1,1,'C',0,'2016-07-30 00:00:00','2016-07-31 00:00:00','P','10','20','1','0','0',2,2,'2016-07-19 16:40:06','2016-08-01 13:56:25','1'),(2,'test123',1,0,'T',2,'2016-08-01 13:58:18','2017-08-01 00:00:00','C','50','100','1','0','0',2,2,'2016-07-28 18:25:08','2016-08-01 13:58:18','1'),(3,'l9JQLKtT',1,0,'C',0,'2016-07-20 00:00:00','2016-07-31 00:00:00','P','10','50','1','2','0',2,2,'2016-07-29 15:57:01','2016-08-01 13:56:00','1'),(4,'32xghTEB',1,0,'C',0,'2016-08-10 00:00:00','2016-08-31 00:00:00','C','100','500','1','0','0',2,0,'2016-08-10 15:57:13','2016-08-10 15:57:13','1'),(5,'CC1',0,0,'C',0,'2016-08-29 00:00:00','2016-08-31 00:00:00','C','70','100','1','0','0',2,2,'2016-08-29 11:50:32','2016-08-29 16:14:08','1'),(6,'CC123456',0,0,'C',0,'2016-08-29 00:00:00','2016-08-31 00:00:00','C','50','100','1','0','0',2,2,'2016-08-29 12:38:42','2016-08-29 12:43:03','1'),(7,'  CC123456  ',0,0,'C',0,'2016-08-29 00:00:00','2016-09-01 00:00:00','P','100','200','1','0','0',2,0,'2016-08-29 12:43:50','2016-08-29 13:43:48','2'),(8,'rftgfdtretertertreterttttttttttttttttttttttttttttt',0,0,'T',1,'2016-08-29 13:52:50','2017-11-02 00:00:00','P','50','50','1','0','0',2,2,'2016-08-29 13:41:49','2016-08-29 13:53:18','2');
/*!40000 ALTER TABLE `lie_promocodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_admin_contact_us`
--

DROP TABLE IF EXISTS `lie_rest_admin_contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_admin_contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_admin_contact_us`
--

LOCK TABLES `lie_rest_admin_contact_us` WRITE;
/*!40000 ALTER TABLE `lie_rest_admin_contact_us` DISABLE KEYS */;
INSERT INTO `lie_rest_admin_contact_us` VALUES (1,'asdasdasda','dsasdasdasd','sadasd','vikas@gmail.com','31231312312','14690205007338.jpg',1,1,'2016-07-20 18:45:00','2016-07-20 18:45:00','1'),(2,'ffsd','dfsdf','ewewfwe','vivek.jaiswal27@gmail.com','1234567890','14691000531795.jpg',1,1,'2016-07-21 16:50:53','2016-07-21 16:50:53','1'),(3,'delay','checkout','hi, this is lieferzonas','arun.alivenetsolution@gmail.com','909090909090','14701218486695.png',6,6,'2016-08-02 12:40:48','2016-08-02 12:40:48','1'),(4,'Testing','Test','test test test','bindukharub@gmail.com','9711366589','14708236642841.jpg',4,4,'2016-08-10 15:37:44','2016-08-10 15:37:44','1'),(5,'Hi testing','Hi testing','Hi testing','rajlakshmi.alivenetsolution@gmail.com','9656565656','14708910859779.jpg',1,1,'2016-08-11 10:21:25','2016-08-11 10:21:25','1'),(6,'hi testing','hi testing','hi testing','rajlakshmi.alivenetsolution@gmail.com','9656565656','14708913421303.jpg',1,1,'2016-08-11 10:25:42','2016-08-11 10:25:42','1'),(7,'Test','Test','dfsfdsfsdfdf','test@fdfdf.com','7887878787','14720447703875.jpg',16,16,'2016-08-24 18:49:30','2016-08-24 18:49:30','1'),(8,'Test','Test','dfsfdsfsdfdf','test@fdfdf.com','7887878787','14720448721115.jpg',16,16,'2016-08-24 18:51:12','2016-08-24 18:51:12','1'),(9,'sdfsfsdf','dsfsdfsfsfs','sfsfsfsdfsdfsf','vikas@gmail.com','234423423324','14721001738610.png',1,1,'2016-08-25 10:12:53','2016-08-25 10:12:53','1'),(10,'sdfsfsdf','dsfsdfsfsfs','sfsfsfsdfsdfsf','vikas@gmail.com','234423423324','14721014748556.png',1,1,'2016-08-25 10:34:34','2016-08-25 10:34:34','1'),(11,'asdads','asdaas','asdasdasdas','vikas.alivenetsolutions@gmail.com','32443242343','14721017899455.png',1,1,'2016-08-25 10:39:49','2016-08-25 10:39:49','1'),(12,'sdfsfsdf','batra','asdasdasd','batra@gmail.com','234423423324','14721023028908.png',1,1,'2016-08-25 10:48:22','2016-08-25 10:48:22','1'),(13,'sadsad','dsfsdfsfsfs','sadasdasdasd','vikas.alivenetsolutions@gmail.com','32443242343','14721027966838.png',1,1,'2016-08-25 10:56:36','2016-08-25 10:56:36','1'),(14,'test','test','hiiiii','bindukharub@gmail.com','9711366589','14733412797380.jpg',4,4,'2016-09-08 18:57:59','2016-09-08 18:57:59','1');
/*!40000 ALTER TABLE `lie_rest_admin_contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_cashback_maps`
--

DROP TABLE IF EXISTS `lie_rest_cashback_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_cashback_maps` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `cashback_percentage` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `restaurant_id` (`restaurant_id`),
  CONSTRAINT `lie_rest_cashback_maps_ibfk_1` FOREIGN KEY (`restaurant_id`) REFERENCES `lie_rest_details` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_cashback_maps`
--

LOCK TABLES `lie_rest_cashback_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_cashback_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_rest_cashback_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_categories`
--

DROP TABLE IF EXISTS `lie_rest_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `root_cat_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_categories`
--

LOCK TABLES `lie_rest_categories` WRITE;
/*!40000 ALTER TABLE `lie_rest_categories` DISABLE KEYS */;
INSERT INTO `lie_rest_categories` VALUES (1,1,1,'Potato','Potato','14707032805793.jpg','2016-07-31 00:00:00','2016-08-02 00:00:00',2,1,'2016-07-28 15:46:51','2016-08-09 06:11:20','1'),(2,1,1,'Test Category','Test Category1','1469791485537.jpg','2016-07-31 00:00:00','2016-08-01 00:00:00',2,2,'2016-07-28 15:47:27','2016-07-30 13:38:15','1'),(3,2,2,'Test Category2','Test Category2','1469791485537.jpg','2016-07-26 00:00:00','2016-07-29 00:00:00',2,2,'2016-07-28 15:49:03','2016-07-28 15:49:03','1'),(4,1,1,'Test Category4','Test Category4 Test Category4','1469791485537.jpg','2016-07-29 00:00:00','2016-07-31 00:00:00',2,2,'2016-07-29 11:59:11','2016-07-29 11:59:11','1'),(5,5,2,'Onion','test','14731494722175.jpg','2016-07-29 00:00:00','2016-07-29 00:00:00',2,4,'2016-07-29 13:45:36','2016-09-06 13:41:12','1'),(6,1,1,'teat category 5','testing','1469866409818.jpg','2016-07-30 00:00:00','2016-07-31 00:00:00',2,2,'2016-07-30 13:43:29','2016-07-30 13:43:29','1'),(7,4,1,'pasta','asdsddsdasdsadasddas','1469872343960.jpg','2016-07-30 00:00:00','2016-07-31 00:00:00',2,2,'2016-07-30 15:22:23','2016-07-30 18:05:59','1'),(8,8,4,'burgers','burgers    **  Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events','1470044875653.png','2016-08-01 00:00:00','2016-08-02 00:00:00',2,2,'2016-08-01 15:17:55','2016-08-01 15:17:55','1'),(9,8,2,'paneer','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events','1470045007846.jpg','2016-08-01 00:00:00','2016-08-30 00:00:00',2,2,'2016-08-01 15:20:07','2016-08-01 15:20:07','1'),(10,9,2,'South Indian','Just For test','1470123316900.png','2016-08-04 00:00:00','2016-08-01 00:00:00',2,2,'2016-08-02 13:05:16','2016-08-02 13:09:52','1'),(11,9,2,'Noodles','Noodles','1470124317295.png','2016-08-02 00:00:00','2016-08-10 00:00:00',2,2,'2016-08-02 13:21:57','2016-08-02 13:22:28','1'),(12,8,8,'choclate','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events.','1470131696156.jpg','2016-08-02 00:00:00','2016-08-09 00:00:00',2,2,'2016-08-02 15:24:56','2016-08-02 15:24:56','1'),(13,8,5,'veg burgers','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events.','1470132002760.png','2016-08-02 00:00:00','2016-08-09 00:00:00',2,2,'2016-08-02 15:30:02','2016-08-02 15:30:02','1'),(14,10,5,'chilly potato',' Peel potatoes and cut diagonally. Mix red chilli sauce with red chili powder using little water. Sprinkle flour, red chili powder and salt. Deep fry or grill until golden. Transfer the oil and keep little in the pan. Fry garlic and chilli until they smell good. Add onions and saute until transparent or pink.','1470140235792.jpg','2016-08-03 00:00:00','2016-08-09 00:00:00',2,2,'2016-08-02 17:47:15','2016-08-02 17:47:15','1'),(15,10,5,'paneer',' Peel potatoes and cut diagonally. Mix red chilli sauce with red chili powder using little water. Sprinkle flour, red chili powder and salt. Deep fry or grill until golden. Transfer the oil and keep little in the pan. Fry garlic and chilli until they smell good. Add onions and saute until transparent or pink.','1470140517195.jpg','2016-08-02 00:00:00','2016-08-09 00:00:00',2,2,'2016-08-02 17:51:57','2016-08-02 17:51:57','1'),(16,10,6,'veg burgers',' Peel potatoes and cut diagonally. Mix red chilli sauce with red chili powder using little water. Sprinkle flour, red chili powder and salt. Deep fry or grill until golden. Transfer the oil and keep little in the pan. Fry garlic and chilli until they smell good. Add onions and saute until transparent or pink.','1470140634167.png','2016-08-02 00:00:00','2016-08-09 00:00:00',2,2,'2016-08-02 17:53:54','2016-08-02 17:53:54','1'),(17,11,6,'Pizza','Pizza is a flatbread generally topped with tomato sauce and cheese and baked in an oven. It is commonly topped with a selection of meats, vegetables and condiments.','1470141079580.png','2016-08-03 00:00:00','2016-08-09 00:00:00',2,2,'2016-08-02 18:01:19','2016-08-02 18:01:19','1'),(18,13,9,'carrot soup',' Melt butter in heavy large pot over medium-high heat. Add onion; sauté 4 minutes. Add ginger and garlic; sauté 2 minutes. Puree soup in batches in blender. Return soup to pot. Mix in lemon juice. Bring soup to simmer, thinning with more stock, if desired. Ladle into bowls','1470142284479.jpg','2016-08-02 00:00:00','2016-08-03 00:00:00',2,2,'2016-08-02 18:21:24','2016-08-02 18:21:24','1'),(19,12,8,'cake','Cake is a form of sweet dessert that is typically baked. In its oldest forms, cakes were modifications of breads but now cover a wide range of preparations','1470142455826.jpg','2016-08-02 00:00:00','2016-08-03 00:00:00',2,2,'2016-08-02 18:24:15','2016-08-02 18:24:15','1'),(20,1,1,'Test Category8','hi','1470223126373.jpg','2016-08-23 00:00:00','2016-08-29 00:00:00',2,2,'2016-08-03 16:48:46','2016-08-03 16:48:46','1'),(21,15,11,'Pizza des Hauses','eine sehr schöne Kategorie ','1471987873861.jpeg','2016-08-05 00:00:00','2021-11-25 00:00:00',2,2,'2016-08-05 05:12:20','2016-08-24 05:48:09','1'),(22,9,2,'test','hi','1470831933968.jpg','2016-08-10 00:00:00','2016-08-15 00:00:00',2,2,'2016-08-10 17:55:33','2016-08-10 17:55:33','1'),(23,15,1,'Pasta','Italienische Pasta gerichte','1471993182652.png','2016-08-13 00:00:00','2017-07-06 00:00:00',2,2,'2016-08-13 07:09:53','2016-08-24 04:29:42','1'),(24,17,2,'North Indian','All the best','1471863579623.png','2016-08-22 00:00:00','2016-08-30 00:00:00',2,2,'2016-08-22 16:29:39','2016-08-22 16:29:39','1'),(25,17,2,'South Indian','dsfdsfsd','1471863769522.png','2016-08-22 00:00:00','2016-08-22 00:00:00',2,2,'2016-08-22 16:32:49','2016-08-22 16:32:49','1'),(26,15,12,'Di Pollo - Geflügel ','','1471998402932.png','2016-08-24 00:00:00','2019-09-02 00:00:00',2,2,'2016-08-24 05:56:42','2016-08-24 05:56:42','1'),(27,15,13,'Di Carne - Fleischgerichte ','','1472000881783.png','2016-09-01 00:00:00','2020-08-25 00:00:00',2,2,'2016-08-24 06:32:38','2016-08-24 06:41:16','1'),(28,15,2,'Vegetarisch','\r\n                                ','58249e4317abe.jpg','2000-01-01 00:00:00','2099-01-01 00:00:00',0,0,'2016-11-10 21:50:19','2016-11-10 21:50:19','1');
/*!40000 ALTER TABLE `lie_rest_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_complaints`
--

DROP TABLE IF EXISTS `lie_rest_complaints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_complaints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL DEFAULT '0',
  `order_id` varchar(255) NOT NULL DEFAULT '0',
  `topic` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `check_status` enum('R','U') NOT NULL DEFAULT 'U' COMMENT 'R->Read,U->Unread',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_complaints`
--

LOCK TABLES `lie_rest_complaints` WRITE;
/*!40000 ALTER TABLE `lie_rest_complaints` DISABLE KEYS */;
INSERT INTO `lie_rest_complaints` VALUES (1,1,'363321432744YR2','asdasdasdasd','dasdasdad','Nikhil','nikhil@gmail.com','1234567890','R',4,4,'2016-07-21 16:04:17','2016-07-22 01:38:34','1'),(2,5,'363325404323IR2','Testing','TestingTestingTestingTesting','Bindu','bindukharub@gmail.com','7865892565','R',5,5,'2016-07-25 17:44:42','2016-08-16 12:51:21','2'),(3,2,'363320470813OR2','sadsad','dsadassadasdasd','Nikhil','nikhil@gmail.com','1234567890','R',4,4,'2016-07-27 17:26:30','2016-07-28 04:44:03','1'),(4,2,'363320351800QR2','Lieferung','die lieferung ist nicht angekommen. ich will mein geld zurück die ich online bezahle','Lieferzonas','info@lieferzonas.at','066488399491','R',3,3,'2016-07-29 05:41:50','2016-07-29 05:45:02','1'),(5,1,'363329332536ZR1','hilfe','was soll das','Lieferzonas','info@lieferzonas.at','066488399491','R',3,3,'2016-08-06 04:40:17','2016-08-06 04:40:36','1'),(6,17,'363423412759TR17','Very bad restaurant','has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in','Jitendra','jitendra.alivenetsolutions@gmail.com','8860280996','R',11,11,'2016-08-26 13:17:18','2016-08-26 13:19:16','1');
/*!40000 ALTER TABLE `lie_rest_complaints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_cust_bonus`
--

DROP TABLE IF EXISTS `lie_rest_cust_bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_cust_bonus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` float(16,2) NOT NULL,
  `type` enum('p','c') NOT NULL DEFAULT 'p' COMMENT 'p for percent,c for cash',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_cust_bonus`
--

LOCK TABLES `lie_rest_cust_bonus` WRITE;
/*!40000 ALTER TABLE `lie_rest_cust_bonus` DISABLE KEYS */;
INSERT INTO `lie_rest_cust_bonus` VALUES (1,10.00,'p',2,2,'2016-07-22 14:11:39','2016-07-22 14:11:39','1');
/*!40000 ALTER TABLE `lie_rest_cust_bonus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_cust_bonus_maps`
--

DROP TABLE IF EXISTS `lie_rest_cust_bonus_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_cust_bonus_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `bonus_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_cust_bonus_maps`
--

LOCK TABLES `lie_rest_cust_bonus_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_cust_bonus_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_cust_bonus_maps` VALUES (1,1,'1',2,2,'2016-07-20 12:30:12','2016-07-20 18:13:41','1'),(2,5,'1',2,2,'2016-07-25 18:24:50','2016-09-06 12:10:16','1'),(3,6,'1',2,2,'2016-07-27 12:45:55','2016-07-27 12:45:55','1'),(4,15,'1',2,2,'2016-08-12 06:13:19','2016-09-15 06:10:14','1');
/*!40000 ALTER TABLE `lie_rest_cust_bonus_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_daily_meals`
--

DROP TABLE IF EXISTS `lie_rest_daily_meals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_daily_meals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL DEFAULT '0',
  `meal_name` varchar(255) NOT NULL,
  `meal_desc` text NOT NULL,
  `meal_price` varchar(255) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `active_time_from` varchar(255) NOT NULL,
  `active_time_to` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_daily_meals`
--

LOCK TABLES `lie_rest_daily_meals` WRITE;
/*!40000 ALTER TABLE `lie_rest_daily_meals` DISABLE KEYS */;
INSERT INTO `lie_rest_daily_meals` VALUES (1,2,'Test Meal','Summer special meal','15','2016-07-22 00:00:00','2016-12-08 00:00:00','09:00 AM','11:00 AM',2,2,'2016-08-22 07:14:47','2016-08-22 07:14:47','1'),(2,5,'Test1','Test test test','100','2016-07-25 00:00:00','2016-07-25 00:00:00','04:00 PM','11:59 PM',2,2,'2016-07-25 16:55:21','2016-07-25 16:55:21','1'),(3,8,'burger','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events','100','2016-08-01 00:00:00','2016-08-01 00:00:00','00:00 AM','00:30 AM',2,2,'2016-08-01 15:22:16','2016-08-01 16:28:35','2'),(4,8,'special paneer','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events \r\nDescription is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events','100','2016-08-01 00:00:00','2016-08-29 00:00:00','05:30 PM','11:59 PM',2,2,'2016-08-10 17:49:16','2016-08-10 17:49:16','1'),(5,5,'Parantha','Test test','100','2016-08-10 00:00:00','2016-09-15 00:00:00','00:00 AM','00:30 AM',2,2,'2016-09-06 16:22:12','2016-09-06 16:22:12','1'),(6,15,'di Polo text','hukljnm,','12.49','2016-08-26 00:00:00','2017-08-26 00:00:00','00:00 AM','00:30 AM',2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(7,15,'abcde','beschriftung','2.99','2016-09-15 00:00:00','2016-09-15 00:00:00','00:00 AM','11:59 PM',2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1');
/*!40000 ALTER TABLE `lie_rest_daily_meals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_day_time_maps`
--

DROP TABLE IF EXISTS `lie_rest_day_time_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_day_time_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `open_time` varchar(50) NOT NULL,
  `close_time` varchar(50) NOT NULL,
  `is_open` int(11) NOT NULL,
  `is_24` int(11) NOT NULL COMMENT '1 for open, 0 for close',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_day_time_maps`
--

LOCK TABLES `lie_rest_day_time_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_day_time_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_day_time_maps` VALUES (1,1,1,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-19 18:10:11','2016-07-20 18:11:32','1'),(2,1,2,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-19 18:10:11','2016-07-20 18:11:32','1'),(3,1,3,'00:00 AM','00:30 AM',1,0,2,2,'2016-07-19 18:10:11','2016-07-20 18:11:32','1'),(4,1,4,'00:00 AM','00:30 AM',1,0,2,2,'2016-07-19 18:10:11','2016-07-20 18:11:32','1'),(5,1,5,'08:30 AM','09:00 PM',1,0,2,2,'2016-07-19 18:10:11','2016-07-20 18:11:32','1'),(6,1,6,'','',0,0,2,2,'2016-07-19 18:10:11','2016-07-20 18:11:32','1'),(7,1,7,'','',0,0,2,2,'2016-07-19 18:10:11','2016-07-20 18:11:32','1'),(8,2,1,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-20 12:29:15','2016-07-20 12:29:15','1'),(9,2,2,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-20 12:29:15','2016-07-20 12:29:15','1'),(10,2,3,'01:30 AM','05:00 AM',1,0,2,2,'2016-07-20 12:29:15','2016-07-20 12:29:15','1'),(11,2,4,'04:30 AM','09:30 AM',1,0,2,2,'2016-07-20 12:29:15','2016-07-20 12:29:15','1'),(12,2,5,'05:00 AM','10:00 AM',1,0,2,2,'2016-07-20 12:29:15','2016-07-20 12:29:15','1'),(13,2,6,'04:30 AM','08:00 AM',1,0,2,2,'2016-07-20 12:29:15','2016-07-20 12:29:15','1'),(14,2,7,'01:30 AM','10:00 AM',1,0,2,2,'2016-07-20 12:29:15','2016-07-20 12:29:15','1'),(15,4,1,'08:00 AM','10:00 AM',1,0,2,2,'2016-07-22 15:08:35','2016-07-22 15:08:35','1'),(16,4,2,'','',0,0,2,2,'2016-07-22 15:08:35','2016-07-22 15:08:35','1'),(17,4,3,'00:00 AM','00:30 AM',1,0,2,2,'2016-07-22 15:08:35','2016-07-22 15:08:35','1'),(18,4,4,'','',0,0,2,2,'2016-07-22 15:08:35','2016-07-22 15:08:35','1'),(19,4,5,'','',0,0,2,2,'2016-07-22 15:08:35','2016-07-22 15:08:35','1'),(20,4,6,'','',0,0,2,2,'2016-07-22 15:08:35','2016-07-22 15:08:35','1'),(21,4,7,'','',0,0,2,2,'2016-07-22 15:08:35','2016-07-22 15:08:35','1'),(22,5,1,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-25 15:43:41','2016-09-06 11:30:47','1'),(23,5,2,'04:00 AM','11:59 PM',1,0,2,2,'2016-07-25 15:43:41','2016-09-06 11:30:47','1'),(24,5,3,'07:00 AM','11:00 AM',1,0,2,2,'2016-07-25 15:43:41','2016-09-06 11:30:47','1'),(25,5,4,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-25 15:43:41','2016-09-06 11:30:47','1'),(26,5,5,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-25 15:43:41','2016-09-06 11:30:47','1'),(27,5,6,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-25 15:43:41','2016-09-06 11:30:47','1'),(28,5,7,'','',0,0,2,2,'2016-07-25 15:43:41','2016-09-06 11:30:47','1'),(29,6,1,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-27 12:39:00','2016-07-27 12:39:00','1'),(30,6,2,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-27 12:39:00','2016-07-27 12:39:00','1'),(31,6,3,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-27 12:39:00','2016-07-27 12:39:00','1'),(32,6,4,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-27 12:39:00','2016-07-27 12:39:00','1'),(33,6,5,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-27 12:39:00','2016-07-27 12:39:00','1'),(34,6,6,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-27 12:39:00','2016-07-27 12:39:00','1'),(35,6,7,'00:00 AM','11:59 PM',1,1,2,2,'2016-07-27 12:39:00','2016-07-27 12:39:00','1'),(36,15,1,'00:00 AM','11:59 PM',1,1,2,2,'2016-08-13 04:40:25','2016-08-13 05:45:38','1'),(37,15,2,'00:00 AM','11:59 PM',1,1,2,2,'2016-08-13 04:40:25','2016-08-13 05:45:38','1'),(38,15,3,'00:00 AM','11:59 PM',1,0,2,2,'2016-08-13 04:40:25','2016-08-13 05:45:38','1'),(39,15,4,'11:00 AM','05:00 PM',1,0,2,2,'2016-08-13 04:40:25','2016-08-13 05:45:38','1'),(40,15,5,'00:00 AM','11:59 PM',1,1,2,2,'2016-08-13 04:40:25','2016-08-13 05:45:38','1'),(41,15,6,'00:00 AM','11:59 PM',1,1,2,2,'2016-08-13 04:40:25','2016-08-13 05:45:38','1'),(42,15,7,'00:00 AM','11:59 PM',1,1,2,2,'2016-08-13 04:40:25','2016-08-13 05:45:38','1'),(52,14,1,'00:00 AM','00:30 AM',1,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(53,14,1,'00:00 AM','00:30 AM',1,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(54,14,2,'','',0,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(55,14,3,'00:00 AM','00:30 AM',1,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(56,14,3,'00:00 AM','00:30 AM',1,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(57,14,4,'','',0,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(58,14,5,'','',0,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(59,14,6,'','',0,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(60,14,7,'','',0,0,2,2,'2016-09-09 18:17:01','2016-09-09 18:17:01','1'),(61,17,1,'00:00 AM','11:59 PM',1,1,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1'),(62,17,2,'04:00 AM','06:00 AM',1,0,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1'),(63,17,2,'10:00 AM','02:00 PM',1,0,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1'),(64,17,2,'07:00 AM','11:00 AM',1,0,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1'),(65,17,3,'','',0,0,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1'),(66,17,4,'','',0,0,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1'),(67,17,5,'','',0,0,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1'),(68,17,6,'','',0,0,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1'),(69,17,7,'','',0,0,2,2,'2016-09-10 16:33:51','2016-09-10 16:33:51','1');
/*!40000 ALTER TABLE `lie_rest_day_time_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_days`
--

DROP TABLE IF EXISTS `lie_rest_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_days`
--

LOCK TABLES `lie_rest_days` WRITE;
/*!40000 ALTER TABLE `lie_rest_days` DISABLE KEYS */;
INSERT INTO `lie_rest_days` VALUES (1,'Monday',0,0,'2016-04-28 10:59:37','2016-04-28 10:59:37','1'),(2,'Tuesday',0,0,'2016-04-28 10:59:37','2016-04-28 10:59:37','1'),(3,'Wednesday',0,0,'2016-04-28 10:59:37','2016-04-28 10:59:37','1'),(4,'Thursday',0,0,'2016-04-28 10:59:37','2016-04-28 10:59:37','1'),(5,'Friday',0,0,'2016-04-28 10:59:37','2016-04-28 10:59:37','1'),(6,'Saturday',0,0,'2016-04-28 10:59:37','2016-04-28 10:59:37','1'),(7,'Sunday',0,0,'2016-04-28 10:59:37','2016-04-28 10:59:37','1');
/*!40000 ALTER TABLE `lie_rest_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_delivery_costs`
--

DROP TABLE IF EXISTS `lie_rest_delivery_costs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_delivery_costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant` int(11) NOT NULL,
  `minimum_order_amount` decimal(10,2) NOT NULL,
  `below_order_delivery_cost` decimal(10,2) DEFAULT NULL,
  `above_order_delivery_cost` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `restaurant` (`restaurant`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_delivery_costs`
--

LOCK TABLES `lie_rest_delivery_costs` WRITE;
/*!40000 ALTER TABLE `lie_rest_delivery_costs` DISABLE KEYS */;
INSERT INTO `lie_rest_delivery_costs` VALUES (1,1,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(2,2,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(3,3,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(4,4,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(5,5,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(6,6,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(7,8,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(8,9,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(9,10,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(10,11,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(11,12,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(12,13,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(13,14,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(14,15,10.00,2.00,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(15,17,10.00,NULL,0.00,'2000-01-01 00:00:00','2000-01-01 00:00:00',1),(16,18,6.00,NULL,0.00,'2000-01-01 00:00:00','2016-12-02 00:29:14',1);
/*!40000 ALTER TABLE `lie_rest_delivery_costs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_details`
--

DROP TABLE IF EXISTS `lie_rest_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `add1` varchar(100) NOT NULL,
  `add2` varchar(100) NOT NULL,
  `city` varchar(40) NOT NULL,
  `state` varchar(40) NOT NULL,
  `country` varchar(40) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `tin_number` varchar(50) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `total_email` int(11) NOT NULL,
  `total_mobile` int(11) NOT NULL,
  `total_landline` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_details`
--

LOCK TABLES `lie_rest_details` WRITE;
/*!40000 ALTER TABLE `lie_rest_details` DISABLE KEYS */;
INSERT INTO `lie_rest_details` VALUES (1,'Eastern Restaurant','','','Quellenstrasse 167','Favoriten','11','21','4','1','1234567','28.5099861','77.292222','','1470971449950.png',1,1,0,2,1,'2016-07-19 16:23:38','2016-09-08 17:40:46','1'),(2,'Food Plaza','','','Meena Bazar near temple','motihari','24','18','4','','7y657tyut','28.53672','77.2660866','11254215','1471860861368.png',2,2,1,2,2,'2016-07-19 17:02:03','2016-08-22 15:44:21','1'),(3,'The Hunger House','','','lane 1','street 54','2','1','1','4','','28.256425','77.365848','','1468937483489.jpg',1,1,1,2,2,'2016-07-19 19:41:23','2016-07-19 19:41:23','1'),(4,'The Chicken Curry','','','add 1','add 2','4','3','4','3','','','','152055','1469177770746.jpg',1,1,1,2,2,'2016-07-22 14:26:10','2016-07-22 14:26:10','1'),(5,'Test Restaurant','','','12345','fgjhg','11','21','4','1','12345','','','','1480576492245.jpg',1,1,3,2,4,'2016-07-25 13:56:40','2016-12-01 08:14:52','1'),(6,'test','','','test','test','12','11','1','1','fhfdfkgfh','','','','1469520700889.jpg',1,1,1,2,5,'2016-07-26 13:41:40','2016-07-29 13:49:46','2'),(8,'Sagar Ratna restaurant','','','A11 sec 37','huda market','1','1','1','','','','','123456789','1470031953289.gif',1,1,1,2,6,'2016-08-01 11:42:33','2016-08-02 10:56:31','1'),(9,'Black Spoon','','','A-81','A-82','1','1','1','1','202135, 201301','','','201301','1470053954244.jpg',1,1,1,2,7,'2016-08-01 17:49:14','2016-08-11 12:55:50','1'),(10,'KFC','','','civil line ','gurgaon haryana','1','1','1','','','','','21234545','1470114713880.png',1,1,1,2,2,'2016-08-02 10:41:53','2016-08-02 10:53:45','1'),(11,'dominos','','','sec37 huda market','haryana','11','3','4','11','','','','43556677','1470120909495.jpg',1,1,1,2,2,'2016-08-02 11:10:26','2016-08-02 12:25:09','1'),(12,'elchico','','','subhas chowk, sohna road','haryana','12','11','1','1','','','','1245498','1470135799727.png',1,1,1,2,2,'2016-08-02 16:33:19','2016-08-02 16:33:19','1'),(13,'Fassos','','','subhas chowk, sohna road','haryana','12','11','1','3','','','','21234545','1470136019640.jpg',1,1,1,2,2,'2016-08-02 16:36:59','2016-08-02 16:36:59','1'),(14,'Bikaner','','','shop no 44','sec 18','13','12','11','13','','','','323121','1470219378581.jpg',1,1,1,2,2,'2016-08-03 15:46:18','2016-08-03 16:00:42','1'),(15,'Pizzeria Ristorante  Castello','','','Herzgasse 12','Favoriten','11','21','4','1','','','','123456789101213','1472078670492.png',1,1,1,2,15,'2016-08-05 04:53:38','2016-09-07 22:12:06','1'),(17,'The Dosa King','','','New Delhi ','Vivek Vihar','22','13','4','1','','','','1010','1471862130833.jpg',1,1,1,2,16,'2016-08-22 16:05:30','2016-08-22 17:14:24','1'),(18,'Biryani House','','','fdsfdsfdsfdfd','juyjtyj','11','21','4','1','','','','54534','1472109424446.png',1,1,1,2,2,'2016-08-25 12:47:04','2016-08-30 15:27:58','1');
/*!40000 ALTER TABLE `lie_rest_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_email_maps`
--

DROP TABLE IF EXISTS `lie_rest_email_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_email_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `email_type` varchar(30) NOT NULL,
  `is_primary` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_email_maps`
--

LOCK TABLES `lie_rest_email_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_email_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_email_maps` VALUES (11,3,'hunger@gmail.com','2','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(15,4,'chickencurry@gmail.com','2','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(20,6,'bindukharub@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2'),(29,10,'KFC1@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(30,8,'Sagarratna1@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(37,11,'dominnos@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(40,9,'bs@gmail.com','1','1',0,7,'0000-00-00 00:00:00','2016-08-11 12:55:50','1'),(41,12,'elchico@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(42,13,'Fassos@gmail.com','2','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(44,14,'rajlakshmi017@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(54,2,'food@gmail.com','1','0',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(55,2,'foodpalaza@gmail.com','2','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(63,17,'tdk@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(64,15,'support@lieferzonas.at','1','1',0,15,'0000-00-00 00:00:00','2016-09-07 22:12:06','1'),(70,1,'batra@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(74,5,'bindukharub@gmail.com','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(86,18,'fsfdsf@rere.jhj','1','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1');
/*!40000 ALTER TABLE `lie_rest_email_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_extra_maps`
--

DROP TABLE IF EXISTS `lie_rest_extra_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_extra_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_extra_type_id` int(11) NOT NULL,
  `menu_extra_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_extra_maps`
--

LOCK TABLES `lie_rest_extra_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_extra_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_extra_maps` VALUES (1,2,1,1,2,2,'2016-07-19 18:20:31','2016-07-19 18:20:31','1'),(2,4,1,1,2,2,'2016-07-22 14:52:42','2016-07-22 14:52:42','1'),(6,15,1,1,2,2,'2016-09-16 06:02:45','2016-09-16 06:02:45','1');
/*!40000 ALTER TABLE `lie_rest_extra_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_extra_type_maps`
--

DROP TABLE IF EXISTS `lie_rest_extra_type_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_extra_type_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_extra_type_id` int(11) NOT NULL,
  `price` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_extra_type_maps`
--

LOCK TABLES `lie_rest_extra_type_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_extra_type_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_extra_type_maps` VALUES (1,2,1,'',2,2,'2016-07-19 18:20:17','2016-07-19 18:20:17','1'),(2,1,1,'',2,2,'2016-07-20 06:20:23','2016-07-20 06:20:23','1'),(3,4,1,'',2,2,'2016-07-22 14:52:00','2016-07-22 14:52:00','1'),(4,15,1,'',2,2,'2016-08-12 07:58:36','2016-08-12 07:58:36','1');
/*!40000 ALTER TABLE `lie_rest_extra_type_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_kitchen_maps`
--

DROP TABLE IF EXISTS `lie_rest_kitchen_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_kitchen_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_kitchen_maps`
--

LOCK TABLES `lie_rest_kitchen_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_kitchen_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_kitchen_maps` VALUES (1,2,1,2,2,'2016-07-19 17:12:54','2016-07-19 17:12:54','1'),(2,2,2,2,2,'2016-07-19 17:12:54','2016-07-19 17:12:54','1'),(3,2,5,2,2,'2016-07-19 17:12:54','2016-07-19 17:12:54','1'),(4,2,9,2,2,'2016-07-19 17:12:54','2016-07-19 17:12:54','1'),(5,1,1,2,2,'2016-07-20 06:16:21','2016-07-20 06:16:21','1'),(6,1,6,2,2,'2016-07-20 06:16:21','2016-07-20 06:16:21','1'),(7,1,12,2,2,'2016-07-20 06:16:21','2016-07-20 06:16:21','1'),(8,4,3,2,2,'2016-07-22 14:29:01','2016-07-22 14:29:01','1'),(9,4,5,2,2,'2016-07-22 14:29:01','2016-07-22 14:29:01','1'),(10,4,9,2,2,'2016-07-22 14:29:01','2016-07-22 14:29:01','1'),(11,4,10,2,2,'2016-07-22 14:29:01','2016-07-22 14:29:01','1'),(12,4,12,2,2,'2016-07-22 14:29:01','2016-07-22 14:29:01','1'),(13,5,1,2,2,'2016-07-25 13:57:14','2016-07-25 13:57:14','1'),(14,5,2,2,2,'2016-07-25 13:57:14','2016-07-25 13:57:14','1'),(15,5,4,2,2,'2016-07-25 13:57:14','2016-07-25 13:57:14','1'),(16,6,1,2,2,'2016-07-27 12:31:56','2016-07-27 12:31:56','1'),(17,6,2,2,2,'2016-07-27 12:31:56','2016-07-27 12:31:56','1'),(40,9,13,2,2,'2016-08-02 13:14:47','2016-08-02 13:14:47','1'),(41,9,14,2,2,'2016-08-02 13:14:47','2016-08-02 13:14:47','1'),(42,11,1,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(43,11,2,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(44,11,3,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(45,11,4,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(46,11,5,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(47,11,6,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(48,11,7,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(49,11,8,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(50,11,9,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(51,11,12,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(52,11,13,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(53,11,14,2,2,'2016-08-02 13:18:56','2016-08-02 13:18:56','1'),(54,8,1,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(55,8,2,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(56,8,3,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(57,8,4,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(58,8,5,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(59,8,6,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(60,8,7,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(61,8,8,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(62,8,9,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(63,8,10,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(64,8,12,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(65,8,14,2,2,'2016-08-02 13:19:43','2016-08-02 13:19:43','1'),(103,13,1,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(104,13,2,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(105,13,3,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(106,13,4,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(107,13,5,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(108,13,6,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(109,13,7,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(110,13,8,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(111,13,9,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(112,13,10,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(113,13,12,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(114,13,13,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(115,13,14,2,2,'2016-08-02 18:26:21','2016-08-02 18:26:21','1'),(116,10,1,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(117,10,2,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(118,10,3,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(119,10,4,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(120,10,5,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(121,10,6,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(122,10,7,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(123,10,8,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(124,10,9,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(125,10,10,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(126,10,12,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(127,10,13,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(128,10,14,2,2,'2016-08-02 18:38:34','2016-08-02 18:38:34','1'),(129,3,1,2,2,'2016-08-02 18:40:01','2016-08-02 18:40:01','1'),(130,3,5,2,2,'2016-08-02 18:40:01','2016-08-02 18:40:01','1'),(131,3,9,2,2,'2016-08-02 18:40:01','2016-08-02 18:40:01','1'),(132,3,14,2,2,'2016-08-02 18:40:01','2016-08-02 18:40:01','1'),(133,12,1,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(134,12,2,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(135,12,3,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(136,12,4,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(137,12,5,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(138,12,6,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(139,12,7,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(140,12,8,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(141,12,9,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(142,12,10,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(143,12,12,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(144,12,13,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(145,12,14,2,2,'2016-08-02 18:40:44','2016-08-02 18:40:44','1'),(158,17,13,2,2,'2016-08-22 16:32:07','2016-08-22 16:32:07','1'),(159,17,14,2,2,'2016-08-22 16:32:07','2016-08-22 16:32:07','1'),(160,17,15,2,2,'2016-08-22 16:32:07','2016-08-22 16:32:07','1'),(161,15,1,2,2,'2016-08-24 07:04:14','2016-08-24 07:04:14','1'),(162,15,6,2,2,'2016-08-24 07:04:14','2016-08-24 07:04:14','1'),(163,15,7,2,2,'2016-08-24 07:04:14','2016-08-24 07:04:14','1'),(164,15,9,2,2,'2016-08-24 07:04:14','2016-08-24 07:04:14','1'),(165,15,12,2,2,'2016-08-24 07:04:14','2016-08-24 07:04:14','1'),(166,15,16,2,2,'2016-08-24 07:04:14','2016-08-24 07:04:14','1'),(167,15,17,2,2,'2016-08-24 07:04:14','2016-08-24 07:04:14','1');
/*!40000 ALTER TABLE `lie_rest_kitchen_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_landline_maps`
--

DROP TABLE IF EXISTS `lie_rest_landline_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_landline_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `landline` varchar(15) NOT NULL,
  `extension` varchar(6) NOT NULL,
  `name` varchar(40) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_landline_maps`
--

LOCK TABLES `lie_rest_landline_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_landline_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_landline_maps` VALUES (7,3,'123456789','','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(9,4,'25698741','12','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(16,6,'1254828','','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2'),(27,10,'9898989898','098','manager',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(28,8,'1234567890','12345','abc',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(35,11,'90909090909090','876','director',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(38,9,'23421085','121','GB nagar',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(39,12,'12345678791','2345','founder',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(40,13,'9898989898','548548','qc',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(42,14,'423325235','3344','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(48,2,'06252','30181','Reception',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(56,17,'8860280996','2342','owner',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(57,15,'06644975080','','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(78,5,'1291526896','','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(79,5,'1291526896','','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(80,5,'1291526896','','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(92,18,'45345345345','534','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1');
/*!40000 ALTER TABLE `lie_rest_landline_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_meal_item_maps`
--

DROP TABLE IF EXISTS `lie_rest_meal_item_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_meal_item_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL DEFAULT '0',
  `meal_id` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `item_type` varchar(255) NOT NULL DEFAULT '',
  `item_name` varchar(255) NOT NULL DEFAULT '',
  `item_quantity` varchar(255) NOT NULL DEFAULT '0',
  `item_price` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_meal_item_maps`
--

LOCK TABLES `lie_rest_meal_item_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_meal_item_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_meal_item_maps` VALUES (1,2,1,1,'submenu','Test Sbumenu','1','',2,2,'2016-07-22 13:58:25','2016-08-22 07:14:47','1'),(2,2,1,1,'menu','Chilli Potato','4','',2,2,'2016-07-22 13:58:25','2016-08-22 07:14:47','1'),(3,5,2,6,'submenu','Parantha','5','',2,2,'2016-07-25 16:55:21','2016-07-25 16:55:21','1'),(4,8,3,7,'submenu','Bharta','1','',2,2,'2016-08-01 15:22:16','2016-08-01 15:22:16','1'),(5,8,4,7,'submenu','Bharta','1','',2,2,'2016-08-01 16:33:37','2016-08-10 17:49:16','1'),(6,5,5,6,'submenu','Parantha','1','',2,2,'2016-08-10 18:08:10','2016-09-06 16:22:12','1'),(7,15,6,21,'submenu','Pollo alla Milanese ','2','',2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(8,5,5,27,'submenu','Dahi parantha','1','',2,2,'2016-09-06 16:22:12','2016-09-06 16:22:12','1'),(9,15,7,13,'submenu','Pomodoro','1','',2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1');
/*!40000 ALTER TABLE `lie_rest_meal_item_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_meal_valid_days`
--

DROP TABLE IF EXISTS `lie_rest_meal_valid_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_meal_valid_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_id` int(11) NOT NULL DEFAULT '0',
  `day_id` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_meal_valid_days`
--

LOCK TABLES `lie_rest_meal_valid_days` WRITE;
/*!40000 ALTER TABLE `lie_rest_meal_valid_days` DISABLE KEYS */;
INSERT INTO `lie_rest_meal_valid_days` VALUES (1,1,6,2,2,'2016-07-22 13:58:25','2016-08-22 07:14:47','1'),(2,1,7,2,2,'2016-07-22 13:58:25','2016-08-22 07:14:47','1'),(3,2,1,2,2,'2016-07-25 16:55:21','2016-07-25 16:55:21','1'),(4,2,2,2,2,'2016-07-25 16:55:21','2016-07-25 16:55:21','1'),(5,1,1,2,2,'2016-07-25 16:56:56','2016-08-22 07:14:47','1'),(6,3,6,2,2,'2016-08-01 15:22:16','2016-08-01 15:22:16','1'),(7,3,7,2,2,'2016-08-01 15:22:16','2016-08-01 15:22:16','1'),(8,4,5,2,2,'2016-08-01 16:33:37','2016-08-10 17:49:16','1'),(9,4,6,2,2,'2016-08-01 16:33:37','2016-08-10 17:49:16','1'),(10,4,7,2,2,'2016-08-01 16:33:37','2016-08-10 17:49:16','1'),(11,5,1,2,2,'2016-08-10 18:08:10','2016-09-06 16:22:12','1'),(12,5,2,2,2,'2016-08-10 18:08:10','2016-09-06 16:22:12','1'),(13,5,3,2,2,'2016-08-10 18:08:10','2016-09-06 16:22:12','1'),(14,5,4,2,2,'2016-08-10 18:08:10','2016-09-06 16:22:12','1'),(15,5,5,2,2,'2016-08-10 18:08:10','2016-09-06 16:22:12','1'),(16,5,6,2,2,'2016-08-10 18:08:10','2016-09-06 16:22:12','1'),(17,1,2,2,2,'2016-08-22 07:14:47','2016-08-22 07:14:47','1'),(18,1,3,2,2,'2016-08-22 07:14:47','2016-08-22 07:14:47','1'),(19,1,4,2,2,'2016-08-22 07:14:47','2016-08-22 07:14:47','1'),(20,1,5,2,2,'2016-08-22 07:14:47','2016-08-22 07:14:47','1'),(21,6,1,2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(22,6,2,2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(23,6,3,2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(24,6,4,2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(25,6,5,2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(26,6,6,2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(27,6,7,2,2,'2016-08-26 05:51:19','2016-08-26 05:51:19','1'),(28,7,1,2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1'),(29,7,2,2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1'),(30,7,3,2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1'),(31,7,4,2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1'),(32,7,5,2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1'),(33,7,6,2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1'),(34,7,7,2,2,'2016-09-15 06:18:28','2016-09-15 06:18:28','1');
/*!40000 ALTER TABLE `lie_rest_meal_valid_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_menu_allergic_maps`
--

DROP TABLE IF EXISTS `lie_rest_menu_allergic_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_menu_allergic_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `allergic_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_menu_allergic_maps`
--

LOCK TABLES `lie_rest_menu_allergic_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_menu_allergic_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_menu_allergic_maps` VALUES (11,1,11,1,2,2,'2016-08-01 11:28:29','2016-08-01 11:28:29','1'),(12,8,18,1,2,2,'2016-08-02 18:49:41','2016-08-02 18:49:41','1'),(19,17,21,6,2,2,'2016-08-22 16:52:07','2016-08-22 16:52:07','1'),(20,17,21,7,2,2,'2016-08-22 16:52:07','2016-08-22 16:52:07','1'),(24,15,19,11,2,2,'2016-08-24 02:57:33','2016-08-24 02:57:33','1'),(25,15,19,15,2,2,'2016-08-24 02:57:33','2016-08-24 02:57:33','1'),(26,15,19,19,2,2,'2016-08-24 02:57:33','2016-08-24 02:57:33','1'),(43,17,25,6,2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','1'),(44,17,25,10,2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','1'),(45,17,25,14,2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','1'),(46,17,25,18,2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','1');
/*!40000 ALTER TABLE `lie_rest_menu_allergic_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_menu_spice_maps`
--

DROP TABLE IF EXISTS `lie_rest_menu_spice_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_menu_spice_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `spice_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_menu_spice_maps`
--

LOCK TABLES `lie_rest_menu_spice_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_menu_spice_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_menu_spice_maps` VALUES (6,8,18,1,2,2,'2016-08-02 18:49:41','2016-08-02 18:49:41','1'),(34,17,21,1,2,2,'2016-08-22 16:52:07','2016-08-22 16:52:07','1'),(35,17,21,2,2,2,'2016-08-22 16:52:07','2016-08-22 16:52:07','1'),(39,15,19,6,2,2,'2016-08-24 02:57:33','2016-08-24 02:57:33','1'),(46,17,25,4,2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','1'),(47,17,25,8,2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','1'),(48,17,25,12,2,2,'2016-08-25 12:45:14','2016-08-25 12:45:14','1'),(49,1,3,4,2,2,'2016-08-26 11:52:41','2016-08-26 11:52:41','1'),(50,1,3,5,2,2,'2016-08-26 11:52:41','2016-08-26 11:52:41','1'),(51,1,3,6,2,2,'2016-08-26 11:52:41','2016-08-26 11:52:41','1'),(52,1,3,10,2,2,'2016-08-26 11:52:41','2016-08-26 11:52:41','1'),(53,1,3,11,2,2,'2016-08-26 11:52:41','2016-08-26 11:52:41','1'),(54,5,6,4,2,2,'2016-09-07 11:54:01','2016-09-07 11:54:01','1');
/*!40000 ALTER TABLE `lie_rest_menu_spice_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_menus`
--

DROP TABLE IF EXISTS `lie_rest_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `rest_category_id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(150) NOT NULL,
  `code` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `info` varchar(255) NOT NULL,
  `priority` varchar(20) NOT NULL,
  `have_submenu` enum('0','1') NOT NULL COMMENT '''0=>no submenu,1=>having submenu',
  `is_alergic` enum('0','1') NOT NULL COMMENT '0=>no alergic,1=>its alergic',
  `is_spicy` enum('0','1') NOT NULL COMMENT '0=>no spicy,1=>its spicy',
  `out_of_stock` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>no,1=>yes',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_menus`
--

LOCK TABLES `lie_rest_menus` WRITE;
/*!40000 ALTER TABLE `lie_rest_menus` DISABLE KEYS */;
INSERT INTO `lie_rest_menus` VALUES (1,2,1,2,'Chilli Potato','','Chp151','','1468930378184.jpg','This is Chilli Potato','15','0','0','0','0',2,2,'2016-07-19 17:42:58','2016-07-19 17:42:58','1'),(2,2,2,2,'Test Menu','','Tst558','This is a test dscription','1468930378184.jpg','This is a test menu','20','1','0','0','0',2,2,'2016-07-19 17:45:31','2016-07-19 17:46:02','1'),(3,1,1,1,'Pasta','hallo','werwas','testing','1472192561516.png','neue pastas','1','0','1','1','1',2,1,'2016-07-20 06:18:00','2016-09-09 18:23:10','1'),(4,1,4,1,'All Tonno','','2','','1470969977158.png','info','2','0','0','1','0',2,1,'2016-07-20 06:35:39','2016-08-30 15:14:13','1'),(5,4,5,9,'MENU 1','m1','M012','description','1468930378184.jpg','information','1','1','0','0','0',2,2,'2016-07-22 14:49:49','2016-07-22 14:49:49','1'),(6,5,5,1,'OnionParantha','Parantha','01','test','1471932067263.jpg','test','1','0','1','1','0',2,2,'2016-07-25 15:58:19','2016-09-07 11:53:26','1'),(7,2,1,1,'test potato menu','potato','pot123','gfhgfhgh','1468930378184.jpg','rtretret','1','0','0','0','0',2,2,'2016-07-25 16:47:18','2016-07-25 16:47:18','1'),(8,6,8,2,'Baingan bharta','Bharta','BB','','1468930378184.jpg','12','2','1','0','0','0',2,2,'2016-07-27 12:34:02','2016-07-27 12:34:02','1'),(9,1,1,1,'Potato chips','Potato chips','1234','testing','1469862629764.jpg','testing','16','0','0','0','0',2,1,'2016-07-30 12:40:29','2016-08-30 15:14:17','1'),(10,1,1,1,'Potato chilly','','1234','testing','1469874966875.jpg','testing','3','1','0','0','0',2,2,'2016-07-30 16:06:06','2016-07-30 16:06:06','1'),(11,1,1,1,'finger chipps','chips','12345','sdsdsdsd','1470031109259.jpg','ddsdsds','2','0','1','0','1',2,1,'2016-08-01 11:28:29','2016-09-09 18:23:21','1'),(12,4,7,3,'manchurian','manchurian','manchurian123','sad ds s sf sdf sdfs fsdf sdf','1470031273897.jpg','asgdha dha sdasd ','5','0','1','1','0',2,2,'2016-08-01 11:31:13','2016-08-01 11:31:13','1'),(13,9,10,13,'Dosa','DOSA','001','Just For test','','Dosa','1','1','0','0','0',2,2,'2016-08-02 13:16:17','2016-08-02 13:16:17','1'),(14,9,11,14,'Maggi','Maggi','004','Maggi','','Maggi','004','1','0','0','0',2,2,'2016-08-02 13:23:26','2016-08-02 13:23:26','1'),(15,13,18,2,'hot carrot soup ','carrt soop','C1','','1470142885612.jpg','good','1','1','0','0','0',2,2,'2016-08-02 18:31:25','2016-08-02 18:31:25','1'),(16,8,9,2,'butter paneer','butter paneer','B11','','1470143064808.jpg','test','1','0','0','0','0',2,2,'2016-08-02 18:34:24','2016-08-02 18:34:24','1'),(17,10,14,0,'potato','pto','pt11','','','123','1','0','0','0','0',2,2,'2016-08-02 18:37:31','2016-08-02 18:37:31','1'),(18,8,9,2,'paneer masala','pm','pm11','','1470143981644.jpg','1','1','0','1','1','0',2,2,'2016-08-02 18:49:41','2016-08-02 18:49:41','1'),(19,15,21,1,'Pizza des Hauses','Haus short','3','','1470968427607.gif','Hier finden sie die besten Pizzen unter den klassiker','1','0','1','1','0',2,15,'2016-08-05 05:14:45','2016-09-29 02:45:16','1'),(20,15,23,1,'Pasta ','','4','','1471994055916.png','Alle Gerichte werden mit Nudelsorte Ihrer Wahl serviert.','4','1','1','1','0',2,2,'2016-08-13 07:15:35','2016-08-24 04:54:31','1'),(21,17,25,13,'Dosa','Dosa','001','Test the application','1471863903934.jpg','Dosa','01','0','1','1','0',2,16,'2016-08-22 16:35:03','2016-08-30 16:35:14','1'),(22,15,26,1,'Di Pollo - Geflügel ','','5','','1471997743556.jpg','mit und ohne untertittel!!!!!! FUNKTION','5','1','1','1','0',2,2,'2016-08-24 04:39:47','2016-08-24 06:06:58','1'),(23,15,27,12,'Di Carne','','5','','','Alle Rindfleischgerichte werden mit Pommes frites oder Reis, einer Sauce nach Wahl und einem Beilagensalat serviert.','5','1','0','0','0',2,2,'2016-08-24 06:42:51','2016-08-24 06:54:33','1'),(24,5,5,1,'Chees Paratha','Chees Paratha','CP001','Chees Paratha','1472043151440.png','Chees Paratha','01','0','1','1','0',2,4,'2016-08-24 18:22:31','2016-09-05 12:26:31','1'),(25,17,24,13,'Rajma Handi','Rajma Handi','RC001','Rajma Handi','1472108920640.jpg','Rajma Handi','002','0','1','1','0',2,16,'2016-08-25 12:38:40','2016-08-30 16:35:14','1');
/*!40000 ALTER TABLE `lie_rest_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_mobile_maps`
--

DROP TABLE IF EXISTS `lie_rest_mobile_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_mobile_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_primary` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_mobile_maps`
--

LOCK TABLES `lie_rest_mobile_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_mobile_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_mobile_maps` VALUES (11,3,'9988776655','Reception','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(15,4,'9899899899','Reception','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(20,6,'98989898998','test','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2'),(29,10,'9999900000','Arun ','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(30,8,'912345678900','Arun','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(37,11,'989898989898','Arun','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(40,9,'08860280996','BS','1',0,7,'0000-00-00 00:00:00','2016-08-11 12:55:50','1'),(41,12,'8000987632','Arun','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(42,13,'9089786756','Arun kumar','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(44,14,'9876543211','Rajlakshmi','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(54,2,'9654123654','rajesh','0',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(55,2,'9547632541','rakesh','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(63,17,'8860280996','Amit Dher','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(64,15,'0664497508','Blaumann Heinz','1',0,15,'0000-00-00 00:00:00','2016-09-07 22:12:06','1'),(70,1,'9876543211','Vikas','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(74,5,'9711366588','Bindu','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(86,18,'5464645656','fhgfhfhg','1',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1');
/*!40000 ALTER TABLE `lie_rest_mobile_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_new_cust_bonus_logs`
--

DROP TABLE IF EXISTS `lie_rest_new_cust_bonus_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_new_cust_bonus_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `is_new_cust_bonus` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `type` enum('S','A') NOT NULL DEFAULT 'S' COMMENT 'S for superadmin, A for admin',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_new_cust_bonus_logs`
--

LOCK TABLES `lie_rest_new_cust_bonus_logs` WRITE;
/*!40000 ALTER TABLE `lie_rest_new_cust_bonus_logs` DISABLE KEYS */;
INSERT INTO `lie_rest_new_cust_bonus_logs` VALUES (1,1,'0','S',2,2,'2016-07-20 12:30:12','2016-07-20 12:30:12','1'),(2,1,'1','S',2,2,'2016-07-20 12:30:23','2016-07-20 12:30:23','1'),(3,1,'1','S',2,2,'2016-07-20 14:31:30','2016-07-20 14:31:30','1'),(4,1,'1','S',2,2,'2016-07-20 18:13:41','2016-07-20 18:13:41','1'),(5,5,'1','S',2,2,'2016-07-25 18:24:50','2016-07-25 18:24:50','1'),(6,6,'1','S',2,2,'2016-07-27 12:45:55','2016-07-27 12:45:55','1'),(7,15,'1','S',2,2,'2016-08-12 06:13:19','2016-08-12 06:13:19','1'),(8,15,'0','S',2,2,'2016-08-12 06:13:40','2016-08-12 06:13:40','1'),(9,5,'1','S',2,2,'2016-08-12 17:38:43','2016-08-12 17:38:43','1'),(10,15,'1','S',2,2,'2016-08-26 05:13:13','2016-08-26 05:13:13','1'),(11,15,'1','S',2,2,'2016-09-06 07:11:12','2016-09-06 07:11:12','1'),(12,5,'1','S',2,2,'2016-09-06 12:10:16','2016-09-06 12:10:16','1'),(13,15,'1','S',2,2,'2016-09-15 06:10:14','2016-09-15 06:10:14','1');
/*!40000 ALTER TABLE `lie_rest_new_cust_bonus_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_notification_maps`
--

DROP TABLE IF EXISTS `lie_rest_notification_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_notification_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_notification_maps`
--

LOCK TABLES `lie_rest_notification_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_notification_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_notification_maps` VALUES (1,1,1,2,0,'2016-07-19 17:02:38','2016-07-19 17:02:38','2'),(2,1,1,2,0,'2016-07-19 17:02:52','2016-07-19 17:02:52','2'),(3,1,1,2,0,'2016-07-19 17:03:11','2016-07-19 17:03:11','2'),(4,1,1,2,0,'2016-07-19 17:06:58','2016-07-19 17:06:58','2'),(5,1,1,2,0,'2016-07-19 17:07:27','2016-07-19 17:07:27','2'),(6,1,1,2,0,'2016-07-19 17:07:28','2016-07-19 17:07:28','2'),(7,1,1,2,0,'2016-07-19 17:36:53','2016-07-19 17:36:53','2'),(8,1,4,2,0,'2016-08-10 11:53:36','2016-08-10 11:53:36','2'),(9,14,4,2,0,'2016-08-10 11:55:55','2016-08-10 11:55:55','1'),(10,11,5,2,0,'2016-08-10 12:15:07','2016-08-10 12:15:07','2'),(11,10,6,2,0,'2016-08-10 17:37:48','2016-08-10 17:37:48','1'),(12,12,6,2,0,'2016-08-10 17:37:48','2016-08-10 17:37:48','1'),(13,10,7,2,0,'2016-08-10 17:42:53','2016-08-10 17:42:53','2'),(14,12,7,2,0,'2016-08-10 17:42:53','2016-08-10 17:42:53','2'),(15,13,7,2,0,'2016-08-10 17:42:53','2016-08-10 17:42:53','2'),(16,10,7,2,0,'2016-08-10 17:43:31','2016-08-10 17:43:31','2'),(17,12,7,2,0,'2016-08-10 17:43:31','2016-08-10 17:43:31','2'),(18,13,7,2,0,'2016-08-10 17:43:31','2016-08-10 17:43:31','2'),(19,9,14,2,0,'2016-08-22 12:43:06','2016-08-22 12:43:06','2'),(20,9,14,2,0,'2016-08-22 12:44:05','2016-08-22 12:44:05','2'),(21,9,14,2,0,'2016-08-22 12:44:19','2016-08-22 12:44:19','2'),(22,2,14,2,0,'2016-08-22 12:56:45','2016-08-22 12:56:45','2'),(23,2,14,2,0,'2016-08-22 12:57:23','2016-08-22 12:57:23','2'),(24,2,14,2,0,'2016-08-22 12:58:09','2016-08-22 12:58:09','2'),(25,8,14,2,0,'2016-08-22 12:58:09','2016-08-22 12:58:09','2'),(26,2,14,2,0,'2016-08-22 13:15:49','2016-08-22 13:15:49','2'),(27,8,14,2,0,'2016-08-22 13:15:49','2016-08-22 13:15:49','2'),(28,9,14,2,0,'2016-08-22 13:15:49','2016-08-22 13:15:49','2'),(29,2,14,2,0,'2016-08-22 13:17:22','2016-08-22 13:17:22','2'),(30,8,14,2,0,'2016-08-22 13:17:22','2016-08-22 13:17:22','2'),(31,9,14,2,0,'2016-08-22 13:17:22','2016-08-22 13:17:22','2'),(32,9,14,2,0,'2016-08-22 13:29:35','2016-08-22 13:29:35','2'),(33,1,14,2,0,'2016-08-22 13:29:58','2016-08-22 13:29:58','2'),(34,9,14,2,0,'2016-08-22 13:29:58','2016-08-22 13:29:58','2'),(35,1,12,2,0,'2016-08-22 13:49:29','2016-08-22 13:49:29','2'),(36,3,12,2,0,'2016-08-22 13:49:29','2016-08-22 13:49:29','2'),(37,1,12,2,0,'2016-08-22 13:49:38','2016-08-22 13:49:38','1'),(38,1,14,2,0,'2016-08-22 13:51:27','2016-08-22 13:51:27','2'),(39,1,14,2,0,'2016-08-22 13:51:53','2016-08-22 13:51:53','2'),(40,3,14,2,0,'2016-08-22 13:51:53','2016-08-22 13:51:53','2'),(41,9,14,2,0,'2016-08-22 13:51:53','2016-08-22 13:51:53','2'),(42,1,14,2,0,'2016-08-22 13:52:01','2016-08-22 13:52:01','2'),(43,9,14,2,0,'2016-08-22 13:52:01','2016-08-22 13:52:01','2'),(44,9,14,2,0,'2016-08-22 13:52:30','2016-08-22 13:52:30','2'),(45,2,14,2,0,'2016-08-22 13:52:42','2016-08-22 13:52:42','2'),(46,2,14,2,0,'2016-08-22 13:55:36','2016-08-22 13:55:36','2'),(47,4,14,2,0,'2016-08-22 13:55:37','2016-08-22 13:55:37','2'),(48,2,14,2,0,'2016-08-22 13:55:47','2016-08-22 13:55:47','2'),(49,4,14,2,0,'2016-08-22 13:55:47','2016-08-22 13:55:47','2'),(50,9,14,2,0,'2016-08-22 13:55:47','2016-08-22 13:55:47','2'),(51,2,14,2,0,'2016-08-22 13:55:56','2016-08-22 13:55:56','2'),(52,4,14,2,0,'2016-08-22 13:55:56','2016-08-22 13:55:56','2'),(53,2,14,2,0,'2016-08-22 13:56:20','2016-08-22 13:56:20','2'),(54,4,14,2,0,'2016-08-22 13:56:20','2016-08-22 13:56:20','2'),(55,5,14,2,0,'2016-08-22 13:56:20','2016-08-22 13:56:20','2'),(56,9,14,2,0,'2016-08-22 13:56:20','2016-08-22 13:56:20','2'),(57,2,14,2,0,'2016-08-22 13:57:24','2016-08-22 13:57:24','1'),(58,4,14,2,0,'2016-08-22 13:57:24','2016-08-22 13:57:24','1');
/*!40000 ALTER TABLE `lie_rest_notification_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_notifications`
--

DROP TABLE IF EXISTS `lie_rest_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `send_type` enum('1','2') NOT NULL COMMENT '1 for all,2 for custom resturant',
  `is_type` enum('1','2') NOT NULL COMMENT '1 for notification,2 for sms',
  `is_read` enum('0','1') NOT NULL COMMENT '0 for unread,1 for read',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_notifications`
--

LOCK TABLES `lie_rest_notifications` WRITE;
/*!40000 ALTER TABLE `lie_rest_notifications` DISABLE KEYS */;
INSERT INTO `lie_rest_notifications` VALUES (1,'Test Notification','Testing By Rajlakshmi...','1','1','1',2,2,'2016-07-19 17:02:38','2016-08-21 05:58:23','2'),(2,'ghjmö','fghbjnmkl,ö.-','1','1','1',2,0,'2016-07-22 08:05:38','2016-08-10 15:07:00','2'),(3,'Test Notification1','testing','1','1','1',2,0,'2016-07-30 17:59:58','2016-08-21 05:58:23','2'),(4,'hi','hello notification','2','1','1',2,2,'2016-08-10 11:53:36','2016-08-21 05:58:23','2'),(5,'hello','hello dominos','1','1','1',2,2,'2016-08-10 12:15:07','2016-08-21 05:58:23','2'),(6,'very good','very goods','2','1','1',2,0,'2016-08-10 17:37:48','2016-08-10 17:41:04','2'),(7,'foods','Food is any substance consumed to provide nutritional support for the body. It is usually of plant or animal origin, and contains essential nutrients, such as fats, ...\r\nFood is any substance consumed to provide nutritional support for the body. It is usually of plant or animal origin, and contains essential nutrients, such as fats, ...Food is any substance consumed to provide nutritional support for the body. It is usually of plant or animal origin, and contains essential nutrients, such as fats, ...','1','1','1',2,2,'2016-08-10 17:42:53','2016-08-21 05:58:23','2'),(8,'hi lieferzonas','Food is any substance consumed to provide nutritional support for the body. It is usually of plant or animal origin, and contains essential nutrients, such as fats, ...Food is any substance consumed to provide nutritional support for the body. It is usually of plant or animal origin, and contains essential nutrients, such as fats, ...Food is any substance consumed to provide nutritional support for the body. It is usually of plant or animal origin, and contains essential nutrients, such as fats, ...Food is any substance consumed to provide nutritional support for the body. It is usually of plant or animal origin, and contains essential nutrients, such as fats, ...','1','1','1',2,0,'2016-08-10 17:46:11','2016-08-21 05:58:23','2'),(9,'asdassd','asdasdasdsa','1','1','1',2,0,'2016-08-12 12:01:45','2016-08-21 05:58:23','2'),(10,'dasdasdasdasdasd','asdasdasdasd','1','1','1',2,0,'2016-08-12 12:01:52','2016-08-21 05:58:23','2'),(11,'abc','test','1','1','1',2,0,'2016-08-12 14:55:56','2016-08-21 05:58:23','2'),(12,'Banner Werbung','Hinterlegen Sie auf Ihrer Website den Link www.lieferzonas.at/Ihr-restaurant-name, der die Kunden direkt zur richtigen Seite führt. \r\nAlternativ können Sie den Code unten auf die Seite kopieren, dort erscheint dann: ….. Wer darauf klickt, wird automatisch zu Ihrer lieferzonas.at-Website weitergeleitet.\r\n','2','1','1',2,2,'2016-08-21 05:59:32','2016-08-22 13:49:38','1'),(13,'Ihr Restaurant als unser Tipp','Ab 2 Euro wöchentlich können Sie Ihr Restaurant auf unserer Seite hervorheben. \r\n\r\nUnter der von Ihnen gebuchten Postleitzahl sehen die Kunden dann Ihr Restaurant als erstes, versehen mit einem „Leucht“-Effekt, \r\nwas erheblich die Wahrscheinlichkeit erhöht, dass der Kunde bei Ihnen bestellt. \r\n\r\nEffekt-Buchungen nehmen wir für eine Dauer von bis zu vier Wochen an.\r\n','1','1','1',2,2,'2016-08-21 07:14:25','2016-08-21 07:17:36','1'),(14,'test notification by jitu Just for test the applic','Just for test the application Just for test the application Just for test the application','2','1','1',2,2,'2016-08-22 12:43:06','2016-08-22 13:57:24','1');
/*!40000 ALTER TABLE `lie_rest_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_owner_login_logs`
--

DROP TABLE IF EXISTS `lie_rest_owner_login_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_owner_login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `rest_owner_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_owner_login_logs`
--

LOCK TABLES `lie_rest_owner_login_logs` WRITE;
/*!40000 ALTER TABLE `lie_rest_owner_login_logs` DISABLE KEYS */;
INSERT INTO `lie_rest_owner_login_logs` VALUES (1,1,1,'L','180.151.7.42','2016-07-19 16:24:53',1,1,'2016-07-19 16:24:53','2016-07-19 16:24:53','1'),(2,1,1,'L','180.151.7.42','2016-07-19 17:06:20',1,1,'2016-07-19 17:06:20','2016-07-19 17:06:20','1'),(3,1,1,'O','80.110.92.176','2016-07-20 05:42:58',1,1,'2016-07-20 05:42:58','2016-07-20 05:42:58','1'),(4,1,1,'L','80.110.92.176','2016-07-20 05:52:36',1,1,'2016-07-20 05:52:36','2016-07-20 05:52:36','1'),(5,1,1,'O','80.110.92.176','2016-07-20 07:29:58',1,1,'2016-07-20 07:29:58','2016-07-20 07:29:58','1'),(6,1,1,'L','180.151.7.42','2016-07-20 10:20:35',1,1,'2016-07-20 10:20:35','2016-07-20 10:20:35','1'),(7,1,1,'O','180.151.7.42','2016-07-20 18:54:56',1,1,'2016-07-20 18:54:56','2016-07-20 18:54:56','1'),(8,2,2,'L','180.151.7.42','2016-07-20 18:55:10',2,2,'2016-07-20 18:55:10','2016-07-20 18:55:10','1'),(9,1,1,'O','80.110.92.176','2016-07-21 04:33:56',1,1,'2016-07-21 04:33:56','2016-07-21 04:33:56','1'),(10,2,2,'L','80.110.92.176','2016-07-21 04:34:04',2,2,'2016-07-21 04:34:04','2016-07-21 04:34:04','1'),(11,1,1,'L','180.151.7.42','2016-07-21 10:27:19',1,1,'2016-07-21 10:27:19','2016-07-21 10:27:19','1'),(12,1,1,'L','180.151.7.42','2016-07-21 11:45:15',1,1,'2016-07-21 11:45:15','2016-07-21 11:45:15','1'),(13,1,1,'O','180.151.7.42','2016-07-21 11:45:32',1,1,'2016-07-21 11:45:32','2016-07-21 11:45:32','1'),(14,2,2,'L','180.151.7.42','2016-07-21 11:46:22',2,2,'2016-07-21 11:46:22','2016-07-21 11:46:22','1'),(15,2,2,'L','180.151.7.42','2016-07-21 11:48:23',2,2,'2016-07-21 11:48:23','2016-07-21 11:48:23','1'),(16,2,2,'L','180.151.7.42','2016-07-21 11:51:17',2,2,'2016-07-21 11:51:17','2016-07-21 11:51:17','1'),(17,1,1,'L','180.151.7.42','2016-07-21 12:48:55',1,1,'2016-07-21 12:48:55','2016-07-21 12:48:55','1'),(18,2,2,'L','180.151.7.45','2016-07-21 15:11:48',2,2,'2016-07-21 15:11:48','2016-07-21 15:11:48','1'),(19,1,1,'L','180.151.7.45','2016-07-21 15:33:01',1,1,'2016-07-21 15:33:01','2016-07-21 15:33:01','1'),(20,1,1,'O','180.151.7.42','2016-07-21 15:39:12',1,1,'2016-07-21 15:39:12','2016-07-21 15:39:12','1'),(21,2,2,'L','180.151.7.42','2016-07-21 15:39:17',2,2,'2016-07-21 15:39:17','2016-07-21 15:39:17','1'),(22,1,1,'L','180.151.7.42','2016-07-21 16:02:18',1,1,'2016-07-21 16:02:18','2016-07-21 16:02:18','1'),(23,1,1,'O','180.151.7.42','2016-07-21 16:17:08',1,1,'2016-07-21 16:17:08','2016-07-21 16:17:08','1'),(24,2,2,'L','180.151.7.42','2016-07-21 16:17:24',2,2,'2016-07-21 16:17:24','2016-07-21 16:17:24','1'),(25,1,1,'L','180.151.7.42','2016-07-21 16:30:31',1,1,'2016-07-21 16:30:31','2016-07-21 16:30:31','1'),(26,1,1,'O','180.151.7.42','2016-07-21 16:38:47',1,1,'2016-07-21 16:38:47','2016-07-21 16:38:47','1'),(27,2,2,'L','180.151.7.42','2016-07-21 16:39:10',2,2,'2016-07-21 16:39:10','2016-07-21 16:39:10','1'),(28,1,1,'O','80.110.92.176','2016-07-21 17:29:54',1,1,'2016-07-21 17:29:54','2016-07-21 17:29:54','1'),(29,2,2,'L','80.110.92.176','2016-07-21 17:29:59',2,2,'2016-07-21 17:29:59','2016-07-21 17:29:59','1'),(30,2,2,'L','80.110.92.176','2016-07-22 02:42:17',2,2,'2016-07-22 02:42:17','2016-07-22 02:42:17','1'),(31,2,2,'L','180.151.7.42','2016-07-22 09:55:46',2,2,'2016-07-22 09:55:46','2016-07-22 09:55:46','1'),(32,2,2,'O','180.151.7.42','2016-07-22 09:56:02',2,2,'2016-07-22 09:56:02','2016-07-22 09:56:02','1'),(33,1,1,'L','180.151.7.42','2016-07-22 09:56:09',1,1,'2016-07-22 09:56:09','2016-07-22 09:56:09','1'),(34,2,2,'L','80.110.92.176','2016-07-22 15:36:33',2,2,'2016-07-22 15:36:33','2016-07-22 15:36:33','1'),(35,1,1,'O','80.110.92.176','2016-07-23 04:18:30',1,1,'2016-07-23 04:18:30','2016-07-23 04:18:30','1'),(36,2,2,'L','80.110.92.176','2016-07-23 04:18:35',2,2,'2016-07-23 04:18:35','2016-07-23 04:18:35','1'),(37,2,2,'O','80.110.92.176','2016-07-25 04:14:03',2,2,'2016-07-25 04:14:03','2016-07-25 04:14:03','1'),(38,1,1,'L','180.151.7.42','2016-07-25 12:21:23',1,1,'2016-07-25 12:21:23','2016-07-25 12:21:23','1'),(39,1,1,'O','180.151.7.42','2016-07-25 13:49:52',1,1,'2016-07-25 13:49:52','2016-07-25 13:49:52','1'),(40,5,4,'L','180.151.7.42','2016-07-25 15:01:35',4,4,'2016-07-25 15:01:35','2016-07-25 15:01:35','1'),(41,5,4,'O','180.151.7.42','2016-07-25 18:31:29',4,4,'2016-07-25 18:31:29','2016-07-25 18:31:29','1'),(42,2,2,'L','180.151.7.42','2016-07-25 18:44:17',2,2,'2016-07-25 18:44:17','2016-07-25 18:44:17','1'),(43,2,2,'L','80.110.92.176','2016-07-26 04:31:23',2,2,'2016-07-26 04:31:23','2016-07-26 04:31:23','1'),(44,5,4,'L','180.151.7.42','2016-07-26 10:48:50',4,4,'2016-07-26 10:48:50','2016-07-26 10:48:50','1'),(45,1,1,'L','180.151.7.42','2016-07-26 17:38:00',1,1,'2016-07-26 17:38:00','2016-07-26 17:38:00','1'),(46,2,2,'L','180.151.7.42','2016-07-26 18:03:07',2,2,'2016-07-26 18:03:07','2016-07-26 18:03:07','1'),(47,2,2,'L','182.74.164.213','2016-07-27 11:04:18',2,2,'2016-07-27 11:04:18','2016-07-27 11:04:18','1'),(48,5,4,'L','180.151.7.43','2016-07-27 11:32:07',4,4,'2016-07-27 11:32:07','2016-07-27 11:32:07','1'),(49,5,4,'O','180.151.7.43','2016-07-27 11:45:48',4,4,'2016-07-27 11:45:48','2016-07-27 11:45:48','1'),(50,6,5,'L','180.151.7.43','2016-07-27 11:46:05',5,5,'2016-07-27 11:46:05','2016-07-27 11:46:05','1'),(51,2,2,'L','80.110.92.176','2016-07-28 04:39:54',2,2,'2016-07-28 04:39:54','2016-07-28 04:39:54','1'),(52,1,1,'L','180.151.7.42','2016-07-28 09:41:12',1,1,'2016-07-28 09:41:12','2016-07-28 09:41:12','1'),(53,5,4,'L','180.151.7.42','2016-07-28 11:12:22',4,4,'2016-07-28 11:12:22','2016-07-28 11:12:22','1'),(54,1,1,'L','180.151.7.42','2016-07-28 11:35:20',1,1,'2016-07-28 11:35:20','2016-07-28 11:35:20','1'),(55,1,1,'O','180.151.7.42','2016-07-28 11:35:29',1,1,'2016-07-28 11:35:29','2016-07-28 11:35:29','1'),(56,1,1,'O','180.151.7.42','2016-07-28 13:38:47',1,1,'2016-07-28 13:38:47','2016-07-28 13:38:47','1'),(57,1,1,'O','180.151.7.42','2016-07-28 13:55:20',1,1,'2016-07-28 13:55:20','2016-07-28 13:55:20','1'),(58,2,2,'L','180.151.7.42','2016-07-28 13:55:26',2,2,'2016-07-28 13:55:26','2016-07-28 13:55:26','1'),(59,2,2,'O','180.151.7.42','2016-07-28 13:55:47',2,2,'2016-07-28 13:55:47','2016-07-28 13:55:47','1'),(60,1,1,'L','180.151.7.42','2016-07-28 13:55:55',1,1,'2016-07-28 13:55:55','2016-07-28 13:55:55','1'),(61,5,4,'O','180.151.7.42','2016-07-28 15:26:15',4,4,'2016-07-28 15:26:15','2016-07-28 15:26:15','1'),(62,2,2,'L','180.151.7.42','2016-07-28 15:26:34',2,2,'2016-07-28 15:26:34','2016-07-28 15:26:34','1'),(63,1,1,'L','180.151.7.42','2016-07-28 15:32:27',1,1,'2016-07-28 15:32:27','2016-07-28 15:32:27','1'),(64,2,2,'O','80.110.92.176','2016-07-29 05:20:08',2,2,'2016-07-29 05:20:08','2016-07-29 05:20:08','1'),(65,2,2,'L','80.110.92.176','2016-07-29 05:20:11',2,2,'2016-07-29 05:20:11','2016-07-29 05:20:11','1'),(66,1,1,'L','180.151.7.42','2016-07-29 10:44:02',1,1,'2016-07-29 10:44:02','2016-07-29 10:44:02','1'),(67,1,1,'L','180.151.7.42','2016-07-29 11:18:11',1,1,'2016-07-29 11:18:11','2016-07-29 11:18:11','1'),(68,5,4,'L','180.151.7.42','2016-07-29 13:31:52',4,4,'2016-07-29 13:31:52','2016-07-29 13:31:52','1'),(69,1,1,'O','180.151.7.42','2016-07-29 17:06:06',1,1,'2016-07-29 17:06:06','2016-07-29 17:06:06','1'),(70,5,4,'O','180.151.7.42','2016-07-29 17:18:13',4,4,'2016-07-29 17:18:13','2016-07-29 17:18:13','1'),(71,1,1,'L','180.151.7.42','2016-07-29 17:41:54',1,1,'2016-07-29 17:41:54','2016-07-29 17:41:54','1'),(72,5,4,'L','180.151.7.42','2016-07-29 17:45:25',4,4,'2016-07-29 17:45:25','2016-07-29 17:45:25','1'),(73,1,1,'O','180.151.7.42','2016-07-29 18:26:27',1,1,'2016-07-29 18:26:27','2016-07-29 18:26:27','1'),(74,1,1,'L','180.151.7.42','2016-07-29 18:26:40',1,1,'2016-07-29 18:26:40','2016-07-29 18:26:40','1'),(75,2,2,'O','80.110.92.176','2016-07-30 05:10:18',2,2,'2016-07-30 05:10:18','2016-07-30 05:10:18','1'),(76,1,1,'L','80.110.92.176','2016-07-30 05:10:23',1,1,'2016-07-30 05:10:23','2016-07-30 05:10:23','1'),(77,1,1,'L','180.151.7.42','2016-07-30 11:45:52',1,1,'2016-07-30 11:45:52','2016-07-30 11:45:52','1'),(78,1,1,'O','180.151.7.42','2016-07-30 17:37:23',1,1,'2016-07-30 17:37:23','2016-07-30 17:37:23','1'),(79,1,1,'L','180.151.7.42','2016-07-30 17:58:11',1,1,'2016-07-30 17:58:11','2016-07-30 17:58:11','1'),(80,2,2,'O','80.110.92.176','2016-08-01 05:22:25',2,2,'2016-08-01 05:22:25','2016-08-01 05:22:25','1'),(81,2,2,'L','80.110.92.176','2016-08-01 05:22:32',2,2,'2016-08-01 05:22:32','2016-08-01 05:22:32','1'),(82,9,7,'L','180.151.7.42','2016-08-01 17:56:47',7,7,'2016-08-01 17:56:47','2016-08-01 17:56:47','1'),(83,9,7,'O','180.151.7.42','2016-08-01 18:00:37',7,7,'2016-08-01 18:00:37','2016-08-01 18:00:37','1'),(84,9,7,'L','180.151.7.42','2016-08-01 18:00:49',7,7,'2016-08-01 18:00:49','2016-08-01 18:00:49','1'),(85,9,7,'O','180.151.7.42','2016-08-01 18:01:03',7,7,'2016-08-01 18:01:03','2016-08-01 18:01:03','1'),(86,9,7,'L','180.151.7.42','2016-08-01 18:01:13',7,7,'2016-08-01 18:01:13','2016-08-01 18:01:13','1'),(87,8,6,'L','180.151.7.42','2016-08-01 18:47:13',6,6,'2016-08-01 18:47:13','2016-08-01 18:47:13','1'),(88,8,6,'O','180.151.7.42','2016-08-01 19:06:53',6,6,'2016-08-01 19:06:53','2016-08-01 19:06:53','1'),(89,8,6,'L','180.151.7.42','2016-08-01 19:07:22',6,6,'2016-08-01 19:07:22','2016-08-01 19:07:22','1'),(90,2,2,'O','178.115.130.242','2016-08-01 21:48:17',2,2,'2016-08-01 21:48:17','2016-08-01 21:48:17','1'),(91,1,1,'L','178.115.130.242','2016-08-01 21:48:30',1,1,'2016-08-01 21:48:30','2016-08-01 21:48:30','1'),(92,10,8,'L','180.151.7.42','2016-08-02 11:22:30',8,8,'2016-08-02 11:22:30','2016-08-02 11:22:30','1'),(93,10,8,'O','180.151.7.42','2016-08-02 11:24:26',8,8,'2016-08-02 11:24:26','2016-08-02 11:24:26','1'),(94,10,8,'L','180.151.7.42','2016-08-02 11:41:42',8,8,'2016-08-02 11:41:42','2016-08-02 11:41:42','1'),(95,10,8,'O','180.151.7.42','2016-08-02 11:41:52',8,8,'2016-08-02 11:41:52','2016-08-02 11:41:52','1'),(96,8,6,'L','180.151.7.42','2016-08-02 11:42:23',6,6,'2016-08-02 11:42:23','2016-08-02 11:42:23','1'),(97,1,1,'L','180.151.7.42','2016-08-02 12:36:00',1,1,'2016-08-02 12:36:00','2016-08-02 12:36:00','1'),(98,9,7,'L','180.151.7.42','2016-08-02 12:45:44',7,7,'2016-08-02 12:45:44','2016-08-02 12:45:44','1'),(99,8,6,'O','180.151.7.42','2016-08-02 16:43:17',6,6,'2016-08-02 16:43:17','2016-08-02 16:43:17','1'),(100,8,6,'L','180.151.7.42','2016-08-02 16:45:26',6,6,'2016-08-02 16:45:26','2016-08-02 16:45:26','1'),(101,8,6,'O','180.151.7.42','2016-08-02 17:00:20',6,6,'2016-08-02 17:00:20','2016-08-02 17:00:20','1'),(102,12,10,'L','180.151.7.42','2016-08-02 17:00:57',10,10,'2016-08-02 17:00:57','2016-08-02 17:00:57','1'),(103,12,10,'O','180.151.7.42','2016-08-02 17:02:36',10,10,'2016-08-02 17:02:36','2016-08-02 17:02:36','1'),(104,8,6,'L','180.151.7.42','2016-08-02 17:02:43',6,6,'2016-08-02 17:02:43','2016-08-02 17:02:43','1'),(105,8,6,'O','180.151.7.42','2016-08-02 17:02:58',6,6,'2016-08-02 17:02:58','2016-08-02 17:02:58','1'),(106,12,10,'L','180.151.7.42','2016-08-02 17:04:15',10,10,'2016-08-02 17:04:15','2016-08-02 17:04:15','1'),(107,12,10,'O','180.151.7.42','2016-08-02 17:06:14',10,10,'2016-08-02 17:06:14','2016-08-02 17:06:14','1'),(108,13,11,'L','180.151.7.42','2016-08-02 17:06:30',11,11,'2016-08-02 17:06:30','2016-08-02 17:06:30','1'),(109,1,1,'L','180.151.7.42','2016-08-03 11:08:07',1,1,'2016-08-03 11:08:07','2016-08-03 11:08:07','1'),(110,9,7,'L','180.151.7.42','2016-08-03 18:30:28',7,7,'2016-08-03 18:30:28','2016-08-03 18:30:28','1'),(111,1,1,'L','80.110.92.176','2016-08-04 05:04:18',1,1,'2016-08-04 05:04:18','2016-08-04 05:04:18','1'),(112,1,1,'O','80.110.92.176','2016-08-04 05:04:27',1,1,'2016-08-04 05:04:27','2016-08-04 05:04:27','1'),(113,5,4,'L','180.151.7.42','2016-08-08 17:08:39',4,4,'2016-08-08 17:08:39','2016-08-08 17:08:39','1'),(114,5,4,'O','180.151.7.42','2016-08-08 17:11:45',4,4,'2016-08-08 17:11:45','2016-08-08 17:11:45','1'),(115,5,4,'L','180.151.7.42','2016-08-08 17:11:55',4,4,'2016-08-08 17:11:55','2016-08-08 17:11:55','1'),(116,1,1,'L','180.151.7.42','2016-08-08 18:23:48',1,1,'2016-08-08 18:23:48','2016-08-08 18:23:48','1'),(117,1,1,'L','80.110.127.130','2016-08-09 05:09:01',1,1,'2016-08-09 05:09:01','2016-08-09 05:09:01','1'),(118,1,1,'L','180.151.7.42','2016-08-09 12:45:07',1,1,'2016-08-09 12:45:07','2016-08-09 12:45:07','1'),(119,1,1,'L','80.110.89.221','2016-08-10 04:55:25',1,1,'2016-08-10 04:55:25','2016-08-10 04:55:25','1'),(120,5,4,'L','180.151.7.42','2016-08-10 10:11:06',4,4,'2016-08-10 10:11:06','2016-08-10 10:11:06','1'),(121,1,1,'L','180.151.7.42','2016-08-10 11:55:01',1,1,'2016-08-10 11:55:01','2016-08-10 11:55:01','1'),(122,1,1,'O','180.151.7.42','2016-08-10 12:02:12',1,1,'2016-08-10 12:02:12','2016-08-10 12:02:12','1'),(123,13,13,'L','180.151.7.42','2016-08-10 12:12:42',13,13,'2016-08-10 12:12:42','2016-08-10 12:12:42','1'),(124,13,13,'O','180.151.7.42','2016-08-10 12:13:57',13,13,'2016-08-10 12:13:57','2016-08-10 12:13:57','1'),(125,13,13,'L','180.151.7.42','2016-08-10 12:15:34',13,13,'2016-08-10 12:15:34','2016-08-10 12:15:34','1'),(126,13,13,'O','180.151.7.42','2016-08-10 12:38:02',13,13,'2016-08-10 12:38:02','2016-08-10 12:38:02','1'),(127,12,10,'L','180.151.7.42','2016-08-10 12:38:40',10,10,'2016-08-10 12:38:40','2016-08-10 12:38:40','1'),(128,12,10,'O','180.151.7.42','2016-08-10 12:52:41',10,10,'2016-08-10 12:52:41','2016-08-10 12:52:41','1'),(129,8,14,'L','180.151.7.42','2016-08-10 12:52:55',14,14,'2016-08-10 12:52:55','2016-08-10 12:52:55','1'),(130,8,14,'O','180.151.7.42','2016-08-10 12:54:17',14,14,'2016-08-10 12:54:17','2016-08-10 12:54:17','1'),(131,1,1,'L','180.151.7.42','2016-08-10 14:44:34',1,1,'2016-08-10 14:44:34','2016-08-10 14:44:34','1'),(132,1,1,'O','180.151.7.42','2016-08-10 14:44:47',1,1,'2016-08-10 14:44:47','2016-08-10 14:44:47','1'),(133,8,14,'L','180.151.7.42','2016-08-10 14:45:33',14,14,'2016-08-10 14:45:33','2016-08-10 14:45:33','1'),(134,8,14,'O','180.151.7.42','2016-08-10 15:07:38',14,14,'2016-08-10 15:07:38','2016-08-10 15:07:38','1'),(135,9,7,'L','180.151.7.42','2016-08-10 15:37:53',7,7,'2016-08-10 15:37:53','2016-08-10 15:37:53','1'),(136,9,7,'O','180.151.7.42','2016-08-10 15:38:30',7,7,'2016-08-10 15:38:30','2016-08-10 15:38:30','1'),(137,1,1,'L','180.151.7.42','2016-08-10 15:39:26',1,1,'2016-08-10 15:39:26','2016-08-10 15:39:26','1'),(138,1,1,'O','180.151.7.42','2016-08-10 15:50:58',1,1,'2016-08-10 15:50:58','2016-08-10 15:50:58','1'),(139,8,14,'L','180.151.7.42','2016-08-10 15:51:15',14,14,'2016-08-10 15:51:15','2016-08-10 15:51:15','1'),(140,5,4,'L','180.151.7.42','2016-08-10 17:30:07',4,4,'2016-08-10 17:30:07','2016-08-10 17:30:07','1'),(141,12,10,'L','180.151.7.42','2016-08-10 17:31:25',10,10,'2016-08-10 17:31:25','2016-08-10 17:31:25','1'),(142,12,10,'O','180.151.7.42','2016-08-10 17:39:23',10,10,'2016-08-10 17:39:23','2016-08-10 17:39:23','1'),(143,10,12,'L','180.151.7.42','2016-08-10 17:40:00',12,12,'2016-08-10 17:40:00','2016-08-10 17:40:00','1'),(144,10,12,'O','180.151.7.42','2016-08-10 17:47:17',12,12,'2016-08-10 17:47:17','2016-08-10 17:47:17','1'),(145,1,1,'L','180.151.7.42','2016-08-10 17:47:18',1,1,'2016-08-10 17:47:18','2016-08-10 17:47:18','1'),(146,12,10,'L','180.151.7.42','2016-08-10 17:47:45',10,10,'2016-08-10 17:47:45','2016-08-10 17:47:45','1'),(147,12,10,'O','180.151.7.42','2016-08-10 17:47:54',10,10,'2016-08-10 17:47:54','2016-08-10 17:47:54','1'),(148,1,1,'L','180.151.7.42','2016-08-10 17:56:51',1,1,'2016-08-10 17:56:51','2016-08-10 17:56:51','1'),(149,12,10,'L','180.151.7.42','2016-08-10 18:09:52',10,10,'2016-08-10 18:09:52','2016-08-10 18:09:52','1'),(150,12,10,'O','180.151.7.42','2016-08-10 18:10:00',10,10,'2016-08-10 18:10:00','2016-08-10 18:10:00','1'),(151,1,1,'L','180.151.7.42','2016-08-10 18:15:29',1,1,'2016-08-10 18:15:29','2016-08-10 18:15:29','1'),(152,5,4,'L','180.151.7.42','2016-08-11 10:05:52',4,4,'2016-08-11 10:05:52','2016-08-11 10:05:52','1'),(153,1,1,'L','180.151.7.42','2016-08-11 10:11:40',1,1,'2016-08-11 10:11:40','2016-08-11 10:11:40','1'),(154,1,1,'L','180.151.7.42','2016-08-11 10:54:08',1,1,'2016-08-11 10:54:08','2016-08-11 10:54:08','1'),(155,1,1,'O','180.151.7.42','2016-08-11 10:54:25',1,1,'2016-08-11 10:54:25','2016-08-11 10:54:25','1'),(156,1,1,'L','180.151.7.42','2016-08-11 10:54:48',1,1,'2016-08-11 10:54:48','2016-08-11 10:54:48','1'),(157,1,1,'O','180.151.7.42','2016-08-11 12:16:13',1,1,'2016-08-11 12:16:13','2016-08-11 12:16:13','1'),(158,1,1,'L','180.151.7.42','2016-08-11 12:16:28',1,1,'2016-08-11 12:16:28','2016-08-11 12:16:28','1'),(159,1,1,'O','180.151.7.42','2016-08-11 12:18:44',1,1,'2016-08-11 12:18:44','2016-08-11 12:18:44','1'),(160,1,1,'L','180.151.7.42','2016-08-11 12:38:56',1,1,'2016-08-11 12:38:56','2016-08-11 12:38:56','1'),(161,1,1,'O','180.151.7.42','2016-08-11 12:41:03',1,1,'2016-08-11 12:41:03','2016-08-11 12:41:03','1'),(162,9,7,'L','180.151.7.42','2016-08-11 12:44:05',7,7,'2016-08-11 12:44:05','2016-08-11 12:44:05','1'),(163,9,7,'O','180.151.7.42','2016-08-11 14:53:28',7,7,'2016-08-11 14:53:28','2016-08-11 14:53:28','1'),(164,1,1,'L','180.151.7.42','2016-08-11 15:13:50',1,1,'2016-08-11 15:13:50','2016-08-11 15:13:50','1'),(165,1,1,'O','180.151.7.42','2016-08-11 15:13:57',1,1,'2016-08-11 15:13:57','2016-08-11 15:13:57','1'),(166,9,7,'L','180.151.7.42','2016-08-11 15:14:17',7,7,'2016-08-11 15:14:17','2016-08-11 15:14:17','1'),(167,9,7,'L','180.151.7.42','2016-08-11 17:23:03',7,7,'2016-08-11 17:23:03','2016-08-11 17:23:03','1'),(168,1,1,'L','180.151.7.42','2016-08-12 09:49:33',1,1,'2016-08-12 09:49:33','2016-08-12 09:49:33','1'),(169,5,4,'L','180.151.7.42','2016-08-12 10:05:53',4,4,'2016-08-12 10:05:53','2016-08-12 10:05:53','1'),(170,5,4,'O','180.151.7.42','2016-08-12 11:05:23',4,4,'2016-08-12 11:05:23','2016-08-12 11:05:23','1'),(171,2,2,'L','180.151.7.42','2016-08-12 11:08:00',2,2,'2016-08-12 11:08:00','2016-08-12 11:08:00','1'),(172,2,2,'O','180.151.7.42','2016-08-12 11:15:07',2,2,'2016-08-12 11:15:07','2016-08-12 11:15:07','1'),(173,5,4,'L','180.151.7.42','2016-08-12 11:15:22',4,4,'2016-08-12 11:15:22','2016-08-12 11:15:22','1'),(174,1,1,'L','180.151.7.42','2016-08-12 11:32:54',1,1,'2016-08-12 11:32:54','2016-08-12 11:32:54','1'),(175,5,4,'O','180.151.7.42','2016-08-12 12:19:06',4,4,'2016-08-12 12:19:06','2016-08-12 12:19:06','1'),(176,5,4,'L','180.151.7.42','2016-08-12 12:26:30',4,4,'2016-08-12 12:26:30','2016-08-12 12:26:30','1'),(177,1,1,'L','180.151.7.42','2016-08-12 12:33:02',1,1,'2016-08-12 12:33:02','2016-08-12 12:33:02','1'),(178,1,1,'L','180.151.7.42','2016-08-12 14:51:44',1,1,'2016-08-12 14:51:44','2016-08-12 14:51:44','1'),(179,5,4,'O','180.151.7.42','2016-08-12 15:28:48',4,4,'2016-08-12 15:28:48','2016-08-12 15:28:48','1'),(180,1,1,'L','180.151.7.42','2016-08-12 15:29:03',1,1,'2016-08-12 15:29:03','2016-08-12 15:29:03','1'),(181,15,15,'L','80.110.89.221','2016-08-13 06:48:38',15,15,'2016-08-13 06:48:38','2016-08-13 06:48:38','1'),(182,1,1,'L','180.151.7.42','2016-08-13 09:37:09',1,1,'2016-08-13 09:37:09','2016-08-13 09:37:09','1'),(183,5,4,'L','180.151.7.42','2016-08-13 09:49:58',4,4,'2016-08-13 09:49:58','2016-08-13 09:49:58','1'),(184,5,4,'L','180.151.7.42','2016-08-16 10:28:16',4,4,'2016-08-16 10:28:16','2016-08-16 10:28:16','1'),(185,5,4,'L','180.151.7.42','2016-08-16 11:10:29',4,4,'2016-08-16 11:10:29','2016-08-16 11:10:29','1'),(186,5,4,'O','180.151.7.42','2016-08-16 11:13:32',4,4,'2016-08-16 11:13:32','2016-08-16 11:13:32','1'),(187,1,1,'L','180.151.7.42','2016-08-16 11:13:39',1,1,'2016-08-16 11:13:39','2016-08-16 11:13:39','1'),(188,1,1,'O','180.151.7.42','2016-08-16 11:43:11',1,1,'2016-08-16 11:43:11','2016-08-16 11:43:11','1'),(189,5,4,'L','180.151.7.42','2016-08-16 11:43:15',4,4,'2016-08-16 11:43:15','2016-08-16 11:43:15','1'),(190,1,1,'L','180.151.7.42','2016-08-16 14:20:05',1,1,'2016-08-16 14:20:05','2016-08-16 14:20:05','1'),(191,5,4,'L','180.151.7.43','2016-08-17 12:23:22',4,4,'2016-08-17 12:23:22','2016-08-17 12:23:22','1'),(192,5,4,'L','180.151.7.43','2016-08-17 12:23:34',4,4,'2016-08-17 12:23:34','2016-08-17 12:23:34','1'),(193,5,4,'O','180.151.7.43','2016-08-17 12:23:36',4,4,'2016-08-17 12:23:36','2016-08-17 12:23:36','1'),(194,5,4,'L','180.151.7.43','2016-08-17 12:23:40',4,4,'2016-08-17 12:23:40','2016-08-17 12:23:40','1'),(195,5,4,'L','180.151.7.43','2016-08-17 12:24:08',4,4,'2016-08-17 12:24:08','2016-08-17 12:24:08','1'),(196,5,4,'O','180.151.7.43','2016-08-17 12:24:23',4,4,'2016-08-17 12:24:23','2016-08-17 12:24:23','1'),(197,5,4,'O','180.151.7.43','2016-08-17 12:24:37',4,4,'2016-08-17 12:24:37','2016-08-17 12:24:37','1'),(198,5,4,'L','180.151.7.43','2016-08-17 12:24:52',4,4,'2016-08-17 12:24:52','2016-08-17 12:24:52','1'),(199,1,1,'L','180.151.7.43','2016-08-17 17:36:27',1,1,'2016-08-17 17:36:27','2016-08-17 17:36:27','1'),(200,1,1,'O','180.151.7.43','2016-08-17 17:55:57',1,1,'2016-08-17 17:55:57','2016-08-17 17:55:57','1'),(201,9,7,'L','180.151.7.43','2016-08-17 17:57:07',7,7,'2016-08-17 17:57:07','2016-08-17 17:57:07','1'),(202,9,7,'O','180.151.7.43','2016-08-17 18:35:44',7,7,'2016-08-17 18:35:44','2016-08-17 18:35:44','1'),(203,9,7,'L','180.151.7.43','2016-08-17 18:36:09',7,7,'2016-08-17 18:36:09','2016-08-17 18:36:09','1'),(204,9,7,'O','180.151.7.43','2016-08-17 18:38:03',7,7,'2016-08-17 18:38:03','2016-08-17 18:38:03','1'),(205,9,7,'L','180.151.7.43','2016-08-17 18:38:21',7,7,'2016-08-17 18:38:21','2016-08-17 18:38:21','1'),(206,5,4,'L','180.151.7.43','2016-08-18 11:35:11',4,4,'2016-08-18 11:35:11','2016-08-18 11:35:11','1'),(207,1,1,'L','80.110.89.221','2016-08-19 04:39:41',1,1,'2016-08-19 04:39:41','2016-08-19 04:39:41','1'),(208,1,1,'L','180.151.7.43','2016-08-19 10:30:32',1,1,'2016-08-19 10:30:32','2016-08-19 10:30:32','1'),(209,1,1,'O','180.151.7.43','2016-08-19 12:31:21',1,1,'2016-08-19 12:31:21','2016-08-19 12:31:21','1'),(210,5,4,'L','180.151.7.43','2016-08-19 17:24:47',4,4,'2016-08-19 17:24:47','2016-08-19 17:24:47','1'),(211,1,1,'L','89.144.194.144','2016-08-21 05:27:38',1,1,'2016-08-21 05:27:38','2016-08-21 05:27:38','1'),(212,1,1,'L','180.151.7.43','2016-08-22 12:21:16',1,1,'2016-08-22 12:21:16','2016-08-22 12:21:16','1'),(213,1,1,'O','180.151.7.43','2016-08-22 12:21:19',1,1,'2016-08-22 12:21:19','2016-08-22 12:21:19','1'),(214,9,7,'L','180.151.7.43','2016-08-22 12:56:11',7,7,'2016-08-22 12:56:11','2016-08-22 12:56:11','1'),(215,9,7,'O','180.151.7.43','2016-08-22 16:45:07',7,7,'2016-08-22 16:45:07','2016-08-22 16:45:07','1'),(216,17,16,'L','180.151.7.43','2016-08-22 16:45:22',16,16,'2016-08-22 16:45:22','2016-08-22 16:45:22','1'),(217,1,1,'L','80.110.92.92','2016-08-22 17:28:18',1,1,'2016-08-22 17:28:18','2016-08-22 17:28:18','1'),(218,5,4,'L','180.151.7.43','2016-08-23 11:04:48',4,4,'2016-08-23 11:04:48','2016-08-23 11:04:48','1'),(219,17,16,'L','180.151.7.43','2016-08-23 11:44:46',16,16,'2016-08-23 11:44:46','2016-08-23 11:44:46','1'),(220,17,16,'L','180.151.7.43','2016-08-23 12:36:45',16,16,'2016-08-23 12:36:45','2016-08-23 12:36:45','1'),(221,1,1,'L','180.151.7.43','2016-08-23 15:52:14',1,1,'2016-08-23 15:52:14','2016-08-23 15:52:14','1'),(222,1,1,'L','180.151.7.43','2016-08-23 16:05:02',1,1,'2016-08-23 16:05:02','2016-08-23 16:05:02','1'),(223,1,1,'L','180.151.7.43','2016-08-23 16:17:09',1,1,'2016-08-23 16:17:09','2016-08-23 16:17:09','1'),(224,1,1,'O','180.151.7.43','2016-08-23 16:21:14',1,1,'2016-08-23 16:21:14','2016-08-23 16:21:14','1'),(225,2,2,'L','180.151.7.43','2016-08-23 16:21:21',2,2,'2016-08-23 16:21:21','2016-08-23 16:21:21','1'),(226,2,2,'O','180.151.7.43','2016-08-23 16:31:18',2,2,'2016-08-23 16:31:18','2016-08-23 16:31:18','1'),(227,17,16,'L','180.151.7.43','2016-08-23 16:32:07',16,16,'2016-08-23 16:32:07','2016-08-23 16:32:07','1'),(228,1,1,'L','80.110.92.92','2016-08-24 00:10:30',1,1,'2016-08-24 00:10:30','2016-08-24 00:10:30','1'),(229,1,1,'L','80.110.92.92','2016-08-24 05:04:06',1,1,'2016-08-24 05:04:06','2016-08-24 05:04:06','1'),(230,1,1,'O','80.110.92.92','2016-08-24 05:04:11',1,1,'2016-08-24 05:04:11','2016-08-24 05:04:11','1'),(231,15,15,'L','80.110.92.92','2016-08-24 05:04:20',15,15,'2016-08-24 05:04:20','2016-08-24 05:04:20','1'),(232,1,1,'L','180.151.7.43','2016-08-24 12:56:05',1,1,'2016-08-24 12:56:05','2016-08-24 12:56:05','1'),(233,1,1,'L','180.151.7.43','2016-08-24 17:11:34',1,1,'2016-08-24 17:11:34','2016-08-24 17:11:34','1'),(234,1,1,'O','180.151.7.43','2016-08-24 17:13:14',1,1,'2016-08-24 17:13:14','2016-08-24 17:13:14','1'),(235,17,16,'L','180.151.7.43','2016-08-24 17:13:26',16,16,'2016-08-24 17:13:26','2016-08-24 17:13:26','1'),(236,1,1,'L','180.151.7.43','2016-08-25 10:11:44',1,1,'2016-08-25 10:11:44','2016-08-25 10:11:44','1'),(237,17,16,'L','180.151.7.43','2016-08-25 12:33:43',16,16,'2016-08-25 12:33:43','2016-08-25 12:33:43','1'),(238,15,15,'L','83.65.71.26','2016-08-25 13:34:04',15,15,'2016-08-25 13:34:04','2016-08-25 13:34:04','1'),(239,15,15,'O','83.65.71.26','2016-08-25 13:53:27',15,15,'2016-08-25 13:53:27','2016-08-25 13:53:27','1'),(240,1,1,'L','83.65.71.26','2016-08-25 13:53:32',1,1,'2016-08-25 13:53:32','2016-08-25 13:53:32','1'),(241,15,15,'L','80.110.92.92','2016-08-26 05:08:48',15,15,'2016-08-26 05:08:48','2016-08-26 05:08:48','1'),(242,1,1,'L','180.151.7.43','2016-08-26 10:56:03',1,1,'2016-08-26 10:56:03','2016-08-26 10:56:03','1'),(243,17,16,'L','180.151.7.43','2016-08-26 12:28:32',16,16,'2016-08-26 12:28:32','2016-08-26 12:28:32','1'),(244,1,1,'L','89.144.225.47','2016-08-27 02:30:31',1,1,'2016-08-27 02:30:31','2016-08-27 02:30:31','1'),(245,1,1,'O','89.144.225.47','2016-08-27 02:31:07',1,1,'2016-08-27 02:31:07','2016-08-27 02:31:07','1'),(246,1,1,'L','89.144.225.47','2016-08-27 02:31:43',1,1,'2016-08-27 02:31:43','2016-08-27 02:31:43','1'),(247,1,1,'L','180.151.7.43','2016-08-27 09:56:14',1,1,'2016-08-27 09:56:14','2016-08-27 09:56:14','1'),(248,17,16,'L','180.151.7.42','2016-08-27 11:29:57',16,16,'2016-08-27 11:29:57','2016-08-27 11:29:57','1'),(249,1,1,'L','180.151.7.42','2016-08-27 12:26:07',1,1,'2016-08-27 12:26:07','2016-08-27 12:26:07','1'),(250,1,1,'L','180.151.7.42','2016-08-27 13:56:54',1,1,'2016-08-27 13:56:54','2016-08-27 13:56:54','1'),(251,1,1,'L','180.151.7.42','2016-08-27 15:26:12',1,1,'2016-08-27 15:26:12','2016-08-27 15:26:12','1'),(252,1,1,'O','213.47.175.30','2016-08-27 17:37:15',1,1,'2016-08-27 17:37:15','2016-08-27 17:37:15','1'),(253,15,15,'L','80.110.92.92','2016-08-28 05:43:53',15,15,'2016-08-28 05:43:53','2016-08-28 05:43:53','1'),(254,1,1,'L','180.151.7.42','2016-08-29 10:16:38',1,1,'2016-08-29 10:16:38','2016-08-29 10:16:38','1'),(255,17,16,'L','180.151.7.42','2016-08-29 10:59:25',16,16,'2016-08-29 10:59:25','2016-08-29 10:59:25','1'),(256,17,16,'O','180.151.7.42','2016-08-29 11:17:04',16,16,'2016-08-29 11:17:04','2016-08-29 11:17:04','1'),(257,1,1,'L','180.151.7.42','2016-08-29 11:34:49',1,1,'2016-08-29 11:34:49','2016-08-29 11:34:49','1'),(258,17,16,'L','180.151.7.42','2016-08-29 12:57:23',16,16,'2016-08-29 12:57:23','2016-08-29 12:57:23','1'),(259,15,15,'L','80.110.92.92','2016-08-30 04:56:07',15,15,'2016-08-30 04:56:07','2016-08-30 04:56:07','1'),(260,1,1,'L','180.151.7.42','2016-08-30 10:14:54',1,1,'2016-08-30 10:14:54','2016-08-30 10:14:54','1'),(261,1,1,'L','180.151.7.42','2016-08-30 15:14:01',1,1,'2016-08-30 15:14:01','2016-08-30 15:14:01','1'),(262,1,1,'L','180.151.7.42','2016-08-30 15:58:34',1,1,'2016-08-30 15:58:34','2016-08-30 15:58:34','1'),(263,17,16,'L','180.151.7.42','2016-08-30 16:31:09',16,16,'2016-08-30 16:31:09','2016-08-30 16:31:09','1'),(264,1,1,'L','180.151.7.42','2016-08-30 18:13:01',1,1,'2016-08-30 18:13:01','2016-08-30 18:13:01','1'),(265,15,15,'L','80.110.92.92','2016-08-31 00:22:02',15,15,'2016-08-31 00:22:02','2016-08-31 00:22:02','1'),(266,15,15,'O','80.110.92.92','2016-08-31 00:28:18',15,15,'2016-08-31 00:28:18','2016-08-31 00:28:18','1'),(267,15,15,'L','80.110.92.92','2016-08-31 07:01:26',15,15,'2016-08-31 07:01:26','2016-08-31 07:01:26','1'),(268,1,1,'L','180.151.7.42','2016-08-31 09:48:39',1,1,'2016-08-31 09:48:39','2016-08-31 09:48:39','1'),(269,17,16,'L','180.151.7.42','2016-08-31 12:25:41',16,16,'2016-08-31 12:25:41','2016-08-31 12:25:41','1'),(270,15,15,'L','80.110.92.92','2016-09-01 05:19:55',15,15,'2016-09-01 05:19:55','2016-09-01 05:19:55','1'),(271,15,15,'O','80.110.92.92','2016-09-02 06:49:28',15,15,'2016-09-02 06:49:28','2016-09-02 06:49:28','1'),(272,15,15,'L','80.110.92.92','2016-09-02 09:16:41',15,15,'2016-09-02 09:16:41','2016-09-02 09:16:41','1'),(273,1,1,'L','180.151.7.42','2016-09-02 16:04:08',1,1,'2016-09-02 16:04:08','2016-09-02 16:04:08','1'),(274,1,1,'O','180.151.7.42','2016-09-02 16:21:08',1,1,'2016-09-02 16:21:08','2016-09-02 16:21:08','1'),(275,1,1,'L','180.151.7.43','2016-09-02 17:23:23',1,1,'2016-09-02 17:23:23','2016-09-02 17:23:23','1'),(276,1,1,'L','180.151.7.43','2016-09-02 17:50:07',1,1,'2016-09-02 17:50:07','2016-09-02 17:50:07','1'),(277,15,15,'L','80.110.92.92','2016-09-05 04:20:24',15,15,'2016-09-05 04:20:24','2016-09-05 04:20:24','1'),(278,5,4,'L','180.151.7.43','2016-09-05 10:58:40',4,4,'2016-09-05 10:58:40','2016-09-05 10:58:40','1'),(279,1,1,'L','180.151.7.43','2016-09-05 17:34:09',1,1,'2016-09-05 17:34:09','2016-09-05 17:34:09','1'),(280,5,4,'L','180.151.7.43','2016-09-06 10:18:52',4,4,'2016-09-06 10:18:52','2016-09-06 10:18:52','1'),(281,15,15,'L','80.110.92.92','2016-09-07 04:32:58',15,15,'2016-09-07 04:32:58','2016-09-07 04:32:58','1'),(282,15,15,'L','80.110.92.92','2016-09-07 04:32:58',15,15,'2016-09-07 04:32:58','2016-09-07 04:32:58','1'),(283,5,4,'L','180.151.7.43','2016-09-07 10:30:02',4,4,'2016-09-07 10:30:02','2016-09-07 10:30:02','1'),(284,15,15,'L','213.47.175.30','2016-09-07 22:09:58',15,15,'2016-09-07 22:09:58','2016-09-07 22:09:58','1'),(285,5,4,'L','180.151.7.43','2016-09-08 10:40:38',4,4,'2016-09-08 10:40:38','2016-09-08 10:40:38','1'),(286,1,1,'L','180.151.7.43','2016-09-08 12:49:35',1,1,'2016-09-08 12:49:35','2016-09-08 12:49:35','1'),(287,1,1,'O','180.151.7.43','2016-09-08 12:50:18',1,1,'2016-09-08 12:50:18','2016-09-08 12:50:18','1'),(288,1,1,'L','180.151.7.43','2016-09-08 17:38:48',1,1,'2016-09-08 17:38:48','2016-09-08 17:38:48','1'),(289,15,15,'L','80.110.92.92','2016-09-09 08:38:26',15,15,'2016-09-09 08:38:26','2016-09-09 08:38:26','1'),(290,15,15,'O','80.110.92.92','2016-09-09 08:38:56',15,15,'2016-09-09 08:38:56','2016-09-09 08:38:56','1'),(291,15,15,'L','80.110.92.92','2016-09-09 08:44:17',15,15,'2016-09-09 08:44:17','2016-09-09 08:44:17','1'),(292,5,4,'L','180.151.7.43','2016-09-09 10:30:39',4,4,'2016-09-09 10:30:39','2016-09-09 10:30:39','1'),(293,1,1,'L','180.151.7.43','2016-09-09 15:05:16',1,1,'2016-09-09 15:05:16','2016-09-09 15:05:16','1'),(294,1,1,'L','180.151.7.43','2016-09-09 18:15:31',1,1,'2016-09-09 18:15:31','2016-09-09 18:15:31','1'),(295,1,1,'O','180.151.7.43','2016-09-09 18:15:43',1,1,'2016-09-09 18:15:43','2016-09-09 18:15:43','1'),(296,1,1,'L','180.151.7.43','2016-09-09 18:17:14',1,1,'2016-09-09 18:17:14','2016-09-09 18:17:14','1'),(297,1,1,'L','180.151.7.43','2016-09-10 10:19:09',1,1,'2016-09-10 10:19:09','2016-09-10 10:19:09','1'),(298,17,16,'L','180.151.7.43','2016-09-10 11:01:56',16,16,'2016-09-10 11:01:56','2016-09-10 11:01:56','1'),(299,1,1,'O','180.151.7.43','2016-09-10 12:43:35',1,1,'2016-09-10 12:43:35','2016-09-10 12:43:35','1'),(300,15,15,'L','213.47.175.30','2016-09-12 12:38:35',15,15,'2016-09-12 12:38:35','2016-09-12 12:38:35','1'),(301,15,15,'L','80.110.92.92','2016-09-14 04:31:25',15,15,'2016-09-14 04:31:25','2016-09-14 04:31:25','1'),(302,15,15,'L','80.110.92.92','2016-09-15 04:54:55',15,15,'2016-09-15 04:54:55','2016-09-15 04:54:55','1'),(303,15,15,'L','80.110.92.92','2016-09-16 04:47:16',15,15,'2016-09-16 04:47:16','2016-09-16 04:47:16','1'),(304,15,15,'L','213.47.175.30','2016-09-17 03:45:18',15,15,'2016-09-17 03:45:18','2016-09-17 03:45:18','1'),(305,15,15,'L','213.47.175.30','2016-09-18 01:28:30',15,15,'2016-09-18 01:28:30','2016-09-18 01:28:30','1'),(306,15,15,'O','213.47.175.30','2016-09-18 03:07:26',15,15,'2016-09-18 03:07:26','2016-09-18 03:07:26','1'),(307,15,15,'L','213.47.175.30','2016-09-18 03:07:30',15,15,'2016-09-18 03:07:30','2016-09-18 03:07:30','1'),(308,1,1,'L','180.151.7.43','2016-09-19 11:35:19',1,1,'2016-09-19 11:35:19','2016-09-19 11:35:19','1'),(309,1,1,'O','180.151.7.43','2016-09-19 11:35:28',1,1,'2016-09-19 11:35:28','2016-09-19 11:35:28','1'),(310,1,1,'O','180.151.7.43','2016-09-19 11:37:13',1,1,'2016-09-19 11:37:13','2016-09-19 11:37:13','1'),(311,15,15,'L','213.47.175.30','2016-09-19 23:40:42',15,15,'2016-09-19 23:40:42','2016-09-19 23:40:42','1'),(312,15,15,'L','178.212.111.36','2016-09-21 03:26:11',15,15,'2016-09-21 03:26:11','2016-09-21 03:26:11','1'),(313,1,1,'L','80.110.92.92','2016-09-21 03:26:41',1,1,'2016-09-21 03:26:41','2016-09-21 03:26:41','1'),(314,1,1,'O','80.110.92.92','2016-09-21 03:26:53',1,1,'2016-09-21 03:26:53','2016-09-21 03:26:53','1'),(315,15,15,'L','80.110.92.92','2016-09-21 03:26:57',15,15,'2016-09-21 03:26:57','2016-09-21 03:26:57','1'),(316,15,15,'L','195.12.59.17','2016-09-21 03:44:23',15,15,'2016-09-21 03:44:23','2016-09-21 03:44:23','1'),(317,15,15,'L','80.110.92.92','2016-09-24 05:49:24',15,15,'2016-09-24 05:49:24','2016-09-24 05:49:24','1'),(318,15,15,'L','80.110.92.92','2016-09-26 06:53:11',15,15,'2016-09-26 06:53:11','2016-09-26 06:53:11','1'),(319,15,15,'L','80.110.92.92','2016-09-29 02:28:18',15,15,'2016-09-29 02:28:18','2016-09-29 02:28:18','1'),(320,15,15,'L','213.47.175.30','2016-10-04 04:32:25',15,15,'2016-10-04 04:32:25','2016-10-04 04:32:25','1'),(321,15,15,'L','::1','2016-11-20 21:27:33',15,15,'2016-11-20 21:27:33','2016-11-20 21:27:33','1'),(322,15,15,'L','::1','2016-11-26 01:48:16',15,15,'2016-11-26 01:48:16','2016-11-26 01:48:16','1'),(323,15,15,'L','::1','2016-11-28 19:17:37',15,15,'2016-11-28 19:17:37','2016-11-28 19:17:37','1'),(324,15,15,'L','::1','2016-11-28 23:51:01',15,15,'2016-11-28 23:51:01','2016-11-28 23:51:01','1'),(325,15,15,'L','::1','2016-12-15 16:29:11',15,15,'2016-12-15 16:29:11','2016-12-15 16:29:11','1'),(326,15,15,'L','::1','2016-12-31 15:12:44',15,15,'2016-12-31 15:12:44','2016-12-31 15:12:44','1');
/*!40000 ALTER TABLE `lie_rest_owner_login_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_owners`
--

DROP TABLE IF EXISTS `lie_rest_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_owners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `contact_no` varchar(12) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(50) NOT NULL DEFAULT '0',
  `activation_key` varchar(150) NOT NULL,
  `validity_type` enum('T','C') NOT NULL COMMENT 'T for table,C for calendar',
  `validity_table` int(11) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_owners`
--

LOCK TABLES `lie_rest_owners` WRITE;
/*!40000 ALTER TABLE `lie_rest_owners` DISABLE KEYS */;
INSERT INTO `lie_rest_owners` VALUES (1,1,'Vikas','Batra','9876543211','batra@gmail.com','$2y$10$u4/UL3m8XqTYQGzm.BB2AO/7H946qnmHBQ6AVJ0q/eMPqpvrZCW5G','CV6CzM0S4eeDAWXt5dycmr7f4pqyPU87ZZdrFlALPwGtGlxTqw','dmlrYXMuYWxpdmVuZXRzb2x1dGlvbnNAZ21haWwuY29t91','T',1,'2016-08-12 08:43:29','2017-10-16 00:00:00',2,2,'2016-07-19 16:24:38','2016-09-21 03:26:53','1'),(2,2,'Kishan','Kumar','9578664867','kishan@gmail.com','$2y$10$LIYTcHoxXEDLx2duS8vwcu2gRQOSHZ7Cw8YL/YVyKQPG8uHiDDoUq','zxBdXFAzIo4NHPcDyFeYcWISCG4sv1Payl7W4tvggGTinwAsJc','','T',1,'2016-07-20 18:54:40','2017-09-23 00:00:00',2,0,'2016-07-20 18:54:40','2016-08-23 16:31:18','1'),(3,4,'Owner','owner','9874563210','owner@mail.com','$2y$10$ZBGCiTN2fh26BrTyGESmTeV4RAOKpoe7VtrbrdDjGBIdkhnJcNPTK','0','','T',3,'2016-08-17 07:31:54','2017-10-16 00:00:00',2,2,'2016-07-22 14:32:28','2016-08-17 07:31:54','1'),(4,5,'Bindu','Kharub','9711366589','bindukharub@gmail.com','$2y$10$5oH6JxcXUOzw4Fy0FrSQWemHb2yd330jYjsmNp96MLBF15w62udZe','K0q3kLg1oOYlbLyLmUFeLvX18yeccNpGbYNbxMD4VIav25prKR','','T',2,'2016-08-12 12:26:20','2017-08-12 00:00:00',2,2,'2016-07-25 14:01:33','2016-08-17 12:24:37','1'),(5,6,'Bindu','K','9711366589','bindu.alivenetsolution@gmail.com','$2y$10$XVG0p5k9HNle3Lc3LD6lR.Ys9mIakpdqO4LW5PGcXH4.LHDpFlPCa','0','','T',2,'2016-07-27 11:45:33','2017-07-27 00:00:00',2,2,'2016-07-27 11:42:06','2016-08-02 12:10:49','2'),(6,8,'Arun','sharma','9026484405','arun.alivenetsolution@gmail.com','$2y$10$JyqDW/7/uQfe9G925UOQ6ejx7XNKBpawmhPZFyM3ur0v4tdnBhzvq','FsB0v7BFUeTipYsoimmVw2x68jBDYCp4TMKiOHtofbYHKhBmpX','','T',2,'2016-08-01 13:03:25','2017-08-01 00:00:00',2,0,'2016-08-01 13:03:25','2016-08-02 17:02:58','2'),(7,9,'Harish','Singh','8860280997','bsjitu@gmail.com','$2y$10$duGhJYg6OHCvd78HdHhYN.GMQ0XpYYr7RUgcVREcMqU1clIt9o/Lu','cUbhBXfd0nRh4Dj0m9TbjyDncSY10tkI7PC9281F3MYzyJ3KWR','cmFqbGFrc2htaTAxN0BnbWFpbC5jb20=17','C',0,'2016-04-05 00:00:00','2016-11-30 00:00:00',2,2,'2016-08-01 17:56:10','2016-08-23 11:41:38','1'),(8,10,'KFC','KFC','9090909090','KFC@gmail.com','$2y$10$Wirfb/BuhPZC57rY8QNb6O4tbrH1tH0zIPyaZ/XDQNzYlCGRDkVP.','QvP2TEQnGQL9tkTWcqU3HHtFaycNqV6drDoHPOH1OKWOy0nA53','','C',0,'2016-08-02 00:00:00','2016-08-02 00:00:00',2,0,'2016-08-02 11:19:22','2016-08-02 11:41:52','2'),(9,10,'KFC','KFC','9090909090','KFC@gmail.com','$2y$10$Yphr9hVFlEYBi40puhOwQ.xw.t6Bmw1qaTDbRpvYe6EDvwA4zXGnu','0','','C',0,'2016-08-03 00:00:00','2016-09-04 00:00:00',2,0,'2016-08-02 11:40:56','2016-08-02 12:06:32','2'),(10,12,'elchico','elchico','9090909090','elchico@gmail.com','$2y$10$kVKYjweANEWpdLtm2YPKU.UNoxhxPwQyUiZHK5hsiDoBVNA4rHYoi','y5UGqsYHxi4YvxxUdBLsFxroQNtCS6xcUpYjx7Lql4wx4KanNH','','C',0,'2016-08-11 01:00:00','2017-08-11 00:00:00',2,2,'2016-08-02 17:00:05','2016-08-11 09:47:29','1'),(11,13,'fassoos','restaurant','7896789067','fasoos@gmail.com','$2y$10$YsFBx6EegM/W9EVqyn/jHuyjEnmGuX7fyZwzJ1HkW.XEkilQs6Cpy','0','','C',0,'2016-08-02 00:00:00','2016-08-16 00:00:00',2,0,'2016-08-02 17:06:02','2016-08-10 12:11:12','2'),(12,10,'kfc','kfc','9876543210','kfc@gmail.com','$2y$10$u4/UL3m8XqTYQGzm.BB2AO/7H946qnmHBQ6AVJ0q/eMPqpvrZCW5G','D0JJ2r2oYDCafMueprbp3LkYy0z1qm3uh414lApr81HWCMjcuh','','T',3,'2016-08-17 07:31:42','2017-10-16 00:00:00',2,2,'2016-08-10 11:59:34','2016-08-17 07:31:42','1'),(13,13,'fassos','fassos','9638527410','fassos@gmail.com','$2y$10$kevu.N.JIZ8726PlRZwuWu30/CxbZAzWHsdSxA1yx0qnjfhV73IDW','zKgYhXBrvBLuWaXOE59zpBb7VjNW0PyWIwxH60XY3KQ9MquETY','','T',3,'2016-08-17 07:31:24','2017-10-16 00:00:00',2,2,'2016-08-10 12:12:20','2016-08-17 07:31:24','1'),(14,8,'sagar','ratna','9876543210','sagarratna@gmail.com','$2y$10$h2og/ID75U.rl.No6Esq2.tDYwrDbGQvp8B5fR6qZAuTizhgDKT9y','C7FnVmQbtEyccC4mhhnSi0Ch6jfMsxbrS3dBdB1vsdmSrywDoT','','C',0,'2016-08-10 00:00:00','2016-08-17 00:00:00',2,0,'2016-08-10 12:52:23','2016-08-10 15:07:38','1'),(15,15,'Recai','Firat','06644975080','viennalife@live.at','$2y$10$wVK/rhhGXbynAWNb.jJVpOGTcVJsXiHibckXyBj5PUtK0YOnJUh86','wUQ5xBu5qrnhX2BBmjcaRLcBe7dnb59mFScIADZkZhNWmqgpsd','66','T',2,'2016-08-17 07:32:12','2017-08-17 00:00:00',2,2,'2016-08-13 06:46:34','2016-09-18 03:07:26','1'),(16,17,'Amit','dher','1254788956','tdkamit@gmail.com','$2y$10$5wtvKPCnxBk8Vv8jfgGjK.UFYRBlRd2EMHsqa2yQcy84H/zGs0X4C','zdv0XKaBe9DKuoMAK92q0AkzE6urFBC7XsX1SNuE13sF7F2UTa','','C',0,'2016-08-20 00:00:00','2016-09-30 00:00:00',2,2,'2016-08-22 16:22:04','2016-08-31 12:59:48','1');
/*!40000 ALTER TABLE `lie_rest_owners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_price_maps`
--

DROP TABLE IF EXISTS `lie_rest_price_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_price_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_price_maps`
--

LOCK TABLES `lie_rest_price_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_price_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_price_maps` VALUES (20,7,1,2,2,'2016-06-16 07:27:05','2016-06-16 07:27:05','0'),(21,7,2,2,2,'2016-06-16 07:27:05','2016-06-16 07:27:05','0'),(62,12,1,2,2,'2016-07-05 13:11:09','2016-07-05 13:11:09','0'),(63,12,2,2,2,'2016-07-05 13:11:09','2016-07-05 13:11:09','0'),(79,3,3,2,2,'2016-07-19 19:41:23','2016-07-19 19:41:23','0'),(80,3,4,2,2,'2016-07-19 19:41:23','2016-07-19 19:41:23','0'),(84,4,3,2,2,'2016-07-22 14:26:10','2016-07-22 14:26:10','0'),(85,4,4,2,2,'2016-07-22 14:26:10','2016-07-22 14:26:10','0'),(89,6,3,2,2,'2016-07-29 13:49:46','2016-07-29 13:49:46','0'),(103,10,3,2,2,'2016-08-02 10:53:45','2016-08-02 10:53:45','0'),(104,10,4,2,2,'2016-08-02 10:53:45','2016-08-02 10:53:45','0'),(105,8,3,2,2,'2016-08-02 10:56:31','2016-08-02 10:56:31','0'),(118,11,3,2,2,'2016-08-02 12:25:09','2016-08-02 12:25:09','0'),(119,11,4,2,2,'2016-08-02 12:25:09','2016-08-02 12:25:09','0'),(124,9,3,2,2,'2016-08-02 12:31:39','2016-08-02 12:31:39','0'),(125,9,4,2,2,'2016-08-02 12:31:39','2016-08-02 12:31:39','0'),(126,12,3,2,2,'2016-08-02 16:33:19','2016-08-02 16:33:19','0'),(127,12,4,2,2,'2016-08-02 16:33:19','2016-08-02 16:33:19','0'),(128,13,3,2,2,'2016-08-02 16:36:59','2016-08-02 16:36:59','0'),(129,13,4,2,2,'2016-08-02 16:36:59','2016-08-02 16:36:59','0'),(147,2,1,2,2,'2016-08-22 15:44:21','2016-08-22 15:44:21','0'),(161,17,1,2,2,'2016-08-22 17:14:24','2016-08-22 17:14:24','0'),(162,17,2,2,2,'2016-08-22 17:14:24','2016-08-22 17:14:24','0'),(163,15,1,2,2,'2016-08-25 04:14:30','2016-08-25 04:14:30','0'),(164,15,2,2,2,'2016-08-25 04:14:30','2016-08-25 04:14:30','0'),(174,1,1,2,2,'2016-11-29 21:48:45','2016-11-29 21:48:45','0'),(175,1,2,2,2,'2016-11-29 21:48:45','2016-11-29 21:48:45','0'),(179,5,2,2,2,'2016-12-01 08:14:52','2016-12-01 08:14:52','0'),(202,18,1,2,2,'2016-12-02 00:29:14','2016-12-02 00:29:14','0'),(203,18,2,2,2,'2016-12-02 00:29:14','2016-12-02 00:29:14','0');
/*!40000 ALTER TABLE `lie_rest_price_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_privilege_logs`
--

DROP TABLE IF EXISTS `lie_rest_privilege_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_privilege_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL COMMENT '1=>normal,2=>featured',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_privilege_logs`
--

LOCK TABLES `lie_rest_privilege_logs` WRITE;
/*!40000 ALTER TABLE `lie_rest_privilege_logs` DISABLE KEYS */;
INSERT INTO `lie_rest_privilege_logs` VALUES (1,1,0,2,2,'2016-07-19 16:23:38','2016-07-19 16:23:38','1'),(2,2,0,2,2,'2016-07-19 17:02:03','2016-07-19 17:02:03','1'),(3,2,1,2,2,'2016-07-19 18:00:59','2016-07-19 18:00:59','1'),(4,3,0,2,2,'2016-07-19 19:41:23','2016-07-19 19:41:23','1'),(5,2,1,2,2,'2016-07-22 03:28:03','2016-07-22 03:28:03','1'),(6,2,1,2,2,'2016-07-22 03:33:49','2016-07-22 03:33:49','1'),(7,4,0,2,2,'2016-07-22 14:26:10','2016-07-22 14:26:10','1'),(8,4,2,2,2,'2016-07-22 14:56:08','2016-07-22 14:56:08','1'),(9,1,2,8,8,'2016-07-25 04:37:14','2016-07-25 04:37:14','1'),(10,5,0,2,2,'2016-07-25 13:56:40','2016-07-25 13:56:40','1'),(11,6,0,2,2,'2016-07-26 13:41:40','2016-07-26 13:41:40','1'),(12,6,1,2,2,'2016-07-27 12:37:56','2016-07-27 12:37:56','1'),(13,6,1,2,2,'2016-07-27 12:38:15','2016-07-27 12:38:15','1'),(14,2,2,2,2,'2016-07-28 04:59:57','2016-07-28 04:59:57','1'),(15,5,1,2,2,'2016-07-29 13:51:12','2016-07-29 13:51:12','1'),(16,2,2,2,2,'2016-07-30 05:08:33','2016-07-30 05:08:33','1'),(17,1,2,2,2,'2016-07-30 05:11:13','2016-07-30 05:11:13','1'),(18,1,2,2,2,'2016-07-30 05:12:20','2016-07-30 05:12:20','1'),(20,8,0,2,2,'2016-08-01 11:42:33','2016-08-01 11:42:33','1'),(21,9,0,2,2,'2016-08-01 17:49:14','2016-08-01 17:49:14','1'),(22,10,0,2,2,'2016-08-02 10:41:53','2016-08-02 10:41:53','1'),(23,11,0,2,2,'2016-08-02 11:10:26','2016-08-02 11:10:26','1'),(24,12,0,2,2,'2016-08-02 16:33:19','2016-08-02 16:33:19','1'),(25,13,0,2,2,'2016-08-02 16:36:59','2016-08-02 16:36:59','1'),(26,14,0,2,2,'2016-08-03 15:46:18','2016-08-03 15:46:18','1'),(27,1,1,2,2,'2016-08-04 08:33:10','2016-08-04 08:33:10','1'),(28,15,0,2,2,'2016-08-05 04:53:38','2016-08-05 04:53:38','1'),(29,1,1,2,2,'2016-08-10 05:12:08','2016-08-10 05:12:08','1'),(30,1,2,2,2,'2016-08-10 05:12:44','2016-08-10 05:12:44','1'),(31,1,1,2,2,'2016-08-10 05:14:02','2016-08-10 05:14:02','1'),(33,1,1,2,2,'2016-08-11 04:17:48','2016-08-11 04:17:48','1'),(34,14,1,2,2,'2016-08-11 17:19:54','2016-08-11 17:19:54','1'),(35,14,1,2,2,'2016-08-11 17:20:20','2016-08-11 17:20:20','1'),(36,15,1,2,2,'2016-08-12 07:06:30','2016-08-12 07:06:30','1'),(37,15,1,2,2,'2016-08-12 07:56:21','2016-08-12 07:56:21','1'),(38,15,1,2,2,'2016-08-13 06:53:30','2016-08-13 06:53:30','1'),(39,9,1,2,2,'2016-08-17 07:19:43','2016-08-17 07:19:43','1'),(40,15,1,2,2,'2016-08-21 07:54:34','2016-08-21 07:54:34','1'),(41,1,1,2,2,'2016-08-22 05:02:32','2016-08-22 05:02:32','1'),(42,17,0,2,2,'2016-08-22 16:05:30','2016-08-22 16:05:30','1'),(43,5,1,2,2,'2016-08-23 11:34:22','2016-08-23 11:34:22','1'),(44,15,2,2,2,'2016-08-24 03:00:26','2016-08-24 03:00:26','1'),(45,18,0,2,2,'2016-08-25 12:47:04','2016-08-25 12:47:04','1'),(46,15,2,2,2,'2016-08-28 06:07:11','2016-08-28 06:07:11','1'),(47,15,1,2,2,'2016-08-31 06:58:33','2016-08-31 06:58:33','1'),(48,15,2,8,8,'2016-09-05 05:12:42','2016-09-05 05:12:42','1'),(49,5,1,2,2,'2016-09-06 18:02:38','2016-09-06 18:02:38','1'),(50,5,1,2,2,'2016-09-09 15:49:31','2016-09-09 15:49:31','1'),(51,15,1,2,2,'2016-09-15 05:33:58','2016-09-15 05:33:58','1'),(52,15,1,2,2,'2016-09-26 04:42:57','2016-09-26 04:42:57','1');
/*!40000 ALTER TABLE `lie_rest_privilege_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_privileges`
--

DROP TABLE IF EXISTS `lie_rest_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_privileges` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactiive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_privileges`
--

LOCK TABLES `lie_rest_privileges` WRITE;
/*!40000 ALTER TABLE `lie_rest_privileges` DISABLE KEYS */;
INSERT INTO `lie_rest_privileges` VALUES (1,'normal','','sfdfsdfsdf','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(2,'featured','','dfgfdgdfg','0000-00-00 00:00:00','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1');
/*!40000 ALTER TABLE `lie_rest_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_root_cat_maps`
--

DROP TABLE IF EXISTS `lie_rest_root_cat_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_root_cat_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `root_cat_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_root_cat_maps`
--

LOCK TABLES `lie_rest_root_cat_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_root_cat_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_root_cat_maps` VALUES (1,1,1,2,2,'2016-07-19 16:34:18','2016-07-19 16:34:18','1'),(4,4,1,2,2,'2016-07-22 14:28:30','2016-07-22 14:28:30','1'),(5,4,2,2,2,'2016-07-22 14:28:30','2016-07-22 14:28:30','1'),(7,5,1,2,2,'2016-07-25 15:42:06','2016-07-25 15:42:06','1'),(8,5,2,2,2,'2016-07-25 15:42:06','2016-07-25 15:42:06','1'),(9,6,2,2,2,'2016-07-27 12:26:30','2016-07-27 12:26:30','1'),(17,9,2,2,2,'2016-08-02 13:03:14','2016-08-02 13:03:14','1'),(33,8,1,2,2,'2016-08-02 16:07:17','2016-08-02 16:07:17','1'),(34,8,2,2,2,'2016-08-02 16:07:17','2016-08-02 16:07:17','1'),(35,8,4,2,2,'2016-08-02 16:07:17','2016-08-02 16:07:17','1'),(36,8,5,2,2,'2016-08-02 16:07:17','2016-08-02 16:07:17','1'),(37,8,6,2,2,'2016-08-02 16:07:17','2016-08-02 16:07:17','1'),(38,8,8,2,2,'2016-08-02 16:07:17','2016-08-02 16:07:17','1'),(39,8,9,2,2,'2016-08-02 16:07:17','2016-08-02 16:07:17','1'),(40,8,10,2,2,'2016-08-02 16:07:17','2016-08-02 16:07:17','1'),(41,2,1,2,2,'2016-08-02 17:36:38','2016-08-02 17:36:38','1'),(42,2,2,2,2,'2016-08-02 17:36:38','2016-08-02 17:36:38','1'),(43,2,4,2,2,'2016-08-02 17:36:38','2016-08-02 17:36:38','1'),(58,10,1,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(59,10,2,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(60,10,4,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(61,10,5,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(62,10,6,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(63,10,7,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(64,10,8,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(65,10,9,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(66,10,10,2,2,'2016-08-02 17:43:10','2016-08-02 17:43:10','1'),(67,11,1,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(68,11,2,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(69,11,4,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(70,11,5,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(71,11,6,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(72,11,7,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(73,11,8,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(74,11,9,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(75,11,10,2,2,'2016-08-02 17:58:32','2016-08-02 17:58:32','1'),(76,12,1,2,2,'2016-08-02 17:58:51','2016-08-02 17:58:51','1'),(77,12,2,2,2,'2016-08-02 17:58:51','2016-08-02 17:58:51','1'),(78,12,4,2,2,'2016-08-02 17:58:51','2016-08-02 17:58:51','1'),(79,12,8,2,2,'2016-08-02 17:58:51','2016-08-02 17:58:51','1'),(80,12,9,2,2,'2016-08-02 17:58:51','2016-08-02 17:58:51','1'),(81,12,10,2,2,'2016-08-02 17:58:51','2016-08-02 17:58:51','1'),(82,13,1,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(83,13,2,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(84,13,4,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(85,13,5,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(86,13,6,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(87,13,7,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(88,13,8,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(89,13,9,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(90,13,10,2,2,'2016-08-02 18:18:25','2016-08-02 18:18:25','1'),(103,17,2,2,2,'2016-08-22 16:25:07','2016-08-22 16:25:07','1'),(124,15,1,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1'),(125,15,2,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1'),(126,15,5,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1'),(127,15,6,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1'),(128,15,7,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1'),(129,15,8,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1'),(130,15,11,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1'),(131,15,12,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1'),(132,15,13,2,2,'2016-08-24 06:40:49','2016-08-24 06:40:49','1');
/*!40000 ALTER TABLE `lie_rest_root_cat_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_search_filters`
--

DROP TABLE IF EXISTS `lie_rest_search_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_search_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `low_value` int(11) NOT NULL,
  `high_value` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_search_filters`
--

LOCK TABLES `lie_rest_search_filters` WRITE;
/*!40000 ALTER TABLE `lie_rest_search_filters` DISABLE KEYS */;
INSERT INTO `lie_rest_search_filters` VALUES (1,'Price',5,100,1,1,'2016-05-04 00:00:00','2016-05-25 10:38:25','1'),(2,'Distance',1,10,1,1,'2016-05-04 00:00:00','2016-05-04 14:16:03','1');
/*!40000 ALTER TABLE `lie_rest_search_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_search_rank_settings`
--

DROP TABLE IF EXISTS `lie_rest_search_rank_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_search_rank_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `price` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_search_rank_settings`
--

LOCK TABLES `lie_rest_search_rank_settings` WRITE;
/*!40000 ALTER TABLE `lie_rest_search_rank_settings` DISABLE KEYS */;
INSERT INTO `lie_rest_search_rank_settings` VALUES (1,1,'4.99',1,2,'2016-07-08 05:48:33','2016-09-10 18:10:39','1'),(2,2,'3.99',1,2,'2016-07-08 05:48:33','2016-09-10 18:10:39','1'),(3,3,'2.99',1,2,'2016-07-08 05:48:33','2016-09-10 18:10:39','1'),(4,4,'1.99',1,2,'2016-07-08 05:48:33','2016-09-10 18:10:39','1'),(5,5,'1.49',1,2,'2016-07-08 05:48:33','2016-09-10 18:10:39','1'),(6,6,'1.09',1,2,'2016-07-08 05:48:33','2016-09-10 18:10:39','1'),(7,7,'0.99',1,2,'2016-07-08 05:48:33','2016-09-10 18:10:39','1'),(8,8,'0.99',1,2,'2016-07-08 05:48:33','2016-09-10 18:10:39','1');
/*!40000 ALTER TABLE `lie_rest_search_rank_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_search_ranks`
--

DROP TABLE IF EXISTS `lie_rest_search_ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_search_ranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zipcode_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `price` varchar(50) NOT NULL,
  `week_start_date` date NOT NULL,
  `week_end_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_search_ranks`
--

LOCK TABLES `lie_rest_search_ranks` WRITE;
/*!40000 ALTER TABLE `lie_rest_search_ranks` DISABLE KEYS */;
INSERT INTO `lie_rest_search_ranks` VALUES (2,1,1,2,'3.99','2016-07-25','2016-07-31',1,1,'2016-07-20 18:54:34','2016-07-20 18:54:34','1'),(3,3,1,8,'079','2016-07-25','2016-07-31',1,1,'2016-07-20 18:54:34','2016-07-20 18:54:34','1'),(4,1,2,5,'1.49','2016-07-25','2016-07-31',2,2,'2016-07-21 16:30:59','2016-07-21 16:30:59','1'),(5,4,2,2,'3.99','2016-07-25','2016-07-31',2,2,'2016-07-21 16:30:59','2016-07-21 16:30:59','1'),(6,1,2,4,'1.99','2016-08-01','2016-08-07',2,2,'2016-07-27 15:13:40','2016-07-27 15:13:40','1'),(7,4,2,6,'1.09','2016-08-01','2016-08-07',2,2,'2016-07-27 15:13:40','2016-07-27 15:13:40','1'),(8,1,1,1,'4.99','2016-08-08','2016-08-14',1,1,'2016-08-01 23:15:40','2016-08-01 23:15:40','1'),(9,3,1,2,'3.99','2016-08-08','2016-08-14',1,1,'2016-08-01 23:15:40','2016-08-01 23:15:40','1'),(10,4,15,1,'4.99','2016-08-29','2016-09-04',15,15,'2016-08-25 13:52:43','2016-08-25 13:52:43','1'),(11,5,15,1,'4.99','2016-08-29','2016-09-04',15,15,'2016-08-25 13:52:43','2016-08-25 13:52:43','1'),(12,1,15,5,'1.49','2016-08-29','2016-09-04',15,15,'2016-08-25 13:52:43','2016-08-25 13:52:43','1'),(13,4,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(14,8,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(15,11,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(16,5,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(17,12,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(18,1,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(19,2,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(20,3,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(21,9,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(22,13,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(23,14,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(24,15,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(25,16,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(26,17,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(27,18,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(28,19,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(29,20,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(30,21,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(31,22,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(32,23,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(33,24,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(34,25,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(35,26,15,1,'4.99','2016-09-12','2016-09-18',15,15,'2016-09-05 04:22:46','2016-09-05 04:22:46','1'),(36,4,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(37,8,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(38,11,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(39,5,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(40,12,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(41,1,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(42,2,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(43,3,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(44,9,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(45,13,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(46,14,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(47,15,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(48,16,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(49,17,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(50,18,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(51,19,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(52,20,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(53,21,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(54,22,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(55,23,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(56,24,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(57,25,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1'),(58,26,15,1,'4.99','2016-10-03','2016-10-09',15,15,'2016-09-26 07:06:51','2016-09-26 07:06:51','1');
/*!40000 ALTER TABLE `lie_rest_search_ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_service_maps`
--

DROP TABLE IF EXISTS `lie_rest_service_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_service_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_service_maps`
--

LOCK TABLES `lie_rest_service_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_service_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_service_maps` VALUES (2,1,1,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),(3,1,2,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),(4,2,5,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),(5,2,6,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),(6,1,8,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),(7,3,8,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),(8,4,8,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),(9,4,13,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),(11,4,15,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0');
/*!40000 ALTER TABLE `lie_rest_service_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_settings`
--

DROP TABLE IF EXISTS `lie_rest_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `is_open` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>closed,1=>open',
  `is_new` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>not new,1=>new',
  `allow_admin` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1->change category image,0->not change',
  `header_color` varchar(100) NOT NULL,
  `body_color` varchar(100) NOT NULL,
  `privilege_id` int(11) NOT NULL COMMENT '1=>normal,2=>featured',
  `is_new_valid` datetime NOT NULL,
  `header_color_valid` datetime NOT NULL,
  `body_color_valid` datetime NOT NULL,
  `privilege_valid` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_settings`
--

LOCK TABLES `lie_rest_settings` WRITE;
/*!40000 ALTER TABLE `lie_rest_settings` DISABLE KEYS */;
INSERT INTO `lie_rest_settings` VALUES (1,1,'1','1','','#d3d0d4','#d3d0d4',1,'1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00',2,2,'2016-07-19 16:23:38','2016-08-22 05:02:32','1'),(2,2,'1','1','','#205162','#d41e33',2,'1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00',2,2,'2016-07-19 17:02:03','2016-07-30 05:08:33','1'),(3,3,'1','0','0','#ffffff','#ffffff',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',2,2,'2016-07-19 19:41:23','2016-07-19 19:41:23','1'),(4,4,'1','1','1','#271616','#e8e01c',2,'2016-07-29 00:00:00','2016-07-30 00:00:00','2016-07-27 00:00:00','2016-07-25 00:00:00',2,2,'2016-07-22 14:26:10','2016-07-22 14:56:08','1'),(5,5,'1','0','1','#220044','#d41e33',1,'1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00',2,2,'2016-07-25 13:56:40','2016-09-09 15:49:31','1'),(6,6,'1','1','1','#205162','#e8e01c',1,'2016-07-28 00:00:00','2016-07-28 00:00:00','2016-07-28 00:00:00','1970-01-01 00:00:00',2,2,'2016-07-26 13:41:40','2016-07-27 12:38:15','1'),(8,8,'0','0','0','#ffffff','#ffffff',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',2,14,'2016-08-01 11:42:33','2016-08-10 15:51:39','1'),(9,9,'1','1','','#d41e33','#d3d0d4',1,'1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00',2,2,'2016-08-01 17:49:14','2016-08-17 07:19:43','1'),(10,10,'1','0','0','#ffffff','#ffffff',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',2,2,'2016-08-02 10:41:53','2016-08-02 10:41:53','1'),(11,11,'1','0','0','#ffffff','#ffffff',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',2,2,'2016-08-02 11:10:26','2016-08-02 11:10:26','1'),(12,12,'1','0','0','#ffffff','#ffffff',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',2,2,'2016-08-02 16:33:19','2016-08-02 16:33:19','1'),(13,13,'1','0','0','#ffffff','#ffffff',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',2,2,'2016-08-02 16:36:59','2016-08-02 16:36:59','1'),(14,14,'1','0','1','#000000','#220044',1,'2016-08-16 00:00:00','2016-08-23 00:00:00','2016-08-25 00:00:00','2016-08-31 00:00:00',2,2,'2016-08-03 15:46:18','2016-08-11 17:20:20','1'),(15,15,'1','1','','#000000','#d41e33',1,'1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00','1970-01-01 00:00:00',2,15,'2016-08-05 04:53:38','2016-11-28 20:02:04','1'),(17,17,'0','0','0','#ffffff','#ffffff',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',2,16,'2016-08-22 16:05:30','2016-08-23 18:33:10','1'),(18,18,'1','0','0','#ffffff','#ffffff',1,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',2,2,'2016-08-25 12:47:04','2016-08-25 12:47:04','1');
/*!40000 ALTER TABLE `lie_rest_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_stamp_maps`
--

DROP TABLE IF EXISTS `lie_rest_stamp_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_stamp_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `stamp_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  `stamp_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_stamp_maps`
--

LOCK TABLES `lie_rest_stamp_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_stamp_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_stamp_maps` VALUES (1,1,'1',2,2,'2016-07-20 12:26:41','2016-08-04 08:46:53','1',1),(2,5,'1',2,2,'2016-07-25 16:02:07','2016-08-12 17:38:29','1',1),(3,6,'1',2,2,'2016-07-27 12:45:43','2016-07-27 12:45:43','1',1),(4,15,'1',2,2,'2016-08-12 06:13:00','2016-12-31 14:11:16','1',1);
/*!40000 ALTER TABLE `lie_rest_stamp_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_stamps`
--

DROP TABLE IF EXISTS `lie_rest_stamps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_stamps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stamp_count` int(11) NOT NULL DEFAULT '0',
  `value` float(16,2) NOT NULL,
  `type` enum('p','c') NOT NULL DEFAULT 'p' COMMENT 'p for percent,c for cash',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_stamps`
--

LOCK TABLES `lie_rest_stamps` WRITE;
/*!40000 ALTER TABLE `lie_rest_stamps` DISABLE KEYS */;
INSERT INTO `lie_rest_stamps` VALUES (1,10,20.00,'p',2,2,'2016-07-22 14:11:13','2016-07-22 14:11:13','1');
/*!40000 ALTER TABLE `lie_rest_stamps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_sub_menus`
--

DROP TABLE IF EXISTS `lie_rest_sub_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_sub_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `short_name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `info` varchar(255) NOT NULL,
  `code` varchar(150) NOT NULL,
  `priority` varchar(150) NOT NULL,
  `image` text NOT NULL,
  `is_alergic` enum('0','1') NOT NULL COMMENT '0=>no alergic,1=>its alergic',
  `is_spicy` enum('0','1') NOT NULL COMMENT '0=>no spicy,1=>its spicy',
  `out_of_stock` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>no,1=>yes',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_sub_menus`
--

LOCK TABLES `lie_rest_sub_menus` WRITE;
/*!40000 ALTER TABLE `lie_rest_sub_menus` DISABLE KEYS */;
INSERT INTO `lie_rest_sub_menus` VALUES (1,2,2,'Test Sbumenu','','This is description','This is a test submenu information','tstsub','5','1468931243319.jpg','1','1','0',2,2,'2016-07-19 17:57:23','2016-07-19 17:57:23','1'),(2,2,2,'Test Sbumenu 2','test','This is a description','This is another test submenu information','tstsb2','6','1468931327872.jpg','0','0','0',2,2,'2016-07-19 17:58:47','2016-07-19 17:59:16','1'),(3,1,3,'Bolognesse','wie wer','','äääää','code','2','1469578117331.jpg','1','0','0',2,1,'2016-07-20 06:19:15','2016-08-30 15:14:21','1'),(4,1,4,'Alltonno','','','erdfghjklö','','2','','0','0','0',2,2,'2016-07-20 06:37:44','2016-07-20 06:37:44','1'),(5,4,5,'Menu 1 SUbmenu','ms1','desc','information','M0012','1','1469179277313.jpg','1','1','0',2,2,'2016-07-22 14:51:17','2016-07-22 14:51:17','1'),(6,5,6,'Parantha','Parantha','onion parantha with cheeze','1','1','1','1471932181607.jpg','0','1','0',2,2,'2016-07-25 16:32:41','2016-09-07 11:55:56','1'),(7,6,8,'Bharta','bharta','test','1','BB1','1','1469603122291.jpg','0','0','0',2,2,'2016-07-27 12:35:22','2016-07-27 12:35:22','1'),(8,9,13,'Masala Dosa','Masala Dosa','Masala Dosa','Masala Dosa','002','2','','0','1','0',2,2,'2016-08-02 13:18:07','2016-08-02 13:18:07','1'),(9,9,13,'Normal Dosa','Normal Dosa','Normal Dosa','Normal Dosa','003','003','','0','0','0',2,2,'2016-08-02 13:19:13','2016-08-02 13:19:13','1'),(10,9,13,'Paneer Dosa','Paneer Dosa','Paneer Dosa','Paneer Dosa','004','004','','1','1','0',2,2,'2016-08-02 13:20:06','2016-08-02 13:20:06','1'),(11,15,19,'All Tonno','All Tonno','Tomaten, Käse, Thunfisch und Zwiebeln','Tomaten, Käse, Thunfisch und Zwiebeln','2','2','1471988903515.png','1','1','0',2,15,'2016-08-05 05:36:13','2016-09-24 05:49:38','1'),(12,15,19,'Cardinale','Pizza Cardinale','Pizza Cardinale','Tomaten, Käse, Schinken','1','1','','1','1','0',2,15,'2016-08-12 08:01:24','2016-09-15 04:55:07','1'),(13,15,20,'Pomodoro','Pasta Pomodoro','','Tomaten, Zwiebel, Knoblauch','3','3','1471995033994.jpg','1','1','0',2,15,'2016-08-13 07:27:29','2016-09-29 02:28:33','1'),(14,15,19,'Pizza Provenciale','Pizza Provenciale','Pizza Provenciale','Tomaten, Käse, Schinken, Speck, Mais, Pfefferoni','4','4','','1','1','0',2,2,'2016-08-24 04:11:42','2016-08-24 04:19:55','1'),(15,15,19,'Contandino','Contandino','Pizza Contandino','Tomaten, Käse, Speck, Zwiebel, Pfefferoni, Knoblauch','5','5','','1','1','0',2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(16,15,19,'Margarita','Margarita','Pizza Margarita','Tomaten, Käse','6','6','','1','1','0',2,2,'2016-08-24 04:16:13','2016-08-24 04:16:13','1'),(17,15,19,'Funghi','Funghi','Pizza Funghi','Tomaten, Käse, Champignons','7','7','','1','1','0',2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(18,15,19,'Pizza Cipolla','Pizza Cipolla','Pizza Cipolla','Tomaten, Käse, Zwiebeln','8','8','','1','1','1',2,15,'2016-08-24 04:23:43','2016-09-29 02:45:11','1'),(19,15,20,'Bolognesse','Pasta Bolognese','Pasta Bolognese','Tomaten, Fleischsauce, Zwiebel, Knoblauch','9','9','','1','1','1',2,15,'2016-08-24 05:20:50','2016-09-29 02:45:13','1'),(20,15,20,'alla Carbonara','Pasta Carbonara','Pasta Carbonara','m. Schinken, Speck, Ei , Knoblauch und Rahmsauce','10','10','','1','1','0',2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(21,15,22,'Pollo alla Milanese ','Pollo alla Milanese ','jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj','paniertes Hühnerbrustfilet mit Pommes frites und Salat','13','13','1473985430391.jpg','1','1','0',2,2,'2016-08-24 06:09:13','2016-09-16 05:53:50','1'),(22,15,22,'Pollo ai Funghi','Pollo ai Funghi','','Hähnchenbrustfilet in Champignon - Sahnesauce mit Pommes frites und Salat','14','14','','1','1','0',2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(23,15,22,'Pollo ai Gorgonzola','Pollo ai Gorgonzola','','Hähnchenbrustfilet in Gorgonzola - Sahnesauce mit Pommes frites und Salat','16','16','','1','1','0',2,2,'2016-08-24 06:12:21','2016-08-24 06:15:48','1'),(24,15,22,'Pollo al Pepe','Pollo al Pepe','','Hähnchenbrustfilet in Pfeffer - Sahnesauce mit Pommes frites und Salat','15','15','','1','1','0',2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(25,15,23,'Bistecca alla Griglia (200g)','','','Rumpsteak gegrillt mit Kräuterbutter','17','17','1473984725896.jpg','1','1','0',2,2,'2016-08-24 06:53:22','2016-09-16 05:42:06','1'),(26,15,23,'Filetto al Griglia','Filetto al Griglia','','Rinderfilet gegrillt mit Kräuterbutter','18','18','1473985184454.jpg','1','1','0',2,2,'2016-08-24 07:00:28','2016-09-16 05:49:44','1'),(27,5,6,'Dahi parantha','Paran','gfffffffffffffffffffg','fjk','dpp','1','','0','0','0',2,2,'2016-09-06 10:50:36','2016-09-09 15:28:10','1'),(28,5,24,'Parantha','Parantha','cheese parantha','fggjgj','ppp','1','1473228784341.jpg','0','0','1',2,4,'2016-09-06 11:13:27','2016-09-09 15:20:45','1'),(30,15,19,'Lasagne Al Forno','Lasagne','','Nudeln, Bolognese und Käse','LS001','2','','0','0','0',2,2,'2016-11-11 01:00:56','2016-11-11 01:00:56','1');
/*!40000 ALTER TABLE `lie_rest_sub_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_submenu_allergic_maps`
--

DROP TABLE IF EXISTS `lie_rest_submenu_allergic_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_submenu_allergic_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `allergic_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_submenu_allergic_maps`
--

LOCK TABLES `lie_rest_submenu_allergic_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_submenu_allergic_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_submenu_allergic_maps` VALUES (1,2,2,1,1,2,2,'2016-07-19 17:57:23','2016-07-19 17:57:23','1'),(2,2,2,1,2,2,2,'2016-07-19 17:57:23','2016-07-19 17:57:23','1'),(6,4,5,5,2,2,2,'2016-07-22 14:51:17','2016-07-22 14:51:17','1'),(7,4,5,5,3,2,2,'2016-07-22 14:51:17','2016-07-22 14:51:17','1'),(17,1,3,3,1,2,2,'2016-07-28 17:08:24','2016-07-28 17:08:24','1'),(18,1,3,3,2,2,2,'2016-07-28 17:08:24','2016-07-28 17:08:24','1'),(19,1,3,3,3,2,2,'2016-07-28 17:08:24','2016-07-28 17:08:24','1'),(20,9,13,10,1,2,2,'2016-08-02 13:20:06','2016-08-02 13:20:06','1'),(21,9,13,10,3,2,2,'2016-08-02 13:20:06','2016-08-02 13:20:06','1'),(84,15,19,11,6,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(85,15,19,11,8,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(86,15,19,11,12,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(87,15,19,11,18,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(88,15,19,11,19,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(89,15,19,12,6,2,2,'2016-08-24 04:07:03','2016-08-24 04:07:03','1'),(90,15,19,12,8,2,2,'2016-08-24 04:07:03','2016-08-24 04:07:03','1'),(91,15,19,12,12,2,2,'2016-08-24 04:07:03','2016-08-24 04:07:03','1'),(92,15,19,12,17,2,2,'2016-08-24 04:07:03','2016-08-24 04:07:03','1'),(93,15,19,12,18,2,2,'2016-08-24 04:07:03','2016-08-24 04:07:03','1'),(100,15,19,15,6,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(101,15,19,15,8,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(102,15,19,15,11,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(103,15,19,15,12,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(104,15,19,15,14,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(105,15,19,15,15,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(106,15,19,15,17,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(107,15,19,15,18,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(115,15,19,16,6,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(116,15,19,16,8,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(117,15,19,16,11,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(118,15,19,16,12,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(119,15,19,16,14,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(120,15,19,16,17,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(121,15,19,16,18,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(122,15,19,14,8,2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','1'),(123,15,19,14,12,2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','1'),(124,15,19,14,14,2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','1'),(125,15,19,14,15,2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','1'),(126,15,19,14,17,2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','1'),(127,15,19,14,18,2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','1'),(128,15,19,17,6,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(129,15,19,17,8,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(130,15,19,17,11,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(131,15,19,17,12,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(132,15,19,17,14,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(133,15,19,17,17,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(134,15,19,17,18,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(135,15,19,18,6,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(136,15,19,18,8,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(137,15,19,18,11,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(138,15,19,18,12,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(139,15,19,18,14,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(140,15,19,18,17,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(141,15,19,18,18,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(142,15,20,13,6,2,2,'2016-08-24 05:00:33','2016-08-24 05:00:33','1'),(143,15,20,13,10,2,2,'2016-08-24 05:00:33','2016-08-24 05:00:33','1'),(144,15,20,13,14,2,2,'2016-08-24 05:00:33','2016-08-24 05:00:33','1'),(145,15,20,13,18,2,2,'2016-08-24 05:00:33','2016-08-24 05:00:33','1'),(146,15,20,19,6,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(147,15,20,19,8,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(148,15,20,19,10,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(149,15,20,19,11,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(150,15,20,19,12,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(151,15,20,19,14,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(152,15,20,19,15,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(153,15,20,19,17,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(154,15,20,19,18,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(155,15,20,20,6,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(156,15,20,20,8,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(157,15,20,20,10,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(158,15,20,20,11,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(159,15,20,20,12,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(160,15,20,20,14,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(161,15,20,20,17,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(162,15,20,20,18,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(171,15,22,22,6,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(172,15,22,22,7,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(173,15,22,22,8,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(174,15,22,22,11,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(175,15,22,22,12,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(176,15,22,22,17,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(177,15,22,22,18,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(185,15,22,24,6,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(186,15,22,24,8,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(187,15,22,24,10,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(188,15,22,24,12,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(189,15,22,24,14,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(190,15,22,24,15,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(191,15,22,24,17,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(192,15,22,24,18,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(193,15,22,23,6,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(194,15,22,23,7,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(195,15,22,23,8,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(196,15,22,23,11,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(197,15,22,23,12,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(198,15,22,23,17,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(199,15,22,23,18,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(214,15,23,25,6,2,2,'2016-09-16 05:42:06','2016-09-16 05:42:06','1'),(215,15,23,25,7,2,2,'2016-09-16 05:42:06','2016-09-16 05:42:06','1'),(216,15,23,25,10,2,2,'2016-09-16 05:42:06','2016-09-16 05:42:06','1'),(217,15,23,25,14,2,2,'2016-09-16 05:42:06','2016-09-16 05:42:06','1'),(218,15,23,25,18,2,2,'2016-09-16 05:42:06','2016-09-16 05:42:06','1'),(223,15,23,26,6,2,2,'2016-09-16 05:49:44','2016-09-16 05:49:44','1'),(224,15,23,26,10,2,2,'2016-09-16 05:49:44','2016-09-16 05:49:44','1'),(225,15,23,26,14,2,2,'2016-09-16 05:49:44','2016-09-16 05:49:44','1'),(226,15,23,26,18,2,2,'2016-09-16 05:49:44','2016-09-16 05:49:44','1'),(227,15,22,21,6,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1'),(228,15,22,21,8,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1'),(229,15,22,21,11,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1'),(230,15,22,21,12,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1'),(231,15,22,21,14,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1'),(232,15,22,21,16,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1'),(233,15,22,21,17,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1'),(234,15,22,21,18,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1');
/*!40000 ALTER TABLE `lie_rest_submenu_allergic_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_submenu_spice_maps`
--

DROP TABLE IF EXISTS `lie_rest_submenu_spice_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_submenu_spice_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `spice_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_submenu_spice_maps`
--

LOCK TABLES `lie_rest_submenu_spice_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_submenu_spice_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_submenu_spice_maps` VALUES (1,2,2,1,1,2,2,'2016-07-19 17:57:23','2016-07-19 17:57:23','1'),(2,4,5,5,1,2,2,'2016-07-22 14:51:17','2016-07-22 14:51:17','1'),(3,4,5,5,2,2,2,'2016-07-22 14:51:17','2016-07-22 14:51:17','1'),(4,9,13,8,1,2,2,'2016-08-02 13:18:07','2016-08-02 13:18:07','1'),(5,9,13,8,2,2,2,'2016-08-02 13:18:07','2016-08-02 13:18:07','1'),(37,15,19,11,5,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(38,15,19,11,7,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(39,15,19,11,10,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(40,15,19,11,11,2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','1'),(41,15,19,12,5,2,2,'2016-08-24 04:07:03','2016-08-24 04:07:03','1'),(42,15,19,12,9,2,2,'2016-08-24 04:07:03','2016-08-24 04:07:03','1'),(45,15,19,15,5,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(46,15,19,15,9,2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','1'),(49,15,19,16,10,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(50,15,19,16,11,2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','1'),(51,15,19,14,5,2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','1'),(52,15,19,14,9,2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','1'),(53,15,19,17,10,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(54,15,19,17,11,2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','1'),(55,15,19,18,10,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(56,15,19,18,11,2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','1'),(57,15,20,13,4,2,2,'2016-08-24 05:00:33','2016-08-24 05:00:33','1'),(58,15,20,19,5,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(59,15,20,19,8,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(60,15,20,19,9,2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','1'),(61,15,20,20,5,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(62,15,20,20,9,2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','1'),(64,15,22,22,5,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(65,15,22,22,12,2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','1'),(68,15,22,24,4,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(69,15,22,24,5,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(70,15,22,24,12,2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','1'),(71,15,22,23,5,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(72,15,22,23,12,2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','1'),(77,5,6,6,4,2,2,'2016-09-07 11:55:56','2016-09-07 11:55:56','1'),(80,15,23,25,5,2,2,'2016-09-16 05:42:06','2016-09-16 05:42:06','1'),(81,15,23,25,8,2,2,'2016-09-16 05:42:06','2016-09-16 05:42:06','1'),(84,15,23,26,5,2,2,'2016-09-16 05:49:44','2016-09-16 05:49:44','1'),(85,15,23,26,8,2,2,'2016-09-16 05:49:44','2016-09-16 05:49:44','1'),(86,15,22,21,12,2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','1');
/*!40000 ALTER TABLE `lie_rest_submenu_spice_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_suggestions`
--

DROP TABLE IF EXISTS `lie_rest_suggestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_suggestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_service` varchar(40) NOT NULL,
  `street` varchar(60) NOT NULL,
  `houseno` varchar(40) NOT NULL,
  `zipcode` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `information` text NOT NULL,
  `is_read` enum('0','1') NOT NULL COMMENT '0 for unread,1 for read',
  `status` enum('0','1','2') NOT NULL COMMENT '0 for inacative,1 for active,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_suggestions`
--

LOCK TABLES `lie_rest_suggestions` WRITE;
/*!40000 ALTER TABLE `lie_rest_suggestions` DISABLE KEYS */;
INSERT INTO `lie_rest_suggestions` VALUES (1,'00000000000','00000000','000000000','0000000000','a@gmail.com','00000','000000000','000000000','00000000000','000000000000','1','1',0,0,'2016-07-21 15:20:11','2016-07-29 15:01:03'),(2,'jajaja','fghjkl','120','1010','info@lieferzonas.at','cgvhbnm','ghjk','2200232','1200121','tzhgjkl','1','1',0,0,'2016-07-22 08:03:06','2016-07-22 08:04:12'),(3,'Test Restaurant','Bindu','0129241586','121001','bindukharub@gmail.com','Test','Kharub','9711366589','9758522','test test','1','1',0,0,'2016-07-28 15:53:28','2016-07-28 16:00:39'),(4,'Pizzeria La mire Mare','wienerbergstrasse ','3','1100 / Wien','info@lieferzonas.at','Recai','Firat','06644975080','','Ist eine sehr gute Pizzeria','1','1',0,0,'2016-08-11 02:07:54','2016-08-11 02:39:07'),(5,'ttyyt','uyu','456','64645','rajlakshmi017@gmail.com','ututu','','565656566','45436','','0','1',0,0,'2016-08-17 16:26:03','2016-08-17 16:26:03'),(6,'ttyyt','uyu','456','64645','rajlakshmi017@gmail.com','ututu','','565656566','45436','','0','1',0,0,'2016-08-17 16:26:05','2016-08-17 16:26:05'),(7,'ewrwr','rewrwer','rere','rewrew','rajlakshmi017@gmail.com','erwer','rwer','4324345','rewrewr','err','0','1',0,0,'2016-08-17 16:30:06','2016-08-17 16:30:06'),(8,'erewre','wrwerwe','rewrewr','rewrewr','rajlakshmi017@gmail.com','wrwerewre','rwerwe','5454535','4234234','rtrt','0','1',0,0,'2016-08-17 16:41:33','2016-08-17 16:41:33'),(9,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','42432442332','4243244','','0','1',0,0,'2016-08-17 16:44:13','2016-08-17 16:44:13'),(10,'test','test','test','test','rajlakshmi017@gmail.com','test','test','4234234324','342434','test','0','1',0,0,'2016-08-17 17:04:40','2016-08-17 17:04:40'),(11,'test','test','test','test','rajlakshmi017@gmail.com','test','test','242343234','324324324','test','0','1',0,0,'2016-08-17 17:50:03','2016-08-17 17:50:03'),(12,'Take Away','ans','a-30','202135','xdscdvgfdg@tg.fy','Banti','','886028096','1020124578','','0','1',0,0,'2016-08-17 18:45:57','2016-08-17 18:45:57'),(13,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3213213321','32132132','test','0','1',0,0,'2016-08-18 11:06:11','2016-08-18 11:06:11'),(14,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3213213321','32132132','','0','1',0,0,'2016-08-18 11:06:36','2016-08-18 11:06:36'),(15,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3213213321','32132132','','0','1',0,0,'2016-08-18 11:12:57','2016-08-18 11:12:57'),(16,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3213213321','32132132','','0','1',0,0,'2016-08-18 11:14:22','2016-08-18 11:14:22'),(17,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3242342344','42343243','test','0','1',0,0,'2016-08-18 15:01:37','2016-08-18 15:01:37'),(18,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3242342344','42343243','test','0','1',0,0,'2016-08-18 15:32:46','2016-08-18 15:32:46'),(19,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3242342344','42343243','test','0','1',0,0,'2016-08-18 15:32:46','2016-08-18 15:32:46'),(20,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3242342344','42343243','test','0','1',0,0,'2016-08-18 15:32:47','2016-08-18 15:32:47'),(21,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3242342344','42343243','test','0','1',0,0,'2016-08-18 15:32:48','2016-08-18 15:32:48'),(22,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','3242342344','42343243','test','0','1',0,0,'2016-08-18 15:32:51','2016-08-18 15:32:51'),(23,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','23224323423','234234234','test','0','1',0,0,'2016-08-18 16:09:18','2016-08-18 16:09:18'),(24,'test','test','test','test','rajlakshmi.alivenetsolution@gmail.com','test','test','23224323423','234234234','','0','1',0,0,'2016-08-18 16:11:49','2016-08-18 16:11:49'),(25,'raj','raj','3423','213123','rajlakshmi017@gmail.com','raj','raj','23423424','434444','','0','1',0,0,'2016-08-22 10:48:02','2016-08-22 10:48:02'),(26,'dsfs','fdfd','4545','545','rajlakshmi017@gmail.com','fdsfdf','fdfdf','534554555','54545','fsdfd','0','1',0,0,'2016-08-22 10:48:52','2016-08-22 10:48:52'),(27,'dsfs','fdfd','4545','545','rajlakshmi017@gmail.com','fdsfdf','fdfdf','534554555','54545','fsdfd','0','1',0,0,'2016-08-22 10:49:14','2016-08-22 10:49:14'),(28,'tytuyuty','utyu','yuytu','76668','rajlakshmi017@gmail.com','uyuyt','tyuytuuy','5657567','75677','hjh','0','1',0,0,'2016-08-22 11:25:03','2016-08-22 11:25:04'),(29,'tytuyuty','utyu','yuytu','76668','rajlakshmi017@gmail.com','uyuyt','tyuytuuy','5657567','75677','hjh','0','1',0,0,'2016-08-22 11:25:05','2016-08-22 11:25:05'),(30,'tytuyuty','utyu','yuytu','76668','rajlakshmi017@gmail.com','uyuyt','tyuytuuy','5657567','75677','hjh','0','1',0,0,'2016-08-22 11:34:22','2016-08-22 11:34:22'),(31,'tytuyuty','utyu','yuytu','76668','rajlakshmi017@gmail.com','uyuyt','tyuytuuy','5657567','75677','hjh','1','1',0,0,'2016-08-22 11:35:29','2016-08-22 13:27:48'),(32,'tytuyuty','utyu','yuytu','76668','rajlakshmi017@gmail.com','uyuyt','tyuytuuy','5657567','75677','hjh','0','1',0,0,'2016-08-22 11:36:45','2016-08-22 11:36:45'),(33,'tytuyuty','utyu','yuytu','76668','rajlakshmi017@gmail.com','uyuyt','tyuytuuy','5657567','75677','hjh','0','1',0,0,'2016-08-22 11:41:17','2016-08-22 11:41:17'),(34,'test','test','test','232332','rajlakshmi017@gmail.com','test','test','3232323','23244','test','0','1',0,0,'2016-08-22 13:43:36','2016-08-22 13:43:36'),(35,'Take Away','A-81','93','202135','hospital@gmail.com','Jitendra','Jitu','+666666','','','1','1',0,0,'2016-08-22 13:54:43','2016-08-22 13:59:37'),(36,'Take Away','A-81','93','202135','hospital@gmail.com','Jitendra','Jitu','+666666','','','0','1',0,0,'2016-08-22 13:54:50','2016-08-22 13:54:50'),(37,'Take Away','A-81','93','202135','hospital@gmail.com','Jitendra','Jitu','+666666','','','1','1',0,0,'2016-08-22 13:54:52','2016-08-22 13:55:10'),(38,'test','test','test','test','rajlakshmi017@gmail.com','test','test','54654858','754657','test','0','1',0,0,'2016-08-22 16:09:24','2016-08-22 16:09:24'),(39,'test','test','test','test','rajlakshmi017@gmail.com','test','test','54654858','754657','test','1','1',0,0,'2016-08-22 16:11:14','2016-08-23 10:55:24'),(40,'ddddddddddddddddddddddddddddddddddddddd','dddddddddddddd','ddddddddddddd','877777','8787878787@rtrt.kj','ddddddddddddddddddd','','34343434343','','hggggggggg','1','1',0,0,'2016-08-23 16:38:04','2016-08-23 16:38:26'),(41,'test','23','412','121001','bindukharub@gmail.com','bindu','','97113665','','','0','1',0,0,'2016-09-05 10:50:24','2016-09-05 10:50:24'),(42,'test','23','412','121001','bindukharub@gmail.com','bindu','','97113665','','','0','1',0,0,'2016-09-05 11:49:42','2016-09-05 11:49:42'),(43,'test','23','412','121001','bindukharub@gmail.com','bindu','','97113665','','','0','1',0,0,'2016-09-05 11:49:42','2016-09-05 11:49:42'),(44,'test','23','412','121001','bindukharub1@gmail.com','bindu','','97113665','','','0','1',0,0,'2016-09-05 11:49:46','2016-09-05 11:49:46'),(45,'test','23','412','121001','bindukharub1@gmail.com','bindu','','97113665','','','0','1',0,0,'2016-09-05 11:49:51','2016-09-05 11:49:51'),(46,'test','23','412','121001','bindukharub1@gmail.com','bindu','','97113665','','','0','1',0,0,'2016-09-05 11:49:51','2016-09-05 11:49:51'),(47,'test','23','412','121001','bindukharub1@gmail.com','bindu','','97113665','','','1','1',0,0,'2016-09-05 11:49:53','2016-09-05 17:02:07'),(48,'test','test','342343','3432432434','rajlakshmi.alivenetsolution@gmail.com','test','test','43243243','324234234','hi','0','1',0,0,'2016-09-08 18:58:23','2016-09-08 18:58:23'),(49,'test','test','342343','3432432434','rajlakshmi.alivenetsolution@gmail.com','test','test','43243243','324234234','hi','0','1',0,0,'2016-09-08 19:00:49','2016-09-08 19:00:49'),(50,'wewew','wewqe','ewew','231123123','rajlakshmi.alivenetsolution@gmail.com','weewqe','wewew','3213213213','23123213','hi','0','1',0,0,'2016-09-12 13:42:44','2016-09-12 13:42:44'),(51,'gfgfgf','fgfgf','ggfgf','5435345','rajlakshmi.alivenetsolution@gmail.com','fgfgf','gfgfg','435354454','543543555','','0','1',0,0,'2016-09-12 15:12:54','2016-09-12 15:12:54'),(52,'dffdfsddg','fdgfdgdfgdf','fgfdgfg','4545','rajlakshmi.alivenetsolution@gmail.com','ggfdg','gfdgfdg','4333534','543545','gfg','0','1',0,0,'2016-09-12 16:07:58','2016-09-12 16:07:58'),(53,'test','test','test','56566','rajlakshmi.alivenetsolution@gmail.com','test','test','678898798','7675776','hi','0','1',0,0,'2016-09-12 16:11:16','2016-09-12 16:11:16'),(54,'pizzeria Castello','Strasse name','12','1210','office@lieferzonas.at','Mark','Zuckerberg','06644975080','06644975080','ist ein gutes restaurant','0','1',0,0,'2016-09-26 04:33:35','2016-09-26 04:33:35');
/*!40000 ALTER TABLE `lie_rest_suggestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_tax_maps`
--

DROP TABLE IF EXISTS `lie_rest_tax_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_tax_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `tax_setting_id` int(11) NOT NULL,
  `tax_type` enum('p','f') NOT NULL COMMENT 'p->percentage, f->fixed',
  `tax_amount` varchar(15) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_id`),
  KEY `lie_tax_setting_id` (`tax_setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_tax_maps`
--

LOCK TABLES `lie_rest_tax_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_tax_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_tax_maps` VALUES (1,1,3,'f','102',2,2,'2016-07-20 12:19:52','2016-07-20 12:22:06','1'),(2,3,1,'p','15',2,2,'2016-07-22 15:12:37','2016-07-22 15:12:37','1'),(3,6,3,'f','50',2,2,'2016-07-27 12:44:52','2016-07-27 12:44:52','1'),(4,15,1,'p','20',2,8,'2016-08-12 06:54:29','2016-09-05 05:03:03','1'),(5,5,3,'p','5',2,2,'2016-09-06 16:05:54','2016-09-06 16:05:54','1');
/*!40000 ALTER TABLE `lie_rest_tax_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_rest_zipcode_delivery_maps`
--

DROP TABLE IF EXISTS `lie_rest_zipcode_delivery_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_rest_zipcode_delivery_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zipcode_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_rest_zipcode_delivery_maps`
--

LOCK TABLES `lie_rest_zipcode_delivery_maps` WRITE;
/*!40000 ALTER TABLE `lie_rest_zipcode_delivery_maps` DISABLE KEYS */;
INSERT INTO `lie_rest_zipcode_delivery_maps` VALUES (1,1,1,1010,2,2,'2016-07-19 18:05:05','2016-07-20 12:26:22','1'),(2,3,1,1030,2,2,'2016-07-19 18:05:05','2016-07-20 12:26:22','1'),(3,1,2,1010,2,2,'2016-07-19 18:05:25','2016-07-19 18:05:25','1'),(4,4,2,1040,2,2,'2016-07-19 18:05:25','2016-07-19 18:05:25','1'),(5,1,3,1010,2,2,'2016-07-19 19:42:59','2016-07-19 19:42:59','1'),(6,2,3,1020,2,2,'2016-07-19 19:42:59','2016-07-19 19:42:59','1'),(7,3,4,1030,2,2,'2016-07-22 15:13:11','2016-07-22 15:13:11','1'),(8,4,4,1040,2,2,'2016-07-22 15:13:11','2016-07-22 15:13:11','1'),(9,1,5,1010,2,2,'2016-07-25 15:46:43','2016-07-27 12:45:15','1'),(11,1,6,1010,2,2,'2016-07-27 12:45:31','2016-07-27 12:45:31','1'),(13,1,9,1010,2,2,'2016-08-02 12:57:19','2016-09-09 12:08:33','1'),(14,2,9,1020,2,2,'2016-08-02 12:57:19','2016-09-09 12:08:33','1'),(15,4,15,1040,2,2,'2016-08-05 05:16:30','2016-08-24 02:26:51','1'),(16,8,15,1050,2,2,'2016-08-05 05:16:30','2016-08-24 02:26:51','1'),(17,11,15,1100,2,2,'2016-08-05 05:16:30','2016-08-24 02:26:51','1'),(18,5,15,1110,2,2,'2016-08-12 06:10:45','2016-08-24 02:26:51','1'),(19,12,15,1120,2,2,'2016-08-12 06:10:45','2016-08-24 02:26:51','1'),(20,1,17,1010,2,2,'2016-08-22 16:38:07','2016-08-22 16:38:07','1'),(21,1,15,1010,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(22,2,15,1020,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(23,3,15,1030,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(24,9,15,1060,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(25,13,15,1130,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(26,14,15,1140,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(27,15,15,1150,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(28,16,15,1160,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(29,17,15,1170,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(30,18,15,1180,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(31,19,15,1190,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(32,20,15,1200,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(33,21,15,1210,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(34,22,15,1220,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(35,23,15,1230,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(36,24,15,1070,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(37,25,15,1080,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(38,26,15,1090,2,2,'2016-08-24 02:26:51','2016-08-24 02:26:51','1'),(39,8,9,1050,2,2,'2016-09-09 12:05:40','2016-09-09 12:08:33','1');
/*!40000 ALTER TABLE `lie_rest_zipcode_delivery_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_root_categories`
--

DROP TABLE IF EXISTS `lie_root_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_root_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_root_categories`
--

LOCK TABLES `lie_root_categories` WRITE;
/*!40000 ALTER TABLE `lie_root_categories` DISABLE KEYS */;
INSERT INTO `lie_root_categories` VALUES (1,'Test root category','Testing by Rajlakshmi','2016-07-20 00:00:00','2016-07-21 00:00:00',1,2,2,'2016-07-19 16:33:13','2016-07-20 18:10:43','1'),(2,'Vegetables','This is a vegetables category','2016-07-29 00:00:00','2016-08-27 00:00:00',5,2,2,'2016-07-19 17:11:40','2016-07-19 17:56:31','1'),(3,'sdfsdfsfsdf','sdfsdfsdfsfsd','2016-07-20 00:00:00','2016-07-21 00:00:00',3,2,2,'2016-07-19 17:54:54','2016-07-19 17:55:31','2'),(4,'test','test test test','2016-07-29 00:00:00','2026-03-05 00:00:00',10,2,2,'2016-07-29 11:39:49','2016-09-05 11:09:02','2'),(5,'vegies','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events','2016-08-01 00:00:00','2016-08-31 00:00:00',1,2,2,'2016-08-01 15:08:26','2016-08-01 15:08:26','1'),(6,'snacks','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events.','2016-08-02 00:00:00','2016-08-20 00:00:00',5,2,2,'2016-08-02 13:53:59','2016-08-02 13:53:59','1'),(7,'chickens','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events.','2016-08-02 00:00:00','2016-08-04 00:00:00',9,2,2,'2016-08-02 13:54:41','2016-08-02 15:11:45','1'),(8,'desserts','Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.[citation needed] Definition: The pattern of development that presents a word picture of a thing, a person, a situation, or a series of events.','2016-08-02 00:00:00','2016-08-09 00:00:00',10,2,2,'2016-08-02 15:14:15','2016-08-02 15:14:57','1'),(9,'soup','Soup is a primarily liquid food, generally served warm (but may be cool or cold), that is made by combining ingredients such as meat and vegetables with stock, juice, water, or another liquid. Hot soups are additionally characterized by boiling solid ingredients in liquids in a pot until the flavors are extracted, forming a broth.\r\n\r\nTraditionally, soups are classified into two main groups: clear soups and thick soups. The established French classifications of clear soups are bouillon and consommé. Thick soups are classified depending upon the type of thickening agent used: purées are vegetable soups thickened with starch; bisques are made from puréed shellfish or vegetables thickened with cream; cream soups may be thickened with béchamel sauce; and veloutés are thickened with eggs, butter, and cream. Other ingredients commonly used to thicken soups and broths include egg,[1] rice, lentils, flour, and grains; many popular soups also include carrots and potatoes.\r\n\r\nSoups are similar to stews, and in some cases there may not be a clear distinction between the two; however, soups generally have more liquid than stews.[2]','2016-08-02 00:00:00','2016-08-09 00:00:00',12,2,2,'2016-08-02 15:55:45','2016-08-02 16:05:02','1'),(10,'gravy','Gravy is made often from the juices of meats that run naturally during cooking and often thickened with wheat flour or cornstarch for added texture. In The United States the term can refer to a wider variety of sauces. The gravy may be further colored and flavored with gravy salt (a simple mix of salt and caramel food colouring) or gravy browning (gravy salt dissolved in water) or ready-made cubes and powders can be used as a substitute for natural meat or vegetable extracts. Canned gravies are also available. Gravy is commonly served with roasts, meatloaf, rice,[1] and mashed potatoes.','2016-08-02 00:00:00','2016-08-09 00:00:00',2,2,2,'2016-08-02 15:59:07','2016-08-02 15:59:07','1'),(11,'Italienisch','Italienische Küsch','2016-08-12 00:00:00','2078-07-07 00:00:00',1,2,2,'2016-08-12 05:34:43','2016-08-12 05:34:43','1'),(12,'Di Pollo ','Di Pollo - Geflügel ','2016-08-24 00:00:00','2018-08-24 00:00:00',12,2,2,'2016-08-24 05:49:10','2016-08-24 05:49:10','1'),(13,'Di Carne','Fleisch gerichte','2016-08-24 00:00:00','2020-08-31 00:00:00',17,2,2,'2016-08-24 06:29:50','2016-08-24 06:29:50','1'),(14,'Test','Bindu Test ','2016-09-05 00:00:00','2016-09-06 00:00:00',1,2,2,'2016-09-05 11:09:31','2016-09-05 11:09:55','1'),(15,'Bindu','gvf,.djblk;./kl;','2016-09-05 00:00:00','2016-09-05 00:00:00',1,2,2,'2016-09-05 11:12:42','2016-09-05 11:12:42','1');
/*!40000 ALTER TABLE `lie_root_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_service_rest_maps`
--

DROP TABLE IF EXISTS `lie_service_rest_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_service_rest_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `service_setting_id` int(11) NOT NULL,
  `expire_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`),
  KEY `lie_service_setting_id` (`service_setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_service_rest_maps`
--

LOCK TABLES `lie_service_rest_maps` WRITE;
/*!40000 ALTER TABLE `lie_service_rest_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_service_rest_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_service_settings`
--

DROP TABLE IF EXISTS `lie_service_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_service_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_service_settings`
--

LOCK TABLES `lie_service_settings` WRITE;
/*!40000 ALTER TABLE `lie_service_settings` DISABLE KEYS */;
INSERT INTO `lie_service_settings` VALUES (1,'service','service',2,2,2016,2016,'1'),(2,'test','testets',2,2,2016,2016,'1'),(3,'asdasda','sdasdasd',2,2,2016,2016,'2');
/*!40000 ALTER TABLE `lie_service_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_set_bonus_points`
--

DROP TABLE IF EXISTS `lie_set_bonus_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_set_bonus_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_title` varchar(50) NOT NULL,
  `points` varchar(5) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_set_bonus_points`
--

LOCK TABLES `lie_set_bonus_points` WRITE;
/*!40000 ALTER TABLE `lie_set_bonus_points` DISABLE KEYS */;
INSERT INTO `lie_set_bonus_points` VALUES (1,'Bestellungsbewertung','70','test',9,2,'2016-05-10 00:00:00','2016-07-04 05:22:29','1'),(2,'test1','80','test',9,1,'2016-05-10 00:00:00','2016-05-16 07:09:37','1');
/*!40000 ALTER TABLE `lie_set_bonus_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_set_cashback_points`
--

DROP TABLE IF EXISTS `lie_set_cashback_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_set_cashback_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cashback_title` varchar(50) NOT NULL,
  `points` varchar(5) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_set_cashback_points`
--

LOCK TABLES `lie_set_cashback_points` WRITE;
/*!40000 ALTER TABLE `lie_set_cashback_points` DISABLE KEYS */;
INSERT INTO `lie_set_cashback_points` VALUES (1,'test','50','gggd sdwsadfwrewr',9,2,'2016-05-09 00:00:00','2016-07-19 16:06:52','1'),(2,'test1','60','test',9,2,'2016-05-09 00:00:00','2016-07-19 16:07:04','1');
/*!40000 ALTER TABLE `lie_set_cashback_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_categories`
--

DROP TABLE IF EXISTS `lie_shop_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_categories`
--

LOCK TABLES `lie_shop_categories` WRITE;
/*!40000 ALTER TABLE `lie_shop_categories` DISABLE KEYS */;
INSERT INTO `lie_shop_categories` VALUES (1,'Shirts','ShirtsShirts',2,2,'2016-07-19 16:40:59','2016-07-19 16:51:31','1'),(2,'Jeans','JeansJeansJeans',2,2,'2016-07-19 16:44:16','2016-07-19 16:51:16','1'),(3,'shoes','shoesss',2,2,'2016-07-19 16:44:32','2016-07-19 16:52:56','1'),(6,'cap','shop caps',2,2,'2016-07-19 16:47:05','2016-07-19 16:51:51','1'),(7,'Beverages','Soft Drinks',2,2,'2016-07-26 10:48:07','2016-07-26 10:48:23','1'),(8,'T-shirts','lieferzonas T-shirt',2,2,'2016-08-10 14:54:55','2016-08-11 12:00:56','1');
/*!40000 ALTER TABLE `lie_shop_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_inventories`
--

DROP TABLE IF EXISTS `lie_shop_inventories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_category_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `metric_id` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `price` double NOT NULL,
  `action` enum('P','M') NOT NULL DEFAULT 'P' COMMENT 'P for Item Added & M for Item Used',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_inventories`
--

LOCK TABLES `lie_shop_inventories` WRITE;
/*!40000 ALTER TABLE `lie_shop_inventories` DISABLE KEYS */;
INSERT INTO `lie_shop_inventories` VALUES (1,3,4,1,20,12000,'P',2,2,'2016-07-19 17:00:52','2016-07-19 17:00:52','1'),(2,3,5,1,10,11000,'P',2,2,'2016-07-19 17:00:52','2016-07-19 17:00:52','1'),(3,3,5,1,5,5500,'P',2,2,'2016-07-19 17:01:18','2016-07-19 17:01:18','1'),(4,6,1,1,100,6000,'P',2,2,'2016-07-19 17:02:14','2016-07-19 17:02:14','1'),(5,2,2,2,1.1,550,'P',2,2,'2016-07-19 17:02:35','2016-07-19 17:03:14','2'),(6,6,1,1,4,240,'M',1,1,'2016-07-19 17:26:08','2016-07-19 17:26:08','1'),(7,2,2,2,1,500,'M',1,1,'2016-07-19 17:26:08','2016-07-19 17:26:08','1'),(8,3,4,1,1,600,'M',1,1,'2016-07-20 18:16:07','2016-07-20 18:16:07','1'),(9,3,5,1,1,1100,'M',1,1,'2016-07-20 18:16:07','2016-07-20 18:16:07','1'),(10,3,4,1,1,600,'M',1,1,'2016-07-20 18:33:27','2016-07-20 18:33:27','1'),(11,3,4,1,1,600,'M',1,1,'2016-07-20 18:34:22','2016-07-20 18:34:22','1'),(12,3,5,1,1,1100,'M',1,1,'2016-07-20 18:37:12','2016-07-20 18:37:12','1'),(13,6,1,1,1,60,'M',1,1,'2016-07-20 18:37:12','2016-07-20 18:37:12','1'),(14,6,1,1,1,60,'M',1,1,'2016-07-21 15:43:49','2016-07-21 15:43:49','1'),(15,3,5,1,1,1100,'M',2,2,'2016-07-21 16:58:51','2016-07-21 16:58:51','1'),(16,6,1,1,2,120,'M',2,2,'2016-07-21 16:58:51','2016-07-21 16:58:51','1'),(17,3,4,1,1,600,'M',1,1,'2016-07-22 09:59:50','2016-07-22 09:59:50','1'),(18,6,1,1,1,60,'M',1,1,'2016-07-22 09:59:50','2016-07-22 09:59:50','1'),(19,2,2,2,2,1000,'P',2,2,'2016-07-23 12:06:00','2016-07-23 12:06:00','1'),(20,7,6,5,10,550,'P',2,2,'2016-07-26 11:01:15','2016-07-26 11:01:15','1'),(21,3,4,1,1,600,'M',4,4,'2016-07-26 12:06:17','2016-07-26 12:06:17','1'),(22,7,6,5,3,165,'M',4,4,'2016-07-26 12:06:17','2016-07-26 12:06:17','1'),(23,2,2,2,1,500,'M',4,4,'2016-07-26 12:13:27','2016-07-26 12:13:27','1'),(24,6,1,1,3,180,'M',4,4,'2016-07-26 12:43:56','2016-07-26 12:43:56','1'),(25,7,6,5,7,385,'M',4,4,'2016-07-26 16:09:59','2016-07-26 16:09:59','1'),(26,2,2,2,1,500,'M',4,4,'2016-07-26 16:09:59','2016-07-26 16:09:59','1'),(27,6,1,1,2,120,'M',4,4,'2016-07-26 16:09:59','2016-07-26 16:09:59','1'),(28,3,4,1,15,9000,'M',4,4,'2016-07-26 16:14:30','2016-07-26 16:14:30','1'),(29,6,1,1,1,60,'M',2,2,'2016-07-27 11:32:37','2016-07-27 11:32:37','1'),(30,3,5,1,1,1100,'M',2,2,'2016-07-27 11:32:37','2016-07-27 11:32:37','1'),(31,7,6,5,10,550,'P',2,2,'2016-07-27 11:37:39','2016-07-27 11:37:39','1'),(32,7,6,5,40,2200,'P',2,2,'2016-07-27 11:37:59','2016-07-27 11:37:59','1'),(33,7,6,5,5,275,'P',2,2,'2016-07-27 11:50:30','2016-07-27 11:50:30','1'),(34,3,4,1,5,3000,'P',2,2,'2016-07-27 11:58:16','2016-07-27 11:58:16','1'),(35,7,6,7,2,110,'P',2,2,'2016-07-27 12:01:19','2016-07-27 12:01:19','1'),(36,7,6,7,2,110,'P',2,2,'2016-07-27 12:01:55','2016-07-27 12:01:55','1'),(37,6,1,1,1,60,'M',2,2,'2016-07-27 12:08:27','2016-07-27 12:08:27','1'),(38,7,6,7,2,110,'M',2,2,'2016-07-27 12:08:27','2016-07-27 12:08:27','1'),(39,6,1,1,1,60,'M',5,5,'2016-07-27 12:16:32','2016-07-27 12:16:32','1'),(40,7,6,7,5,275,'M',5,5,'2016-07-27 12:23:27','2016-07-27 12:23:27','1'),(41,3,5,1,3,3300,'M',5,5,'2016-07-27 12:23:27','2016-07-27 12:23:27','1'),(42,6,1,1,1,60,'M',5,5,'2016-07-27 12:23:27','2016-07-27 12:23:27','1'),(43,7,6,7,1,55,'M',2,2,'2016-07-27 13:19:41','2016-07-27 13:19:41','1'),(44,6,1,1,1,60,'M',2,2,'2016-07-27 13:19:41','2016-07-27 13:19:41','1'),(45,3,5,1,1,1100,'M',2,2,'2016-07-27 13:19:41','2016-07-27 13:19:41','1'),(46,6,1,1,1,60,'M',2,2,'2016-07-27 13:40:58','2016-07-27 13:40:58','1'),(47,7,6,7,1,55,'M',2,2,'2016-07-27 13:40:58','2016-07-27 13:40:58','1'),(48,3,5,1,1,1,'M',2,2,'2016-07-27 13:40:58','2016-07-27 13:40:58','1'),(49,3,5,1,1,1,'M',2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(50,7,6,7,1,55,'M',2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(51,3,4,1,1,600,'M',2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(52,6,1,1,1,60,'M',2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(53,3,5,1,10,11000,'P',2,2,'2016-07-27 15:21:53','2016-07-27 15:21:53','1'),(54,3,4,1,2,1200,'P',2,2,'2016-07-27 15:25:13','2016-07-27 15:25:13','1'),(55,3,4,1,2,1200,'P',2,2,'2016-07-27 15:31:26','2016-07-27 15:31:26','1'),(56,2,4,1,1,600,'P',2,2,'2016-07-27 15:34:34','2016-07-27 15:34:34','1'),(57,2,4,1,1,600,'P',2,2,'2016-07-27 15:35:12','2016-07-27 15:35:12','1'),(58,2,4,1,1,600,'P',2,2,'2016-07-27 15:39:11','2016-07-27 15:39:11','1'),(59,3,2,2,1,500,'P',2,2,'2016-07-27 15:39:32','2016-07-27 15:39:32','1'),(60,6,6,7,5,275,'P',2,2,'2016-07-27 15:41:39','2016-07-27 15:41:39','1'),(61,7,4,1,10,6000,'P',2,2,'2016-07-27 15:42:12','2016-07-27 15:42:12','1'),(62,7,5,1,10,11000,'P',2,2,'2016-07-27 15:42:12','2016-07-27 15:42:12','1'),(63,3,4,1,10,6000,'P',2,2,'2016-07-27 15:43:10','2016-07-27 15:43:10','1'),(64,3,5,1,10,11000,'P',2,2,'2016-07-27 15:43:10','2016-07-27 15:43:10','1'),(65,7,4,1,5,3000,'P',2,2,'2016-07-27 15:43:34','2016-07-27 15:43:34','1'),(66,7,5,1,5,5500,'P',2,2,'2016-07-27 15:43:34','2016-07-27 15:43:34','1'),(67,3,4,1,1,600,'P',2,2,'2016-07-27 15:45:20','2016-07-27 15:45:20','1'),(68,3,5,1,2,2200,'P',2,2,'2016-07-27 15:45:20','2016-07-27 15:45:20','1'),(69,7,6,7,1,55,'P',2,2,'2016-07-27 15:45:20','2016-07-27 15:45:20','1'),(70,3,4,1,1,600,'P',2,2,'2016-07-27 15:46:18','2016-07-27 15:46:18','1'),(71,3,5,1,1,1100,'P',2,2,'2016-07-27 15:46:18','2016-07-27 15:46:18','1'),(72,2,2,2,1,500,'P',2,2,'2016-07-27 16:01:40','2016-07-27 16:01:40','1'),(73,3,5,1,1,1,'P',2,2,'2016-07-27 16:01:40','2016-07-27 16:01:40','1'),(74,3,4,1,1,600,'P',2,2,'2016-07-27 16:01:40','2016-07-27 16:01:40','1'),(75,7,6,7,1,55,'P',2,2,'2016-07-27 16:01:40','2016-07-27 16:01:40','1'),(76,6,1,1,1,60,'M',2,2,'2016-07-27 17:51:55','2016-07-27 17:51:55','1'),(77,3,5,1,1,1,'M',2,2,'2016-07-27 17:51:55','2016-07-27 17:51:55','1'),(78,7,6,7,1,55,'M',2,2,'2016-07-27 17:51:55','2016-07-27 17:51:55','1'),(79,6,1,1,1,60,'M',2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(80,3,5,1,1,1,'M',2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(81,3,4,1,1,600,'M',2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(82,7,6,7,1,55,'M',2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(83,6,1,1,1,60,'M',2,2,'2016-07-27 18:07:47','2016-07-27 18:07:47','1'),(84,7,6,7,1,55,'M',2,2,'2016-07-27 18:07:47','2016-07-27 18:07:47','1'),(85,3,5,1,1,1,'M',2,2,'2016-07-27 18:07:47','2016-07-27 18:07:47','1'),(86,7,6,7,1,55,'M',2,2,'2016-07-27 18:16:46','2016-07-27 18:16:46','1'),(87,3,5,1,1,1,'M',2,2,'2016-07-27 18:16:46','2016-07-27 18:16:46','1'),(88,6,1,1,1,60,'M',2,2,'2016-07-27 18:16:46','2016-07-27 18:16:46','1'),(89,6,1,1,1,60,'M',2,2,'2016-07-27 18:29:58','2016-07-27 18:29:58','1'),(90,3,5,1,1,1100,'M',2,2,'2016-07-27 18:29:58','2016-07-27 18:29:58','1'),(91,7,6,7,1,55,'M',2,2,'2016-07-27 18:29:58','2016-07-27 18:29:58','1'),(92,3,5,1,1,1100,'M',2,2,'2016-07-27 18:30:41','2016-07-27 18:30:41','1'),(93,7,6,7,1,55,'M',2,2,'2016-07-27 18:30:41','2016-07-27 18:30:41','1'),(94,6,1,1,1,60,'M',2,2,'2016-07-27 18:30:41','2016-07-27 18:30:41','1'),(95,2,2,2,1,500,'P',2,2,'2016-07-28 09:57:47','2016-07-28 09:57:47','1'),(96,3,5,1,2,2200,'P',2,2,'2016-07-28 09:57:47','2016-07-28 09:57:47','1'),(97,3,4,1,3,1800,'P',2,2,'2016-07-28 09:57:47','2016-07-28 09:57:47','1'),(98,7,6,7,4,220,'P',2,2,'2016-07-28 09:57:47','2016-07-28 09:57:47','1'),(99,6,1,1,1,60,'M',1,1,'2016-07-28 11:26:38','2016-07-28 11:26:38','1'),(100,3,5,1,1,1100,'M',1,1,'2016-07-28 11:26:38','2016-07-28 11:26:38','1'),(101,7,6,7,1,55,'M',1,1,'2016-07-28 11:26:38','2016-07-28 11:26:38','1'),(102,3,5,1,1,1100,'M',1,1,'2016-07-28 11:29:12','2016-07-28 11:29:12','1'),(103,7,6,7,1,55,'M',1,1,'2016-07-28 11:29:12','2016-07-28 11:29:12','1'),(104,6,1,1,1,60,'M',1,1,'2016-07-28 11:29:12','2016-07-28 11:29:12','1'),(105,7,6,7,2,110,'M',4,4,'2016-07-28 11:56:30','2016-07-28 11:56:30','1'),(106,3,5,1,5,5500,'M',4,4,'2016-07-28 11:56:30','2016-07-28 11:56:30','1'),(107,2,2,2,2,1000,'M',4,4,'2016-07-28 11:56:30','2016-07-28 11:56:30','1'),(108,7,6,7,5,275,'M',1,1,'2016-07-28 11:59:39','2016-07-28 11:59:39','1'),(109,6,1,1,3,180,'M',1,1,'2016-07-28 11:59:39','2016-07-28 11:59:39','1'),(110,2,2,2,1,500,'M',1,1,'2016-07-28 11:59:39','2016-07-28 11:59:39','1'),(111,6,1,1,5,300,'M',4,4,'2016-07-28 12:07:33','2016-07-28 12:07:33','1'),(112,3,4,1,5,3000,'M',4,4,'2016-07-28 12:07:33','2016-07-28 12:07:33','1'),(113,3,5,1,2,2200,'M',4,4,'2016-07-28 12:07:33','2016-07-28 12:07:33','1'),(114,2,2,2,5,2500,'P',2,2,'2016-07-28 15:06:55','2016-07-28 15:06:55','1'),(115,1,7,6,20,3000,'P',2,2,'2016-08-01 19:13:18','2016-08-01 19:13:18','1'),(116,7,6,7,1,55,'M',4,4,'2016-08-08 17:09:45','2016-08-08 17:09:45','1'),(117,6,1,1,1,60,'M',1,1,'2016-08-09 05:37:04','2016-08-09 05:37:04','1'),(118,3,4,1,1,600,'M',1,1,'2016-08-09 05:37:04','2016-08-09 05:37:04','1'),(119,2,2,2,2,1000,'M',1,1,'2016-08-09 05:37:04','2016-08-09 05:37:04','1'),(120,8,8,2,50,12500,'P',2,2,'2016-08-10 15:03:40','2016-08-10 15:03:40','1'),(121,8,8,2,10,2000,'P',2,2,'2016-08-10 15:06:21','2016-08-10 15:06:21','1'),(122,8,8,2,5,2500,'M',4,4,'2016-08-10 15:27:22','2016-08-10 15:27:22','1'),(123,8,8,4,1,500,'M',4,4,'2016-08-10 15:33:00','2016-08-10 15:33:00','1'),(124,1,7,6,2,300,'M',4,4,'2016-08-10 15:35:09','2016-08-10 15:35:09','1'),(125,8,8,4,5,2500,'P',2,2,'2016-08-11 14:18:16','2016-08-11 14:18:16','1'),(126,7,6,7,5,275,'P',2,2,'2016-08-16 11:40:48','2016-08-16 11:40:48','1'),(127,7,6,7,15,825,'P',2,2,'2016-08-16 11:42:39','2016-08-16 11:42:39','1'),(128,7,6,7,5,275,'M',4,4,'2016-08-16 11:45:59','2016-08-16 11:45:59','1'),(129,1,7,6,1,150,'M',15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1'),(130,2,2,2,2,1000,'M',15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1'),(131,8,8,4,1,500,'M',15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1'),(132,3,5,1,1,1100,'M',15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1');
/*!40000 ALTER TABLE `lie_shop_inventories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_items`
--

DROP TABLE IF EXISTS `lie_shop_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_category_id` int(11) NOT NULL,
  `metric_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `item_number` varchar(255) NOT NULL,
  `item_image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `front_price` double NOT NULL,
  `quantity` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_items`
--

LOCK TABLES `lie_shop_items` WRITE;
/*!40000 ALTER TABLE `lie_shop_items` DISABLE KEYS */;
INSERT INTO `lie_shop_items` VALUES (1,6,1,'Shop Caps','shop cap','','sdad asdad asdad',50,60,62,2,2,'2016-07-19 16:57:49','2016-08-09 05:37:04','1'),(2,2,2,'shop jeans','jeasn','','jeans for workers',400,500,1,2,2,'2016-07-19 16:56:00','2016-09-20 05:47:22','1'),(4,3,1,'employee shoes','shoes-9','','shoes for employees',400,600,33,2,2,'2016-07-19 16:59:32','2016-08-09 05:37:04','1'),(5,3,1,'leather shoes','leather','14709051674NT1DUZmq9Mz7N1O.png','sdasdasd asd asd as das',1000,1100,30,2,2,'2016-08-11 14:16:22','2016-09-20 05:47:22','1'),(6,7,7,'Cold Drink','12345','1473157101ngF7d94JpTVAzt1K.jpg','test test test',50,55,59,2,2,'2016-09-06 15:48:21','2016-09-06 15:48:21','1'),(7,1,6,'T-Shirt (Black)','TS001','','Just For test Just For testJust For test Just For testJust For testJust For testJust For testJust For testJust For testJust For testJust For testJust For testJust For testJust For testJust For testJust For testJust For test',100,150,17,2,2,'2016-08-01 19:05:35','2016-09-20 05:47:22','1'),(8,8,4,'T-shirt','Tshirt','','Red T-shirts',400,500,58,2,2,'2016-08-10 15:28:47','2016-09-20 05:47:22','1');
/*!40000 ALTER TABLE `lie_shop_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_metric_categories`
--

DROP TABLE IF EXISTS `lie_shop_metric_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_metric_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_category_id` int(11) NOT NULL,
  `metric_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_metric_categories`
--

LOCK TABLES `lie_shop_metric_categories` WRITE;
/*!40000 ALTER TABLE `lie_shop_metric_categories` DISABLE KEYS */;
INSERT INTO `lie_shop_metric_categories` VALUES (30,1,2,2,2,'2016-07-19 16:52:31','2016-07-19 16:52:31','1'),(31,1,6,2,2,'2016-07-19 16:52:31','2016-07-19 16:52:31','1'),(32,2,2,2,2,'2016-07-19 16:52:42','2016-07-19 16:52:42','1'),(33,2,4,2,2,'2016-07-19 16:52:42','2016-07-19 16:52:42','1'),(34,3,1,2,2,'2016-07-19 16:53:04','2016-07-19 16:53:04','1'),(35,3,2,2,2,'2016-07-19 16:53:04','2016-07-19 16:53:04','1'),(36,3,4,2,2,'2016-07-19 16:53:04','2016-07-19 16:53:04','1'),(37,6,1,2,2,'2016-07-19 16:53:18','2016-07-19 16:53:18','1'),(39,7,5,2,2,'2016-07-27 11:28:55','2016-07-27 11:28:55','1'),(40,7,7,2,2,'2016-07-27 11:28:55','2016-07-27 11:28:55','1'),(41,8,1,2,2,'2016-08-10 14:59:26','2016-08-10 14:59:26','1'),(42,8,2,2,2,'2016-08-10 14:59:26','2016-08-10 14:59:26','1'),(43,8,4,2,2,'2016-08-10 14:59:26','2016-08-10 14:59:26','1'),(44,8,6,2,2,'2016-08-10 14:59:26','2016-08-10 14:59:26','1');
/*!40000 ALTER TABLE `lie_shop_metric_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_order_statuses`
--

DROP TABLE IF EXISTS `lie_shop_order_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_order_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(255) NOT NULL,
  `status_desc` text NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_order_statuses`
--

LOCK TABLES `lie_shop_order_statuses` WRITE;
/*!40000 ALTER TABLE `lie_shop_order_statuses` DISABLE KEYS */;
INSERT INTO `lie_shop_order_statuses` VALUES (1,'Placed','placed',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(2,'Accepted','accepted',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(3,'Rejected','rejected',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(4,'Cancelled','cancelled',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(5,'In Transit','out',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1'),(6,'Delivered','delivered',0,0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','1');
/*!40000 ALTER TABLE `lie_shop_order_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_orders`
--

DROP TABLE IF EXISTS `lie_shop_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `shop_order_status_id` int(11) NOT NULL DEFAULT '1',
  `total_price` double NOT NULL,
  `discount` double NOT NULL,
  `grand_total` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_orders`
--

LOCK TABLES `lie_shop_orders` WRITE;
/*!40000 ALTER TABLE `lie_shop_orders` DISABLE KEYS */;
INSERT INTO `lie_shop_orders` VALUES (7,'Order-1',1,1,1,0,1,1,1,'2016-07-20 18:37:12','2016-07-20 18:37:12','1'),(8,'Order-8',1,1,60,0,60,1,1,'2016-07-21 15:43:49','2016-07-21 15:43:49','1'),(9,'Order-9',2,1,1,0,1,2,2,'2016-07-21 16:58:51','2016-07-21 16:58:51','1'),(10,'Order-10',1,1,660,0,660,1,1,'2016-07-22 09:59:50','2016-07-22 09:59:50','1'),(11,'Order-11',5,1,765,0,765,4,4,'2016-07-26 12:06:17','2016-07-26 12:06:17','1'),(12,'Order-12',5,1,500,0,500,4,4,'2016-07-26 12:13:27','2016-07-26 12:13:27','1'),(13,'Order-13',5,1,180,0,180,4,4,'2016-07-26 12:43:56','2016-07-26 12:43:56','1'),(14,'Order-14',5,1,1,0,1,4,4,'2016-07-26 16:09:58','2016-07-26 16:09:58','1'),(15,'Order-15',5,1,9,0,9,4,4,'2016-07-26 16:14:30','2016-07-26 16:14:30','1'),(16,'Order-16',2,1,1,0,1,2,2,'2016-07-27 11:32:37','2016-07-27 11:32:37','1'),(17,'Order-17',2,1,170,0,170,2,2,'2016-07-27 12:08:27','2016-07-27 12:08:27','1'),(18,'Order-18',6,1,60,0,60,5,5,'2016-07-27 12:16:32','2016-07-27 12:16:32','1'),(19,'Order-19',6,1,3,0,3,5,5,'2016-07-27 12:23:27','2016-07-27 12:23:27','1'),(20,'Order-20',2,1,1,0,1,2,2,'2016-07-27 13:19:41','2016-07-27 13:19:41','1'),(21,'Order-21',2,1,1,0,1,2,2,'2016-07-27 13:40:58','2016-07-27 13:40:58','1'),(22,'Order-22',2,1,1,0,1,2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(23,'Order-23',2,1,1,0,1,2,2,'2016-07-27 17:51:55','2016-07-27 17:51:55','1'),(24,'Order-24',2,1,1,0,1,2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(25,'Order-25',2,1,1,0,1,2,2,'2016-07-27 18:07:47','2016-07-27 18:07:47','1'),(26,'Order-26',2,1,1,0,1,2,2,'2016-07-27 18:16:46','2016-07-27 18:16:46','1'),(27,'Order-27',2,1,1215,0,1215,2,2,'2016-07-27 18:29:58','2016-07-27 18:29:58','1'),(28,'Order-28',2,1,1215,0,1215,2,2,'2016-07-27 18:30:41','2016-07-27 18:30:41','1'),(29,'Order-29',1,1,1215,0,1215,1,1,'2016-07-28 11:26:38','2016-07-28 11:26:38','1'),(30,'Order-30',1,1,1215,0.25,1215,1,1,'2016-07-28 11:29:12','2016-07-28 11:29:12','1'),(31,'Order-31',5,1,6610,0,6610,4,4,'2016-07-28 11:56:30','2016-07-28 11:56:30','1'),(32,'Order-32',1,5,955,0,955,1,2,'2016-07-28 11:59:39','2016-08-12 09:49:52','1'),(33,'Order-33',5,1,5500,0,5500,4,4,'2016-07-28 12:07:33','2016-07-28 12:07:33','1'),(34,'Order-34',5,1,55,0,55,4,4,'2016-08-08 17:08:23','2016-08-08 17:08:23','1'),(35,'Order-35',5,4,55,0,55,4,2,'2016-08-08 17:09:45','2016-09-06 15:40:55','1'),(36,'Order-36',1,6,1660,0,1660,1,2,'2016-08-09 05:37:04','2016-08-10 14:48:08','1'),(37,'Order-37',5,1,2500,0,2500,4,4,'2016-08-10 15:27:22','2016-08-10 15:27:22','1'),(38,'Order-38',5,4,500,0,500,4,2,'2016-08-10 15:33:00','2016-08-10 15:49:18','1'),(39,'Order-39',5,4,300,0,300,4,2,'2016-08-10 15:35:09','2016-08-10 17:46:48','1'),(40,'Order-40',5,4,275,0,275,4,2,'2016-08-16 11:45:59','2016-09-19 05:00:06','1'),(41,'Order-41',15,1,2750,0,2750,15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1');
/*!40000 ALTER TABLE `lie_shop_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_rest_cart_maps`
--

DROP TABLE IF EXISTS `lie_shop_rest_cart_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_rest_cart_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `price` double NOT NULL,
  `discount` double NOT NULL,
  `grand_total` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_rest_cart_maps`
--

LOCK TABLES `lie_shop_rest_cart_maps` WRITE;
/*!40000 ALTER TABLE `lie_shop_rest_cart_maps` DISABLE KEYS */;
INSERT INTO `lie_shop_rest_cart_maps` VALUES (7,'Order-1',1,5,1,1,0,1,1,1,'2016-07-20 18:37:12','2016-07-20 18:37:12','1'),(8,'Order-1',1,1,1,60,0,60,1,1,'2016-07-20 18:37:12','2016-07-20 18:37:12','1'),(9,'Order-8',1,1,1,60,0,60,1,1,'2016-07-21 15:43:49','2016-07-21 15:43:49','1'),(10,'Order-9',2,5,1,1,0,1,2,2,'2016-07-21 16:58:51','2016-07-21 16:58:51','1'),(11,'Order-9',2,1,2,120,0,120,2,2,'2016-07-21 16:58:51','2016-07-21 16:58:51','1'),(12,'Order-10',1,4,1,600,0,600,1,1,'2016-07-22 09:59:50','2016-07-22 09:59:50','1'),(13,'Order-10',1,1,1,60,0,60,1,1,'2016-07-22 09:59:50','2016-07-22 09:59:50','1'),(14,'Order-11',5,4,1,600,0,600,4,4,'2016-07-26 12:06:17','2016-07-26 12:06:17','1'),(15,'Order-11',5,6,3,165,0,165,4,4,'2016-07-26 12:06:17','2016-07-26 12:06:17','1'),(16,'Order-12',5,2,1,500,0,500,4,4,'2016-07-26 12:13:27','2016-07-26 12:13:27','1'),(17,'Order-13',5,1,3,180,0,180,4,4,'2016-07-26 12:43:56','2016-07-26 12:43:56','1'),(18,'Order-14',5,6,7,385,0,385,4,4,'2016-07-26 16:09:59','2016-07-26 16:09:59','1'),(19,'Order-14',5,2,1,500,0,500,4,4,'2016-07-26 16:09:59','2016-07-26 16:09:59','1'),(20,'Order-14',5,1,2,120,0,120,4,4,'2016-07-26 16:09:59','2016-07-26 16:09:59','1'),(21,'Order-15',5,4,15,9,0,9,4,4,'2016-07-26 16:14:30','2016-07-26 16:14:30','1'),(22,'Order-16',2,1,1,60,0,60,2,2,'2016-07-27 11:32:37','2016-07-27 11:32:37','1'),(23,'Order-16',2,5,1,1,0,1,2,2,'2016-07-27 11:32:37','2016-07-27 11:32:37','1'),(24,'Order-17',2,1,1,60,0,60,2,2,'2016-07-27 12:08:27','2016-07-27 12:08:27','1'),(25,'Order-17',2,6,2,110,0,110,2,2,'2016-07-27 12:08:27','2016-07-27 12:08:27','1'),(26,'Order-18',6,1,1,60,0,60,5,5,'2016-07-27 12:16:32','2016-07-27 12:16:32','1'),(27,'Order-19',6,6,5,275,0,275,5,5,'2016-07-27 12:23:27','2016-07-27 12:23:27','1'),(28,'Order-19',6,5,3,3,0,3,5,5,'2016-07-27 12:23:27','2016-07-27 12:23:27','1'),(29,'Order-19',6,1,1,60,0,60,5,5,'2016-07-27 12:23:27','2016-07-27 12:23:27','1'),(30,'Order-20',2,6,1,55,0,55,2,2,'2016-07-27 13:19:41','2016-07-27 13:19:41','1'),(31,'Order-20',2,1,1,60,0,60,2,2,'2016-07-27 13:19:41','2016-07-27 13:19:41','1'),(32,'Order-20',2,5,1,1,0,1,2,2,'2016-07-27 13:19:41','2016-07-27 13:19:41','1'),(33,'Order-21',2,1,1,60,0,60,2,2,'2016-07-27 13:40:58','2016-07-27 13:40:58','1'),(34,'Order-21',2,6,1,55,0,55,2,2,'2016-07-27 13:40:58','2016-07-27 13:40:58','1'),(35,'Order-21',2,5,1,1,0,1,2,2,'2016-07-27 13:40:58','2016-07-27 13:40:58','1'),(36,'Order-22',2,5,1,1,0,1,2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(37,'Order-22',2,6,1,55,0,55,2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(38,'Order-22',2,4,1,600,0,600,2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(39,'Order-22',2,1,1,60,0,60,2,2,'2016-07-27 15:08:46','2016-07-27 15:08:46','1'),(40,'Order-23',2,1,1,60,0,60,2,2,'2016-07-27 17:51:55','2016-07-27 17:51:55','1'),(41,'Order-23',2,5,1,1,0,1,2,2,'2016-07-27 17:51:55','2016-07-27 17:51:55','1'),(42,'Order-23',2,6,1,55,0,55,2,2,'2016-07-27 17:51:55','2016-07-27 17:51:55','1'),(43,'Order-24',2,1,1,60,0,60,2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(44,'Order-24',2,5,1,1,0,1,2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(45,'Order-24',2,4,1,600,0,600,2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(46,'Order-24',2,6,1,55,0,55,2,2,'2016-07-27 17:55:36','2016-07-27 17:55:36','1'),(47,'Order-25',2,1,1,60,0,60,2,2,'2016-07-27 18:07:47','2016-07-27 18:07:47','1'),(48,'Order-25',2,6,1,55,0,55,2,2,'2016-07-27 18:07:47','2016-07-27 18:07:47','1'),(49,'Order-25',2,5,1,1,0,1,2,2,'2016-07-27 18:07:47','2016-07-27 18:07:47','1'),(50,'Order-26',2,6,1,55,0,55,2,2,'2016-07-27 18:16:46','2016-07-27 18:16:46','1'),(51,'Order-26',2,5,1,1,0,1,2,2,'2016-07-27 18:16:46','2016-07-27 18:16:46','1'),(52,'Order-26',2,1,1,60,0,60,2,2,'2016-07-27 18:16:46','2016-07-27 18:16:46','1'),(53,'Order-27',2,1,1,60,0,60,2,2,'2016-07-27 18:29:58','2016-07-27 18:29:58','1'),(54,'Order-27',2,5,1,1100,0,1100,2,2,'2016-07-27 18:29:58','2016-07-27 18:29:58','1'),(55,'Order-27',2,6,1,55,0,55,2,2,'2016-07-27 18:29:58','2016-07-27 18:29:58','1'),(56,'Order-28',2,5,1,1100,0,1100,2,2,'2016-07-27 18:30:41','2016-07-27 18:30:41','1'),(57,'Order-28',2,6,1,55,0,55,2,2,'2016-07-27 18:30:41','2016-07-27 18:30:41','1'),(58,'Order-28',2,1,1,60,0,60,2,2,'2016-07-27 18:30:41','2016-07-27 18:30:41','1'),(59,'Order-29',1,1,1,60,0.25,60,1,1,'2016-07-28 11:26:38','2016-07-28 11:26:38','1'),(60,'Order-29',1,5,1,1100,0.25,1100,1,1,'2016-07-28 11:26:38','2016-07-28 11:26:38','1'),(61,'Order-29',1,6,1,55,0.25,55,1,1,'2016-07-28 11:26:38','2016-07-28 11:26:38','1'),(62,'Order-30',1,5,1,1100,0.25,1100,1,1,'2016-07-28 11:29:12','2016-07-28 11:29:12','1'),(63,'Order-30',1,6,1,55,0.25,55,1,1,'2016-07-28 11:29:12','2016-07-28 11:29:12','1'),(64,'Order-30',1,1,1,60,0.25,60,1,1,'2016-07-28 11:29:12','2016-07-28 11:29:12','1'),(65,'Order-31',5,6,2,110,0,110,4,4,'2016-07-28 11:56:30','2016-07-28 11:56:30','1'),(66,'Order-31',5,5,5,5500,0,5500,4,4,'2016-07-28 11:56:30','2016-07-28 11:56:30','1'),(67,'Order-31',5,2,2,1000,0,1000,4,4,'2016-07-28 11:56:30','2016-07-28 11:56:30','1'),(68,'Order-32',1,6,5,275,0,275,1,1,'2016-07-28 11:59:39','2016-07-28 11:59:39','1'),(69,'Order-32',1,1,3,180,0,180,1,1,'2016-07-28 11:59:39','2016-07-28 11:59:39','1'),(70,'Order-32',1,2,1,500,0,500,1,1,'2016-07-28 11:59:39','2016-07-28 11:59:39','1'),(71,'Order-33',5,1,5,300,0,300,4,4,'2016-07-28 12:07:33','2016-07-28 12:07:33','1'),(72,'Order-33',5,4,5,3000,0,3000,4,4,'2016-07-28 12:07:33','2016-07-28 12:07:33','1'),(73,'Order-33',5,5,2,2200,0,2200,4,4,'2016-07-28 12:07:33','2016-07-28 12:07:33','1'),(74,'Order-35',5,6,1,55,0,55,4,4,'2016-08-08 17:09:45','2016-08-08 17:09:45','1'),(75,'Order-36',1,1,1,60,0,60,1,1,'2016-08-09 05:37:04','2016-08-09 05:37:04','1'),(76,'Order-36',1,4,1,600,0,600,1,1,'2016-08-09 05:37:04','2016-08-09 05:37:04','1'),(77,'Order-36',1,2,2,1000,0,1000,1,1,'2016-08-09 05:37:04','2016-08-09 05:37:04','1'),(78,'Order-37',5,8,5,2500,0,2500,4,4,'2016-08-10 15:27:22','2016-08-10 15:27:22','1'),(79,'Order-38',5,8,1,500,0,500,4,4,'2016-08-10 15:33:00','2016-08-10 15:33:00','1'),(80,'Order-39',5,7,2,300,0,300,4,4,'2016-08-10 15:35:09','2016-08-10 15:35:09','1'),(81,'Order-40',5,6,5,275,0,275,4,4,'2016-08-16 11:45:59','2016-08-16 11:45:59','1'),(82,'Order-41',15,7,1,150,0,150,15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1'),(83,'Order-41',15,2,2,1000,0,1000,15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1'),(84,'Order-41',15,8,1,500,0,500,15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1'),(85,'Order-41',15,5,1,1100,0,1100,15,15,'2016-09-20 05:47:22','2016-09-20 05:47:22','1');
/*!40000 ALTER TABLE `lie_shop_rest_cart_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_rest_cart_temps`
--

DROP TABLE IF EXISTS `lie_shop_rest_cart_temps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_rest_cart_temps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `quantity` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_rest_cart_temps`
--

LOCK TABLES `lie_shop_rest_cart_temps` WRITE;
/*!40000 ALTER TABLE `lie_shop_rest_cart_temps` DISABLE KEYS */;
INSERT INTO `lie_shop_rest_cart_temps` VALUES (89,2,2,1,2,2,'2016-07-30 04:35:17','2016-07-30 04:35:17','1'),(93,8,6,1,6,6,'2016-08-02 13:03:45','2016-08-02 13:03:45','1'),(106,1,7,1,1,1,'2016-08-11 11:13:38','2016-08-11 11:13:38','1'),(107,1,8,1,1,1,'2016-08-11 11:31:59','2016-08-11 11:38:41','1'),(108,1,5,1,1,1,'2016-08-12 04:15:56','2016-08-12 04:15:56','1'),(111,9,7,1,7,7,'2016-08-17 17:57:40','2016-08-17 17:57:40','1');
/*!40000 ALTER TABLE `lie_shop_rest_cart_temps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_shop_transactions`
--

DROP TABLE IF EXISTS `lie_shop_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_shop_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL DEFAULT '0',
  `shop_order_id` varchar(50) NOT NULL DEFAULT '0',
  `f_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `mobile_no` varchar(50) NOT NULL,
  `add1` text NOT NULL,
  `add2` text NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_shop_transactions`
--

LOCK TABLES `lie_shop_transactions` WRITE;
/*!40000 ALTER TABLE `lie_shop_transactions` DISABLE KEYS */;
INSERT INTO `lie_shop_transactions` VALUES (16,1,'Order-1','Eastern','Spices','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'8614690200512440','2016-07-20 18:37:12',1,1,'2016-07-20 18:36:59','2016-07-20 18:37:12','1'),(17,1,'0','Eastern','Spices','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'','0000-00-00 00:00:00',1,1,'2016-07-21 15:40:58','2016-07-21 15:40:58','1'),(18,1,'Order-8','Eastern','Spices','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'8614690960475176','2016-07-21 15:43:49',1,1,'2016-07-21 15:40:58','2016-07-21 15:43:49','1'),(19,2,'Order-9','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'7914691005508699','2016-07-21 16:58:51',2,2,'2016-07-21 16:57:44','2016-07-21 16:58:51','1'),(20,1,'0','Eastern','Spices','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'','0000-00-00 00:00:00',1,1,'2016-07-22 07:11:08','2016-07-22 07:11:08','1'),(21,1,'0','Eastern','Spices','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'','0000-00-00 00:00:00',1,1,'2016-07-22 09:57:30','2016-07-22 09:57:30','1'),(22,1,'Order-10','Eastern','Spices','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'8814691618091314','2016-07-22 09:59:50',1,1,'2016-07-22 09:58:16','2016-07-22 09:59:50','1'),(23,5,'0','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'','0000-00-00 00:00:00',4,4,'2016-07-26 12:00:36','2016-07-26 12:00:36','1'),(24,5,'0','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'','0000-00-00 00:00:00',4,4,'2016-07-26 12:01:23','2016-07-26 12:01:23','1'),(25,5,'0','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'','0000-00-00 00:00:00',4,4,'2016-07-26 12:04:20','2016-07-26 12:04:20','1'),(26,5,'Order-11','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'7914695149985838','2016-07-26 12:06:17',4,4,'2016-07-26 12:05:56','2016-07-26 12:06:17','1'),(27,5,'0','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'','0000-00-00 00:00:00',4,4,'2016-07-26 12:06:56','2016-07-26 12:06:56','1'),(28,5,'0','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'','0000-00-00 00:00:00',4,4,'2016-07-26 12:10:57','2016-07-26 12:10:57','1'),(29,5,'Order-12','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'7914695154289548','2016-07-26 12:13:27',4,4,'2016-07-26 12:12:28','2016-07-26 12:13:27','1'),(30,5,'Order-13','Test','Test','1234567890','test','test',12,11,1,'7914695172562231','2016-07-26 12:43:56',4,4,'2016-07-26 12:42:29','2016-07-26 12:43:56','1'),(31,5,'0','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'','0000-00-00 00:00:00',4,4,'2016-07-26 12:44:36','2016-07-26 12:44:36','1'),(32,5,'Order-14','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'7914695296201286','2016-07-26 16:09:58',4,4,'2016-07-26 16:09:33','2016-07-26 16:09:58','1'),(33,5,'0','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'','0000-00-00 00:00:00',4,4,'2016-07-26 16:10:23','2016-07-26 16:10:23','1'),(34,5,'0','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'','0000-00-00 00:00:00',4,4,'2016-07-26 16:13:09','2016-07-26 16:13:09','1'),(35,5,'Order-15','Test Restaurant','','9711366589','Faridabad','NIT',1,1,1,'7914695298915560','2016-07-26 16:14:30',4,4,'2016-07-26 16:13:54','2016-07-26 16:14:30','1'),(36,2,'Order-16','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614695993794279','2016-07-27 11:32:37',2,2,'2016-07-27 11:31:33','2016-07-27 11:32:37','1'),(37,2,'0','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'','0000-00-00 00:00:00',2,2,'2016-07-27 11:39:21','2016-07-27 11:39:21','1'),(38,2,'0','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'','0000-00-00 00:00:00',2,2,'2016-07-27 11:44:58','2016-07-27 11:44:58','1'),(39,2,'Order-17','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696015286336','2016-07-27 12:08:27',2,2,'2016-07-27 12:08:12','2016-07-27 12:08:27','1'),(40,6,'Order-18','test','','98989898998','test','test',12,11,1,'7914696020139883','2016-07-27 12:16:32',5,5,'2016-07-27 12:16:03','2016-07-27 12:16:32','1'),(41,6,'0','test','','98989898998','test','test',12,11,1,'','0000-00-00 00:00:00',5,5,'2016-07-27 12:17:43','2016-07-27 12:17:43','1'),(42,6,'Order-19','test','','98989898998','test','test',12,11,1,'7914696024284487','2016-07-27 12:23:27',5,5,'2016-07-27 12:22:30','2016-07-27 12:23:27','1'),(43,6,'0','test','','98989898998','test','test',12,11,1,'','0000-00-00 00:00:00',5,5,'2016-07-27 12:24:24','2016-07-27 12:24:24','1'),(44,2,'Order-20','test','batra','7896321450','palwal','delhi',1,1,1,'8614696058038693','2016-07-27 13:19:41',2,2,'2016-07-27 13:19:05','2016-07-27 13:19:41','1'),(45,2,'Order-21','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696070795163','2016-07-27 13:40:58',2,2,'2016-07-27 13:40:43','2016-07-27 13:40:58','1'),(46,2,'Order-22','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696123485713','2016-07-27 15:08:46',2,2,'2016-07-27 15:08:24','2016-07-27 15:08:46','1'),(47,2,'Order-23','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696221374066','2016-07-27 17:51:55',2,2,'2016-07-27 17:50:39','2016-07-27 17:51:55','1'),(48,2,'Order-24','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696223570174','2016-07-27 17:55:36',2,2,'2016-07-27 17:55:19','2016-07-27 17:55:36','1'),(49,2,'Order-25','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696225968194','2016-07-27 18:07:47',2,2,'2016-07-27 17:59:19','2016-07-27 18:07:47','1'),(50,2,'Order-26','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696236275969','2016-07-27 18:16:46',2,2,'2016-07-27 18:16:31','2016-07-27 18:16:46','1'),(51,2,'Order-27','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696238523226','2016-07-27 18:29:58',2,2,'2016-07-27 18:20:18','2016-07-27 18:29:58','1'),(52,2,'Order-28','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'8614696244637523','2016-07-27 18:30:41',2,2,'2016-07-27 18:30:20','2016-07-27 18:30:41','1'),(53,1,'Order-29','Eastern','Spices','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'8514696854198445','2016-07-28 11:26:38',1,1,'2016-07-28 11:25:33','2016-07-28 11:26:38','1'),(54,1,'Order-30','Eastern','Spices','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'8514696855749873','2016-07-28 11:29:12',1,1,'2016-07-28 11:28:29','2016-07-28 11:29:12','1'),(55,5,'Order-31','TestRestaurant','test','9711366588','Faridabad','NIT',1,1,1,'7914696872125196','2016-07-28 11:56:30',4,4,'2016-07-28 11:56:01','2016-07-28 11:56:30','1'),(56,1,'Order-32','vikas','batra','1234567890','neelam chowk','testing team',12,11,1,'8514696874011925','2016-07-28 11:59:39',1,1,'2016-07-28 11:59:16','2016-07-28 11:59:39','1'),(57,5,'Order-33','ABC','ABC','1234567890','ABC','ABC',12,11,1,'7914696878759535','2016-07-28 12:07:33',4,4,'2016-07-28 12:07:09','2016-07-28 12:07:33','1'),(58,2,'0','Food Plaza','','9547632541','Meena Bazar near temple','motihari',2,1,1,'','0000-00-00 00:00:00',2,2,'2016-07-30 04:35:34','2016-07-30 04:35:34','1'),(59,9,'0','Black Spoon 1','','08860280996','A-81','A-82',1,1,1,'','0000-00-00 00:00:00',7,7,'2016-08-01 19:15:25','2016-08-01 19:15:25','1'),(60,8,'0','Arun',' sharma','9090909090','A 11 sec 37 ','faridabad',1,1,1,'','0000-00-00 00:00:00',6,6,'2016-08-02 13:04:55','2016-08-02 13:04:55','1'),(61,8,'0','Arun','sharma','8080808080','e22 b-block','tuglakabad',1,1,1,'','0000-00-00 00:00:00',6,6,'2016-08-02 13:13:25','2016-08-02 13:13:25','1'),(62,5,'Order-35','TestRestaurant','test','9711366588','Faridabad','NIT',1,1,1,'8514706563871164','2016-08-08 17:09:45',4,4,'2016-08-08 17:09:08','2016-08-08 17:09:45','1'),(63,1,'0','Eastern Restaurant','','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'','0000-00-00 00:00:00',1,1,'2016-08-08 18:24:06','2016-08-08 18:24:06','1'),(64,1,'0','Eastern Restaurant','','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'','0000-00-00 00:00:00',1,1,'2016-08-09 05:10:39','2016-08-09 05:10:39','1'),(65,1,'Order-36','Eastern Restaurant','','9876543211','A-52,Gali no 10','South Ganesh nagar',1,1,1,'8614707012272870','2016-08-09 05:37:04',1,1,'2016-08-09 05:32:15','2016-08-09 05:37:04','1'),(66,5,'0','Test Restaurant','','9711366588','Faridabad','NIT',12,11,1,'','0000-00-00 00:00:00',4,4,'2016-08-10 15:21:30','2016-08-10 15:21:30','1'),(67,5,'0','Test Restaurant','','9711366588','Faridabad','NIT',12,11,1,'','0000-00-00 00:00:00',4,4,'2016-08-10 15:22:12','2016-08-10 15:22:12','1'),(68,5,'Order-37','Test Restaurant','','9711366588','Faridabad','NIT',12,11,1,'8814708230459305','2016-08-10 15:27:22',4,4,'2016-08-10 15:22:28','2016-08-10 15:27:22','1'),(69,5,'Order-38','Bindu','kharub','9711366589','NIT','Faridabad',12,11,1,'8814708233832466','2016-08-10 15:33:00',4,4,'2016-08-10 15:32:46','2016-08-10 15:33:00','1'),(70,5,'Order-39','Test Restaurant','','9711366588','12345','',12,11,1,'8814708235125596','2016-08-10 15:35:09',4,4,'2016-08-10 15:34:58','2016-08-10 15:35:09','1'),(71,1,'0','Eastern Restaurant','','9876543211','A-52,Gali no 10','South Ganesh nagar',14,17,4,'','0000-00-00 00:00:00',1,1,'2016-08-11 11:26:02','2016-08-11 11:26:02','1'),(72,1,'0','Eastern Restaurant','','9876543211','A-52,Gali no 10','South Ganesh nagar',14,17,4,'','0000-00-00 00:00:00',1,1,'2016-08-11 11:27:36','2016-08-11 11:27:36','1'),(73,1,'0','Eastern Restaurant','','9876543211','A-52,Gali no 10','South Ganesh nagar',14,17,4,'','0000-00-00 00:00:00',1,1,'2016-08-11 11:32:04','2016-08-11 11:32:04','1'),(74,1,'0','Eastern Restaurant','','9876543211','A-52,Gali no 10','South Ganesh nagar',14,17,4,'','0000-00-00 00:00:00',1,1,'2016-08-11 11:33:05','2016-08-11 11:33:05','1'),(75,1,'0','Eastern Restaurant','','9876543211','A-52,Gali no 10','South Ganesh nagar',14,17,4,'','0000-00-00 00:00:00',1,1,'2016-08-12 04:16:02','2016-08-12 04:16:02','1'),(76,5,'Order-40','Test Restaurant','','9711366588','12345','',12,11,1,'7914713281603836','2016-08-16 11:45:59',4,4,'2016-08-16 11:45:25','2016-08-16 11:45:59','1'),(77,9,'0','Black Spoon','','08860280996','A-81','A-82',1,1,1,'','0000-00-00 00:00:00',7,7,'2016-08-17 17:57:52','2016-08-17 17:57:52','1'),(78,1,'0','Eastern Restaurant','','9876543211','Quellenstrasse 167','Favoriten',11,21,4,'','0000-00-00 00:00:00',1,1,'2016-08-21 07:24:29','2016-08-21 07:24:29','1'),(79,15,'0','Pizzeria Ristorante  Castello','','0664497508','Herzgasse 12','Favoriten',11,21,4,'','0000-00-00 00:00:00',15,15,'2016-09-15 05:35:49','2016-09-15 05:35:49','1'),(80,15,'Order-41','Pizzeria Ristorante  Castello','','0664497508','Herzgasse 12','Favoriten',11,21,4,'8814743306480653','2016-09-20 05:47:22',15,15,'2016-09-20 05:41:24','2016-09-20 05:47:22','1');
/*!40000 ALTER TABLE `lie_shop_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_social_networks`
--

DROP TABLE IF EXISTS `lie_social_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_social_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `google` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `vk` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `pinterest` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_social_networks`
--

LOCK TABLES `lie_social_networks` WRITE;
/*!40000 ALTER TABLE `lie_social_networks` DISABLE KEYS */;
INSERT INTO `lie_social_networks` VALUES (1,'https://www.facebook.com/Lieferzonas.at','https://twitter.com/lieferzonasat','http://www.youtube.com/lieferzonas','http://plus.google.com/+Lieferzonas','http://www.linkedin.com/company/lieferzonas-at','http://www.blog.lieferzonas.at','http://www.instagram.com/lieferzonas.at','http://www.pinterest.com/lieferzonasat',1,1,'2016-05-03 00:00:00','2016-08-17 06:18:02','1');
/*!40000 ALTER TABLE `lie_social_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_social_types`
--

DROP TABLE IF EXISTS `lie_social_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_social_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_social_types`
--

LOCK TABLES `lie_social_types` WRITE;
/*!40000 ALTER TABLE `lie_social_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_social_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_special_offer_cat_maps`
--

DROP TABLE IF EXISTS `lie_special_offer_cat_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_special_offer_cat_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `special_offer_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_special_offer_cat_maps_lie_rest_categories_id_fk` (`category_id`),
  KEY `lie_special_offer_cat_maps_lie_special_offers_id_fk` (`special_offer_id`),
  CONSTRAINT `lie_special_offer_cat_maps_lie_rest_categories_id_fk` FOREIGN KEY (`category_id`) REFERENCES `lie_rest_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lie_special_offer_cat_maps_lie_special_offers_id_fk` FOREIGN KEY (`special_offer_id`) REFERENCES `lie_special_offers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_special_offer_cat_maps`
--

LOCK TABLES `lie_special_offer_cat_maps` WRITE;
/*!40000 ALTER TABLE `lie_special_offer_cat_maps` DISABLE KEYS */;
INSERT INTO `lie_special_offer_cat_maps` VALUES (1,1,1,1,0,'2016-08-30 16:10:28','2016-08-30 16:10:28','1'),(2,1,2,1,0,'2016-08-30 16:15:46','2016-08-30 16:15:46','1'),(3,5,3,4,0,'2016-09-05 18:41:52','2016-09-05 18:41:52','1'),(8,23,8,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1');
/*!40000 ALTER TABLE `lie_special_offer_cat_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_special_offer_day_maps`
--

DROP TABLE IF EXISTS `lie_special_offer_day_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_special_offer_day_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `special_offer_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_special_offer_day_maps_lie_special_offers_id_fk` (`special_offer_id`),
  CONSTRAINT `lie_special_offer_day_maps_lie_special_offers_id_fk` FOREIGN KEY (`special_offer_id`) REFERENCES `lie_special_offers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_special_offer_day_maps`
--

LOCK TABLES `lie_special_offer_day_maps` WRITE;
/*!40000 ALTER TABLE `lie_special_offer_day_maps` DISABLE KEYS */;
INSERT INTO `lie_special_offer_day_maps` VALUES (1,1,3,0,1,'2016-08-30 16:10:28','2016-08-30 16:10:28','1'),(2,2,1,0,1,'2016-08-30 16:15:46','2016-08-30 16:15:46','1'),(3,2,2,0,1,'2016-08-30 16:15:46','2016-08-30 16:15:46','1'),(4,3,1,0,4,'2016-09-05 18:41:52','2016-09-05 18:41:52','1'),(5,3,2,0,4,'2016-09-05 18:41:52','2016-09-05 18:41:52','1'),(6,3,3,0,4,'2016-09-05 18:41:52','2016-09-05 18:41:52','1'),(20,8,1,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1'),(21,8,4,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1'),(22,8,2,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1'),(23,8,5,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1');
/*!40000 ALTER TABLE `lie_special_offer_day_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_special_offer_day_time_maps`
--

DROP TABLE IF EXISTS `lie_special_offer_day_time_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_special_offer_day_time_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `special_offer_id` int(11) NOT NULL,
  `time_from` varchar(10) NOT NULL,
  `time_to` varchar(10) NOT NULL,
  `day_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_special_offer_day_time_maps`
--

LOCK TABLES `lie_special_offer_day_time_maps` WRITE;
/*!40000 ALTER TABLE `lie_special_offer_day_time_maps` DISABLE KEYS */;
INSERT INTO `lie_special_offer_day_time_maps` VALUES (1,1,'00:30 AM','01:30 AM',1,1,0,'2016-08-30 16:10:28','2016-08-30 16:10:28','1'),(2,1,'00:30 AM','02:00 AM',2,1,0,'2016-08-30 16:10:28','2016-08-30 16:10:28','1'),(3,2,'08:30 AM','08:30 AM',3,1,0,'2016-08-30 16:15:46','2016-08-30 16:15:46','1'),(4,2,'09:30 AM','10:30 AM',3,1,0,'2016-08-30 16:15:46','2016-08-30 16:15:46','1'),(18,8,'11:00 AM','2:00 PM',1,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1'),(19,8,'11:00 AM','2:00 PM',4,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1'),(20,8,'11:00 AM','2:00 PM',2,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1'),(21,8,'11:00 AM','2:00 PM',5,2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1');
/*!40000 ALTER TABLE `lie_special_offer_day_time_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_special_offer_menu_maps`
--

DROP TABLE IF EXISTS `lie_special_offer_menu_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_special_offer_menu_maps` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `special_offer_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_special_offer_menu_maps`
--

LOCK TABLES `lie_special_offer_menu_maps` WRITE;
/*!40000 ALTER TABLE `lie_special_offer_menu_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_special_offer_menu_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_special_offer_order_services`
--

DROP TABLE IF EXISTS `lie_special_offer_order_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_special_offer_order_services` (
  `id` int(11) NOT NULL,
  `special_offer_id` int(11) NOT NULL,
  `order_service_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_special_offer_order_services`
--

LOCK TABLES `lie_special_offer_order_services` WRITE;
/*!40000 ALTER TABLE `lie_special_offer_order_services` DISABLE KEYS */;
INSERT INTO `lie_special_offer_order_services` VALUES (0,1,1,1,0,'2016-08-30 16:10:28','2016-08-30 16:10:28','1'),(0,2,1,1,0,'2016-08-30 16:15:46','2016-08-30 16:15:46','1'),(0,3,1,4,0,'2016-09-05 18:41:52','2016-09-05 18:41:52','1');
/*!40000 ALTER TABLE `lie_special_offer_order_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_special_offer_submenu_maps`
--

DROP TABLE IF EXISTS `lie_special_offer_submenu_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_special_offer_submenu_maps` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `special_offer_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_special_offer_submenu_maps`
--

LOCK TABLES `lie_special_offer_submenu_maps` WRITE;
/*!40000 ALTER TABLE `lie_special_offer_submenu_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_special_offer_submenu_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_special_offers`
--

DROP TABLE IF EXISTS `lie_special_offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_special_offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `validity_type` enum('T','C') NOT NULL COMMENT 'T for table,C for calendar',
  `type` varchar(10) NOT NULL,
  `validity_table` int(11) NOT NULL DEFAULT '0',
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `discount_type_id` int(11) NOT NULL,
  `original_price` varchar(6) NOT NULL,
  `discount_price` varchar(6) NOT NULL,
  `discount_type_value` varchar(50) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','3') NOT NULL COMMENT '1 for active->confirmed,0 for deactive->pending,3 for rejection,2 for delete',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_special_offers`
--

LOCK TABLES `lie_special_offers` WRITE;
/*!40000 ALTER TABLE `lie_special_offers` DISABLE KEYS */;
INSERT INTO `lie_special_offers` VALUES (1,'Test offer',1,'C','1',0,'2016-08-30 00:00:00','2016-09-03 00:00:00',1,'1000','100','100',1,0,'2016-08-30 16:10:28','2016-12-03 09:13:01','1',100.00),(2,'potato offer',1,'C','1',0,'2016-08-30 00:00:00','2016-09-05 00:00:00',1,'100','50','10',1,0,'2016-08-30 16:15:46','2016-09-05 18:05:09','1',10.00),(3,'Parantha special',5,'C','1',0,'2016-09-05 00:00:00','2016-09-08 00:00:00',1,'200','150','100',4,0,'2016-09-05 18:41:52','2016-09-05 18:42:28','1',100.00),(8,'Castello Mittagsmenü',15,'T','',0,'2016-12-04 00:00:00','2099-01-01 00:00:00',1,'','','5.50',2,2,'2016-12-04 01:22:46','2016-12-04 01:22:46','1',5.50);
/*!40000 ALTER TABLE `lie_special_offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_spices`
--

DROP TABLE IF EXISTS `lie_spices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_spices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_spices`
--

LOCK TABLES `lie_spices` WRITE;
/*!40000 ALTER TABLE `lie_spices` DISABLE KEYS */;
INSERT INTO `lie_spices` VALUES (1,'Mirchi Powder','1468926444231.jpg','Testing By Rajlakshmi',2,0,'2016-07-19 16:37:24','2016-08-24 02:37:08','2'),(2,'Dhaniya','1468931114441.jpg','This is a dhaniya',2,2,'2016-07-19 17:55:14','2016-08-24 02:37:08','2'),(3,'test','','test',2,0,'2016-07-27 12:46:42','2016-08-24 02:36:48','2'),(4,'Chili ','1473900066337.png','Scharf',2,2,'2016-08-12 07:16:58','2016-09-15 06:11:06','1'),(5,'Knoblauch','1473900110277.png','Knoblauch',2,2,'2016-08-12 07:17:45','2016-09-15 06:11:50','1'),(6,'Vegetarisch','1473900208249.png','Diese Speise ist Vegetarisch',2,2,'2016-08-12 07:18:25','2016-09-15 06:13:28','1'),(7,'Fisch','1473900079315.png','Fisch',2,2,'2016-08-12 07:18:55','2016-09-15 06:11:19','1'),(8,'Rindfleisch','1473900148897.png','Rindfleisch',2,2,'2016-08-12 07:19:34','2016-09-15 06:12:28','1'),(9,'Schweinefleisch','1473900159537.png','Schweinefleisch',2,2,'2016-08-12 07:20:06','2016-09-15 06:12:39','1'),(10,'Helal','1473900124624.png','Diese Speise ist Helal',2,2,'2016-08-12 07:20:48','2016-09-15 06:12:04','1'),(11,'Kosher','1473900136177.png','Diese Speise ist Kosher',2,2,'2016-08-12 07:21:33','2016-09-15 06:12:16','1'),(12,'Geflügel','1473900096246.png','Geflügel',2,2,'2016-08-12 07:22:22','2016-09-15 06:11:36','1');
/*!40000 ALTER TABLE `lie_spices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_states`
--

DROP TABLE IF EXISTS `lie_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_states`
--

LOCK TABLES `lie_states` WRITE;
/*!40000 ALTER TABLE `lie_states` DISABLE KEYS */;
INSERT INTO `lie_states` VALUES (1,1,'Delhi','delhi','',0,0,'0000-00-00 00:00:00','2016-08-11 03:03:55','2'),(2,2,'Vienna','vienna','',0,0,'0000-00-00 00:00:00','2016-08-11 03:03:41','2'),(3,4,'Wienk','wein','Stadt Wien',0,0,'0000-00-00 00:00:00','2016-08-11 04:58:28','2'),(4,4,'Punjab','punjab','punjab punjab',0,0,'2016-07-19 16:19:02','2016-08-11 03:03:41','2'),(5,5,'Big Town','big-town','',0,0,'0000-00-00 00:00:00','2016-08-11 03:03:55','2'),(6,6,'New Jursey','new-jursey','',0,0,'0000-00-00 00:00:00','2016-08-11 03:03:41','2'),(7,7,'Sydney','sydney','',0,0,'0000-00-00 00:00:00','2016-08-11 03:03:41','2'),(8,8,'Madras','madras','',0,0,'0000-00-00 00:00:00','2016-08-11 03:03:41','2'),(9,9,'Beijing','beijing','',0,0,'0000-00-00 00:00:00','2016-08-11 03:03:55','2'),(10,10,'London','london','',0,0,'0000-00-00 00:00:00','2016-08-11 03:03:41','2'),(11,1,'Haryana','haryana','Haryana state',0,0,'2016-07-25 16:02:53','2016-08-11 03:03:55','2'),(12,11,'pataya','pataya','pataya pataya',0,0,'2016-07-30 16:43:29','2016-08-11 03:03:41','2'),(13,4,'Niederöesterreich ','niederoesterreich','niederösterreich',0,0,'2016-08-05 07:18:11','2016-08-31 03:03:20','1'),(14,4,'Burgenland','burgenland','Burgenland',0,0,'2016-08-05 07:19:06','2016-08-05 07:19:06','1'),(15,4,'Oberösterreich','oberoesterreich','Oberösterreich',0,0,'2016-08-05 07:19:48','2016-08-31 03:03:39','1'),(16,4,'Salzburg','salzburg-1','Bundesland Salzburg',0,0,'2016-08-11 03:05:23','2016-08-11 03:05:23','1'),(17,4,'Steiermarkt','steiermarkt-1','Bundesland Steiermarkt',0,0,'2016-08-11 03:13:22','2016-08-11 03:13:22','1'),(18,4,'Vorarlberg','vorarlberg-1','Bundesland Vorarlberg',0,0,'2016-08-11 03:15:03','2016-08-11 03:15:03','1'),(19,4,'Tirol','tirol-1','Bundesland Tirol',0,0,'2016-08-11 03:15:40','2016-08-11 03:15:40','1'),(20,4,'Kärnten','kaernten-1','Bundesland Kärnten',0,0,'2016-08-11 03:19:16','2016-08-31 03:00:17','1'),(21,4,'Wien','wien','Stadt Wien',0,0,'2016-08-11 04:58:50','2016-08-11 04:58:50','1'),(22,4,'Kärnteng','k-rnten','Bundesland Kärnten',0,0,'2016-08-31 02:54:03','2016-08-31 03:00:03','2'),(23,4,'Falsch- fehler Dieses entfernen','falsch-fehler-dieses-entfernen','falsch-fehler-dieses-entfernen  Edit\r\n',0,0,'2016-08-31 05:54:05','2016-08-31 05:54:05','1');
/*!40000 ALTER TABLE `lie_states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_sub_menu_extra_map`
--

DROP TABLE IF EXISTS `lie_sub_menu_extra_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_sub_menu_extra_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_menu_id` int(11) NOT NULL,
  `extra_choice_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sub_menu_id` (`sub_menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_sub_menu_extra_map`
--

LOCK TABLES `lie_sub_menu_extra_map` WRITE;
/*!40000 ALTER TABLE `lie_sub_menu_extra_map` DISABLE KEYS */;
INSERT INTO `lie_sub_menu_extra_map` VALUES (1,12,2,'2016-11-13 22:15:49','2016-11-13 22:15:49',0,0,'1'),(2,11,2,'2016-11-13 22:16:00','2016-11-13 22:16:00',0,0,'1'),(4,30,2,'2016-11-14 01:53:19','2016-11-14 01:53:19',0,0,'1'),(5,30,3,'2016-11-14 01:53:19','2016-11-14 01:53:19',0,0,'1');
/*!40000 ALTER TABLE `lie_sub_menu_extra_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_sub_menu_price_maps`
--

DROP TABLE IF EXISTS `lie_sub_menu_price_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_sub_menu_price_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `price_3` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted',
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_sub_menu_price_maps`
--

LOCK TABLES `lie_sub_menu_price_maps` WRITE;
/*!40000 ALTER TABLE `lie_sub_menu_price_maps` DISABLE KEYS */;
INSERT INTO `lie_sub_menu_price_maps` VALUES (1,2,2,1,1,'100',2,2,'2016-07-19 17:57:23','2016-07-19 17:57:23','0',100.00),(3,2,2,2,1,'145',2,2,'2016-07-19 17:59:16','2016-07-19 17:59:16','0',145.00),(4,4,5,5,3,'10',2,2,'2016-07-22 14:51:17','2016-07-22 14:51:17','0',10.00),(5,4,5,5,4,'10',2,2,'2016-07-22 14:51:17','2016-07-22 14:51:17','0',10.00),(10,1,4,4,3,'5.15',2,2,'2016-07-27 05:39:13','2016-07-27 05:39:13','0',5.15),(12,6,8,7,6,'200',2,2,'2016-07-27 12:35:22','2016-07-27 12:35:22','0',200.00),(13,6,8,7,4,'250',2,2,'2016-07-27 12:35:22','2016-07-27 12:35:22','0',250.00),(14,1,3,3,3,'5',2,2,'2016-07-28 17:08:24','2016-07-28 17:08:24','0',5.00),(15,9,13,8,3,'100',2,2,'2016-08-02 13:18:07','2016-08-02 13:18:07','0',100.00),(16,9,13,8,4,'100',2,2,'2016-08-02 13:18:07','2016-08-02 13:18:07','0',100.00),(17,9,13,9,3,'90',2,2,'2016-08-02 13:19:13','2016-08-02 13:19:13','0',90.00),(18,9,13,9,4,'90',2,2,'2016-08-02 13:19:13','2016-08-02 13:19:13','0',90.00),(19,9,13,10,3,'200',2,2,'2016-08-02 13:20:06','2016-08-02 13:20:06','0',200.00),(20,9,13,10,4,'200',2,2,'2016-08-02 13:20:06','2016-08-02 13:20:06','0',200.00),(46,15,19,11,1,'7.99',2,2,'2016-08-24 04:06:38','2016-08-24 04:06:38','0',7.99),(47,15,19,12,1,'6',2,2,'2016-08-24 04:07:03','2016-08-24 04:07:03','0',6.00),(49,15,19,15,1,'6.9',2,2,'2016-08-24 04:14:25','2016-08-24 04:14:25','0',6.90),(51,15,19,16,1,'5.2',2,2,'2016-08-24 04:16:36','2016-08-24 04:16:36','0',5.20),(52,15,19,14,1,'6.9',2,2,'2016-08-24 04:19:55','2016-08-24 04:19:55','0',6.90),(53,15,19,17,1,'5.9',2,2,'2016-08-24 04:21:37','2016-08-24 04:21:37','0',5.90),(54,15,19,18,1,'5.9',2,2,'2016-08-24 04:23:43','2016-08-24 04:23:43','0',5.90),(55,15,20,13,1,'5.9',2,2,'2016-08-24 05:00:33','2016-08-24 05:00:33','0',5.90),(56,15,20,19,1,'7.90',2,2,'2016-08-24 05:20:50','2016-08-24 05:20:50','0',7.90),(57,15,20,20,1,'7.5',2,2,'2016-08-24 05:25:48','2016-08-24 05:25:48','0',7.50),(59,15,22,22,1,'8.70',2,2,'2016-08-24 06:12:21','2016-08-24 06:12:21','0',8.70),(61,15,22,24,1,'11.50',2,2,'2016-08-24 06:13:46','2016-08-24 06:13:46','0',11.50),(62,15,22,23,1,'8.70',2,2,'2016-08-24 06:15:48','2016-08-24 06:15:48','0',8.70),(67,5,24,28,1,'200',2,2,'2016-09-07 11:43:04','2016-09-07 11:43:04','0',200.00),(68,5,24,28,2,'180',2,2,'2016-09-07 11:43:04','2016-09-07 11:43:04','0',180.00),(69,5,6,6,1,'250',2,2,'2016-09-07 11:55:56','2016-09-07 11:55:56','0',250.00),(70,5,6,6,2,'200',2,2,'2016-09-07 11:55:56','2016-09-07 11:55:56','0',200.00),(71,5,6,27,1,'260',2,2,'2016-09-09 15:28:10','2016-09-09 15:28:10','0',260.00),(72,5,6,27,2,'230',2,2,'2016-09-09 15:28:10','2016-09-09 15:28:10','0',230.00),(74,15,23,25,1,'13.50',2,2,'2016-09-16 05:42:06','2016-09-16 05:42:06','0',13.50),(76,15,23,26,1,'14.50',2,2,'2016-09-16 05:49:44','2016-09-16 05:49:44','0',14.50),(77,15,22,21,1,'9.99',2,2,'2016-09-16 05:53:50','2016-09-16 05:53:50','0',9.99),(78,15,19,30,1,'',2,2,'2016-11-11 01:00:56','2016-11-11 01:00:56','0',20.00),(79,15,19,30,2,'',2,2,'2016-11-11 01:00:56','2016-11-11 01:00:56','0',30.00);
/*!40000 ALTER TABLE `lie_sub_menu_price_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_suggestion_kitchen_maps`
--

DROP TABLE IF EXISTS `lie_suggestion_kitchen_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_suggestion_kitchen_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_suggestion_id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT 'o for inactive,1 for active,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_suggestion_kitchen_maps`
--

LOCK TABLES `lie_suggestion_kitchen_maps` WRITE;
/*!40000 ALTER TABLE `lie_suggestion_kitchen_maps` DISABLE KEYS */;
INSERT INTO `lie_suggestion_kitchen_maps` VALUES (1,1,4,'1',0,0,'2016-07-21 15:20:11','2016-07-21 15:20:11'),(2,2,3,'1',0,0,'2016-07-22 08:03:06','2016-07-22 08:03:06'),(3,3,2,'1',0,0,'2016-07-28 15:53:28','2016-07-28 15:53:28'),(4,4,1,'1',0,0,'2016-08-11 02:07:54','2016-08-11 02:07:54'),(5,4,3,'1',0,0,'2016-08-11 02:07:54','2016-08-11 02:07:54'),(6,8,1,'1',0,0,'2016-08-17 16:41:33','2016-08-17 16:41:33'),(7,9,1,'1',0,0,'2016-08-17 16:44:13','2016-08-17 16:44:13'),(8,11,1,'1',0,0,'2016-08-17 17:50:03','2016-08-17 17:50:03'),(9,13,1,'1',0,0,'2016-08-18 11:06:11','2016-08-18 11:06:11'),(10,14,1,'1',0,0,'2016-08-18 11:06:36','2016-08-18 11:06:36'),(11,15,1,'1',0,0,'2016-08-18 11:12:57','2016-08-18 11:12:57'),(12,16,1,'1',0,0,'2016-08-18 11:14:22','2016-08-18 11:14:22'),(13,17,1,'1',0,0,'2016-08-18 15:01:37','2016-08-18 15:01:37'),(14,18,1,'1',0,0,'2016-08-18 15:32:46','2016-08-18 15:32:46'),(15,19,1,'1',0,0,'2016-08-18 15:32:46','2016-08-18 15:32:46'),(16,20,1,'1',0,0,'2016-08-18 15:32:47','2016-08-18 15:32:47'),(17,21,1,'1',0,0,'2016-08-18 15:32:48','2016-08-18 15:32:48'),(18,22,1,'1',0,0,'2016-08-18 15:32:51','2016-08-18 15:32:51'),(19,23,1,'1',0,0,'2016-08-18 16:09:18','2016-08-18 16:09:18'),(20,24,1,'1',0,0,'2016-08-18 16:11:49','2016-08-18 16:11:49'),(21,26,1,'1',0,0,'2016-08-22 10:48:52','2016-08-22 10:48:52'),(22,27,1,'1',0,0,'2016-08-22 10:49:14','2016-08-22 10:49:14'),(23,28,1,'1',0,0,'2016-08-22 11:25:04','2016-08-22 11:25:04'),(24,29,1,'1',0,0,'2016-08-22 11:25:05','2016-08-22 11:25:05'),(25,30,1,'1',0,0,'2016-08-22 11:34:22','2016-08-22 11:34:22'),(26,31,1,'1',0,0,'2016-08-22 11:35:29','2016-08-22 11:35:29'),(27,32,1,'1',0,0,'2016-08-22 11:36:45','2016-08-22 11:36:45'),(28,33,1,'1',0,0,'2016-08-22 11:41:17','2016-08-22 11:41:17'),(29,33,2,'1',0,0,'2016-08-22 11:41:17','2016-08-22 11:41:17'),(30,33,4,'1',0,0,'2016-08-22 11:41:17','2016-08-22 11:41:17'),(31,34,1,'1',0,0,'2016-08-22 13:43:36','2016-08-22 13:43:36'),(32,34,2,'1',0,0,'2016-08-22 13:43:36','2016-08-22 13:43:36'),(33,40,15,'1',0,0,'2016-08-23 16:38:04','2016-08-23 16:38:04'),(34,48,1,'1',0,0,'2016-09-08 18:58:23','2016-09-08 18:58:23'),(35,49,1,'1',0,0,'2016-09-08 19:00:49','2016-09-08 19:00:49'),(36,50,1,'1',0,0,'2016-09-12 13:42:44','2016-09-12 13:42:44'),(37,51,1,'1',0,0,'2016-09-12 15:12:54','2016-09-12 15:12:54'),(38,52,1,'1',0,0,'2016-09-12 16:07:58','2016-09-12 16:07:58'),(39,53,1,'1',0,0,'2016-09-12 16:11:16','2016-09-12 16:11:16'),(40,54,1,'1',0,0,'2016-09-26 04:33:35','2016-09-26 04:33:35'),(41,54,16,'1',0,0,'2016-09-26 04:33:35','2016-09-26 04:33:35'),(42,54,17,'1',0,0,'2016-09-26 04:33:35','2016-09-26 04:33:35');
/*!40000 ALTER TABLE `lie_suggestion_kitchen_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_super_admin_service_tax_invoices`
--

DROP TABLE IF EXISTS `lie_super_admin_service_tax_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_super_admin_service_tax_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `invoice_id` varchar(250) NOT NULL,
  `total_sale` bigint(20) NOT NULL,
  `online_sale` bigint(20) NOT NULL,
  `service_tax` int(11) NOT NULL,
  `shop_orders` bigint(20) NOT NULL,
  `search_rank` bigint(20) NOT NULL,
  `final_amount` bigint(20) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_super_admin_service_tax_invoices`
--

LOCK TABLES `lie_super_admin_service_tax_invoices` WRITE;
/*!40000 ALTER TABLE `lie_super_admin_service_tax_invoices` DISABLE KEYS */;
INSERT INTO `lie_super_admin_service_tax_invoices` VALUES (1,2,'Inv36341639E',7480,4595,10,2609,6,1232,7,2016,'1',2,0,'2016-08-16 11:03:52','2016-08-16 11:03:52'),(2,5,'Inv36341639G',0,0,10,13565,0,-13565,7,2016,'1',2,0,'2016-08-16 11:06:33','2016-08-16 11:06:33'),(3,1,'Inv36341639D',600,600,10,4106,82,-3648,7,2016,'1',2,0,'2016-08-16 11:13:50','2016-08-16 11:13:50'),(4,15,'Inv36350134D',437,14,10,0,9,-39,8,2016,'1',2,0,'2016-09-01 06:52:04','2016-09-01 06:52:04'),(5,1,'Inv36350333H',1037,445,10,1660,7,-1326,8,2016,'1',2,0,'2016-09-03 05:24:33','2016-09-03 05:24:33'),(6,5,'Inv36350740I',0,0,8,3685,0,-3685,8,2016,'1',2,0,'2016-09-07 12:17:53','2016-09-07 12:17:53');
/*!40000 ALTER TABLE `lie_super_admin_service_tax_invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_super_admin_settings`
--

DROP TABLE IF EXISTS `lie_super_admin_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_super_admin_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cashback_point_value` bigint(20) NOT NULL,
  `bonus_point_value` bigint(20) NOT NULL,
  `service_tax` float NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_super_admin_settings`
--

LOCK TABLES `lie_super_admin_settings` WRITE;
/*!40000 ALTER TABLE `lie_super_admin_settings` DISABLE KEYS */;
INSERT INTO `lie_super_admin_settings` VALUES (1,100,100,8,'1',9,2,'2016-07-19 00:00:00','2016-09-03 05:25:54');
/*!40000 ALTER TABLE `lie_super_admin_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_superadmin_login_logs`
--

DROP TABLE IF EXISTS `lie_superadmin_login_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_superadmin_login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_superadmin_login_logs`
--

LOCK TABLES `lie_superadmin_login_logs` WRITE;
/*!40000 ALTER TABLE `lie_superadmin_login_logs` DISABLE KEYS */;
INSERT INTO `lie_superadmin_login_logs` VALUES (1,2,'L','180.151.7.42','2016-07-19 15:58:16',2,2,'2016-07-19 15:58:16','2016-07-19 15:58:16','1'),(2,2,'L','180.151.7.42','2016-07-19 16:06:16',2,2,'2016-07-19 16:06:16','2016-07-19 16:06:16','1'),(3,2,'L','180.151.7.42','2016-07-19 16:07:32',2,2,'2016-07-19 16:07:32','2016-07-19 16:07:32','1'),(4,2,'L','180.151.7.42','2016-07-19 16:18:22',2,2,'2016-07-19 16:18:22','2016-07-19 16:18:22','1'),(5,2,'L','180.151.7.42','2016-07-19 16:18:59',2,2,'2016-07-19 16:18:59','2016-07-19 16:18:59','1'),(6,2,'L','180.151.7.42','2016-07-20 09:37:55',2,2,'2016-07-20 09:37:55','2016-07-20 09:37:55','1'),(7,2,'L','180.151.7.42','2016-07-20 12:53:41',2,2,'2016-07-20 12:53:41','2016-07-20 12:53:41','1'),(8,2,'L','180.151.7.42','2016-07-20 18:15:27',2,2,'2016-07-20 18:15:27','2016-07-20 18:15:27','1'),(9,2,'L','180.151.7.42','2016-07-20 18:54:13',2,2,'2016-07-20 18:54:13','2016-07-20 18:54:13','1'),(10,2,'O','180.151.7.42','2016-07-20 19:09:13',2,2,'2016-07-20 19:09:13','2016-07-20 19:09:13','1'),(11,2,'L','180.151.7.42','2016-07-21 11:21:44',2,2,'2016-07-21 11:21:44','2016-07-21 11:21:44','1'),(12,2,'L','180.151.7.42','2016-07-21 12:45:20',2,2,'2016-07-21 12:45:20','2016-07-21 12:45:20','1'),(13,2,'L','180.151.7.42','2016-07-21 13:27:35',2,2,'2016-07-21 13:27:35','2016-07-21 13:27:35','1'),(14,2,'L','180.151.7.42','2016-07-21 15:44:38',2,2,'2016-07-21 15:44:38','2016-07-21 15:44:38','1'),(15,2,'L','180.151.7.42','2016-07-21 16:33:40',2,2,'2016-07-21 16:33:40','2016-07-21 16:33:40','1'),(16,2,'O','180.151.7.42','2016-07-21 17:57:27',2,2,'2016-07-21 17:57:27','2016-07-21 17:57:27','1'),(17,2,'L','80.110.92.176','2016-07-22 07:46:54',2,2,'2016-07-22 07:46:54','2016-07-22 07:46:54','1'),(18,2,'L','180.151.7.42','2016-07-22 13:52:12',2,2,'2016-07-22 13:52:12','2016-07-22 13:52:12','1'),(19,2,'L','180.151.7.42','2016-07-23 11:11:42',2,2,'2016-07-23 11:11:42','2016-07-23 11:11:42','1'),(20,8,'L','80.110.92.176','2016-07-25 04:31:15',8,8,'2016-07-25 04:31:15','2016-07-25 04:31:15','1'),(21,2,'L','180.151.7.42','2016-07-25 12:21:37',2,2,'2016-07-25 12:21:37','2016-07-25 12:21:37','1'),(22,2,'L','180.151.7.42','2016-07-25 16:45:49',2,2,'2016-07-25 16:45:49','2016-07-25 16:45:49','1'),(23,2,'L','180.151.7.42','2016-07-26 10:43:11',2,2,'2016-07-26 10:43:11','2016-07-26 10:43:11','1'),(24,2,'L','180.151.7.42','2016-07-26 16:58:41',2,2,'2016-07-26 16:58:41','2016-07-26 16:58:41','1'),(25,2,'L','180.151.7.42','2016-07-26 18:02:26',2,2,'2016-07-26 18:02:26','2016-07-26 18:02:26','1'),(26,2,'L','180.151.7.42','2016-07-26 18:50:02',2,2,'2016-07-26 18:50:02','2016-07-26 18:50:02','1'),(27,2,'O','80.110.92.176','2016-07-27 05:41:23',2,2,'2016-07-27 05:41:23','2016-07-27 05:41:23','1'),(28,2,'L','80.110.92.176','2016-07-27 05:42:11',2,2,'2016-07-27 05:42:11','2016-07-27 05:42:11','1'),(29,2,'O','80.110.92.176','2016-07-27 05:45:11',2,2,'2016-07-27 05:45:11','2016-07-27 05:45:11','1'),(30,8,'L','80.110.92.176','2016-07-27 05:45:20',8,8,'2016-07-27 05:45:20','2016-07-27 05:45:20','1'),(31,2,'L','182.74.164.213','2016-07-27 11:05:13',2,2,'2016-07-27 11:05:13','2016-07-27 11:05:13','1'),(32,2,'L','180.151.7.43','2016-07-27 11:13:55',2,2,'2016-07-27 11:13:55','2016-07-27 11:13:55','1'),(33,2,'L','180.151.7.43','2016-07-27 11:14:34',2,2,'2016-07-27 11:14:34','2016-07-27 11:14:34','1'),(34,2,'O','180.151.7.42','2016-07-27 16:19:15',2,2,'2016-07-27 16:19:15','2016-07-27 16:19:15','1'),(35,2,'L','180.151.7.42','2016-07-27 19:11:25',2,2,'2016-07-27 19:11:25','2016-07-27 19:11:25','1'),(36,2,'L','180.151.7.42','2016-07-28 09:41:05',2,2,'2016-07-28 09:41:05','2016-07-28 09:41:05','1'),(37,2,'L','180.151.7.42','2016-07-28 11:12:06',2,2,'2016-07-28 11:12:06','2016-07-28 11:12:06','1'),(38,2,'L','180.151.7.42','2016-07-29 10:36:06',2,2,'2016-07-29 10:36:06','2016-07-29 10:36:06','1'),(39,2,'L','180.151.7.42','2016-07-29 10:36:17',2,2,'2016-07-29 10:36:17','2016-07-29 10:36:17','1'),(40,2,'L','180.151.7.42','2016-07-29 13:08:19',2,2,'2016-07-29 13:08:19','2016-07-29 13:08:19','1'),(41,2,'O','180.151.7.42','2016-07-29 15:12:20',2,2,'2016-07-29 15:12:20','2016-07-29 15:12:20','1'),(42,2,'L','180.151.7.42','2016-07-29 16:51:04',2,2,'2016-07-29 16:51:04','2016-07-29 16:51:04','1'),(43,2,'L','80.110.92.176','2016-07-30 04:45:06',2,2,'2016-07-30 04:45:06','2016-07-30 04:45:06','1'),(44,2,'L','180.151.7.42','2016-07-30 10:33:15',2,2,'2016-07-30 10:33:15','2016-07-30 10:33:15','1'),(45,2,'L','180.151.7.42','2016-07-30 16:17:30',2,2,'2016-07-30 16:17:30','2016-07-30 16:17:30','1'),(46,2,'L','180.151.7.42','2016-08-01 09:57:45',2,2,'2016-08-01 09:57:45','2016-08-01 09:57:45','1'),(47,2,'L','180.151.7.42','2016-08-01 11:27:38',2,2,'2016-08-01 11:27:38','2016-08-01 11:27:38','1'),(48,2,'L','180.151.7.42','2016-08-01 13:55:08',2,2,'2016-08-01 13:55:08','2016-08-01 13:55:08','1'),(49,2,'L','180.151.7.42','2016-08-01 17:43:40',2,2,'2016-08-01 17:43:40','2016-08-01 17:43:40','1'),(50,2,'L','180.151.7.42','2016-08-02 09:35:26',2,2,'2016-08-02 09:35:26','2016-08-02 09:35:26','1'),(51,2,'L','180.151.7.42','2016-08-02 10:26:19',2,2,'2016-08-02 10:26:19','2016-08-02 10:26:19','1'),(52,2,'O','180.151.7.42','2016-08-02 11:02:05',2,2,'2016-08-02 11:02:05','2016-08-02 11:02:05','1'),(53,2,'L','180.151.7.42','2016-08-02 11:02:12',2,2,'2016-08-02 11:02:12','2016-08-02 11:02:12','1'),(54,2,'L','180.151.7.42','2016-08-02 11:09:30',2,2,'2016-08-02 11:09:30','2016-08-02 11:09:30','1'),(55,2,'O','180.151.7.42','2016-08-02 11:09:45',2,2,'2016-08-02 11:09:45','2016-08-02 11:09:45','1'),(56,2,'L','180.151.7.42','2016-08-02 11:09:58',2,2,'2016-08-02 11:09:58','2016-08-02 11:09:58','1'),(57,2,'L','180.151.7.42','2016-08-02 11:34:07',2,2,'2016-08-02 11:34:07','2016-08-02 11:34:07','1'),(58,2,'L','180.151.7.42','2016-08-02 12:29:32',2,2,'2016-08-02 12:29:32','2016-08-02 12:29:32','1'),(59,2,'L','180.151.7.42','2016-08-03 10:16:46',2,2,'2016-08-03 10:16:46','2016-08-03 10:16:46','1'),(60,2,'L','180.151.7.42','2016-08-03 10:26:04',2,2,'2016-08-03 10:26:04','2016-08-03 10:26:04','1'),(61,2,'L','180.151.7.42','2016-08-03 10:32:52',2,2,'2016-08-03 10:32:52','2016-08-03 10:32:52','1'),(62,2,'L','180.151.7.42','2016-08-04 10:47:53',2,2,'2016-08-04 10:47:53','2016-08-04 10:47:53','1'),(63,2,'L','180.151.7.42','2016-08-08 10:49:33',2,2,'2016-08-08 10:49:33','2016-08-08 10:49:33','1'),(64,2,'L','180.151.7.42','2016-08-08 19:00:24',2,2,'2016-08-08 19:00:24','2016-08-08 19:00:24','1'),(65,2,'L','180.151.7.42','2016-08-09 11:47:33',2,2,'2016-08-09 11:47:33','2016-08-09 11:47:33','1'),(66,2,'L','180.151.7.42','2016-08-09 14:47:27',2,2,'2016-08-09 14:47:27','2016-08-09 14:47:27','1'),(67,2,'O','80.110.89.221','2016-08-10 09:08:45',2,2,'2016-08-10 09:08:45','2016-08-10 09:08:45','1'),(68,2,'L','80.110.89.221','2016-08-10 09:09:51',2,2,'2016-08-10 09:09:51','2016-08-10 09:09:51','1'),(69,2,'O','80.110.89.221','2016-08-10 09:11:08',2,2,'2016-08-10 09:11:08','2016-08-10 09:11:08','1'),(70,2,'L','80.110.89.221','2016-08-10 09:11:44',2,2,'2016-08-10 09:11:44','2016-08-10 09:11:44','1'),(71,2,'O','80.110.89.221','2016-08-10 09:15:38',2,2,'2016-08-10 09:15:38','2016-08-10 09:15:38','1'),(72,2,'L','80.110.89.221','2016-08-10 09:16:17',2,2,'2016-08-10 09:16:17','2016-08-10 09:16:17','1'),(73,2,'L','180.151.7.42','2016-08-10 10:10:04',2,2,'2016-08-10 10:10:04','2016-08-10 10:10:04','1'),(74,2,'L','180.151.7.42','2016-08-10 10:45:55',2,2,'2016-08-10 10:45:55','2016-08-10 10:45:55','1'),(75,2,'L','180.151.7.42','2016-08-10 11:50:51',2,2,'2016-08-10 11:50:51','2016-08-10 11:50:51','1'),(76,2,'L','180.151.7.42','2016-08-10 17:24:31',2,2,'2016-08-10 17:24:31','2016-08-10 17:24:31','1'),(77,2,'L','180.151.7.42','2016-08-10 17:26:32',2,2,'2016-08-10 17:26:32','2016-08-10 17:26:32','1'),(78,2,'L','180.151.7.42','2016-08-10 17:28:12',2,2,'2016-08-10 17:28:12','2016-08-10 17:28:12','1'),(79,2,'L','180.151.7.42','2016-08-10 17:28:18',2,2,'2016-08-10 17:28:18','2016-08-10 17:28:18','1'),(80,2,'L','180.151.7.42','2016-08-10 17:33:06',2,2,'2016-08-10 17:33:06','2016-08-10 17:33:06','1'),(81,2,'L','180.151.7.42','2016-08-10 17:50:41',2,2,'2016-08-10 17:50:41','2016-08-10 17:50:41','1'),(82,2,'L','80.110.89.221','2016-08-11 02:11:24',2,2,'2016-08-11 02:11:24','2016-08-11 02:11:24','1'),(83,2,'L','180.151.7.42','2016-08-11 09:27:03',2,2,'2016-08-11 09:27:03','2016-08-11 09:27:03','1'),(84,2,'L','180.151.7.42','2016-08-11 09:42:32',2,2,'2016-08-11 09:42:32','2016-08-11 09:42:32','1'),(85,2,'L','180.151.7.42','2016-08-11 09:56:40',2,2,'2016-08-11 09:56:40','2016-08-11 09:56:40','1'),(86,2,'L','180.151.7.42','2016-08-11 10:05:39',2,2,'2016-08-11 10:05:39','2016-08-11 10:05:39','1'),(87,2,'L','180.151.7.42','2016-08-12 09:25:47',2,2,'2016-08-12 09:25:47','2016-08-12 09:25:47','1'),(88,2,'L','180.151.7.42','2016-08-12 09:49:16',2,2,'2016-08-12 09:49:16','2016-08-12 09:49:16','1'),(89,2,'L','180.151.7.42','2016-08-12 10:06:00',2,2,'2016-08-12 10:06:00','2016-08-12 10:06:00','1'),(90,2,'L','80.110.89.221','2016-08-13 04:24:35',2,2,'2016-08-13 04:24:35','2016-08-13 04:24:35','1'),(91,2,'L','180.151.7.42','2016-08-13 09:36:19',2,2,'2016-08-13 09:36:19','2016-08-13 09:36:19','1'),(92,2,'L','180.151.7.42','2016-08-13 09:50:25',2,2,'2016-08-13 09:50:25','2016-08-13 09:50:25','1'),(93,2,'L','180.151.7.42','2016-08-13 11:48:09',2,2,'2016-08-13 11:48:09','2016-08-13 11:48:09','1'),(94,2,'L','80.110.89.221','2016-08-15 05:21:41',2,2,'2016-08-15 05:21:41','2016-08-15 05:21:41','1'),(95,2,'L','180.151.7.42','2016-08-16 09:41:53',2,2,'2016-08-16 09:41:53','2016-08-16 09:41:53','1'),(96,2,'L','180.151.7.42','2016-08-16 09:43:39',2,2,'2016-08-16 09:43:39','2016-08-16 09:43:39','1'),(97,2,'L','180.151.7.42','2016-08-16 10:28:47',2,2,'2016-08-16 10:28:47','2016-08-16 10:28:47','1'),(98,2,'L','180.151.7.43','2016-08-17 11:49:04',2,2,'2016-08-17 11:49:04','2016-08-17 11:49:04','1'),(99,2,'L','180.151.7.43','2016-08-17 15:15:00',2,2,'2016-08-17 15:15:00','2016-08-17 15:15:00','1'),(100,2,'L','180.151.7.43','2016-08-18 11:47:55',2,2,'2016-08-18 11:47:55','2016-08-18 11:47:55','1'),(101,2,'L','180.151.7.43','2016-08-18 12:20:43',2,2,'2016-08-18 12:20:43','2016-08-18 12:20:43','1'),(102,2,'O','180.151.7.43','2016-08-18 17:48:36',2,2,'2016-08-18 17:48:36','2016-08-18 17:48:36','1'),(103,2,'L','180.151.7.43','2016-08-19 11:04:39',2,2,'2016-08-19 11:04:39','2016-08-19 11:04:39','1'),(104,2,'L','180.151.7.43','2016-08-19 17:03:50',2,2,'2016-08-19 17:03:50','2016-08-19 17:03:50','1'),(105,2,'L','180.151.7.43','2016-08-22 09:26:09',2,2,'2016-08-22 09:26:09','2016-08-22 09:26:09','1'),(106,2,'L','180.151.7.43','2016-08-22 13:28:30',2,2,'2016-08-22 13:28:30','2016-08-22 13:28:30','1'),(107,2,'L','180.151.7.43','2016-08-22 13:41:49',2,2,'2016-08-22 13:41:49','2016-08-22 13:41:49','1'),(108,2,'L','180.151.7.43','2016-08-22 13:49:06',2,2,'2016-08-22 13:49:06','2016-08-22 13:49:06','1'),(109,2,'L','180.151.7.43','2016-08-23 09:50:01',2,2,'2016-08-23 09:50:01','2016-08-23 09:50:01','1'),(110,2,'L','180.151.7.43','2016-08-23 10:26:19',2,2,'2016-08-23 10:26:19','2016-08-23 10:26:19','1'),(111,2,'L','180.151.7.43','2016-08-23 11:08:21',2,2,'2016-08-23 11:08:21','2016-08-23 11:08:21','1'),(112,2,'L','180.151.7.43','2016-08-23 16:20:13',2,2,'2016-08-23 16:20:13','2016-08-23 16:20:13','1'),(113,2,'L','180.151.7.43','2016-08-23 16:31:31',2,2,'2016-08-23 16:31:31','2016-08-23 16:31:31','1'),(114,2,'L','180.151.7.43','2016-08-24 10:58:23',2,2,'2016-08-24 10:58:23','2016-08-24 10:58:23','1'),(115,2,'O','180.151.7.43','2016-08-24 11:27:46',2,2,'2016-08-24 11:27:46','2016-08-24 11:27:46','1'),(116,2,'L','180.151.7.43','2016-08-24 11:48:09',2,2,'2016-08-24 11:48:09','2016-08-24 11:48:09','1'),(117,2,'L','180.151.7.43','2016-08-24 11:59:58',2,2,'2016-08-24 11:59:58','2016-08-24 11:59:58','1'),(118,2,'O','180.151.7.43','2016-08-24 13:26:58',2,2,'2016-08-24 13:26:58','2016-08-24 13:26:58','1'),(119,2,'L','180.151.7.43','2016-08-25 09:58:12',2,2,'2016-08-25 09:58:12','2016-08-25 09:58:12','1'),(120,2,'L','83.65.71.26','2016-08-25 13:55:21',2,2,'2016-08-25 13:55:21','2016-08-25 13:55:21','1'),(121,2,'L','89.144.225.47','2016-08-26 00:33:59',2,2,'2016-08-26 00:33:59','2016-08-26 00:33:59','1'),(122,2,'L','180.151.7.43','2016-08-26 09:32:04',2,2,'2016-08-26 09:32:04','2016-08-26 09:32:04','1'),(123,2,'L','180.151.7.43','2016-08-26 12:28:03',2,2,'2016-08-26 12:28:03','2016-08-26 12:28:03','1'),(124,2,'L','180.151.7.43','2016-08-26 16:33:55',2,2,'2016-08-26 16:33:55','2016-08-26 16:33:55','1'),(125,2,'L','80.110.92.92','2016-08-27 04:45:03',2,2,'2016-08-27 04:45:03','2016-08-27 04:45:03','1'),(126,2,'L','180.151.7.43','2016-08-27 09:56:04',2,2,'2016-08-27 09:56:04','2016-08-27 09:56:04','1'),(127,2,'L','180.151.7.42','2016-08-27 12:43:00',2,2,'2016-08-27 12:43:00','2016-08-27 12:43:00','1'),(128,2,'L','213.47.175.30','2016-08-27 13:44:38',2,2,'2016-08-27 13:44:38','2016-08-27 13:44:38','1'),(129,2,'L','180.151.7.42','2016-08-27 15:18:16',2,2,'2016-08-27 15:18:16','2016-08-27 15:18:16','1'),(130,2,'L','180.151.7.42','2016-08-29 09:39:35',2,2,'2016-08-29 09:39:35','2016-08-29 09:39:35','1'),(131,2,'L','180.151.7.42','2016-08-29 16:22:39',2,2,'2016-08-29 16:22:39','2016-08-29 16:22:39','1'),(132,2,'L','80.110.92.92','2016-08-30 05:02:00',2,2,'2016-08-30 05:02:00','2016-08-30 05:02:00','1'),(133,2,'L','180.151.7.42','2016-08-30 09:31:29',2,2,'2016-08-30 09:31:29','2016-08-30 09:31:29','1'),(134,2,'L','180.151.7.42','2016-08-30 15:11:51',2,2,'2016-08-30 15:11:51','2016-08-30 15:11:51','1'),(135,2,'L','180.151.7.42','2016-08-30 15:25:45',2,2,'2016-08-30 15:25:45','2016-08-30 15:25:45','1'),(136,2,'L','180.151.7.42','2016-08-30 18:09:40',2,2,'2016-08-30 18:09:40','2016-08-30 18:09:40','1'),(137,2,'L','180.151.7.42','2016-08-30 18:28:53',2,2,'2016-08-30 18:28:53','2016-08-30 18:28:53','1'),(138,2,'L','80.110.92.92','2016-08-31 00:28:32',2,2,'2016-08-31 00:28:32','2016-08-31 00:28:32','1'),(139,2,'L','80.110.92.92','2016-08-31 01:33:56',2,2,'2016-08-31 01:33:56','2016-08-31 01:33:56','1'),(140,2,'L','180.151.7.42','2016-08-31 09:47:25',2,2,'2016-08-31 09:47:25','2016-08-31 09:47:25','1'),(141,2,'L','180.151.7.42','2016-08-31 10:55:00',2,2,'2016-08-31 10:55:00','2016-08-31 10:55:00','1'),(142,2,'L','180.151.7.42','2016-08-31 12:14:03',2,2,'2016-08-31 12:14:03','2016-08-31 12:14:03','1'),(143,2,'L','180.151.7.42','2016-08-31 13:14:24',2,2,'2016-08-31 13:14:24','2016-08-31 13:14:24','1'),(144,2,'L','80.110.92.92','2016-09-01 06:51:23',2,2,'2016-09-01 06:51:23','2016-09-01 06:51:23','1'),(145,2,'L','213.47.175.30','2016-09-02 03:26:14',2,2,'2016-09-02 03:26:14','2016-09-02 03:26:14','1'),(146,2,'L','180.151.7.42','2016-09-02 09:39:48',2,2,'2016-09-02 09:39:48','2016-09-02 09:39:48','1'),(147,2,'O','80.110.92.92','2016-09-03 07:34:55',2,2,'2016-09-03 07:34:55','2016-09-03 07:34:55','1'),(148,2,'L','80.110.92.92','2016-09-03 07:37:04',2,2,'2016-09-03 07:37:04','2016-09-03 07:37:04','1'),(149,2,'O','80.110.92.92','2016-09-03 07:44:41',2,2,'2016-09-03 07:44:41','2016-09-03 07:44:41','1'),(150,8,'L','80.110.92.92','2016-09-03 07:45:03',8,8,'2016-09-03 07:45:03','2016-09-03 07:45:03','1'),(151,2,'L','180.151.7.43','2016-09-05 09:27:31',2,2,'2016-09-05 09:27:31','2016-09-05 09:27:31','1'),(152,2,'L','180.151.7.43','2016-09-05 10:45:55',2,2,'2016-09-05 10:45:55','2016-09-05 10:45:55','1'),(153,2,'L','180.151.7.43','2016-09-05 18:41:33',2,2,'2016-09-05 18:41:33','2016-09-05 18:41:33','1'),(154,2,'L','180.151.7.43','2016-09-06 10:18:25',2,2,'2016-09-06 10:18:25','2016-09-06 10:18:25','1'),(155,2,'L','180.151.7.43','2016-09-06 12:04:35',2,2,'2016-09-06 12:04:35','2016-09-06 12:04:35','1'),(156,2,'L','180.151.7.43','2016-09-07 10:30:14',2,2,'2016-09-07 10:30:14','2016-09-07 10:30:14','1'),(157,2,'L','213.47.175.30','2016-09-07 22:41:23',2,2,'2016-09-07 22:41:23','2016-09-07 22:41:23','1'),(158,2,'L','180.151.7.43','2016-09-08 10:40:45',2,2,'2016-09-08 10:40:45','2016-09-08 10:40:45','1'),(159,2,'L','180.151.7.43','2016-09-08 12:42:35',2,2,'2016-09-08 12:42:35','2016-09-08 12:42:35','1'),(160,2,'L','180.151.7.43','2016-09-09 10:22:41',2,2,'2016-09-09 10:22:41','2016-09-09 10:22:41','1'),(161,2,'L','180.151.7.43','2016-09-09 11:33:21',2,2,'2016-09-09 11:33:21','2016-09-09 11:33:21','1'),(162,2,'L','180.151.7.43','2016-09-09 18:15:50',2,2,'2016-09-09 18:15:50','2016-09-09 18:15:50','1'),(163,2,'L','180.151.7.43','2016-09-09 18:17:29',2,2,'2016-09-09 18:17:29','2016-09-09 18:17:29','1'),(164,2,'L','180.151.7.43','2016-09-10 10:18:53',2,2,'2016-09-10 10:18:53','2016-09-10 10:18:53','1'),(165,2,'L','180.151.7.43','2016-09-12 09:50:03',2,2,'2016-09-12 09:50:03','2016-09-12 09:50:03','1'),(166,2,'L','213.47.175.30','2016-09-12 12:51:20',2,2,'2016-09-12 12:51:20','2016-09-12 12:51:20','1'),(167,2,'L','80.110.92.92','2016-09-15 05:32:48',2,2,'2016-09-15 05:32:48','2016-09-15 05:32:48','1'),(168,2,'L','80.110.92.92','2016-09-16 05:32:40',2,2,'2016-09-16 05:32:40','2016-09-16 05:32:40','1'),(169,2,'L','80.110.92.92','2016-09-19 04:58:16',2,2,'2016-09-19 04:58:16','2016-09-19 04:58:16','1'),(170,2,'L','180.151.7.43','2016-09-19 11:36:10',2,2,'2016-09-19 11:36:10','2016-09-19 11:36:10','1'),(171,2,'O','180.151.7.43','2016-09-19 11:36:19',2,2,'2016-09-19 11:36:19','2016-09-19 11:36:19','1'),(172,2,'O','180.151.7.43','2016-09-19 11:38:22',2,2,'2016-09-19 11:38:22','2016-09-19 11:38:22','1'),(173,2,'L','213.47.175.30','2016-09-20 00:09:06',2,2,'2016-09-20 00:09:06','2016-09-20 00:09:06','1'),(174,2,'L','80.110.92.92','2016-09-20 07:08:39',2,2,'2016-09-20 07:08:39','2016-09-20 07:08:39','1'),(175,2,'L','80.110.92.92','2016-09-26 04:36:08',2,2,'2016-09-26 04:36:08','2016-09-26 04:36:08','1'),(176,2,'L','80.110.92.92','2016-09-30 06:32:18',2,2,'2016-09-30 06:32:18','2016-09-30 06:32:18','1'),(177,2,'L','213.47.175.30','2016-10-04 04:39:48',2,2,'2016-10-04 04:39:48','2016-10-04 04:39:48','1'),(178,2,'L','::1','2016-11-28 10:55:16',2,2,'2016-11-28 10:55:16','2016-11-28 10:55:16','1'),(179,2,'L','::1','2016-11-28 23:51:15',2,2,'2016-11-28 23:51:15','2016-11-28 23:51:15','1'),(180,2,'L','::1','2016-11-29 21:11:22',2,2,'2016-11-29 21:11:22','2016-11-29 21:11:22','1'),(181,2,'L','::1','2016-12-24 16:45:12',2,2,'2016-12-24 16:45:12','2016-12-24 16:45:12','1'),(182,2,'L','::1','2016-12-31 14:07:18',2,2,'2016-12-31 14:07:18','2016-12-31 14:07:18','1');
/*!40000 ALTER TABLE `lie_superadmin_login_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_tax_settings`
--

DROP TABLE IF EXISTS `lie_tax_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_tax_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(50) NOT NULL,
  `tax_type` enum('p','f') NOT NULL DEFAULT 'p' COMMENT 'p->percentage, f->fixed',
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_tax_settings`
--

LOCK TABLES `lie_tax_settings` WRITE;
/*!40000 ALTER TABLE `lie_tax_settings` DISABLE KEYS */;
INSERT INTO `lie_tax_settings` VALUES (1,'vat','p','sdfdsf sdf ',2,2,'2016-07-20 11:38:51','2016-07-20 11:38:51','1'),(2,'sasdasdas','p','asdasdasd',2,2,'2016-07-20 11:39:01','2016-07-20 11:39:13','2'),(3,'service','f','service',2,2,'2016-07-20 12:19:05','2016-07-20 18:12:50','1');
/*!40000 ALTER TABLE `lie_tax_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_time_validations`
--

DROP TABLE IF EXISTS `lie_time_validations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_time_validations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `year` tinyint(11) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `week` tinyint(4) NOT NULL,
  `day` tinyint(4) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_time_validations`
--

LOCK TABLES `lie_time_validations` WRITE;
/*!40000 ALTER TABLE `lie_time_validations` DISABLE KEYS */;
INSERT INTO `lie_time_validations` VALUES (1,'1 year 2 month 5 days',1,2,0,5,'Testing By Rajlakshmi...','1',2,2,'2016-07-19 17:01:01','2016-07-20 10:46:06'),(2,'1 Year',1,0,0,0,'For 1 year','1',2,2,'2016-07-22 16:13:27','2016-09-07 11:24:26'),(3,'1 Year 2 month',1,2,0,0,'1 Year','1',2,2,'2016-07-27 15:09:48','2016-07-27 15:10:24'),(4,'1 year sfdsg',0,0,0,0,'1 year','1',2,2,'2016-08-10 17:37:04','2016-08-10 17:37:55');
/*!40000 ALTER TABLE `lie_time_validations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_bonus_point_logs`
--

DROP TABLE IF EXISTS `lie_user_bonus_point_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_bonus_point_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL DEFAULT '0',
  `creditordebit` enum('0','1') NOT NULL COMMENT 'o for debit,1 for credit',
  `amount` int(11) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_bonus_point_logs`
--

LOCK TABLES `lie_user_bonus_point_logs` WRITE;
/*!40000 ALTER TABLE `lie_user_bonus_point_logs` DISABLE KEYS */;
INSERT INTO `lie_user_bonus_point_logs` VALUES (1,1,1,'363222355305','1',50,'','1',9,9,'2016-07-12 00:00:00','0000-00-00 00:00:00'),(2,1,1,'363222355305','0',30,'','1',9,9,'2016-07-20 00:00:00','0000-00-00 00:00:00'),(3,1,1,'363320334211LR2','1',5,'you reviewed an order','1',0,0,'2016-08-01 16:21:29','2016-08-01 16:21:29'),(4,1,1,'363320334211LR2','1',5,'you reviewed an order','1',0,0,'2016-08-01 16:23:24','2016-08-01 16:23:24'),(5,1,1,'363320334211LR2','1',5,'you reviewed an order','1',0,0,'2016-08-01 16:28:01','2016-08-01 16:28:01'),(6,1,1,'363320334211LR2','1',5,'you reviewed an order','1',0,0,'2016-08-01 16:29:15','2016-08-01 16:29:15'),(7,1,1,'363320334211LR2','1',5,'you reviewed an order','1',0,0,'2016-08-01 16:31:21','2016-08-01 16:31:21'),(8,1,1,'363320334211LR2','1',5,'you reviewed an order','1',0,0,'2016-08-01 16:33:04','2016-08-01 16:33:04'),(9,3,1,'363401503850ZR1','1',5,'you reviewed an order','1',0,0,'2016-08-02 03:20:43','2016-08-02 03:20:43'),(10,3,1,'363401494705CR1','1',5,'you reviewed an order','1',0,0,'2016-08-02 03:21:17','2016-08-02 03:21:17'),(11,3,1,'363402315221LR1','1',5,'you reviewed an order','1',0,0,'2016-08-02 03:58:12','2016-08-02 03:58:12'),(12,3,1,'363329332536ZR1','1',5,'you reviewed an order','1',0,0,'2016-08-04 06:09:06','2016-08-04 06:09:06'),(13,3,1,'363324323153TR2','1',5,'you reviewed an order','1',0,0,'2016-08-04 06:09:33','2016-08-04 06:09:33'),(14,3,1,'363326323051YR2','1',5,'you reviewed an order','1',0,0,'2016-08-04 06:09:46','2016-08-04 06:09:46'),(15,3,1,'363326332729OR2','1',5,'you reviewed an order','1',0,0,'2016-08-04 06:10:04','2016-08-04 06:10:04'),(16,3,1,'363327325848WR2','1',5,'you reviewed an order','1',0,0,'2016-08-04 06:10:17','2016-08-04 06:10:17'),(17,3,1,'363327330014ER2','1',5,'you reviewed an order','1',0,0,'2016-08-04 06:10:33','2016-08-04 06:10:33'),(18,3,1,'363328330213JR2','1',5,'you reviewed an order','1',0,0,'2016-08-04 06:10:49','2016-08-04 06:10:49'),(19,3,1,'363405334239SR1','1',5,'you reviewed an order','1',0,0,'2016-08-05 05:46:29','2016-08-05 05:46:29'),(20,3,1,'363409340207FR1','1',5,'you reviewed an order','1',0,0,'2016-08-09 06:06:39','2016-08-09 06:06:39'),(21,4,1,'363321432009HR2','1',5,'you reviewed an order','1',0,0,'2016-08-10 17:46:38','2016-08-10 17:46:38'),(22,4,1,'363401442359PR1','1',5,'you reviewed an order','1',0,0,'2016-08-10 17:46:53','2016-08-10 17:46:53'),(23,3,1,'363412362905AR1','1',5,'you reviewed an order','1',0,0,'2016-08-12 08:32:56','2016-08-12 08:32:56'),(24,3,1,'363412362605OR1','1',5,'you reviewed an order','1',0,0,'2016-08-13 01:53:26','2016-08-13 01:53:26'),(25,3,1,'363323321816ZR2','1',5,'you reviewed an order','1',0,0,'2016-08-18 06:11:27','2016-08-18 06:11:27'),(26,3,1,'363322433618FR2','1',5,'you reviewed an order','1',0,0,'2016-08-21 04:25:15','2016-08-21 04:25:15'),(27,11,1,'363423412759TR17','1',5,'you reviewed an order','1',0,0,'2016-08-23 13:44:57','2016-08-23 13:44:57'),(28,11,1,'363422451004YR17','1',5,'you reviewed an order','1',0,0,'2016-08-23 13:45:10','2016-08-23 13:45:10'),(29,4,1,'363329391405AR1','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:22:04','2016-08-23 16:22:04'),(30,4,1,'363322382721TR2','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:22:35','2016-08-23 16:22:35'),(31,4,1,'363320470813OR2','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:23:06','2016-08-23 16:23:06'),(32,4,1,'363423401236DR17','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:25:07','2016-08-23 16:25:07'),(33,4,1,'363423394738GR17','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:25:49','2016-08-23 16:25:49'),(34,4,1,'363423394332HR17','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:26:30','2016-08-23 16:26:30'),(35,4,1,'363423443310ZR17','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:55:46','2016-08-23 16:55:46'),(36,4,1,'363423443310ZR17','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:55:46','2016-08-23 16:55:46'),(37,4,1,'363329393927DR1','1',5,'you reviewed an order','1',0,0,'2016-08-23 16:56:12','2016-08-23 16:56:12'),(38,4,1,'363329394045NR1','1',5,'you reviewed an order','1',0,0,'2016-08-23 17:00:19','2016-08-23 17:00:19'),(39,4,1,'363321432744YR2','1',5,'you reviewed an order','1',0,0,'2016-08-23 17:05:51','2016-08-23 17:05:51'),(40,4,1,'363329393802CR1','1',5,'you reviewed an order','1',0,0,'2016-08-23 17:06:06','2016-08-23 17:06:06'),(41,3,1,'363426332145SR15','1',5,'you reviewed an order','1',0,0,'2016-08-26 05:24:11','2016-08-26 05:24:11'),(42,11,1,'363426405855DR17','1',5,'you reviewed an order','1',0,0,'2016-08-27 11:39:14','2016-08-27 11:39:14'),(43,3,1,'363428322305JR15','1',5,'you reviewed an order','1',0,0,'2016-08-28 05:51:20','2016-08-28 05:51:20'),(44,3,1,'363429350103YR15','1',5,'you reviewed an order','1',0,0,'2016-09-03 08:03:27','2016-09-03 08:03:27'),(45,3,1,'363429345930IR15','1',5,'you reviewed an order','1',0,0,'2016-09-03 08:03:44','2016-09-03 08:03:44'),(46,3,1,'363502310112ER15','1',5,'you reviewed an order','1',0,0,'2016-09-03 08:04:14','2016-09-03 08:04:14'),(47,5,1,'363506393236OR5','1',5,'you reviewed an order','1',0,0,'2016-09-06 12:30:25','2016-09-06 12:30:25'),(48,5,1,'363506403409SR5','1',5,'you reviewed an order','1',0,0,'2016-09-06 12:35:43','2016-09-06 12:35:43'),(49,3,1,'363425431020YR15','1',5,'you reviewed an order','1',0,0,'2016-09-10 02:19:26','2016-09-10 02:19:26'),(50,3,1,'363511334553FR15','1',5,'you reviewed an order','1',0,0,'2016-09-12 12:14:04','2016-09-12 12:14:04'),(51,3,1,'363512441804FR15','1',5,'you reviewed an order','1',0,0,'2016-09-15 04:57:15','2016-09-15 04:57:15'),(52,3,1,'363515325428FR15','1',5,'you reviewed an order','1',0,0,'2016-09-20 05:26:43','2016-09-20 05:26:43'),(53,3,1,'363518292750GR15','1',5,'you reviewed an order','1',0,0,'2016-09-24 06:26:21','2016-09-24 06:26:21'),(54,3,1,'363604323156AR15','1',5,'you reviewed an order','1',0,0,'2016-10-04 04:37:14','2016-10-04 04:37:14'),(55,23,1,'363726294709AR15','1',5,'you reviewed an order','1',0,0,'2016-11-26 06:20:13','2016-11-26 06:20:13'),(56,23,1,'363831382659UR15','1',5,'you reviewed an order','1',0,0,'2016-12-31 21:11:43','2016-12-31 21:11:43');
/*!40000 ALTER TABLE `lie_user_bonus_point_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_bonus_points`
--

DROP TABLE IF EXISTS `lie_user_bonus_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_bonus_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `awarded_points` bigint(10) NOT NULL,
  `used_points` bigint(10) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_bonus_points`
--

LOCK TABLES `lie_user_bonus_points` WRITE;
/*!40000 ALTER TABLE `lie_user_bonus_points` DISABLE KEYS */;
INSERT INTO `lie_user_bonus_points` VALUES (1,1,67,67,'0',9,9,'2016-06-30 00:00:00','0000-00-00 00:00:00'),(2,1,67,10,'1',9,9,'2016-02-28 00:00:00','0000-00-00 00:00:00'),(3,1,67,0,'1',9,9,'2016-03-01 00:00:00','0000-00-00 00:00:00'),(4,2,67,0,'1',9,0,'2016-06-30 00:00:00','0000-00-00 00:00:00'),(5,1,5,0,'1',0,0,'2016-08-01 16:21:29','2016-08-01 16:21:29'),(6,1,5,0,'1',0,0,'2016-08-01 16:23:24','2016-08-01 16:23:24'),(7,1,5,0,'1',0,0,'2016-08-01 16:28:01','2016-08-01 16:28:01'),(8,1,5,0,'1',0,0,'2016-08-01 16:29:15','2016-08-01 16:29:15'),(9,1,5,0,'1',0,0,'2016-08-01 16:31:21','2016-08-01 16:31:21'),(10,1,5,0,'1',0,0,'2016-08-01 16:33:04','2016-08-01 16:33:04'),(11,3,5,0,'1',0,0,'2016-08-02 03:20:43','2016-08-02 03:20:43'),(12,3,5,0,'1',0,0,'2016-08-02 03:21:17','2016-08-02 03:21:17'),(13,3,5,0,'1',0,0,'2016-08-02 03:58:12','2016-08-02 03:58:12'),(14,3,5,0,'1',0,0,'2016-08-04 06:09:06','2016-08-04 06:09:06'),(15,3,5,0,'1',0,0,'2016-08-04 06:09:33','2016-08-04 06:09:33'),(16,3,5,0,'1',0,0,'2016-08-04 06:09:46','2016-08-04 06:09:46'),(17,3,5,0,'1',0,0,'2016-08-04 06:10:04','2016-08-04 06:10:04'),(18,3,5,0,'1',0,0,'2016-08-04 06:10:17','2016-08-04 06:10:17'),(19,3,5,0,'1',0,0,'2016-08-04 06:10:33','2016-08-04 06:10:33'),(20,3,5,0,'1',0,0,'2016-08-04 06:10:49','2016-08-04 06:10:49'),(21,3,5,0,'1',0,0,'2016-08-05 05:46:29','2016-08-05 05:46:29'),(22,3,5,0,'1',0,0,'2016-08-09 06:06:39','2016-08-09 06:06:39'),(23,4,5,0,'1',0,0,'2016-08-10 17:42:42','2016-08-10 17:42:42'),(24,4,5,0,'1',0,0,'2016-08-10 17:42:46','2016-08-10 17:42:46'),(25,4,5,0,'1',0,0,'2016-08-10 17:43:30','2016-08-10 17:43:30'),(26,4,5,0,'1',0,0,'2016-08-10 17:43:34','2016-08-10 17:43:34'),(27,4,5,0,'1',0,0,'2016-08-10 17:43:43','2016-08-10 17:43:43'),(28,4,5,0,'1',0,0,'2016-08-10 17:46:38','2016-08-10 17:46:38'),(29,4,5,0,'1',0,0,'2016-08-10 17:46:53','2016-08-10 17:46:53'),(30,3,5,0,'1',0,0,'2016-08-12 08:32:56','2016-08-12 08:32:56'),(31,3,5,0,'1',0,0,'2016-08-13 01:53:26','2016-08-13 01:53:26'),(32,3,5,0,'1',0,0,'2016-08-18 06:11:27','2016-08-18 06:11:27'),(33,3,5,0,'1',0,0,'2016-08-21 04:25:15','2016-08-21 04:25:15'),(34,11,5,0,'1',0,0,'2016-08-23 13:44:57','2016-08-23 13:44:57'),(35,11,5,0,'1',0,0,'2016-08-23 13:45:10','2016-08-23 13:45:10'),(36,4,5,0,'1',0,0,'2016-08-23 16:22:04','2016-08-23 16:22:04'),(37,4,5,0,'1',0,0,'2016-08-23 16:22:35','2016-08-23 16:22:35'),(38,4,5,0,'1',0,0,'2016-08-23 16:23:06','2016-08-23 16:23:06'),(39,4,5,0,'1',0,0,'2016-08-23 16:25:07','2016-08-23 16:25:07'),(40,4,5,0,'1',0,0,'2016-08-23 16:25:49','2016-08-23 16:25:49'),(41,4,5,0,'1',0,0,'2016-08-23 16:26:30','2016-08-23 16:26:30'),(42,4,5,0,'1',0,0,'2016-08-23 16:55:46','2016-08-23 16:55:46'),(43,4,5,0,'1',0,0,'2016-08-23 16:55:46','2016-08-23 16:55:46'),(44,4,5,0,'1',0,0,'2016-08-23 16:56:12','2016-08-23 16:56:12'),(45,4,5,0,'1',0,0,'2016-08-23 17:00:19','2016-08-23 17:00:19'),(46,4,5,0,'1',0,0,'2016-08-23 17:05:51','2016-08-23 17:05:51'),(47,4,5,0,'1',0,0,'2016-08-23 17:06:06','2016-08-23 17:06:06'),(48,3,5,0,'1',0,0,'2016-08-26 05:24:11','2016-08-26 05:24:11'),(49,11,5,0,'1',0,0,'2016-08-27 11:39:14','2016-08-27 11:39:14'),(50,3,5,0,'1',0,0,'2016-08-28 05:51:20','2016-08-28 05:51:20'),(51,3,5,0,'1',0,0,'2016-09-03 08:03:27','2016-09-03 08:03:27'),(52,3,5,0,'1',0,0,'2016-09-03 08:03:44','2016-09-03 08:03:44'),(53,3,5,0,'1',0,0,'2016-09-03 08:04:14','2016-09-03 08:04:14'),(54,5,5,0,'1',0,0,'2016-09-06 12:30:25','2016-09-06 12:30:25'),(55,5,5,0,'1',0,0,'2016-09-06 12:35:43','2016-09-06 12:35:43'),(56,3,5,0,'1',0,0,'2016-09-10 02:19:26','2016-09-10 02:19:26'),(57,3,5,0,'1',0,0,'2016-09-12 12:14:04','2016-09-12 12:14:04'),(58,3,5,0,'1',0,0,'2016-09-15 04:57:15','2016-09-15 04:57:15'),(59,3,5,0,'1',0,0,'2016-09-20 05:26:43','2016-09-20 05:26:43'),(60,3,5,0,'1',0,0,'2016-09-24 06:26:21','2016-09-24 06:26:21'),(61,3,5,0,'1',0,0,'2016-10-04 04:37:14','2016-10-04 04:37:14'),(62,23,5,0,'1',0,0,'2016-11-26 06:20:13','2016-11-26 06:20:13'),(63,23,5,0,'1',0,0,'2016-12-31 21:11:43','2016-12-31 21:11:43');
/*!40000 ALTER TABLE `lie_user_bonus_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_cashback_point_logs`
--

DROP TABLE IF EXISTS `lie_user_cashback_point_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_cashback_point_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL DEFAULT '0',
  `creditordebit` enum('0','1') NOT NULL COMMENT 'o for debit,1 for credit',
  `amount` int(11) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_cashback_point_logs`
--

LOCK TABLES `lie_user_cashback_point_logs` WRITE;
/*!40000 ALTER TABLE `lie_user_cashback_point_logs` DISABLE KEYS */;
INSERT INTO `lie_user_cashback_point_logs` VALUES (1,1,1,'363222355305','1',50,'','1',9,9,'2016-07-19 00:00:00','0000-00-00 00:00:00'),(2,1,1,'363222355305','0',20,'','1',9,9,'2016-07-19 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `lie_user_cashback_point_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_cashback_points`
--

DROP TABLE IF EXISTS `lie_user_cashback_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_cashback_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `set_cashback_point_id` int(11) NOT NULL,
  `awarded_points` bigint(10) NOT NULL,
  `used_points` bigint(10) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_cashback_points`
--

LOCK TABLES `lie_user_cashback_points` WRITE;
/*!40000 ALTER TABLE `lie_user_cashback_points` DISABLE KEYS */;
INSERT INTO `lie_user_cashback_points` VALUES (1,1,1,50,50,'','0',9,0,'2016-06-30 00:00:00','0000-00-00 00:00:00'),(2,1,1,50,0,'','1',9,0,'2016-03-27 00:00:00','0000-00-00 00:00:00'),(3,1,2,70,0,'','1',9,0,'2016-06-30 00:00:00','0000-00-00 00:00:00'),(4,2,2,70,0,'','1',9,0,'2016-06-30 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `lie_user_cashback_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_contact_maps`
--

DROP TABLE IF EXISTS `lie_user_contact_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_contact_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_contact_maps`
--

LOCK TABLES `lie_user_contact_maps` WRITE;
/*!40000 ALTER TABLE `lie_user_contact_maps` DISABLE KEYS */;
INSERT INTO `lie_user_contact_maps` VALUES (1,8,'066488399491',2,0,'2016-07-20 05:36:13','2016-07-20 05:36:13',1,'2'),(2,8,'01/252252',2,0,'2016-07-20 05:36:13','2016-07-20 05:36:13',0,'1'),(3,9,'9026484405',2,0,'2016-08-01 13:10:15','2016-08-01 13:10:15',1,'1'),(4,9,'129-1234',2,0,'2016-08-01 13:10:15','2016-08-01 13:10:15',0,'1'),(5,10,'9026484405',2,0,'2016-08-01 13:58:20','2016-08-01 13:58:20',1,'1'),(6,10,'90807060',2,0,'2016-08-01 13:58:20','2016-08-01 13:58:20',0,'1'),(7,11,'9999987654',2,0,'2016-08-01 15:47:59','2016-08-01 15:47:59',1,'1'),(8,11,'9080706050',2,0,'2016-08-01 15:47:59','2016-08-01 15:47:59',0,'1'),(9,12,'9876543212',2,0,'2016-08-01 15:56:43','2016-08-01 15:56:43',1,'1'),(10,12,'1234567890',2,0,'2016-08-01 15:56:43','2016-08-01 15:56:43',0,'1'),(11,13,'9656565656',2,0,'2016-08-01 16:37:39','2016-08-01 16:37:39',1,'1'),(12,13,'',2,0,'2016-08-01 16:37:39','2016-08-01 16:37:39',0,'1'),(13,14,'9012345678',2,0,'2016-08-01 16:41:43','2016-08-01 16:41:43',1,'1'),(14,14,'9087654321',2,0,'2016-08-01 16:41:43','2016-08-01 16:41:43',0,'1'),(15,15,'9876543212',2,0,'2016-08-01 16:44:37','2016-08-01 16:44:37',1,'1'),(16,15,'1234567890',2,0,'2016-08-01 16:44:37','2016-08-01 16:44:37',0,'1'),(17,16,'23232323423',2,0,'2016-08-01 16:48:07','2016-08-01 16:48:07',1,'1'),(18,16,'22234234324',2,0,'2016-08-01 16:48:07','2016-08-01 16:48:07',0,'1'),(19,17,'9656565656',2,0,'2016-08-01 17:26:10','2016-08-01 17:26:10',1,'1'),(20,17,'',2,0,'2016-08-01 17:26:10','2016-08-01 17:26:10',0,'1'),(21,18,'9656565656',2,0,'2016-08-01 17:49:43','2016-08-01 17:49:43',1,'1'),(22,18,'',2,0,'2016-08-01 17:49:43','2016-08-01 17:49:43',0,'1'),(23,19,'1234567890',2,0,'2016-08-01 17:53:50','2016-08-01 17:53:50',1,'1'),(24,19,'1234567890',2,0,'2016-08-01 17:53:50','2016-08-01 17:53:50',0,'1'),(25,8,'06644975080',0,2,'2016-08-10 09:08:03','2016-08-10 09:08:03',1,'1');
/*!40000 ALTER TABLE `lie_user_contact_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_email_maps`
--

DROP TABLE IF EXISTS `lie_user_email_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_email_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_email_maps`
--

LOCK TABLES `lie_user_email_maps` WRITE;
/*!40000 ALTER TABLE `lie_user_email_maps` DISABLE KEYS */;
INSERT INTO `lie_user_email_maps` VALUES (1,8,'info@lieferzonas.at',2,0,'2016-07-20 05:36:13','2016-07-20 05:36:13',1,'2'),(2,8,'office@lieferzonas.at',2,0,'2016-07-20 05:36:13','2016-07-20 05:36:13',0,'2'),(3,8,'21324@lieferzonas.at',0,2,'2016-07-27 11:14:57','2016-07-27 11:14:57',1,'2'),(4,8,'raj@lieferzonas.at',0,2,'2016-07-27 11:15:59','2016-07-27 11:15:59',1,'2'),(5,8,'!123456@lieferzonas.at',0,2,'2016-07-27 11:35:01','2016-07-27 11:35:01',1,'2'),(6,8,'123456@lieferzonas.at',0,2,'2016-07-27 11:35:12','2016-07-27 11:35:12',1,'2'),(7,8,'rekai@lieferzonas.at',0,2,'2016-07-27 11:35:27','2016-07-27 11:35:27',1,'2'),(8,8,'!rekai@lieferzonas.at',0,2,'2016-07-27 11:56:46','2016-07-27 11:56:46',1,'2'),(9,8,'9rekai@lieferzonas.at',0,2,'2016-07-27 11:56:56','2016-07-27 11:56:56',1,'2'),(10,8,'',0,2,'2016-07-27 12:27:18','2016-07-27 12:27:18',0,'2'),(11,8,'jitendra.alivenetsolution@gmail.com',0,2,'2016-07-27 12:33:20','2016-07-27 12:33:20',0,'2'),(12,8,'rekai@lieferzonas.at',0,2,'2016-07-27 12:33:35','2016-07-27 12:33:35',0,'2'),(13,8,'',0,2,'2016-07-27 12:37:10','2016-07-27 12:37:10',0,'2'),(14,8,'rekai@lieferzonas.at',0,2,'2016-07-27 12:37:49','2016-07-27 12:37:49',1,'2'),(15,8,'rekai@lieferzonas.at',0,2,'2016-07-27 12:40:09','2016-07-27 12:40:09',0,'2'),(16,8,'',0,2,'2016-07-27 12:43:54','2016-07-27 12:43:54',0,'2'),(17,8,'rekai@lieferzonas.at',0,2,'2016-07-27 12:44:26','2016-07-27 12:44:26',0,'2'),(18,8,'',0,2,'2016-07-27 12:45:42','2016-07-27 12:45:42',0,'2'),(19,8,'rekai@lieferzonas.at',0,2,'2016-07-27 12:46:47','2016-07-27 12:46:47',0,'2'),(20,8,'dsd',0,2,'2016-07-27 12:54:37','2016-07-27 12:54:37',0,'2'),(21,8,'rekai@lieferzonas.at',0,2,'2016-07-27 12:54:56','2016-07-27 12:54:56',0,'2'),(22,8,'dsd',0,2,'2016-07-27 12:57:41','2016-07-27 12:57:41',0,'2'),(23,8,'rekai@lieferzonas.at',0,2,'2016-07-27 12:57:56','2016-07-27 12:57:56',0,'2'),(24,9,'arun.alivenetsolution@gmail.com',2,0,'2016-08-01 13:10:15','2016-08-01 13:10:15',1,'1'),(25,9,'arun.alivenetsolution@gmail.com',2,0,'2016-08-01 13:10:15','2016-08-01 13:10:15',0,'1'),(26,10,'Arunkumarsharma@gmail.com',2,0,'2016-08-01 13:58:20','2016-08-01 13:58:20',1,'1'),(27,10,'arun@gmail.com',2,0,'2016-08-01 13:58:20','2016-08-01 13:58:20',0,'1'),(28,11,'rameshsuresh@gmail.com',2,0,'2016-08-01 15:47:59','2016-08-01 15:47:59',1,'1'),(29,11,'ramesh@gmail.com',2,0,'2016-08-01 15:47:59','2016-08-01 15:47:59',0,'1'),(30,12,'shayam@gmail.com',2,0,'2016-08-01 15:56:43','2016-08-01 15:56:43',1,'2'),(31,12,'shayamsharma@gmail.com',2,0,'2016-08-01 15:56:43','2016-08-01 15:56:43',0,'1'),(32,13,'ritika@gmail.com',2,0,'2016-08-01 16:37:39','2016-08-01 16:37:39',1,'1'),(33,13,'',2,0,'2016-08-01 16:37:39','2016-08-01 16:37:39',0,'1'),(34,14,'surendra@gmail.com',2,0,'2016-08-01 16:41:43','2016-08-01 16:41:43',1,'1'),(35,14,'surendrasharma@gmail.com',2,0,'2016-08-01 16:41:43','2016-08-01 16:41:43',0,'1'),(36,12,'shayamsharma@gmail.com',0,2,'2016-08-01 16:42:40','2016-08-01 16:42:40',1,'1'),(37,15,'ramu@gmail.com',2,0,'2016-08-01 16:44:37','2016-08-01 16:44:37',1,'1'),(38,15,'shamu@gmail.com',2,0,'2016-08-01 16:44:37','2016-08-01 16:44:37',0,'1'),(39,16,'test@gmail.com',2,0,'2016-08-01 16:48:07','2016-08-01 16:48:07',1,'1'),(40,16,'test@gmail.com',2,0,'2016-08-01 16:48:07','2016-08-01 16:48:07',0,'1'),(41,9,'anjali@gmail.com',0,0,'2016-08-01 17:04:34','2016-08-01 17:04:34',1,'1'),(42,17,'rekha@gmail.com',2,0,'2016-08-01 17:26:10','2016-08-01 17:26:10',1,'1'),(43,17,'',2,0,'2016-08-01 17:26:10','2016-08-01 17:26:10',0,'1'),(44,18,'anjali.alivenetsolutions@gmail.com',2,0,'2016-08-01 17:49:43','2016-08-01 17:49:43',1,'1'),(45,18,'',2,0,'2016-08-01 17:49:43','2016-08-01 17:49:43',0,'1'),(46,19,'testt@gmail.com',2,0,'2016-08-01 17:53:50','2016-08-01 17:53:50',1,'1'),(47,19,'testt@gmail.com',2,0,'2016-08-01 17:53:50','2016-08-01 17:53:50',0,'1'),(48,10,'arun.alivenetsolution@gmail.com',0,0,'2016-08-02 12:46:53','2016-08-02 12:46:53',1,'1'),(49,11,'jitendra.alivenetsolutions@gmail.com',0,0,'2016-08-03 15:59:05','2016-08-03 15:59:05',1,'1'),(50,12,'laxmi@gmail.com',0,0,'2016-08-03 16:07:33','2016-08-03 16:07:33',1,'1'),(51,13,'arun@gmail.com',0,0,'2016-08-04 16:36:10','2016-08-04 16:36:10',1,'1'),(52,8,'recaifirat@lieferzonas.at',0,2,'2016-08-10 09:07:39','2016-08-10 09:07:39',1,'1'),(53,8,'recaifirat@lieferzonas.at',0,2,'2016-08-10 09:07:39','2016-08-10 09:07:39',0,'1');
/*!40000 ALTER TABLE `lie_user_email_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_masters`
--

DROP TABLE IF EXISTS `lie_user_masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `doj` date NOT NULL,
  `profile_pic` varchar(250) NOT NULL,
  `add1` varchar(100) NOT NULL,
  `add2` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `zipcode` varchar(6) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_masters`
--

LOCK TABLES `lie_user_masters` WRITE;
/*!40000 ALTER TABLE `lie_user_masters` DISABLE KEYS */;
INSERT INTO `lie_user_masters` VALUES (2,2,'vikas','batra','vikas@gmail.com','7876222011','1989-03-30','2016-06-28','1463390024742.jpg','A-512,Gali no 10','South Ganesh Nagar','11','21','4','1100',1,2,'2016-05-16 05:16:11','2016-09-03 07:39:26','1'),(6,8,'Recai','Firat','recaifirat@lieferzonas.at','06644975080','1982-10-12','2016-09-03','1469578444933.png','Margaretenstrasse 100','','11','21','4','2',2,2,'2016-07-20 05:36:13','2016-09-03 07:43:14','1'),(7,10,'ram','Sharma','Arunkumarsharma@gmail.com','9026484405','1991-02-12','2016-07-10','','A 22 sec 37 ','Ashoka enclave','12','11','1','1',2,2,'2016-08-01 13:58:20','2016-08-01 17:23:19','1'),(8,13,'Ritika','Sharma','ritika@gmail.com','9656565656','1998-07-15','2016-08-30','','test','','1','1','1','2',2,2,'2016-08-01 16:37:39','2016-08-01 16:48:45','1'),(9,14,'surendra','sharma','surendra@gmail.com','9012345678','1986-10-28','2016-07-13','','E 22 d block','tuglakabad','1','1','1','3',2,0,'2016-08-01 16:41:43','2016-08-01 16:41:43','1'),(10,18,'Anjali','Kumari','anjali.alivenetsolutions@gmail.com','9656565656','1995-10-31','2016-08-30','1470053983903.jpg','abc','abc','1','1','1','2',2,2,'2016-08-01 17:49:43','2016-08-01 17:50:09','1'),(11,19,'testt','testt','testt@gmail.com','1234567890','1998-07-16','2016-08-17','1470054273432.png','testt@gmail.com','testt@gmail.com','1','1','1','2',2,2,'2016-08-01 17:53:50','2016-08-01 17:54:33','1');
/*!40000 ALTER TABLE `lie_user_masters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_notification_maps`
--

DROP TABLE IF EXISTS `lie_user_notification_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_notification_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_notification_maps`
--

LOCK TABLES `lie_user_notification_maps` WRITE;
/*!40000 ALTER TABLE `lie_user_notification_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_user_notification_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_notifications`
--

DROP TABLE IF EXISTS `lie_user_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `browser_always` tinyint(4) DEFAULT '0',
  `mail_always` tinyint(4) DEFAULT '0',
  `sms_always` tinyint(4) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_notifications`
--

LOCK TABLES `lie_user_notifications` WRITE;
/*!40000 ALTER TABLE `lie_user_notifications` DISABLE KEYS */;
INSERT INTO `lie_user_notifications` VALUES (1,'Restaurant','Ein neues Restaurant wurde hinzugefügt',0,0,0,1,1,'2017-01-11 17:24:11','2017-01-11 17:24:14','1'),(2,'Bestellung Eingang','Eine Bestellung ist bei uns eingegangen',0,0,0,1,1,'2017-01-11 17:24:44','2017-01-11 17:24:46','1'),(3,'Bestellung Bestätigung','Eine Bestellung wurde vom Restaurant bestätigt',0,0,0,1,1,'2017-01-11 17:25:15','2017-01-11 17:25:17','1');
/*!40000 ALTER TABLE `lie_user_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_order_reviews`
--

DROP TABLE IF EXISTS `lie_user_order_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_order_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `comment` varchar(250) NOT NULL,
  `quality_rating` float NOT NULL,
  `service_rating` float NOT NULL,
  `is_reported` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 for not reported,1 for reported',
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_order_reviews`
--

LOCK TABLES `lie_user_order_reviews` WRITE;
/*!40000 ALTER TABLE `lie_user_order_reviews` DISABLE KEYS */;
INSERT INTO `lie_user_order_reviews` VALUES (1,2,1,'363320334211LR2','Nice. I liked it.',2.9,2.9,'0','1',0,0,'2016-08-01 16:33:04','2016-08-01 16:33:04'),(2,1,3,'363401503850ZR1','sehr gut',4.9,0,'0','1',0,0,'2016-08-02 03:20:43','2016-08-02 03:20:43'),(3,1,3,'363401494705CR1','lecker',4.9,4.9,'1','1',0,0,'2016-08-02 03:21:17','2016-08-25 13:54:50'),(4,1,3,'363402315221LR1','sehr gut fdfdf',4.8,4.9,'0','1',0,2,'2016-08-02 03:58:12','2016-08-27 12:44:41'),(5,1,3,'363329332536ZR1','sehr gute pizza',5,5,'0','1',0,2,'2016-08-04 06:09:06','2016-08-13 06:39:43'),(6,2,3,'363324323153TR2','',4.8,4.6,'0','1',0,0,'2016-08-04 06:09:33','2016-08-04 06:09:33'),(7,2,3,'363326323051YR2','',4.7,4.8,'0','1',0,0,'2016-08-04 06:09:46','2016-08-04 06:09:46'),(8,2,3,'363326332729OR2','',4.7,4.8,'0','1',0,0,'2016-08-04 06:10:04','2016-08-04 06:10:04'),(9,2,3,'363327325848WR2','',4.8,4.6,'0','1',0,0,'2016-08-04 06:10:17','2016-08-04 06:10:17'),(10,2,3,'363327330014ER2','gut',4.8,4.7,'0','1',0,0,'2016-08-04 06:10:33','2016-08-04 06:10:33'),(11,2,3,'363328330213JR2','sehr gut',4.7,4.8,'1','1',0,0,'2016-08-04 06:10:49','2016-08-12 11:08:19'),(12,1,3,'363405334239SR1','bravo',4.8,4.7,'0','1',0,0,'2016-08-05 05:46:29','2016-08-05 05:46:29'),(13,1,3,'363409340207FR1','woow',5,5,'0','1',0,2,'2016-08-09 06:06:39','2016-08-25 13:56:32'),(14,2,4,'363321394253KR2','Nice. I liked it.',5,5,'0','1',0,2,'2016-08-10 17:42:42','2016-08-13 06:40:38'),(15,2,4,'363321394253KR2','Nice. I liked it.',3.7,4.7,'0','1',0,0,'2016-08-10 17:42:46','2016-08-10 17:42:46'),(16,1,4,'363401442620AR1','hi',5,5,'0','1',0,2,'2016-08-10 17:43:30','2016-08-13 06:41:38'),(17,1,4,'363401442620AR1','hi',5,5,'0','1',0,2,'2016-08-10 17:43:34','2016-08-13 06:41:17'),(18,1,4,'363401442620AR1','hi',5,5,'0','1',0,2,'2016-08-10 17:43:43','2016-08-13 06:41:26'),(19,2,4,'363321432009HR2','hi',2.9,0,'1','1',0,0,'2016-08-10 17:46:38','2016-08-12 11:08:56'),(20,1,4,'363401442359PR1','hi',2.9,0,'0','1',0,0,'2016-08-10 17:46:53','2016-08-10 17:46:53'),(21,1,3,'363412362905AR1','Ich muss ehrlich zugeben Pizza wahr super gut\r\nAlles hat gut funktioniert\r\nwieder gern',5,5,'1','1',0,2,'2016-08-12 08:32:56','2016-08-24 01:23:38'),(22,1,3,'363412362605OR1','Die Pizza ist gut gelungen ',5,5,'1','1',0,2,'2016-08-13 01:53:26','2016-08-27 12:21:41'),(23,2,3,'363323321816ZR2','nice',4.8,4.9,'0','1',0,0,'2016-08-18 06:11:27','2016-08-18 06:11:27'),(24,2,3,'363322433618FR2','',4.9,4.9,'0','1',0,0,'2016-08-21 04:25:15','2016-08-21 04:25:15'),(25,17,11,'363423412759TR17','',0,0,'0','1',0,0,'2016-08-23 13:44:57','2016-08-23 13:44:57'),(26,17,11,'363422451004YR17','',0,0,'0','1',0,0,'2016-08-23 13:45:10','2016-08-23 13:45:10'),(27,1,4,'363329391405AR1','',0,0,'0','1',0,0,'2016-08-23 16:22:04','2016-08-23 16:22:04'),(28,2,4,'363322382721TR2','hi',0,0,'0','1',0,0,'2016-08-23 16:22:35','2016-08-23 16:22:35'),(29,2,4,'363320470813OR2','hi',0,3.2,'0','1',0,0,'2016-08-23 16:23:06','2016-08-23 16:23:06'),(30,17,4,'363423401236DR17','',4,5,'0','1',0,0,'2016-08-23 16:25:07','2016-08-23 16:25:07'),(31,17,4,'363423394738GR17','',4,4,'0','1',0,0,'2016-08-23 16:25:49','2016-08-23 16:25:49'),(32,17,4,'363423394332HR17','hi',3.2,3.1,'0','1',0,0,'2016-08-23 16:26:30','2016-08-23 16:26:30'),(33,17,4,'363423443310ZR17','hi',0,2.9,'0','1',0,0,'2016-08-23 16:55:46','2016-08-23 16:55:46'),(34,17,4,'363423443310ZR17','hi',0,2.9,'0','1',0,0,'2016-08-23 16:55:46','2016-08-23 16:55:46'),(35,1,4,'363329393927DR1','hi ffgg',0,2.9,'0','1',0,2,'2016-08-23 16:56:12','2016-08-27 12:50:33'),(36,1,4,'363329394045NR1','hi',3,4,'0','1',0,0,'2016-08-23 17:00:19','2016-08-23 17:00:19'),(37,2,4,'363321432744YR2','hi',3.1,2.1,'0','1',0,0,'2016-08-23 17:05:51','2016-08-23 17:05:51'),(38,1,4,'363329393802CR1','hi',3.1,2.1,'0','1',0,0,'2016-08-23 17:06:06','2016-08-23 17:06:06'),(39,15,3,'363426332145SR15','schaut gut aus',0,4.9,'1','1',0,0,'2016-08-26 05:24:11','2016-09-11 05:42:28'),(40,17,11,'363426405855DR17','Very good',2,2,'1','1',0,2,'2016-08-27 11:39:14','2016-08-27 12:43:20'),(41,15,3,'363428322305JR15','hallo',5,5,'1','1',0,2,'2016-08-28 05:51:20','2016-08-30 16:23:29'),(42,15,3,'363429350103YR15','wie gesagt',4.9,4.8,'0','1',0,0,'2016-09-03 08:03:27','2016-09-03 08:03:27'),(43,15,3,'363429345930IR15','ja genau ',4.8,4.7,'0','1',0,0,'2016-09-03 08:03:44','2016-09-03 08:03:44'),(44,15,3,'363502310112ER15','ja sehr gut',4.8,4.8,'1','1',0,0,'2016-09-03 08:04:14','2016-09-11 05:42:36'),(45,5,5,'363506393236OR5','excellent delivery',0,4.9,'1','1',0,2,'2016-09-06 12:30:25','2016-09-06 13:16:01'),(46,5,5,'363506403409SR5','good ',4.6,4.8,'1','1',0,0,'2016-09-06 12:35:43','2016-09-06 16:36:45'),(47,15,3,'363425431020YR15','',4.8,4.9,'0','1',0,0,'2016-09-10 02:19:26','2016-09-10 02:19:26'),(48,15,3,'363511334553FR15','zuhjkl,.',4.8,4.9,'0','1',0,0,'2016-09-12 12:14:04','2016-09-12 12:14:04'),(49,15,3,'363512441804FR15','',4.8,4.8,'0','1',0,0,'2016-09-15 04:57:15','2016-09-15 04:57:15'),(50,15,3,'363515325428FR15','',4.9,4.9,'0','1',0,0,'2016-09-20 05:26:43','2016-09-20 05:26:43'),(51,15,3,'363518292750GR15','',4.8,4.8,'1','1',0,0,'2016-09-24 06:26:21','2016-09-29 02:30:47'),(52,15,3,'363604323156AR15','ist nicht gut',4.7,4.6,'0','1',0,0,'2016-10-04 04:37:14','2016-10-04 04:37:14'),(53,15,23,'363726294709AR15','asdfq',4.7,4.4,'0','1',0,0,'2016-11-26 06:20:13','2016-11-26 06:20:13'),(54,15,23,'363831382659UR15','schlecht',0.5,2,'0','1',0,0,'2016-12-31 21:11:43','2016-12-31 21:11:43');
/*!40000 ALTER TABLE `lie_user_order_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_orders`
--

DROP TABLE IF EXISTS `lie_user_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(100) NOT NULL,
  `front_user_id` int(11) NOT NULL,
  `front_user_address_id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `front_order_status_id` int(11) NOT NULL DEFAULT '1' COMMENT '1->placed',
  `total_amount` varchar(150) NOT NULL,
  `grand_total` varchar(150) NOT NULL,
  `order_type` varchar(100) NOT NULL COMMENT '1=>delivery,2=>takeaway',
  `rest_given_time` varchar(150) NOT NULL,
  `user_time` datetime NOT NULL,
  `time_type` enum('1','2') NOT NULL COMMENT '1->Now, 2->Later',
  `delivery_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_orders`
--

LOCK TABLES `lie_user_orders` WRITE;
/*!40000 ALTER TABLE `lie_user_orders` DISABLE KEYS */;
INSERT INTO `lie_user_orders` VALUES (1,'363320334211LR2',1,2,2,5,'350','350','1','45','2016-08-05 02:30:01','1','2016-08-05 06:30:01',3,2,'2016-08-05 02:30:01','2016-07-22 02:44:34','1'),(2,'363320351800QR2',3,2,2,1,'50','50','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,3,'2016-06-20 07:18:00','2016-06-20 07:18:00','1'),(3,'363320470813OR2',4,3,2,1,'195','195','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',4,4,'2016-07-20 19:08:13','2016-07-20 19:08:13','1'),(4,'363321323825YR2',3,2,2,1,'150','150','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,3,'2016-07-21 04:38:25','2016-07-21 04:38:25','1'),(5,'363321394253KR2',4,3,2,2,'150','150','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',4,2,'2016-07-21 11:42:53','2016-07-21 11:47:30','1'),(6,'363321430642MR2',5,4,2,3,'50','50','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-21 15:06:42','2016-07-21 15:54:27','1'),(7,'363321432009HR2',4,3,2,6,'350','350','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',4,2,'2016-07-21 15:20:09','2016-07-21 17:35:06','1'),(8,'363321432744YR2',4,6,2,5,'350','350','1','45','0000-00-00 00:00:00','1','2016-08-05 06:30:40',4,2,'2016-08-05 02:30:01','2016-07-21 15:51:46','1'),(9,'363321435640NR2',5,4,2,5,'100','100','1','15','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-21 15:56:40','2016-07-21 16:00:50','1'),(10,'363321452938OR2',3,2,2,6,'100','100','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,2,'2016-07-21 17:29:38','2016-07-21 17:31:34','1'),(11,'363322382721TR2',4,6,2,1,'50','50','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',4,4,'2016-07-22 10:27:21','2016-07-22 10:27:21','1'),(12,'363322433618FR2',3,2,2,2,'45','45','1','45','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,2,'2016-07-22 15:36:18','2016-07-22 15:36:48','1'),(13,'363323321816ZR2',3,2,2,2,'50','50','1','','2016-07-24 01:45:00','2','2016-08-05 06:30:01',3,2,'2016-07-23 04:18:16','2016-07-23 04:19:42','1'),(14,'363324323153TR2',3,2,2,6,'50','50','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,2,'2016-07-24 04:31:53','2016-07-24 04:32:20','1'),(15,'363325404323IR2',5,4,2,6,'250','250','1','','2016-07-26 13:45:00','2','2016-08-05 06:30:01',5,2,'2016-07-25 12:43:23','2016-07-25 18:45:47','1'),(16,'363326323051YR2',3,2,2,5,'250','250','1','15','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,2,'2016-07-26 04:30:51','2016-07-26 04:40:07','1'),(17,'363326332729OR2',3,2,2,3,'50','50','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,2,'2016-07-26 05:27:29','2016-07-27 04:48:19','1'),(18,'363327325848WR2',3,2,2,1,'45','45','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,3,'2016-07-27 04:58:48','2016-07-27 04:58:48','1'),(19,'363327330014ER2',3,2,2,2,'45','45','1','180','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,2,'2016-07-27 05:00:14','2016-07-27 05:01:01','1'),(20,'363328330213JR2',3,2,2,1,'100','100','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,3,'2016-08-27 05:02:13','2016-07-28 05:02:13','1'),(21,'363328405223OR2',5,13,2,1,'500','500','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,5,'2016-08-27 12:52:23','2016-07-28 12:52:23','1'),(22,'363328405340VR2',5,13,2,1,'500','500','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,5,'2016-07-28 12:53:40','2016-07-28 12:53:40','1'),(23,'363328405440ZR2',5,4,2,1,'500','500','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,5,'2016-07-28 12:54:40','2016-07-28 12:54:40','1'),(24,'363328405609CR2',5,4,2,1,'500','500','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,5,'2016-07-28 12:56:09','2016-07-28 12:56:09','1'),(25,'363328405845TR2',5,13,2,1,'500','500','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,5,'2016-07-28 12:58:45','2016-07-28 12:58:45','1'),(26,'363328405923PR2',5,13,2,1,'500','500','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,5,'2016-07-28 12:59:23','2016-07-28 12:59:23','1'),(27,'363328410724WR2',5,4,2,1,'500','500','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,5,'2016-07-28 13:07:24','2016-07-28 13:07:24','1'),(28,'363328410822GR2',5,4,2,1,'100','100','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,5,'2016-07-28 13:08:22','2016-07-28 13:08:22','1'),(29,'363328410910RR2',5,4,2,6,'100','100','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 13:09:10','2016-07-28 15:33:17','1'),(30,'363328412251GR2',5,4,2,6,'100','100','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 13:22:51','2016-07-28 15:33:00','1'),(31,'363328412315WR2',5,4,2,6,'100','100','1','15','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 13:23:15','2016-07-28 15:32:46','1'),(32,'363328412518HR2',5,4,2,6,'100','100','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 13:25:18','2016-07-28 15:32:29','1'),(33,'363328412704OR2',5,4,2,6,'100','100','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 13:27:04','2016-08-12 11:01:51','2'),(34,'363328412728UR2',5,4,2,6,'100','100','1','90','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 13:27:28','2016-07-28 15:31:41','1'),(35,'363328413125GR2',5,4,2,6,'100','100','1','75','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 13:31:25','2016-08-12 11:01:29','2'),(36,'363328413244TR2',5,4,2,6,'100','100','1','15','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 13:32:44','2016-07-28 15:30:19','1'),(37,'363328430604QR2',5,4,2,6,'100','100','1','','2016-07-28 14:30:00','2','2016-08-05 06:30:01',5,2,'2016-07-28 15:06:04','2016-07-28 15:29:47','1'),(38,'363328430640JR2',5,4,2,6,'100','100','1','','2016-07-28 14:30:00','2','2016-08-05 06:30:01',5,2,'2016-07-28 15:06:40','2016-07-28 15:29:35','1'),(39,'363328430800YR2',5,4,2,6,'100','100','1','45','0000-00-00 00:00:00','1','2016-08-05 06:30:01',5,2,'2016-07-28 15:08:00','2016-07-28 15:29:13','1'),(40,'363329332536ZR1',3,2,1,3,'100','100','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,1,'2016-07-29 05:25:36','2016-08-19 04:41:05','1'),(41,'363329391405AR1',4,7,1,2,'100','100','1','60','0000-00-00 00:00:00','1','2016-08-30 04:15:13',4,1,'2016-07-29 11:14:05','2016-08-30 15:15:13','1'),(42,'363329392718TR1',4,7,1,7,'100','100','1','90','0000-00-00 00:00:00','1','2016-08-25 02:33:25',4,1,'2016-07-29 11:27:18','2016-08-25 13:07:59','1'),(43,'363329393802CR1',4,7,1,2,'100','100','1','60','0000-00-00 00:00:00','1','2016-08-25 02:10:46',4,1,'2016-07-29 11:38:02','2016-08-25 13:10:46','1'),(44,'363329393927DR1',4,7,1,2,'100','100','1','30','0000-00-00 00:00:00','1','2016-08-27 01:01:06',4,1,'2016-07-29 11:39:27','2016-08-27 12:31:06','1'),(45,'363329394045NR1',4,7,1,5,'100','100','1','45','0000-00-00 00:00:00','1','2016-08-25 12:59:27',4,1,'2016-07-29 11:40:45','2016-08-25 12:14:30','1'),(46,'363401442359PR1',4,14,1,5,'50','50','1','15','0000-00-00 00:00:00','1','2016-08-25 11:56:47',4,1,'2016-08-01 16:23:59','2016-08-25 11:42:11','1'),(47,'363401442620AR1',4,14,1,3,'50','50','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',4,1,'2016-08-01 16:26:20','2016-08-09 05:40:39','1'),(48,'363401494705CR1',3,2,1,6,'50','50','1','30','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,1,'2016-08-01 21:47:05','2016-08-01 21:50:00','1'),(50,'363401503850ZR1',3,2,1,6,'395','395','1','45','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,1,'2016-08-01 22:38:50','2016-08-09 05:39:55','1'),(51,'363402315221LR1',3,2,1,6,'144.9','144.9','1','60','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,1,'2016-08-02 03:52:21','2016-08-03 07:30:28','1'),(52,'363405334239SR1',3,2,1,2,'100','100','1','45','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,1,'2016-08-05 05:42:39','2016-08-05 05:45:01','1'),(53,'363409340207FR1',3,2,1,6,'49.45','49.45','1','15','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,1,'2016-08-09 06:02:07','2016-08-09 06:08:17','1'),(54,'363412362605OR1',3,2,1,7,'99.4','99.4','1','45','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,1,'2016-08-27 08:26:05','2016-08-25 13:36:39','1'),(55,'363412362905AR1',3,2,1,7,'99.4','99.4','1','15','0000-00-00 00:00:00','1','2016-08-05 06:30:01',3,1,'2016-08-27 08:29:05','2016-08-27 12:25:29','1'),(57,'363422445634XR17',11,19,17,1,'600','600','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',11,11,'2016-08-22 16:56:34','2016-08-22 16:56:34','1'),(58,'363422445647VR17',11,19,17,3,'600','600','1','','0000-00-00 00:00:00','1','2016-08-05 06:30:01',11,16,'2016-08-22 16:56:47','2016-08-22 17:41:51','1'),(59,'363422451004YR17',11,20,17,7,'200','200','1','60','0000-00-00 00:00:00','1','2016-08-05 06:30:01',11,16,'2016-08-22 17:10:04','2016-09-10 16:56:58','1'),(60,'363423332354TR17',3,2,17,2,'200','200','1','30','0000-00-00 00:00:00','1','2016-08-23 06:20:11',3,16,'2016-08-23 05:23:54','2016-08-23 17:50:11','1'),(61,'363423394332HR17',4,7,17,2,'200','200','1','120','0000-00-00 00:00:00','1','2016-08-05 06:30:01',4,16,'2016-08-23 11:43:32','2016-08-23 11:46:30','1'),(62,'363423394738GR17',4,7,17,5,'400','400','1','','2016-08-24 12:45:00','2','2016-08-05 06:30:01',4,16,'2016-08-23 11:47:38','2016-08-23 17:33:08','1'),(63,'363423401236DR17',4,7,17,2,'400','400','1','','2016-08-25 13:15:00','2','2016-08-25 13:15:00',4,16,'2016-08-23 12:12:36','2016-08-23 12:12:52','1'),(64,'363423412759TR17',11,19,17,1,'1000','1000','1','','2016-08-24 14:30:00','2','0000-00-00 00:00:00',11,11,'2016-08-23 13:27:59','2016-08-23 13:27:59','1'),(65,'363423443310ZR17',4,7,17,2,'200','200','1','','2016-08-27 20:00:00','2','2016-08-27 20:00:00',4,16,'2016-08-23 16:33:10','2016-08-23 16:33:35','1'),(66,'363423444601GR17',19,25,17,6,'200','200','1','','2016-08-25 18:30:00','2','2016-08-25 18:30:00',19,16,'2016-08-23 16:46:01','2016-08-23 17:34:21','1'),(67,'363424330338TR15',3,2,15,6,'135.67','135.67','1','15','0000-00-00 00:00:00','1','2016-08-24 05:22:51',3,15,'2016-08-24 05:03:38','2016-08-24 05:13:46','1'),(68,'363425413334YR15',3,2,15,6,'33.9','33.9','1','15','0000-00-00 00:00:00','1','2016-08-25 01:51:43',3,15,'2016-08-25 13:33:34','2016-08-25 13:37:20','1'),(69,'363425431020YR15',3,2,15,7,'13.5','13.5','1','15','0000-00-00 00:00:00','1','2016-08-31 12:40:11',3,15,'2016-08-25 15:10:20','2016-08-31 00:25:22','1'),(70,'363426324658FR15',3,2,15,2,'13.5','13.5','1','','2016-08-28 23:30:00','2','2016-08-28 23:30:00',3,15,'2016-08-26 04:46:58','2016-08-26 05:09:14','1'),(71,'363426332145SR15',3,2,15,7,'170.27','170.27','1','15','0000-00-00 00:00:00','1','2016-08-26 05:37:04',3,15,'2016-08-26 05:21:45','2016-08-26 05:22:46','1'),(72,'363426405855DR17',11,20,17,7,'700','700','1','90','0000-00-00 00:00:00','1','2016-08-26 02:36:20',11,16,'2016-08-26 12:58:55','2016-08-26 18:05:54','1'),(73,'363428322305JR15',3,2,15,7,'36.99','36.99','1','','2016-08-28 05:15:00','2','2016-08-28 05:15:00',3,15,'2016-08-28 04:23:05','2016-08-28 05:50:51','1'),(74,'363429341457RR15',3,2,15,3,'12.6','12.6','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',3,15,'2016-08-29 06:14:57','2016-08-29 06:17:45','1'),(75,'363429345930IR15',3,2,15,7,'12.6','12.6','1','15','0000-00-00 00:00:00','1','2016-08-29 07:17:39',3,15,'2016-08-29 06:59:30','2016-08-29 07:02:48','1'),(76,'363429350103YR15',3,2,15,7,'5.9','5.9','1','15','0000-00-00 00:00:00','1','2016-08-29 07:16:50',3,15,'2016-08-29 07:01:03','2016-08-29 07:02:04','1'),(77,'363429404729GR17',11,19,17,3,'100','100','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',11,16,'2016-08-29 12:47:29','2016-08-29 12:59:18','1'),(78,'363431413340KR17',11,19,17,4,'450','450','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',11,11,'2016-08-31 13:33:40','2016-08-31 13:42:54','1'),(79,'363501341315ZR15',3,2,15,7,'13.6','13.6','1','','2016-09-01 11:00:00','2','2016-09-01 11:00:00',3,15,'2016-09-01 06:13:15','2016-09-16 04:48:53','1'),(80,'363502310112ER15',3,2,15,7,'12.6','12.6','1','15','0000-00-00 00:00:00','1','2016-09-02 03:16:40',3,15,'2016-09-02 03:01:12','2016-09-02 03:07:32','1'),(81,'363503333720CR15',3,2,15,7,'26.1','26.1','1','15','0000-00-00 00:00:00','1','2016-09-03 05:53:04',3,15,'2016-09-03 05:37:20','2016-09-07 13:37:02','2'),(82,'363506393236OR5',5,32,5,7,'400','400','1','15','0000-00-00 00:00:00','1','2016-09-06 11:52:33',5,4,'2016-09-06 11:32:36','2016-09-07 13:37:02','2'),(83,'363506403409SR5',5,32,5,7,'200','200','1','15','0000-00-00 00:00:00','1','2016-09-06 12:49:48',5,4,'2016-09-06 12:34:09','2016-09-06 12:35:09','1'),(84,'363506440656WR5',5,32,5,7,'600','600','1','15','0000-00-00 00:00:00','1','2016-09-06 04:22:25',5,4,'2016-09-06 16:06:56','2016-09-06 16:07:51','1'),(85,'363506462416VR1',5,32,1,1,'100','100','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',5,5,'2016-09-06 18:24:16','2016-09-07 13:37:02','2'),(86,'363506462553NR5',5,32,5,7,'400','400','1','15','0000-00-00 00:00:00','1','2016-09-06 06:41:39',5,4,'2016-09-06 18:25:53','2016-09-06 18:48:15','1'),(87,'363506465018ZR1',21,33,1,1,'390','390','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',21,21,'2016-09-06 18:50:18','2016-09-06 18:50:18','1'),(88,'363506465037JR1',21,33,1,1,'390','390','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',21,21,'2016-09-06 18:50:37','2016-09-06 18:50:37','1'),(89,'363507352047SR15',3,31,15,2,'13.5','13.5','1','15','0000-00-00 00:00:00','1','2016-09-07 07:36:41',3,15,'2016-09-07 07:20:47','2016-09-07 13:38:44','2'),(90,'363507395820ZR5',5,32,5,1,'500','500','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',5,5,'2016-09-07 11:58:20','2016-09-07 13:37:02','2'),(91,'363508412812KR5',1,26,5,1,'250','250','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',1,1,'2016-09-08 13:28:12','2016-09-08 13:28:12','1'),(92,'363509442214OR5',5,32,5,1,'400','400','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',5,5,'2016-09-09 16:22:14','2016-09-09 16:22:14','1'),(93,'363510301713AR15',3,2,15,6,'42.5','42.5','1','30','0000-00-00 00:00:00','1','2016-09-10 02:47:36',3,15,'2016-09-10 02:17:13','2016-09-10 02:17:45','1'),(94,'363510434445YR17',11,19,17,1,'300','300','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',11,11,'2016-09-10 15:44:45','2016-09-10 15:44:45','1'),(95,'363510435558WR17',11,30,17,1,'300','300','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',11,11,'2016-09-10 15:55:58','2016-09-10 15:55:58','1'),(96,'363511334553FR15',3,2,15,7,'58','58','1','15','0000-00-00 00:00:00','1','2016-09-11 06:03:21',3,15,'2016-09-11 05:45:53','2016-09-11 05:54:09','1'),(97,'363512413231UR17',4,16,17,1,'150','150','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',4,4,'2016-09-12 13:32:31','2016-09-12 13:32:31','1'),(98,'363512413304WR15',11,35,15,5,'50.39','50.39','1','15','0000-00-00 00:00:00','1','2016-09-12 03:22:40',11,15,'2016-09-12 13:33:04','2016-09-12 15:07:46','1'),(99,'363512441804FR15',3,2,15,7,'13.5','13.5','1','15','0000-00-00 00:00:00','1','2016-09-12 04:33:44',3,15,'2016-09-12 16:18:04','2016-09-12 16:18:52','1'),(100,'363513310244LR15',3,2,15,4,'15.98','15.98','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',3,3,'2016-09-13 03:02:44','2016-09-13 08:03:32','1'),(101,'363515325428FR15',3,31,15,7,'13.99','13.99','1','15','0000-00-00 00:00:00','1','2016-09-15 05:11:34',3,15,'2016-09-15 04:54:28','2016-09-15 05:19:06','1'),(102,'363518292750GR15',3,39,15,7,'19.98','19.98','1','15','0000-00-00 00:00:00','1','2016-09-18 01:43:46',3,15,'2016-09-18 01:27:50','2016-09-20 02:45:15','1'),(103,'363520332906LR15',3,40,15,2,'43.22','43.22','1','15','0000-00-00 00:00:00','1','2016-09-20 05:44:25',3,15,'2016-09-20 05:29:06','2016-09-20 05:29:25','1'),(104,'363520333555XR15',3,41,15,4,'69.18','69.18','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',3,3,'2016-09-20 05:35:55','2016-09-20 05:52:01','1'),(105,'363520334810IR15',3,39,15,6,'13.5','13.5','1','15','0000-00-00 00:00:00','1','2016-09-20 06:06:23',3,15,'2016-09-20 05:48:10','2016-09-20 05:51:37','1'),(106,'363524335113SR15',3,39,15,4,'5.9','5.9','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',3,3,'2016-09-24 05:51:13','2016-09-24 05:52:00','1'),(107,'363524351237QR15',3,39,15,4,'13.5','13.5','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',3,3,'2016-09-24 07:12:37','2016-09-24 07:13:05','1'),(108,'363604323156AR15',3,39,15,7,'26.7','26.7','1','15','0000-00-00 00:00:00','1','2016-10-04 04:48:26',3,15,'2016-10-04 04:31:56','2016-10-04 04:36:27','1'),(109,'363720492649YR15',23,0,15,3,'30.50','30.50','2','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',23,15,'2016-11-20 21:26:49','2016-11-20 21:29:14','1'),(110,'363720493052GR15',23,43,15,2,'20.00','20.00','1','30','0000-00-00 00:00:00','1','2016-11-20 10:01:32',23,15,'2016-11-20 21:30:52','2016-11-20 21:31:32','1'),(111,'363726294709AR15',23,43,15,7,'13.50','13.50','1','30','0000-00-00 00:00:00','1','2016-11-26 02:18:55',23,15,'2016-11-26 01:47:09','2016-11-26 01:49:02','1'),(113,'363828464612FR15',23,50,15,1,'8.49','8.49','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',23,23,'2016-12-28 18:46:12','2016-12-28 18:46:12','1'),(114,'363828471509SR15',23,50,15,1,'8.49','8.49','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',23,23,'2016-12-28 19:15:09','2016-12-28 19:15:09','1'),(115,'363828491818KR15',23,50,15,1,'20.50','20.50','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',23,23,'2016-12-28 21:18:18','2016-12-28 21:18:18','1'),(116,'363830331021GR15',23,50,15,1,'6.90','6.90','1','','0000-00-00 00:00:00','1','0000-00-00 00:00:00',23,23,'2016-12-30 05:10:21','2016-12-30 05:10:21','1'),(117,'363831382659UR15',23,52,15,7,'20.80','20.80','1','30','0000-00-00 00:00:00','1','2016-12-31 03:43:07',23,15,'2016-12-31 10:26:59','2016-12-31 15:13:13','1');
/*!40000 ALTER TABLE `lie_user_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_recent_rest_visits`
--

DROP TABLE IF EXISTS `lie_user_recent_rest_visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_recent_rest_visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_recent_rest_visits`
--

LOCK TABLES `lie_user_recent_rest_visits` WRITE;
/*!40000 ALTER TABLE `lie_user_recent_rest_visits` DISABLE KEYS */;
INSERT INTO `lie_user_recent_rest_visits` VALUES (51,7,2,'1',7,0,'2016-07-28 11:42:22','2016-07-28 11:42:22'),(68,1,2,'1',1,0,'2016-07-30 17:12:44','2016-07-30 17:12:44'),(69,1,3,'1',1,0,'2016-07-30 17:13:30','2016-07-30 17:13:30'),(98,3,1,'1',3,0,'2016-08-12 08:34:03','2016-08-12 08:34:03'),(101,5,9,'1',5,0,'2016-08-12 10:27:44','2016-08-12 10:27:44'),(103,5,2,'1',5,0,'2016-08-12 10:31:22','2016-08-12 10:31:22'),(104,5,3,'1',5,0,'2016-08-12 10:31:55','2016-08-12 10:31:55'),(105,5,4,'1',5,0,'2016-08-12 10:47:59','2016-08-12 10:47:59'),(109,15,6,'1',15,0,'2016-08-12 15:13:46','2016-08-12 15:13:46'),(114,15,3,'1',15,0,'2016-08-12 16:07:54','2016-08-12 16:07:54'),(117,15,4,'1',15,0,'2016-08-12 16:08:30','2016-08-12 16:08:30'),(118,15,5,'1',15,0,'2016-08-12 17:46:21','2016-08-12 17:46:21'),(119,15,2,'1',15,0,'2016-08-12 17:46:40','2016-08-12 17:46:40'),(121,3,3,'1',3,0,'2016-08-13 07:38:53','2016-08-13 07:38:53'),(123,17,2,'1',17,0,'2016-08-17 11:16:48','2016-08-17 11:16:48'),(126,11,2,'1',11,0,'2016-08-17 18:50:01','2016-08-17 18:50:01'),(130,4,2,'1',4,0,'2016-08-19 11:58:35','2016-08-19 11:58:35'),(135,3,2,'1',3,0,'2016-08-22 07:15:37','2016-08-22 07:15:37'),(139,3,17,'1',3,0,'2016-08-23 05:21:00','2016-08-23 05:21:00'),(156,11,5,'1',11,0,'2016-08-24 18:20:17','2016-08-24 18:20:17'),(190,1,6,'1',1,0,'2016-08-30 17:24:02','2016-08-30 17:24:02'),(203,1,9,'1',1,0,'2016-08-31 12:41:44','2016-08-31 12:41:44'),(206,11,6,'1',11,0,'2016-08-31 12:49:40','2016-08-31 12:49:40'),(215,4,15,'1',4,0,'2016-09-03 04:13:57','2016-09-03 04:13:57'),(218,5,6,'1',5,0,'2016-09-05 12:28:59','2016-09-05 12:28:59'),(222,3,4,'1',3,0,'2016-09-06 06:53:33','2016-09-06 06:53:33'),(228,5,1,'1',5,0,'2016-09-06 18:22:28','2016-09-06 18:22:28'),(231,5,17,'1',5,0,'2016-09-07 11:50:43','2016-09-07 11:50:43'),(237,1,1,'1',1,0,'2016-09-08 17:50:12','2016-09-08 17:50:12'),(238,1,5,'1',1,0,'2016-09-08 17:50:22','2016-09-08 17:50:22'),(239,1,17,'1',1,0,'2016-09-08 17:50:30','2016-09-08 17:50:30'),(240,1,15,'1',1,0,'2016-09-08 17:50:40','2016-09-08 17:50:40'),(241,3,5,'1',3,0,'2016-09-09 04:34:32','2016-09-09 04:34:32'),(243,5,5,'1',5,0,'2016-09-09 16:23:03','2016-09-09 16:23:03'),(256,4,5,'1',4,0,'2016-09-10 16:22:53','2016-09-10 16:22:53'),(260,4,9,'1',4,0,'2016-09-10 16:23:10','2016-09-10 16:23:10'),(267,4,1,'1',4,0,'2016-09-10 17:18:11','2016-09-10 17:18:11'),(268,4,17,'1',4,0,'2016-09-10 17:30:54','2016-09-10 17:30:54'),(275,11,17,'1',11,0,'2016-09-12 11:34:23','2016-09-12 11:34:23'),(276,11,1,'1',11,0,'2016-09-12 11:34:37','2016-09-12 11:34:37'),(279,11,15,'1',11,0,'2016-09-12 13:30:51','2016-09-12 13:30:51'),(281,11,9,'1',11,0,'2016-09-12 15:43:29','2016-09-12 15:43:29'),(288,3,9,'1',3,0,'2016-09-15 06:07:24','2016-09-15 06:07:24'),(299,3,15,'1',3,0,'2016-10-04 04:31:09','2016-10-04 04:31:09'),(307,23,15,'1',23,23,'2016-11-26 01:45:37','2017-01-11 18:57:03'),(308,23,5,'1',23,23,'2016-12-08 14:22:16','2017-01-11 13:09:07'),(309,23,14,'1',23,23,'2016-12-31 10:34:57','2016-12-31 10:43:30'),(310,23,4,'1',23,23,'2017-01-11 13:02:59','2017-01-11 13:02:59'),(311,23,2,'1',23,23,'2017-01-11 13:09:03','2017-01-11 13:09:03'),(312,23,1,'1',23,23,'2017-01-11 13:11:00','2017-01-11 13:14:57');
/*!40000 ALTER TABLE `lie_user_recent_rest_visits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_rest_favourites`
--

DROP TABLE IF EXISTS `lie_user_rest_favourites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_rest_favourites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_rest_favourites`
--

LOCK TABLES `lie_user_rest_favourites` WRITE;
/*!40000 ALTER TABLE `lie_user_rest_favourites` DISABLE KEYS */;
INSERT INTO `lie_user_rest_favourites` VALUES (2,2,2,0,0,'2016-07-19 19:29:57','2016-07-19 19:29:57','1'),(4,2,1,0,0,'2016-07-20 12:23:01','2016-07-20 12:23:01','1'),(9,2,3,0,0,'2016-07-23 04:06:19','2016-07-23 04:06:19','1'),(18,1,5,0,0,'2016-07-25 15:23:38','2016-07-25 15:23:38','1'),(22,1,3,0,0,'2016-08-10 05:14:21','2016-08-10 05:14:21','1'),(26,2,11,0,0,'2016-08-17 18:50:36','2016-08-17 18:50:36','1'),(27,3,11,0,0,'2016-08-17 18:50:41','2016-08-17 18:50:41','1'),(28,5,11,0,0,'2016-08-17 18:50:44','2016-08-17 18:50:44','1'),(31,9,11,0,0,'2016-08-17 18:50:51','2016-08-17 18:50:51','1'),(32,3,3,0,0,'2016-08-18 05:50:34','2016-08-18 05:50:34','1'),(36,6,3,0,0,'2016-08-18 05:50:52','2016-08-18 05:50:52','1'),(37,9,3,0,0,'2016-08-18 05:50:53','2016-08-18 05:50:53','1'),(38,5,3,0,0,'2016-08-18 05:50:56','2016-08-18 05:50:56','1'),(41,17,11,0,0,'2016-08-24 18:56:28','2016-08-24 18:56:28','1'),(77,5,5,0,0,'2016-09-08 17:50:35','2016-09-08 17:50:35','1'),(78,15,3,0,0,'2016-09-11 05:44:27','2016-09-11 05:44:27','1'),(79,1,23,23,0,'2016-11-30 02:12:10','2016-11-30 02:12:10','1'),(83,5,23,23,0,'2016-12-08 14:03:59','2016-12-08 14:03:59','1');
/*!40000 ALTER TABLE `lie_user_rest_favourites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_role_maps`
--

DROP TABLE IF EXISTS `lie_user_role_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_role_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_id` (`user_id`),
  KEY `lie_user_role_id` (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_role_maps`
--

LOCK TABLES `lie_user_role_maps` WRITE;
/*!40000 ALTER TABLE `lie_user_role_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_user_role_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_roles`
--

DROP TABLE IF EXISTS `lie_user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_roles`
--

LOCK TABLES `lie_user_roles` WRITE;
/*!40000 ALTER TABLE `lie_user_roles` DISABLE KEYS */;
INSERT INTO `lie_user_roles` VALUES (1,'CEO','Delivery Network Holding',0,0,'2016-07-20 05:34:21','2016-07-20 05:34:21','1'),(2,'Customer Service Agent','test test test',0,0,'2016-07-22 14:08:46','2016-07-22 14:08:46','1'),(3,'manager','management ',0,0,'2016-08-01 13:15:35','2016-08-01 13:15:35','1'),(4,'development','test development    or best results use Drush to export nodes to a specific module. Any node is supported along with its menu links. Path alias is not working yet because of a core bug. Whenever a module with exported content is enabled, its content will be imported. Each node has an export tab which presents the user with a file downoad but only of the node, not of the menu item. In addition a page has been started which lists all the exported content visible to the site. More work is needed on the UI and on language support.',0,0,'2016-08-01 13:17:41','2016-08-01 13:17:41','1');
/*!40000 ALTER TABLE `lie_user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_stamp_rest_currents`
--

DROP TABLE IF EXISTS `lie_user_stamp_rest_currents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_stamp_rest_currents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `stamp_count` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_stamp_rest_currents`
--

LOCK TABLES `lie_user_stamp_rest_currents` WRITE;
/*!40000 ALTER TABLE `lie_user_stamp_rest_currents` DISABLE KEYS */;
INSERT INTO `lie_user_stamp_rest_currents` VALUES (1,1,2,5,'1',2,2,'2016-07-25 00:00:00','0000-00-00 00:00:00'),(2,23,15,4,'1',2,2,'2016-12-08 11:02:46','2016-12-08 11:02:47');
/*!40000 ALTER TABLE `lie_user_stamp_rest_currents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_user_stamp_rest_logs`
--

DROP TABLE IF EXISTS `lie_user_stamp_rest_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_user_stamp_rest_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `order_id` int(20) NOT NULL,
  `stamp_count` int(11) NOT NULL,
  `log_type` enum('1','2') NOT NULL COMMENT '1 for awarded,2 for used',
  `status` enum('0','1','2') NOT NULL COMMENT 'ALTER TABLE `lie_user_cashback_points` CHANGE `status` `status` ENUM(''0'',''1'',''2'') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT ''1'' COMMENT ''1 for active,0 for deactive,2 for delete'';',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_user_stamp_rest_logs`
--

LOCK TABLES `lie_user_stamp_rest_logs` WRITE;
/*!40000 ALTER TABLE `lie_user_stamp_rest_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `lie_user_stamp_rest_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_users`
--

DROP TABLE IF EXISTS `lie_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_role_id` (`user_role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_users`
--

LOCK TABLES `lie_users` WRITE;
/*!40000 ALTER TABLE `lie_users` DISABLE KEYS */;
INSERT INTO `lie_users` VALUES (2,1,'vikas@gmail.com','$2y$10$wUNu232dnxrNYPiekvFK5uZ7PbNqyg5I8zL1/4SwHw/BRNR7NplH6',0,1,2,'2016-05-16 05:16:11','2016-09-19 11:38:22','1'),(8,1,'recaifirat@lieferzonas.at','$2y$10$UKq53bfb3qeq8mZLZ.7ol.nJQw4t7db2DVD6Ul8iEsrqg.TvgEnlK',0,2,2,'2016-07-20 05:36:13','2016-09-03 07:43:14','1'),(9,1,'arun.alivenetsolution@gmail.com','$2y$10$.w6cQpkSA7QNDKlCqD5VgeSdy.zGcu8pHtXkYU/aDJ0bcxggnR/Qi',0,2,0,'2016-08-01 13:10:15','2016-08-01 13:10:15','1'),(10,4,'Arunkumarsharma@gmail.com','$2y$10$FU2j2n9.10j1zbP4gF9cY.39eDhHadnL76q9ZMmtNqrmNICbvcoTO',0,2,2,'2016-08-01 13:58:20','2016-08-01 17:23:19','1'),(11,3,'rameshsuresh@gmail.com','$2y$10$47YoT8rzoXsiGYaKqLAmzeqLcvNCfY2bx93Jn5QJacEfNPjwDQQGq',0,2,0,'2016-08-01 15:47:59','2016-08-01 15:47:59','1'),(12,3,'shayamsharma@gmail.com','$2y$10$SoKrw8wPoDXMnNj574C6N.GoELZCVRtFNtiWDUy8950bxKUt875ii',0,2,2,'2016-08-01 15:56:43','2016-08-01 16:42:56','1'),(13,1,'ritika@gmail.com','$2y$10$RqoKMTtfMnhj4QqFzOGTYOQlybWJEyoz9k6e0CBubF96NLyhJmHom',0,2,2,'2016-08-01 16:37:39','2016-08-01 16:48:45','1'),(14,3,'surendra@gmail.com','$2y$10$p0AoExGiabqCzkg1/z2Az.cwbYV7OpcYN8z5D.AkuYWD4jXAe.cDy',0,2,0,'2016-08-01 16:41:43','2016-08-01 16:41:43','1'),(15,4,'ramu@gmail.com','$2y$10$WoAS9vgZU6jebvSQyw/Dy.lOfCie0W2xoed/DiR8pXSA1nEdxeaxW',0,2,0,'2016-08-01 16:44:37','2016-08-01 16:44:37','1'),(16,3,'test@gmail.com','$2y$10$XIMWDf2w.fopdc4sIkGIAeBxGLOiBF6xlGiUkCdu9VnWDfus/Cwk2',0,2,0,'2016-08-01 16:48:07','2016-08-01 16:48:07','1'),(17,1,'rekha@gmail.com','$2y$10$mSjoWa4n2JJ0CfXwykCWHORzeC3hLm4VLSB5gBjVYO0gWZk2nAgcu',0,2,0,'2016-08-01 17:26:10','2016-08-01 17:26:10','1'),(18,1,'anjali.alivenetsolutions@gmail.com','$2y$10$ZuqQAAd/PPPlRTUNH5oImOtPM2duIGgBUK9VlOSFa9ctKypiES4ja',0,2,2,'2016-08-01 17:49:43','2016-08-01 17:50:09','1'),(19,2,'testt@gmail.com','$2y$10$AHaYjfrG/mSPBlJD7iP4zuk4jT6vekH8LLtSEDVC5kU4bY.61PCCK',0,2,2,'2016-08-01 17:53:50','2016-08-01 17:54:33','1');
/*!40000 ALTER TABLE `lie_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_videos`
--

DROP TABLE IF EXISTS `lie_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_videos` (
  `id` int(11) NOT NULL,
  `video_title` varchar(50) NOT NULL,
  `video_name` varchar(255) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0 for incative,1 for active,2 for delete ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_videos`
--

LOCK TABLES `lie_videos` WRITE;
/*!40000 ALTER TABLE `lie_videos` DISABLE KEYS */;
INSERT INTO `lie_videos` VALUES (1,'test','1472343044218.mp4',9,2,'2016-06-28 00:00:00','2016-08-28 05:40:44','1');
/*!40000 ALTER TABLE `lie_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lie_zipcodes`
--

DROP TABLE IF EXISTS `lie_zipcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lie_zipcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lie_zipcodes`
--

LOCK TABLES `lie_zipcodes` WRITE;
/*!40000 ALTER TABLE `lie_zipcodes` DISABLE KEYS */;
INSERT INTO `lie_zipcodes` VALUES (1,11,'1010','Innere Stadt',0,0,'2016-07-19 17:17:59','2016-08-11 03:36:29','1'),(2,11,'1020','Leopoldstadt',0,0,'2016-07-19 17:23:52','2016-08-11 03:37:25','1'),(3,11,'1030','Landstraße',0,0,'2016-07-19 17:30:40','2016-08-11 03:37:48','1'),(4,11,'1040','Wieden',0,0,'2016-07-19 17:35:58','2016-08-11 03:38:15','1'),(5,11,'1110','Simmering',0,0,'2016-07-25 16:03:49','2016-08-11 03:41:54','1'),(6,12,'121002','Faridabad Ballabhgarh city',0,0,'2016-07-25 16:05:36','2016-07-25 16:10:16','2'),(7,12,'121003','Palwal city',0,0,'2016-07-25 16:06:54','2016-07-25 16:10:16','2'),(8,11,'1050','Margareten',0,0,'2016-07-29 12:37:00','2016-08-11 03:38:47','1'),(9,11,'1060','Mariahilf',0,0,'2016-07-29 12:44:40','2016-08-11 03:40:55','1'),(10,12,'11110','Special offer is being created without selecting Category - Restaurant Admin',0,0,'2016-07-30 11:31:37','2016-07-30 11:31:52','2'),(11,11,'1100','Favoriten',0,0,'2016-08-05 04:49:53','2016-08-05 04:49:53','1'),(12,11,'1120','Meidling',0,0,'2016-08-11 03:43:07','2016-08-11 03:43:07','1'),(13,11,'1130','Hietzing',0,0,'2016-08-11 03:45:48','2016-08-11 03:45:48','1'),(14,11,'1140','Penzing',0,0,'2016-08-11 03:47:19','2016-08-11 03:47:19','1'),(15,11,'1150','Rudolfsheim-Fünfhaus',0,0,'2016-08-11 03:48:02','2016-08-11 03:48:02','1'),(16,11,'1160','Ottakring',0,0,'2016-08-11 03:48:34','2016-08-11 03:48:34','1'),(17,11,'1170','Hernals',0,0,'2016-08-11 03:49:01','2016-08-11 03:49:01','1'),(18,11,'1180','Währing',0,0,'2016-08-11 03:50:14','2016-08-11 03:50:14','1'),(19,11,'1190','Döbling',0,0,'2016-08-11 03:51:06','2016-08-11 03:51:06','1'),(20,11,'1200','Brigittenau',0,0,'2016-08-11 03:51:58','2016-08-11 03:51:58','1'),(21,11,'1210','Floridsdorf',0,0,'2016-08-11 03:52:55','2016-08-11 03:53:56','1'),(22,11,'1220','Donaustadt',0,0,'2016-08-11 03:53:30','2016-08-11 03:53:30','1'),(23,11,'1230','Liesing',0,0,'2016-08-11 03:54:23','2016-08-11 03:54:23','1'),(24,11,'1070','Neubau',0,0,'2016-08-11 04:39:15','2016-08-11 04:39:15','1'),(25,11,'1080','Josefstadt',0,0,'2016-08-11 04:40:25','2016-08-11 04:40:25','1'),(26,11,'1090','Alsergrund',0,0,'2016-08-11 04:41:04','2016-08-11 04:41:04','1'),(27,11,'102345','aaaa',0,0,'2016-08-11 10:48:53','2016-08-19 11:50:39','1'),(28,32,'2500','Niederösterreich - Baden',0,0,'2016-08-31 05:09:15','2016-08-31 05:09:15','1');
/*!40000 ALTER TABLE `lie_zipcodes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-21  0:36:49
