

//////////Set Base Url/////////////

var base_host = window.location.origin;
var base_url=base_host + SITE_URL_PRE_FIX;

function fieldAdd(id,parentId,oldData,newData,removeDivString) {
	//console.log(oldData[0]);
	var arr_count=oldData.length;
	//alert(id+'-count');
	var count=$("#"+id+'-count').val();
	var increase_count=parseInt(count)+parseInt(1);

	var splitId=id.split('-');
	var splitCount=splitId.length;
	var data=$("#"+id).clone();
	//console.log(data);

/////////Replace string of privious div that in cloning to new div/////////////////////
	data[0]['id'] = data[0]['id'].replace('-'+splitId[splitCount-1],'-'+increase_count);
	data[0]['innerHTML'] = data[0]['innerHTML'].replace(removeDivString , data[0]['id']);
	data[0]['innerHTML'] = data[0]['innerHTML'].replace('class="text-danger" style="display:none;"' , 'class="text-danger" style="display:inline-block;"');
	data[0]['innerHTML'] = data[0]['innerHTML'].replace('class="text-danger new-tag"' , 'class="text-danger" style="display:none;"');

	/*for (var i = 0; i < arr_count; i++) {
		data[0]['innerHTML'] = data[0]['innerHTML'].replace(oldData[i],newData[i]);
	}*/


///////////Set New Id for new cloned div///////////////////////////
	var newId=data[0]['id'];


///////////Append new div to parent Div/////////////////
	data.appendTo("#"+parentId);
	//$("#"+parentId).append(data);

//////////// Set input box as blank of new cloned div//////////////
	$('#'+newId+':last-child input[type="text"]').val('');
	//$('#'+id).find('.text-danger').show();


//////////// Set new increase value to radio button of new cloned div//////////////
	$("#"+id+'-count').val(increase_count);
	$('#'+newId+':last-child input[type="radio"]').val(increase_count);

}

function remove_div(id){

	var splitId=id.split('-');
	var splitCount=splitId.length;
	var newId=id.replace(splitId[splitCount-1],'1');

	///////////Assign previous value of hidden field ////////////
	var hiddenCount=$("#"+newId+'-count').val();

///////////////////Remove Div/////////////////////////////
	$("#"+id).remove();

	///////////////Set decrease value to hidden field///////////////////////
	$("#"+newId+'-count').val(parseInt(hiddenCount)-parseInt(1));
}


function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn,selectedOption=0) {

	var token=$("#token-value").val();

	$.ajax({

		url: base_url+'/ajax/gatData',
        type: "POST",
        data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,
        'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn,'selectedOption':selectedOption,'_token':token},

	    success: function (data) {

			$('#'+appendId).html(data);

		},
	    error: function (jqXHR, textStatus, errorThrown) {

	    }
	});

}

function showModal(id){
	$("#"+id).modal("show");
}

////////////For check existing value ///////////////////////////////////
function checkExistingValue(firstId,secondId,tableName,firstCond,secondCond,appendId,msg,id=null) {
	var firstValue=$("#"+firstId).val();
	var secondValue=$("#"+secondId).val();

	$.ajax({

		url: base_url+'/ajax/getExistValue',
        type: "get",
        data: {'firstValue':firstValue,'secondValue':secondValue, 'table':tableName,'firstCond':firstCond,
        'secondCond':secondCond,'id':id},

	    success: function (data) {

		   if(data=='error'){
			$('#'+appendId).html(msg);
			setTimeout(function(){ $("#"+firstId).val(' '); }, 3000);

		   }
		   else{
		   	$('#'+appendId).html('');
		   }

		},
	    error: function (jqXHR, textStatus, errorThrown) {

	    }
	});

}

///////////////Delete Items///////////////////////

function delete_items(classname,inputName,tableName){
        //var len = $("."+classname+":checked").length;
        var token=$("#token-value").val();
		
		//code commented by vikas
		//var checked=$('.'+classname).is(':checked');
		//if(checked==false){
            //alert('Please select atleast one value to delete');
            //return false;
        //}
		//code commented by vikas
		
		//new code start by vikas
		var len = $("."+classname+":checked").length;
        if(len==0){
            alert('Please select atleast one value to delete');
            return false;
        }
		//end new code start by vikas

        var r = confirm("Are you sure to delete ?");
        if (r == true) {
            var selectedItems = new Array();
            $('input:checkbox[name="'+inputName+'"]:checked').each(function(){
                selectedItems.push($(this).val());
            });

            $.ajax({

                url: base_url+'/ajax/deleteItems',
                type: "POST",
                dataType:"json",
                data: {'selectedItems':selectedItems,'tableName':tableName, "_token":token},
                success: function(res){

                    if(res.success==1)
                    {
                        location.reload();
                    }
                    else
                    {
                        alert('Record not deleted');
                    }
                }
            });
        } else {
            return false;
        }
    }


 ////////////////////Select Data from singlr table (Dependency)//////////////////////

 function getDataSingleTable(cond,tableName,firstColumn,secondColumn,whereColumn,appendId){

 	var token=$("#token-value").val();

 	$.ajax({
		url: base_url+'/ajax/gatDataSingleTable',
        type: "POST",
        dataType:"json",
        data: {'cond':cond, 'table':tableName,'firstColumn':firstColumn,'secondColumn':secondColumn,
        'whereColumn':whereColumn,'_token':token},

	    success: function (data) {

	    	var option=data.option;
	    	var values=data.info;

			$('#'+appendId).html(option+values);

		},
	    error: function (jqXHR, textStatus, errorThrown) {

	    }
	});
 } 

///////////////////////////////Show and Hide value on checkbox click/////////////////////

function showValueOnCheckbox(classname,displayId) {
	var checked=$('.'+classname).is(':checked');
	if(checked==true){
		$("#"+displayId).attr("style","display:block;");
	}
	else{
		$("#"+displayId).attr("style","display:none;");
	}
}

////////////For check existing value For one condition ///////////////////////////////////

function checkExistValOneCond(firstId,tableName,firstCond,appendId,msg,id=null) {
	
	var firstValue=$("#"+firstId).val();
	var token=$("#token-value").val();
	
	$.ajax({

		url: base_url+'/front/ajax/getExistValOneCond',
		type: "POST",
		data: {'firstValue':firstValue, 'table':tableName,'firstCond':firstCond,'id':id,'_token':token},

		success: function (data) {

			if(data=='error'){
				$('#'+appendId).html(msg);
				setTimeout(function(){ $("#"+firstId).val(' '); }, 3000);

			}
			else{
				$('#'+appendId).html('');
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {

		}
	});

}

///////  Select / Deselect All Checkboxes by vikas //////////////////
function SelectDeselectAllCheckbox() {
    $(".checkBoxClass").prop('checked', $(".selectAll").prop("checked"));
}
function trimmer(id){
    $('#'+id).val($('#'+id).val().trim());
}
///////  Select / Deselect All Checkboxes by vikas //////////////////


