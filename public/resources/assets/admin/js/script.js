

//////////Set Base Url/////////////

var base_host = window.location.origin;
var base_url=base_host + SITE_URL_PRE_FIX;

function fieldAdd(id,parentId,oldData,newData,removeDivString) {
	//console.log(oldData[0]);
	var arr_count=oldData.length;
	//alert(id+'-count');
	var count=$("#"+id+'-count').val();
	var increase_count=parseInt(count)+parseInt(1);
	
	var splitId=id.split('-');
	var splitCount=splitId.length;
	var data=$("#"+id).clone();
	console.log(data);

/////////Replace string of privious div that in cloning to new div/////////////////////	
	data[0]['id'] = data[0]['id'].replace('-'+splitId[splitCount-1],'-'+increase_count);
	data[0]['innerHTML'] = data[0]['innerHTML'].replace(removeDivString , data[0]['id']);
	data[0]['innerHTML'] = data[0]['innerHTML'].replace('class="text-danger" style="display:none;"' , 'class="text-danger" style="display:inline-block;"');
	/*for (var i = 0; i < arr_count; i++) {
		data[0]['innerHTML'] = data[0]['innerHTML'].replace(oldData[i],newData[i]);
	}*/


///////////Set New Id for new cloned div///////////////////////////	
	var newId=data[0]['id'];


///////////Append new div to parent Div/////////////////	
	data.appendTo("#"+parentId);
	//$("#"+parentId).append(data);

//////////// Set input box as blank of new cloned div//////////////
	$('#'+newId+':last-child input[type="text"]').val('');
	//$('#'+id).find('.text-danger').show();
	
	
//////////// Set new increase value to radio button of new cloned div//////////////
	$("#"+id+'-count').val(increase_count);
	$('#'+newId+':last-child input[type="radio"]').val(increase_count);
	
}

function remove_div(id){
	
	var splitId=id.split('-');
	var splitCount=splitId.length;
	var newId=id.replace(splitId[splitCount-1],'1');

	///////////Assign previous value of hidden field ////////////
	var hiddenCount=$("#"+newId+'-count').val();

///////////////////Remove Div/////////////////////////////
	$("#"+id).remove(); 

	///////////////Set decrease value to hidden field///////////////////////
	$("#"+newId+'-count').val(parseInt(hiddenCount)-parseInt(1));
}


function getData(val,tableName,mapTable,appendId,joinColumn,selectColumn,whereColumn,secondJoinColumn,selectedOption=0) {

	$.ajax({

		url: base_url+'/ajax/gatData',
        type: "get",
        data: {'value':val, 'table':tableName,'maptable':mapTable,'joincolumn':joinColumn,'selectcolumn':selectColumn,
        'wherecolumn':whereColumn,'secondjoincolumn':secondJoinColumn,'selectedOption':selectedOption},
        
	    success: function (data) {
		   
			$('#'+appendId).html(data);
			
		},
	    error: function (jqXHR, textStatus, errorThrown) {
	      
	    }
	});
	
}

function showModal(id){
	$("#"+id).modal("show");
}


