-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `lie_access_lists`;
CREATE TABLE `lie_access_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_access_lists` (`id`, `access_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'Add',	'Add',	0,	0,	'0000-00-00 00:00:00',	'2016-05-19 23:17:49',	'1'),
(2,	'Edit',	'Edit',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(3,	'View',	'View',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(4,	'Delete',	'View',	0,	0,	'0000-00-00 00:00:00',	'2016-05-19 23:18:12',	'1'),
(5,	'Update',	'Update Update Update',	0,	0,	'2016-05-25 11:30:07',	'2016-05-25 11:30:20',	'2'),
(6,	'Restaurant Verwalten',	'Nur Restaurant Verwalten',	0,	0,	'2016-06-01 00:29:03',	'2016-06-01 00:29:03',	'1'),
(7,	'Bestellungen',	'Beobachten / Ansehen',	0,	0,	'2016-06-01 00:30:40',	'2016-06-01 00:31:00',	'1'),
(8,	'Bestellungen Bearbeiten',	'Bestellungen Bearbeiten / User ansicht /  Restaurant Ansicht / Benachrichtigungen Full Ansicht /',	0,	0,	'2016-06-01 00:31:54',	'2016-06-01 00:38:29',	'1');

DROP TABLE IF EXISTS `lie_access_maps`;
CREATE TABLE `lie_access_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_list_id` int(11) NOT NULL,
  `page_list_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_access_list_id` (`access_list_id`),
  KEY `lie_page_list_id` (`page_list_id`),
  KEY `lie_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_access_maps` (`id`, `access_list_id`, `page_list_id`, `user_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	2,	1,	1,	'2016-05-16 05:17:38',	'2016-05-16 05:17:38',	'2'),
(2,	2,	1,	2,	1,	1,	'2016-05-16 05:17:38',	'2016-05-16 05:17:38',	'2'),
(3,	1,	1,	2,	1,	1,	'2016-05-16 05:17:46',	'2016-05-16 05:17:46',	'2'),
(4,	2,	1,	2,	1,	1,	'2016-05-16 05:17:46',	'2016-05-16 05:17:46',	'2'),
(5,	3,	1,	2,	1,	1,	'2016-05-16 05:17:46',	'2016-05-16 05:17:46',	'2'),
(6,	2,	1,	2,	1,	1,	'2016-05-16 05:26:48',	'2016-05-16 05:26:48',	'2'),
(7,	3,	1,	2,	1,	1,	'2016-05-16 05:26:48',	'2016-05-16 05:26:48',	'2'),
(8,	1,	2,	2,	1,	1,	'2016-05-16 05:26:48',	'2016-05-16 05:26:48',	'2'),
(9,	2,	2,	2,	1,	1,	'2016-05-16 05:26:48',	'2016-05-16 05:26:48',	'2'),
(10,	1,	1,	1,	1,	1,	'2016-05-16 06:43:11',	'2016-05-16 06:43:11',	'2'),
(11,	1,	5,	1,	2,	2,	'2016-05-20 07:33:04',	'2016-05-20 07:33:04',	'2'),
(12,	2,	5,	1,	2,	2,	'2016-05-20 07:33:04',	'2016-05-20 07:33:04',	'2'),
(13,	1,	6,	1,	2,	2,	'2016-05-20 07:33:04',	'2016-05-20 07:33:04',	'2'),
(14,	2,	6,	1,	2,	2,	'2016-05-20 07:33:04',	'2016-05-20 07:33:04',	'2'),
(15,	1,	1,	1,	2,	2,	'2016-05-20 07:33:04',	'2016-05-20 07:33:04',	'2'),
(16,	3,	1,	1,	2,	2,	'2016-05-20 07:33:04',	'2016-05-20 07:33:04',	'2'),
(17,	1,	5,	1,	2,	2,	'2016-05-20 07:33:24',	'2016-05-20 07:33:24',	'2'),
(18,	2,	5,	1,	2,	2,	'2016-05-20 07:33:24',	'2016-05-20 07:33:24',	'2'),
(19,	1,	6,	1,	2,	2,	'2016-05-20 07:33:24',	'2016-05-20 07:33:24',	'2'),
(20,	2,	6,	1,	2,	2,	'2016-05-20 07:33:24',	'2016-05-20 07:33:24',	'2'),
(21,	1,	5,	1,	2,	2,	'2016-05-20 07:33:33',	'2016-05-20 07:33:33',	'2'),
(22,	2,	5,	1,	2,	2,	'2016-05-20 07:33:33',	'2016-05-20 07:33:33',	'2'),
(23,	3,	5,	1,	2,	2,	'2016-05-20 07:33:33',	'2016-05-20 07:33:33',	'2'),
(24,	1,	6,	1,	2,	2,	'2016-05-20 07:33:33',	'2016-05-20 07:33:33',	'2'),
(25,	2,	6,	1,	2,	2,	'2016-05-20 07:33:33',	'2016-05-20 07:33:33',	'2'),
(26,	1,	5,	1,	2,	2,	'2016-05-20 07:33:47',	'2016-05-20 07:33:47',	'1'),
(27,	2,	5,	1,	2,	2,	'2016-05-20 07:33:47',	'2016-05-20 07:33:47',	'1'),
(28,	3,	5,	1,	2,	2,	'2016-05-20 07:33:47',	'2016-05-20 07:33:47',	'1');

DROP TABLE IF EXISTS `lie_alergic_contents`;
CREATE TABLE `lie_alergic_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_alergic_contents` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(10,	'Mirch',	'testys',	2,	2,	'2016-05-25 09:51:11',	'2016-05-25 09:51:19',	'1'),
(11,	'turmeric',	'Test Test test',	2,	2,	'2016-05-25 11:06:36',	'2016-05-25 11:06:48',	'0');

DROP TABLE IF EXISTS `lie_cities`;
CREATE TABLE `lie_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_cities` (`id`, `country_id`, `state_id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	'Noida',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(2,	1,	2,	'Chennai',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(3,	1,	3,	'Mumbai',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(4,	1,	5,	'Kolkata',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(5,	1,	6,	'Jaipur',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(6,	1,	7,	'Chandigarh',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(7,	1,	8,	'Lucknow',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(8,	1,	9,	'Ahmedabad',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(9,	1,	10,	'Pune',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(10,	1,	11,	'Kochi',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(11,	8,	8,	'Faridabad',	'Faridabad Faridabad Faridabad',	0,	0,	'2016-05-25 10:32:29',	'2016-05-25 10:32:29',	'1');

DROP TABLE IF EXISTS `lie_color_settings`;
CREATE TABLE `lie_color_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(50) NOT NULL,
  `color_code` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_color_settings` (`id`, `color`, `color_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'yellows',	'#e9306f',	0,	2,	'2016-05-04 04:39:24',	'2016-05-25 09:51:43',	'1'),
(2,	'red',	'#ca467b',	0,	0,	'2016-05-04 10:15:39',	'2016-05-04 10:16:27',	'1'),
(3,	'ssss',	'#744848',	0,	0,	'2016-05-04 10:15:50',	'2016-05-04 10:16:03',	'2'),
(4,	'yellows',	'#a74343',	0,	0,	'2016-05-14 11:53:05',	'2016-05-14 11:53:31',	'2'),
(5,	'ssss',	'#851a1a',	3,	3,	'2016-05-16 06:09:49',	'2016-05-16 06:10:27',	'2'),
(6,	'Green',	'#50a143',	2,	2,	'2016-05-25 11:07:18',	'2016-05-25 11:07:28',	'1');

DROP TABLE IF EXISTS `lie_countries`;
CREATE TABLE `lie_countries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_countries` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(0,	'INDIAA',	'Test Test Test Test',	0,	0,	'2016-05-25 10:26:25',	'2016-05-25 10:26:56',	'2'),
(1,	'India',	' INDIA INDIA INDIA INDIA',	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:25:36',	'1'),
(2,	'France',	'France',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(3,	'America',	'America',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(5,	'Bhutan',	'Bhutan',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(6,	'Sri Lanka',	'Sri Lanka',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(7,	'UK',	'UK',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(8,	'Belgium',	'belgium',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(9,	'Brazil',	'Brazil',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(10,	'Argentina',	'Argentina',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(11,	'Russia',	'Russia',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_currencies`;
CREATE TABLE `lie_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `currency_code` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_currencies` (`id`, `name`, `currency_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4,	'Indian',	'INR',	0,	2,	'2016-05-03 12:31:51',	'2016-05-25 10:19:29',	'1'),
(6,	'dubai',	'euro',	0,	0,	'2016-05-04 10:59:23',	'2016-05-04 11:27:58',	'1'),
(8,	'usa',	'pound',	0,	2,	'2016-05-04 11:02:34',	'2016-05-25 10:19:33',	'0'),
(11,	'Australia Dollar',	'AUD',	2,	2,	'2016-05-25 11:10:45',	'2016-05-25 11:10:45',	'1');

DROP TABLE IF EXISTS `lie_discount_types`;
CREATE TABLE `lie_discount_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `type` enum('0','1') NOT NULL COMMENT '0 for ''no popup'',1 for ''popup''',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_discount_types` (`id`, `name`, `description`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'Fixed',	'Fixed',	'0',	9,	9,	'2016-05-02 00:00:00',	'2016-05-21 07:20:27',	'1'),
(2,	'Percentage',	'Percentage',	'0',	9,	9,	'2016-05-24 00:00:00',	'2016-05-21 07:20:59',	'1'),
(3,	'One free on purchase',	'One free on purchase',	'1',	9,	0,	'2016-05-31 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_email_templates`;
CREATE TABLE `lie_email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `type` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT '1 for superadmin,2 for admin,3 for users',
  `text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_email_templates` (`id`, `title`, `type`, `text`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(5,	'Login',	'1',	'Login E-Mail',	2,	2,	'2016-05-25 09:44:15',	'2016-05-25 09:44:24',	'1'),
(6,	'Just for TEst',	'3',	'Just for test',	2,	2,	'2016-05-25 11:22:40',	'2016-05-25 11:23:06',	'1');

DROP TABLE IF EXISTS `lie_email_types`;
CREATE TABLE `lie_email_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_email_types` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'corporate',	'This is a corporate email',	0,	0,	0,	0,	'1'),
(4,	'personal',	'personal',	0,	2,	2016,	2016,	'1'),
(8,	'Test',	'Just for Test',	2,	2,	2016,	2016,	'0');

DROP TABLE IF EXISTS `lie_faq_categories`;
CREATE TABLE `lie_faq_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_faq_categories` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'0',	'0',	0,	0,	'2016-05-16 07:15:17',	'2016-05-16 07:50:09',	'2'),
(2,	'Test Cat',	'test cat test cat test cat test cat',	0,	0,	'2016-05-16 07:44:32',	'2016-05-16 07:44:32',	'1');

DROP TABLE IF EXISTS `lie_faq_questions`;
CREATE TABLE `lie_faq_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_faq_questions` (`id`, `faq_category_id`, `question`, `answer`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	2,	'Test FAQ',	'test faq answer test faq answer',	0,	0,	'2016-05-16 07:45:00',	'2016-05-16 07:45:00',	'1');

DROP TABLE IF EXISTS `lie_front_city_maps`;
CREATE TABLE `lie_front_city_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_city_maps` (`id`, `city_id`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	9,	1,	0,	0,	0,	2016,	'1'),
(2,	1,	2,	0,	0,	0,	2016,	'1'),
(3,	2,	3,	0,	0,	0,	2016,	'1'),
(4,	4,	4,	0,	0,	0,	0,	'1'),
(5,	5,	5,	0,	0,	0,	0,	'1'),
(6,	6,	6,	0,	0,	0,	0,	'1'),
(7,	7,	7,	0,	0,	0,	0,	'1'),
(8,	8,	8,	0,	0,	0,	0,	'1'),
(9,	9,	9,	0,	0,	0,	0,	'1'),
(10,	10,	10,	0,	0,	0,	0,	'1');

DROP TABLE IF EXISTS `lie_front_color_maps`;
CREATE TABLE `lie_front_color_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `header_color` int(11) NOT NULL,
  `background_color` int(11) NOT NULL,
  `expire_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_front_country_maps`;
CREATE TABLE `lie_front_country_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_country_maps` (`id`, `country_id`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(2,	9,	2,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:33:56',	'1'),
(3,	3,	3,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(4,	5,	4,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(5,	8,	5,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:33:56',	'1'),
(6,	7,	6,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(7,	8,	7,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(8,	9,	8,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(9,	10,	9,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(10,	11,	10,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_front_kitchen_maps`;
CREATE TABLE `lie_front_kitchen_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kitchen_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_kitchen_maps` (`id`, `kitchen_id`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	6,	1,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:34:51',	'1'),
(2,	6,	2,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:36:37',	'1'),
(3,	6,	3,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:36:37',	'1'),
(4,	11,	4,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 11:28:24',	'1'),
(5,	6,	5,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:48:19',	'1'),
(6,	6,	6,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(7,	11,	7,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 11:28:24',	'1'),
(8,	6,	8,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:48:23',	'1'),
(9,	0,	9,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 11:28:24',	'1'),
(10,	0,	10,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 11:28:24',	'1');

DROP TABLE IF EXISTS `lie_front_pages`;
CREATE TABLE `lie_front_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `type` enum('s','d') NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_pages` (`id`, `title`, `slug`, `priority`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'test page',	'test-page-slug',	0,	's',	0,	0,	'2016-05-16 07:46:10',	'2016-05-16 07:46:15',	'1');

DROP TABLE IF EXISTS `lie_front_page_maps`;
CREATE TABLE `lie_front_page_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_page_maps` (`id`, `page_id`, `content`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'dastgsdgsdghsdvhsdhsfgvrhtjvjfdastgsdgsdghsdvhsdhsfgvrhtjvjfdastgsdgsdghsdvhsdhsfgvrhtjvjfdastgsdgsdghsdvhsdhsfgvrhtjvjfdastgsdgsdghsdvhsdhsfgvrhtjvjf',	0,	0,	'2016-05-16 07:46:10',	'2016-05-16 07:46:10',	'0');

DROP TABLE IF EXISTS `lie_front_services`;
CREATE TABLE `lie_front_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_services` (`id`, `name`, `description`, `valid_from`, `valid_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'test service',	'test service',	'2016-05-23',	'2016-05-31',	9,	1,	'2016-04-01 00:00:00',	'2016-05-16 07:12:19',	'1'),
(2,	'test service1',	'test service1',	'2016-05-24',	'2016-05-27',	9,	9,	'2016-05-12 07:54:52',	'2016-05-12 11:41:58',	'1'),
(3,	'test service2',	'test service2w',	'0000-00-00',	'0000-00-00',	9,	9,	'2016-05-12 08:14:03',	'2016-05-12 08:14:13',	'1'),
(4,	'',	'',	'0000-00-00',	'0000-00-00',	9,	0,	'2016-05-12 08:19:12',	'2016-05-12 08:20:19',	'2'),
(5,	'test service3',	'test service3',	'0000-00-00',	'0000-00-00',	9,	9,	'2016-05-12 08:27:07',	'2016-05-12 08:27:28',	'1'),
(6,	'test service4',	'test service4',	'2016-05-23',	'2016-05-31',	9,	0,	'2016-05-12 11:47:51',	'2016-05-12 11:47:51',	'1'),
(7,	'Test',	'Test Test Test Test Test test',	'2016-05-25',	'2016-06-04',	2,	2,	'2016-05-25 10:55:45',	'2016-05-25 10:56:32',	'0');

DROP TABLE IF EXISTS `lie_front_state_maps`;
CREATE TABLE `lie_front_state_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_state_maps` (`id`, `state_id`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	8,	1,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:34:21',	'1'),
(2,	2,	2,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(3,	3,	3,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(4,	5,	4,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(5,	6,	5,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(6,	7,	6,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(7,	1,	7,	0,	0,	'0000-00-00 00:00:00',	'2016-05-25 10:34:21',	'1'),
(8,	9,	8,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(9,	10,	9,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(10,	11,	10,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_front_users`;
CREATE TABLE `lie_front_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(50) NOT NULL DEFAULT '0',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_front_users` (`id`, `email`, `password`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'nikhil@gmail.com',	'$2y$10$NGgP.6Q7M89btqz.khoVz.cdT2tDqUudv0DwWg81JrNysI.44aVay',	'61YHfin46PbkNKWJUwMaKtsLVMvPaIF47y3eLjZIJIS8t1yHSb',	0,	0,	'0000-00-00 00:00:00',	'2016-06-13 07:58:47',	'0'),
(5,	'office@lieferzonas.at',	'$2y$10$j9QeQmxm/0d0ei9A85mebu0QdKwIf9fr0nWDSjddsPoLwQLACL1ga',	'0',	0,	0,	'2016-06-01 00:11:03',	'2016-06-01 00:11:03',	'0');

DROP TABLE IF EXISTS `lie_front_user_addresses`;
CREATE TABLE `lie_front_user_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `city` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `is_type` enum('p','a') NOT NULL COMMENT 'p->primary, s->secondary',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_front_user_details`;
CREATE TABLE `lie_front_user_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` enum('m','f','o') NOT NULL COMMENT 'm->male, f->female, o->other',
  `dob` date NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `registered_via` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_user_details` (`id`, `front_user_id`, `nickname`, `fname`, `lname`, `mobile`, `email`, `gender`, `dob`, `profile_pic`, `registered_via`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'',	'nikhil',	'malik',	'',	'nikhil@gmail.com',	'm',	'0000-00-00',	'',	1,	0,	0,	'2016-06-01 00:11:03',	'2016-06-01 00:11:03',	'1'),
(18,	28,	'',	'vikas',	'batra',	'',	'vikas21089@gmail.com',	'm',	'0000-00-00',	'1465804760396.jpg',	1,	0,	0,	'2016-06-13 07:59:57',	'2016-06-13 07:59:57',	'1');

DROP TABLE IF EXISTS `lie_front_user_email_verifications`;
CREATE TABLE `lie_front_user_email_verifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `is_verified` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0->no, 1->yes',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_front_user_email_verifications` (`id`, `front_user_id`, `verification_code`, `email_id`, `is_verified`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	5,	'b2ZmaWNlQGxpZWZlcnpvbmFzLmF01464739863',	'office@lieferzonas.at',	'0',	0,	0,	'2016-06-01 00:11:03',	'2016-06-01 00:11:03',	'0');

DROP TABLE IF EXISTS `lie_front_user_login_logs`;
CREATE TABLE `lie_front_user_login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_front_user_login_logs` (`id`, `front_user_id`, `type`, `ip_address`, `time`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	10,	'O',	'180.151.7.42',	'2016-06-06 08:58:17',	10,	10,	'2016-06-06 08:58:17',	'2016-06-06 08:58:17',	'1'),
(2,	10,	'O',	'180.151.7.42',	'2016-06-06 09:01:56',	10,	10,	'2016-06-06 09:01:56',	'2016-06-06 09:01:56',	'1'),
(3,	10,	'O',	'180.151.7.42',	'2016-06-06 09:45:42',	10,	10,	'2016-06-06 09:45:42',	'2016-06-06 09:45:42',	'1'),
(4,	10,	'O',	'180.151.7.42',	'2016-06-06 09:47:45',	10,	10,	'2016-06-06 09:47:45',	'2016-06-06 09:47:45',	'1'),
(5,	10,	'O',	'180.151.7.42',	'2016-06-06 09:48:04',	10,	10,	'2016-06-06 09:48:04',	'2016-06-06 09:48:04',	'1'),
(6,	10,	'O',	'180.151.7.42',	'2016-06-06 10:00:02',	10,	10,	'2016-06-06 10:00:02',	'2016-06-06 10:00:02',	'1'),
(7,	10,	'O',	'180.151.7.42',	'2016-06-06 10:09:31',	10,	10,	'2016-06-06 10:09:31',	'2016-06-06 10:09:31',	'1'),
(8,	10,	'O',	'180.151.7.42',	'2016-06-06 10:13:24',	10,	10,	'2016-06-06 10:13:24',	'2016-06-06 10:13:24',	'1'),
(9,	10,	'O',	'180.151.7.42',	'2016-06-06 10:18:11',	10,	10,	'2016-06-06 10:18:11',	'2016-06-06 10:18:11',	'1'),
(10,	10,	'O',	'180.151.7.42',	'2016-06-06 10:18:52',	10,	10,	'2016-06-06 10:18:52',	'2016-06-06 10:18:52',	'1'),
(11,	10,	'O',	'180.151.7.42',	'2016-06-06 10:19:11',	10,	10,	'2016-06-06 10:19:11',	'2016-06-06 10:19:11',	'1'),
(12,	10,	'O',	'180.151.7.42',	'2016-06-06 11:57:39',	10,	10,	'2016-06-06 11:57:39',	'2016-06-06 11:57:39',	'1'),
(13,	10,	'O',	'180.151.7.42',	'2016-06-06 12:09:26',	10,	10,	'2016-06-06 12:09:26',	'2016-06-06 12:09:26',	'1'),
(14,	1,	'O',	'180.151.7.42',	'2016-06-07 05:04:05',	1,	1,	'2016-06-07 05:04:05',	'2016-06-07 05:04:05',	'1'),
(15,	1,	'O',	'180.151.7.42',	'2016-06-07 05:10:50',	1,	1,	'2016-06-07 05:10:50',	'2016-06-07 05:10:50',	'1'),
(16,	1,	'O',	'180.151.7.42',	'2016-06-07 05:12:50',	1,	1,	'2016-06-07 05:12:50',	'2016-06-07 05:12:50',	'1'),
(17,	11,	'O',	'180.151.7.42',	'2016-06-07 05:34:18',	11,	11,	'2016-06-07 05:34:18',	'2016-06-07 05:34:18',	'1'),
(18,	12,	'L',	'180.151.7.42',	'2016-06-07 07:10:51',	12,	12,	'2016-06-07 07:10:51',	'2016-06-07 07:10:51',	'1'),
(19,	12,	'O',	'180.151.7.42',	'2016-06-07 07:11:46',	12,	12,	'2016-06-07 07:11:46',	'2016-06-07 07:11:46',	'1'),
(20,	12,	'O',	'180.151.7.42',	'2016-06-07 07:21:23',	12,	12,	'2016-06-07 07:21:23',	'2016-06-07 07:21:23',	'1'),
(21,	12,	'O',	'180.151.7.42',	'2016-06-07 07:23:36',	12,	12,	'2016-06-07 07:23:36',	'2016-06-07 07:23:36',	'1'),
(22,	12,	'L',	'180.151.7.42',	'2016-06-07 07:25:15',	12,	12,	'2016-06-07 07:25:15',	'2016-06-07 07:25:15',	'1'),
(23,	12,	'O',	'180.151.7.42',	'2016-06-07 07:26:13',	12,	12,	'2016-06-07 07:26:13',	'2016-06-07 07:26:13',	'1'),
(24,	12,	'L',	'180.151.7.42',	'2016-06-07 07:28:26',	12,	12,	'2016-06-07 07:28:26',	'2016-06-07 07:28:26',	'1'),
(25,	12,	'L',	'180.151.7.42',	'2016-06-07 07:29:04',	12,	12,	'2016-06-07 07:29:04',	'2016-06-07 07:29:04',	'1'),
(26,	12,	'O',	'180.151.7.42',	'2016-06-07 07:29:24',	12,	12,	'2016-06-07 07:29:24',	'2016-06-07 07:29:24',	'1'),
(27,	12,	'O',	'180.151.7.42',	'2016-06-07 07:41:39',	12,	12,	'2016-06-07 07:41:39',	'2016-06-07 07:41:39',	'1'),
(28,	12,	'O',	'180.151.7.42',	'2016-06-07 07:57:55',	12,	12,	'2016-06-07 07:57:55',	'2016-06-07 07:57:55',	'1'),
(29,	12,	'O',	'180.151.7.42',	'2016-06-07 08:01:38',	12,	12,	'2016-06-07 08:01:38',	'2016-06-07 08:01:38',	'1'),
(30,	12,	'O',	'180.151.7.42',	'2016-06-07 09:40:16',	12,	12,	'2016-06-07 09:40:16',	'2016-06-07 09:40:16',	'1'),
(31,	12,	'O',	'180.151.7.42',	'2016-06-07 09:43:40',	12,	12,	'2016-06-07 09:43:40',	'2016-06-07 09:43:40',	'1'),
(32,	12,	'O',	'180.151.7.42',	'2016-06-07 09:57:59',	12,	12,	'2016-06-07 09:57:59',	'2016-06-07 09:57:59',	'1'),
(33,	12,	'O',	'180.151.7.42',	'2016-06-07 10:11:37',	12,	12,	'2016-06-07 10:11:37',	'2016-06-07 10:11:37',	'1'),
(34,	12,	'O',	'180.151.7.42',	'2016-06-07 10:12:30',	12,	12,	'2016-06-07 10:12:30',	'2016-06-07 10:12:30',	'1'),
(35,	15,	'O',	'180.151.7.42',	'2016-06-07 11:41:37',	15,	15,	'2016-06-07 11:41:37',	'2016-06-07 11:41:37',	'1'),
(36,	18,	'O',	'180.151.7.42',	'2016-06-07 12:02:36',	18,	18,	'2016-06-07 12:02:36',	'2016-06-07 12:02:36',	'1'),
(37,	18,	'O',	'180.151.7.42',	'2016-06-07 12:02:59',	18,	18,	'2016-06-07 12:02:59',	'2016-06-07 12:02:59',	'1'),
(38,	18,	'O',	'180.151.7.42',	'2016-06-07 12:04:26',	18,	18,	'2016-06-07 12:04:26',	'2016-06-07 12:04:26',	'1'),
(39,	19,	'O',	'180.151.7.42',	'2016-06-07 12:07:03',	19,	19,	'2016-06-07 12:07:03',	'2016-06-07 12:07:03',	'1'),
(40,	19,	'O',	'180.151.7.42',	'2016-06-08 04:21:56',	19,	19,	'2016-06-08 04:21:56',	'2016-06-08 04:21:56',	'1'),
(41,	20,	'O',	'180.151.7.42',	'2016-06-08 04:59:44',	20,	20,	'2016-06-08 04:59:44',	'2016-06-08 04:59:44',	'1'),
(42,	20,	'O',	'180.151.7.42',	'2016-06-08 06:29:38',	20,	20,	'2016-06-08 06:29:38',	'2016-06-08 06:29:38',	'1'),
(43,	20,	'O',	'180.151.7.42',	'2016-06-08 06:30:40',	20,	20,	'2016-06-08 06:30:40',	'2016-06-08 06:30:40',	'1'),
(44,	20,	'O',	'180.151.7.42',	'2016-06-08 06:58:07',	20,	20,	'2016-06-08 06:58:07',	'2016-06-08 06:58:07',	'1'),
(45,	1,	'O',	'180.151.7.42',	'2016-06-08 07:09:10',	1,	1,	'2016-06-08 07:09:10',	'2016-06-08 07:09:10',	'1'),
(46,	21,	'O',	'180.151.7.42',	'2016-06-08 07:32:19',	21,	21,	'2016-06-08 07:32:19',	'2016-06-08 07:32:19',	'1'),
(47,	21,	'O',	'180.151.7.42',	'2016-06-08 11:13:26',	21,	21,	'2016-06-08 11:13:26',	'2016-06-08 11:13:26',	'1'),
(48,	21,	'O',	'180.151.7.42',	'2016-06-08 11:13:54',	21,	21,	'2016-06-08 11:13:54',	'2016-06-08 11:13:54',	'1'),
(49,	22,	'O',	'180.151.7.42',	'2016-06-08 11:15:50',	22,	22,	'2016-06-08 11:15:50',	'2016-06-08 11:15:50',	'1'),
(50,	1,	'O',	'180.151.7.43',	'2016-06-09 06:26:09',	1,	1,	'2016-06-09 06:26:09',	'2016-06-09 06:26:09',	'1'),
(51,	1,	'O',	'180.151.7.43',	'2016-06-09 06:26:42',	1,	1,	'2016-06-09 06:26:42',	'2016-06-09 06:26:42',	'1'),
(52,	23,	'O',	'180.151.7.43',	'2016-06-09 06:29:06',	23,	23,	'2016-06-09 06:29:06',	'2016-06-09 06:29:06',	'1'),
(53,	23,	'O',	'180.151.7.43',	'2016-06-09 06:52:46',	23,	23,	'2016-06-09 06:52:46',	'2016-06-09 06:52:46',	'1'),
(54,	23,	'O',	'180.151.7.43',	'2016-06-09 07:45:22',	23,	23,	'2016-06-09 07:45:22',	'2016-06-09 07:45:22',	'1'),
(55,	23,	'O',	'180.151.7.43',	'2016-06-09 11:34:07',	23,	23,	'2016-06-09 11:34:07',	'2016-06-09 11:34:07',	'1'),
(56,	24,	'O',	'180.151.7.43',	'2016-06-10 06:54:59',	24,	24,	'2016-06-10 06:54:59',	'2016-06-10 06:54:59',	'1'),
(57,	1,	'O',	'180.151.7.43',	'2016-06-10 10:02:05',	1,	1,	'2016-06-10 10:02:05',	'2016-06-10 10:02:05',	'1'),
(58,	23,	'O',	'180.151.7.43',	'2016-06-10 10:19:53',	23,	23,	'2016-06-10 10:19:53',	'2016-06-10 10:19:53',	'1'),
(59,	25,	'O',	'180.151.7.43',	'2016-06-10 10:23:22',	25,	25,	'2016-06-10 10:23:22',	'2016-06-10 10:23:22',	'1'),
(60,	26,	'O',	'180.151.7.43',	'2016-06-10 11:26:45',	26,	26,	'2016-06-10 11:26:45',	'2016-06-10 11:26:45',	'1'),
(61,	27,	'O',	'180.151.7.43',	'2016-06-10 11:28:48',	27,	27,	'2016-06-10 11:28:48',	'2016-06-10 11:28:48',	'1'),
(62,	27,	'O',	'180.151.7.43',	'2016-06-10 11:29:08',	27,	27,	'2016-06-10 11:29:08',	'2016-06-10 11:29:08',	'1'),
(63,	1,	'O',	'180.151.7.43',	'2016-06-10 12:45:52',	1,	1,	'2016-06-10 12:45:52',	'2016-06-10 12:45:52',	'1'),
(64,	1,	'L',	'180.151.7.43',	'2016-06-13 05:50:48',	1,	1,	'2016-06-13 05:50:48',	'2016-06-13 05:50:48',	'1'),
(65,	1,	'O',	'180.151.7.43',	'2016-06-13 06:06:07',	1,	1,	'2016-06-13 06:06:07',	'2016-06-13 06:06:07',	'1'),
(66,	1,	'O',	'180.151.7.43',	'2016-06-13 07:46:22',	1,	1,	'2016-06-13 07:46:22',	'2016-06-13 07:46:22',	'1'),
(67,	1,	'O',	'180.151.7.43',	'2016-06-13 07:58:27',	1,	1,	'2016-06-13 07:58:27',	'2016-06-13 07:58:27',	'1'),
(68,	1,	'O',	'180.151.7.43',	'2016-06-13 07:58:47',	1,	1,	'2016-06-13 07:58:47',	'2016-06-13 07:58:47',	'1');

DROP TABLE IF EXISTS `lie_front_user_mobile_verifications`;
CREATE TABLE `lie_front_user_mobile_verifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `is_verified` enum('0','1') NOT NULL COMMENT '0->no, 1->yes',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_front_user_social_maps`;
CREATE TABLE `lie_front_user_social_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `front_user_id` int(11) NOT NULL,
  `social_type` enum('F','T','G') NOT NULL COMMENT 'F->Facebook,T->Twitter,G->Google',
  `social_id` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_kitchens`;
CREATE TABLE `lie_kitchens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_kitchens` (`id`, `name`, `description`, `valid_from`, `valid_to`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(6,	'root',	'rootrootroots',	'2016-05-06 00:00:00',	'2016-05-12 00:00:00',	2,	0,	2,	'2016-05-25 09:49:23',	'2016-05-25 09:49:23',	'1'),
(7,	'test',	'adasdasd',	'2016-05-05 00:00:00',	'2016-05-12 00:00:00',	4,	0,	2,	'2016-05-25 09:49:31',	'2016-05-25 09:49:31',	'0'),
(11,	'Test Kitchen',	'Test Test test',	'2016-05-25 00:00:00',	'2016-06-04 00:00:00',	1,	2,	2,	'2016-05-25 11:06:14',	'2016-05-25 11:06:14',	'1'),
(12,	'Backwaren',	'Backwaren jeglicher art',	'2016-05-31 00:00:00',	'2019-12-29 00:00:00',	1,	2,	2,	'2016-05-31 23:32:24',	'2016-05-31 23:32:24',	'1');

DROP TABLE IF EXISTS `lie_live_feeds`;
CREATE TABLE `lie_live_feeds` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_live_feeds` (`id`, `subject`, `topic`, `description`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'New Order',	'Nikhil places new order with Bercos',	'WOW Amazing. Nikhil got 10% discount, Place now and enjoy',	'https://www.google.co.in',	1,	1,	'2016-05-04 00:00:00',	'2016-05-04 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_marketing_tips`;
CREATE TABLE `lie_marketing_tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_marketing_tips` (`id`, `topic`, `description`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'Test Tip',	'Test tip test tip test tip',	0,	0,	0,	'2016-05-16 07:13:05',	'2016-05-16 07:13:05',	'1'),
(2,	'fdcs',	'gfdcgtfrdeshnbgvfcdxsynhbgfvcdxsayhbgvfcd s',	0,	0,	0,	'2016-05-23 23:37:33',	'2016-05-23 23:37:33',	'1'),
(3,	'TEST TEST',	'TEST TESTTEST TESTTEST TEST',	0,	0,	0,	'2016-05-25 10:33:12',	'2016-05-25 10:33:12',	'1');

DROP TABLE IF EXISTS `lie_menu_categories`;
CREATE TABLE `lie_menu_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_menu_categories` (`id`, `cat_name`, `description`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(12,	'menu',	'categories',	2,	2,	2,	'2016-05-25 10:24:22',	'2016-05-25 10:24:31',	'1'),
(13,	'Test MENU',	'TEST Test',	1,	2,	2,	'2016-05-25 11:12:57',	'2016-05-25 11:12:57',	'1');

DROP TABLE IF EXISTS `lie_menu_extras`;
CREATE TABLE `lie_menu_extras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extra_type_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` varchar(6) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_menu_extras` (`id`, `extra_type_id`, `name`, `description`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	2,	'menu1',	'menu1',	'1000',	9,	1,	'2016-05-10 00:00:00',	'2016-05-16 07:22:19',	'1'),
(2,	2,	'menu4',	'menu4',	'10000',	9,	9,	'2016-05-13 10:08:39',	'2016-05-13 10:40:17',	'2'),
(3,	2,	'menu2',	'menu2',	'1000',	9,	0,	'2016-05-13 10:10:16',	'2016-05-13 10:40:07',	'2'),
(4,	2,	'menu3',	'menu3',	'1000',	9,	9,	'2016-05-13 10:10:23',	'2016-05-13 10:40:17',	'2'),
(5,	1,	'menu5',	'menu5',	'1000',	9,	0,	'2016-05-13 10:20:57',	'2016-05-13 10:20:57',	'1'),
(6,	5,	'TEst',	'Test Test',	'100',	2,	0,	'2016-05-25 11:15:26',	'2016-05-25 11:15:26',	'1');

DROP TABLE IF EXISTS `lie_menu_extra_types`;
CREATE TABLE `lie_menu_extra_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_menu_extra_types` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'test',	'test',	9,	1,	'2016-05-12 00:00:00',	'2016-05-16 07:21:09',	'1'),
(2,	'test1',	'test1',	9,	9,	'2016-05-13 06:28:52',	'2016-05-13 06:30:34',	'1'),
(3,	'test2',	'test2',	9,	9,	'2016-05-13 06:52:00',	'2016-05-13 10:40:35',	'2'),
(4,	'vvv',	'vvvvvvvvv',	11,	0,	'2016-05-14 11:57:32',	'2016-05-25 11:14:59',	'2'),
(5,	'Test New',	'Test New test new',	2,	0,	'2016-05-25 11:14:51',	'2016-05-25 11:14:51',	'1');

DROP TABLE IF EXISTS `lie_menu_price_maps`;
CREATE TABLE `lie_menu_price_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `price` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_menu_price_maps` (`id`, `rest_detail_id`, `menu_id`, `price_type_id`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	6,	5,	6,	'500',	2,	2,	'2016-05-25 11:01:49',	'2016-05-25 11:01:49',	'0');

DROP TABLE IF EXISTS `lie_metrics`;
CREATE TABLE `lie_metrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_metrics` (`id`, `name`, `code`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(2,	'MIliGram',	'mg',	'mili gram gram gram gram',	0,	2,	'0000-00-00 00:00:00',	'2016-05-25 10:28:42',	'1'),
(4,	'grams',	'gram',	'abcdabcd',	0,	2,	'2016-05-14 11:46:27',	'2016-05-25 10:28:53',	'1'),
(6,	'abcd',	'abcdabcd',	'abcdabcd',	3,	3,	'2016-05-16 06:56:14',	'2016-05-16 06:56:58',	'2'),
(7,	'Size',	'large',	'largee',	2,	2,	'2016-05-25 10:31:07',	'2016-05-25 10:31:07',	'1'),
(8,	'Test',	'Test',	'Test Test Test',	2,	2,	'2016-05-25 10:40:32',	'2016-05-25 10:41:26',	'0');

DROP TABLE IF EXISTS `lie_order_services`;
CREATE TABLE `lie_order_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_order_services` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'Delivery',	'Delivery',	9,	0,	'2016-05-24 00:00:00',	'2016-05-20 13:11:39',	'1'),
(2,	'Take away',	'Take away',	9,	0,	'2016-05-20 13:24:50',	'2016-05-20 13:24:50',	'1');

DROP TABLE IF EXISTS `lie_page_lists`;
CREATE TABLE `lie_page_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_page_lists` (`id`, `page_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(7,	'Testing',	'TestingTestings',	2,	2,	'2016-05-25 09:54:12',	'2016-05-25 09:54:17',	'1'),
(8,	'Test new',	'Test Test test',	2,	2,	'2016-05-25 11:09:08',	'2016-05-25 11:09:26',	'2');

DROP TABLE IF EXISTS `lie_price_types`;
CREATE TABLE `lie_price_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_price_types` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'delivery',	'This is a home delivery price',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(2,	'takeaway',	'This is a takeaway price',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_promocodes`;
CREATE TABLE `lie_promocodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(50) NOT NULL,
  `is_rest_specific` int(11) NOT NULL,
  `is_user_specific` int(11) NOT NULL,
  `validity_type` enum('T','C') NOT NULL COMMENT 'T for table,C for calendar',
  `validity_table` int(11) NOT NULL DEFAULT '0',
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `payment_mode` enum('P','C') NOT NULL COMMENT 'P for percentage and C for cash',
  `payment_amount` varchar(10) NOT NULL,
  `min_amount` varchar(10) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_promocodes` (`id`, `coupon_code`, `is_rest_specific`, `is_user_specific`, `validity_type`, `validity_table`, `valid_from`, `valid_to`, `payment_mode`, `payment_amount`, `min_amount`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'b6OTFhPe',	1,	1,	'T',	6,	'2016-05-14 12:40:27',	'2017-07-19 00:00:00',	'P',	'10',	'100',	9,	9,	'2016-05-09 08:20:26',	'2016-05-14 12:40:27',	'0'),
(2,	'e8igML3X',	1,	1,	'T',	6,	'2016-05-09 11:08:47',	'2017-07-13 00:00:00',	'P',	'10',	'127',	9,	9,	'2016-05-09 08:21:14',	'2016-05-09 11:08:47',	'0'),
(3,	'OXyBzSmV',	1,	1,	'C',	0,	'2016-05-30 00:00:00',	'2016-05-31 00:00:00',	'C',	'100',	'1000',	9,	9,	'2016-05-09 08:22:42',	'2016-05-14 12:09:37',	'1'),
(4,	'5fdIWgrc',	1,	1,	'C',	0,	'2016-05-17 00:00:00',	'2016-05-23 00:00:00',	'P',	'10',	'100',	9,	9,	'2016-05-09 08:24:00',	'2016-05-10 10:54:54',	'1'),
(5,	'6RQA2grJ',	1,	1,	'C',	0,	'2016-05-18 00:00:00',	'2016-05-31 00:00:00',	'P',	'10',	'100',	9,	9,	'2016-05-09 08:25:42',	'2016-05-10 12:47:43',	'1'),
(6,	'0KGLR5ui',	0,	0,	'T',	6,	'2016-05-10 12:47:51',	'2017-07-14 00:00:00',	'P',	'10',	'100',	9,	9,	'2016-05-09 08:28:48',	'2016-05-10 12:47:51',	'1'),
(7,	'LpLGCQiP',	1,	1,	'T',	6,	'2016-05-11 05:10:17',	'2017-07-15 00:00:00',	'P',	'10',	'100',	9,	9,	'2016-05-09 08:29:26',	'2016-05-11 05:10:17',	'1'),
(8,	'QSr8tTxA',	0,	0,	'T',	6,	'2016-05-09 08:30:14',	'2016-05-09 08:30:14',	'P',	'10',	'100',	9,	0,	'2016-05-09 08:30:14',	'2016-05-09 08:30:14',	'1'),
(9,	'6mSHMfJU',	0,	0,	'T',	6,	'2016-05-09 09:40:23',	'2017-10-30 00:00:00',	'P',	'10',	'100',	9,	0,	'2016-05-09 09:40:23',	'2016-05-09 09:40:23',	'1'),
(10,	'i98uIKEq',	1,	1,	'C',	0,	'2016-05-09 00:00:00',	'2017-07-10 00:00:00',	'P',	'15',	'10000',	9,	9,	'2016-05-09 10:38:23',	'2016-05-11 05:09:52',	'1'),
(11,	'p[p',	1,	0,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'C',	'',	'pp[',	9,	0,	'2016-05-09 11:44:20',	'2016-05-09 11:44:20',	'1'),
(12,	'pWEV6kvA',	1,	0,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'C',	'',	'oio',	9,	9,	'2016-05-09 11:56:40',	'2016-05-10 10:50:10',	'1'),
(13,	'TGLxtWSG',	0,	0,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'P',	'100',	'67',	9,	0,	'2016-05-10 10:49:52',	'2016-05-10 10:49:52',	'1'),
(14,	'iv9tuisT',	1,	1,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'P',	'',	'70',	9,	0,	'2016-05-10 10:54:41',	'2016-05-10 10:54:41',	'1'),
(15,	'KTQ4m4qc',	0,	0,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'P',	'100',	'1000',	9,	0,	'2016-05-10 11:22:49',	'2016-05-10 11:22:49',	'1'),
(16,	'8xG2RVWL',	0,	0,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'P',	'100',	'100',	9,	0,	'2016-05-10 11:28:35',	'2016-05-10 11:28:35',	'1'),
(17,	'cpTWR3aI',	0,	0,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'P',	'100',	'200',	9,	0,	'2016-05-10 11:29:22',	'2016-05-10 11:29:22',	'1'),
(18,	'ggg',	0,	0,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'P',	'fff',	'hhh',	9,	0,	'2016-05-10 11:52:45',	'2016-05-10 11:52:45',	'1'),
(19,	'ytuy6u',	0,	0,	'C',	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'C',	'oiuo',	'poip',	9,	0,	'2016-05-10 12:01:07',	'2016-05-10 12:01:07',	'1'),
(20,	'8ZRJ4zMI',	0,	0,	'C',	0,	'2016-05-10 00:00:00',	'2016-05-23 00:00:00',	'P',	'10',	'100',	9,	0,	'2016-05-10 12:26:08',	'2016-05-10 12:26:08',	'1'),
(21,	'sbV7U7CP',	1,	1,	'T',	7,	'2016-05-16 06:54:24',	'2017-05-30 00:00:00',	'P',	'10',	'1000',	1,	1,	'2016-05-16 06:53:51',	'2016-05-16 06:54:24',	'1'),
(22,	'SAwbC9Do',	0,	0,	'C',	0,	'2016-05-01 00:00:00',	'2016-05-27 00:00:00',	'C',	'2',	'5',	2,	0,	'2016-05-17 23:35:00',	'2016-05-17 23:35:00',	'1'),
(23,	'TEST',	1,	0,	'C',	0,	'2016-05-25 00:00:00',	'2016-05-31 00:00:00',	'C',	'200',	'600',	2,	2,	'2016-05-25 11:19:27',	'2016-05-25 11:20:04',	'1');

DROP TABLE IF EXISTS `lie_promo_rest_maps`;
CREATE TABLE `lie_promo_rest_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_promo_rest_maps` (`id`, `promo_id`, `rest_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	9,	0,	'2016-05-09 08:20:26',	'2016-05-09 08:20:26',	'2'),
(2,	2,	2,	9,	0,	'2016-05-09 08:21:14',	'2016-05-09 08:21:14',	'2'),
(3,	4,	1,	9,	0,	'2016-05-09 08:24:00',	'2016-05-09 08:24:00',	'2'),
(4,	5,	1,	9,	0,	'2016-05-09 08:25:42',	'2016-05-09 08:25:42',	'2'),
(5,	10,	2,	9,	0,	'2016-05-09 09:43:45',	'2016-05-09 09:43:45',	'2'),
(6,	10,	1,	9,	0,	'2016-05-09 10:07:34',	'2016-05-09 10:07:34',	'2'),
(7,	10,	2,	9,	0,	'2016-05-09 10:07:34',	'2016-05-09 10:07:34',	'2'),
(8,	10,	1,	9,	0,	'2016-05-09 10:07:42',	'2016-05-09 10:07:42',	'2'),
(9,	10,	2,	9,	0,	'2016-05-09 10:07:42',	'2016-05-09 10:07:42',	'2'),
(10,	10,	1,	9,	0,	'2016-05-09 10:07:55',	'2016-05-09 10:07:55',	'2'),
(11,	10,	2,	9,	0,	'2016-05-09 10:07:55',	'2016-05-09 10:07:55',	'2'),
(12,	10,	1,	9,	0,	'2016-05-09 10:08:13',	'2016-05-09 10:08:13',	'2'),
(13,	10,	2,	9,	0,	'2016-05-09 10:08:13',	'2016-05-09 10:08:13',	'2'),
(14,	10,	1,	9,	0,	'2016-05-09 10:09:06',	'2016-05-09 10:09:06',	'2'),
(15,	10,	2,	9,	0,	'2016-05-09 10:09:06',	'2016-05-09 10:09:06',	'2'),
(16,	10,	1,	9,	0,	'2016-05-09 10:11:14',	'2016-05-09 10:11:14',	'2'),
(17,	10,	2,	9,	0,	'2016-05-09 10:11:14',	'2016-05-09 10:11:14',	'2'),
(18,	10,	1,	9,	0,	'2016-05-09 10:12:12',	'2016-05-09 10:12:12',	'2'),
(19,	10,	2,	9,	0,	'2016-05-09 10:12:12',	'2016-05-09 10:12:12',	'2'),
(20,	10,	1,	9,	0,	'2016-05-09 10:12:42',	'2016-05-09 10:12:42',	'2'),
(21,	10,	2,	9,	0,	'2016-05-09 10:12:42',	'2016-05-09 10:12:42',	'2'),
(22,	10,	1,	9,	0,	'2016-05-09 10:12:53',	'2016-05-09 10:12:53',	'2'),
(23,	10,	2,	9,	0,	'2016-05-09 10:12:53',	'2016-05-09 10:12:53',	'2'),
(24,	10,	1,	9,	0,	'2016-05-09 10:13:39',	'2016-05-09 10:13:39',	'2'),
(25,	10,	2,	9,	0,	'2016-05-09 10:13:39',	'2016-05-09 10:13:39',	'2'),
(26,	10,	1,	9,	0,	'2016-05-09 10:16:35',	'2016-05-09 10:16:35',	'2'),
(27,	10,	2,	9,	0,	'2016-05-09 10:16:35',	'2016-05-09 10:16:35',	'2'),
(28,	10,	1,	9,	0,	'2016-05-09 10:17:56',	'2016-05-09 10:17:56',	'2'),
(29,	10,	2,	9,	0,	'2016-05-09 10:17:56',	'2016-05-09 10:17:56',	'2'),
(30,	10,	1,	9,	0,	'2016-05-09 10:19:44',	'2016-05-09 10:19:44',	'2'),
(31,	10,	2,	9,	0,	'2016-05-09 10:19:44',	'2016-05-09 10:19:44',	'2'),
(32,	10,	1,	9,	0,	'2016-05-09 10:20:00',	'2016-05-09 10:20:00',	'2'),
(33,	10,	2,	9,	0,	'2016-05-09 10:20:00',	'2016-05-09 10:20:00',	'2'),
(34,	10,	1,	9,	0,	'2016-05-09 10:38:12',	'2016-05-09 10:38:12',	'2'),
(35,	10,	2,	9,	0,	'2016-05-09 10:38:12',	'2016-05-09 10:38:12',	'2'),
(36,	10,	1,	9,	0,	'2016-05-09 10:38:23',	'2016-05-09 10:38:23',	'2'),
(37,	10,	2,	9,	0,	'2016-05-09 10:38:23',	'2016-05-09 10:38:23',	'2'),
(38,	10,	1,	9,	0,	'2016-05-09 10:42:03',	'2016-05-09 10:42:03',	'2'),
(39,	10,	2,	9,	0,	'2016-05-09 10:42:03',	'2016-05-09 10:42:03',	'2'),
(40,	5,	1,	9,	0,	'2016-05-09 10:45:00',	'2016-05-09 10:45:00',	'2'),
(41,	4,	1,	9,	0,	'2016-05-09 10:45:29',	'2016-05-09 10:45:29',	'2'),
(42,	3,	1,	9,	0,	'2016-05-09 10:53:37',	'2016-05-09 10:53:37',	'2'),
(43,	3,	2,	9,	0,	'2016-05-09 10:53:37',	'2016-05-09 10:53:37',	'2'),
(44,	3,	1,	9,	0,	'2016-05-09 10:57:32',	'2016-05-09 10:57:32',	'2'),
(45,	3,	2,	9,	0,	'2016-05-09 10:57:32',	'2016-05-09 10:57:32',	'2'),
(46,	3,	1,	9,	0,	'2016-05-09 10:57:54',	'2016-05-09 10:57:54',	'2'),
(47,	3,	2,	9,	0,	'2016-05-09 10:57:54',	'2016-05-09 10:57:54',	'2'),
(48,	2,	1,	9,	0,	'2016-05-09 11:05:04',	'2016-05-09 11:05:04',	'2'),
(49,	2,	2,	9,	0,	'2016-05-09 11:05:04',	'2016-05-09 11:05:04',	'2'),
(50,	1,	1,	9,	0,	'2016-05-09 11:08:19',	'2016-05-09 11:08:19',	'0'),
(51,	3,	1,	9,	0,	'2016-05-09 11:08:32',	'2016-05-09 11:08:32',	'2'),
(52,	3,	2,	9,	0,	'2016-05-09 11:08:32',	'2016-05-09 11:08:32',	'2'),
(53,	2,	1,	9,	0,	'2016-05-09 11:08:47',	'2016-05-09 11:08:47',	'0'),
(54,	2,	2,	9,	0,	'2016-05-09 11:08:47',	'2016-05-09 11:08:47',	'0'),
(55,	3,	1,	9,	0,	'2016-05-09 11:22:54',	'2016-05-09 11:22:54',	'2'),
(56,	3,	2,	9,	0,	'2016-05-09 11:22:54',	'2016-05-09 11:22:54',	'2'),
(57,	3,	1,	9,	0,	'2016-05-09 13:09:04',	'2016-05-09 13:09:04',	'2'),
(58,	3,	2,	9,	0,	'2016-05-09 13:09:04',	'2016-05-09 13:09:04',	'2'),
(59,	14,	1,	9,	0,	'2016-05-10 10:54:41',	'2016-05-10 10:54:41',	'1'),
(60,	10,	1,	9,	0,	'2016-05-11 05:09:52',	'2016-05-11 05:09:52',	'1'),
(61,	7,	1,	9,	0,	'2016-05-11 05:10:10',	'2016-05-11 05:10:10',	'2'),
(62,	7,	1,	9,	0,	'2016-05-11 05:10:17',	'2016-05-11 05:10:17',	'1'),
(63,	7,	2,	9,	0,	'2016-05-11 05:10:18',	'2016-05-11 05:10:18',	'1'),
(64,	3,	2,	9,	0,	'2016-05-12 10:47:40',	'2016-05-12 10:47:40',	'2'),
(65,	3,	2,	9,	0,	'2016-05-14 12:08:54',	'2016-05-14 12:08:54',	'2'),
(66,	3,	2,	9,	0,	'2016-05-14 12:09:14',	'2016-05-14 12:09:14',	'2'),
(67,	3,	3,	9,	0,	'2016-05-14 12:09:14',	'2016-05-14 12:09:14',	'2'),
(68,	3,	2,	9,	0,	'2016-05-14 12:09:26',	'2016-05-14 12:09:26',	'0'),
(69,	3,	3,	9,	0,	'2016-05-14 12:09:26',	'2016-05-14 12:09:26',	'0'),
(70,	21,	1,	1,	0,	'2016-05-16 06:53:51',	'2016-05-16 06:53:51',	'2'),
(71,	21,	2,	1,	0,	'2016-05-16 06:53:51',	'2016-05-16 06:53:51',	'2'),
(72,	21,	1,	1,	0,	'2016-05-16 06:54:24',	'2016-05-16 06:54:24',	'1'),
(73,	21,	2,	1,	0,	'2016-05-16 06:54:24',	'2016-05-16 06:54:24',	'1'),
(74,	23,	1,	2,	0,	'2016-05-25 11:19:27',	'2016-05-25 11:19:27',	'2'),
(75,	23,	2,	2,	0,	'2016-05-25 11:19:27',	'2016-05-25 11:19:27',	'2'),
(76,	23,	6,	2,	0,	'2016-05-25 11:19:27',	'2016-05-25 11:19:27',	'2'),
(77,	23,	1,	2,	0,	'2016-05-25 11:20:04',	'2016-05-25 11:20:04',	'1'),
(78,	23,	2,	2,	0,	'2016-05-25 11:20:04',	'2016-05-25 11:20:04',	'1'),
(79,	23,	6,	2,	0,	'2016-05-25 11:20:04',	'2016-05-25 11:20:04',	'1');

DROP TABLE IF EXISTS `lie_promo_user_maps`;
CREATE TABLE `lie_promo_user_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_promo_user_maps` (`id`, `promo_id`, `user_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	2,	9,	0,	'2016-05-09 08:20:26',	'2016-05-09 08:20:26',	'2'),
(2,	1,	4,	9,	0,	'2016-05-09 08:20:26',	'2016-05-09 08:20:26',	'2'),
(3,	2,	4,	9,	0,	'2016-05-09 08:21:14',	'2016-05-09 08:21:14',	'2'),
(4,	2,	10,	9,	0,	'2016-05-09 08:21:14',	'2016-05-09 08:21:14',	'2'),
(5,	4,	1,	9,	0,	'2016-05-09 08:24:01',	'2016-05-09 08:24:01',	'2'),
(6,	5,	1,	9,	0,	'2016-05-09 08:25:42',	'2016-05-09 08:25:42',	'2'),
(7,	10,	2,	9,	0,	'2016-05-09 09:43:45',	'2016-05-09 09:43:45',	'2'),
(8,	10,	2,	9,	0,	'2016-05-09 10:07:34',	'2016-05-09 10:07:34',	'2'),
(9,	10,	2,	9,	0,	'2016-05-09 10:07:42',	'2016-05-09 10:07:42',	'2'),
(10,	10,	10,	9,	0,	'2016-05-09 10:07:42',	'2016-05-09 10:07:42',	'2'),
(11,	10,	2,	9,	0,	'2016-05-09 10:07:55',	'2016-05-09 10:07:55',	'2'),
(12,	10,	10,	9,	0,	'2016-05-09 10:07:55',	'2016-05-09 10:07:55',	'2'),
(13,	10,	2,	9,	0,	'2016-05-09 10:08:13',	'2016-05-09 10:08:13',	'2'),
(14,	10,	10,	9,	0,	'2016-05-09 10:08:13',	'2016-05-09 10:08:13',	'2'),
(15,	10,	2,	9,	0,	'2016-05-09 10:09:06',	'2016-05-09 10:09:06',	'2'),
(16,	10,	10,	9,	0,	'2016-05-09 10:09:06',	'2016-05-09 10:09:06',	'2'),
(17,	10,	2,	9,	0,	'2016-05-09 10:11:14',	'2016-05-09 10:11:14',	'2'),
(18,	10,	10,	9,	0,	'2016-05-09 10:11:14',	'2016-05-09 10:11:14',	'2'),
(19,	10,	2,	9,	0,	'2016-05-09 10:12:12',	'2016-05-09 10:12:12',	'2'),
(20,	10,	10,	9,	0,	'2016-05-09 10:12:12',	'2016-05-09 10:12:12',	'2'),
(21,	10,	2,	9,	0,	'2016-05-09 10:12:42',	'2016-05-09 10:12:42',	'2'),
(22,	10,	10,	9,	0,	'2016-05-09 10:12:42',	'2016-05-09 10:12:42',	'2'),
(23,	10,	2,	9,	0,	'2016-05-09 10:12:53',	'2016-05-09 10:12:53',	'2'),
(24,	10,	10,	9,	0,	'2016-05-09 10:12:53',	'2016-05-09 10:12:53',	'2'),
(25,	10,	2,	9,	0,	'2016-05-09 10:13:40',	'2016-05-09 10:13:40',	'2'),
(26,	10,	10,	9,	0,	'2016-05-09 10:13:40',	'2016-05-09 10:13:40',	'2'),
(27,	10,	2,	9,	0,	'2016-05-09 10:16:35',	'2016-05-09 10:16:35',	'2'),
(28,	10,	10,	9,	0,	'2016-05-09 10:16:35',	'2016-05-09 10:16:35',	'2'),
(29,	10,	2,	9,	0,	'2016-05-09 10:17:56',	'2016-05-09 10:17:56',	'2'),
(30,	10,	10,	9,	0,	'2016-05-09 10:17:56',	'2016-05-09 10:17:56',	'2'),
(31,	10,	2,	9,	0,	'2016-05-09 10:19:44',	'2016-05-09 10:19:44',	'2'),
(32,	10,	10,	9,	0,	'2016-05-09 10:19:44',	'2016-05-09 10:19:44',	'2'),
(33,	10,	2,	9,	0,	'2016-05-09 10:20:00',	'2016-05-09 10:20:00',	'2'),
(34,	10,	10,	9,	0,	'2016-05-09 10:20:00',	'2016-05-09 10:20:00',	'2'),
(35,	10,	2,	9,	0,	'2016-05-09 10:38:12',	'2016-05-09 10:38:12',	'2'),
(36,	10,	10,	9,	0,	'2016-05-09 10:38:12',	'2016-05-09 10:38:12',	'2'),
(37,	10,	2,	9,	0,	'2016-05-09 10:38:23',	'2016-05-09 10:38:23',	'2'),
(38,	10,	10,	9,	0,	'2016-05-09 10:38:23',	'2016-05-09 10:38:23',	'2'),
(39,	10,	2,	9,	0,	'2016-05-09 10:42:03',	'2016-05-09 10:42:03',	'2'),
(40,	10,	10,	9,	0,	'2016-05-09 10:42:03',	'2016-05-09 10:42:03',	'2'),
(41,	5,	1,	9,	0,	'2016-05-09 10:45:00',	'2016-05-09 10:45:00',	'2'),
(42,	4,	1,	9,	0,	'2016-05-09 10:45:29',	'2016-05-09 10:45:29',	'2'),
(43,	3,	1,	9,	0,	'2016-05-09 10:53:37',	'2016-05-09 10:53:37',	'2'),
(44,	3,	4,	9,	0,	'2016-05-09 10:53:38',	'2016-05-09 10:53:38',	'2'),
(45,	3,	1,	9,	0,	'2016-05-09 10:57:32',	'2016-05-09 10:57:32',	'2'),
(46,	3,	4,	9,	0,	'2016-05-09 10:57:32',	'2016-05-09 10:57:32',	'2'),
(47,	3,	1,	9,	0,	'2016-05-09 10:57:54',	'2016-05-09 10:57:54',	'2'),
(48,	3,	4,	9,	0,	'2016-05-09 10:57:54',	'2016-05-09 10:57:54',	'2'),
(49,	2,	4,	9,	0,	'2016-05-09 11:05:04',	'2016-05-09 11:05:04',	'2'),
(50,	2,	10,	9,	0,	'2016-05-09 11:05:04',	'2016-05-09 11:05:04',	'2'),
(51,	1,	2,	9,	0,	'2016-05-09 11:08:19',	'2016-05-09 11:08:19',	'0'),
(52,	1,	4,	9,	0,	'2016-05-09 11:08:19',	'2016-05-09 11:08:19',	'0'),
(53,	3,	1,	9,	0,	'2016-05-09 11:08:32',	'2016-05-09 11:08:32',	'2'),
(54,	3,	4,	9,	0,	'2016-05-09 11:08:32',	'2016-05-09 11:08:32',	'2'),
(55,	2,	4,	9,	0,	'2016-05-09 11:08:47',	'2016-05-09 11:08:47',	'0'),
(56,	2,	10,	9,	0,	'2016-05-09 11:08:47',	'2016-05-09 11:08:47',	'0'),
(57,	3,	1,	9,	0,	'2016-05-09 11:22:54',	'2016-05-09 11:22:54',	'2'),
(58,	3,	4,	9,	0,	'2016-05-09 11:22:54',	'2016-05-09 11:22:54',	'2'),
(59,	3,	1,	9,	0,	'2016-05-09 13:09:04',	'2016-05-09 13:09:04',	'2'),
(60,	3,	4,	9,	0,	'2016-05-09 13:09:04',	'2016-05-09 13:09:04',	'2'),
(61,	4,	1,	9,	0,	'2016-05-09 13:09:20',	'2016-05-09 13:09:20',	'2'),
(62,	3,	1,	9,	0,	'2016-05-09 13:09:34',	'2016-05-09 13:09:34',	'2'),
(63,	3,	4,	9,	0,	'2016-05-09 13:09:34',	'2016-05-09 13:09:34',	'2'),
(64,	5,	1,	9,	0,	'2016-05-09 13:09:49',	'2016-05-09 13:09:49',	'2'),
(65,	3,	1,	9,	0,	'2016-05-10 10:49:58',	'2016-05-10 10:49:58',	'2'),
(66,	3,	4,	9,	0,	'2016-05-10 10:49:58',	'2016-05-10 10:49:58',	'2'),
(67,	3,	1,	9,	0,	'2016-05-10 10:51:57',	'2016-05-10 10:51:57',	'2'),
(68,	3,	4,	9,	0,	'2016-05-10 10:51:57',	'2016-05-10 10:51:57',	'2'),
(69,	3,	1,	9,	0,	'2016-05-10 10:52:06',	'2016-05-10 10:52:06',	'2'),
(70,	3,	4,	9,	0,	'2016-05-10 10:52:06',	'2016-05-10 10:52:06',	'2'),
(71,	3,	1,	9,	0,	'2016-05-10 10:54:49',	'2016-05-10 10:54:49',	'2'),
(72,	3,	4,	9,	0,	'2016-05-10 10:54:49',	'2016-05-10 10:54:49',	'2'),
(73,	4,	1,	9,	0,	'2016-05-10 10:54:54',	'2016-05-10 10:54:54',	'1'),
(74,	3,	1,	9,	0,	'2016-05-10 12:06:12',	'2016-05-10 12:06:12',	'2'),
(75,	3,	4,	9,	0,	'2016-05-10 12:06:12',	'2016-05-10 12:06:12',	'2'),
(76,	3,	1,	9,	0,	'2016-05-10 12:09:55',	'2016-05-10 12:09:55',	'2'),
(77,	3,	4,	9,	0,	'2016-05-10 12:09:55',	'2016-05-10 12:09:55',	'2'),
(78,	5,	1,	9,	0,	'2016-05-10 12:47:43',	'2016-05-10 12:47:43',	'1'),
(79,	10,	2,	9,	0,	'2016-05-11 05:09:52',	'2016-05-11 05:09:52',	'1'),
(80,	10,	4,	9,	0,	'2016-05-11 05:09:52',	'2016-05-11 05:09:52',	'1'),
(81,	10,	10,	9,	0,	'2016-05-11 05:09:52',	'2016-05-11 05:09:52',	'1'),
(82,	7,	1,	9,	0,	'2016-05-11 05:10:10',	'2016-05-11 05:10:10',	'2'),
(83,	7,	1,	9,	0,	'2016-05-11 05:10:18',	'2016-05-11 05:10:18',	'1'),
(84,	3,	1,	9,	0,	'2016-05-12 10:47:40',	'2016-05-12 10:47:40',	'2'),
(85,	3,	4,	9,	0,	'2016-05-12 10:47:40',	'2016-05-12 10:47:40',	'2'),
(86,	21,	1,	1,	0,	'2016-05-16 06:53:51',	'2016-05-16 06:53:51',	'2'),
(87,	21,	2,	1,	0,	'2016-05-16 06:53:51',	'2016-05-16 06:53:51',	'2'),
(88,	21,	1,	1,	0,	'2016-05-16 06:54:24',	'2016-05-16 06:54:24',	'1'),
(89,	21,	2,	1,	0,	'2016-05-16 06:54:24',	'2016-05-16 06:54:24',	'1');

DROP TABLE IF EXISTS `lie_rest_admin_contact_us`;
CREATE TABLE `lie_rest_admin_contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_admin_contact_us` (`id`, `topic`, `subject`, `message`, `email`, `phone`, `image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(6,	'testing',	'testing',	'testing',	'ramesh.alivenetsolution@gmail.com',	'1234567890',	'1464173067991.jpg',	1,	1,	'2016-05-25 10:44:27',	'2016-05-25 10:44:27',	'1'),
(7,	'vikas',	'asdasdasd',	'asdasdasdas',	'asdadas@gmail.com',	'7876222011',	'1464175109497.jpg',	1,	1,	'2016-05-25 11:18:29',	'2016-05-25 11:18:29',	'1'),
(8,	'hjkl',	'office',	'iopüiopü',	'fffffffffffffffff@live.at',	'532155622555',	'1464226805845.png',	1,	1,	'2016-05-26 01:40:05',	'2016-05-26 01:40:05',	'1'),
(9,	'hi testing',	'hi testing',	'hi testing',	'rajlakshmi017@gmail.com',	'9656565656',	'1465277421596.pdf',	1,	1,	'2016-06-07 05:30:21',	'2016-06-07 05:30:21',	'1'),
(10,	'hi testing',	'hi testing',	'hi testing',	'rajlakshmi017@gmail.com',	'9656565656',	'1465277548466.pdf',	1,	1,	'2016-06-07 05:32:28',	'2016-06-07 05:32:28',	'1'),
(11,	'hi testing',	'hi testing',	'hi testing',	'rajlakshmi017@gmail.com',	'9656565656',	'1465277751139.pdf',	1,	1,	'2016-06-07 05:35:51',	'2016-06-07 05:35:51',	'1'),
(12,	'retettet',	'etewtet',	'fsfsf ewr eeqw2q',	'rajlakshmi017@gmail.com',	'9656565656',	'1465278017269.pdf',	1,	1,	'2016-06-07 05:40:17',	'2016-06-07 05:40:17',	'1'),
(13,	'retettet',	'etewtet',	'fsfsf ewr eeqw2q',	'rajlakshmi017@gmail.com',	'9656565656',	'1465278028552.pdf',	1,	1,	'2016-06-07 05:40:28',	'2016-06-07 05:40:28',	'1'),
(14,	'retettet',	'etewtet',	'fsfsf ewr eeqw2q',	'rajlakshmi017@gmail.com',	'9656565656',	'1465278137582.pdf',	1,	1,	'2016-06-07 05:42:17',	'2016-06-07 05:42:17',	'1'),
(15,	'tretretertert',	'ertretreter',	'yedertwesr rwe2wewqe',	'rajlakshmi017@gmail.com',	'9656565656',	'1465278248454.pdf',	1,	1,	'2016-06-07 05:44:08',	'2016-06-07 05:44:08',	'1'),
(16,	'tretretertert',	'ertretreter',	'yedertwesr rwe2wewqe',	'rajlakshmi017@gmail.com',	'9656565656',	'1465278403996.pdf',	1,	1,	'2016-06-07 05:46:43',	'2016-06-07 05:46:43',	'1'),
(17,	'tretretertert',	'ertretreter',	'yedertwesr rwe2wewqe',	'rajlakshmi017@gmail.com',	'9656565656',	'1465278474785.pdf',	1,	1,	'2016-06-07 05:47:54',	'2016-06-07 05:47:54',	'1'),
(18,	'fsdgg',	'hdfhdfh',	'tetertert',	'rajlakshmi017@gmail.com',	'9656565656',	'1465278610273.pdf',	1,	1,	'2016-06-07 05:50:10',	'2016-06-07 05:50:10',	'1'),
(19,	'fsdgg',	'hdfhdfh',	'tetertert',	'rajlakshmi017@gmail.com',	'9656565656',	'1465279716409.pdf',	1,	1,	'2016-06-07 06:08:36',	'2016-06-07 06:08:36',	'1'),
(20,	'fsdgg',	'hdfhdfh',	'tetertert',	'rajlakshmi017@gmail.com',	'9656565656',	'1465280280597.pdf',	1,	1,	'2016-06-07 06:18:00',	'2016-06-07 06:18:00',	'1'),
(21,	'fsdgg',	'hdfhdfh',	'tetertert',	'rajlakshmi017@gmail.com',	'9656565656',	'1465280346248.jpg',	1,	1,	'2016-06-07 06:19:06',	'2016-06-07 06:19:06',	'1'),
(22,	'tertretre',	'eteter',	'yryrt frgtry',	'rajlakshmi017@gmail.com',	'9656565656',	'1465280429609.jpg',	1,	1,	'2016-06-07 06:20:29',	'2016-06-07 06:20:29',	'1'),
(23,	'hi testing',	'hi testing',	'hi testing here.',	'rajlakshmi.alivenetsolution@gmail.com',	'9656565656',	'1465361531553.jpg',	1,	1,	'2016-06-08 04:52:11',	'2016-06-08 04:52:11',	'1');

DROP TABLE IF EXISTS `lie_rest_categories`;
CREATE TABLE `lie_rest_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `root_cat_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_categories` (`id`, `rest_detail_id`, `root_cat_id`, `name`, `description`, `image`, `valid_from`, `valid_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	3,	'test12356',	'q',	'1463382189303.jpg',	'2016-05-25 00:00:00',	'2016-05-31 00:00:00',	3,	3,	'2016-05-16 07:03:09',	'2016-05-16 07:19:42',	'1'),
(3,	3,	3,	'tzhjk',	'dfztuimk,lö',	'',	'2016-05-18 00:00:00',	'2016-06-29 00:00:00',	2,	2,	'2016-05-17 23:22:17',	'2016-05-17 23:22:17',	'1'),
(4,	6,	13,	'Test',	'Test Test Test',	'1464173913188.jpg',	'2016-05-25 00:00:00',	'2016-06-04 00:00:00',	2,	2,	'2016-05-25 10:58:33',	'2016-05-25 10:59:04',	'1');

DROP TABLE IF EXISTS `lie_rest_cust_bonus`;
CREATE TABLE `lie_rest_cust_bonus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` float(16,2) NOT NULL,
  `type` enum('p','c') NOT NULL DEFAULT 'p' COMMENT 'p for percent,c for cash',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_cust_bonus` (`id`, `value`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	60.00,	'p',	2,	2,	'2016-05-25 11:23:28',	'2016-05-25 11:23:28',	'1');

DROP TABLE IF EXISTS `lie_rest_cust_bonus_maps`;
CREATE TABLE `lie_rest_cust_bonus_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `bonus_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_cust_bonus_maps` (`id`, `rest_detail_id`, `bonus_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(3,	2,	'0',	2,	2,	'2016-05-25 10:25:51',	'2016-05-25 10:25:51',	'1'),
(4,	1,	'1',	2,	2,	'2016-05-25 10:26:32',	'2016-06-03 23:32:54',	'1'),
(5,	6,	'1',	2,	2,	'2016-05-25 11:16:11',	'2016-05-25 11:16:11',	'1');

DROP TABLE IF EXISTS `lie_rest_days`;
CREATE TABLE `lie_rest_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_days` (`id`, `day`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'Monday',	0,	0,	'2016-04-28 10:59:37',	'2016-04-28 10:59:37',	'1'),
(2,	'Tuesday',	0,	0,	'2016-04-28 10:59:37',	'2016-04-28 10:59:37',	'0'),
(3,	'Wednesday',	0,	0,	'2016-04-28 10:59:37',	'2016-04-28 10:59:37',	'0'),
(4,	'Thursday',	0,	0,	'2016-04-28 10:59:37',	'2016-04-28 10:59:37',	'0'),
(5,	'Friday',	0,	0,	'2016-04-28 10:59:37',	'2016-04-28 10:59:37',	'0'),
(6,	'Saturday',	0,	0,	'2016-04-28 10:59:37',	'2016-04-28 10:59:37',	'0'),
(7,	'Sunday',	0,	0,	'2016-04-28 10:59:37',	'2016-04-28 10:59:37',	'0');

DROP TABLE IF EXISTS `lie_rest_day_time_maps`;
CREATE TABLE `lie_rest_day_time_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `open_time` varchar(50) NOT NULL,
  `close_time` varchar(50) NOT NULL,
  `is_open` int(11) NOT NULL,
  `is_24` int(11) NOT NULL COMMENT '1 for open, 0 for close',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_day_time_maps` (`id`, `rest_detail_id`, `day_id`, `open_time`, `close_time`, `is_open`, `is_24`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(3,	1,	4,	'00:00 AM',	'11:59 PM',	1,	1,	3,	3,	'2016-05-04 04:40:32',	'2016-05-16 06:17:25',	'1'),
(4,	1,	5,	'12:00 PM',	'04:30 PM',	1,	0,	3,	3,	'2016-05-04 04:40:32',	'2016-05-16 06:17:25',	'1'),
(5,	1,	6,	'01:30 AM',	'06:00 PM',	1,	0,	0,	0,	'2016-05-04 10:17:26',	'2016-05-14 12:58:40',	'1'),
(6,	2,	4,	'00:00 AM',	'11:59 PM',	1,	0,	0,	0,	'2016-05-04 10:22:20',	'2016-05-14 12:58:40',	'1'),
(7,	2,	5,	'00:00 AM',	'11:59 PM',	1,	1,	0,	0,	'2016-05-04 10:28:06',	'2016-05-14 12:58:40',	'1'),
(8,	2,	6,	'00:00 AM',	'11:59 PM',	1,	1,	0,	0,	'2016-05-04 10:32:31',	'2016-05-14 12:58:40',	'1'),
(9,	5,	4,	'08:30 AM',	'10:00 AM',	1,	0,	3,	3,	'2016-05-14 11:53:58',	'2016-05-16 06:17:25',	'1'),
(10,	5,	5,	'00:00 AM',	'00:30 AM',	1,	0,	3,	3,	'2016-05-16 06:17:11',	'2016-05-16 06:17:11',	'1'),
(11,	5,	6,	'01:00 AM',	'03:30 AM',	1,	0,	3,	3,	'2016-05-16 06:17:11',	'2016-05-16 06:17:11',	'1'),
(12,	1,	7,	'00:00 AM',	'11:59 PM',	1,	1,	3,	3,	'2016-05-04 04:40:32',	'2016-05-16 06:17:25',	'1'),
(13,	1,	1,	'00:00 AM',	'11:59 PM',	1,	1,	3,	3,	'2016-05-04 04:40:32',	'2016-05-16 06:17:25',	'1'),
(14,	1,	2,	'00:00 AM',	'11:59 PM',	1,	1,	3,	3,	'2016-05-04 04:40:32',	'2016-05-16 06:17:25',	'1'),
(15,	1,	3,	'00:00 AM',	'11:59 PM',	1,	1,	3,	3,	'2016-05-04 04:40:32',	'2016-05-16 06:17:25',	'1'),
(16,	2,	7,	'00:00 AM',	'11:59 PM',	1,	0,	0,	0,	'2016-05-04 10:22:20',	'2016-05-14 12:58:40',	'1'),
(17,	2,	1,	'00:00 AM',	'11:59 PM',	1,	0,	0,	0,	'2016-05-04 10:22:20',	'2016-05-14 12:58:40',	'1'),
(18,	2,	2,	'00:00 AM',	'11:59 PM',	1,	0,	0,	0,	'2016-05-04 10:22:20',	'2016-05-14 12:58:40',	'1'),
(19,	2,	3,	'00:00 AM',	'11:59 PM',	1,	0,	0,	0,	'2016-05-04 10:22:20',	'2016-05-14 12:58:40',	'1'),
(20,	5,	7,	'08:30 AM',	'10:00 AM',	1,	0,	3,	3,	'2016-05-14 11:53:58',	'2016-05-16 06:17:25',	'1'),
(21,	5,	1,	'08:30 AM',	'10:00 AM',	1,	0,	3,	3,	'2016-05-14 11:53:58',	'2016-05-16 06:17:25',	'1'),
(22,	5,	2,	'08:30 AM',	'10:00 AM',	1,	0,	3,	3,	'2016-05-14 11:53:58',	'2016-05-16 06:17:25',	'1'),
(23,	5,	3,	'08:30 AM',	'10:00 AM',	1,	0,	3,	3,	'2016-05-14 11:53:58',	'2016-05-16 06:17:25',	'1');

DROP TABLE IF EXISTS `lie_rest_details`;
CREATE TABLE `lie_rest_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `add1` varchar(100) NOT NULL,
  `add2` varchar(100) NOT NULL,
  `city` varchar(40) NOT NULL,
  `state` varchar(40) NOT NULL,
  `country` varchar(40) NOT NULL,
  `pincode` varchar(6) NOT NULL,
  `tin_number` varchar(50) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `total_email` int(11) NOT NULL,
  `total_mobile` int(11) NOT NULL,
  `total_landline` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_details` (`id`, `f_name`, `l_name`, `image`, `add1`, `add2`, `city`, `state`, `country`, `pincode`, `tin_number`, `latitude`, `longitude`, `fax`, `total_email`, `total_mobile`, `total_landline`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'food',	'plaza',	'logo_de.png',	'gandhi chowk',	'ewrwerewr',	'1',	'1',	'1',	'1',	'3343434',	'28.5561624',	'77.0999578',	'5845621',	0,	0,	0,	0,	1,	'2016-04-26 07:52:03',	'2016-05-25 08:01:31',	'1'),
(2,	'Annapurna',	'',	'logo_de.png',	'motihari',	'',	'2',	'2',	'2',	'2',	'',	'28.6139391',	'77.2090212',	'125478',	0,	0,	0,	0,	0,	'2016-04-26 07:54:40',	'2016-04-26 07:54:40',	'1'),
(3,	'Suniti',	'Sweets',	'logo_de.png',	'gandhi chowk',	'',	'5',	'5',	'5',	'5',	'',	'',	'',	'345345',	0,	0,	0,	0,	0,	'2016-04-26 07:57:07',	'2016-04-26 07:57:07',	'1'),
(4,	'jayeka',	'',	'logo_de.png',	'meena bazar',	'',	'5',	'5',	'5',	'5',	'',	'',	'',	'125412',	0,	0,	0,	0,	0,	'2016-04-27 05:05:54',	'2016-04-27 05:05:54',	'1'),
(5,	'jayeka',	'hotel',	'logo_de.png',	'main road',	'motihari',	'6',	'6',	'6',	'6',	'',	'28.6129167',	'77.227321',	'25415784',	2,	2,	1,	2,	2,	'2016-05-25 08:10:29',	'2016-05-25 10:39:13',	'1');

DROP TABLE IF EXISTS `lie_rest_email_maps`;
CREATE TABLE `lie_rest_email_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `email_type` varchar(30) NOT NULL,
  `is_primary` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_email_maps` (`id`, `rest_detail_id`, `email`, `email_type`, `is_primary`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'food@fmail.com',	'1',	'0',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(2,	1,	'kishan@gmail.com',	'2',	'1',	0,	1,	'0000-00-00 00:00:00',	'2016-06-07 11:43:36',	'0'),
(3,	2,	'raj@gmail.com',	'1',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(4,	3,	'suniti@gmail.com',	'1',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(5,	4,	'jayeka@gmail.com',	'1',	'0',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(6,	4,	'roshan@gmail.com',	'2',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(17,	5,	'gokul@gmail.com',	'1',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(18,	5,	'gk@gmail.com',	'',	'0',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(20,	6,	'test@gmail.com',	'1',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_rest_extra_maps`;
CREATE TABLE `lie_rest_extra_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_extra_type_id` int(11) NOT NULL,
  `menu_extra_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_rest_extra_type_maps`;
CREATE TABLE `lie_rest_extra_type_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_extra_type_id` int(11) NOT NULL,
  `price` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_extra_type_maps` (`id`, `rest_detail_id`, `menu_extra_type_id`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(2,	1,	2,	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(5,	1,	1,	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(7,	3,	1,	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(8,	3,	2,	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(9,	1,	4,	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(10,	2,	2,	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(11,	6,	1,	'',	2,	2,	'2016-05-25 11:03:20',	'2016-05-25 11:03:20',	'1'),
(12,	6,	2,	'',	2,	2,	'2016-05-25 11:03:20',	'2016-05-25 11:03:20',	'1'),
(13,	6,	4,	'',	2,	2,	'2016-05-25 11:03:20',	'2016-05-25 11:03:20',	'1');

DROP TABLE IF EXISTS `lie_rest_kitchen_maps`;
CREATE TABLE `lie_rest_kitchen_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_kitchen_maps` (`id`, `rest_detail_id`, `kitchen_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(12,	3,	2,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(47,	1111,	2,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(48,	1111,	6,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(52,	1,	6,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(54,	6,	6,	2,	2,	'2016-05-25 10:59:34',	'2016-05-25 10:59:34',	'1'),
(55,	5,	6,	2,	2,	'2016-05-25 10:59:58',	'2016-05-25 10:59:58',	'1');

DROP TABLE IF EXISTS `lie_rest_landline_maps`;
CREATE TABLE `lie_rest_landline_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `landline` varchar(15) NOT NULL,
  `extension` varchar(6) NOT NULL,
  `name` varchar(40) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_landline_maps` (`id`, `rest_detail_id`, `landline`, `extension`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	3,	'30181',	'152',	'Counter',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(2,	3,	'30181',	'155',	'Acoount',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(3,	4,	'21542',	'211',	'test',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(4,	4,	'21542',	'144',	'251',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(11,	5,	'06252',	'30182',	'roshan',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_rest_menus`;
CREATE TABLE `lie_rest_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `rest_category_id` int(11) NOT NULL,
  `kitchen_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(150) NOT NULL,
  `code` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `info` varchar(255) NOT NULL,
  `priority` varchar(20) NOT NULL,
  `have_submenu` enum('0','1') NOT NULL COMMENT '''0=>no submenu,1=>having submenu',
  `is_alergic` enum('0','1') NOT NULL COMMENT '0=>no alergic,1=>its alergic',
  `is_spicy` enum('0','1') NOT NULL COMMENT '0=>no spicy,1=>its spicy',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_menus` (`id`, `rest_detail_id`, `rest_category_id`, `kitchen_id`, `name`, `short_name`, `code`, `description`, `image`, `info`, `priority`, `have_submenu`, `is_alergic`, `is_spicy`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4,	1,	1,	6,	'fdsfsdf',	'dsfdsf',	'dsf43',	'sdfsdf',	'1463382355980.jpg',	'',	'4',	'0',	'0',	'0',	3,	3,	'2016-05-16 07:05:55',	'2016-05-16 07:05:55',	'1'),
(5,	6,	4,	6,	'Test Restauro',	'Test',	'Test',	'Test Test TEst',	'',	'',	'5',	'0',	'0',	'0',	2,	2,	'2016-05-25 11:01:49',	'2016-05-25 11:01:49',	'1');

DROP TABLE IF EXISTS `lie_rest_menu_allergic_maps`;
CREATE TABLE `lie_rest_menu_allergic_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `allergic_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_rest_menu_spice_maps`;
CREATE TABLE `lie_rest_menu_spice_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `spice_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_rest_mobile_maps`;
CREATE TABLE `lie_rest_mobile_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_primary` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_mobile_maps` (`id`, `rest_detail_id`, `mobile`, `name`, `is_primary`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'9587451254',	'Account',	'1',	0,	1,	'0000-00-00 00:00:00',	'2016-06-07 11:43:36',	'0'),
(2,	1,	'9651248769',	'HR',	'0',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(3,	2,	'9654875412',	'Owner',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(4,	3,	'8547821546',	'Owner',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(5,	4,	'9587451254',	'Owner',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(6,	4,	'9654875412',	'Account',	'0',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(14,	5,	'9542154785',	'alok',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(16,	6,	'989999999',	'test',	'1',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_rest_new_cust_bonus_logs`;
CREATE TABLE `lie_rest_new_cust_bonus_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `is_new_cust_bonus` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `type` enum('S','A') NOT NULL DEFAULT 'S' COMMENT 'S for superadmin, A for admin',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_new_cust_bonus_logs` (`id`, `rest_detail_id`, `is_new_cust_bonus`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(5,	2,	'0',	'S',	2,	2,	'2016-05-25 10:25:51',	'2016-05-25 10:25:51',	'1'),
(6,	1,	'0',	'S',	2,	2,	'2016-05-25 10:26:32',	'2016-05-25 10:26:32',	'1'),
(7,	6,	'1',	'S',	2,	2,	'2016-05-25 11:16:11',	'2016-05-25 11:16:11',	'1'),
(8,	1,	'1',	'S',	2,	2,	'2016-06-03 23:32:54',	'2016-06-03 23:32:54',	'1');

DROP TABLE IF EXISTS `lie_rest_notifications`;
CREATE TABLE `lie_rest_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `send_type` enum('1','2') NOT NULL COMMENT '1 for all,2 for custom resturant',
  `is_type` enum('1','2') NOT NULL COMMENT '1 for notification,2 for sms',
  `is_read` enum('0','1') NOT NULL COMMENT '0 for unread,1 for read',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_notifications` (`id`, `title`, `comment`, `send_type`, `is_type`, `is_read`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'test1',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry.\r\nLorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',	'1',	'1',	'0',	9,	9,	'2016-05-17 06:46:22',	'2016-05-17 10:47:58',	'1'),
(2,	'test2',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry.\r\nLorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',	'2',	'1',	'0',	9,	9,	'2016-05-17 08:16:55',	'2016-05-17 08:17:08',	'1'),
(3,	'test3',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry.\r\nLorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',	'2',	'1',	'0',	9,	0,	'2016-05-17 08:21:34',	'2016-05-17 08:21:34',	'1'),
(4,	'test4',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry.\r\nLorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',	'2',	'1',	'0',	9,	0,	'2016-05-17 10:06:33',	'2016-05-17 10:06:33',	'1'),
(5,	'test5',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry.\r\nLorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',	'1',	'1',	'0',	9,	0,	'2016-05-10 00:00:00',	'0000-00-00 00:00:00',	'1'),
(6,	'test6',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry.\r\nLorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',	'1',	'1',	'0',	9,	0,	'2016-05-10 00:00:00',	'0000-00-00 00:00:00',	'1'),
(7,	'test7',	'Lorem Ipsum is simply dummy text of the printing and typesetting industry.\r\nLorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',	'1',	'1',	'0',	9,	0,	'2016-05-16 00:00:00',	'0000-00-00 00:00:00',	'1'),
(8,	'Bindu Test',	'Test Test Test Test',	'2',	'1',	'0',	2,	2,	'2016-05-25 10:39:19',	'2016-05-25 10:39:36',	'1'),
(9,	'hallo achtung Bitte',	'tfzbuimk,öblvkcrxysxdcfvghbnjmk,lkjhg fdyxrdctfvhbgnijmk,lnjbvgcdxsyaesxrdcfvzbgunmjk,l',	'1',	'1',	'0',	2,	0,	'2016-06-04 00:28:39',	'2016-06-04 00:29:07',	'2');

DROP TABLE IF EXISTS `lie_rest_notification_maps`;
CREATE TABLE `lie_rest_notification_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_notification_maps` (`id`, `rest_detail_id`, `notification_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	9,	0,	'2016-05-17 06:46:22',	'2016-05-17 06:46:22',	'2'),
(2,	2,	1,	9,	0,	'2016-05-17 06:46:22',	'2016-05-17 06:46:22',	'2'),
(3,	1,	1,	9,	0,	'2016-05-17 07:48:14',	'2016-05-17 07:48:14',	'2'),
(4,	1,	1,	9,	0,	'2016-05-17 07:50:05',	'2016-05-17 07:50:05',	'2'),
(5,	1,	1,	9,	0,	'2016-05-17 07:50:30',	'2016-05-17 07:50:30',	'2'),
(6,	1,	2,	9,	0,	'2016-05-17 08:16:55',	'2016-05-17 08:16:55',	'2'),
(7,	1,	2,	9,	0,	'2016-05-17 08:17:08',	'2016-05-17 08:17:08',	'1'),
(8,	1,	3,	9,	0,	'2016-05-17 08:21:34',	'2016-05-17 08:21:34',	'1'),
(9,	1,	4,	9,	0,	'2016-05-17 10:06:33',	'2016-05-17 10:06:33',	'1'),
(10,	1,	1,	9,	0,	'2016-05-17 10:44:52',	'2016-05-17 10:44:52',	'0'),
(11,	1,	1,	9,	0,	'2016-05-17 10:47:58',	'2016-05-17 10:47:58',	'0'),
(12,	2,	1,	9,	0,	'2016-05-17 10:47:58',	'2016-05-17 10:47:58',	'0'),
(13,	2,	8,	2,	0,	'2016-05-25 10:39:36',	'2016-05-25 10:39:36',	'1');

DROP TABLE IF EXISTS `lie_rest_owners`;
CREATE TABLE `lie_rest_owners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `contact_no` varchar(12) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(50) NOT NULL DEFAULT '0',
  `validity_type` enum('T','C') NOT NULL COMMENT 'T for table,C for calendar',
  `validity_table` int(11) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_owners` (`id`, `rest_detail_id`, `firstname`, `lastname`, `contact_no`, `email`, `password`, `remember_token`, `validity_type`, `validity_table`, `valid_from`, `valid_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'vikas',	'batra',	'1234567890',	'batra@gmail.com',	'$2y$10$NGgP.6Q7M89btqz.khoVz.cdT2tDqUudv0DwWg81JrNysI.44aVay',	'0XunYFvbk70kXgLpUIFJpZUCdnVMOKWzhwzBTXoojtTBZTN7Bp',	'T',	6,	'2016-05-14 11:48:21',	'2017-07-18 00:00:00',	0,	9,	'0000-00-00 00:00:00',	'2016-06-13 06:05:58',	'0'),
(3,	6,	'Bindu',	'Kharub',	'97856989',	'bindukharub@gmail.com',	'$2y$10$kq/0J7d81czJoWbKd52kqu6zVzkWOozGltRZUYYP6mjANi24JIqte',	'0',	'C',	0,	'2016-05-25 00:00:00',	'2016-05-31 00:00:00',	2,	2,	'2016-05-25 10:55:08',	'2016-05-25 10:55:23',	'1');

DROP TABLE IF EXISTS `lie_rest_owner_login_logs`;
CREATE TABLE `lie_rest_owner_login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `rest_owner_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_owner_login_logs` (`id`, `rest_id`, `rest_owner_id`, `type`, `ip_address`, `time`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	'L',	'180.151.7.42',	'2016-05-25 10:42:10',	1,	1,	'2016-05-25 10:42:10',	'2016-05-25 10:42:10',	'1'),
(2,	1,	1,	'L',	'180.151.7.42',	'2016-05-25 11:17:41',	1,	1,	'2016-05-25 11:17:41',	'2016-05-25 11:17:41',	'1'),
(3,	1,	1,	'L',	'180.151.7.42',	'2016-05-25 11:28:43',	1,	1,	'2016-05-25 11:28:43',	'2016-05-25 11:28:43',	'1'),
(4,	1,	1,	'L',	'80.110.78.199',	'2016-05-25 11:52:19',	1,	1,	'2016-05-25 11:52:19',	'2016-05-25 11:52:19',	'1'),
(5,	1,	1,	'L',	'93.111.6.108',	'2016-05-25 17:19:15',	1,	1,	'2016-05-25 17:19:15',	'2016-05-25 17:19:15',	'1'),
(6,	1,	1,	'O',	'93.111.6.108',	'2016-05-25 17:21:36',	1,	1,	'2016-05-25 17:21:36',	'2016-05-25 17:21:36',	'1'),
(7,	1,	1,	'L',	'80.110.78.199',	'2016-05-26 01:38:18',	1,	1,	'2016-05-26 01:38:18',	'2016-05-26 01:38:18',	'1'),
(8,	1,	1,	'L',	'180.151.7.42',	'2016-05-26 04:58:03',	1,	1,	'2016-05-26 04:58:03',	'2016-05-26 04:58:03',	'1'),
(9,	1,	1,	'L',	'180.151.7.42',	'2016-05-26 05:19:44',	1,	1,	'2016-05-26 05:19:44',	'2016-05-26 05:19:44',	'1'),
(10,	1,	1,	'L',	'80.110.78.199',	'2016-05-30 23:11:23',	1,	1,	'2016-05-30 23:11:23',	'2016-05-30 23:11:23',	'1'),
(11,	1,	1,	'L',	'80.110.78.199',	'2016-06-01 19:42:24',	1,	1,	'2016-06-01 19:42:24',	'2016-06-01 19:42:24',	'1'),
(12,	1,	1,	'L',	'80.110.78.199',	'2016-06-02 23:37:48',	1,	1,	'2016-06-02 23:37:48',	'2016-06-02 23:37:48',	'1'),
(13,	1,	1,	'L',	'180.151.7.43',	'2016-06-03 12:38:46',	1,	1,	'2016-06-03 12:38:46',	'2016-06-03 12:38:46',	'1'),
(14,	1,	1,	'O',	'180.151.7.43',	'2016-06-03 12:38:48',	1,	1,	'2016-06-03 12:38:48',	'2016-06-03 12:38:48',	'1'),
(15,	1,	1,	'L',	'80.110.78.199',	'2016-06-04 00:25:42',	1,	1,	'2016-06-04 00:25:42',	'2016-06-04 00:25:42',	'1'),
(16,	1,	1,	'L',	'80.110.78.199',	'2016-06-05 00:44:34',	1,	1,	'2016-06-05 00:44:34',	'2016-06-05 00:44:34',	'1'),
(17,	1,	1,	'L',	'213.47.175.30',	'2016-06-06 19:37:02',	1,	1,	'2016-06-06 19:37:02',	'2016-06-06 19:37:02',	'1'),
(18,	1,	1,	'L',	'180.151.7.42',	'2016-06-07 04:50:16',	1,	1,	'2016-06-07 04:50:16',	'2016-06-07 04:50:16',	'1'),
(19,	1,	1,	'L',	'180.151.7.42',	'2016-06-07 07:29:27',	1,	1,	'2016-06-07 07:29:27',	'2016-06-07 07:29:27',	'1'),
(20,	1,	1,	'L',	'80.110.111.223',	'2016-06-08 00:37:33',	1,	1,	'2016-06-08 00:37:33',	'2016-06-08 00:37:33',	'1'),
(21,	1,	1,	'L',	'180.151.7.42',	'2016-06-08 04:51:18',	1,	1,	'2016-06-08 04:51:18',	'2016-06-08 04:51:18',	'1'),
(22,	1,	1,	'L',	'180.151.7.42',	'2016-06-08 07:42:46',	1,	1,	'2016-06-08 07:42:46',	'2016-06-08 07:42:46',	'1'),
(23,	1,	1,	'L',	'180.151.7.43',	'2016-06-13 06:04:17',	1,	1,	'2016-06-13 06:04:17',	'2016-06-13 06:04:17',	'1'),
(24,	1,	1,	'O',	'180.151.7.43',	'2016-06-13 06:05:58',	1,	1,	'2016-06-13 06:05:58',	'2016-06-13 06:05:58',	'1'),
(25,	1,	1,	'L',	'180.151.7.43',	'2016-06-13 09:44:25',	1,	1,	'2016-06-13 09:44:25',	'2016-06-13 09:44:25',	'1'),
(26,	1,	1,	'L',	'180.151.7.43',	'2016-06-13 09:54:04',	1,	1,	'2016-06-13 09:54:04',	'2016-06-13 09:54:04',	'1');

DROP TABLE IF EXISTS `lie_rest_price_maps`;
CREATE TABLE `lie_rest_price_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_price_maps` (`id`, `rest_detail_id`, `price_type_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(11,	5,	6,	2,	2,	'2016-05-25 10:25:34',	'2016-05-25 10:25:34',	'0'),
(13,	6,	6,	2,	2,	'2016-05-25 10:53:17',	'2016-05-25 10:53:17',	'0');

DROP TABLE IF EXISTS `lie_rest_privilege`;
CREATE TABLE `lie_rest_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactiive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_rest_privileges`;
CREATE TABLE `lie_rest_privileges` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactiive,1=>active,2=>deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_privileges` (`id`, `name`, `image`, `description`, `valid_from`, `valid_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'normal',	'',	'sfdfsdfsdf',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(2,	'featured',	'',	'dfgfdgdfg',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1');

DROP TABLE IF EXISTS `lie_rest_privilege_logs`;
CREATE TABLE `lie_rest_privilege_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL COMMENT '1=>normal,2=>featured',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_privilege_logs` (`id`, `rest_detail_id`, `privilege_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	5,	0,	2,	2,	'2016-05-25 09:48:04',	'2016-05-25 09:48:04',	'1'),
(2,	6,	0,	2,	2,	'2016-05-25 10:53:02',	'2016-05-25 10:53:02',	'1'),
(3,	6,	2,	2,	2,	'2016-05-25 11:04:07',	'2016-05-25 11:04:07',	'1');

DROP TABLE IF EXISTS `lie_rest_root_cat_maps`;
CREATE TABLE `lie_rest_root_cat_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `root_cat_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_root_cat_maps` (`id`, `rest_detail_id`, `root_cat_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(2,	2,	3,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(10,	1,	3,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(11,	3,	3,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(12,	3,	4,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(14,	6,	13,	2,	2,	'2016-05-25 10:57:48',	'2016-05-25 10:57:48',	'1');

DROP TABLE IF EXISTS `lie_rest_search_filters`;
CREATE TABLE `lie_rest_search_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `low_value` int(11) NOT NULL,
  `high_value` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_search_filters` (`id`, `name`, `low_value`, `high_value`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'Price',	5,	100,	1,	1,	'2016-05-04 00:00:00',	'2016-05-25 10:38:25',	'1'),
(2,	'Distance',	1,	10,	1,	1,	'2016-05-04 00:00:00',	'2016-05-04 14:16:03',	'1');

DROP TABLE IF EXISTS `lie_rest_service_maps`;
CREATE TABLE `lie_rest_service_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_service_maps` (`id`, `service_id`, `rest_detail_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(10,	1,	2,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(11,	2,	2,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(12,	3,	2,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(19,	2,	3,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(20,	3,	3,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(21,	6,	3,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(22,	1,	1,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(23,	2,	1,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(24,	3,	1,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(25,	5,	1,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(26,	1,	4,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(27,	2,	4,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(29,	1,	6,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0'),
(30,	2,	6,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'0');

DROP TABLE IF EXISTS `lie_rest_settings`;
CREATE TABLE `lie_rest_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `is_open` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>closed,1=>open',
  `is_new` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=>not new,1=>new',
  `header_color` varchar(100) NOT NULL,
  `body_color` varchar(100) NOT NULL,
  `privilege_id` int(11) NOT NULL COMMENT '1=>normal,2=>featured',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_settings` (`id`, `rest_detail_id`, `is_open`, `is_new`, `header_color`, `body_color`, `privilege_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	5,	'1',	'0',	'#ffffff',	'#ffffff',	0,	2,	2,	'2016-05-25 09:48:04',	'2016-05-25 09:48:04',	'1'),
(2,	6,	'1',	'1',	'#ca467b',	'#ca467b',	2,	2,	2,	'2016-05-25 10:53:02',	'2016-05-25 11:04:07',	'1');

DROP TABLE IF EXISTS `lie_rest_stamps`;
CREATE TABLE `lie_rest_stamps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stamp_count` int(11) NOT NULL DEFAULT '0',
  `value` float(16,2) NOT NULL,
  `type` enum('p','c') NOT NULL DEFAULT 'p' COMMENT 'p for percent,c for cash',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_stamps` (`id`, `stamp_count`, `value`, `type`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	20,	10.00,	'p',	2,	2,	'2016-05-25 11:24:07',	'2016-05-25 11:24:07',	'1');

DROP TABLE IF EXISTS `lie_rest_stamp_maps`;
CREATE TABLE `lie_rest_stamp_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL DEFAULT '0',
  `stamp_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for off, 1 for on',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_stamp_maps` (`id`, `rest_detail_id`, `stamp_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4,	2,	'0',	2,	2,	'2016-05-25 10:25:43',	'2016-05-25 10:25:43',	'1'),
(5,	6,	'1',	2,	2,	'2016-05-25 11:15:41',	'2016-05-25 11:15:41',	'1');

DROP TABLE IF EXISTS `lie_rest_submenu_allergic_maps`;
CREATE TABLE `lie_rest_submenu_allergic_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `allergic_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_rest_submenu_spice_maps`;
CREATE TABLE `lie_rest_submenu_spice_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `spice_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_rest_sub_menus`;
CREATE TABLE `lie_rest_sub_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `short_name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `info` varchar(255) NOT NULL,
  `code` varchar(150) NOT NULL,
  `priority` varchar(150) NOT NULL,
  `image` text NOT NULL,
  `is_alergic` enum('0','1') NOT NULL COMMENT '0=>no alergic,1=>its alergic',
  `is_spicy` enum('0','1') NOT NULL COMMENT '0=>no spicy,1=>its spicy',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_rest_sub_menus` (`id`, `rest_detail_id`, `menu_id`, `name`, `short_name`, `description`, `info`, `code`, `priority`, `image`, `is_alergic`, `is_spicy`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	2,	1,	'big lassi',	'lassi',	'sdfsdf',	'dfsf',	'ls254',	'12',	'1463034884753.jpg',	'0',	'0',	9,	9,	'2016-05-12 06:34:44',	'2016-05-12 10:00:35',	'1'),
(2,	1,	2,	'veg single',	'veg',	'sdfdsf',	'dfsds',	'piz1254',	'2',	'',	'0',	'0',	9,	9,	'2016-05-12 06:46:33',	'2016-05-12 06:46:33',	'1'),
(3,	1,	3,	'wewe',	'ww',	'wewqe',	'wew',	'we23',	'2',	'1463046422811.jpg',	'0',	'0',	9,	9,	'2016-05-12 09:47:02',	'2016-05-12 09:47:02',	'2'),
(4,	2,	1,	'big lassi',	'lassi',	'sdfsdf',	'dfsf',	'ls254',	'11',	'',	'0',	'0',	9,	9,	'2016-05-12 09:59:15',	'2016-05-12 09:59:15',	'1'),
(5,	1,	2,	'dsfdsf',	'dsdsf',	'dgdfg',	'fdg',	'ee334',	'4',	'',	'0',	'0',	9,	9,	'2016-05-14 12:35:41',	'2016-05-14 12:35:41',	'1'),
(6,	6,	5,	'Test',	'Test1',	'Test Test Test',	'',	'TEst1',	'4',	'1464174169239.jpg',	'0',	'0',	2,	2,	'2016-05-25 11:02:49',	'2016-05-25 11:02:49',	'1');

DROP TABLE IF EXISTS `lie_rest_tax_maps`;
CREATE TABLE `lie_rest_tax_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `tax_setting_id` int(11) NOT NULL,
  `tax_type` varchar(20) NOT NULL,
  `tax_amount` varchar(15) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`),
  KEY `lie_tax_setting_id` (`tax_setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_rest_zipcode_delivery_maps`;
CREATE TABLE `lie_rest_zipcode_delivery_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zipcode_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_rest_zipcode_delivery_maps` (`id`, `zipcode_id`, `rest_id`, `zipcode`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	3,	2,	101010,	0,	2,	'0000-00-00 00:00:00',	'2016-05-27 06:35:28',	'1'),
(6,	2,	1,	23232,	0,	2,	'2016-05-26 12:14:18',	'2016-05-26 12:29:25',	'1'),
(7,	3,	1,	101010,	0,	2,	'2016-05-26 12:14:18',	'2016-05-26 12:29:25',	'1'),
(8,	3,	5,	101010,	2,	2,	'2016-05-26 12:29:43',	'2016-05-26 12:40:57',	'1'),
(9,	4,	5,	122020,	2,	2,	'2016-05-26 12:29:43',	'2016-05-26 12:40:57',	'1');

DROP TABLE IF EXISTS `lie_root_categories`;
CREATE TABLE `lie_root_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_root_categories` (`id`, `name`, `description`, `valid_from`, `valid_to`, `priority`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(13,	'DemoCat',	'Here heres',	'2016-05-06 00:00:00',	'2016-05-12 00:00:00',	3,	2,	2,	'2016-05-25 09:46:48',	'2016-05-25 09:46:54',	'1'),
(14,	'Test',	'Test Test',	'2016-05-25 00:00:00',	'2016-06-02 00:00:00',	1,	2,	2,	'2016-05-25 11:05:22',	'2016-05-25 11:05:33',	'1');

DROP TABLE IF EXISTS `lie_service_rest_maps`;
CREATE TABLE `lie_service_rest_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `service_setting_id` int(11) NOT NULL,
  `expire_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_rest_detail_id` (`rest_detail_id`),
  KEY `lie_service_setting_id` (`service_setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_service_settings`;
CREATE TABLE `lie_service_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_service_settings` (`id`, `service_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4,	'service',	'serviceservice',	2,	2,	2016,	2016,	'0'),
(5,	'test Service',	'test Service test Service',	2,	2,	2016,	2016,	'1');

DROP TABLE IF EXISTS `lie_set_bonus_points`;
CREATE TABLE `lie_set_bonus_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_title` varchar(50) NOT NULL,
  `points` varchar(5) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_set_bonus_points` (`id`, `activity_title`, `points`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'test',	'75',	'test',	9,	2,	'2016-05-10 00:00:00',	'2016-05-25 11:21:41',	'1'),
(2,	'test1',	'80',	'test',	9,	1,	'2016-05-10 00:00:00',	'2016-05-16 07:09:37',	'1');

DROP TABLE IF EXISTS `lie_set_cashback_points`;
CREATE TABLE `lie_set_cashback_points` (
  `id` int(11) NOT NULL,
  `cashback_title` varchar(50) NOT NULL,
  `points` varchar(5) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_set_cashback_points` (`id`, `cashback_title`, `points`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'test',	'50',	'test',	9,	2,	'2016-05-09 00:00:00',	'2016-05-25 11:21:28',	'1'),
(2,	'test1',	'70',	'test',	9,	1,	'2016-05-09 00:00:00',	'2016-05-16 07:09:14',	'1');

DROP TABLE IF EXISTS `lie_shop_categories`;
CREATE TABLE `lie_shop_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_shop_categories` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'abcd',	'testtest',	3,	2,	'2016-05-14 11:47:59',	'2016-05-25 10:29:07',	'1'),
(3,	'abcds',	'abcdabcd',	3,	3,	'2016-05-16 07:08:38',	'2016-05-16 07:09:17',	'2'),
(4,	'abcds',	'sdsdsd',	3,	3,	'2016-05-16 07:09:24',	'2016-05-16 07:09:31',	'2'),
(5,	'TEST',	'TEST TEST TEST 123',	2,	2,	'2016-05-25 10:42:00',	'2016-05-25 10:42:09',	'1'),
(6,	'TEST NEw',	'TEST TEST',	2,	2,	'2016-05-25 10:42:37',	'2016-05-25 10:42:44',	'2');

DROP TABLE IF EXISTS `lie_shop_inventories`;
CREATE TABLE `lie_shop_inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_category_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `quantity` float(16,2) NOT NULL DEFAULT '0.00',
  `price` float(16,2) NOT NULL DEFAULT '0.00',
  `action` enum('P','M') NOT NULL DEFAULT 'P' COMMENT 'P for Item Added & M for Item Used',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_shop_inventories` (`id`, `shop_category_id`, `shop_item_id`, `quantity`, `price`, `action`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(5,	1,	1,	20.00,	440.00,	'P',	11,	11,	'2016-05-14 11:50:13',	'2016-05-14 11:50:13',	'1'),
(6,	1,	2,	50.00,	2500.00,	'P',	3,	3,	'2016-05-16 07:29:25',	'2016-05-16 07:29:25',	'1'),
(7,	1,	1,	40.00,	880.00,	'P',	3,	3,	'2016-05-16 07:29:25',	'2016-05-16 07:29:25',	'1'),
(8,	1,	4,	10.00,	600.00,	'P',	2,	2,	'2016-05-25 10:32:38',	'2016-05-25 10:32:38',	'1'),
(9,	1,	4,	1.00,	60.00,	'M',	1,	1,	'2016-05-25 10:43:12',	'2016-05-25 10:43:12',	'1'),
(10,	5,	5,	5.00,	3000.00,	'P',	2,	2,	'2016-05-25 10:45:46',	'2016-05-25 10:45:46',	'1'),
(11,	1,	2,	100.00,	6000.00,	'P',	2,	2,	'2016-05-25 23:06:07',	'2016-05-25 23:06:07',	'1'),
(12,	1,	4,	1.00,	60.00,	'M',	1,	1,	'2016-05-31 23:25:51',	'2016-05-31 23:25:51',	'1'),
(13,	1,	2,	3.00,	180.00,	'M',	1,	1,	'2016-05-31 23:25:51',	'2016-05-31 23:25:51',	'1'),
(14,	1,	4,	1.00,	60.00,	'M',	1,	1,	'2016-06-01 19:44:41',	'2016-06-01 19:44:41',	'1'),
(15,	5,	5,	1.00,	600.00,	'M',	1,	1,	'2016-06-01 19:44:41',	'2016-06-01 19:44:41',	'1'),
(16,	1,	2,	1.00,	60.00,	'M',	1,	1,	'2016-06-01 19:44:41',	'2016-06-01 19:44:41',	'1'),
(17,	1,	4,	2.00,	120.00,	'M',	1,	1,	'2016-06-07 00:24:33',	'2016-06-07 00:24:33',	'1');

DROP TABLE IF EXISTS `lie_shop_items`;
CREATE TABLE `lie_shop_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_category_id` int(11) NOT NULL,
  `metric_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `item_number` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float(16,2) NOT NULL,
  `front_price` float(16,2) NOT NULL,
  `quantity` float(16,2) NOT NULL DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_shop_items` (`id`, `shop_category_id`, `metric_id`, `name`, `item_number`, `description`, `price`, `front_price`, `quantity`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	4,	'daal',	'item 1',	'sdsdsdsd vvvvv',	22.00,	333.00,	0.00,	3,	2,	'2016-05-25 10:30:05',	'2016-05-25 10:30:05',	'1'),
(2,	1,	2,	'cheeni',	'item 2',	'vvvvvvvvv',	50.00,	60.00,	96.00,	3,	2,	'2016-05-18 06:47:52',	'2016-06-01 19:44:41',	'1'),
(3,	1,	2,	'dddd',	'item 3',	'sdasdas',	22.00,	33.00,	0.00,	3,	3,	'2016-05-16 07:26:15',	'2016-05-16 07:26:24',	'2'),
(4,	1,	7,	'shirt',	'item 3',	'[1] Die Sätze dieses Textes bilden eine sprachliche Einheit. In Grenzfällen kann man auch von Ein-Wort-Texten sprechen, etwa bei Ausrufen.\r\n[1] „Die Auswahl von Texten und ihre Zusammenstellung in einem Textkorpus ist ebenso wie die theoretische Verankerung des Analysevorhabens ein methodischer Vorgang, der in hohem Maße Einfluß auf die Ergebnisse hat.“[3]\r\n[1] „Texte sind, wie jede kulturelle Gestaltung - ob Bilder, Gebäude oder Partituren -, geordnete Mengen von Elementen.“[4]\r\n[2] Der Schauspieler musste lachen und konnte sich nicht mehr auf seinen Text konzentrieren.',	50.00,	60.00,	5.00,	2,	2,	'2016-06-04 02:12:26',	'2016-06-07 00:24:33',	'1'),
(5,	5,	7,	'Saree',	'item new',	'Test Test',	500.00,	600.00,	4.00,	2,	2,	'2016-05-25 10:45:03',	'2016-06-01 19:44:41',	'1');

DROP TABLE IF EXISTS `lie_shop_metric_categories`;
CREATE TABLE `lie_shop_metric_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_category_id` int(11) NOT NULL,
  `metric_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_shop_metric_categories` (`id`, `shop_category_id`, `metric_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(7,	1,	2,	2,	2,	'2016-05-25 10:31:29',	'2016-05-25 10:31:29',	'1'),
(8,	1,	4,	2,	2,	'2016-05-25 10:31:29',	'2016-05-25 10:31:29',	'1'),
(9,	1,	7,	2,	2,	'2016-05-25 10:31:29',	'2016-05-25 10:31:29',	'1'),
(10,	5,	7,	2,	2,	'2016-05-25 10:43:07',	'2016-05-25 10:43:07',	'1');

DROP TABLE IF EXISTS `lie_shop_orders`;
CREATE TABLE `lie_shop_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `total_price` float(16,2) NOT NULL DEFAULT '0.00',
  `discount` float(16,2) NOT NULL DEFAULT '0.00',
  `grand_total` float(16,2) NOT NULL DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_shop_orders` (`id`, `order_id`, `rest_detail_id`, `total_price`, `discount`, `grand_total`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(5,	'Order-1',	1,	60.00,	0.00,	60.00,	1,	1,	'2016-05-25 10:43:12',	'2016-05-25 10:43:12',	'1'),
(6,	'Order-6',	1,	240.00,	0.00,	240.00,	1,	1,	'2016-05-31 23:25:51',	'2016-05-31 23:25:51',	'1'),
(7,	'Order-7',	1,	720.00,	0.00,	720.00,	1,	1,	'2016-06-01 19:44:41',	'2016-06-01 19:44:41',	'1'),
(8,	'Order-8',	1,	120.00,	0.00,	120.00,	1,	1,	'2016-06-07 00:24:33',	'2016-06-07 00:24:33',	'1');

DROP TABLE IF EXISTS `lie_shop_rest_cart_maps`;
CREATE TABLE `lie_shop_rest_cart_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `rest_detail_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `quantity` float(16,2) NOT NULL DEFAULT '0.00',
  `price` float(16,2) NOT NULL DEFAULT '0.00',
  `discount` float(16,2) NOT NULL DEFAULT '0.00',
  `grand_total` float(16,2) NOT NULL DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_shop_rest_cart_maps` (`id`, `order_id`, `rest_detail_id`, `shop_item_id`, `quantity`, `price`, `discount`, `grand_total`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(11,	'Order-1',	1,	4,	1.00,	60.00,	0.00,	60.00,	1,	1,	'2016-05-25 10:43:12',	'2016-05-25 10:43:12',	'1'),
(12,	'Order-6',	1,	4,	1.00,	60.00,	0.00,	60.00,	1,	1,	'2016-05-31 23:25:51',	'2016-05-31 23:25:51',	'1'),
(13,	'Order-6',	1,	2,	3.00,	180.00,	0.00,	180.00,	1,	1,	'2016-05-31 23:25:51',	'2016-05-31 23:25:51',	'1'),
(14,	'Order-7',	1,	4,	1.00,	60.00,	0.00,	60.00,	1,	1,	'2016-06-01 19:44:41',	'2016-06-01 19:44:41',	'1'),
(15,	'Order-7',	1,	5,	1.00,	600.00,	0.00,	600.00,	1,	1,	'2016-06-01 19:44:41',	'2016-06-01 19:44:41',	'1'),
(16,	'Order-7',	1,	2,	1.00,	60.00,	0.00,	60.00,	1,	1,	'2016-06-01 19:44:41',	'2016-06-01 19:44:41',	'1'),
(17,	'Order-8',	1,	4,	2.00,	120.00,	0.00,	120.00,	1,	1,	'2016-06-07 00:24:33',	'2016-06-07 00:24:33',	'1');

DROP TABLE IF EXISTS `lie_shop_rest_cart_temps`;
CREATE TABLE `lie_shop_rest_cart_temps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `shop_item_id` int(11) NOT NULL,
  `quantity` float(16,2) NOT NULL DEFAULT '0.00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;


DROP TABLE IF EXISTS `lie_social_networks`;
CREATE TABLE `lie_social_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `google` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `vk` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `pinterest` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_social_networks` (`id`, `facebook`, `twitter`, `youtube`, `google`, `linkedin`, `vk`, `instagram`, `pinterest`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'https://www.facebook.com/Lieferzonas.at',	'twitter',	'youtube',	'google',	'linkedin',	'vk',	'instagram',	'pinterest',	1,	1,	'2016-05-03 00:00:00',	'2016-06-01 00:23:34',	'1');

DROP TABLE IF EXISTS `lie_social_types`;
CREATE TABLE `lie_social_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_special_offers`;
CREATE TABLE `lie_special_offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_id` int(11) NOT NULL,
  `validity_type` enum('T','C') NOT NULL COMMENT 'T for table,C for calendar',
  `type` varchar(10) NOT NULL,
  `validity_table` int(11) NOT NULL DEFAULT '0',
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `discount_type_id` int(11) NOT NULL,
  `original_price` varchar(6) NOT NULL,
  `discount_price` varchar(6) NOT NULL,
  `discount_type_value` varchar(50) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','3') NOT NULL COMMENT '1 for active->confirmed,0 for deactive->pending,3 for rejection,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_special_offers` (`id`, `rest_id`, `validity_type`, `type`, `validity_table`, `valid_from`, `valid_to`, `discount_type_id`, `original_price`, `discount_price`, `discount_type_value`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'T',	'1',	6,	'2016-05-28 08:16:04',	'2017-08-01 00:00:00',	1,	'100',	'10',	'10',	4,	4,	'2016-05-27 10:28:12',	'2016-05-28 08:16:04',	'0'),
(2,	1,	'T',	'2',	6,	'2016-05-27 11:35:26',	'2017-07-31 00:00:00',	3,	'1000',	'100',	'10',	4,	0,	'2016-05-27 11:35:26',	'2016-05-27 11:35:26',	'0'),
(3,	1,	'T',	'1',	6,	'2016-05-27 12:54:21',	'2017-07-31 00:00:00',	1,	'11',	'11',	'gf',	4,	4,	'2016-05-27 11:36:02',	'2016-05-27 12:54:21',	'0');

DROP TABLE IF EXISTS `lie_special_offer_cat_maps`;
CREATE TABLE `lie_special_offer_cat_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `special_offer_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_special_offer_cat_maps` (`id`, `category_id`, `special_offer_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	4,	0,	'2016-05-27 10:28:13',	'2016-05-27 10:28:13',	'2'),
(2,	1,	1,	4,	0,	'2016-05-27 10:38:29',	'2016-05-27 10:38:29',	'2'),
(3,	1,	1,	4,	0,	'2016-05-27 10:39:34',	'2016-05-27 10:39:34',	'2'),
(4,	1,	1,	4,	0,	'2016-05-27 10:39:34',	'2016-05-27 10:39:34',	'2'),
(5,	1,	1,	4,	0,	'2016-05-27 10:39:44',	'2016-05-27 10:39:44',	'2'),
(6,	1,	1,	4,	0,	'2016-05-27 10:43:23',	'2016-05-27 10:43:23',	'2'),
(7,	1,	1,	4,	0,	'2016-05-27 10:53:45',	'2016-05-27 10:53:45',	'2'),
(8,	1,	3,	4,	0,	'2016-05-27 11:36:02',	'2016-05-27 11:36:02',	'2'),
(9,	1,	3,	4,	0,	'2016-05-27 11:36:02',	'2016-05-27 11:36:02',	'2'),
(10,	1,	1,	4,	0,	'2016-05-27 12:41:51',	'2016-05-27 12:41:51',	'2'),
(11,	1,	1,	4,	0,	'2016-05-27 12:41:51',	'2016-05-27 12:41:51',	'2'),
(12,	1,	1,	4,	0,	'2016-05-27 12:41:51',	'2016-05-27 12:41:51',	'2'),
(13,	1,	1,	4,	0,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(14,	1,	1,	4,	0,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(15,	1,	1,	4,	0,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(16,	1,	1,	4,	0,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(17,	1,	1,	4,	0,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(18,	1,	1,	4,	0,	'2016-05-27 12:49:35',	'2016-05-27 12:49:35',	'2'),
(19,	1,	1,	4,	0,	'2016-05-27 12:53:59',	'2016-05-27 12:53:59',	'2'),
(20,	1,	3,	4,	0,	'2016-05-27 12:54:12',	'2016-05-27 12:54:12',	'2'),
(21,	1,	1,	4,	0,	'2016-05-28 06:38:29',	'2016-05-28 06:38:29',	'2'),
(22,	1,	1,	4,	0,	'2016-05-28 06:58:55',	'2016-05-28 06:58:55',	'2'),
(23,	1,	1,	4,	0,	'2016-05-28 06:59:14',	'2016-05-28 06:59:14',	'2'),
(24,	1,	1,	4,	0,	'2016-05-28 06:59:51',	'2016-05-28 06:59:51',	'2'),
(25,	1,	1,	4,	0,	'2016-05-28 07:00:25',	'2016-05-28 07:00:25',	'2'),
(26,	1,	1,	4,	0,	'2016-05-28 08:07:18',	'2016-05-28 08:07:18',	'2'),
(27,	1,	1,	4,	0,	'2016-05-28 08:10:40',	'2016-05-28 08:10:40',	'2'),
(28,	1,	1,	4,	0,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1'),
(29,	1,	1,	4,	0,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1');

DROP TABLE IF EXISTS `lie_special_offer_day_maps`;
CREATE TABLE `lie_special_offer_day_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `special_offer_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_special_offer_day_maps` (`id`, `special_offer_id`, `day_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	0,	4,	'2016-05-27 10:28:13',	'2016-05-27 10:28:13',	'2'),
(2,	1,	2,	0,	4,	'2016-05-27 10:28:13',	'2016-05-27 10:28:13',	'2'),
(3,	1,	1,	0,	4,	'2016-05-27 10:28:31',	'2016-05-27 10:28:31',	'2'),
(4,	1,	2,	0,	4,	'2016-05-27 10:28:31',	'2016-05-27 10:28:31',	'2'),
(5,	1,	1,	0,	4,	'2016-05-27 10:38:30',	'2016-05-27 10:38:30',	'2'),
(6,	1,	2,	0,	4,	'2016-05-27 10:38:30',	'2016-05-27 10:38:30',	'2'),
(7,	1,	1,	0,	4,	'2016-05-27 10:39:34',	'2016-05-27 10:39:34',	'2'),
(8,	1,	2,	0,	4,	'2016-05-27 10:39:34',	'2016-05-27 10:39:34',	'2'),
(9,	1,	1,	0,	4,	'2016-05-27 10:39:44',	'2016-05-27 10:39:44',	'2'),
(10,	1,	2,	0,	4,	'2016-05-27 10:39:44',	'2016-05-27 10:39:44',	'2'),
(11,	1,	1,	0,	4,	'2016-05-27 10:43:23',	'2016-05-27 10:43:23',	'2'),
(12,	1,	2,	0,	4,	'2016-05-27 10:43:23',	'2016-05-27 10:43:23',	'2'),
(13,	1,	1,	0,	4,	'2016-05-27 10:53:45',	'2016-05-27 10:53:45',	'2'),
(14,	1,	2,	0,	4,	'2016-05-27 10:53:45',	'2016-05-27 10:53:45',	'2'),
(15,	3,	1,	0,	4,	'2016-05-27 11:36:02',	'2016-05-27 11:36:02',	'2'),
(16,	1,	1,	0,	4,	'2016-05-27 12:41:51',	'2016-05-27 12:41:51',	'2'),
(17,	1,	2,	0,	4,	'2016-05-27 12:41:51',	'2016-05-27 12:41:51',	'2'),
(18,	1,	1,	0,	4,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(19,	1,	2,	0,	4,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(20,	1,	1,	0,	4,	'2016-05-27 12:49:35',	'2016-05-27 12:49:35',	'2'),
(21,	1,	2,	0,	4,	'2016-05-27 12:49:35',	'2016-05-27 12:49:35',	'2'),
(22,	1,	1,	0,	4,	'2016-05-27 12:49:46',	'2016-05-27 12:49:46',	'2'),
(23,	1,	2,	0,	4,	'2016-05-27 12:49:46',	'2016-05-27 12:49:46',	'2'),
(24,	1,	1,	0,	4,	'2016-05-27 12:49:54',	'2016-05-27 12:49:54',	'2'),
(25,	1,	2,	0,	4,	'2016-05-27 12:49:54',	'2016-05-27 12:49:54',	'2'),
(26,	1,	1,	0,	4,	'2016-05-27 12:53:59',	'2016-05-27 12:53:59',	'2'),
(27,	1,	2,	0,	4,	'2016-05-27 12:53:59',	'2016-05-27 12:53:59',	'2'),
(28,	3,	1,	0,	4,	'2016-05-27 12:54:12',	'2016-05-27 12:54:12',	'2'),
(29,	3,	1,	0,	4,	'2016-05-27 12:54:21',	'2016-05-27 12:54:21',	'1'),
(30,	1,	2,	0,	4,	'2016-05-28 06:38:29',	'2016-05-28 06:38:29',	'2'),
(31,	1,	1,	0,	4,	'2016-05-28 06:58:56',	'2016-05-28 06:58:56',	'2'),
(32,	1,	3,	0,	4,	'2016-05-28 08:10:40',	'2016-05-28 08:10:40',	'2'),
(33,	1,	3,	0,	4,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1');

DROP TABLE IF EXISTS `lie_special_offer_day_time_maps`;
CREATE TABLE `lie_special_offer_day_time_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `special_offer_id` int(11) NOT NULL,
  `time_from` varchar(10) NOT NULL,
  `time_to` varchar(10) NOT NULL,
  `day_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_special_offer_day_time_maps` (`id`, `special_offer_id`, `time_from`, `time_to`, `day_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'00:00 AM',	'',	1,	4,	0,	'2016-05-28 06:38:29',	'2016-05-28 06:38:29',	'2'),
(2,	1,	'09:00 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 06:58:56',	'2016-05-28 06:58:56',	'2'),
(3,	1,	'01:00 AM',	'02:30 AM',	1,	4,	0,	'2016-05-28 06:59:15',	'2016-05-28 06:59:15',	'2'),
(4,	1,	'09:00 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 06:59:15',	'2016-05-28 06:59:15',	'2'),
(5,	1,	'01:00 AM',	'02:30 AM',	1,	4,	0,	'2016-05-28 06:59:51',	'2016-05-28 06:59:51',	'2'),
(6,	1,	'09:00 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 06:59:51',	'2016-05-28 06:59:51',	'2'),
(7,	1,	'01:00 AM',	'02:30 AM',	1,	4,	0,	'2016-05-28 07:00:25',	'2016-05-28 07:00:25',	'2'),
(8,	1,	'03:00 AM',	'04:00 AM',	1,	4,	0,	'2016-05-28 07:00:25',	'2016-05-28 07:00:25',	'2'),
(9,	1,	'09:00 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 07:00:25',	'2016-05-28 07:00:25',	'2'),
(10,	1,	'09:00 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 07:00:25',	'2016-05-28 07:00:25',	'2'),
(11,	1,	'01:00 AM',	'02:30 AM',	1,	4,	0,	'2016-05-28 08:07:18',	'2016-05-28 08:07:18',	'2'),
(12,	1,	'03:00 AM',	'04:00 AM',	1,	4,	0,	'2016-05-28 08:07:18',	'2016-05-28 08:07:18',	'2'),
(13,	1,	'05:00 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 08:07:18',	'2016-05-28 08:07:18',	'2'),
(14,	1,	'06:30 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 08:07:18',	'2016-05-28 08:07:18',	'2'),
(15,	1,	'01:00 AM',	'02:30 AM',	1,	4,	0,	'2016-05-28 08:10:40',	'2016-05-28 08:10:40',	'2'),
(16,	1,	'03:00 AM',	'04:00 AM',	1,	4,	0,	'2016-05-28 08:10:40',	'2016-05-28 08:10:40',	'2'),
(17,	1,	'05:00 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 08:10:40',	'2016-05-28 08:10:40',	'2'),
(18,	1,	'06:30 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 08:10:40',	'2016-05-28 08:10:40',	'2'),
(19,	1,	'01:00 AM',	'02:30 AM',	1,	4,	0,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1'),
(20,	1,	'03:00 AM',	'04:00 AM',	1,	4,	0,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1'),
(21,	1,	'05:00 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1'),
(22,	1,	'06:30 AM',	'07:00 AM',	2,	4,	0,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1');

DROP TABLE IF EXISTS `lie_special_offer_menu_maps`;
CREATE TABLE `lie_special_offer_menu_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `special_offer_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_special_offer_menu_maps` (`id`, `category_id`, `menu_id`, `special_offer_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	1,	4,	0,	'2016-05-27 10:38:30',	'2016-05-27 10:38:30',	'2'),
(2,	1,	1,	1,	4,	0,	'2016-05-27 10:39:34',	'2016-05-27 10:39:34',	'2');

DROP TABLE IF EXISTS `lie_special_offer_order_services`;
CREATE TABLE `lie_special_offer_order_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `special_offer_id` int(11) NOT NULL,
  `order_service_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_special_offer_order_services` (`id`, `special_offer_id`, `order_service_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	4,	0,	'2016-05-27 10:28:13',	'2016-05-27 10:28:13',	'2'),
(2,	1,	1,	0,	4,	'2016-05-27 10:28:31',	'2016-05-27 10:28:31',	'2'),
(3,	1,	1,	0,	4,	'2016-05-27 10:38:29',	'2016-05-27 10:38:29',	'2'),
(4,	1,	1,	0,	4,	'2016-05-27 10:39:34',	'2016-05-27 10:39:34',	'2'),
(5,	1,	1,	0,	4,	'2016-05-27 10:39:44',	'2016-05-27 10:39:44',	'2'),
(6,	1,	1,	0,	4,	'2016-05-27 10:43:23',	'2016-05-27 10:43:23',	'2'),
(7,	1,	1,	0,	4,	'2016-05-27 10:53:45',	'2016-05-27 10:53:45',	'2'),
(8,	2,	1,	4,	0,	'2016-05-27 11:35:26',	'2016-05-27 11:35:26',	'1'),
(9,	3,	1,	4,	0,	'2016-05-27 11:36:02',	'2016-05-27 11:36:02',	'2'),
(10,	1,	2,	0,	4,	'2016-05-27 12:41:51',	'2016-05-27 12:41:51',	'2'),
(11,	1,	1,	0,	4,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(12,	1,	2,	0,	4,	'2016-05-27 12:42:37',	'2016-05-27 12:42:37',	'2'),
(13,	1,	1,	0,	4,	'2016-05-27 12:49:35',	'2016-05-27 12:49:35',	'2'),
(14,	1,	2,	0,	4,	'2016-05-27 12:49:35',	'2016-05-27 12:49:35',	'2'),
(15,	1,	1,	0,	4,	'2016-05-27 12:49:45',	'2016-05-27 12:49:45',	'2'),
(16,	1,	2,	0,	4,	'2016-05-27 12:49:45',	'2016-05-27 12:49:45',	'2'),
(17,	1,	1,	0,	4,	'2016-05-27 12:49:53',	'2016-05-27 12:49:53',	'2'),
(18,	1,	2,	0,	4,	'2016-05-27 12:49:53',	'2016-05-27 12:49:53',	'2'),
(19,	1,	1,	0,	4,	'2016-05-27 12:53:59',	'2016-05-27 12:53:59',	'2'),
(20,	1,	2,	0,	4,	'2016-05-27 12:53:59',	'2016-05-27 12:53:59',	'2'),
(21,	3,	1,	0,	4,	'2016-05-27 12:54:12',	'2016-05-27 12:54:12',	'2'),
(22,	3,	1,	0,	4,	'2016-05-27 12:54:21',	'2016-05-27 12:54:21',	'1'),
(23,	1,	1,	0,	4,	'2016-05-28 06:38:29',	'2016-05-28 06:38:29',	'2'),
(24,	1,	2,	0,	4,	'2016-05-28 06:38:29',	'2016-05-28 06:38:29',	'2'),
(25,	1,	1,	0,	4,	'2016-05-28 06:58:55',	'2016-05-28 06:58:55',	'2'),
(26,	1,	2,	0,	4,	'2016-05-28 06:58:55',	'2016-05-28 06:58:55',	'2'),
(27,	1,	1,	0,	4,	'2016-05-28 06:59:14',	'2016-05-28 06:59:14',	'2'),
(28,	1,	2,	0,	4,	'2016-05-28 06:59:14',	'2016-05-28 06:59:14',	'2'),
(29,	1,	1,	0,	4,	'2016-05-28 06:59:51',	'2016-05-28 06:59:51',	'2'),
(30,	1,	2,	0,	4,	'2016-05-28 06:59:51',	'2016-05-28 06:59:51',	'2'),
(31,	1,	1,	0,	4,	'2016-05-28 07:00:25',	'2016-05-28 07:00:25',	'2'),
(32,	1,	2,	0,	4,	'2016-05-28 07:00:25',	'2016-05-28 07:00:25',	'2'),
(33,	1,	1,	0,	4,	'2016-05-28 08:07:18',	'2016-05-28 08:07:18',	'2'),
(34,	1,	2,	0,	4,	'2016-05-28 08:07:18',	'2016-05-28 08:07:18',	'2'),
(35,	1,	1,	0,	4,	'2016-05-28 08:10:40',	'2016-05-28 08:10:40',	'2'),
(36,	1,	2,	0,	4,	'2016-05-28 08:10:40',	'2016-05-28 08:10:40',	'2'),
(37,	1,	1,	0,	4,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1'),
(38,	1,	2,	0,	4,	'2016-05-28 08:16:04',	'2016-05-28 08:16:04',	'1');

DROP TABLE IF EXISTS `lie_special_offer_submenu_maps`;
CREATE TABLE `lie_special_offer_submenu_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `special_offer_id` int(11) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_special_offer_submenu_maps` (`id`, `category_id`, `menu_id`, `sub_menu_id`, `special_offer_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	1,	1,	1,	4,	0,	'2016-05-27 10:38:30',	'2016-05-27 10:38:30',	'2'),
(2,	1,	1,	1,	1,	4,	0,	'2016-05-27 10:39:34',	'2016-05-27 10:39:34',	'2');

DROP TABLE IF EXISTS `lie_spices`;
CREATE TABLE `lie_spices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_spices` (`id`, `name`, `image`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'testspice',	'1463723703900.jpg',	'testspice',	9,	2,	'2016-05-03 00:00:00',	'2016-05-25 10:26:52',	'1'),
(2,	'test2',	'1463721999235.jpg',	'test2',	9,	2,	'2016-05-20 05:12:46',	'2016-05-25 10:27:51',	'1'),
(3,	'test3',	'1463722013342.jpg',	'test3',	9,	9,	'2016-05-20 05:14:55',	'2016-05-20 05:26:53',	'1'),
(4,	'test4',	'1463722020750.jpg',	'test4',	9,	9,	'2016-05-20 05:16:06',	'2016-05-20 05:27:00',	'1'),
(5,	'test5',	'1463722027257.jpg',	'test5',	9,	9,	'2016-05-20 05:19:23',	'2016-05-20 05:27:07',	'1'),
(6,	'testspice1',	'1463723796330.jpg',	'testspice1',	9,	0,	'2016-05-20 05:56:36',	'2016-05-20 05:56:36',	'1'),
(7,	'testspice3',	'1463724444357.jpg',	'testspice3',	9,	9,	'2016-05-20 06:07:01',	'2016-05-20 06:09:41',	'1'),
(8,	'oioi',	'1463724600155.jpg',	'oio',	9,	9,	'2016-05-20 06:09:51',	'2016-05-20 06:10:13',	'2'),
(9,	'ddddd',	'1464172034616.jpg',	'dddddddddd',	2,	2,	'2016-05-25 10:27:01',	'2016-05-25 10:27:14',	'1'),
(10,	'Test',	'1464174998236.jpg',	'Test TEst Test',	2,	0,	'2016-05-25 11:16:38',	'2016-05-25 11:17:05',	'2');

DROP TABLE IF EXISTS `lie_states`;
CREATE TABLE `lie_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_states` (`id`, `country_id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'Wien',	'Test Test Test',	0,	0,	'0000-00-00 00:00:00',	'2016-06-01 00:20:00',	'1'),
(2,	2,	'Punjab',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(3,	4,	'Delhi',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(5,	5,	'Kerala\r\n',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(6,	6,	'Maharashtra\r\n',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(7,	7,	'Mizoram\r\n',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(8,	8,	'Haryana\r\n',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(9,	9,	'Jammu and Kashmir\r\n',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(10,	10,	'Rajasthan\r\n',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(11,	11,	'Goa\r\n',	'',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(12,	1,	'UK',	'UKUKUKUKUKUK',	0,	0,	'2016-05-25 10:31:26',	'2016-05-25 10:31:41',	'0'),
(13,	1,	'Uttar Pradesh',	'State of India',	0,	0,	'2016-05-25 11:38:28',	'2016-05-25 11:38:28',	'1');

DROP TABLE IF EXISTS `lie_sub_menu_price_maps`;
CREATE TABLE `lie_sub_menu_price_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_detail_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `price` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0=>inactive,1=>active,2=>deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_superadmin_login_logs`;
CREATE TABLE `lie_superadmin_login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` enum('L','O') NOT NULL COMMENT 'L for login,O for logout',
  `ip_address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `lie_superadmin_login_logs` (`id`, `user_id`, `type`, `ip_address`, `time`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	2,	'L',	'180.151.7.42',	'2016-05-25 09:41:09',	2,	2,	'2016-05-25 09:41:09',	'2016-05-25 09:41:09',	'1'),
(2,	2,	'L',	'180.151.7.42',	'2016-05-25 09:42:47',	2,	2,	'2016-05-25 09:42:47',	'2016-05-25 09:42:47',	'1'),
(3,	2,	'L',	'180.151.7.42',	'2016-05-25 10:07:00',	2,	2,	'2016-05-25 10:07:00',	'2016-05-25 10:07:00',	'1'),
(4,	2,	'L',	'180.151.7.42',	'2016-05-25 10:19:56',	2,	2,	'2016-05-25 10:19:56',	'2016-05-25 10:19:56',	'1'),
(5,	2,	'L',	'180.151.7.42',	'2016-05-25 10:19:58',	2,	2,	'2016-05-25 10:19:58',	'2016-05-25 10:19:58',	'1'),
(6,	2,	'L',	'180.151.7.42',	'2016-05-25 10:23:14',	2,	2,	'2016-05-25 10:23:14',	'2016-05-25 10:23:14',	'1'),
(7,	2,	'L',	'180.151.7.42',	'2016-05-25 10:31:16',	2,	2,	'2016-05-25 10:31:16',	'2016-05-25 10:31:16',	'1'),
(8,	2,	'L',	'180.151.7.42',	'2016-05-25 10:41:05',	2,	2,	'2016-05-25 10:41:05',	'2016-05-25 10:41:05',	'1'),
(9,	2,	'L',	'180.151.7.42',	'2016-05-25 10:47:18',	2,	2,	'2016-05-25 10:47:18',	'2016-05-25 10:47:18',	'1'),
(10,	2,	'L',	'80.110.78.199',	'2016-05-25 11:49:01',	2,	2,	'2016-05-25 11:49:01',	'2016-05-25 11:49:01',	'1'),
(11,	2,	'L',	'80.110.78.199',	'2016-05-25 23:02:52',	2,	2,	'2016-05-25 23:02:52',	'2016-05-25 23:02:52',	'1'),
(12,	2,	'L',	'80.110.78.199',	'2016-05-26 01:45:38',	2,	2,	'2016-05-26 01:45:38',	'2016-05-26 01:45:38',	'1'),
(13,	2,	'L',	'180.151.7.42',	'2016-05-26 04:54:14',	2,	2,	'2016-05-26 04:54:14',	'2016-05-26 04:54:14',	'1'),
(14,	2,	'O',	'180.151.7.42',	'2016-05-26 04:57:46',	2,	2,	'2016-05-26 04:57:46',	'2016-05-26 04:57:46',	'1'),
(15,	2,	'O',	'80.110.78.199',	'2016-05-28 00:33:21',	2,	2,	'2016-05-28 00:33:21',	'2016-05-28 00:33:21',	'1'),
(16,	2,	'L',	'80.110.78.199',	'2016-05-28 00:33:47',	2,	2,	'2016-05-28 00:33:47',	'2016-05-28 00:33:47',	'1'),
(17,	2,	'L',	'180.151.7.43',	'2016-05-28 04:57:45',	2,	2,	'2016-05-28 04:57:45',	'2016-05-28 04:57:45',	'1'),
(18,	2,	'L',	'180.151.7.43',	'2016-05-28 06:18:21',	2,	2,	'2016-05-28 06:18:21',	'2016-05-28 06:18:21',	'1'),
(19,	2,	'L',	'80.110.78.199',	'2016-05-29 02:35:33',	2,	2,	'2016-05-29 02:35:33',	'2016-05-29 02:35:33',	'1'),
(20,	2,	'O',	'80.110.78.199',	'2016-05-29 02:37:04',	2,	2,	'2016-05-29 02:37:04',	'2016-05-29 02:37:04',	'1'),
(21,	2,	'L',	'80.110.78.199',	'2016-05-29 22:50:16',	2,	2,	'2016-05-29 22:50:16',	'2016-05-29 22:50:16',	'1'),
(22,	2,	'L',	'80.110.78.199',	'2016-06-01 00:04:54',	2,	2,	'2016-06-01 00:04:54',	'2016-06-01 00:04:54',	'1'),
(23,	2,	'L',	'80.110.78.199',	'2016-06-01 00:11:46',	2,	2,	'2016-06-01 00:11:46',	'2016-06-01 00:11:46',	'1'),
(24,	2,	'L',	'80.110.78.199',	'2016-06-01 00:14:31',	2,	2,	'2016-06-01 00:14:31',	'2016-06-01 00:14:31',	'1'),
(25,	2,	'L',	'80.110.78.199',	'2016-06-01 19:48:27',	2,	2,	'2016-06-01 19:48:27',	'2016-06-01 19:48:27',	'1'),
(26,	2,	'L',	'80.110.78.199',	'2016-06-02 23:36:52',	2,	2,	'2016-06-02 23:36:52',	'2016-06-02 23:36:52',	'1'),
(27,	2,	'L',	'180.151.7.43',	'2016-06-03 07:59:32',	2,	2,	'2016-06-03 07:59:32',	'2016-06-03 07:59:32',	'1'),
(28,	2,	'O',	'180.151.7.43',	'2016-06-03 07:59:47',	2,	2,	'2016-06-03 07:59:47',	'2016-06-03 07:59:47',	'1'),
(29,	2,	'L',	'180.151.7.43',	'2016-06-03 12:38:24',	2,	2,	'2016-06-03 12:38:24',	'2016-06-03 12:38:24',	'1'),
(30,	2,	'O',	'180.151.7.43',	'2016-06-03 12:38:30',	2,	2,	'2016-06-03 12:38:30',	'2016-06-03 12:38:30',	'1'),
(31,	2,	'L',	'80.110.78.199',	'2016-06-03 23:29:33',	2,	2,	'2016-06-03 23:29:33',	'2016-06-03 23:29:33',	'1'),
(32,	2,	'L',	'80.110.78.199',	'2016-06-03 23:35:05',	2,	2,	'2016-06-03 23:35:05',	'2016-06-03 23:35:05',	'1'),
(33,	2,	'L',	'80.110.78.199',	'2016-06-05 00:40:53',	2,	2,	'2016-06-05 00:40:53',	'2016-06-05 00:40:53',	'1'),
(34,	2,	'O',	'80.110.78.199',	'2016-06-05 00:40:59',	2,	2,	'2016-06-05 00:40:59',	'2016-06-05 00:40:59',	'1'),
(35,	2,	'L',	'180.151.7.42',	'2016-06-07 10:40:34',	2,	2,	'2016-06-07 10:40:34',	'2016-06-07 10:40:34',	'1'),
(36,	2,	'L',	'80.110.111.223',	'2016-06-08 00:38:37',	2,	2,	'2016-06-08 00:38:37',	'2016-06-08 00:38:37',	'1'),
(37,	2,	'L',	'180.151.7.42',	'2016-06-08 07:41:49',	2,	2,	'2016-06-08 07:41:49',	'2016-06-08 07:41:49',	'1'),
(38,	2,	'L',	'180.151.7.42',	'2016-06-09 05:21:51',	2,	2,	'2016-06-09 05:21:51',	'2016-06-09 05:21:51',	'1'),
(39,	2,	'L',	'180.151.7.43',	'2016-06-11 06:42:16',	2,	2,	'2016-06-11 06:42:16',	'2016-06-11 06:42:16',	'1'),
(40,	2,	'O',	'180.151.7.43',	'2016-06-11 06:42:34',	2,	2,	'2016-06-11 06:42:34',	'2016-06-11 06:42:34',	'1'),
(41,	2,	'L',	'180.151.7.43',	'2016-06-11 06:43:58',	2,	2,	'2016-06-11 06:43:58',	'2016-06-11 06:43:58',	'1'),
(42,	2,	'L',	'120.57.239.25',	'2016-06-11 13:01:22',	2,	2,	'2016-06-11 13:01:22',	'2016-06-11 13:01:22',	'1'),
(43,	2,	'L',	'80.110.105.229',	'2016-06-11 22:57:30',	2,	2,	'2016-06-11 22:57:30',	'2016-06-11 22:57:30',	'1'),
(44,	2,	'L',	'180.151.7.43',	'2016-06-13 06:03:09',	2,	2,	'2016-06-13 06:03:09',	'2016-06-13 06:03:09',	'1'),
(45,	2,	'L',	'180.151.7.43',	'2016-06-13 06:05:53',	2,	2,	'2016-06-13 06:05:53',	'2016-06-13 06:05:53',	'1'),
(46,	2,	'O',	'180.151.7.43',	'2016-06-13 06:06:01',	2,	2,	'2016-06-13 06:06:01',	'2016-06-13 06:06:01',	'1'),
(47,	2,	'L',	'180.151.7.43',	'2016-06-13 06:36:37',	2,	2,	'2016-06-13 06:36:37',	'2016-06-13 06:36:37',	'1'),
(48,	2,	'L',	'180.151.7.43',	'2016-06-13 07:50:39',	2,	2,	'2016-06-13 07:50:39',	'2016-06-13 07:50:39',	'1'),
(49,	2,	'O',	'180.151.7.43',	'2016-06-13 07:56:37',	2,	2,	'2016-06-13 07:56:37',	'2016-06-13 07:56:37',	'1'),
(50,	2,	'O',	'180.151.7.43',	'2016-06-13 08:25:09',	2,	2,	'2016-06-13 08:25:09',	'2016-06-13 08:25:09',	'1'),
(51,	2,	'L',	'180.151.7.43',	'2016-06-13 08:25:18',	2,	2,	'2016-06-13 08:25:18',	'2016-06-13 08:25:18',	'1'),
(52,	2,	'L',	'180.151.7.43',	'2016-06-13 09:46:12',	2,	2,	'2016-06-13 09:46:12',	'2016-06-13 09:46:12',	'1');

DROP TABLE IF EXISTS `lie_tax_settings`;
CREATE TABLE `lie_tax_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_tax_settings` (`id`, `tax_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4,	'tax',	'settings',	2,	2,	'2016-05-25 10:25:07',	'2016-05-25 10:25:15',	'1'),
(5,	'Test',	'TEST Tax',	2,	2,	'2016-05-25 11:14:12',	'2016-05-25 11:14:20',	'1');

DROP TABLE IF EXISTS `lie_time_validations`;
CREATE TABLE `lie_time_validations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `year` tinyint(11) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `week` tinyint(4) NOT NULL,
  `day` tinyint(4) NOT NULL,
  `description` text NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '1 for active,0 for deactive,2 for delete',
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_time_validations` (`id`, `name`, `year`, `month`, `week`, `day`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1,	'dfdfd',	1,	0,	1,	0,	'ouio',	'2',	9,	9,	'2016-05-06 05:38:16',	'2016-05-06 09:55:52'),
(2,	'fgjgj',	1,	0,	0,	0,	'fdgfg',	'2',	9,	9,	'2016-05-06 05:38:41',	'2016-05-06 10:04:24'),
(3,	'hgfhg',	0,	1,	0,	0,	'jhjh',	'2',	9,	0,	'2016-05-06 06:05:49',	'2016-05-09 06:58:42'),
(4,	'gjhhjh',	1,	0,	0,	0,	'jgjh',	'2',	9,	0,	'2016-05-06 06:06:04',	'2016-05-09 06:58:42'),
(5,	'gjhhjhkl',	0,	0,	0,	0,	'jgjh',	'2',	9,	0,	'2016-05-06 06:06:15',	'2016-05-09 06:58:42'),
(6,	'1 year 2 month 6 days validity',	1,	2,	0,	6,	'1 year 2 month 6 days validity',	'1',	9,	1,	'2016-05-09 07:06:01',	'2016-05-16 06:52:39'),
(7,	'1 year 2 weeks',	1,	0,	2,	0,	'1 year 2 weeks',	'1',	1,	0,	'2016-05-16 06:53:07',	'2016-05-16 06:53:07'),
(8,	'Test',	1,	0,	0,	15,	'Test Test',	'1',	2,	2,	'2016-05-25 11:20:49',	'2016-05-25 11:21:03');

DROP TABLE IF EXISTS `lie_users`;
CREATE TABLE `lie_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_role_id` (`user_role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_users` (`id`, `user_role_id`, `email`, `password`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	2,	'nikhil.alivenet@gmail.com',	'$2y$10$Ib5HGOKBILjqXTHXixbW3upZqDC5zrq1qQX3DKk6x8WPNhBWq627.',	0,	0,	1,	'0000-00-00 00:00:00',	'2016-05-16 06:25:55',	'1'),
(2,	1,	'vikas@gmail.com',	'$2y$10$NGgP.6Q7M89btqz.khoVz.cdT2tDqUudv0DwWg81JrNysI.44aVay',	4,	1,	2,	'2016-05-16 05:16:11',	'2016-06-13 08:25:09',	'1');

DROP TABLE IF EXISTS `lie_user_contact_maps`;
CREATE TABLE `lie_user_contact_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_user_contact_maps` (`id`, `user_id`, `contact`, `created_by`, `updated_by`, `created_at`, `updated_at`, `flag`, `status`) VALUES
(1,	2,	'9876345212',	1,	0,	'2016-05-16 05:16:11',	'2016-05-16 05:16:11',	1,	'2'),
(2,	2,	'9121211111',	1,	0,	'2016-05-16 05:16:11',	'2016-05-16 05:16:11',	0,	'2'),
(4,	1,	'9876543211',	0,	1,	'2016-05-16 06:25:55',	'2016-05-16 06:25:55',	1,	'0'),
(5,	2,	'7876222011',	0,	2,	'2016-05-16 09:13:44',	'2016-05-16 09:13:44',	1,	'0'),
(6,	2,	'',	0,	2,	'2016-05-16 09:13:44',	'2016-05-16 09:13:44',	0,	'0');

DROP TABLE IF EXISTS `lie_user_email_maps`;
CREATE TABLE `lie_user_email_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `flag` tinyint(1) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_user_email_maps` (`id`, `user_id`, `email`, `created_by`, `updated_by`, `created_at`, `updated_at`, `flag`, `status`) VALUES
(1,	2,	'rekha@gmail.com',	1,	0,	'2016-05-16 05:16:11',	'2016-05-16 05:16:11',	1,	'2'),
(2,	2,	'rekha@gmail.com',	1,	0,	'2016-05-16 05:16:11',	'2016-05-16 05:16:11',	0,	'2'),
(4,	1,	'nikhil.alivenet@gmail.com',	0,	1,	'2016-05-16 06:25:55',	'2016-05-16 06:25:55',	1,	'0'),
(5,	2,	'vikas@gmail.com',	0,	2,	'2016-05-16 09:13:44',	'2016-05-16 09:13:44',	1,	'0'),
(6,	2,	'',	0,	2,	'2016-05-16 09:13:44',	'2016-05-16 09:13:44',	0,	'0');

DROP TABLE IF EXISTS `lie_user_masters`;
CREATE TABLE `lie_user_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `doj` date NOT NULL,
  `profile_pic` varchar(250) NOT NULL,
  `add1` varchar(100) NOT NULL,
  `add2` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `zipcode` varchar(6) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_user_masters` (`id`, `user_id`, `first_name`, `last_name`, `email`, `contact`, `dob`, `doj`, `profile_pic`, `add1`, `add2`, `city`, `state`, `country`, `zipcode`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'Nikhil',	'Nikhil',	'nikhil.alivenet@gmail.com',	'9876543211',	'1989-03-30',	'2016-05-15',	'',	'A-512,Gali no 10',	'South Ganesh Nagar',	'1',	'1',	'1',	'201301',	0,	1,	'0000-00-00 00:00:00',	'2016-05-16 06:25:55',	'1'),
(2,	2,	'vikas',	'batra',	'vikas@gmail.com',	'7876222011',	'1989-03-30',	'2016-05-16',	'1463390024742.jpg',	'A-512,Gali no 10',	'South Ganesh Nagar',	'1',	'1',	'1',	'123456',	1,	2,	'2016-05-16 05:16:11',	'2016-05-16 09:48:14',	'1');

DROP TABLE IF EXISTS `lie_user_roles`;
CREATE TABLE `lie_user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_user_roles` (`id`, `role_name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'Super Admin',	'Super Admin',	1,	1,	'0000-00-00 00:00:00',	'2016-05-14 11:47:29',	'1'),
(2,	'Sales',	'sales',	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'1'),
(3,	'admin',	'admin',	1,	1,	'2016-04-19 12:38:04',	'2016-04-19 12:38:04',	'1'),
(4,	'Vertrieb Manager',	'Vertrieb CEO',	0,	0,	'2016-05-18 22:23:44',	'2016-05-18 22:23:44',	'1'),
(5,	'Vertrieb',	'Vertrieb Agent',	0,	0,	'2016-05-18 22:24:37',	'2016-05-18 22:24:37',	'1'),
(6,	'CEO',	'Recai Firat',	0,	0,	'2016-05-18 22:36:34',	'2016-05-18 22:36:34',	'1'),
(7,	'Tester',	'Test TEst Test Test 12345',	0,	0,	'2016-05-25 11:26:37',	'2016-05-25 11:29:43',	'1'),
(8,	'Web Designer',	'Web designing, Graphic Designing, Logo Designing, Template Designing',	0,	0,	'2016-06-08 07:56:08',	'2016-06-08 07:57:50',	'2');

DROP TABLE IF EXISTS `lie_user_role_maps`;
CREATE TABLE `lie_user_role_maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1 for active,0 for deactive,2 for delete',
  PRIMARY KEY (`id`),
  KEY `lie_user_id` (`user_id`),
  KEY `lie_user_role_id` (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `lie_videos`;
CREATE TABLE `lie_videos` (
  `id` int(11) NOT NULL,
  `video_title` varchar(50) NOT NULL,
  `video_name` varchar(255) NOT NULL,
  `created_by` tinyint(4) NOT NULL,
  `updated_by` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL COMMENT '0 for incative,1 for active,2 for delete ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_videos` (`id`, `video_title`, `video_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	'test',	'1465477605315.mp4',	9,	2,	'2016-06-28 00:00:00',	'2016-06-09 13:06:45',	'1');

DROP TABLE IF EXISTS `lie_zipcodes`;
CREATE TABLE `lie_zipcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('0','1','2','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lie_zipcodes` (`id`, `city_id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(1,	1,	'110059',	'New Delhi.',	1,	0,	'2016-04-29 12:46:13',	'2016-04-29 13:22:35',	'1'),
(2,	1,	'23232',	'fgnffnfghnfgngf',	0,	0,	'2016-04-29 12:53:24',	'2016-04-29 12:56:46',	'1'),
(3,	11,	'101010',	'Innere Stadt',	0,	0,	'2016-05-18 22:34:20',	'2016-05-18 22:34:20',	'1'),
(4,	1,	'122020',	'TEST TEST TEST',	0,	0,	'2016-05-25 10:28:54',	'2016-05-25 10:28:54',	'1'),
(5,	1,	'110017',	'Delhi Cantt',	0,	0,	'2016-05-25 10:31:57',	'2016-05-25 10:31:57',	'1'),
(6,	11,	'142030',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(7,	11,	'256301',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(8,	11,	'789630',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(9,	11,	'654120',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(10,	11,	'121010',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(11,	11,	'258741',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(12,	11,	'121252',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(13,	11,	'258963',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(14,	11,	'124545',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(15,	11,	'121002',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1'),
(16,	11,	'121101',	'test ztest etst',	0,	0,	'2016-05-25 11:29:03',	'2016-05-25 11:29:03',	'1');

-- 2016-06-13 10:13:32
