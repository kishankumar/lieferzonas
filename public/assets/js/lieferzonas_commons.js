/* Restaurant Links */

var baseHost  = window.location.origin;
var baseUrl = baseHost+'/lieferzonas_new';

$(function() {
    $("[data-rest-link]").click(function(evt) {
        var restaurantId = $(this).data("rest-link");
        if (restaurantId) {
            window.location.href = baseUrl+"/restaurant/" + restaurantId + "/details";
        }
    });
});



/* ---------------- */

/* Abholung Zustellung */

function registerPickupDeliveryToggle(csrfToken, successCallback, errorCallback) {
    $(".lie-pickup-toggle").each(function (index, element) {
        $(element).change(function () {
            var jqElement = $(this);
            setPickupDelivery(!jqElement.prop("checked"), csrfToken, successCallback, errorCallback);
        });
    });
}

function setPickupDelivery(isPickup, csrfToken, successCallback, errorCallback) {
    $.ajax({
        url: baseUrl+"/set_pickup",
        data: {
            _token: csrfToken,
            is_pickup: isPickup
        },
        type: "post",
        dataType: "json",
        success: function (response) {
            if (successCallback) successCallback(response.new_is_pickup);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.error("Konnte nicht zwischen Zustellung und Abholung wechseln!");
            console.error(textStatus);
            if (errorCallback) errorCallback();
        }
    });
}

/* ------------------- */

/* Währung */

function currency_format(number) {
    if (!number) return "€ --,--";
    if (typeof(number) == "string") {
        number = parseFloat(number);
    }
    return "€ " + number.toFixed(2).replace(/\./, ",");
}

/* ------- */

/* Location */

function geolocationSuccess(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    $.ajax({
        url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true',
        type: "POST",
        dataType: "json",
        accepts: 'application/json',
        data: {},
        success: function (data) {
            var len = data.results[0]['address_components'].length;
            var zipcode = data.results[0]['address_components'][len - 1]['long_name'];
            $(".lie-zipcode-input-field").val(zipcode);
        }
    });
}

function geolocationError(error) {
    alert("Fehler während der Ortung: (" + error.code + ") " + error.message);
}

function findDistance(t1, n1, t2, n2) {
    var Rm = 3961;
    var Rk = 6373;

    var lat1, lon1, lat2, lon2, dlat, dlon, a, c, dm, dk, mi, km;

    lat1 = deg2rad(t1);
    lon1 = deg2rad(n1);
    lat2 = deg2rad(t2);
    lon2 = deg2rad(n2);

    dlat = lat2 - lat1;
    dlon = lon2 - lon1;

    a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    dm = c * Rm;
    dk = c * Rk;

    mi = round(dm);
    km = round(dk);

    return km;
}

/* -------- */

/* Adressen */

function showAddAddressModal() {
    $("#lie-add-address-modal").modal("show");
}

function showEditAddressModal(address) {
    $("#lie-edit-address-modal-name").val(address.booking_person_name);
    $("#lie-edit-address-modal-street").val(address.street);
    $("#lie-edit-address-modal-house").val(address.house);
    $("#lie-edit-address-modal-staircase").val(address.staircase);
    $("#lie-edit-address-modal-floor").val(address.floor);
    $("#lie-edit-address-modal-apartment").val(address.apartment);
    $("#lie-edit-address-modal-zipcode").val(address.zipcode);
    $("#lie-edit-address-modal-city").val(address.city_name);
    $("#lie-edit-address-modal-phone-number").val(address.mobile);
    $("#lie-edit-address-modal-company").val(address.company);
    $("#lie-edit-address-modal-info").val(address.info);
    $("#lie-edit-address-modal-id").val(address.id);
    $("#lie-edit-address-modal").modal("show");
}

function onClickConfirmEditAddressButton(csrfToken, callback) {
    var addressId = $("#lie-edit-address-modal-id").val();
    $.ajax({
        url: baseUrl+"/front/addresses/" + addressId + "?" + $("#lie-edit-address-modal-form").serialize(),
        type: "PUT",
        dataType: "json",
        data: {
            "_token": csrfToken
        },
        success: function(response) {
            callback(response);
        },
        error: function() {
            callback({"status": "failure"});
        }
    });
}

function onClickConfirmAddAddressButton(csrfToken, callback) {
    $.ajax({
        url: baseUrl+"/front/addresses?" + $("#lie-add-address-modal-form").serialize(),
        type: "POST",
        dataType: "json",
        data: {
            "_token": csrfToken
        },
        success: function(response) {
            callback(response);
        },
        error: function() {
            callback({"status": "failure"});
        }
    });
}

function deleteAddress(csrfToken, addressId, callback) {
    $.ajax({
        url: baseUrl+"/front/addresses/" + addressId,
        type: "DELETE",
        dataType: "json",
        data: {
            "_token": csrfToken
        },
        success: function(response) {
            callback(response);
        },
        error: function() {
            callback({"status": "failure"});
        }
    });
}

/* -------- */

/* Postleitzahlen */

function checkZipCodeSuccess() {
    $("#lie-zip-form").submit();
}

function checkZipCodeError(error) {
    alert("Fehler beim Suchen: " + error);
}

function checkZipCode(token, success, error) {
    var zip = $('.lie-zipcode-input-field').first().val();
    $.ajax({
        url: baseUrl+"/checkzip_new",
        type: "post",
        dataType: "json",
        data: {
            zipcode: zip,
            "_token": token
        },
        success: function(data){
            if (data.status != "success") return error(data.reason);
            else return success(data.id);
        }
    });
}

/* -------------- */

/* Favoriten */

function onClickToggleFavoriteButton(csrfToken, restaurantId) {
    $.ajax({
        url: baseUrl+"/front/favourite/toggle",
        data: {
            "_token": csrfToken,
            "rest_id": restaurantId
        },
        type: "POST",
        dataType: "json",
        success: function (response) {
            if (response.status == "success") {
                var isNowFavorite = response.isFavorite;
                var favoriteCount = response.count;
                $(".lie-rest-favorite-count[data-rest='" + restaurantId + "']").text("" + favoriteCount);
                if (isNowFavorite) {
                    $(".lie-rest-favorite-wrap[data-rest='" + restaurantId + "']").addClass("active");
                }
                else {
                    $(".lie-rest-favorite-wrap[data-rest='" + restaurantId + "']").removeClass("active");
                }
            }
        }
    });
}

function registerToggleFavoriteButtons(csrfToken, callback) {
    $(".lie-toggle-favorite-button").click(function (evt) {
        var restaurantId = $(this).data("restaurant");
        $.ajax({
            url: baseUrl+"/front/favourite/toggle",
            data: {
                "_token": csrfToken,
                "rest_id": restaurantId
            },
            type: "POST",
            dataType: "json",
            success: function (response) {
                if (response.status == "success") {
                    callback(restaurantId);
                }
            }
        });
        evt.stopPropagation();
    });
}

/* --------- */

/* Filter Kontroll-Elemente */

$(function () {
    $(".lie-checkbox-input").change(function () {
        if ($(this).prop("checked")) {
            $(this).parent().find(".lie-checkbox-icon").html("<i class='fa fa-check'></i>");
            $(this).parent().addClass("lie-checkbox-label-checked");
        }
        else {
            $(this).parent().find(".lie-checkbox-icon").html("<i class='fa fa-square-o'></i>");
            $(this).parent().removeClass("lie-checkbox-label-checked");
        }
    });
    $(".lie-checkbox-input").trigger("change");
});

/* ------------------------ */

/* Postleitzahl Eingabe Formular */

$("#lie-geolocation-button").click(function(evt) {
    if (!window.navigator.geolocation) {
        return alert("Die Ortungs-API ist in diesem Browser nicht verfügbar.");
    }
    window.navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
});

$("#lie-continue-button").click(function(evt) {
    var token = $(this).data("csrf");
    checkZipCode(token, checkZipCodeSuccess, checkZipCodeError);
});

/* ------------------ */

/* Utilities */

function lie_sync_request(csrfToken, path, data, method) {

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    var tokenField = document.createElement("input");
    tokenField.setAttribute("type", "hidden");
    tokenField.setAttribute("name", "_token");
    tokenField.setAttribute("value", csrfToken);
    form.appendChild(tokenField);

    for(var key in data) {
        if(data.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", data[key]);
            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

/* --------- */

/* Checkout */

$(function() {
    var submitButton = $("#lie-submit-order-button");
    var acceptTermsCheckbox = $("#lie-accept-terms");
    submitButton.addClass("disabled");
    acceptTermsCheckbox.change(function(evt) {
        if (acceptTermsCheckbox.prop("checked")) {
            submitButton.removeClass("disabled");
        }
        else {
            submitButton.addClass("disabled");
        }
    });
});

/* -------- */

/* Tooltips */

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

/* -------- */

/* Rating */

function setRating(id, value) {

    var html = "";

    for (var i = 0; i < value; i++) {
        html
    }

    $("#" + id);
    return value;
}

/* ------ */

/* Events */

function onButtonClick(buttonSelector, callback) {
    $(buttonSelector).click(function(evt) {
        if ($(this).hasClass("disabled") || $(this).prop("disabled")) return;
        return callback(evt);
    });
}

/* ------ */

/* Eingabe Validierung */

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/* ------------------- */